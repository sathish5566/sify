var dtCh= "/";
var global_submit_type="";

function getHTTPObject()
{
  try
  {
  req = new XMLHttpRequest();
  }
  catch (err1)
  {
    try
    {
    req = new ActiveXObject("Msxml12.XMLHTTP");
    }
    catch (err2)
    {
    try
    {
    req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    catch (err3)
    {
    req = false;
    }
    }
  }
return req;
}
var http = getHTTPObject();

function dv(obj){
  if(document.getElementById){
    if(document.getElementById(obj)!=null)
      return document.getElementById(obj);
    else
      return '';
  }
}


function trim(value) {
   var temp = value;
   var obj = /^(\s*)([\W\w]*)(\b\s*$)/;
   if (obj.test(temp)) { temp = temp.replace(obj, '$2'); }
   var obj = / +/g;
   temp = temp.replace(obj, " ");
   if (temp == " ") { temp = ""; }
   return temp;
}


function get_radio_value(obj)
{	
	var rad_val='';
	for (var i=0; i < obj.length; i++)
  	{
	   if (obj[i].checked)
	   {
		  var rad_val = obj[i].value;
	   }
   	}
 return  rad_val;
}

//anuja added reg
function isemail(objemail) {
   //var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
     var reg = /^([a-zA-z0-9])*([0-9a-zA-z\.])\w+([\.\-]\w+)*\@\w+([\-\.]\w+)*\.[a-z]{2,4}$/gi
   var address = objemail.value;
   if(reg.test(address) == false) {
      return false;
   }else{
	return true;
   }
}
//close

function validateSearch() {
	var intSrhValue = document.getElementById('selSearchType').value
	if(intSrhValue=='')
	{
		alert("Please Select Search Type!...");
		document.getElementById('selSearchType').focus();
		return false;
	}
	return true;
}

function txtmob()
{
	if(!isNumeric('txtmob'))
	{
		alert("Mobile No should be 10 digits");
		dv('txtmob').focus();
		return false;
	}
	else if(dv('txtmob').value==0)
	{
		alert("Mobile No should not be zero(s)");
		dv('txtmob').focus();
		return false;
	}
	else if(trim(dv('txtmob').value).length!=10)
	{
		alert("Mobile No should be 10 digits");
		dv('txtmob').focus();
		return false;
	}
	else if((trim(dv('txtmob').value).substr(0,1)!=9) && (trim(dv('txtmob').value).substr(0,1)!=8) && (trim(dv('txtmob').value).substr(0,1)!=7) && (trim(dv('txtmob').value).substr(0,1)!=6))
	{
		alert("Mobile No should start with 6 or 7 or 8 or 9.");
		dv('txtmob').focus();
		return false;
	}
return true ;

}


function alpha(e)
{	
	var key;
	var keychar;
	if (window.event){
		key = window.event.keyCode;		
	}else if (e){
		key = e.which;		
	}
	else
		return true;
	if((key == 8) || (key == 0))
		return true;
		
	keychar = String.fromCharCode(key);
	keychar = keychar.toLowerCase();
	if(key!=32)
	{ 
		
	invalids = "`~@#$%^*()_+=\|{}[]:;'\"<>&?/!,.-1234567890\\";
	//	invalids = "`~@#$%^*()_+=\|\,{}[]:;'\"<>&?/!\\";
		  for(i=0; i<invalids.length; i++) {
			if(keychar.indexOf(invalids.charAt(i)) >= 0 || keychar==false) {				           			  
				   return false;               
			}
		  }
	}
	return true;
		
}

function alphaspacedothyphen(e)
{	
	var key;
	var keychar;
	if (window.event){
		key = window.event.keyCode;		
	}else if (e){
		key = e.which;		
	}
	else
		return true;
	if((key == 8) || (key == 0))
		return true;
		
	keychar = String.fromCharCode(key);
	keychar = keychar.toLowerCase();
	if(key!=32)
	{ 
		
	invalids = "`~@#$%^*()_+=\|{}[]:;'\"<>&?/!,1234567890\\";
		  for(i=0; i<invalids.length; i++) {
			if(keychar.indexOf(invalids.charAt(i)) >= 0 || keychar==false) {				           			  
				   return false;               
			}
		  }
	}
	return true;
		
}

function isNameFormat(obj_nam){
    var x = trim(document.getElementById(obj_nam).value);    
    var invalids = "`~@#$%^*()_+=\|{}[]:;,'\"<>?!/1234567890";    
    for(i=0; i<invalids.length; i++) {	  
	  if(x.indexOf(invalids.charAt(i)) >= 0 || x==false) {				           			  			   
			   return false;               
		}
    }
    return true;
}

function checkallzeroes(str)
{	
	var zerostr="";
	var len=str.length;
	for(j=0;j<len;j++)
	{
		zerostr+="0";
	}
	if(str == zerostr)
		return false;
	return true;
}


function isNumeric(obj_nam){
  var x=trim(document.getElementById(obj_nam).value);
  var anum=/(^\d+$)/;
  if (anum.test(x)){    
	return true;
  }else{
	  return false;             
  }
}



function alphactrl(e)
{	
	var key;
	var keychar;
	var isCtrl;
	var forbiddenKeys = new Array('a', 'n', 'c', 'x', 'v', 'j');
	if (window.event){
		key = window.event.keyCode;	
		if(window.event.ctrlKey)
			isCtrl = true;
        else
            isCtrl = false;
	}else if (e){
		key = e.which;
		if(e.ctrlKey)
			isCtrl = true;
        else
            isCtrl = false;
	}
	else
		return true;
	//alert(key);
	if(isCtrl)
	{
		for(i=0; i<forbiddenKeys .length; i++)
		{
				//case-insensitive comparation				
			if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase())
			{
					alert('Key combination CTRL + '
							+String.fromCharCode(key)
							+' has been disabled.');
					return false;
			}
		}
	}

	if((key == 8) || (key == 0))
		return true;
	
}

function number(e)
{	
	var key;
	var keychar;
	if (window.event){
		key = window.event.keyCode;		
	}else if (e){
		key = e.which;		
	}
	else
		return true;

	if((key == 8) || (key == 0))
		return true;
		
	keychar = String.fromCharCode(key);
	keychar = keychar.toLowerCase();
	if((key > 47) && (key < 58) || (key > 95) && (key < 106)){				
		return true;
	}else
	    return false;	
}

function checkCaptcha()
{
	if(document.reg_frm.txtCode.value=='')
	{
		alert('Please enter the Security Code');
		document.reg_frm.txtCode.value='';
		document.reg_frm.txtCode.focus();
		return false;
	} // Now the Ajax CAPTCHA validation
	else
	{
		checkcode(document.reg_frm.txtCode.value);
		return false;
	}
}

function send_reg_mail(rno){
	var c = confirm('Do you want to re-send registration mail?');
	if(c)
	{
		var url="../admin/send_reg_mail.php?rno="+rno;
		http.open("POST", url, true);
		http.onreadystatechange = handleSendMailResponse;
		http.send(null);
	}
}

function handleSendMailResponse()
{
//alert(http.responseText) ;
	if (http.readyState == 4)
	{
		resp = http.responseText;
		if(resp != 'Y')
		{
			alert('Registration mail not sent. Please try again');
			return false;	
		}
		else
		{
			alert('Registration mail sent successfully.');
		}
	}
}


function checkcode(thecode, submit_type){
	var url="./captcha/secureck.php?security_code="+escape(thecode);
	http.open("POST", url, true);
	http.onreadystatechange = handleHttpResponse ;
	http.send(null);
}
function checkcode2(thecode, submit_type){
	
	var url="./captcha/secureck.php?security_code="+escape(thecode);
	http.open("POST", url, true);
	http.onreadystatechange = handleHttpResponse3 ;
	http.send(null);
}
function handleHttpResponse3()
{	
	var mtyp = document.getElementById('hdnMtype').value;
	var ecd = document.getElementById('hdnExId').value
	if (http.readyState == 4)
	{
		captchaOK = http.responseText;
	
		if(captchaOK != 'true')
		{
			alert('Incorrect Security Code. Please try again');
			document.Elogin_frm.txtCode.value='';
			document.getElementById('image').src = './captcha/CaptchaSecurityImages.php?width=100&height=40&characters=5&sid='+Math.random();
			document.Elogin_frm.txtCode.focus();
			return false;	
		}
		else
		{ 
			//Modified by sathya on 05-02-2014 to display the message
			if(document.getElementById('hdnExId').value== '21' || document.getElementById('hdnExId').value=='60' || document.getElementById('hdnExId').value=='62' || document.getElementById('hdnExId').value=='63' || document.getElementById('hdnExId').value=='64' ||document.getElementById('hdnExId').value=='65' ||document.getElementById('hdnExId').value=='66' ||document.getElementById('hdnExId').value=='67' ||document.getElementById('hdnExId').value=='68' ||document.getElementById('hdnExId').value=='69' ||document.getElementById('hdnExId').value=='70' ||document.getElementById('hdnExId').value=='71' ||document.getElementById('hdnExId').value=='72' ) {

				alert("# Read Carefully - IMPORTANT ANNOUNCEMENT for JAIIB/CAIIB/Cert. in Electives - May/June 2014 Examination on the website given under EXAM RELATED before applying.");
			}
			document.Elogin_frm.rrsub.value = "regSubmit" ;
			document.Elogin_frm.action = "ExamLogin.php?Mtype="+mtyp+"&ExId="+ecd ;
			document.Elogin_frm.submit();

		}
	}
}

function checkcode1(thecode, submit_type){
	var url="./captcha/secureck.php?security_code="+escape(thecode);
	http.open("POST", url, true);
	http.onreadystatechange = handleHttpResponse2 ;
	http.send(null);
}
function handleHttpResponse2()
{
		var mtyp1 = document.getElementById('Mtype').value;
	if (http.readyState == 4)
	{
		captchaOK = http.responseText;
		if(captchaOK != 'true')
		{
			alert('Incorrect Security Code. Please try again');
			document.login_frm.txtCode.value='';
			document.getElementById('image').src = './captcha/CaptchaSecurityImages.php?width=100&height=40&characters=5&sid='+Math.random();
			document.login_frm.txtCode.focus();
			return false;	
		}
		else
		{ 
			document.login_frm.rrsub.value = "regSubmit" ;
			document.login_frm.action = "login_mem.php?Mtype="+mtyp1;
			document.login_frm.submit();

		}
	}
}

function handleHttpResponse()
{
	if (http.readyState == 4)
	{
		captchaOK = http.responseText;
		if(captchaOK != 'true')
		{
			alert('Incorrect Security Code. Please try again');
			document.reg_frm.txtCode.value='';
			document.getElementById('image').src = './captcha/CaptchaSecurityImages.php?width=100&height=40&characters=5&sid='+Math.random();
			document.reg_frm.txtCode.focus();
			return false;	
		}
		else
		{ 
			document.reg_frm.rrsub.value = "regSubmit" ;
			document.reg_frm.action = "registration.php" ;
			document.reg_frm.submit();

		}
	}
}
function checkcode5(thecode, submit_type){
	var url="./captcha/secureck.php?security_code="+escape(thecode);
	http.open("POST", url, true);
	http.onreadystatechange = handleHttpResponse5 ;
	http.send(null);
}

function handleHttpResponse5()
{
	var mtyp = document.getElementById('mtype').value;
	var ecd = document.getElementById('exid').value
	if (http.readyState == 4)
	{
		captchaOK = http.responseText;
		if(captchaOK != 'true')
		{
			alert('Incorrect Security Code. Please try again');
			document.reg_frm.txtCode.value='';
			document.getElementById('image').src = './captcha/CaptchaSecurityImages.php?width=100&height=40&characters=5&sid='+Math.random();
			document.reg_frm.txtCode.focus();
			return false;	
		}
		else
		{ 
			document.reg_frm.rrsub.value = "regSubmit" ;
			document.reg_frm.action = "NMregistration.php?Mtype="+mtyp+"&ExId="+ecd;
			document.reg_frm.submit();

		}
	}
}


function checkcode7(thecode, submit_type){
	
	var url="./captcha/secureck.php?security_code="+escape(thecode);
	http.open("POST", url, true);
	http.onreadystatechange = handleHttpResponse7 ;
	http.send(null);
}
function handleHttpResponse7()
{	

	if (http.readyState == 4)
	{
		captchaOK = http.responseText;
	
		if(captchaOK != 'true')
		{
			alert('Incorrect Security Code. Please try again');
			document.off_frm.txtCode.value='';
			document.getElementById('image').src = './captcha/CaptchaSecurityImages.php?width=100&height=40&characters=5&sid='+Math.random();
			document.off_frm.txtCode.focus();
			return false;	
		}
		else
		{ 
			document.off_frm.rrsub.value = "regSubmit" ;
			document.off_frm.action = "offlineApplication.php" ;
			document.off_frm.submit();

		}
	}
}

function checkcode8(thecode, submit_type){
	
	var url="./captcha/secureck.php?security_code="+escape(thecode);
	http.open("POST", url, true);
	http.onreadystatechange = handleHttpResponse8 ;
	http.send(null);
}
function handleHttpResponse8()
{	

	if (http.readyState == 4)
	{
		captchaOK = http.responseText;
	
		if(captchaOK != 'true')
		{
			alert('Incorrect Security Code. Please try again');
			document.off_frm.txtCode.value='';
			document.getElementById('image').src = './captcha/CaptchaSecurityImages.php?width=100&height=40&characters=5&sid='+Math.random();
			document.off_frm.txtCode.focus();
			return false;	
		}
		else
		{ 
			document.off_frm.rrsub.value = "regSubmit" ;
			document.off_frm.action = "PasswordSend.php" ;
			document.off_frm.submit();

		}
	}
}

//validation for photo and signature
function filevalidate(fileobj,imgtype) 
{
	var imgtype_js = imgtype ;

	if(imgtype_js=='photo')
	{
		var max_size = '12288' ;
		var min_size = '8192' ;
	}
	if(imgtype_js=='sign')
	{
		var max_size = '12288' ;
		var min_size = '8192' ;
	}
	if(imgtype_js=='proof')
	{
		var max_size = '12288' ;
		var min_size = '8192' ;
	}

    var node = fileobj ;
    var nodeval=fileobj.value ;

    var extArray = new Array(".jpg",".jpeg") ;

    var allowSubmit = false ;

    while (nodeval.indexOf("\\") != -1)
        nodeval = nodeval.slice(nodeval.indexOf("\\") + 1);

    ext = nodeval.slice(nodeval.indexOf(".")).toLowerCase();
    for (var i = 0; i < extArray.length; i++) {
             if (extArray[i] == ext) { allowSubmit = true; break; }
    }
    if (!allowSubmit){
        alert("Please only upload files that end in types: "+ (extArray.join("  ")) + "\nPlease select a new file to upload and submit again.");
        return false;
    }

    return true;  
}


function chksubmit()
{
	var minjoinyear = parseInt(document.reg_frm.seldobyear.value) + parseInt(18) ;

	var phonechk=0;	

	if(document.reg_frm.txtfname.value =='')
	{
		alert("Please enter your First Name");
		document.reg_frm.txtfname.focus();
		return false;
	}
	else if(document.reg_frm.txtlname.value =='')
	{
		alert("Please enter your Last Name");
		document.reg_frm.txtlname.focus();
		return false;
	}
	else if(document.reg_frm.txtname_card.value =='')
	{
		alert("Please enter the Name as you want to appear on Card");
		document.reg_frm.txtname_card.focus();
		return false;
	}
	else if(document.reg_frm.txtaddress1.value =='')
	{
		alert("Please enter your Address");
		document.reg_frm.txtaddress1.focus();
		return false;
	}
	else if(document.reg_frm.txtdistrict.value =='')
	{
		alert("Please enter your District");
		document.reg_frm.txtdistrict.focus();
		return false;
	}
	else if(document.reg_frm.txtcity.value =='')
	{
		alert("Please enter your City");
		document.reg_frm.txtcity.focus();
		return false;
	}
	else if(document.reg_frm.sel_state.value =='')
	{
		alert("Please select your State");
		document.reg_frm.sel_state.focus();
		return false;
	}
	else if(document.reg_frm.txtpin.value =='')
	{
		alert("Please Enter your Pincode/Zipcode");
		document.reg_frm.txtpin.focus();
		return false;
	}
	else if(!isNumeric('txtpin'))
	{
		alert("Please enter a numeric value in Pincode/Zipcode");
		document.reg_frm.txtpin.focus();
		return false;
	}
	else if(document.reg_frm.txtpin.value !='' && chkpin(document.reg_frm.txtpin.value)== false)
	{
		return false;
	}
	else if(document.reg_frm.seldobday.value =='')
	{
		alert("Please select your Birth Date");
		document.reg_frm.seldobday.focus();
		return false;
	}
	else if(document.reg_frm.seldobmon.value =='')
	{
		alert("Please select your Birth Month");
		document.reg_frm.seldobmon.focus();
		return false;
	}
	else if(document.reg_frm.seldobyear.value =='')
	{
		alert("Please select your Birth Year");
		document.reg_frm.seldobyear.focus();
		return false;
	}
	else if(document.reg_frm.seldobday.value!='' && document.reg_frm.seldobmon.value!= '' && document.reg_frm.seldobyear.value!='' && isDate('seldobday', 'seldobmon', 'seldobyear') == false  )
	{
		return false;				
	}
	else if(document.reg_frm.seldobday.value!='' && document.reg_frm.seldobmon.value!= '' && document.reg_frm.seldobyear.value!='' && chkage('seldobday', 'seldobmon', 'seldobyear') == false  )
	{
		alert("Your Age should be between 18 and 60");
		document.reg_frm.seldobyear.focus();
		return false;			
	}
	else if(get_radio_value(document.reg_frm.optsex) == '')
	{
		alert("Please select your Gender");
		document.getElementById('optsex').focus();
		return false;	
	}
	else if(document.reg_frm.sel_namesub.value =='MR' && get_radio_value(document.reg_frm.optsex) != 'M')
	{
		alert("Please select your Correct Gender");
		document.getElementById('optsex').focus();
		return false;	
	}
	else if( (document.reg_frm.sel_namesub.value =='MRS' || document.reg_frm.sel_namesub.value =='MS') && get_radio_value(document.reg_frm.optsex) != 'F')
	{
		alert("Please select your Correct Gender");
		document.getElementById('optsex').focus();
		return false;	
	}
	else if(get_radio_value(document.reg_frm.optedu) == '')
	{
		alert("Please select your Qualification");
		document.getElementById('optedu').focus();
		return false;	
	}
	else if( (get_radio_value(document.reg_frm.optedu) == 'U') && (document.reg_frm.eduqual1.value =='') )
	{
		alert("Please select your Qualification Stream");
		document.getElementById('eduqual1').focus();
		return false;	
	}
	else if( (get_radio_value(document.reg_frm.optedu) == 'G') && (document.reg_frm.eduqual2.value =='') )
	{
		alert("Please select your Qualification Stream");
		document.getElementById('eduqual2').focus();
		return false;	
	}
	else if( (get_radio_value(document.reg_frm.optedu) == 'P') && (document.reg_frm.eduqual3.value =='') )
	{
		alert("Please select your Qualification Stream");
		document.getElementById('eduqual3').focus();
		return false;	
	}
	else if(document.reg_frm.listbank.value =='')
	{
		alert("Please select Bank/Institution working");
		document.reg_frm.listbank.focus();
		return false;
	}
	else if(document.reg_frm.txtbranch.value =='')
	{
		alert("Please Enter your Branch");
		document.reg_frm.txtbranch.focus();
		return false;
	}
	else if(document.reg_frm.txtdesg.value =='')
	{
		alert("Please select Designation");
		document.reg_frm.txtdesg.focus();
		return false;
	}
	else if(document.reg_frm.seli_dojday.value =='')
	{
		alert("Please select Date of Joining");
		document.reg_frm.seli_dojday.focus();
		return false;
	}
	else if(document.reg_frm.seli_dojmon.value =='')
	{
		alert("Please select Month of Joining");
		document.reg_frm.seli_dojmon.focus();
		return false;
	}
	else if(document.reg_frm.seli_dojyear.value =='')
	{
		alert("Please select Year of Joining");
		document.reg_frm.seli_dojyear.focus();
		return false;
	}
	else if(document.reg_frm.seli_dojyear.value !='' && document.reg_frm.seli_dojyear.value < minjoinyear )
	{
		alert("Please select Proper Year of Joining");
		document.reg_frm.seli_dojyear.focus();
		return false;
	}
	else if(document.reg_frm.seli_dojyear.value!='' && document.reg_frm.seli_dojmon.value!= '' && document.reg_frm.seli_dojyear.value!='' && isDate('seli_dojday','seli_dojmon', 'seli_dojyear') == false  )
	{
		return false;				
	}
	else if(document.reg_frm.seli_dojyear.value!='' && document.reg_frm.seli_dojmon.value!= '' && document.reg_frm.seli_dojyear.value!='' && CompareToday() == false )
	{
		alert('Date of joining should not be greater than today!.');
		document.reg_frm.seli_dojyear.focus();
		return false;
	}
	else if(document.reg_frm.email.value =='')
	{
		alert("Please Enter your Email");
		document.reg_frm.email.focus();
		return false;
	}
	else if(dv('email').value.length>100)
	{
		alert("Email length should not be greater than 100");
		dv('email').focus();
		return false;
	}
	else if(!isemail(dv('email')))
	{
		alert("Invalid Email");
		dv('email').focus();
		return false;
	}
	else if( (document.getElementById('txtphone').value!="" || document.getElementById('txtstd').value!="") && stdphone() == false )
	{
		return false ; 
	}
	else if(document.reg_frm.txtmob.value =='')
	{
		alert("Please Enter your Mobile");
		document.reg_frm.txtmob.focus();
		return false;
	}
	else if(!txtmob() && document.reg_frm.txtmob.value !== '')
		return false;

	else if(document.reg_frm.txtphoto.value=='')
	{
		alert("Please upload Photo");
		document.getElementById('txtphoto').focus();
		return false;				 
	}
	else if(document.reg_frm.txtphoto.value!='' && !filevalidate(document.reg_frm.txtphoto,'photo') )
			return false;
	else if(document.reg_frm.txtsign.value=='')
	{
		alert("Please upload Signature");
		document.getElementById('txtsign').focus();
		return false;				 
	}
	else if(document.reg_frm.txtsign.value!='' && !filevalidate(document.reg_frm.txtsign,'sign') )
		return false;

	else if(get_radio_value(document.reg_frm.proof) == '')
	{
		alert("Please select Id Proof");
		return false;	
	}
	else if(document.reg_frm.txtidno.value =='')
	{
		alert("Please enter your ID Proof No.");
		document.reg_frm.txtidno.focus();
		return false;
	}
	else if(document.reg_frm.txtproof.value=='')
	{
		alert("Please upload Proof");
		document.getElementById('txtproof').focus();
		return false;				 
	}
	else if(document.reg_frm.txtproof.value!='' && !filevalidate(document.reg_frm.txtproof,'proof') )
		return false;
	
	else if(document.reg_frm.declaration1.checked == false)
	{
		alert("Please accept the declarations");
		document.reg_frm.declaration1.focus();
		return false;
	} 
	else if(document.reg_frm.txtCode.value=='')
	{
		alert('Please enter the Security Code');
		document.reg_frm.txtCode.value='';
		document.reg_frm.txtCode.focus();
		return false;
	} // Now the Ajax CAPTCHA validation
	else
	{
		checkcode(document.reg_frm.txtCode.value);
		return false;
	}

}
function NMchksubmit()
{
	var minjoinyear = parseInt(document.reg_frm.seldobyear.value) + parseInt(18) ;

	var phonechk=0;	

	if(document.reg_frm.txtfname.value =='')
	{
		alert("Please enter your First Name");
		document.reg_frm.txtfname.focus();
		return false;
	}
	else if(document.reg_frm.txtlname.value =='')
	{
		alert("Please enter your Last Name");
		document.reg_frm.txtlname.focus();
		return false;
	}
	/*else if(document.reg_frm.txtname_card.value =='')
	{
		alert("Please enter the Name as you want to appear on Card");
		document.reg_frm.txtname_card.focus();
		return false;
	}*/
	else if(document.reg_frm.txtaddress1.value =='')
	{
		alert("Please enter your Address");
		document.reg_frm.txtaddress1.focus();
		return false;
	}
	else if(document.reg_frm.txtdistrict.value =='')
	{
		alert("Please enter your District");
		document.reg_frm.txtdistrict.focus();
		return false;
	}
	else if(document.reg_frm.txtcity.value =='')
	{
		alert("Please enter your City");
		document.reg_frm.txtcity.focus();
		return false;
	}
	else if(document.reg_frm.sel_state.value =='')
	{
		alert("Please select your State");
		document.reg_frm.sel_state.focus();
		return false;
	}
	else if(document.reg_frm.txtpin.value =='')
	{
		alert("Please Enter your Pincode/Zipcode");
		document.reg_frm.txtpin.focus();
		return false;
	}
	else if(!isNumeric('txtpin'))
	{
		alert("Please enter a numeric value in Pincode/Zipcode");
		document.reg_frm.txtpin.focus();
		return false;
	}
	else if(document.reg_frm.txtpin.value !='' && chkpin(document.reg_frm.txtpin.value)== false)
	{
		return false;
	}
	else if(document.reg_frm.seldobday.value =='')
	{
		alert("Please select your Birth Date");
		document.reg_frm.seldobday.focus();
		return false;
	}
	else if(document.reg_frm.seldobmon.value =='')
	{
		alert("Please select your Birth Month");
		document.reg_frm.seldobmon.focus();
		return false;
	}
	else if(document.reg_frm.seldobyear.value =='')
	{
		alert("Please select your Birth Year");
		document.reg_frm.seldobyear.focus();
		return false;
	}
	else if(document.reg_frm.seldobday.value!='' && document.reg_frm.seldobmon.value!= '' && document.reg_frm.seldobyear.value!='' && isDate('seldobday', 'seldobmon', 'seldobyear') == false  )
	{
		return false;				
	}
	/*else if(document.reg_frm.seldobday.value!='' && document.reg_frm.seldobmon.value!= '' && document.reg_frm.seldobyear.value!='' && chkage('seldobday', 'seldobmon', 'seldobyear') == false  )
	{
		alert("Your Age should be between 18 and 40");
		document.reg_frm.seldobyear.focus();
		return false;			
	}*/
	else if(get_radio_value(document.reg_frm.optsex) == '')
	{
		alert("Please select your Gender");
		document.getElementById('optsex').focus();
		return false;	
	}
	else if(document.reg_frm.sel_namesub.value =='MR' && get_radio_value(document.reg_frm.optsex) != 'M')
	{
		alert("Please select your Correct Gender");
		document.getElementById('optsex').focus();
		return false;	
	}
	else if( (document.reg_frm.sel_namesub.value =='MRS' || document.reg_frm.sel_namesub.value =='MS') && get_radio_value(document.reg_frm.optsex) != 'F')
	{
		alert("Please select your Correct Gender");
		document.getElementById('optsex').focus();
		return false;	
	}
	else if(get_radio_value(document.reg_frm.optedu) == '')
	{
		alert("Please select your Qualification");
		document.getElementById('optedu').focus();
		return false;	
	}
	else if( (get_radio_value(document.reg_frm.optedu) == 'U') && (document.reg_frm.eduqual1.value =='') )
	{
		alert("Please select your Qualification Stream");
		document.getElementById('eduqual1').focus();
		return false;	
	}
	else if( (get_radio_value(document.reg_frm.optedu) == 'G') && (document.reg_frm.eduqual2.value =='') )
	{
		alert("Please select your Qualification Stream");
		document.getElementById('eduqual2').focus();
		return false;	
	}
	else if( (get_radio_value(document.reg_frm.optedu) == 'P') && (document.reg_frm.eduqual3.value =='') )
	{
		alert("Please select your Qualification Stream");
		document.getElementById('eduqual3').focus();
		return false;	
	}
	/*else if(document.reg_frm.listbank.value =='')
	{
		alert("Please select Bank/Institution working");
		document.reg_frm.listbank.focus();
		return false;
	}
	else if(document.reg_frm.txtbranch.value =='')
	{
		alert("Please Enter your Branch");
		document.reg_frm.txtbranch.focus();
		return false;
	}
	else if(document.reg_frm.txtdesg.value =='')
	{
		alert("Please select Designation");
		document.reg_frm.txtdesg.focus();
		return false;
	}
	else if(document.reg_frm.seli_dojday.value =='')
	{
		alert("Please select Date of Joining");
		document.reg_frm.seli_dojday.focus();
		return false;
	}
	else if(document.reg_frm.seli_dojmon.value =='')
	{
		alert("Please select Month of Joining");
		document.reg_frm.seli_dojmon.focus();
		return false;
	}
	else if(document.reg_frm.seli_dojyear.value =='')
	{
		alert("Please select Year of Joining");
		document.reg_frm.seli_dojyear.focus();
		return false;
	}
	else if(document.reg_frm.seli_dojyear.value !='' && document.reg_frm.seli_dojyear.value < minjoinyear )
	{
		alert("Please select Proper Year of Joining");
		document.reg_frm.seli_dojyear.focus();
		return false;
	}
	else if(document.reg_frm.seli_dojyear.value!='' && document.reg_frm.seli_dojmon.value!= '' && document.reg_frm.seli_dojyear.value!='' && isDate('seli_dojday','seli_dojmon', 'seli_dojyear') == false  )
	{
		return false;				
	}
	else if(document.reg_frm.seli_dojyear.value!='' && document.reg_frm.seli_dojmon.value!= '' && document.reg_frm.seli_dojyear.value!='' && CompareToday() == false )
	{
		alert('Date of joining should not be greater than today!.');
		document.reg_frm.seli_dojyear.focus();
		return false;
	}*/
	else if(document.reg_frm.email.value =='')
	{
		alert("Please Enter your Email");
		document.reg_frm.email.focus();
		return false;
	}
	else if(dv('email').value.length>100)
	{
		alert("Email length should not be greater than 100");
		dv('email').focus();
		return false;
	}
	else if(!isemail(dv('email')))
	{
		alert("Invalid Email");
		dv('email').focus();
		return false;
	}
	else if( (document.getElementById('txtphone').value!="" || document.getElementById('txtstd').value!="") && stdphone() == false )
	{
		return false ; 
	}
	else if(document.reg_frm.txtmob.value =='')
	{
		alert("Please Enter your Mobile");
		document.reg_frm.txtmob.focus();
		return false;
	}
	else if(!txtmob() && document.reg_frm.txtmob.value !== '')
		return false;

	else if(document.reg_frm.txtphoto.value=='')
	{
		alert("Please upload Photo");
		document.getElementById('txtphoto').focus();
		return false;				 
	}
	else if(document.reg_frm.txtphoto.value!='' && !filevalidate(document.reg_frm.txtphoto,'photo') )
			return false;
	else if(document.reg_frm.txtsign.value=='')
	{
		alert("Please upload Signature");
		document.getElementById('txtsign').focus();
		return false;				 
	}
	else if(document.reg_frm.txtsign.value!='' && !filevalidate(document.reg_frm.txtsign,'sign') )
		return false;

	else if(get_radio_value(document.reg_frm.proof) == '')
	{
		alert("Please select Id Proof");
		return false;	
	}
	else if(document.reg_frm.txtidno.value =='')
	{
		alert("Please enter your ID Proof No.");
		document.reg_frm.txtidno.focus();
		return false;
	}
	else if(document.reg_frm.txtproof.value=='')
	{
		alert("Please upload Proof");
		document.getElementById('txtproof').focus();
		return false;				 
	}
	else if(document.reg_frm.txtproof.value!='' && !filevalidate(document.reg_frm.txtproof,'proof') ) {
		return false;
	
	}
	checkTheBox();
	if(trim(document.getElementById('selCenterName').value) == ''){

			alert("Please select Exam Center Name.");
			document.getElementById('selCenterName').focus();
			return false; 

	}
		var temp = document.getElementById("selCenterName").selectedIndex;	if(trim1(document.getElementById("selCenterName").options[temp].className)=='ON') {
		if(document.getElementById('optsex1').checked ==false && document.getElementById('optsex2').checked ==false){

			alert("Please select Exam Mode");
			document.getElementById('optsex1').focus();
			return false; 

	}
	}else {
		if(document.getElementById('optsex2').checked ==false){

			alert("Please select Exam mode");
			document.getElementById('optsex2').focus();
			return false; 

	}
	}
	if(document.reg_frm.hdnExamCode.value =='42')
	{   
		if(document.reg_frm.txtname_card.value =='')
		{
			alert("Please enter the Name as you want to appear on Card");
			document.reg_frm.txtname_card.focus();
			return false;
		}
	}
	if(document.reg_frm.declaration1.checked == false)
	{
		alert("Please accept the declarations");
		document.reg_frm.declaration1.focus();
		return false;
	} 
	else if(document.reg_frm.txtCode.value=='')
	{
		alert('Please enter the Security Code');
		document.reg_frm.txtCode.value='';
		document.reg_frm.txtCode.focus();
		return false;
	} // Now the Ajax CAPTCHA validation
	else if(document.reg_frm.txtmob.value!='' && document.reg_frm.email.value!='' && getmob()== false)
	{
		return false;
	}
	else
	{
		checkcode5(document.reg_frm.txtCode.value);
		return false;
	}

}


function checkTheBox() {
var flag = 0;
     var arrVals = document.getElementsByTagName("input");
     for (x=0; x<arrVals.length; x++) {
         if (arrVals[x].name == 'optmedium') {
           if (arrVals[x].checked==true){
          flag ++;
          }        
         }
      } 
	  if (flag == 0) {
alert ("Please Select Medium");
document.getElementById('optmedium1').focus();
return false;
}
return true;  
}
function trim1(value) {
  value = value.replace(/^\s+/,'');
  value = value.replace(/\s+$/,'');
  return value;
}
function changedu(dval)
{
	if(dval == 'U')
	{
		document.getElementById('UG').style.display = "block";
		document.getElementById('GR').style.display = "none";
		document.getElementById('PG').style.display = "none";	
		document.getElementById('edu').style.display = "none";	
	}
	else if(dval == 'G')
	{
		document.getElementById('UG').style.display = "none";
		document.getElementById('GR').style.display = "block";
		document.getElementById('PG').style.display = "none";
		document.getElementById('edu').style.display = "none";		
	}
	else if(dval == 'P')
	{
		document.getElementById('UG').style.display = "none";
		document.getElementById('GR').style.display = "none";
		document.getElementById('PG').style.display = "block";	
		document.getElementById('edu').style.display = "none";	
	}
}

function changedu_pre(dval)
{
	if(dval == 'U')
	{
		document.getElementById('UG').style.display = "block";
		document.getElementById('GR').style.display = "none";
		document.getElementById('PG').style.display = "none";	
	}
	else if(dval == 'G')
	{
		document.getElementById('UG').style.display = "none";
		document.getElementById('GR').style.display = "block";
		document.getElementById('PG').style.display = "none";
	}
	else if(dval == 'P')
	{
		document.getElementById('UG').style.display = "none";
		document.getElementById('GR').style.display = "none";
		document.getElementById('PG').style.display = "block";	
	}
}


function chkpreview()
{
	var minjoinyear = parseInt(document.reg_frm.seldobyear.value) + parseInt(18) ;

	if(document.reg_frm.txtfname.value =='')
	{
		alert("Please enter your First Name");
		document.reg_frm.txtfname.focus();
		return false;
	}
	else if(document.reg_frm.txtlname.value =='')
	{
		alert("Please enter your Last Name");
		document.reg_frm.txtlname.focus();
		return false;
	}
	else if(document.reg_frm.txtname_card.value =='')
	{
		alert("Please enter the Name as you want to appear on Card");
		document.reg_frm.txtname_card.focus();
		return false;
	}
	else if(document.reg_frm.txtaddress1.value =='')
	{
		alert("Please enter your Address");
		document.reg_frm.txtaddress1.focus();
		return false;
	}
	else if(document.reg_frm.txtdistrict.value =='')
	{
		alert("Please enter your District");
		document.reg_frm.txtdistrict.focus();
		return false;
	}
	else if(document.reg_frm.txtcity.value =='')
	{
		alert("Please enter your City");
		document.reg_frm.txtcity.focus();
		return false;
	}
	else if(document.reg_frm.sel_state.value =='')
	{
		alert("Please select your State");
		document.reg_frm.sel_state.focus();
		return false;
	}
	else if(document.reg_frm.txtpin.value =='')
	{
		alert("Please Enter your Pincode/Zipcode");
		document.reg_frm.txtpin.focus();
		return false;
	}
	else if(!isNumeric('txtpin'))
	{
		alert("Please enter a numeric value in Pincode/Zipcode");
		document.reg_frm.txtpin.focus();
		return false;
	}
	else if(document.reg_frm.txtpin.value !='' && chkpin(document.reg_frm.txtpin.value)== false)
	{
		return false;
	}
	else if(document.reg_frm.seldobday.value =='')
	{
		alert("Please select your Birth Date");
		document.reg_frm.seldobday.focus();
		return false;
	}
	else if(document.reg_frm.seldobmon.value =='')
	{
		alert("Please select your Birth Month");
		document.reg_frm.seldobmon.focus();
		return false;
	}
	else if(document.reg_frm.seldobyear.value =='')
	{
		alert("Please select your Birth Year");
		document.reg_frm.seldobyear.focus();
		return false;
	}
	else if(document.reg_frm.seldobday.value!='' && document.reg_frm.seldobmon.value!= '' && document.reg_frm.seldobyear.value!='' && isDate('seldobday', 'seldobmon', 'seldobyear') == false  )
	{
		return false;				
	}
	else if(document.reg_frm.seldobday.value!='' && document.reg_frm.seldobmon.value!= '' && document.reg_frm.seldobyear.value!='' && chkage('seldobday', 'seldobmon', 'seldobyear') == false  )
	{
		alert("Your Age should be between 18 and 60");
		document.reg_frm.seldobyear.focus();
		return false;			
	}
	else if(get_radio_value(document.reg_frm.optsex) == '')
	{
		alert("Please select your Gender");
		document.getElementById('optsex').focus();
		return false;	
	}
	else if(document.reg_frm.sel_namesub.value =='MR' && get_radio_value(document.reg_frm.optsex) != 'M')
	{
		alert("Please select your Correct Gender");
		document.getElementById('optsex').focus();
		return false;	
	}
	else if( (document.reg_frm.sel_namesub.value =='MRS' || document.reg_frm.sel_namesub.value =='MS') && get_radio_value(document.reg_frm.optsex) != 'F')
	{
		alert("Please select your Correct Gender");
		document.getElementById('optsex').focus();
		return false;	
	}
	else if(get_radio_value(document.reg_frm.optedu) == '')
	{
		alert("Please select your Qualification");
		document.getElementById('optedu').focus();
		return false;	
	}
	else if( (get_radio_value(document.reg_frm.optedu) == 'U') && (document.reg_frm.eduqual1.value =='') )
	{
		alert("Please select your Qualification Stream");
		document.getElementById('eduqual1').focus();
		return false;	
	}
	else if( (get_radio_value(document.reg_frm.optedu) == 'G') && (document.reg_frm.eduqual2.value =='') )
	{
		alert("Please select your Qualification Stream");
		document.getElementById('eduqual2').focus();
		return false;	
	}
	else if( (get_radio_value(document.reg_frm.optedu) == 'P') && (document.reg_frm.eduqual3.value =='') )
	{
		alert("Please select your Qualification Stream");
		document.getElementById('eduqual3').focus();
		return false;	
	}

	else if(document.reg_frm.listbank.value =='')
	{
		alert("Please select Bank/Institution working");
		document.reg_frm.listbank.focus();
		return false;
	}
	else if(document.reg_frm.txtbranch.value =='')
	{
		alert("Please Enter your Branch");
		document.reg_frm.txtbranch.focus();
		return false;
	}
	else if(document.reg_frm.txtdesg.value =='')
	{
		alert("Please select Designation");
		document.reg_frm.txtdesg.focus();
		return false;
	}
	else if(document.reg_frm.seli_dojday.value =='')
	{
		alert("Please select Date of Joining");
		document.reg_frm.seli_dojday.focus();
		return false;
	}
	else if(document.reg_frm.seli_dojmon.value =='')
	{
		alert("Please select Month of Joining");
		document.reg_frm.seli_dojmon.focus();
		return false;
	}
	else if(document.reg_frm.seli_dojyear.value =='')
	{
		alert("Please select Year of Joining");
		document.reg_frm.seli_dojyear.focus();
		return false;
	}
	else if(document.reg_frm.seli_dojyear.value !='' && document.reg_frm.seli_dojyear.value < minjoinyear )
	{
		alert("Please select Proper Year of Joining");
		document.reg_frm.seli_dojyear.focus();
		return false;
	}
	else if(document.reg_frm.seli_dojyear.value!='' && document.reg_frm.seli_dojmon.value!= '' && document.reg_frm.seli_dojyear.value!='' && isDate('seli_dojday','seli_dojmon', 'seli_dojyear') == false  )
	{
		return false;				
	}
	else if(document.reg_frm.seli_dojyear.value!='' && document.reg_frm.seli_dojmon.value!= '' && document.reg_frm.seli_dojyear.value!='' && CompareToday() == false )
	{
		alert('Date of joining should not be greater than today!.');
		document.reg_frm.seli_dojyear.focus();
		return false;
	}
	else if(document.reg_frm.email.value =='')
	{
		alert("Please Enter your Email");
		document.reg_frm.email.focus();
		return false;
	}
	else if(dv('email').value.length>100)
	{
		alert("Email length should not be greater than 100");
		dv('email').focus();
		return false;
	}
	else if(!isemail(dv('email')))
	{
		alert("Invalid Email");
		dv('email').focus();
		return false;
	}
	else if( (document.getElementById('txtphone').value!="" || document.getElementById('txtstd').value!="") && stdphone() == false )
	{
		return false ; 
	}
	else if(document.reg_frm.txtmob.value =='')
	{
		alert("Please Enter your Mobile");
		document.reg_frm.txtmob.focus();
		return false;
	}
	else if(!txtmob() && document.reg_frm.txtmob.value !== '')
		return false;

	else if(get_radio_value(document.reg_frm.proof) == '')
	{
		alert("Please select Id Proof");
		document.getElementById('proof').focus();
		return false;	
	}
	else if(document.reg_frm.txtidno.value =='')
	{
		alert("Please enter your ID Proof No.");
		document.reg_frm.txtidno.focus();
		return false;
	}
	else
	{
		document.reg_frm.rrsub.value = "regSubmit" ;
		document.reg_frm.action = "registration_preview.php" ;
		document.reg_frm.submit();
	}

}

function getpin(sval){
	var url="functions.php?state_code="+escape(sval)+"&flag=1" ;
	http.open("POST", url, true);
	http.onreadystatechange = handleHttpResponse1 ;
	http.send(null);
}

function handleHttpResponse1()
{
	if (http.readyState == 4)
	{
		var pinval = http.responseText;
		if(pinval == '^')
		{
			alert('Incorrect State Value. Please Select Correct State');
			document.reg_frm.sel_state.value='';
			document.reg_frm.sel_state.focus();
			return false;	
		}
		else
		{
			var pin = pinval.split('^') ;
			document.reg_frm.minpin.value = pin[0];
			document.reg_frm.maxpin.value = pin[1];
		}
	}
}

function chkpin(txtpin)
{
	var minpin = document.reg_frm.minpin.value ;
	var maxpin = document.reg_frm.maxpin.value ;

	if(txtpin.length != 6 )
	{
		alert("Please enter Valid Pincode");
		document.reg_frm.txtpin.value='';
		document.reg_frm.txtpin.focus();
		return false;		
	}
	else if(txtpin < minpin || txtpin > maxpin)
	{
		alert("Please enter Valid Pincode");
		document.reg_frm.txtpin.value='';
		document.reg_frm.txtpin.focus();	
		return false;			
	}
}

function alphanumber(e)
{	
	var key;
	var keychar;
	if (window.event){
		key = window.event.keyCode;		
	}else if (e){
		key = e.which;		
	}
	else
		return true;
	//alert(key);
	if((key == 8) || (key == 0))
		return true;
	
	
	keychar = String.fromCharCode(key);
	keychar = keychar.toLowerCase();
	//alert(keychar);
	
	if(key!=32)
	{ 
		
		invalids = "`~@#$%^*-()_+=\|{}[].:;'\"<>&?/!,\\";
		  for(i=0; i<invalids.length; i++) {
			if(keychar != 0)
			{
				if(keychar.indexOf(invalids.charAt(i)) >= 0 || keychar==false) {				           			  
					return false;               
				}
			}
		  }
	}
	return true;		
}

/*mobilenumber checking */
function getmob()
{

	/*Change by anuja- added mtype*/
	var emailid=document.reg_frm.email.value;
	var val=document.reg_frm.txtmob.value;
	var mtype=document.reg_frm.nmmemtype.value;
	var url="functions_exam.php?mob="+escape(val)+"&email="+emailid+"&mtype="+mtype;
	http.open("POST", url, true);
	http.onreadystatechange = handleHttpResponsemo ;
	http.send(null);
}

function handleHttpResponsemo()
{
	if (http.readyState == 4)
	{
		var mobval = http.responseText;
		if(mobval == 0) 
		{
				return true;
		}
		else
		{
			var memval = mobval.split(',') ;
			var memno=memval[2];
			var name=memval[4];
            var seqid=memval[3];
		    if(seqid=='')
			{
               var regno=memno;
			}
			else
			{
               var regno=seqid;
			}
			alert("The entered email ID and mobile no already exist for membership / registration number "+regno+ "  "+ " , " +name);
			document.reg_frm.txtmob.value='';
			document.reg_frm.email.value='';
			document.reg_frm.email.focus();
			return false;
			
		}
		
	}
}

/*mobilenumber checking */

function alphanumberctrl(e)
{	
	var key;
	var keychar;
	var isCtrl;
	var forbiddenKeys = new Array('a', 'n', 'c', 'x', 'v', 'j');
	if (window.event){
		key = window.event.keyCode;	
		if(window.event.ctrlKey)
			isCtrl = true;
        else
            isCtrl = false;
	}else if (e){
		key = e.which;
		if(e.ctrlKey)
			isCtrl = true;
        else
            isCtrl = false;
	}
	else
		return true;
	//alert(key);
	if(isCtrl)
	{
		for(i=0; i<forbiddenKeys .length; i++)
		{
				//case-insensitive comparation				
			if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase())
			{
					alert('Key combination CTRL + '
							+String.fromCharCode(key)
							+' has been disabled.');
					return false;
			}
		}
	}

	if((key == 8) || (key == 0))
		return true;
	/*
	keychar = String.fromCharCode(key);
	keychar = keychar.toLowerCase();
	//alert(keychar);
	
	if(key!=32)
	{ 
		
		invalids = "`~@#$%^*-()_+=\|{}[].:;'\"<>&?/!,\\";
		  for(i=0; i<invalids.length; i++) {
			if(keychar != 0)
			{
				if(keychar.indexOf(invalids.charAt(i)) >= 0 || keychar==false) {				           			  
				//alert(keychar.indexOf(invalids.charAt(i)));
					return false;               
				}
			}
		  }
	}
	return true;		
	*/
}

function isDate(a1,b1,c1){
	
	var a = document.getElementById(a1).value ; 
	var b = document.getElementById(b1).value ; 
	var c = document.getElementById(c1).value ; 

	var dtStr = b+'/'+a+'/'+c ;

	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strMonth=dtStr.substring(0,pos1)
	var strDay=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)	

	if (pos1==-1 || pos2==-1){
		alert("The date format should be : dd/mm/yyyy");
	        document.getElementById(a1).focus();
		return false;
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("Please select a valid month");
	        document.getElementById(b1).focus();
		return false;
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please select a valid day");
	        document.getElementById(a1).focus();
		return false;
	}
	if (strYear.length != 4 || year==0 ){
		alert("Please select a valid 4 digit year between "+minYear+" and "+maxYear);
	        document.getElementById(c1).focus();
		return false;
	}	
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please select a valid date for the selected month");
	        document.getElementById(b1).focus();
		return false;
	}
	return true;
}

function daysInFebruary (year){
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}

function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function imgvalidation(imagename)
{

	if(document.getElementById(imagename).value!="")	{

		var splt=(document.getElementById(imagename).value).split('.');
		var chksplt=splt[1].toLowerCase();

		var imgbytes = document.getElementById(imagename).fileSize;
		var imgkbytes = Math.round(parseInt(imgbytes)/1024);	
				
		var width = document.getElementById(imagename).width;
		var height = document.getElementById(imagename).height;
		var type = document.getElementById(imagename).type;

		if(chksplt !='jpg' && chksplt !='jpeg' )
		{
			alert("select only jpg, jpeg files");
			return false;
		}
		else if( imgkbytes < 8 || imgkbytes > 12)
		{
			alert("Image size must between 8KB and 12 KB");
			return false;
		}

	}

}

function getImgSize(imgSrc) {
    var newImg = new Image();

    newImg.onload = function() {
    var height = newImg.height;
    var width = newImg.width;
    alert ('The image size is '+width+'*'+height);
    }

    newImg.src = imgSrc; // this must be done AFTER setting onload

}


function stdphone()
{

	if(document.getElementById('txtphone').value!="")
	{
		if(document.getElementById('txtstd').value=="")
		{
			alert("Please enter STD Code");
			document.getElementById('txtstd').focus();
			return false;
		}			
		if(document.getElementById('txtstd').value==0)
		{
			alert("STD Code should not be zero(s)");
			document.getElementById('txtstd').focus();
			return false;
		}
		if(trim(dv('txtphone').value).substr(0,1)==0)
		{
			alert("Phone no should not start with 0.");
			dv('txtphone').focus();
			return false;
		}
		phonechk=1;
	}
	if( (trim(document.getElementById('txtstd').value) !='') )
	{	
		if(!isNumeric('txtstd'))
		{
			alert("STD Code should be digits");
			document.getElementById('txtstd').focus();
			return false;
		}		
		
		if((document.getElementById('txtstd').value).length < 3)
		{
			alert("STD code should be minimum 3 digits");
			document.getElementById('txtstd').focus();
			return false;
		}	
						
		if(document.getElementById('txtphone').value=="")
		{
			alert("Please enter Phone No.");
			document.getElementById('txtphone').focus();
			return false;
		}		
		if(document.getElementById('txtphone').value!="")
		{
			if(!isNumeric('txtphone'))
			{
				alert("Phone No should be digits");
				document.getElementById('txtphone').focus();
				return false;
			}
			if(document.getElementById('txtphone').value==0)
			{
				alert("Phone No should not be zero(s)");
				document.getElementById('txtphone').focus();
				return false;
			}
			if(trim(dv('txtstd').value).substr(0,1)!=0)
			{
				alert("STD Code should  start with 0.");
				dv('txtstd').focus();
				return false;
			}
		}				
	}

	// checking the length of both phone and std
	var firstval_txtphone=parseInt(document.getElementById('txtphone').value.length);
	var secondval_txtstd=parseInt(document.getElementById('txtstd').value.length)
	var stdphone_max_length=firstval_txtphone + secondval_txtstd;

	if(stdphone_max_length != 11)
	{
		alert("Combination of STD and Phone should be 11 digits");
		document.getElementById('txtstd').focus();
		return false;
	}

return true ;

}

function CompareToday()
{

	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!

	var yyyy = today.getFullYear();

	if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm} 
		var today = new Date(yyyy, mm, dd); 

	var jday  = document.reg_frm.seli_dojday.value ;

	var jmnth = document.reg_frm.seli_dojmon.value ;

	var jyear = document.reg_frm.seli_dojyear.value ;

	var jdate = new Date(jyear, jmnth, jday);

	if( jdate > today )
		return false;
} 


function chkage()
{

var dday  = document.reg_frm.seldobday.value ;

var dmnth = document.reg_frm.seldobmon.value ;

var dyear = document.reg_frm.seldobyear.value ;

var dob_date = new Date(dyear, dmnth-1 , dday);


var min_year = document.reg_frm.minyear.value ;
var max_year = document.reg_frm.maxyear.value ;


var date_max = new Date(max_year, '05', '01');

var date_min = new Date(min_year, '05', '01');

if( date_max > dob_date || date_min < dob_date )
{
	return false;
}

}
//Added by Arthi for Candidate change Bank Name Popup display
function noticepopup()
{
  var new_bankname=document.reg_frm.listbank.value ;
  var old_bankname=document.reg_frm.listbank_hid.value ;
  if(old_bankname!=new_bankname)
  {
	  	alert("From June 2012, the candidates belonging to non-banking organisations were treated as not eligible to apply for JAIIB/CAIIB examinations. It is quite possible that some of these candidates might have joined bank/financial institution. In view of this they might have changed the name of their employer by editing the profile on IIBF website. Such candidates should submit an employment certificate to respective zonal office of the Institute for necessary verification. In future, if any candidate joins the bank/financial institution, he/she should submit the employment certificate before making any change in the employer's name. Kindly note that, the system will allow them to make such change in their profile, however, will be permitted to apply for examination online only after submission of the required documents to the satisfaction of the Institute."); 
  }
	
}
//Close
