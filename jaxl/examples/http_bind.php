<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

if(!isset($_GET['jid']) || !isset($_GET['pass'])) {
	echo "invalid input";
	exit;
}

require_once '../jaxl.php';
$client = new JAXL(array(
	'jid' => $_GET['jid'],
	'pass' => $_GET['pass'],
	'bosh_url' => 'http://localhost:7070/http-bind',
	'log_level' => JAXL_DEBUG
));

print_r($client); exit;

$client->add_cb('on_auth_success', function() {
	global $client;
	_info("got on_auth_success cb, jid ".$client->full_jid->to_string());
});

$client->start();
echo "done\n";

?>
