<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

function substr_word($body,$maxlength){
    if (strlen($body)<$maxlength) return $body;
    $body = substr($body, 0, $maxlength);
    $rpos = strrpos($body,' ');
    if ($rpos>0) $body = substr($body, 0, $rpos);
    return $body;
}

$name = "I was born in 1948, so Im a 60s kid, and in the 60s everyone talked all the time, endlessly, about socialism versus capitalism, about political choices, ideology, Marxism, revolution, the system and so on.";


$newtext = wordwrap($name, 75, "@--@");

$image_path = "bg.png";
$img = imagecreatefrompng($image_path); //$image_path -> on which text to be drawn
       imagealphablending($img, true);
       imagesavealpha($img, true);
$textColor = imagecolorallocate($img, 255, 255, 255);
$line_colour = imagecolorallocate( $img, 128, 255, 0 );

$font = '/var/www/phppics/angrybirds-regular.ttf';

$data_text = explode("@--@", $newtext);

$yAxis = 85;
foreach ($data_text as $value) {

imagettftext($img, 20, 0, 40, $yAxis,$textColor,$font, $value);

$yAxis = $yAxis + 30;

}
$author = " - Tony Judt";



imagettftext($img, 20, 0, 350, $yAxis+45,$textColor,$font, $author);

imagettftext($img, 20, 0, 210, 500,$textColor,$font, "More Quotes Visit our website www.kquotes.com");

imageline( $img, 210, 505, 735, 505, $line_colour );

$p_path = "img/te3st.jpg";
$path = "/var/www/phppics/".$p_path;

$image = imagejpeg($img, $path);
imagedestroy($img);
?>
<img src="<?php echo $p_path; ?>" width="615" height="359">