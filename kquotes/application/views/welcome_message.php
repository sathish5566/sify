<!--<div class="letter-navbar" style="text-align: center; letter-spacing: 2px">
<span class="body bq-tn-letters"> <span id="bq-auth-lbl" class="bq-tn-wrap">Authors:</span>
<span class="bq-tn-wrap"> <a href="/quotes/a">A</a>
<a href="/quotes/b">B</a> <a href="/quotes/c">C</a> <a href="/quotes/d">D</a>
<a href="/quotes/e">E</a> <a href="/quotes/f">F</a> <a href="/quotes/g">G</a>
<a href="/quotes/h">H</a> <a href="/quotes/i">I</a> <a href="/quotes/j">J</a>
<a href="/quotes/k">K</a> <a href="/quotes/l">L</a> <a href="/quotes/m">M</a>
</span> <span class="bq-tn-wrap"> <a href="/quotes/n">N</a>
<a href="/quotes/o">O</a> <a href="/quotes/p">P</a> <a href="/quotes/q">Q</a>
<a href="/quotes/r">R</a> <a href="/quotes/s">S</a> <a href="/quotes/t">T</a>
<a href="/quotes/u">U</a> <a href="/quotes/v">V</a> <a href="/quotes/w">W</a>
<a href="/quotes/x">X</a> <a href="/quotes/y">Y</a> <a href="/quotes/z">Z</a>
</span>
</span>
</div>
</div>-->
<div>
<div class="container">
<div class="row">
<div class="span12">
	<div class="bq_s">
	<h2 style="margin-top:20px;">Quote of the Moment
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <div class="fb-like" data-href="http://kquotes.com/" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div>
        </h2>
	<div class="masonryitem boxy bqQt bqShare">
	<div class="boxyPaddingBig">
	<h1 class="bqQuoteLink" style="border-bottom: 0px;">
	<a title="view quote" href="<?php echo HOME_URL."q/".$url_title = substr(url_title(strtolower($quote_at_the_moment['quote'])),0,100); ?>-<?php echo $quote_at_the_moment['unique_id']; ?>" target="_blank">
	<?php 
	echo $quote_at_the_moment['quote'];
	?>
	<?php $topic = explode("@--@", $quote_at_the_moment['topics']); ?>
	</a></h1>
	<div class="bq-aut">
	<a title="view quote" href="<?php echo HOME_URL; ?>a/<?php echo url_title($quote_at_the_moment['author']); ?>"><?php echo $quote_at_the_moment['author']; ?></a></div>
	</div>
	<div class="bq_q_nav boxyBottom boxyPaddingSmall" style="overflow:hidden;">
	<div class="bq20" style="float:right;">
	</div>
	<div class="body bq_boxyRelatedLeft bqBlackLink ">
	<?php
	foreach ($topic as $val) {
	?>
	<a title="view keyword" href="<?php echo HOME_URL; ?>t/<?php echo $val; ?>"><?php echo $val; ?></a> |
	<?php } ?>
	</div>
	</div>
	</div>

</div>
</div>
</div>

<div class="container">
<div class="row">
<div class="span8 sticky_adzone">
<div class="row">
<div class="span4">
<div class="bq_s">
<h2>Popular Topics</h2>
Browse quotations by topic:<br>
<table id="allTopics" class="mHide" style="width:100%; border: 0px;">
<tr>
<td valign="top">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/inspirational">Inspirational</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/motivational">Motivational</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/love">Love</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/funny">Funny</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/positive">Positive</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/life">Life</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/happiness">Happiness</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/success">Success</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/friendship">Friendship</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/wisdom">Wisdom</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/smile">Smile</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/dreams">Dreams</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/change">Change</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/leadership">Leadership</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/education">Education</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/family">Family</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/birthday">Birthday</a></div>
</td>
</tr>
</table>
</div>
</div>
<div class="span4">
<div class="bq_s">
<h2>Popular Authors</h2>
Browse by author:<br>
<table style="width:100%; border: 0px;">
<tr>
<td>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/walt-disney">Walt Disney</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/albert-einstein">Albert Einstein</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/dr-seuss">Dr. Seuss</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/jesus-christ">Jesus Christ</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/eminem">Eminem</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/marilyn-monroe">Marilyn Monroe</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/william-shakespeare">William Shakespeare</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/bill-gates">Bill Gates</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/bob-marley">Bob Marley</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/leonardo-da-vinci">Leonardo da Vinci</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/johnny-depp">Johnny Depp</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/nelson-mandela">Nelson Mandela</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/buddha">Buddha</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/mahatma-gandhi">Mahatma Gandhi</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/aristotle">Aristotle</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/steve-jobs">Steve Jobs</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/abraham-lincoln">Abraham Lincoln</a></div>
</td>
</tr>
</table>
</div>
</div>
</div>

<div class="bq_s">
<div class="row">
<div class="span8">
<h2>Authors to Explore</h2>
</div>
</div>
<div class="row">
<div class="span4">
<div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/a-p-j_abdul_kalam">A. P. J. Abdul Kalam</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/albert-camus">Albert Camus</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/andrew-jackson">Andrew Jackson</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/audrey-hepburn">Audrey Hepburn</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/barack-obama">Barack Obama</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/benjamin-franklin">Benjamin Franklin</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/bruce-lee">Bruce Lee</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/c-s-lewis">C. S. Lewis</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/carl-sagan">Carl Sagan</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/coco-chanel">Coco Chanel</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/confucius">Confucius</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/dalai-lama">Dalai Lama</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/edgar-allan_poe">Edgar Allan Poe</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/eleanor-roosevelt">Eleanor Roosevelt</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/ernest-hemingway">Ernest Hemingway</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/friedrich-nietzsche">Friedrich Nietzsche</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/george-bernard-shaw">George Bernard Shaw</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/george-carlin">George Carlin</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/george-s-patton">George S. Patton</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/george-washington">George Washington</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/groucho-marx">Groucho Marx</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/helen-keller">Helen Keller</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/henry-david-thoreau">Henry David Thoreau</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/henry-ford">Henry Ford</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/jeanpaul-sartre">Jean-Paul Sartre</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/jim-morrison">Jim Morrison</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/jim-rohn">Jim Rohn</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/jimi-hendrix">Jimi Hendrix</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/john-f-kennedy">John F. Kennedy</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/john-lennon">John Lennon</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/john-wooden">John Wooden</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/joseph-joubert">Joseph Joubert</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/joseph-stalin">Joseph Stalin</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/julius-caesar">Julius Caesar</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/karl-marx">Karl Marx</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/khalil-gibran">Khalil Gibran</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/kurt-cobain">Kurt Cobain</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/lao-tzu">Lao Tzu</a></div>
</div>
</div>
<div class="span4">
<div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/malcolm-x">Malcolm X</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/marcus-aurelius">Marcus Aurelius</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/margaret-thatcher">Margaret Thatcher</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/mark-twain">Mark Twain</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/martin-luther-king-jr">Martin Luther King, Jr.</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/maya-angelou">Maya Angelou</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/michael-jordan">Michael Jordan</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/mother-teresa">Mother Teresa</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/muhammad-ali">Muhammad Ali</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/napoleon-bonaparte">Napoleon Bonaparte</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/niccolo-machiavelli">Niccolo Machiavelli</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/oprah-winfrey">Oprah Winfrey</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/oscar-wilde">Oscar Wilde</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/pablo-picasso">Pablo Picasso</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/paulo-coelho">Paulo Coelho</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/plato">Plato</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/ralph-waldo-emerson">Ralph Waldo Emerson</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/robert-frost">Robert Frost</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/ronald-reagan">Ronald Reagan</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/saint-patrick">Saint Patrick</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/socrates">Socrates</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/stephen-hawking">Stephen Hawking</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/sun-tzu">Sun Tzu</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/swami-vivekananda">Swami Vivekananda</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/theodore-roosevelt">Theodore Roosevelt</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/thomas-a-edison">Thomas A. Edison</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/thomas-jefferson">Thomas Jefferson</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/tony-benn">Tony Benn</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/tupac-shakur">Tupac Shakur</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/vince-lombardi">Vince Lombardi</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/voltaire">Voltaire</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/walt-whitman">Walt Whitman</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/warren-buffett">Warren Buffett</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/wayne-dyer">Wayne Dyer</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/winston-churchill">Winston Churchill</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/yogi-berra">Yogi Berra</a></div>
<div class="bqLn"><a href="<?php echo HOME_URL; ?>a/zig-ziglar">Zig Ziglar</a></div>
</div>
</div>
</div>
</div>
</div>
<div class="span4">

<div class="bq_s">


<div class="bq_s">
<h2>Today's Birthdays</h2>
<?php
$dob = file_get_contents("http://192.187.123.194/css/kquotes-dob.php");
$dob_array = json_decode($dob);
foreach ($dob_array as $birthday)
{
$explode_dob = explode("@--@", $birthday);
?>
<div class="bqLn"><?php echo $explode_dob[1]; ?> - <a href="<?php echo HOME_URL; ?>a/<?php echo url_title($explode_dob[0]); ?>"><?php echo $explode_dob[0]; ?></a></div>
<?php
}
?>
</div>

<div class="bq_s">
<h2>In the News</h2>
<div style="padding-bottom:4px">We just updated these authors:</div>
<div class="row">
<div class="span4">
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/chris-pine">Chris Pine</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/keira-knightley">Keira Knightley</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/kenneth-branagh">Kenneth Branagh</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/george-clooney">George Clooney</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/bill-murray">Bill Murray</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/cate-blanchett">Cate Blanchett</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/kate-winslet">Kate Winslet</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/josh-brolin">Josh Brolin</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/bill-nighy">Bill Nighy</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/zac-efron">Zac Efron</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/rodrigo-santoro">Rodrigo Santoro</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/e/eva-green">Eva Green</a>
</div>
</div>
</div>
</div>

</div>
</div>
</div>
</div>

</div>

<div class="bq_bot_nav bq-no-print">
<div class="container">
<div class="row" style="color:white">
<table style="margin: 0 auto;">
<tr>
<td>