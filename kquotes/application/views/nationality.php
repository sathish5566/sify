<div class="bq_center" style="margin-left:-8px; margin-right:-8px;">
<div class="reflow_body bq_left">
<div class="reflow_container">

<div class="span12">
<br />

	<div class="bq_s">
<h2><?php echo $result[0]['nationality']; ?> Author Quotes</h2>
</div>


<div class="pagination bqNPgn pagination-small">
<ul class="nav">
<?php echo $links; ?>
</ul>
</div>

<div class="clearfix sticky_adzone">
<div id="quotesList" style="width:900px;">
<?php 
foreach ($result as $value) {
?>

<div class="masonryitem boxy bqQt bqShare">
<div class="boxyPaddingBig">
<h1 class="bqQuoteLink" style="border-bottom: 0px;">
<a title="view quote" href="<?php echo HOME_URL."q/".$url_title = substr(url_title(strtolower($value['quote'])),0,100); ?>-<?php echo $value['unique_id']; ?>">
<?php 
echo $value['quote'];
?>
<?php $topic = explode("@--@", $value['topics']); ?>
</a></h1><br>
<div class="bq-aut">
<a title="view quote" href="<?php echo HOME_URL; ?>a/<?php echo url_title($value['author']); ?>"><?php echo $value['author']; ?></a></div>
</div>
<div class="bq_q_nav boxyBottom boxyPaddingSmall" style="overflow:hidden;">
<div class="bq20" style="float:right;">
</div>
<div class="body bq_boxyRelatedLeft bqBlackLink ">
<?php
foreach ($topic as $val) {
?>
<a title="view keyword" href="<?php echo HOME_URL; ?>t/<?php echo $val; ?>"><?php echo $val; ?></a> |
<?php } ?>
</div>
</div>
</div>
<br />
<?php    
}
?>

</div>

<div class="pagination bqNPgn pagination-small">
<ul class="nav">
<?php echo $links; ?>
</ul>
</div>


</div>
</div>


</div>
</div>

<div class="bq_bot_nav bq-no-print">
<div class="container">
<div class="row" style="color:white">
<table style="margin: 0 auto;">
<tr>
<td>