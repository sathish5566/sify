<div class="bq_center" style="margin-left:-8px; margin-right:-8px;">
<div class="reflow_body bq_left">
<div class="reflow_container">

<div class="span8">
<br />

<div class="bq_s">
<h2><?php echo $result[0]['author']; ?> Quotes</h2>
</div>

<div class="pagination bqNPgn pagination-small">
<ul class="nav">
<?php echo $links; ?>
</ul>
</div>

<div class="clearfix sticky_adzone">
<div id="quotesList">
<?php 
if(count($result) == 0)
{
  echo "no results found";
}
else
{
foreach ($result as $value) {
?>

<div class="masonryitem boxy bqQt bqShare">
<div class="boxyPaddingBig">
<h1 class="bqQuoteLink" style="border-bottom: 0px;">
<a title="view quote" href="<?php echo HOME_URL."q/".$url_title = substr(url_title(strtolower($value['quote'])),0,100); ?>-<?php echo $value['unique_id']; ?>">
<?php 
echo $value['quote'];
?>
<?php $topic = explode("@--@", $value['topics']); ?>
</a></h1>
<div class="bq-aut">
<a title="view quote" href="<?php echo HOME_URL; ?>a/<?php echo url_title($value['author']); ?>"><?php echo $value['author']; ?></a></div>
</div>
<div class="bq_q_nav boxyBottom boxyPaddingSmall" style="overflow:hidden;">
<div class="bq20" style="float:right;">


<div class="fb-share-button" data-href="<?php echo HOME_URL."q/".$url_title = substr(url_title(strtolower($value['quote'])),0,100); ?>-<?php echo $value['unique_id']; ?>" data-type="button"></div>
&nbsp;&nbsp;&nbsp;
<a href="https://twitter.com/share" class="twitter-share-button" data-url="" data-text="<?php echo $value['quote']; ?> - KQuotes.com" data-lang="en" data-size="small" data-count="none">Tweet</a>



</div>
<div class="body bq_boxyRelatedLeft bqBlackLink ">
<?php
foreach ($topic as $val) {
?>
<a title="view keyword" href="<?php echo HOME_URL; ?>t/<?php echo $val; ?>"><?php echo $val; ?></a> |
<?php } ?>
</div>
</div>
</div>
<br />
<?php    
}
}
?>

</div>

<div class="pagination bqNPgn pagination-small">
<ul class="nav">
<?php echo $links; ?>
</ul>
</div>
</div>

</div>

<div class="span3" style="float:right;">
<div class="bq_s" style="margin-top:100px; position:fixed;">

<script type="text/javascript">
  ( function() {
    if (window.CHITIKA === undefined) { window.CHITIKA = { 'units' : [] }; };
    var unit = {"publisher":"sathish5566","width":160,"height":600,"sid":"Chitika Default","color_site_link":"006600","color_text":"006600"};
    var placement_id = window.CHITIKA.units.length;
    window.CHITIKA.units.push(unit);
    document.write('<div id="chitikaAdBlock-' + placement_id + '"></div>');
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.src = '//cdn.chitika.net/getads.js';
    try { document.getElementsByTagName('head')[0].appendChild(s); } catch(e) { document.write(s.outerHTML); }
}());
</script>

</div>
</div>

</div>
</div>

<div class="bq_bot_nav bq-no-print">
<div class="container">
<div class="row" style="color:white">
<table style="margin: 0 auto;">
<tr>
<td>