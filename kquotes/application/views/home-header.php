<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en" xmlns:fb="http://ogp.me/ns/fb#">
<head>
<meta charset="UTF-8">
<title>Famous Quotes on Web - KQuotes</title>
<meta name="description" content="Collection of famous quotes by famous authors, celebrities. Share our Quotes to Facebook, Twitter, and blogs." />
<meta name="keywords" content="quote, quotes, quotation, quotations, famous, quotable, epigrams, proverbs, aphorisms, words, literature, speeches, writing, reference" />
<meta name="msvalidate.01" content="EB774ABE4ECB8AB3BDEE8AA3A61F9AE1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="robots" content="index,follow" />
<meta property="og:title" content="Famous Quotes on Web"/>
<meta property="og:site_name" content="Kquotes.com"/>
<meta property="og:description" content="Collection of famous quotes by famous authors, celebrities. Share our Quotes to Facebook, Twitter, and blogs."/>


<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<link href="<?php echo base_url(); ?>asset/core-homepg.css" media="screen, print" type="text/css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>asset/style.css">
<script type="text/javascript" src="<?php echo base_url(); ?>asset/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>asset/jquery.autocomplete.min.js"></script>
<?php
$authors = file_get_contents("asset/authors.txt");
$author = explode("@--@", $authors);
?>
<script type="text/javascript">
  $(function(){
  var currencies = [
    <?php foreach ($author as $value) { ?>{value:"<?php echo $value; ?> Quotes"},<?php } ?>
  ];
  
  // setup autocomplete function pulling from currencies[] array
  $('#autocomplete').autocomplete({
    lookup: currencies,
    onSelect: function (suggestion) {
      var thehtml = '<strong>Currency Name:</strong> ' + suggestion.value + ' <br> <strong>Symbol:</strong> ' + suggestion.data;
      $('#outputcontent').html(thehtml);
    }
  });
  

});
</script>

<style type="text/css">h1{font-size:20.0px;}.letter-navbar{min-height:25px;padding-top:3px;}.bq-tn-letters{font-size:13px;}</style>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-49389862-1', 'kquotes.com');
  ga('send', 'pageview');

</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=741736492538046";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

</head>
<body>

<div id="bq-tn" class="bq_tn_cntr">
<div id="bq-top-nav" style="left:0px;">
<div class="slim navbar" style="margin-bottom: 0px">
<div class="navbar-inner navbar-pull-center" data-sai-excludehighlight="true">
<ul class="nav">
<li id="bq-nav-home-i">
<span style="padding-top:0px" onclick="javascript:window.location.href='/'">
&nbsp;&nbsp;</span>
</li>
<li class="active" id="sl-bq-nav-home-b">
<a class="btn btn-navbar bq-tn-btn" href="/">&nbsp;<span class="icon-home icon-white"></span>&nbsp;</a>
</li>
<li class="" id="sl-bq-nav-home-t"><a href="<?php echo base_url(); ?>" class="txnav">KQUOTES.COM</a></li>
<li class="" id="sl-bq-nav-home-t"><a href="<?php echo base_url(); ?>" class="txnav">Home</a></li>
<li class="" id="sl-bq-nav-home-t"><a href="<?php echo HOME_URL; ?>author" class="txnav">Authors</a></li>
<li class="" id="sl-bq-nav-home-t"><a href="<?php echo HOME_URL; ?>topics" class="txnav">Topics</a></li>

<li id="sl-bq-nv-srch-f">

<form class="navbar-search bq-no-print pull-left" action="<?php echo HOME_URL; ?>search" method="get">
<input type="text" class="biginput" id="autocomplete" name="q" value="Totally We Found 305062 Quotes On Web" style="width: 300px; margin-left: 110px;" onclick="this.value='';" onfocus="this.select()" onblur="this.value=!this.value?'Totally We Found 305062 Quotes On Web':this.value;" />
<input type="submit" name="submit" value="submit" style="margin-bottom: 10px;">
</form>

</li>
<li id="sl-bq-nv-srch-b">
<a class="btn btn-navbar bq-tn-btn" href="/search">&nbsp;<span class="icon-search icon-white"></span>&nbsp;</a>
</li>
</ul>
</div>
</div>
</div>
</div>