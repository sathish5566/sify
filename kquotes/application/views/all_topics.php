<div class="container">
<div class="row">
<div class="span12">
<h1>Quote Topics</h1>
<div class="bq_s">
Looking for a quote from your favorite topic? Our quote collections are organized
by topic to help you find the perfect quote.
Enjoy quotes on popular topics like: Love, Life, Friendship, Success, Wisdom.
</div>
</div>
</div>
<div class="row bq_left">
<div class="span8">
</div>
<div class="span4">
</div>
</div>
<div class="row bq_left">
<div class="span4">
<div class="content">
<table>
<tr valign="top">
<td style="width:150px">
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/age" onClick="topicCl('<?php echo HOME_URL; ?>t/age',1,'Index')">Age</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/alone" onClick="topicCl('<?php echo HOME_URL; ?>t/alone',2,'Index')">Alone</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/amazing" onClick="topicCl('<?php echo HOME_URL; ?>t/amazing',3,'Index')">Amazing</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/anger" onClick="topicCl('<?php echo HOME_URL; ?>t/anger',4,'Index')">Anger</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/anniversary" onClick="topicCl('<?php echo HOME_URL; ?>t/anniversary',5,'Index')">Anniversary</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/architecture" onClick="topicCl('<?php echo HOME_URL; ?>t/architecture',6,'Index')">Architecture</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/art" onClick="topicCl('<?php echo HOME_URL; ?>t/art',7,'Index')">Art</a>

</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/attitude" onClick="topicCl('<?php echo HOME_URL; ?>t/attitude',8,'Index')">Attitude</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/beauty" onClick="topicCl('<?php echo HOME_URL; ?>t/beauty',9,'Index')">Beauty</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/best" onClick="topicCl('<?php echo HOME_URL; ?>t/best',10,'Index')">Best</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/birthday" onClick="topicCl('<?php echo HOME_URL; ?>t/birthday',11,'Index')">Birthday</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/brainy" onClick="topicCl('<?php echo HOME_URL; ?>t/brainy',12,'Index')">Brainy</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/business" onClick="topicCl('<?php echo HOME_URL; ?>t/business',13,'Index')">Business</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/car" onClick="topicCl('<?php echo HOME_URL; ?>t/car',14,'Index')">Car</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/change" onClick="topicCl('<?php echo HOME_URL; ?>t/change',15,'Index')">Change</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/communication" onClick="topicCl('<?php echo HOME_URL; ?>t/communication',16,'Index')">Communication</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/computers" onClick="topicCl('<?php echo HOME_URL; ?>t/computers',17,'Index')">Computers</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/cool" onClick="topicCl('<?php echo HOME_URL; ?>t/cool',18,'Index')">Cool</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/courage" onClick="topicCl('<?php echo HOME_URL; ?>t/courage',19,'Index')">Courage</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/dad" onClick="topicCl('<?php echo HOME_URL; ?>t/dad',20,'Index')">Dad</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/dating" onClick="topicCl('<?php echo HOME_URL; ?>t/dating',21,'Index')">Dating</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/death" onClick="topicCl('<?php echo HOME_URL; ?>t/death',22,'Index')">Death</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/design" onClick="topicCl('<?php echo HOME_URL; ?>t/design',23,'Index')">Design</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/diet" onClick="topicCl('<?php echo HOME_URL; ?>t/diet',24,'Index')">Diet</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/dreams" onClick="topicCl('<?php echo HOME_URL; ?>t/dreams',25,'Index')">Dreams</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/education" onClick="topicCl('<?php echo HOME_URL; ?>t/education',26,'Index')">Education</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/environmental" onClick="topicCl('<?php echo HOME_URL; ?>t/environmental',27,'Index')">Environmental</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/equality" onClick="topicCl('<?php echo HOME_URL; ?>t/equality',28,'Index')">Equality</a>
</div>
</div>
</td>
<td>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/experience" onClick="topicCl('<?php echo HOME_URL; ?>t/experience',1,'Index')">Experience</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/failure" onClick="topicCl('<?php echo HOME_URL; ?>t/failure',2,'Index')">Failure</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/faith" onClick="topicCl('<?php echo HOME_URL; ?>t/faith',3,'Index')">Faith</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/family" onClick="topicCl('<?php echo HOME_URL; ?>t/family',4,'Index')">Family</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/famous" onClick="topicCl('<?php echo HOME_URL; ?>t/famous',5,'Index')">Famous</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/fear" onClick="topicCl('<?php echo HOME_URL; ?>t/fear',6,'Index')">Fear</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/finance" onClick="topicCl('<?php echo HOME_URL; ?>t/finance',7,'Index')">Finance</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/fitness" onClick="topicCl('<?php echo HOME_URL; ?>t/fitness',8,'Index')">Fitness</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/food" onClick="topicCl('<?php echo HOME_URL; ?>t/food',9,'Index')">Food</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/forgiveness" onClick="topicCl('<?php echo HOME_URL; ?>t/forgiveness',10,'Index')">Forgiveness</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/freedom" onClick="topicCl('<?php echo HOME_URL; ?>t/freedom',11,'Index')">Freedom</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/friendship" onClick="topicCl('<?php echo HOME_URL; ?>t/friendship',12,'Index')">Friendship</a>

</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/funny" onClick="topicCl('<?php echo HOME_URL; ?>t/funny',13,'Index')">Funny</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/future" onClick="topicCl('<?php echo HOME_URL; ?>t/future',14,'Index')">Future</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/gardening" onClick="topicCl('<?php echo HOME_URL; ?>t/gardening',15,'Index')">Gardening</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/god" onClick="topicCl('<?php echo HOME_URL; ?>t/god',16,'Index')">God</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/good" onClick="topicCl('<?php echo HOME_URL; ?>t/good',17,'Index')">Good</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/government" onClick="topicCl('<?php echo HOME_URL; ?>t/government',18,'Index')">Government</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/graduation" onClick="topicCl('<?php echo HOME_URL; ?>t/graduation',19,'Index')">Graduation</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/great" onClick="topicCl('<?php echo HOME_URL; ?>t/great',20,'Index')">Great</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/happiness" onClick="topicCl('<?php echo HOME_URL; ?>t/happiness',21,'Index')">Happiness</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/health" onClick="topicCl('<?php echo HOME_URL; ?>t/health',22,'Index')">Health</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/history" onClick="topicCl('<?php echo HOME_URL; ?>t/history',23,'Index')">History</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/home" onClick="topicCl('<?php echo HOME_URL; ?>t/home',24,'Index')">Home</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/hope" onClick="topicCl('<?php echo HOME_URL; ?>t/hope',25,'Index')">Hope</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/humor" onClick="topicCl('<?php echo HOME_URL; ?>t/humor',26,'Index')">Humor</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/imagination" onClick="topicCl('<?php echo HOME_URL; ?>t/imagination',27,'Index')">Imagination</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/inspirational" onClick="topicCl('<?php echo HOME_URL; ?>t/inspirational',28,'Index')">Inspirational</a>
</div>
</div>
</td>
</tr>
</table>
</div>
</div>
<div class="span4">
<div class="content">
<table>
<tr valign="top">
<td style="width:150px">
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/intelligence" onClick="topicCl('<?php echo HOME_URL; ?>t/intelligence',1,'Index')">Intelligence</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/jealousy" onClick="topicCl('<?php echo HOME_URL; ?>t/jealousy',2,'Index')">Jealousy</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/knowledge" onClick="topicCl('<?php echo HOME_URL; ?>t/knowledge',3,'Index')">Knowledge</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/leadership" onClick="topicCl('<?php echo HOME_URL; ?>t/leadership',4,'Index')">Leadership</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/learning" onClick="topicCl('<?php echo HOME_URL; ?>t/learning',5,'Index')">Learning</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/legal" onClick="topicCl('<?php echo HOME_URL; ?>t/legal',6,'Index')">Legal</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/life" onClick="topicCl('<?php echo HOME_URL; ?>t/life',7,'Index')">Life</a>

</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/love" onClick="topicCl('<?php echo HOME_URL; ?>t/love',8,'Index')">Love</a>

</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/marriage" onClick="topicCl('<?php echo HOME_URL; ?>t/marriage',9,'Index')">Marriage</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/medical" onClick="topicCl('<?php echo HOME_URL; ?>t/medical',10,'Index')">Medical</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/men" onClick="topicCl('<?php echo HOME_URL; ?>t/men',11,'Index')">Men</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/mom" onClick="topicCl('<?php echo HOME_URL; ?>t/mom',12,'Index')">Mom</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/money" onClick="topicCl('<?php echo HOME_URL; ?>t/money',13,'Index')">Money</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/morning" onClick="topicCl('<?php echo HOME_URL; ?>t/morning',14,'Index')">Morning</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/motivational" onClick="topicCl('<?php echo HOME_URL; ?>t/motivational',15,'Index')">Motivational</a>

</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/movies" onClick="topicCl('<?php echo HOME_URL; ?>t/movies',16,'Index')">Movies</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/movingon" onClick="topicCl('<?php echo HOME_URL; ?>t/movingon',17,'Index')">Moving On</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/music" onClick="topicCl('<?php echo HOME_URL; ?>t/music',18,'Index')">Music</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/nature" onClick="topicCl('<?php echo HOME_URL; ?>t/nature',19,'Index')">Nature</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/parenting" onClick="topicCl('<?php echo HOME_URL; ?>t/parenting',20,'Index')">Parenting</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/patience" onClick="topicCl('<?php echo HOME_URL; ?>t/patience',21,'Index')">Patience</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/patriotism" onClick="topicCl('<?php echo HOME_URL; ?>t/patriotism',22,'Index')">Patriotism</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/peace" onClick="topicCl('<?php echo HOME_URL; ?>t/peace',23,'Index')">Peace</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/pet" onClick="topicCl('<?php echo HOME_URL; ?>t/pet',24,'Index')">Pet</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/poetry" onClick="topicCl('<?php echo HOME_URL; ?>t/poetry',25,'Index')">Poetry</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/politics" onClick="topicCl('<?php echo HOME_URL; ?>t/politics',26,'Index')">Politics</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/positive" onClick="topicCl('<?php echo HOME_URL; ?>t/positive',27,'Index')">Positive</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/power" onClick="topicCl('<?php echo HOME_URL; ?>t/power',28,'Index')">Power</a>
</div>
</div>
</td>
<td>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/relationship" onClick="topicCl('<?php echo HOME_URL; ?>t/relationship',1,'Index')">Relationship</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/religion" onClick="topicCl('<?php echo HOME_URL; ?>t/religion',2,'Index')">Religion</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/respect" onClick="topicCl('<?php echo HOME_URL; ?>t/respect',3,'Index')">Respect</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/romantic" onClick="topicCl('<?php echo HOME_URL; ?>t/romantic',4,'Index')">Romantic</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/sad" onClick="topicCl('<?php echo HOME_URL; ?>t/sad',5,'Index')">Sad</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/science" onClick="topicCl('<?php echo HOME_URL; ?>t/science',6,'Index')">Science</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/smile" onClick="topicCl('<?php echo HOME_URL; ?>t/smile',7,'Index')">Smile</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/society" onClick="topicCl('<?php echo HOME_URL; ?>t/society',8,'Index')">Society</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/sports" onClick="topicCl('<?php echo HOME_URL; ?>t/sports',9,'Index')">Sports</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/strength" onClick="topicCl('<?php echo HOME_URL; ?>t/strength',10,'Index')">Strength</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/success" onClick="topicCl('<?php echo HOME_URL; ?>t/success',11,'Index')">Success</a>

</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/sympathy" onClick="topicCl('<?php echo HOME_URL; ?>t/sympathy',12,'Index')">Sympathy</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/teacher" onClick="topicCl('<?php echo HOME_URL; ?>t/teacher',13,'Index')">Teacher</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/technology" onClick="topicCl('<?php echo HOME_URL; ?>t/technology',14,'Index')">Technology</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/teen" onClick="topicCl('<?php echo HOME_URL; ?>t/teen',15,'Index')">Teen</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/thankful" onClick="topicCl('<?php echo HOME_URL; ?>t/thankful',16,'Index')">Thankful</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/time" onClick="topicCl('<?php echo HOME_URL; ?>t/time',17,'Index')">Time</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/travel" onClick="topicCl('<?php echo HOME_URL; ?>t/travel',18,'Index')">Travel</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/trust" onClick="topicCl('<?php echo HOME_URL; ?>t/trust',19,'Index')">Trust</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/truth" onClick="topicCl('<?php echo HOME_URL; ?>t/truth',20,'Index')">Truth</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/war" onClick="topicCl('<?php echo HOME_URL; ?>t/war',21,'Index')">War</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/wedding" onClick="topicCl('<?php echo HOME_URL; ?>t/wedding',22,'Index')">Wedding</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/wisdom" onClick="topicCl('<?php echo HOME_URL; ?>t/wisdom',23,'Index')">Wisdom</a>

</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/women" onClick="topicCl('<?php echo HOME_URL; ?>t/women',24,'Index')">Women</a>
</div>
</div>
<div class="bqLn">
<div class="bqLn"><a href="<?php echo HOME_URL; ?>t/work" onClick="topicCl('<?php echo HOME_URL; ?>t/work',25,'Index')">Work</a>
</div>
</div>
</td>
</tr>
</table>
</div>
</div>
<div class="span4">
 

</div>
</div>


<div class="bq_bot_nav bq-no-print">
<div class="container">
<div class="row" style="color:white">
<table style="margin: 0 auto;">
<tr>