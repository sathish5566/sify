<div class="bq_center" style="margin-left:-8px; margin-right:-8px;">
<div class="reflow_body bq_left">
<div class="reflow_container">

<div class="span4" style="float:right; width: 28%; margin-top:10px;">
<?php 
foreach ($result as $value) {
?>
<div class="bq_s postDelay">
<h2>Share with your Friends</h2>
<div class="bq25 bqStatIcns">
<div class="fb-share-button" data-href="<?php echo HOME_URL."q/".$url_title = substr(url_title(strtolower($value['quote'])),0,100); ?>-<?php echo $value['unique_id']; ?>" data-type="button"></div>
&nbsp;&nbsp;&nbsp;
<a href="https://twitter.com/share" class="twitter-share-button" data-url="" data-text="<?php echo $value['quote']; ?> - KQuotes.com" data-lang="en" data-size="small" data-count="none" style="padding-top:2px;">Tweet</a>

</div>
Everybody loves a good quotation so don't forget to share with your friends
</div>
<div class="bq_s">
<h2>Biography</h2>
<div class="bqLn">
Author Profession: <a href="<?php echo base_url(); ?>p/<?php echo $value['profession']; ?>"><?php echo $value['profession']; ?></a>
</div>
<div class="bqLn">
Nationality: <a href="<?php echo base_url(); ?>nationality/<?php echo $value['nationality']; ?>"><?php echo $value['nationality']; ?></a>
</div>
<div class="bqLn">
Born: <a href=""><?php echo $value['born']; ?></a>
</div>
</div>
<div class="bq_s">
<h2>Quote Topics</h2>
<div class="bqLn">
<?php $topic = explode("@--@", $value['topics']); 
foreach ($topic as $val) {
?>
<a title="view keyword" href="<?php echo HOME_URL; ?>t/<?php echo $val; ?>"><?php echo $val; ?></a>,
<?php
}
?>
</div>
</div>

<div class="bq_s">
<div class="bqLn">
<script type="text/javascript">
  ( function() {
    if (window.CHITIKA === undefined) { window.CHITIKA = { 'units' : [] }; };
    var unit = {"publisher":"sathish5566","width":300,"height":250,"sid":"Chitika Default","color_site_link":"006600","color_text":"006600"};
    var placement_id = window.CHITIKA.units.length;
    window.CHITIKA.units.push(unit);
    document.write('<div id="chitikaAdBlock-' + placement_id + '"></div>');
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.src = '//cdn.chitika.net/getads.js';
    try { document.getElementsByTagName('head')[0].appendChild(s); } catch(e) { document.write(s.outerHTML); }
}());
</script>
</div>
</div>

<div class="bq_s">
<h2>Related Authors</h2>
<div class="bqLn">
<?php
foreach ($related_author as $val) { 
?>
<a title="view keyword" href="<?php echo base_url(); ?>a/<?php echo url_title($val['doclist']['docs'][0]['author']); ?>"><?php echo $val['doclist']['docs'][0]['author']; ?></a>,
<?php
}
?>
</div>
</div>
<?php } ?>
</div>

<div class="span8">
<br />
<div class="clearfix sticky_adzone">
<div id="quotesList">
<?php 
foreach ($result as $value) {
?>

<div class="masonryitem boxy bqQt bqShare">
<div class="boxyPaddingBig">
<img src="<?php echo base_url().$image_path; ?>" alt="<?php echo $value['quote']; ?>">
<br /><br />
<h1 class="bqQuoteLink" style="border-bottom: 0px;">
<span title="view quote" href="<?php echo HOME_URL."q/".$url_title = substr(url_title(strtolower($value['quote'])),0,100); ?>-<?php echo $value['unique_id']; ?>">
<?php 
echo $value['quote'];
?>
</span></h1><br>

<div class="bq-aut">
<a title="view quote" href="<?php echo HOME_URL; ?>a/<?php echo url_title($value['author']); ?>"><?php echo $value['author']; ?></a></div>
</div>
</div>
<br />
<?php    
}
?>
<h1 style="margin-top:10px; margin-bottom:10px;">Related Quotes</h1>

<?php 
foreach ($related_result as $value) {
?>

<div class="masonryitem boxy bqQt bqShare">
<div class="boxyPaddingBig">
<h1 class="bqQuoteLink" style="border-bottom: 0px;">
<a title="view quote" href="<?php echo HOME_URL."q/".$url_title = substr(url_title(strtolower($value['quote'])),0,100); ?>-<?php echo $value['unique_id']; ?>">
<?php 
echo $value['quote'];
?>
<?php $topic = explode("@--@", $value['topics']); ?>
</a></h1><br>
<div class="bq-aut">
<a title="view quote" href="<?php echo HOME_URL; ?>a/<?php echo url_title($value['author']); ?>"><?php echo $value['author']; ?></a></div>
</div>
<div class="bq_q_nav boxyBottom boxyPaddingSmall" style="overflow:hidden;">
<div class="bq20" style="float:right;">
</div>
<div class="body bq_boxyRelatedLeft bqBlackLink ">
<?php
foreach ($topic as $val) {
?>
<a title="view keyword" href="<?php echo HOME_URL; ?>t/<?php echo url_title($val); ?>"><?php echo $val; ?></a> |
<?php } ?>
</div>
</div>
</div>
<br />
<?php    
}
?>

</div>
</div>
</div>


</div>
</div>

<div class="bq_bot_nav bq-no-print">
<div class="container">
<div class="row" style="color:white">
<table style="margin: 0 auto;">
<tr>
<td>