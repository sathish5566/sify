<div class="container">
<div class="row">
<div class="span12">
<h1>Favorite Authors</h1>
<div class="bq_s">
Looking for quotes by our most popular authors? Gather wisdom from the ages as you
browse favorite quotes by famous authors like: Aristotle, Abraham Lincoln, Thomas
Jeferson, Oscar Wilde, and William Shakespeare.
</div>
</div>
</div>
<div class="row bq_left">
<div class="span8">
<div class="content">
<div class="row">
<div class="span4">
<div class="bq_s">
<h2><a href="/quotes/a">A Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/ansel_adams" onClick="authorCl('<?php echo HOME_URL; ?>a/ansel_adams',1,'FaveIdx')">Ansel Adams</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/douglas_adams" onClick="authorCl('<?php echo HOME_URL; ?>a/douglas_adams',2,'FaveIdx')">Douglas Adams</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/john_adams" onClick="authorCl('<?php echo HOME_URL; ?>a/john_adams',3,'FaveIdx')">John Adams</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/joseph_addison" onClick="authorCl('<?php echo HOME_URL; ?>a/joseph_addison',4,'FaveIdx')">Joseph Addison</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/aesop" onClick="authorCl('<?php echo HOME_URL; ?>a/aesop',5,'FaveIdx')">Aesop</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/sholom_aleichem" onClick="authorCl('<?php echo HOME_URL; ?>a/sholom_aleichem',6,'FaveIdx')">Sholom Aleichem</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/muhammad_ali" onClick="authorCl('<?php echo HOME_URL; ?>a/muhammad_ali',7,'FaveIdx')">Muhammad Ali</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/dante_alighieri" onClick="authorCl('<?php echo HOME_URL; ?>a/dante_alighieri',8,'FaveIdx')">Dante Alighieri</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/woody_allen" onClick="authorCl('<?php echo HOME_URL; ?>a/woody_allen',9,'FaveIdx')">Woody Allen</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/walter_anderson" onClick="authorCl('<?php echo HOME_URL; ?>a/walter_anderson',10,'FaveIdx')">Walter Anderson</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/barbara_de_angelis" onClick="authorCl('<?php echo HOME_URL; ?>a/barbara_de_angelis',11,'FaveIdx')">Barbara de Angelis</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/maya_angelou" onClick="authorCl('<?php echo HOME_URL; ?>a/maya_angelou',12,'FaveIdx')">Maya Angelou</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/susan_b_anthony" onClick="authorCl('<?php echo HOME_URL; ?>a/susan_b_anthony',13,'FaveIdx')">Susan B. Anthony</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/thomas_aquinas" onClick="authorCl('<?php echo HOME_URL; ?>a/thomas_aquinas',14,'FaveIdx')">Thomas Aquinas</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/aristotle" onClick="authorCl('<?php echo HOME_URL; ?>a/aristotle',15,'FaveIdx')">Aristotle</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/lance_armstrong" onClick="authorCl('<?php echo HOME_URL; ?>a/lance_armstrong',16,'FaveIdx')">Lance Armstrong</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/mary_kay_ash" onClick="authorCl('<?php echo HOME_URL; ?>a/mary_kay_ash',17,'FaveIdx')">Mary Kay Ash</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/isaac_asimov" onClick="authorCl('<?php echo HOME_URL; ?>a/isaac_asimov',18,'FaveIdx')">Isaac Asimov</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/francis_of_assisi" onClick="authorCl('<?php echo HOME_URL; ?>a/francis_of_assisi',19,'FaveIdx')">Francis of Assisi</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/saint_augustine" onClick="authorCl('<?php echo HOME_URL; ?>a/saint_augustine',20,'FaveIdx')">Saint Augustine</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/marcus_aurelius" onClick="authorCl('<?php echo HOME_URL; ?>a/marcus_aurelius',21,'FaveIdx')">Marcus Aurelius</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/jane_austen" onClick="authorCl('<?php echo HOME_URL; ?>a/jane_austen',22,'FaveIdx')">Jane Austen</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/saint_teresa_of_avila" onClick="authorCl('<?php echo HOME_URL; ?>a/saint_teresa_of_avila',23,'FaveIdx')">Saint Teresa of Avila</a>
</div>
<div style="padding-top:5px">

</div>
</div>
<div class="bq_s">
<h2><a href="/quotes/b">B Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/sai_baba" onClick="authorCl('<?php echo HOME_URL; ?>a/sai_baba',1,'FaveIdx')">Sai Baba</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/richard_bach" onClick="authorCl('<?php echo HOME_URL; ?>a/richard_bach',2,'FaveIdx')">Richard Bach</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/francis_bacon" onClick="authorCl('<?php echo HOME_URL; ?>a/francis_bacon',3,'FaveIdx')">Francis Bacon</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/james_a_baldwin" onClick="authorCl('<?php echo HOME_URL; ?>a/james_a_baldwin',4,'FaveIdx')">James A. Baldwin</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/lucille_ball" onClick="authorCl('<?php echo HOME_URL; ?>a/lucille_ball',5,'FaveIdx')">Lucille Ball</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/honore_de_balzac" onClick="authorCl('<?php echo HOME_URL; ?>a/honore_de_balzac',6,'FaveIdx')">Honore de Balzac</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/amelia_barr" onClick="authorCl('<?php echo HOME_URL; ?>a/amelia_barr',7,'FaveIdx')">Amelia Barr</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/james_m_barrie" onClick="authorCl('<?php echo HOME_URL; ?>a/james_m_barrie',8,'FaveIdx')">James M. Barrie</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/john_barrymore" onClick="authorCl('<?php echo HOME_URL; ?>a/john_barrymore',9,'FaveIdx')">John Barrymore</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/simone_de_beauvoir" onClick="authorCl('<?php echo HOME_URL; ?>a/simone_de_beauvoir',10,'FaveIdx')">Simone de Beauvoir</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/henry_ward_beecher" onClick="authorCl('<?php echo HOME_URL; ?>a/henry_ward_beecher',11,'FaveIdx')">Henry Ward Beecher</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/bo_bennett" onClick="authorCl('<?php echo HOME_URL; ?>a/bo_bennett',12,'FaveIdx')">Bo Bennett</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/ingrid_bergman" onClick="authorCl('<?php echo HOME_URL; ?>a/ingrid_bergman',13,'FaveIdx')">Ingrid Bergman</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/milton_berle" onClick="authorCl('<?php echo HOME_URL; ?>a/milton_berle',14,'FaveIdx')">Milton Berle</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/yogi_berra" onClick="authorCl('<?php echo HOME_URL; ?>a/yogi_berra',15,'FaveIdx')">Yogi Berra</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/ambrose_bierce" onClick="authorCl('<?php echo HOME_URL; ?>a/ambrose_bierce',16,'FaveIdx')">Ambrose Bierce</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/josh_billings" onClick="authorCl('<?php echo HOME_URL; ?>a/josh_billings',17,'FaveIdx')">Josh Billings</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/william_blake" onClick="authorCl('<?php echo HOME_URL; ?>a/william_blake',18,'FaveIdx')">William Blake</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/niels_bohr" onClick="authorCl('<?php echo HOME_URL; ?>a/niels_bohr',19,'FaveIdx')">Niels Bohr</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/erma_bombeck" onClick="authorCl('<?php echo HOME_URL; ?>a/erma_bombeck',20,'FaveIdx')">Erma Bombeck</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/napoleon_bonaparte" onClick="authorCl('<?php echo HOME_URL; ?>a/napoleon_bonaparte',21,'FaveIdx')">Napoleon Bonaparte</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/vanna_bonta" onClick="authorCl('<?php echo HOME_URL; ?>a/vanna_bonta',22,'FaveIdx')">Vanna Bonta</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/chhristian_nestell_bovee" onClick="authorCl('<?php echo HOME_URL; ?>a/christian_nestell_bovee',23,'FaveIdx')">Christian Nestell Bovee</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/ray_bradbury" onClick="authorCl('<?php echo HOME_URL; ?>a/ray_bradbury',24,'FaveIdx')">Ray Bradbury</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/robert_brault" onClick="authorCl('<?php echo HOME_URL; ?>a/robert_brault',25,'FaveIdx')">Robert Brault</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/david_brinkley" onClick="authorCl('<?php echo HOME_URL; ?>a/david_brinkley',26,'FaveIdx')">David Brinkley</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/h_jackson_brown_jr" onClick="authorCl('<?php echo HOME_URL; ?>a/h_jackson_brown_jr',27,'FaveIdx')">H. Jackson Brown, Jr.</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/les_brown" onClick="authorCl('<?php echo HOME_URL; ?>a/les_brown',28,'FaveIdx')">Les Brown</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/kobe_bryant" onClick="authorCl('<?php echo HOME_URL; ?>a/kobe_bryant',29,'FaveIdx')">Kobe Bryant</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/buddha" onClick="authorCl('<?php echo HOME_URL; ?>a/buddha',30,'FaveIdx')">Buddha</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/warren_buffett" onClick="authorCl('<?php echo HOME_URL; ?>a/warren_buffett',31,'FaveIdx')">Warren Buffett</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/charles_bukowski" onClick="authorCl('<?php echo HOME_URL; ?>a/charles_bukowski',32,'FaveIdx')">Charles Bukowski</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/edmund_burke" onClick="authorCl('<?php echo HOME_URL; ?>a/edmund_burke',33,'FaveIdx')">Edmund Burke</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/george_burns" onClick="authorCl('<?php echo HOME_URL; ?>a/george_burns',34,'FaveIdx')">George Burns</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/john_burroughs" onClick="authorCl('<?php echo HOME_URL; ?>a/john_burroughs',35,'FaveIdx')">John Burroughs</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/william_s_burroughs" onClick="authorCl('<?php echo HOME_URL; ?>a/william_s_burroughs',36,'FaveIdx')">William S. Burroughs</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/leo_buscaglia" onClick="authorCl('<?php echo HOME_URL; ?>a/leo_buscaglia',37,'FaveIdx')">Leo Buscaglia</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/george_w_bush" onClick="authorCl('<?php echo HOME_URL; ?>a/george_w_bush',38,'FaveIdx')">George W. Bush</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/samuel_butler" onClick="authorCl('<?php echo HOME_URL; ?>a/samuel_butler',39,'FaveIdx')">Samuel Butler</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/lord_byron" onClick="authorCl('<?php echo HOME_URL; ?>a/lord_byron',40,'FaveIdx')">Lord Byron</a>
</div>
<div style="padding-top:5px">

</div>
</div>
<div class="bq_s">
<h2><a href="/quotes/c">C Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/julius_caesar" onClick="authorCl('<?php echo HOME_URL; ?>a/julius_caesar',1,'FaveIdx')">Julius Caesar</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/joseph_campbell" onClick="authorCl('<?php echo HOME_URL; ?>a/joseph_campbell',2,'FaveIdx')">Joseph Campbell</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/albert_camus" onClick="authorCl('<?php echo HOME_URL; ?>a/albert_camus',3,'FaveIdx')">Albert Camus</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/george_carlin" onClick="authorCl('<?php echo HOME_URL; ?>a/george_carlin',4,'FaveIdx')">George Carlin</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/thomas_carlyle" onClick="authorCl('<?php echo HOME_URL; ?>a/thomas_carlyle',5,'FaveIdx')">Thomas Carlyle</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/dale_carnegie" onClick="authorCl('<?php echo HOME_URL; ?>a/dale_carnegie',6,'FaveIdx')">Dale Carnegie</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/jim_carrey" onClick="authorCl('<?php echo HOME_URL; ?>a/jim_carrey',7,'FaveIdx')">Jim Carrey</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/lewis_carroll" onClick="authorCl('<?php echo HOME_URL; ?>a/lewis_carroll',8,'FaveIdx')">Lewis Carroll</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/george_washington_carver" onClick="authorCl('<?php echo HOME_URL; ?>a/george_washington_carver',9,'FaveIdx')">George Washington Carver</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/johnny_cash" onClick="authorCl('<?php echo HOME_URL; ?>a/johnny_cash',10,'FaveIdx')">Johnny Cash</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/edgar_cayce" onClick="authorCl('<?php echo HOME_URL; ?>a/edgar_cayce',11,'FaveIdx')">Edgar Cayce</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/chanakya" onClick="authorCl('<?php echo HOME_URL; ?>a/chanakya',12,'FaveIdx')">Chanakya</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/coco_chanel" onClick="authorCl('<?php echo HOME_URL; ?>a/coco_chanel',13,'FaveIdx')">Coco Chanel</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/charlie_chaplin" onClick="authorCl('<?php echo HOME_URL; ?>a/charlie_chaplin',14,'FaveIdx')">Charlie Chaplin</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/pierre_teilhard_de_chardi" onClick="authorCl('<?php echo HOME_URL; ?>a/pierre_teilhard_de_chardi',15,'FaveIdx')">Pierre Teilhard de Chardin</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/anton_chekhov" onClick="authorCl('<?php echo HOME_URL; ?>a/anton_chekhov',16,'FaveIdx')">Anton Chekhov</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/gilbert_k_chesterton" onClick="authorCl('<?php echo HOME_URL; ?>a/gilbert_k_chesterton',17,'FaveIdx')">Gilbert K. Chesterton</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/noam_chomsky" onClick="authorCl('<?php echo HOME_URL; ?>a/noam_chomsky',18,'FaveIdx')">Noam Chomsky</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/deepak_chopra" onClick="authorCl('<?php echo HOME_URL; ?>a/deepak_chopra',19,'FaveIdx')">Deepak Chopra</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/jesus_christ" onClick="authorCl('<?php echo HOME_URL; ?>a/jesus_christ',20,'FaveIdx')">Jesus Christ</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/agatha_christie" onClick="authorCl('<?php echo HOME_URL; ?>a/agatha_christie',21,'FaveIdx')">Agatha Christie</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/winston_churchill" onClick="authorCl('<?php echo HOME_URL; ?>a/winston_churchill',22,'FaveIdx')">Winston Churchill</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/marcus_tullius_cicero" onClick="authorCl('<?php echo HOME_URL; ?>a/marcus_tullius_cicero',23,'FaveIdx')">Marcus Tullius Cicero</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/hillary_clinton" onClick="authorCl('<?php echo HOME_URL; ?>a/hillary_clinton',24,'FaveIdx')">Hillary Clinton</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/kurt_cobain" onClick="authorCl('<?php echo HOME_URL; ?>a/kurt_cobain',25,'FaveIdx')">Kurt Cobain</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/paulo_coelho" onClick="authorCl('<?php echo HOME_URL; ?>a/paulo_coelho',26,'FaveIdx')">Paulo Coelho</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/charles_caleb_colton" onClick="authorCl('<?php echo HOME_URL; ?>a/charles_caleb_colton',27,'FaveIdx')">Charles Caleb Colton</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/confucius" onClick="authorCl('<?php echo HOME_URL; ?>a/confucius',28,'FaveIdx')">Confucius</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/calvin_coolidge" onClick="authorCl('<?php echo HOME_URL; ?>a/calvin_coolidge',29,'FaveIdx')">Calvin Coolidge</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/bill_copeland" onClick="authorCl('<?php echo HOME_URL; ?>a/bill_copeland',30,'FaveIdx')">Bill Copeland</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/bill_cosby" onClick="authorCl('<?php echo HOME_URL; ?>a/bill_cosby',31,'FaveIdx')">Bill Cosby</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/jacques_yves_cousteau" onClick="authorCl('<?php echo HOME_URL; ?>a/jacques_yves_cousteau',32,'FaveIdx')">Jacques Yves Cousteau</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/stephen_covey" onClick="authorCl('<?php echo HOME_URL; ?>a/stephen_covey',33,'FaveIdx')">Stephen Covey</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/e_e_cummings" onClick="authorCl('<?php echo HOME_URL; ?>a/e_e_cummings',34,'FaveIdx')">e. e. cummings</a>

</div>
<div style="padding-top:5px">

</div>
</div>
<div class="bq_s">
<h2><a href="/quotes/d">D Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/salvador_dali" onClick="authorCl('<?php echo HOME_URL; ?>a/salvador_dali',1,'FaveIdx')">Salvador Dali</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/rodney_dangerfield" onClick="authorCl('<?php echo HOME_URL; ?>a/rodney_dangerfield',2,'FaveIdx')">Rodney Dangerfield</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/charles_darwin" onClick="authorCl('<?php echo HOME_URL; ?>a/charles_darwin',3,'FaveIdx')">Charles Darwin</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/richard_dawkins" onClick="authorCl('<?php echo HOME_URL; ?>a/richard_dawkins',4,'FaveIdx')">Richard Dawkins</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/james_dean" onClick="authorCl('<?php echo HOME_URL; ?>a/james_dean',5,'FaveIdx')">James Dean</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/ellen_degeneres" onClick="authorCl('<?php echo HOME_URL; ?>a/ellen_degeneres',6,'FaveIdx')">Ellen DeGeneres</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/democritus" onClick="authorCl('<?php echo HOME_URL; ?>a/democritus',7,'FaveIdx')">Democritus</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/johnny_depp" onClick="authorCl('<?php echo HOME_URL; ?>a/johnny_depp',8,'FaveIdx')">Johnny Depp</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/rene_descartes" onClick="authorCl('<?php echo HOME_URL; ?>a/rene_descartes',9,'FaveIdx')">Rene Descartes</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/john_dewey" onClick="authorCl('<?php echo HOME_URL; ?>a/john_dewey',10,'FaveIdx')">John Dewey</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/princess_diana" onClick="authorCl('<?php echo HOME_URL; ?>a/princess_diana',11,'FaveIdx')">Princess Diana</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/charles_dickens" onClick="authorCl('<?php echo HOME_URL; ?>a/charles_dickens',12,'FaveIdx')">Charles Dickens</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/emily_dickinson" onClick="authorCl('<?php echo HOME_URL; ?>a/emily_dickinson',13,'FaveIdx')">Emily Dickinson</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/marlene_dietrich" onClick="authorCl('<?php echo HOME_URL; ?>a/marlene_dietrich',14,'FaveIdx')">Marlene Dietrich</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/phyllis_diller" onClick="authorCl('<?php echo HOME_URL; ?>a/phyllis_diller',15,'FaveIdx')">Phyllis Diller</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/walt_disney" onClick="authorCl('<?php echo HOME_URL; ?>a/walt_disney',16,'FaveIdx')">Walt Disney</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/benjamin_disraeli" onClick="authorCl('<?php echo HOME_URL; ?>a/benjamin_disraeli',17,'FaveIdx')">Benjamin Disraeli</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/fyodor_dostoevsky" onClick="authorCl('<?php echo HOME_URL; ?>a/fyodor_dostoevsky',18,'FaveIdx')">Fyodor Dostoevsky</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/frederick_douglass" onClick="authorCl('<?php echo HOME_URL; ?>a/frederick_douglass',19,'FaveIdx')">Frederick Douglass</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/arthur_conan_doyle" onClick="authorCl('<?php echo HOME_URL; ?>a/arthur_conan_doyle',20,'FaveIdx')">Arthur Conan Doyle</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/peter_drucker" onClick="authorCl('<?php echo HOME_URL; ?>a/peter_drucker',21,'FaveIdx')">Peter Drucker</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/wayne_dyer" onClick="authorCl('<?php echo HOME_URL; ?>a/wayne_dyer',22,'FaveIdx')">Wayne Dyer</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/henry_van_dyke" onClick="authorCl('<?php echo HOME_URL; ?>a/henry_van_dyke',23,'FaveIdx')">Henry Van Dyke</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/bob_dylan" onClick="authorCl('<?php echo HOME_URL; ?>a/bob_dylan',24,'FaveIdx')">Bob Dylan</a>

</div>
<div style="padding-top:5px">

</div>
</div>
<div class="bq_s">
<h2><a href="/quotes/e">E Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/clint_eastwood" onClick="authorCl('<?php echo HOME_URL; ?>a/clint_eastwood',1,'FaveIdx')">Clint Eastwood</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/meister_eckhart" onClick="authorCl('<?php echo HOME_URL; ?>a/meister_eckhart',2,'FaveIdx')">Meister Eckhart</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/thomas_a_edison" onClick="authorCl('<?php echo HOME_URL; ?>a/thomas_a_edison',3,'FaveIdx')">Thomas A. Edison</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/albert_einstein" onClick="authorCl('<?php echo HOME_URL; ?>a/albert_einstein',4,'FaveIdx')">Albert Einstein</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/dwight_d_eisenhower" onClick="authorCl('<?php echo HOME_URL; ?>a/dwight_d_eisenhower',5,'FaveIdx')">Dwight D. Eisenhower</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/george_eliot" onClick="authorCl('<?php echo HOME_URL; ?>a/george_eliot',6,'FaveIdx')">George Eliot</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/t_s_eliot" onClick="authorCl('<?php echo HOME_URL; ?>a/t_s_eliot',7,'FaveIdx')">T. S. Eliot</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/jim_elliot" onClick="authorCl('<?php echo HOME_URL; ?>a/jim_elliot',8,'FaveIdx')">Jim Elliot</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/albert_ellis" onClick="authorCl('<?php echo HOME_URL; ?>a/albert_ellis',9,'FaveIdx')">Albert Ellis</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/ralph_waldo_emerson" onClick="authorCl('<?php echo HOME_URL; ?>a/ralph_waldo_emerson',10,'FaveIdx')">Ralph Waldo Emerson</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/eminem" onClick="authorCl('<?php echo HOME_URL; ?>a/eminem',11,'FaveIdx')">Eminem</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/epictetus" onClick="authorCl('<?php echo HOME_URL; ?>a/epictetus',12,'FaveIdx')">Epictetus</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/epicurus" onClick="authorCl('<?php echo HOME_URL; ?>a/epicurus',13,'FaveIdx')">Epicurus</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/desiderius_erasmus" onClick="authorCl('<?php echo HOME_URL; ?>a/desiderius_erasmus',14,'FaveIdx')">Desiderius Erasmus</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/euripides" onClick="authorCl('<?php echo HOME_URL; ?>a/euripides',15,'FaveIdx')">Euripides</a>
</div>
<div style="padding-top:5px">

</div>
</div>
<div class="bq_s">
<h2><a href="/quotes/f">F Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/william_feather" onClick="authorCl('<?php echo HOME_URL; ?>a/william_feather',1,'FaveIdx')">William Feather</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/w_c_fields" onClick="authorCl('<?php echo HOME_URL; ?>a/w_c_fields',2,'FaveIdx')">W. C. Fields</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/harvey_fierstein" onClick="authorCl('<?php echo HOME_URL; ?>a/harvey_fierstein',3,'FaveIdx')">Harvey Fierstein</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/f_scott_fitzgerald" onClick="authorCl('<?php echo HOME_URL; ?>a/f_scott_fitzgerald',4,'FaveIdx')">F. Scott Fitzgerald</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/malcolm_forbes" onClick="authorCl('<?php echo HOME_URL; ?>a/malcolm_forbes',5,'FaveIdx')">Malcolm Forbes</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/henry_ford" onClick="authorCl('<?php echo HOME_URL; ?>a/henry_ford',6,'FaveIdx')">Henry Ford</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/e_m_forster" onClick="authorCl('<?php echo HOME_URL; ?>a/e_m_forster',7,'FaveIdx')">E. M. Forster</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/anatole_france" onClick="authorCl('<?php echo HOME_URL; ?>a/anatole_france',8,'FaveIdx')">Anatole France</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/anne_frank" onClick="authorCl('<?php echo HOME_URL; ?>a/anne_frank',9,'FaveIdx')">Anne Frank</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/viktor_e_frankl" onClick="authorCl('<?php echo HOME_URL; ?>a/viktor_e_frankl',10,'FaveIdx')">Viktor E. Frankl</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/benjamin_franklin" onClick="authorCl('<?php echo HOME_URL; ?>a/benjamin_franklin',11,'FaveIdx')">Benjamin Franklin</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/sigmund_freud" onClick="authorCl('<?php echo HOME_URL; ?>a/sigmund_freud',12,'FaveIdx')">Sigmund Freud</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/milton_friedman" onClick="authorCl('<?php echo HOME_URL; ?>a/milton_friedman',13,'FaveIdx')">Milton Friedman</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/erich_fromm" onClick="authorCl('<?php echo HOME_URL; ?>a/erich_fromm',14,'FaveIdx')">Erich Fromm</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/robert_frost" onClick="authorCl('<?php echo HOME_URL; ?>a/robert_frost',15,'FaveIdx')">Robert Frost</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/r_buckminster_fuller" onClick="authorCl('<?php echo HOME_URL; ?>a/r_buckminster_fuller',16,'FaveIdx')">R. Buckminster Fuller</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/thomas_fuller" onClick="authorCl('<?php echo HOME_URL; ?>a/thomas_fuller',17,'FaveIdx')">Thomas Fuller</a>
</div>
<div style="padding-top:5px">

</div>
</div>
<div class="bq_s">
<h2><a href="/quotes/g">G Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/john_kenneth_galbraith" onClick="authorCl('<?php echo HOME_URL; ?>a/john_kenneth_galbraith',1,'FaveIdx')">John Kenneth Galbraith</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/galileo_galilei" onClick="authorCl('<?php echo HOME_URL; ?>a/galileo_galilei',2,'FaveIdx')">Galileo Galilei</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/indira_gandhi" onClick="authorCl('<?php echo HOME_URL; ?>a/indira_gandhi',3,'FaveIdx')">Indira Gandhi</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/mahatma_gandhi" onClick="authorCl('<?php echo HOME_URL; ?>a/mahatma_gandhi',4,'FaveIdx')">Mahatma Gandhi</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/john_w_gardner" onClick="authorCl('<?php echo HOME_URL; ?>a/john_w_gardner',5,'FaveIdx')">John W. Gardner</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/judy_garland" onClick="authorCl('<?php echo HOME_URL; ?>a/judy_garland',6,'FaveIdx')">Judy Garland</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/marcus_garvey" onClick="authorCl('<?php echo HOME_URL; ?>a/marcus_garvey',7,'FaveIdx')">Marcus Garvey</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/bill_gates" onClick="authorCl('<?php echo HOME_URL; ?>a/bill_gates',8,'FaveIdx')">Bill Gates</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/khalil_gibran" onClick="authorCl('<?php echo HOME_URL; ?>a/khalil_gibran',9,'FaveIdx')">Khalil Gibran</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/andre_gide" onClick="authorCl('<?php echo HOME_URL; ?>a/andre_gide',10,'FaveIdx')">Andre Gide</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/william_e_gladstone" onClick="authorCl('<?php echo HOME_URL; ?>a/william_e_gladstone',11,'FaveIdx')">William E. Gladstone</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/arnold_h_glasow" onClick="authorCl('<?php echo HOME_URL; ?>a/arnold_h_glasow',12,'FaveIdx')">Arnold H. Glasow</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/johann_wolfgang_von_goeth" onClick="authorCl('<?php echo HOME_URL; ?>a/johann_wolfgang_von_goeth',13,'FaveIdx')">Johann Wolfgang von Goethe</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/vincent_van_gogh" onClick="authorCl('<?php echo HOME_URL; ?>a/vincent_van_gogh',14,'FaveIdx')">Vincent Van Gogh</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/baltasar_gracian" onClick="authorCl('<?php echo HOME_URL; ?>a/baltasar_gracian',15,'FaveIdx')">Baltasar Gracian</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/billy_graham" onClick="authorCl('<?php echo HOME_URL; ?>a/billy_graham',16,'FaveIdx')">Billy Graham</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/cary_grant" onClick="authorCl('<?php echo HOME_URL; ?>a/cary_grant',17,'FaveIdx')">Cary Grant</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/che_guevara" onClick="authorCl('<?php echo HOME_URL; ?>a/che_guevara',18,'FaveIdx')">Che Guevara</a>

</div>
<div style="padding-top:5px">

</div>
</div>
</div>
<div class="span4">
<div class="bq_s">
<h2><a href="/quotes/h">H Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/buddy_hackett" onClick="authorCl('<?php echo HOME_URL; ?>a/buddy_hackett',1,'FaveIdx')">Buddy Hackett</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/walter_hagen" onClick="authorCl('<?php echo HOME_URL; ?>a/walter_hagen',2,'FaveIdx')">Walter Hagen</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/robert_hall" onClick="authorCl('<?php echo HOME_URL; ?>a/robert_hall',3,'FaveIdx')">Robert Hall</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/alexander_hamilton" onClick="authorCl('<?php echo HOME_URL; ?>a/alexander_hamilton',4,'FaveIdx')">Alexander Hamilton</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/stephen_hawking" onClick="authorCl('<?php echo HOME_URL; ?>a/stephen_hawking',5,'FaveIdx')">Stephen Hawking</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/nathaniel_hawthorne" onClick="authorCl('<?php echo HOME_URL; ?>a/nathaniel_hawthorne',6,'FaveIdx')">Nathaniel Hawthorne</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/helen_hayes" onClick="authorCl('<?php echo HOME_URL; ?>a/helen_hayes',7,'FaveIdx')">Helen Hayes</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/william_hazlitt" onClick="authorCl('<?php echo HOME_URL; ?>a/william_hazlitt',8,'FaveIdx')">William Hazlitt</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/mitch_hedberg" onClick="authorCl('<?php echo HOME_URL; ?>a/mitch_hedberg',9,'FaveIdx')">Mitch Hedberg</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/robert_a_heinlein" onClick="authorCl('<?php echo HOME_URL; ?>a/robert_a_heinlein',10,'FaveIdx')">Robert A. Heinlein</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/ernest_hemingway" onClick="authorCl('<?php echo HOME_URL; ?>a/ernest_hemingway',11,'FaveIdx')">Ernest Hemingway</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/jimi_hendrix" onClick="authorCl('<?php echo HOME_URL; ?>a/jimi_hendrix',12,'FaveIdx')">Jimi Hendrix</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/matthew_henry" onClick="authorCl('<?php echo HOME_URL; ?>a/matthew_henry',13,'FaveIdx')">Matthew Henry</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/patrick_henry" onClick="authorCl('<?php echo HOME_URL; ?>a/patrick_henry',14,'FaveIdx')">Patrick Henry</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/audrey_hepburn" onClick="authorCl('<?php echo HOME_URL; ?>a/audrey_hepburn',15,'FaveIdx')">Audrey Hepburn</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/katharine_hepburn" onClick="authorCl('<?php echo HOME_URL; ?>a/katharine_hepburn',16,'FaveIdx')">Katharine Hepburn</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/heraclitus" onClick="authorCl('<?php echo HOME_URL; ?>a/heraclitus',17,'FaveIdx')">Heraclitus</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/oliver_herford" onClick="authorCl('<?php echo HOME_URL; ?>a/oliver_herford',18,'FaveIdx')">Oliver Herford</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/napoleon_hill" onClick="authorCl('<?php echo HOME_URL; ?>a/napoleon_hill',19,'FaveIdx')">Napoleon Hill</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/lou_holtz" onClick="authorCl('<?php echo HOME_URL; ?>a/lou_holtz',20,'FaveIdx')">Lou Holtz</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/homer" onClick="authorCl('<?php echo HOME_URL; ?>a/homer',21,'FaveIdx')">Homer</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/horace" onClick="authorCl('<?php echo HOME_URL; ?>a/horace',22,'FaveIdx')">Horace</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/elbert_hubbard" onClick="authorCl('<?php echo HOME_URL; ?>a/elbert_hubbard',23,'FaveIdx')">Elbert Hubbard</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/victor_hugo" onClick="authorCl('<?php echo HOME_URL; ?>a/victor_hugo',24,'FaveIdx')">Victor Hugo</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/hubert_h_humphrey" onClick="authorCl('<?php echo HOME_URL; ?>a/hubert_h_humphrey',25,'FaveIdx')">Hubert H. Humphrey</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/zora_neale_hurston" onClick="authorCl('<?php echo HOME_URL; ?>a/zora_neale_hurston',26,'FaveIdx')">Zora Neale Hurston</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/aldous_huxley" onClick="authorCl('<?php echo HOME_URL; ?>a/aldous_huxley',27,'FaveIdx')">Aldous Huxley</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/thomas_huxley" onClick="authorCl('<?php echo HOME_URL; ?>a/thomas_huxley',28,'FaveIdx')">Thomas Huxley</a>
</div>
<div style="padding-top:5px">

</div>
</div>
<div class="bq_s">
<h2><a href="/quotes/i">I Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/robert_green_ingersoll" onClick="authorCl('<?php echo HOME_URL; ?>a/robert_green_ingersoll',1,'FaveIdx')">Robert Green Ingersoll</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/washington_irving" onClick="authorCl('<?php echo HOME_URL; ?>a/washington_irving',2,'FaveIdx')">Washington Irving</a>
</div>
<div style="padding-top:5px">

</div>
</div>
<div class="bq_s">
<h2><a href="/quotes/j">J Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/michael_jackson" onClick="authorCl('<?php echo HOME_URL; ?>a/michael_jackson',1,'FaveIdx')">Michael Jackson</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/william_james" onClick="authorCl('<?php echo HOME_URL; ?>a/william_james',2,'FaveIdx')">William James</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/thomas_jefferson" onClick="authorCl('<?php echo HOME_URL; ?>a/thomas_jefferson',3,'FaveIdx')">Thomas Jefferson</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/steve_jobs" onClick="authorCl('<?php echo HOME_URL; ?>a/steve_jobs',4,'FaveIdx')">Steve Jobs</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/pope_john_xxiii" onClick="authorCl('<?php echo HOME_URL; ?>a/pope_john_xxiii',5,'FaveIdx')">Pope John XXIII</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/samuel_johnson" onClick="authorCl('<?php echo HOME_URL; ?>a/samuel_johnson',6,'FaveIdx')">Samuel Johnson</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/angelina_jolie" onClick="authorCl('<?php echo HOME_URL; ?>a/angelina_jolie',7,'FaveIdx')">Angelina Jolie</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/michael_jordan" onClick="authorCl('<?php echo HOME_URL; ?>a/michael_jordan',8,'FaveIdx')">Michael Jordan</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/chief_joseph" onClick="authorCl('<?php echo HOME_URL; ?>a/chief_joseph',9,'FaveIdx')">Chief Joseph</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/joseph_joubert" onClick="authorCl('<?php echo HOME_URL; ?>a/joseph_joubert',10,'FaveIdx')">Joseph Joubert</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/carl_jung" onClick="authorCl('<?php echo HOME_URL; ?>a/carl_jung',11,'FaveIdx')">Carl Jung</a>

</div>
<div style="padding-top:5px">

</div>
</div>
<div class="bq_s">
<h2><a href="/quotes/k">K Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/franz_kafka" onClick="authorCl('<?php echo HOME_URL; ?>a/franz_kafka',1,'FaveIdx')">Franz Kafka</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/immanuel_kant" onClick="authorCl('<?php echo HOME_URL; ?>a/immanuel_kant',2,'FaveIdx')">Immanuel Kant</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/john_keats" onClick="authorCl('<?php echo HOME_URL; ?>a/john_keats',3,'FaveIdx')">John Keats</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/helen_keller" onClick="authorCl('<?php echo HOME_URL; ?>a/helen_keller',4,'FaveIdx')">Helen Keller</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/john_f_kennedy" onClick="authorCl('<?php echo HOME_URL; ?>a/john_f_kennedy',5,'FaveIdx')">John F. Kennedy</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/robert_kennedy" onClick="authorCl('<?php echo HOME_URL; ?>a/robert_kennedy',6,'FaveIdx')">Robert Kennedy</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/rose_kennedy" onClick="authorCl('<?php echo HOME_URL; ?>a/rose_kennedy',7,'FaveIdx')">Rose Kennedy</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/omar_khayyam" onClick="authorCl('<?php echo HOME_URL; ?>a/omar_khayyam',8,'FaveIdx')">Omar Khayyam</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/victor_kiam" onClick="authorCl('<?php echo HOME_URL; ?>a/victor_kiam',9,'FaveIdx')">Victor Kiam</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/soren_kierkegaard" onClick="authorCl('<?php echo HOME_URL; ?>a/soren_kierkegaard',10,'FaveIdx')">Soren Kierkegaard</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/martin_luther_king_jr" onClick="authorCl('<?php echo HOME_URL; ?>a/martin_luther_king_jr',11,'FaveIdx')">Martin Luther King, Jr.</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/stephen_king" onClick="authorCl('<?php echo HOME_URL; ?>a/stephen_king',12,'FaveIdx')">Stephen King</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/rudyard_kipling" onClick="authorCl('<?php echo HOME_URL; ?>a/rudyard_kipling',13,'FaveIdx')">Rudyard Kipling</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/henry_a_kissinger" onClick="authorCl('<?php echo HOME_URL; ?>a/henry_a_kissinger',14,'FaveIdx')">Henry A. Kissinger</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/robert_kiyosaki" onClick="authorCl('<?php echo HOME_URL; ?>a/robert_kiyosaki',15,'FaveIdx')">Robert Kiyosaki</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/jiddu_krishnamurti" onClick="authorCl('<?php echo HOME_URL; ?>a/jiddu_krishnamurti',16,'FaveIdx')">Jiddu Krishnamurti</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/elisabeth_kublerross" onClick="authorCl('<?php echo HOME_URL; ?>a/elisabeth_kublerross',17,'FaveIdx')">Elisabeth Kubler-Ross</a>
</div>
<div style="padding-top:5px">

</div>
</div>
<div class="bq_s">
<h2><a href="/quotes/l">L Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/louis_lamour" onClick="authorCl('<?php echo HOME_URL; ?>a/louis_lamour',1,'FaveIdx')">Louis L'Amour</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/dalai_lama" onClick="authorCl('<?php echo HOME_URL; ?>a/dalai_lama',2,'FaveIdx')">Dalai Lama</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/charles_lamb" onClick="authorCl('<?php echo HOME_URL; ?>a/charles_lamb',3,'FaveIdx')">Charles Lamb</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/fran_lebowitz" onClick="authorCl('<?php echo HOME_URL; ?>a/fran_lebowitz',4,'FaveIdx')">Fran Lebowitz</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/bruce_lee" onClick="authorCl('<?php echo HOME_URL; ?>a/bruce_lee',5,'FaveIdx')">Bruce Lee</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/robert_e_lee" onClick="authorCl('<?php echo HOME_URL; ?>a/robert_e_lee',6,'FaveIdx')">Robert E. Lee</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/vladimir_lenin" onClick="authorCl('<?php echo HOME_URL; ?>a/vladimir_lenin',7,'FaveIdx')">Vladimir Lenin</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/john_lennon" onClick="authorCl('<?php echo HOME_URL; ?>a/john_lennon',8,'FaveIdx')">John Lennon</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/c_s_lewis" onClick="authorCl('<?php echo HOME_URL; ?>a/c_s_lewis',9,'FaveIdx')">C. S. Lewis</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/abraham_lincoln" onClick="authorCl('<?php echo HOME_URL; ?>a/abraham_lincoln',10,'FaveIdx')">Abraham Lincoln</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/anne_morrow_lindbergh" onClick="authorCl('<?php echo HOME_URL; ?>a/anne_morrow_lindbergh',11,'FaveIdx')">Anne Morrow Lindbergh</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/charles_lindbergh" onClick="authorCl('<?php echo HOME_URL; ?>a/charles_lindbergh',12,'FaveIdx')">Charles Lindbergh</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/john_locke" onClick="authorCl('<?php echo HOME_URL; ?>a/john_locke',13,'FaveIdx')">John Locke</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/vince_lombardi" onClick="authorCl('<?php echo HOME_URL; ?>a/vince_lombardi',14,'FaveIdx')">Vince Lombardi</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/henry_wadsworth_longfello" onClick="authorCl('<?php echo HOME_URL; ?>a/henry_wadsworth_longfello',15,'FaveIdx')">Henry Wadsworth Longfellow</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/h_p_lovecraft" onClick="authorCl('<?php echo HOME_URL; ?>a/h_p_lovecraft',16,'FaveIdx')">H. P. Lovecraft</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/james_russell_lowell" onClick="authorCl('<?php echo HOME_URL; ?>a/james_russell_lowell',17,'FaveIdx')">James Russell Lowell</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/john_lubbock" onClick="authorCl('<?php echo HOME_URL; ?>a/john_lubbock',18,'FaveIdx')">John Lubbock</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/lucretius" onClick="authorCl('<?php echo HOME_URL; ?>a/lucretius',19,'FaveIdx')">Lucretius</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/martin_luther" onClick="authorCl('<?php echo HOME_URL; ?>a/martin_luther',20,'FaveIdx')">Martin Luther</a>

</div>
<div style="padding-top:5px">

</div>
</div>
<div class="bq_s">
<h2><a href="/quotes/m">M Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/douglas_macarthur" onClick="authorCl('<?php echo HOME_URL; ?>a/douglas_macarthur',1,'FaveIdx')">Douglas MacArthur</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/niccolo_machiavelli" onClick="authorCl('<?php echo HOME_URL; ?>a/niccolo_machiavelli',2,'FaveIdx')">Niccolo Machiavelli</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/james_madison" onClick="authorCl('<?php echo HOME_URL; ?>a/james_madison',3,'FaveIdx')">James Madison</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/malcolm_x" onClick="authorCl('<?php echo HOME_URL; ?>a/malcolm_x',4,'FaveIdx')">Malcolm X</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/nelson_mandela" onClick="authorCl('<?php echo HOME_URL; ?>a/nelson_mandela',5,'FaveIdx')">Nelson Mandela</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/og_mandino" onClick="authorCl('<?php echo HOME_URL; ?>a/og_mandino',6,'FaveIdx')">Og Mandino</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/marilyn_manson" onClick="authorCl('<?php echo HOME_URL; ?>a/marilyn_manson',7,'FaveIdx')">Marilyn Manson</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/orison_swett_marden" onClick="authorCl('<?php echo HOME_URL; ?>a/orison_swett_marden',8,'FaveIdx')">Orison Swett Marden</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/bob_marley" onClick="authorCl('<?php echo HOME_URL; ?>a/bob_marley',9,'FaveIdx')">Bob Marley</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/ralph_marston" onClick="authorCl('<?php echo HOME_URL; ?>a/ralph_marston',10,'FaveIdx')">Ralph Marston</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/steve_martin" onClick="authorCl('<?php echo HOME_URL; ?>a/steve_martin',11,'FaveIdx')">Steve Martin</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/groucho_marx" onClick="authorCl('<?php echo HOME_URL; ?>a/groucho_marx',12,'FaveIdx')">Groucho Marx</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/karl_marx" onClick="authorCl('<?php echo HOME_URL; ?>a/karl_marx',13,'FaveIdx')">Karl Marx</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/abraham_maslow" onClick="authorCl('<?php echo HOME_URL; ?>a/abraham_maslow',14,'FaveIdx')">Abraham Maslow</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/w_somerset_maugham" onClick="authorCl('<?php echo HOME_URL; ?>a/w_somerset_maugham',15,'FaveIdx')">W. Somerset Maugham</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/andre_maurois" onClick="authorCl('<?php echo HOME_URL; ?>a/andre_maurois',16,'FaveIdx')">Andre Maurois</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/john_c_maxwell" onClick="authorCl('<?php echo HOME_URL; ?>a/john_c_maxwell',17,'FaveIdx')">John C. Maxwell</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/mignon_mclaughlin" onClick="authorCl('<?php echo HOME_URL; ?>a/mignon_mclaughlin',18,'FaveIdx')">Mignon McLaughlin</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/margaret_mead" onClick="authorCl('<?php echo HOME_URL; ?>a/margaret_mead',19,'FaveIdx')">Margaret Mead</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/bernard_meltzer" onClick="authorCl('<?php echo HOME_URL; ?>a/bernard_meltzer',20,'FaveIdx')">Bernard Meltzer</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/mencius" onClick="authorCl('<?php echo HOME_URL; ?>a/mencius',21,'FaveIdx')">Mencius</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/h_l_mencken" onClick="authorCl('<?php echo HOME_URL; ?>a/h_l_mencken',22,'FaveIdx')">H. L. Mencken</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/thomas_merton" onClick="authorCl('<?php echo HOME_URL; ?>a/thomas_merton',23,'FaveIdx')">Thomas Merton</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/michelangelo" onClick="authorCl('<?php echo HOME_URL; ?>a/michelangelo',24,'FaveIdx')">Michelangelo</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/henry_miller" onClick="authorCl('<?php echo HOME_URL; ?>a/henry_miller',25,'FaveIdx')">Henry Miller</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/a_a_milne" onClick="authorCl('<?php echo HOME_URL; ?>a/a_a_milne',26,'FaveIdx')">A. A. Milne</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/wilson_mizner" onClick="authorCl('<?php echo HOME_URL; ?>a/wilson_mizner',27,'FaveIdx')">Wilson Mizner</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/moliere" onClick="authorCl('<?php echo HOME_URL; ?>a/moliere',28,'FaveIdx')">Moliere</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/marilyn_monroe" onClick="authorCl('<?php echo HOME_URL; ?>a/marilyn_monroe',29,'FaveIdx')">Marilyn Monroe</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/michel_de_montaigne" onClick="authorCl('<?php echo HOME_URL; ?>a/michel_de_montaigne',30,'FaveIdx')">Michel de Montaigne</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/alfred_a_montapert" onClick="authorCl('<?php echo HOME_URL; ?>a/alfred_a_montapert',31,'FaveIdx')">Alfred A. Montapert</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/dwight_l_moody" onClick="authorCl('<?php echo HOME_URL; ?>a/dwight_l_moody',32,'FaveIdx')">Dwight L. Moody</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/christopher_morley" onClick="authorCl('<?php echo HOME_URL; ?>a/christopher_morley',33,'FaveIdx')">Christopher Morley</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/jim_morrison" onClick="authorCl('<?php echo HOME_URL; ?>a/jim_morrison',34,'FaveIdx')">Jim Morrison</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/john_muir" onClick="authorCl('<?php echo HOME_URL; ?>a/john_muir',35,'FaveIdx')">John Muir</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/max_muller" onClick="authorCl('<?php echo HOME_URL; ?>a/max_muller',36,'FaveIdx')">Max Muller</a>
</div>
<div style="padding-top:5px">

</div>
</div>
<div class="bq_s">
<h2><a href="/quotes/n">N Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/george_jean_nathan" onClick="authorCl('<?php echo HOME_URL; ?>a/george_jean_nathan',1,'FaveIdx')">George Jean Nathan</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/willie_nelson" onClick="authorCl('<?php echo HOME_URL; ?>a/willie_nelson',2,'FaveIdx')">Willie Nelson</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/isaac_newton" onClick="authorCl('<?php echo HOME_URL; ?>a/isaac_newton',3,'FaveIdx')">Isaac Newton</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/reinhold_niebuhr" onClick="authorCl('<?php echo HOME_URL; ?>a/reinhold_niebuhr',4,'FaveIdx')">Reinhold Niebuhr</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/friedrich_nietzsche" onClick="authorCl('<?php echo HOME_URL; ?>a/friedrich_nietzsche',5,'FaveIdx')">Friedrich Nietzsche</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/earl_nightingale" onClick="authorCl('<?php echo HOME_URL; ?>a/earl_nightingale',6,'FaveIdx')">Earl Nightingale</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/anais_nin" onClick="authorCl('<?php echo HOME_URL; ?>a/anais_nin',7,'FaveIdx')">Anais Nin</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/richard_m_nixon" onClick="authorCl('<?php echo HOME_URL; ?>a/richard_m_nixon',8,'FaveIdx')">Richard M. Nixon</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/henri_nouwen" onClick="authorCl('<?php echo HOME_URL; ?>a/henri_nouwen',9,'FaveIdx')">Henri Nouwen</a>

</div>
<div style="padding-top:5px">

</div>
</div>
<div class="bq_s">
<h2><a href="/quotes/o">O Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/carroll_oconnor" onClick="authorCl('<?php echo HOME_URL; ?>a/carroll_oconnor',1,'FaveIdx')">Carroll O'Connor</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/p_j_orourke" onClick="authorCl('<?php echo HOME_URL; ?>a/p_j_orourke',2,'FaveIdx')">P. J. O'Rourke</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/barack_obama" onClick="authorCl('<?php echo HOME_URL; ?>a/barack_obama',3,'FaveIdx')">Barack Obama</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/michelle_obama" onClick="authorCl('<?php echo HOME_URL; ?>a/michelle_obama',4,'FaveIdx')">Michelle Obama</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/aristotle_onassis" onClick="authorCl('<?php echo HOME_URL; ?>a/aristotle_onassis',5,'FaveIdx')">Aristotle Onassis</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/george_orwell" onClick="authorCl('<?php echo HOME_URL; ?>a/george_orwell',6,'FaveIdx')">George Orwell</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/joel_osteen" onClick="authorCl('<?php echo HOME_URL; ?>a/joel_osteen',7,'FaveIdx')">Joel Osteen</a>

</div>
<div style="padding-top:5px">

</div>
</div>
<div class="bq_s">
<h2><a href="/quotes/p">P Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/thomas_paine" onClick="authorCl('<?php echo HOME_URL; ?>a/thomas_paine',1,'FaveIdx')">Thomas Paine</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/chuck_palahniuk" onClick="authorCl('<?php echo HOME_URL; ?>a/chuck_palahniuk',2,'FaveIdx')">Chuck Palahniuk</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/gilbert_parker" onClick="authorCl('<?php echo HOME_URL; ?>a/gilbert_parker',3,'FaveIdx')">Gilbert Parker</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/rosa_parks" onClick="authorCl('<?php echo HOME_URL; ?>a/rosa_parks',4,'FaveIdx')">Rosa Parks</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/dolly_parton" onClick="authorCl('<?php echo HOME_URL; ?>a/dolly_parton',5,'FaveIdx')">Dolly Parton</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/blaise_pascal" onClick="authorCl('<?php echo HOME_URL; ?>a/blaise_pascal',6,'FaveIdx')">Blaise Pascal</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/saint_patrick" onClick="authorCl('<?php echo HOME_URL; ?>a/saint_patrick',7,'FaveIdx')">Saint Patrick</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/george_s_patton" onClick="authorCl('<?php echo HOME_URL; ?>a/george_s_patton',8,'FaveIdx')">George S. Patton</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/pope_john_paul_ii" onClick="authorCl('<?php echo HOME_URL; ?>a/pope_john_paul_ii',9,'FaveIdx')">Pope John Paul II</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/pope_paul_vi" onClick="authorCl('<?php echo HOME_URL; ?>a/pope_paul_vi',10,'FaveIdx')">Pope Paul VI</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/ron_paul" onClick="authorCl('<?php echo HOME_URL; ?>a/ron_paul',11,'FaveIdx')">Ron Paul</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/cesare_pavese" onClick="authorCl('<?php echo HOME_URL; ?>a/cesare_pavese',12,'FaveIdx')">Cesare Pavese</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/norman_vincent_peale" onClick="authorCl('<?php echo HOME_URL; ?>a/norman_vincent_peale',13,'FaveIdx')">Norman Vincent Peale</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/william_penn" onClick="authorCl('<?php echo HOME_URL; ?>a/william_penn',14,'FaveIdx')">William Penn</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/laurence_j_peter" onClick="authorCl('<?php echo HOME_URL; ?>a/laurence_j_peter',15,'FaveIdx')">Laurence J. Peter</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/pablo_picasso" onClick="authorCl('<?php echo HOME_URL; ?>a/pablo_picasso',16,'FaveIdx')">Pablo Picasso</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/plato" onClick="authorCl('<?php echo HOME_URL; ?>a/plato',17,'FaveIdx')">Plato</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/plutarch" onClick="authorCl('<?php echo HOME_URL; ?>a/plutarch',18,'FaveIdx')">Plutarch</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/edgar_allan_poe" onClick="authorCl('<?php echo HOME_URL; ?>a/edgar_allan_poe',19,'FaveIdx')">Edgar Allan Poe</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/roman_polanski" onClick="authorCl('<?php echo HOME_URL; ?>a/roman_polanski',20,'FaveIdx')">Roman Polanski</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/alexander_pope" onClick="authorCl('<?php echo HOME_URL; ?>a/alexander_pope',21,'FaveIdx')">Alexander Pope</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/colin_powell" onClick="authorCl('<?php echo HOME_URL; ?>a/colin_powell',22,'FaveIdx')">Colin Powell</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/steve_prefontaine" onClick="authorCl('<?php echo HOME_URL; ?>a/steve_prefontaine',23,'FaveIdx')">Steve Prefontaine</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/marcel_proust" onClick="authorCl('<?php echo HOME_URL; ?>a/marcel_proust',24,'FaveIdx')">Marcel Proust</a>
</div>
<div style="padding-top:5px">

</div>
</div>
</div>
</div>
</div>
</div>
<div class="span4">
 
<div class="bq_s">
<h2><a href="/quotes/q">Q Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/nido_qubein" onClick="authorCl('<?php echo HOME_URL; ?>a/nido_qubein',1,'FaveIdx')">Nido Qubein</a>
</div>
<div style="padding-top:5px">

</div>
</div>
<div class="bq_s">
<h2><a href="/quotes/r">R Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/ayn_rand" onClick="authorCl('<?php echo HOME_URL; ?>a/ayn_rand',1,'FaveIdx')">Ayn Rand</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/ronald_reagan" onClick="authorCl('<?php echo HOME_URL; ?>a/ronald_reagan',2,'FaveIdx')">Ronald Reagan</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/rainer_maria_rilke" onClick="authorCl('<?php echo HOME_URL; ?>a/rainer_maria_rilke',3,'FaveIdx')">Rainer Maria Rilke</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/joan_rivers" onClick="authorCl('<?php echo HOME_URL; ?>a/joan_rivers',4,'FaveIdx')">Joan Rivers</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/tony_robbins" onClick="authorCl('<?php echo HOME_URL; ?>a/tony_robbins',5,'FaveIdx')">Tony Robbins</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/francois_de_la_rochefouca" onClick="authorCl('<?php echo HOME_URL; ?>a/francois_de_la_rochefouca',6,'FaveIdx')">Francois de La Rochefoucauld</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/john_d_rockefeller" onClick="authorCl('<?php echo HOME_URL; ?>a/john_d_rockefeller',7,'FaveIdx')">John D. Rockefeller</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/will_rogers" onClick="authorCl('<?php echo HOME_URL; ?>a/will_rogers',8,'FaveIdx')">Will Rogers</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/jim_rohn" onClick="authorCl('<?php echo HOME_URL; ?>a/jim_rohn',9,'FaveIdx')">Jim Rohn</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/henry_rollins" onClick="authorCl('<?php echo HOME_URL; ?>a/henry_rollins',10,'FaveIdx')">Henry Rollins</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/mitt_romney" onClick="authorCl('<?php echo HOME_URL; ?>a/mitt_romney',11,'FaveIdx')">Mitt Romney</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/andy_rooney" onClick="authorCl('<?php echo HOME_URL; ?>a/andy_rooney',12,'FaveIdx')">Andy Rooney</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/eleanor_roosevelt" onClick="authorCl('<?php echo HOME_URL; ?>a/eleanor_roosevelt',13,'FaveIdx')">Eleanor Roosevelt</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/franklin_d_roosevelt" onClick="authorCl('<?php echo HOME_URL; ?>a/franklin_d_roosevelt',14,'FaveIdx')">Franklin D. Roosevelt</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/theodore_roosevelt" onClick="authorCl('<?php echo HOME_URL; ?>a/theodore_roosevelt',15,'FaveIdx')">Theodore Roosevelt</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/jeanjacques_rousseau" onClick="authorCl('<?php echo HOME_URL; ?>a/jeanjacques_rousseau',16,'FaveIdx')">Jean-Jacques Rousseau</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/rita_rudner" onClick="authorCl('<?php echo HOME_URL; ?>a/rita_rudner',17,'FaveIdx')">Rita Rudner</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/miguel_angel_ruiz" onClick="authorCl('<?php echo HOME_URL; ?>a/miguel_angel_ruiz',18,'FaveIdx')">Miguel Angel Ruiz</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/rumi" onClick="authorCl('<?php echo HOME_URL; ?>a/rumi',19,'FaveIdx')">Rumi</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/john_ruskin" onClick="authorCl('<?php echo HOME_URL; ?>a/john_ruskin',20,'FaveIdx')">John Ruskin</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/bertrand_russell" onClick="authorCl('<?php echo HOME_URL; ?>a/bertrand_russell',21,'FaveIdx')">Bertrand Russell</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/babe_ruth" onClick="authorCl('<?php echo HOME_URL; ?>a/babe_ruth',22,'FaveIdx')">Babe Ruth</a>
</div>
<div style="padding-top:5px">

</div>
</div>
<div class="bq_s">
<h2><a href="/quotes/s">S Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/carl_sagan" onClick="authorCl('<?php echo HOME_URL; ?>a/carl_sagan',1,'FaveIdx')">Carl Sagan</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/antoine_de_saintexupery" onClick="authorCl('<?php echo HOME_URL; ?>a/antoine_de_saintexupery',2,'FaveIdx')">Antoine de Saint-Exupery</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/carl_sandburg" onClick="authorCl('<?php echo HOME_URL; ?>a/carl_sandburg',3,'FaveIdx')">Carl Sandburg</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/george_santayana" onClick="authorCl('<?php echo HOME_URL; ?>a/george_santayana',4,'FaveIdx')">George Santayana</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/jeanpaul_sartre" onClick="authorCl('<?php echo HOME_URL; ?>a/jeanpaul_sartre',5,'FaveIdx')">Jean-Paul Sartre</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/arthur_schopenhauer" onClick="authorCl('<?php echo HOME_URL; ?>a/arthur_schopenhauer',6,'FaveIdx')">Arthur Schopenhauer</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/robert_h_schuller" onClick="authorCl('<?php echo HOME_URL; ?>a/robert_h_schuller',7,'FaveIdx')">Robert H. Schuller</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/charles_m_schulz" onClick="authorCl('<?php echo HOME_URL; ?>a/charles_m_schulz',8,'FaveIdx')">Charles M. Schulz</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/arnold_schwarzenegger" onClick="authorCl('<?php echo HOME_URL; ?>a/arnold_schwarzenegger',9,'FaveIdx')">Arnold Schwarzenegger</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/albert_schweitzer" onClick="authorCl('<?php echo HOME_URL; ?>a/albert_schweitzer',10,'FaveIdx')">Albert Schweitzer</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/walter_scott" onClick="authorCl('<?php echo HOME_URL; ?>a/walter_scott',11,'FaveIdx')">Walter Scott</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/jerry_seinfeld" onClick="authorCl('<?php echo HOME_URL; ?>a/jerry_seinfeld',12,'FaveIdx')">Jerry Seinfeld</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/lucius_annaeus_seneca" onClick="authorCl('<?php echo HOME_URL; ?>a/lucius_annaeus_seneca',13,'FaveIdx')">Lucius Annaeus Seneca</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/robert_w_service" onClick="authorCl('<?php echo HOME_URL; ?>a/robert_w_service',14,'FaveIdx')">Robert W. Service</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/dr_seuss" onClick="authorCl('<?php echo HOME_URL; ?>a/dr_seuss',15,'FaveIdx')">Dr. Seuss</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/william_shakespeare" onClick="authorCl('<?php echo HOME_URL; ?>a/william_shakespeare',16,'FaveIdx')">William Shakespeare</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/tupac_shakur" onClick="authorCl('<?php echo HOME_URL; ?>a/tupac_shakur',17,'FaveIdx')">Tupac Shakur</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/george_bernard_shaw" onClick="authorCl('<?php echo HOME_URL; ?>a/george_bernard_shaw',18,'FaveIdx')">George Bernard Shaw</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/charlie_sheen" onClick="authorCl('<?php echo HOME_URL; ?>a/charlie_sheen',19,'FaveIdx')">Charlie Sheen</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/frank_sinatra" onClick="authorCl('<?php echo HOME_URL; ?>a/frank_sinatra',20,'FaveIdx')">Frank Sinatra</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/isaac_bashevis_singer" onClick="authorCl('<?php echo HOME_URL; ?>a/isaac_bashevis_singer',21,'FaveIdx')">Isaac Bashevis Singer</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/lewis_b_smedes" onClick="authorCl('<?php echo HOME_URL; ?>a/lewis_b_smedes',22,'FaveIdx')">Lewis B. Smedes</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/will_smith" onClick="authorCl('<?php echo HOME_URL; ?>a/will_smith',23,'FaveIdx')">Will Smith</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/socrates" onClick="authorCl('<?php echo HOME_URL; ?>a/socrates',24,'FaveIdx')">Socrates</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/sophocles" onClick="authorCl('<?php echo HOME_URL; ?>a/sophocles',25,'FaveIdx')">Sophocles</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/thomas_sowell" onClick="authorCl('<?php echo HOME_URL; ?>a/thomas_sowell',26,'FaveIdx')">Thomas Sowell</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/charles_spurgeon" onClick="authorCl('<?php echo HOME_URL; ?>a/charles_spurgeon',27,'FaveIdx')">Charles Spurgeon</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/joseph_stalin" onClick="authorCl('<?php echo HOME_URL; ?>a/joseph_stalin',28,'FaveIdx')">Joseph Stalin</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/charles_stanley" onClick="authorCl('<?php echo HOME_URL; ?>a/charles_stanley',29,'FaveIdx')">Charles Stanley</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/john_steinbeck" onClick="authorCl('<?php echo HOME_URL; ?>a/john_steinbeck',30,'FaveIdx')">John Steinbeck</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/stendhal" onClick="authorCl('<?php echo HOME_URL; ?>a/stendhal',31,'FaveIdx')">Stendhal</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/robert_louis_stevenson" onClick="authorCl('<?php echo HOME_URL; ?>a/robert_louis_stevenson',32,'FaveIdx')">Robert Louis Stevenson</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/w_clement_stone" onClick="authorCl('<?php echo HOME_URL; ?>a/w_clement_stone',33,'FaveIdx')">W. Clement Stone</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/jonathan_swift" onClick="authorCl('<?php echo HOME_URL; ?>a/jonathan_swift',34,'FaveIdx')">Jonathan Swift</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/taylor_swift" onClick="authorCl('<?php echo HOME_URL; ?>a/taylor_swift',35,'FaveIdx')">Taylor Swift</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/charles_r_swindoll" onClick="authorCl('<?php echo HOME_URL; ?>a/charles_r_swindoll',36,'FaveIdx')">Charles R. Swindoll</a>
</div>
<div style="padding-top:5px">

</div>
</div>
<!-- <div class="bq_s">
<div id='div-gpt-ad-1348185546004-1' class="bq_ad_320x250  wsz" data-pid='3296'>
 
</div> 
</div> -->
<div class="bq_s">
<h2><a href="/quotes/t">T Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/rabindranath_tagore" onClick="authorCl('<?php echo HOME_URL; ?>a/rabindranath_tagore',1,'FaveIdx')">Rabindranath Tagore</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/elizabeth_taylor" onClick="authorCl('<?php echo HOME_URL; ?>a/elizabeth_taylor',2,'FaveIdx')">Elizabeth Taylor</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/tecumseh" onClick="authorCl('<?php echo HOME_URL; ?>a/tecumseh',3,'FaveIdx')">Tecumseh</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/alfred_lord_tennyson" onClick="authorCl('<?php echo HOME_URL; ?>a/alfred_lord_tennyson',4,'FaveIdx')">Alfred Lord Tennyson</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/mother_teresa" onClick="authorCl('<?php echo HOME_URL; ?>a/mother_teresa',5,'FaveIdx')">Mother Teresa</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/william_makepeace_thacker" onClick="authorCl('<?php echo HOME_URL; ?>a/william_makepeace_thacker',6,'FaveIdx')">William Makepeace Thackeray</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/margaret_thatcher" onClick="authorCl('<?php echo HOME_URL; ?>a/margaret_thatcher',7,'FaveIdx')">Margaret Thatcher</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/hunter_s_thompson" onClick="authorCl('<?php echo HOME_URL; ?>a/hunter_s_thompson',8,'FaveIdx')">Hunter S. Thompson</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/henry_david_thoreau" onClick="authorCl('<?php echo HOME_URL; ?>a/henry_david_thoreau',9,'FaveIdx')">Henry David Thoreau</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/paul_tillich" onClick="authorCl('<?php echo HOME_URL; ?>a/paul_tillich',10,'FaveIdx')">Paul Tillich</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/alexis_de_tocqueville" onClick="authorCl('<?php echo HOME_URL; ?>a/alexis_de_tocqueville',11,'FaveIdx')">Alexis de Tocqueville</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/j_r_r_tolkien" onClick="authorCl('<?php echo HOME_URL; ?>a/j_r_r_tolkien',12,'FaveIdx')">J. R. R. Tolkien</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/leo_tolstoy" onClick="authorCl('<?php echo HOME_URL; ?>a/leo_tolstoy',13,'FaveIdx')">Leo Tolstoy</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/lily_tomlin" onClick="authorCl('<?php echo HOME_URL; ?>a/lily_tomlin',14,'FaveIdx')">Lily Tomlin</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/brian_tracy" onClick="authorCl('<?php echo HOME_URL; ?>a/brian_tracy',15,'FaveIdx')">Brian Tracy</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/harry_s_truman" onClick="authorCl('<?php echo HOME_URL; ?>a/harry_s_truman',16,'FaveIdx')">Harry S. Truman</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/donald_trump" onClick="authorCl('<?php echo HOME_URL; ?>a/donald_trump',17,'FaveIdx')">Donald Trump</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/harriet_tubman" onClick="authorCl('<?php echo HOME_URL; ?>a/harriet_tubman',18,'FaveIdx')">Harriet Tubman</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/lana_turner" onClick="authorCl('<?php echo HOME_URL; ?>a/lana_turner',19,'FaveIdx')">Lana Turner</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/desmond_tutu" onClick="authorCl('<?php echo HOME_URL; ?>a/desmond_tutu',20,'FaveIdx')">Desmond Tutu</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/mark_twain" onClick="authorCl('<?php echo HOME_URL; ?>a/mark_twain',21,'FaveIdx')">Mark Twain</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/mike_tyson" onClick="authorCl('<?php echo HOME_URL; ?>a/mike_tyson',22,'FaveIdx')">Mike Tyson</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/lao_tzu" onClick="authorCl('<?php echo HOME_URL; ?>a/lao_tzu',23,'FaveIdx')">Lao Tzu</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/sun_tzu" onClick="authorCl('<?php echo HOME_URL; ?>a/sun_tzu',24,'FaveIdx')">Sun Tzu</a>

</div>
<div style="padding-top:5px">

</div>
</div>
<div class="bq_s">
<h2><a href="/quotes/u">U Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/morihei_ueshiba" onClick="authorCl('<?php echo HOME_URL; ?>a/morihei_ueshiba',1,'FaveIdx')">Morihei Ueshiba</a>
</div>
<div style="padding-top:5px">

</div>
</div>
<div class="bq_s">
<h2><a href="/quotes/v">V Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/jim_valvano" onClick="authorCl('<?php echo HOME_URL; ?>a/jim_valvano',1,'FaveIdx')">Jim Valvano</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/ken_venturi" onClick="authorCl('<?php echo HOME_URL; ?>a/ken_venturi',2,'FaveIdx')">Ken Venturi</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/gore_vidal" onClick="authorCl('<?php echo HOME_URL; ?>a/gore_vidal',3,'FaveIdx')">Gore Vidal</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/leonardo_da_vinci" onClick="authorCl('<?php echo HOME_URL; ?>a/leonardo_da_vinci',4,'FaveIdx')">Leonardo da Vinci</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/virgil" onClick="authorCl('<?php echo HOME_URL; ?>a/virgil',5,'FaveIdx')">Virgil</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/swami_vivekananda" onClick="authorCl('<?php echo HOME_URL; ?>a/swami_vivekananda',6,'FaveIdx')">Swami Vivekananda</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/voltaire" onClick="authorCl('<?php echo HOME_URL; ?>a/voltaire',7,'FaveIdx')">Voltaire</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/kurt_vonnegut" onClick="authorCl('<?php echo HOME_URL; ?>a/kurt_vonnegut',8,'FaveIdx')">Kurt Vonnegut</a>
</div>
<div style="padding-top:5px">

</div>
</div>
<div class="bq_s">
<h2><a href="/quotes/w">W Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/denis_waitley" onClick="authorCl('<?php echo HOME_URL; ?>a/denis_waitley',1,'FaveIdx')">Denis Waitley</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/alice_walker" onClick="authorCl('<?php echo HOME_URL; ?>a/alice_walker',2,'FaveIdx')">Alice Walker</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/william_arthur_ward" onClick="authorCl('<?php echo HOME_URL; ?>a/william_arthur_ward',3,'FaveIdx')">William Arthur Ward</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/andy_warhol" onClick="authorCl('<?php echo HOME_URL; ?>a/andy_warhol',4,'FaveIdx')">Andy Warhol</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/booker_t_washington" onClick="authorCl('<?php echo HOME_URL; ?>a/booker_t_washington',5,'FaveIdx')">Booker T. Washington</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/george_washington" onClick="authorCl('<?php echo HOME_URL; ?>a/george_washington',6,'FaveIdx')">George Washington</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/martha_washington" onClick="authorCl('<?php echo HOME_URL; ?>a/martha_washington',7,'FaveIdx')">Martha Washington</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/alan_watts" onClick="authorCl('<?php echo HOME_URL; ?>a/alan_watts',8,'FaveIdx')">Alan Watts</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/john_wayne" onClick="authorCl('<?php echo HOME_URL; ?>a/john_wayne',9,'FaveIdx')">John Wayne</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/lil_wayne" onClick="authorCl('<?php echo HOME_URL; ?>a/lil_wayne',10,'FaveIdx')">Lil Wayne</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/simone_weil" onClick="authorCl('<?php echo HOME_URL; ?>a/simone_weil',11,'FaveIdx')">Simone Weil</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/orson_welles" onClick="authorCl('<?php echo HOME_URL; ?>a/orson_welles',12,'FaveIdx')">Orson Welles</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/h_g_wells" onClick="authorCl('<?php echo HOME_URL; ?>a/h_g_wells',13,'FaveIdx')">H. G. Wells</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/mae_west" onClick="authorCl('<?php echo HOME_URL; ?>a/mae_west',14,'FaveIdx')">Mae West</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/e_b_white" onClick="authorCl('<?php echo HOME_URL; ?>a/e_b_white',15,'FaveIdx')">E. B. White</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/ron_white" onClick="authorCl('<?php echo HOME_URL; ?>a/ron_white',16,'FaveIdx')">Ron White</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/walt_whitman" onClick="authorCl('<?php echo HOME_URL; ?>a/walt_whitman',17,'FaveIdx')">Walt Whitman</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/oscar_wilde" onClick="authorCl('<?php echo HOME_URL; ?>a/oscar_wilde',18,'FaveIdx')">Oscar Wilde</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/tennessee_williams" onClick="authorCl('<?php echo HOME_URL; ?>a/tennessee_williams',19,'FaveIdx')">Tennessee Williams</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/marianne_williamson" onClick="authorCl('<?php echo HOME_URL; ?>a/marianne_williamson',20,'FaveIdx')">Marianne Williamson</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/oprah_winfrey" onClick="authorCl('<?php echo HOME_URL; ?>a/oprah_winfrey',21,'FaveIdx')">Oprah Winfrey</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/john_wooden" onClick="authorCl('<?php echo HOME_URL; ?>a/john_wooden',22,'FaveIdx')">John Wooden</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/virginia_woolf" onClick="authorCl('<?php echo HOME_URL; ?>a/virginia_woolf',23,'FaveIdx')">Virginia Woolf</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/william_wordsworth" onClick="authorCl('<?php echo HOME_URL; ?>a/william_wordsworth',24,'FaveIdx')">William Wordsworth</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/frank_lloyd_wright" onClick="authorCl('<?php echo HOME_URL; ?>a/frank_lloyd_wright',25,'FaveIdx')">Frank Lloyd Wright</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/steven_wright" onClick="authorCl('<?php echo HOME_URL; ?>a/steven_wright',26,'FaveIdx')">Steven Wright</a>

</div>
<div style="padding-top:5px">

</div>
</div>
<div class="bq_s">
<h2><a href="/quotes/y">Y Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/william_butler_yeats" onClick="authorCl('<?php echo HOME_URL; ?>a/william_butler_yeats',1,'FaveIdx')">William Butler Yeats</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/henny_youngman" onClick="authorCl('<?php echo HOME_URL; ?>a/henny_youngman',2,'FaveIdx')">Henny Youngman</a>
</div>
<div style="padding-top:5px">

</div>
</div>
<div class="bq_s">
<h2><a href="/quotes/z">Z Authors</a></h2>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/frank_zappa" onClick="authorCl('<?php echo HOME_URL; ?>a/frank_zappa',1,'FaveIdx')">Frank Zappa</a>
</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/zig_ziglar" onClick="authorCl('<?php echo HOME_URL; ?>a/zig_ziglar',2,'FaveIdx')">Zig Ziglar</a>

</div>
<div class="bqLn">
<a href="<?php echo HOME_URL; ?>a/emile_zola" onClick="authorCl('<?php echo HOME_URL; ?>a/emile_zola',3,'FaveIdx')">Emile Zola</a>
</div>
<div style="padding-top:5px">

</div>
</div>
<!-- <div class="bq_s">
 
<div id='div-gpt-ad-1348185546004-0' class="bq_ad_320x250  wsz" data-pid='3297'>
 
</div> 
</div> -->
</div>
</div>
</div>
<script type="text/javascript">
PF_ON = ((Math.random() * 100) < 0);

</script>
<div class="bq_bot_nav bq-no-print">
<div class="container">
<div class="row" style="color:white">
<table style="margin: 0 auto;">
<tr>
<td>