<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en" xmlns:fb="http://ogp.me/ns/fb#">
<head>
<title><?php echo $title; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php if(isset($description)) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } else { ?>
<meta name="description" content="<?php echo $title; ?>" />
<meta name="robots" content="index,follow" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php } ?>
<?php 
$keywords = $author." Quotes";
$topic = explode("@--@", $topics); 
foreach ($topic as $val) { 
$keywords = $keywords.",".$val;
}
?>

<meta name="keywords" content="<?php echo $keywords; ?>" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<link href="<?php echo base_url(); ?>asset/core-homepg.css" media="screen, print" type="text/css" rel="stylesheet"/>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-49389862-1', 'kquotes.com');
  ga('send', 'pageview');

</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=741736492538046";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

</head>
<body>
</div>
 
<div id="bq-tn" class="bq_tn_cntr"><div id="bq-top-nav" style="left:0px;">
<div class="slim navbar" style="margin-bottom: 0px">
<div class="navbar-inner navbar-pull-center" data-sai-excludehighlight="true">
<ul class="nav">
<li id="bq-nav-home-i">
<span style="padding-top:0px" onclick="javascript:window.location.href='/'">
&nbsp;&nbsp;</span>
</li>
<li class="active" id="sl-bq-nav-home-b">
<a class="btn btn-navbar bq-tn-btn" href="/">&nbsp;<span class="icon-home icon-white"></span>&nbsp;</a>
</li>

<li class="" id="sl-bq-nav-home-t"><a href="<?php echo base_url(); ?>" class="txnav">KQUOTES.COM</a></li>
<li class="" id="sl-bq-nav-home-t"><a href="<?php echo base_url(); ?>" class="txnav">Home</a></li>
<li class="" id="sl-bq-nav-home-t"><a href="<?php echo HOME_URL; ?>author" class="txnav">Authors</a></li>
<li class="" id="sl-bq-nav-home-t"><a href="<?php echo HOME_URL; ?>topics" class="txnav">Topics</a></li>

<!--<li><a href="/quotes/favorites" class="txnav">Authors</a></li>
<li><a href="/quotes/topics" class="txnav">Topics</a></li>
<li><a href="/quotes_of_the_day" class="txnav">Quote of the Day</a></li>
-->
<li id="sl-bq-nv-srch-f">

<form class="navbar-search bq-no-print pull-left" action="<?php echo HOME_URL; ?>search" method="get">

<input type="text" class="" name="q" value="<?php if(isset($_GET['q'])) { echo $_GET['q']; } else { echo 'Totally We Found 305062 Quotes On Web'; } ?>" id="q" style="width: 400px; margin-left: 125px;" onclick="this.value='';" onfocus="this.select()" onblur="this.value=!this.value?'Totally We Found 305062 Quotes On Web':this.value;" />

</form>

</li>
<li id="sl-bq-nv-srch-b">
<a class="btn btn-navbar bq-tn-btn" href="/search">&nbsp;<span class="icon-search icon-white"></span>&nbsp;</a>
</li>
</ul>
</div>
</div>
</div>