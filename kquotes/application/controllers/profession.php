<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profession extends CI_Controller {

	public function index()
	{

$my_img = imagecreate( 635, 400 );

$background = imagecolorallocate( $my_img, 0, 0, 0);
$text_colour = imagecolorallocate( $my_img, 255, 255, 255 );
$line_colour = imagecolorallocate( $my_img, 128, 255, 0 );

imagestring($my_img, 52, 20, 25, "I would quite like to create a fragrance for men though - something that I like.", $text_colour );

imagestring( $my_img, 16, 520, 375, "KQUOTES.COM", $text_colour );
imagesetthickness ( $my_img, 5 );
//imageline( $my_img, 30, 45, 165, 45, $line_colour );

header( "Content-type: image/png" );
imagepng( $my_img );
imagecolordeallocate( $line_color );
imagecolordeallocate( $text_color );
imagecolordeallocate( $background );
imagepng($my_img , "/home1/kquotes/public_html/asset/pic-quotes/1.png");
imagedestroy( $my_img );
	
	}
	
}

/*
<div class="span4">
<div class="bqLn"><a href="<?php echo base_url(); ?>p/activist">Activist</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/actor">Actor</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/actress">Actress</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/architect">Architect</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/artist">Artist</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/astronaut">Astronaut</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/athlete">Athlete</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/author">Author</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/aviator">Aviator</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/businessman">Businessman</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/businesswoman">Businesswoman</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/cartoonist">Cartoonist</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/celebrity">Celebrity</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/chef">Chef</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/clergyman">Clergyman</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/coach">Coach</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/comedian">Comedian</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/composer">Composer</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/criminal">Criminal</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/critic">Critic</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/dancer">Dancer</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/designer">Designer</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/diplomat">Diplomat</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/director">Director</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/dramatist">Dramatist</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/driver">Driver</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/economist">Economist</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/editor">Editor</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/educator">Educator</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/entertainer">Entertainer</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/environmentalist">Environmentalist</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/explorer">Explorer</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/firstlady">First Lady</a></div>
</div>
<div class="span4">
<div class="bqLn"><a href="<?php echo base_url(); ?>p/historian">Historian</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/inventor">Inventor</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/journalist">Journalist</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/judge">Judge</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/lawyer">Lawyer</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/leader">Leader</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/mathematician">Mathematician</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/model">Model</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/musician">Musician</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/novelist">Novelist</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/philosopher">Philosopher</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/photographer">Photographer</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/physicist">Physicist</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/playwright">Playwright</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/poet">Poet</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/politician">Politician</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/president">President</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/producer">Producer</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/psychologist">Psychologist</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/publicservant">Public Servant</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/publisher">Publisher</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/revolutionary">Revolutionary</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/royalty">Royalty</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/saint">Saint</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/scientist">Scientist</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/sculptor">Sculptor</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/sociologist">Sociologist</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/soldier">Soldier</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/statesman">Statesman</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/theologian">Theologian</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/vicepresident">Vice President</a></div>
<div class="bqLn"><a href="<?php echo base_url(); ?>p/writer">Writer</a></div>
</div>
</div>
*/