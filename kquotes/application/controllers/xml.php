<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xml extends CI_Controller {

	public function index()
	{
		for($id=1;$id<=33;$id++)
		{
		$my_file = 'asset/sitemap'.$id.'.xml';
		$handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file);
		$xml_data = '<?xml version="1.0" encoding="UTF-8"?>
		<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
		http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
		';
		
		$start = ($id-1)*10000;
		
		$data = $this->db->query("SELECT * FROM details limit ".$start.",10000");
		$data = $data->result_array();
		foreach($data as $value)
		{
		
			$xml_data .= '<url><loc>';
			$xml_data .= base_url()."q/".substr(url_title(strtolower($value['quote'])),0,100)."-".$value['id'];
			$xml_data .= '</loc></url>';
			
			
		}
		$xml_data .= "</urlset>";
		fwrite($handle, $xml_data);
		}
		
	
	}
	
}