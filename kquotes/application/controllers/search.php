<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller {

	public function index()
	{
		
		$this->load->library("pagination"); 

		$query = urlencode($_GET['q']);

		if(isset($_GET['p']) == "")
		{
			$page = 0;
		}
		else
		{
			$page = ($_GET['p']-1)*10;
		}
		$url = SOLR_URL."quotes/select?q=".$query."&wt=json&start=".$page."&rows=10";

		$data = @file_get_contents($url);
		$result = json_decode($data,true);
		$output['count'] = $result['response']['numFound'];
		$output['pageCount'] = round($result['response']['numFound'] / 10) + 1;

		if($result['response']['numFound'] == 0)
		{
			$output['result'] = 0;
		}
		else
		{
			$output['result'] = $result['response']['docs'];
		}

		$config["base_url"] = HOME_URL. "search/?q=".$query;
        $config["total_rows"] = $output['count'];
        $config["per_page"] = 10;
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['query_string_segment'] = 'p';

        $config['first_link'] = 'First';
	    $config['last_link'] = 'Last';
	    $config['anchor_class'] = ' class="page" ';
	    $config['full_tag_open'] = "<div class='npagination'>";
 		$config['full_tag_close'] = "</div>";
	    $config['cur_tag_open'] = '<span class="page active">';
	    $config['cur_tag_close'] = '</span>';

        $this->pagination->initialize($config);
        $output["links"] = $this->pagination->create_links();

        $header['title'] = $query." quotes";
		$header['topics'] = $query." quotes";
		$header['author'] = $query." quotes";
		$this->load->view('newheader',$header);
		$this->load->view('search',$output);
		$this->load->view('footer');
		
	}
}