<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Q extends CI_Controller {

	public function i()
	{ 
		if($this->uri->segment(2) == "i")
		{
		redirect(base_url().'q/'.$this->uri->segment(3), 'refresh');
		exit;
		}
		$query = $this->uri->segment(2);
		$output['query'] = $query;
		$c_query = explode("-", $query);
		
		if(isset($_GET['p']) == "")
		{
			$page = 0;
		}
		else
		{
			$page = ($_GET['p']-1)*10;
		}
		$url = SOLR_URL."quotes/select?q=*:*&fq=unique_id:".$c_query[count($c_query)-1]."&wt=json&start=".$page."&rows=10";

		$data = @file_get_contents($url);
		$result = json_decode($data,true);
		$output['count'] = $result['response']['numFound'];
		$output['pageCount'] = round($result['response']['numFound'] / 10) + 1;

		if($result['response']['numFound'] == 0)
		{
			$output['result'] = 0;
		}
		else
		{
			$output['result'] = $result['response']['docs'];
		} 
		
		$header['author'] = $output['result'][0]['author'];
		$author_count = strlen($header['author']." Quotes");
		$ac = 160 - $author_count;
		$header['title'] = substr($output['result'][0]['quote'],0,$ac)." - ".$header['author']." Quotes";
		$header['topics'] = $output['result'][0]['topics'];
		
		$count_related_url = SOLR_URL."quotes/select?q=".urlencode($output['result'][0]['author'])."&wt=json&start=0&rows=10";
		$count_related_data = file_get_contents($count_related_url);
		$count_related_result = json_decode($count_related_data ,true); 
		$related_count = round($count_related_result['response']['numFound']/10);
		
		$related_url = SOLR_URL."quotes/select?q=".urlencode($header['author'])."&wt=json&start=".rand(0,$related_count)."&rows=10";
		$related_data = file_get_contents($related_url);
		$related_result = json_decode($related_data,true); 
		$output['related_result'] = $related_result['response']['docs'];
		
		
		$related_author_url = SOLR_URL."quotes/select?q=*:*&fq=profession:".urlencode($output['result'][0]['profession'])."&wt=json&start=0&rows=100&group=true&group.field=author&fl=author";
		$related_author_data = file_get_contents($related_author_url);
		$related_author_result = json_decode($related_author_data ,true);
		$output['related_author'] = $related_author_result['grouped']['author']['groups'];

		$pic_file_name = $output['result'][0]['unique_id'];
		$pic_path = "asset/pic-quotes/";
		
		@mkdir($pic_path.substr($pic_file_name,0,1), 0777);
		@mkdir($pic_path.substr($pic_file_name,0,1)."/".substr($pic_file_name,1,1), 0777);
		
		$p_path = $output['result'][0]['unique_id'].".jpg";
		$path = "asset/pic-quotes/".substr($pic_file_name,0,1)."/".substr($pic_file_name,1,1)."/".$p_path;

		if(!file_exists($path))
		{
			/*
			-- Start Image Create Functionlaity ---
			*/
			$newtext = wordwrap($output['result'][0]['quote'], 75, "@--@");

			
			$bgid = rand(1,4);
			$image_path = "asset/pic-quotes/bg/".$bgid.".png";

			$img = 	imagecreatefrompng($image_path); 
					imagealphablending($img, true);
			       	imagesavealpha($img, true);

			$textColor = imagecolorallocate($img, 255, 255, 255);
			
			
			$line_colour = imagecolorallocate( $img, 128, 255, 0 );

			$font = 'asset/pic-quotes/fonts/angrybirds-regular.ttf';

			$data_text = explode("@--@", $newtext);

			if($bgid == 2)
			{
				$yAxis = 235;
			}
			else
			{
				$yAxis = 85;	
			}

			foreach ($data_text as $value) {

			imagettftext($img, 20, 0, 40, $yAxis,$textColor,$font, $value);

			$yAxis = $yAxis + 30;

			}
			$author = " - ".$output['result'][0]['author'];
			imagettftext($img, 20, 0, 350, $yAxis+45,$textColor,$font, $author);
			
			imagettftext($img, 20, 0, 310, 500,$textColor,$font, "www.kquotes.com");
			// imageline( $img, 210, 505, 735, 505, $line_colour );
			
			$image = imagejpeg($img, $path);
			imagedestroy($img);
			
			/*
			-- END IMAGE CREATE --
			*/
		}
		$output['image_path'] = $path;

		$this->load->view('newheader',$header);
		$this->load->view('single',$output);
		$this->load->view('footer');

	}

	public function substr_word($body,$maxlength){
    if (strlen($body)<$maxlength) return $body;
    $body = substr($body, 0, $maxlength);
    $rpos = strrpos($body,' ');
    if ($rpos>0) $body = substr($body, 0, $rpos);
    return $body;
}
}