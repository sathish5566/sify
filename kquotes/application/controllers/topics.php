<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Topics extends CI_Controller {

	public function index()
	{
		$header['title'] = "Favorite topic Quotes on Web - KQuotes";
		$header['description'] = "Collection of famous quotes by famous authors, celebrities. Share our Quotes to Facebook, Twitter, and blogs.";
		$header['keywords'] = "quote, quotes, quotation, quotations, famous, quotable, Love, Life, Friendship, Success, Wisdom";
		$this->load->view('main-header',$header);

		$this->load->view('all_topics');
		$this->load->view('footer');

	}
}