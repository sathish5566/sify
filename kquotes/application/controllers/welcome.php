<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{

		$url = SOLR_URL."quotes/select?q=*:*&wt=json&start=".rand(0,305062)."&rows=1";
		$data = @file_get_contents($url);
		$result = json_decode($data,true);
		$home['quote_at_the_moment'] = $result['response']['docs'][0];
		
		$this->load->view('home-header.php');
		$this->load->view('welcome_message',$home);
		$this->load->view('footer');
	}
}