<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Author extends CI_Controller {

	public function index()
	{
		$header['title'] = "Find Famous Author Quotes on Web - KQuotes";
		$header['description'] = "Collection of famous quotes by famous authors, celebrities. Share our Quotes to Facebook, Twitter, and blogs.";
		$header['keywords'] = "quote, quotes, quotation, quotations, famous, quotable, epigrams, proverbs, aphorisms, words, literature, speeches, writing, reference";

		$this->load->view('main-header',$header);
		$this->load->view('all_authors');
		$this->load->view('footer');

	}
}