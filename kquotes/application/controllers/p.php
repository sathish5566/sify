<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class P extends CI_Controller {

	public function i()
	{
		
		if($this->uri->segment(2) == "i")
		{
		redirect(base_url().'p/'.$this->uri->segment(3), 'refresh');
		exit;
		}
		
		$query = $this->uri->segment(2);
		
		if (strpos($query,'_') !== false) {
			$query = str_replace("_", "-", $query);
			redirect(base_url().'p/'.$query, 'refresh');
			exit;
		}
		
		$this->load->library("pagination"); 
		$query = str_replace("_", "+", $query);
		$query = str_replace("-", "+", $query);
		$query = str_replace(" ", "+", $query);

		if(isset($_GET['p']))
		{
			if($_GET['p'] == "")
			{
				$page = 0;
			}	
			else
			{
				$page = ($_GET['p']-1)*10;
			}
		}
		else
		{
				$page = 0;
		}
		$url = SOLR_URL."quotes/select?q=*:*&fq=profession:\"".$query."\"&wt=json&start=".$page."&rows=10";

		$data = @file_get_contents($url);
		$result = json_decode($data,true);
		$output['count'] = $result['response']['numFound'];
		
		if($output['count'] == 0)
		{
		
		$url = SOLR_URL."quotes/select?q=*:*&fq=profession:".$query."&wt=json&start=".$page."&rows=10";
		$data = @file_get_contents($url);
		$result = json_decode($data,true);
		$output['count'] = $result['response']['numFound'];
		
		}
		
		$output['pageCount'] = round($result['response']['numFound'] / 10) + 1;
		
		$config["base_url"] = base_url(). "p/".$query."?";
	        $config["total_rows"] = $output['count'];
	        $config["per_page"] = 10;
	        $config['page_query_string'] = TRUE;
	        $config['use_page_numbers'] = TRUE;
	        $config['query_string_segment'] = 'p';
	        
	    $config['first_link'] = 'First';
	    $config['last_link'] = 'Last';
	    $config['anchor_class'] = ' class="page" ';
	    $config['full_tag_open'] = "<div class='npagination'>";
 		$config['full_tag_close'] = "</div>";
	    $config['cur_tag_open'] = '<span class="page active">';
	    $config['cur_tag_close'] = '</span>';

		
		
	        $this->pagination->initialize($config);
	        
	        $output["links"] = $this->pagination->create_links();

		if($result['response']['numFound'] == 0)
		{
			$output['result'] = 0;
		}
		else
		{
			$output['result'] = $result['response']['docs'];
		}

		$output['query'] = str_replace("+"," ",$query);
		
		$header['title'] = $output['query']." quotes";
		$header['description'] = $header['title'];
		$header['topics'] = $output['query']." quotes";
		$header['author'] = $output['query']." quotes";
		
		$this->load->view('newheader',$header);
		$this->load->view('profession',$output);
		$this->load->view('footer');
	
	}
	
}