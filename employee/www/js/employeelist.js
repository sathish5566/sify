var serviceURL = "http://localhost/employee/services/";

var employees;

$('#employeeListPage').bind('pageinit', function(event) {
	alert('test');
	getEmployeeList();
});

function getEmployeeList() {
	$.getJSON(serviceURL + 'getemployees.php', function(data) {
		$('#employeeList li').remove();
		employees = data.items;
		$.each(employees, function(index, employee) {
			$('#employeeList').append('<li><a href="employeedetails.html?id=' + employee.id + '">' +
					'<img src="pics/' + employee.picture + '"/>' +
					'<h4>' + employee.firstName + ' ' + employee.lastName + '</h4>' +
					'<p>' + employee.title + '</p>' +
					'<span class="ui-li-count">' + employee.reportCount + '</span></a></li>');

			$('#employeeList2').append('<div class="ui-block-' + employee.id + '">' +
				'<img src="pics/' + employee.picture + '"/></div>');
		});
		$('#employeeList2').listview('refresh');
	});
}