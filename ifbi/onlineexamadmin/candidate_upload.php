<?
//$admin_flag = 0;
//new condition included on 10/25/05
session_start();
if ($_SESSION["admin_type"]== '0') {
	$admin_flag = 0;
} else if ($_SESSION["admin_type"]== '1'){
	$admin_flag = 1;
}
//exit;
require_once "sessionchk.php";
require("constants.inc");
require_once "dbconfig.php";
$header_candidate=array("CTR_CD","MEM_MEM_NO","G_1","MEM_NAM_1","MEM_ADR_1","MEM_ADR_2","MEM_ADR_3","MEM_ADR_4","MEM_ADR_5","MEM_ADR_6","MEM_PIN_CD","ZO","STATE","EXM_CD","SUB_CD","M_1","MEM_INS_CD","INS_INS_NAM_1"); 
//$header_candidate=array("CTR_CD","MEM_TYPE","MEM_MEM_NO","G_1","MEM_NAM_1","MEM_ADR_1","MEM_ADR_2","MEM_ADR_3","MEM_ADR_4","MEM_ADR_5","MEM_ADR_6","MEM_PIN_CD","ZO","STATE","EXM_CD","SUB_CD","M_1","MEM_INS_CD","INS_INS_NAM_1"); 
$validate1 = '/^[0-9]{1,11}$/i'; //examcode,subjectcode,sectioncode,question_id
$marks = '/^((0\.5)|1|2|3)$/i';//marks
$correctans = '/^[1-5]+/i';
$mcq_case='/^(Y|N)/i';
$email_flag_val='/^(Y|N)/i';
$type_case='/^(Y)/i';
$type_case_null='/^(N)/i';
$cmarks = '/^(5|10|15|20)$/i';//casemarks
$validation_alpha= '/^[a-zA-Z ]*$/i'; //alphabets
$member_nums='/^[a-z0-9]*$/i';//membernumber
$address='/^[a-z0-9//\]*$/i';
$code_medium='/^(E|H)/i';
$pincode_val='/^[1-9][0-9]{5}$/i';
//$mem_type_val = '/^[a-z0-9]{1,2}$/i';
include("includes/icicipru_functions.php");
include("includes/excelreader/reader.php");
$filepath="includes/candidate_list/";
$commondebug = false;
$flag	= isSet($_POST["flag"]) ? $_POST["flag"] : "";
$update_flag=0;
$validate=1;
$null=0;
if($flag == "update"){
$file = fileupload($_FILES,$filepath);


if($file!=""){
/**
*     Excel reader class usage starts here
*     Instantiating the Excel reader Class
*/


$data = new Spreadsheet_Excel_Reader();


/** setting the encoding format */
$data->setOutputEncoding('CP1251');

/** This will read from the excel file  */
$data->read($filepath.$file);


$no_of_sheets = count($data->sheets);

$sheetno=0;
$no_of_rows = $data->sheets[0]['numRows'];
$no_of_cols = $data->sheets[0]['numCols'];


if($commonDebug){
			echo $file."<br>";
			echo "ROWS:".$no_of_rows."COLS:".$no_of_cols."<br>";
		}
//echo "The no of cols ".$no_of_cols."<br>";
//echo "The no of rows ".$no_of_rows."<br>";

if(($no_of_cols ==0) || ($no_of_rows ==0)){
	$errormsg = "Error in Excel file. No Records Found";
}
else
{
if($no_of_cols !=18 || $no_of_sheets >1)
{
	if($no_of_cols !=18){
		$errormsg = "Error in Excel file(Should have 18 columns)";
		disp_error($error_msg);
	}
	else if($no_of_sheets >1){
		$errormsg = "Error in Excel file(Should not have more than one sheet)";
		disp_error($error_msg);
	}
}
else
{
	$header_disp=array();
	for($j=1;$j<=$no_of_rows;$j++)
	{
		for($k=1;$k<=$no_of_cols;$k++)
		{
			$cell_info = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
			if($j==1)
			{
				//echo $cell_info."<br>";
				$headercheck = header_candivalidate($k,$cell_info);
				$header_disp[]=$headercheck;
			}
			
		}
	}
	//echo "<pre>";
	//print_r($header_disp);
	//echo "</pre>";
	if (in_array('0',$header_disp))
	{
		$errormsg = "Error in Header";
		$headerflag=1;
	}

	else
	{
	$dup_memno='';
	for($j=2;$j<=$no_of_rows;$j++)
	{
		$exam_centre_code="";
		//$mem_type = "";
		$memno="";
		$gender="";
		$name="";
		$address1="";
		$address2="";
		$address3="";
		$address4="";
		$address5="";
		$address6="";
		$pincode="";
		$zone_code="";
		$state="";
		$exam_code="";
		$subject_code="";
		$medium="";
		$institution_code="";
		$institution_name="";
		$email_flag='';
		for($k=1;$k<=$no_of_cols;$k++)
		{
			if($k == 1){
				$exam_centre_code = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
					$i++;
			}
			//else if($k==2){
				//$mem_type = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
			//}
			else if($k==2){
				$memno = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
			}
			else if($k==3){
				$gender = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
			}
			else if($k==4){
				$name = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
			}
			else if($k==5){
				$address1 = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
			}
			else if($k==6){
				$address2 = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
			}
			else if($k==7){
				$address3 = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
			}
			else if($k==8){
				$address4 = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
			}
			else if($k==9){
				$address5 = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
			}
			else if($k==10){
				$address6 = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
			}
			else if($k==11){
				$pincode = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
			}
			else if($k==12){
				$zone_code = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
			}
			else if($k==13){
				$state_code = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
			}
			else if($k==14){
				$exam_code = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
			}
			else if($k==15){
				$subject_code = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
			}
			else if($k==16){
				$medium_code = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
			}
			else if($k==17){
				$institution_code = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
			}
			else if($k==18){
				$institution_name = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
			}
			/*else if($k==19){
				$email_flag = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
			}*/
		}//inner for loop
		
		//exit;
		if((trim($exam_centre_code) !="")&& (trim($name) !="")&&(trim($memno)!="")&&(trim($address1)!="")&&
			(trim($subject_code)!="")&&(trim($medium_code)!="")&&(trim($exam_code)!="")) 
		{
				$check=preg_match($member_nums,trim($subject_code)) && preg_match($member_nums,trim($exam_code));
				$check=preg_match($member_nums,trim($exam_code)) && $check;
				$check=preg_match($member_nums,trim($memno)) && $check;
				$check=preg_match($member_nums,trim($exam_centre_code)) && $check;
				$check=preg_match($validation_alpha,trim($name)) && $check;
				$check=preg_match($validation_alpha,trim($gender)) && $check;
		     // $check=preg_match($mem_type_val,trim($mem_type)) && $check;
			    //$check=preg_match($email_flag_val,trim($email_flag)) && $check;

				/*echo "exam".preg_match($member_nums,trim($exam_code))."<br>";
				echo "mem".preg_match($member_nums,trim($memno))."<br>";
				echo "examcen".preg_match($member_nums,trim($exam_centre_code))."<br>";
				echo "name".preg_match($validation_alpha,trim($name))."<br>";
				echo "gender".preg_match($validation_alpha,trim($gender))."<br>";echo "check ".$check."<br>";
				$check =(preg_match($member_nums,trim($subject_code)))&&(preg_match($member_nums,trim($exam_code)))&&
				(preg_match($member_nums,trim($memno)))&&(preg_match($member_nums,trim($exam_centre_code)))&&
				(preg_match($validation_alpha,trim($name)))&&(preg_match($validation_alpha,trim($gender)));*/
				if(trim($pincode) != "")
				{
					 $check=(preg_match($pincode_val,trim($pincode)) && $check);
				}
						
				if(trim($state_code) != "")
				{
					$check=(preg_match($validation_alpha,trim($state_code)) && $check);
				}
				if($check)
				{
					$sql_check="select count(1) from iib_candidate where membership_no='$memno'";
					if ($commonDebug)
					{
						echo $sql_check;
					}
					else
					{
						$res_check = mysql_query($sql_check);
						if (mysql_error())
						{
							$errormsg="Error in query";
						}
						else
						{
							list($count)=mysql_fetch_row($res_check);
							if($count==0)
							{
								//select membership_no from iib_candidate order by membership_no 
								//$dbcolumns_cand = " membership_no , exam_centre_code ,mem_type, gender , password , address1,address2,address3,address4,address5,address6,state_code,zone_code, name ,pin_code,institution_code,institution_name";
								$dbcolumns_cand = " membership_no , exam_centre_code , gender , password , address1,address2,address3,address4,address5,address6,state_code,zone_code, name ,pin_code,institution_code,institution_name";
								
								if($dup_memno!=$memno || $dup_memno=='')
								{
									$insert_cand=0;
									$sql_insert_cand="insert into iib_candidate ($dbcolumns_cand)"
													 ." values "
													 ." ('$memno' , "
													 ." '$exam_centre_code' , "
													 ." '$gender' , "
													 ." right(upper(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(password(membership_no),1,'A'),3,'C'),4,'D'),5,'E'),8,'H'),7,'G'),9,'I'),6,'F'),0,'Z'),2,'B')),8) , "
													 ." '$address1' , "
													 ." '$address2' , "
													 ." '$address3' , "
													 ." '$address4' , "
													 ." '$address5' , "
													 ." '$address6' , "
													 ." '$state_code' , "
													 ." '$zone_code' , "
													 ." '$name' , "
													 ." '$pincode',"
													 ." '$institution_code' , "
													 ." '$institution_name' ) ";
													 //." '$email_flag') "
									//echo $sql_insert_cand."<br>";
									$dup_memno=$memno;
									$insert_cand=1;
								}
								if ($commonDebug)
								{
									echo $sql_insert_cand;
								}
								else
								{
									if($insert_cand==1)
									{
										$res_insert_cand= mysql_query($sql_insert_cand);
									}
									
									if (mysql_error())
									{
										$error_row[]=$j-1;
									}
								}
							}//end of count
						
							$insert_exam=0;
							$dbcolumns_exam="membership_no,exam_code,subject_code,medium_code,institution_code,alloted";
							$sql_exam_code="select count(1)from iib_exam where exam_code='$exam_code'";
							//echo $sql_exam_code."<br>";
							$res_exam_code=mysql_query($sql_exam_code);
							$num_exam=mysql_num_rows($res_exam_code);
							if($num_exam>0)
							{
								$sql_sub_code="select count(1)from iib_exam_subjects where exam_code='$exam_code'";
								$res_sub_code=mysql_query($sql_sub_code);
								$num_sub=mysql_num_rows($res_exam_code);
								if($num_sub>0)
								{
									$exam_check="select count(1) from iib_exam_candidate where exam_code='$exam_code' and subject_code='$subject_code' and membership_no='$memno'"; 
									//echo $exam_check."<br>";
									$res_exam_check=mysql_query($exam_check);
									$count_exam_check=mysql_fetch_row($res_exam_check);
									//echo "count".$count_exam_check[0]."<br>";
									if($count_exam_check[0]==0)
									{
										$sql_insert_exam="insert into iib_exam_candidate ($dbcolumns_exam)"
														 ." values "
														 ." ('$memno' , "
														 ." '$exam_code' , "
														 ." '$subject_code' , "
														 ." '$medium_code' , "
														 ." '$institution_code','N') ";
										$insert_exam=1;
										$nUpdatecount++;
										$update_flag=1;
												//echo $sql_insert_exam."<br>";
									}				
								}
							}
							if ($commonDebug)
							{
								echo $sql_insert_exam;
							}
							else
							{
								if($insert_exam==1)
								{
									$res_insert_exam= mysql_query($sql_insert_exam);
								}
								/*if($insert_exam==1)
								{
									$nUpdatecount++;
								}*/
								if (mysql_error())
								{
									$error_row[]=$j-1;
								}
							}
						}//end of else mysql error
					}//end of else common debug
						
				}//end of !check
				else
				{
					$validate_rec[]="$memno|$exam_centre_code|$gender|$address1|$address2|$address3|$address4|$address5|$address6|$state_code|$zone_code|$name|$pincode|$institution_code|$institution_name|$exam_code|$subject_code|$medium_code";
					$validate=0;
				}
			}//if trim
							//else{
								//$duplicate_memno[]="$memno|$name|$email_id|$address1|$address2|$address3|$address4|$city|$state|$pincode";
								//$duplicate_memno[]="$memno|$exam_centre_code|$gender|$address1|$address2|$address3|$address4|$address5|$address6|$state|$zone_code|$name|$pincode|$institution_code|$institution_name|$exam_code|$subject_code|$medium_code";
							//}
			else
			{
				$null=1;
				//$column_null[]="$memno|$name|$email_id|$address1|$address2|$address3|$address4|$city|$state|$pincode";
				$column_null[]="$memno|$exam_centre_code|$gender|$address1|$address2|$address3|$address4|$address5|$address6|$state_code|$zone_code|$name|$pincode|$institution_code|$institution_name|$exam_code|$subject_code|$medium_code";
			}
	}//end of outer for loop
	if(is_array($error_row))
	{
		$error_msg = "Error in row(s) ".implode(" | ",$error_row);
	}

}
}//else
}//else no records
}//$file!=''
else{
	$errormsg=$excelerrormsg;
}
$count_validate=count($validate_rec);
$count_column_null=count($column_null);
if(!$excelerrormsg){
	//$error_msg="Uploaded Successfully";
}
if($nUpdatecount=="" && $update_flag==0 && $count_column_null ==0 ){
	$error_msg="Records already exists in the database";
}
if($nUpdatecount=="" && $update_flag==0 && $count_column_null>0 ){
	$error_msg="Error in Insertion of Records";
}//if($nUpdatecount)
if(($nUpdatecount=="") && ($update_flag==0 && $null=='1')){
	$error_msg="Error in Insertion of Records due to null problem";
}
if(($nUpdatecount=="" && $update_flag==1) || $validate==0){
	$error_msg="Error in Insertion of Records";
}//if($nUpdatecount)

elseif($nUpdatecount==1){
	$error_msg="$nUpdatecount Record has been Inserted";
}//if($nUpdatecount)
else if($nUpdatecount>1){
	$error_msg="$nUpdatecount Records has been Inserted";
}
}//$flag update

?>
<html>
<head>
<title><?=PAGE_TITLE ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<script language='javascript'>
function validate()
{
	var frm=document.upload_candidate;

	if (frm.excelfile.value == '')
	{
		alert('Please Select The File');
		frm.excelfile.focus();
		return;
	}
	frm.action="candidate_upload.php";
	frm.flag.value="update";
	frm.submit();
}
function alert_msg()
{	
	var frm=document.upload_candidate;		
	alert('Please browse the file');
	return false;
}

function newwin()
{
	
    window.open('cand_validation_pbm.php','win1','left=2,top=2,status=no,config="width="+newWidth+" height="+newHeight+",toolbar=no,scrollbars=no,resizable=yes');
}

function newwin1()
{
	
    window.open('cand_null_pbm.php','win1','left=2,top=2,status=no,config="width="+newWidth+" height="+newHeight+",toolbar=no,scrollbars=no,resizable=yes');
}
</script>
</head>
<BODY leftMargin=0 topMargin=0 LeftMargin=0 marginheight=0 marginwidth=0>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
	<!--<TR><!-- Topnav Image -->
       <!-- <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR>
    <TR>
        <TD bgColor=#7292c5 width=780>&nbsp;</TD>
    </TR>-->
<tr><td width="780"><?include("includes/header.php");?></td></tr>

	<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>
	<?
	echo "<tr><td>";
	disp_error($errormsg);
	echo "</td></tr>";

	If(!$errormsg){
		echo "<tr><td>";
		disp_error($error_msg);
		echo "</td></tr>";
	}
		?>
		<TR>
    	<TD background=images/tile.jpg vAlign=top width=780 align=center>
        <table width=780 cellpadding=0 cellspacing=5 border=0>
        	<tr>
	           	<td colspan=2 align="center"><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2> Candidate Details Upload</b></font></td>
            </tr>    
            </tr>
            <td colspan=2 align=center class="greybluetext10">
            <!--<form name=frmtaupload enctype="multipart/form-data" action="taupload.php" method="POST">-->
            <form name=upload_candidate method="post" enctype="multipart/form-data">
			<input type="hidden" name="flag" value="">
			Send this file: <input name="excelfile" type="file" class="textbox" onkeypress='return alert_msg()'>
			<input name="Submit" type="submit" class="button" value="Upload" onClick="javascript:validate();return false;">
     	
     	
		<!--<table width=780 cellpadding=0 cellspacing=5 border=0>
			<tr>
				<td colspan=2 align="center" ><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2>
			</tr>

			<tr>
				<td colspan=2 align="center" ><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2>Upload Candidate List</b></font></td>
			</tr>


			<tr>
				<td align="right" width=390 class="greybluetext10">
				Upload Candidate Details&nbsp; :
				</td>
				<td align="left">
				<input name="excelfile" type="file"  class="textbox">
				</td>
			</tr>
			<tr>
				<td align="right">
				<input name="Submit" type="submit" class="button" value="Upload" onClick="javascript:validate();return false;">
			</tr>-->

<?
//$count_duplicate=count($duplicate_memno);
//$count_email=count($email_invalid);
$count_validate=count($validate_rec);
$count_column_null=count($column_null);
//if(($count_duplicate !=0) || ($count_email !=0) || ($count_column_null !=0) ){
if(($count_duplicate !=0) || ($count_column_null !=0) || $count_validate!=0){
?>

		<tr>
            <td colspan=2  class="greybluetext10" height="35" align=center>
            <table border=0 cellpadding=2 cellspacing=2 bordercolorlight=white width=100%>
            <tr bgcolor="#D1E0EF" class="greybluetext10"><td  align='center' colspan=18><b>Records With Error In Excel</b></td></tr>
			<tr bgcolor="#D1E0EF" class="greybluetext10">
				<td align=center><b>Membership No</b></td>
				<td align=center><b>Exam Centre Code</b></td>
				<!--<td align=center><b>Membership Type</b></td>-->
				<td align=center><b>Gender</b></td>
				<td align=center><b>Address1</b></td>
				<td align=center><b>Address2</b></td>
				<td align=center><b>Address3</b></td>
				<td align=center><b>Address4</b></td>
				<td align=center><b>Address5</b></td>
				<td align=center><b>Address6</b></td>
				<td align=center><b>State</b></td>
				<td align=center><b>Zone</b></td>
				<td align=center><b>Name</b></td>
				<td align=center><b>Pincode</b></td>
				<td align=center><b>Institution Code</b></td>
				<td align=center><b>Institution Name</b></td>
				<td align=center><b>Exam Code</b></td>
				<td align=center><b>Subject Code</b></td>
				<td align=center><b>Medium</b></td>
				<!--<td align=center><b>Email Flag</b></td>-->
				
		    </tr>
			<?
			if($count_duplicate >0)
			{?>
				<tr bgcolor="#D1E0EF" class="greybluetext10"><td align='center' colspan=18><font color="#FF0000"><b>Membership Number Already Exists</font></b></td></tr>

			<?
			}//$count_duplicate

			foreach($duplicate_memno as $key => $value){
				$value1=$value;
				$avalue=explode('|',$value1);
				echo "<tr bgcolor=#D1E0EF class=greybluetext10>";
				foreach($avalue as $key1 => $value2){
					echo "<td align=center width=30%>";
					echo $value2;
					echo "</td>";
				}
				echo "</tr>";
			}

			if($count_validate >0){?>
		    <tr bgcolor="#D1E0EF" class="greybluetext10"><td align='center' colspan=18><font color="#FF0000"><a href="javascript:newwin();"><b>Validation Problem</b></a></font></td></tr>

			<?
			}//$count_validate
			foreach($validate_rec as $key => $value){
				$value1=$value;
				$avalue=explode('|',$value1);
				echo "<tr bgcolor=#D1E0EF class=greybluetext10>";
				foreach($avalue as $key1 => $value2){
					echo "<td align=center width=30%>";
					echo $value2;
					echo "</td>";
				}
				echo "</tr>";
			}

			 if($count_email >0){?>
		    <tr bgcolor="#D1E0EF" class="greybluetext10"><td align='center' colspan=18><font color="#FF0000"><a href="javascript:newwin();"><b>Validation Problem</b></a></font></td></tr>

			<?
			}//$count_email
			foreach($email_invalid as $key => $value){
				$value1=$value;
				$avalue=explode('|',$value1);
				echo "<tr bgcolor=#D1E0EF class=greybluetext10>";
				foreach($avalue as $key1 => $value2){
					echo "<td align=center width=30%>";
					echo $value2;
					echo "</td>";
				}
				echo "</tr>";
			}

			if($count_column_null >0){?>
		    <tr bgcolor="#D1E0EF" class="greybluetext10"><td align='center' colspan=18><font color="#FF0000"><a href="javascript:newwin1();"><b>Null Values</b></a></font></td></tr>

			<?
			}//$count_nCountarray
			foreach($column_null as $key => $value){
				$value1=$value;
				$avalue=explode('|',$value1);
				echo "<tr bgcolor=#D1E0EF class=greybluetext10>";
				foreach($avalue as $key1 => $value2){
					echo "<td align=center>";
					echo $value2;
					echo "</td>";
				}
				echo "</tr>";
			}


		?>
		</table></td><tr>
		<tr class="greybluetext10">
            <td colspan=3  class="greybluetext10" height="35" align=center><b>Note: Please Recheck the above mentioned records and upload it. </b>
            </td>
          </tr>
		<?}//count?>
		<tr class="greybluetext10">
       		 <td colspan=3  class="greybluetext10" height="35" align=left><b>Note: Please Upload the Candidate List with the <a href="includes/download/candidatelist_upload.xls" target="_blank" class="greybluetext10"><font color="blue">Template File</font></a></b>
        </td>
          </tr>

		</table>
		</form>
		</TD>
	</TR>
		<!--<TR>
    	<TD bgColor=#7292c5 width=780>&nbsp;</TD>
    	</TR>-->
<?php include("includes/footer.php")?>
</table>
</CENTER>
</body>
</html>