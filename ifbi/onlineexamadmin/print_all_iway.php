<?
set_time_limit(0);
ob_start();
$admin_flag = 0;
require_once("constants.inc");
$fontPath = $fpdfPath."/font/";
$fpdfFile = $fpdfPath."/fpdf.php";
define('FPDF_FONTPATH',$fontPath);
require_once "sessionchk.php";
require_once "dbconfig.php";
require($fpdfFile);
class PDF_MC_Table extends FPDF
{
var $widths;
function PDF_MC_Table(){
	$this->FPDF();
}
function SetWidths($w)
{
    //Set the array of column widths
    $this->widths=$w;
}

function Row($data,$aln,$optFill,$style)
{
    //Calculate the height of the row
    $nb=0;
    for($i=0;$i<count($data);$i++)
        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
    $h=5*$nb;
    //Issue a page break first if needed
    $this->CheckPageBreak($h);
    //Draw the cells of the row
    for($i=0;$i<count($data);$i++)
    {
        $w=$this->widths[$i];
        //Save the current position
        $x=$this->GetX();
        $y=$this->GetY();
        //Draw the border
		//$col = $pdf->GetFillColor();
		if ($optFill == ''){
			$this->SetFillColor(255,255,255);
	        $this->Rect($x,$y,$w,$h,'F');
		}
		else
        	$this->Rect($x,$y,$w,$h);
        //Print the text
     	$this->SetFont('Arial',$style,10);
	    $this->MultiCell($w,5,$data[$i]." ".$col,0,$aln);
        //Put the position to the right of the cell
        $this->SetXY($x+$w,$y);
    }
    //Go to the next line
    $this->Ln($h);
}
function CheckPageBreak($h)
{
    //If the height h would cause an overflow, add a new page immediately
    if($this->GetY()+$h>$this->PageBreakTrigger)
        $this->AddPage($this->CurOrientation);
}
function PutLineBreak(){
	//put a line break 
    $this->Ln(3);
}
function NbLines($w,$txt)
{
    //Computes the number of lines a MultiCell of width w will take
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);
    if($nb>0 and $s[$nb-1]=="\n")
        $nb--;
    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $nl=1;
    while($i<$nb)
    {
        $c=$s[$i];
        if($c=="\n")
        {
            $i++;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
            continue;
        }
        if($c==' ')
            $sep=$i;
        $l+=$cw[$c];
        if($l>$wmax)
        {
            if($sep==-1)
            {
                if($i==$j)
                    $i++;
            }
            else
                $i=$sep+1;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
        }
        else
            $i++;
    }
    return $nl;
}
}
/* 
$date_array[0]= "2006-03-05";
$date_array[1]= "2005-07-10";
$date_array[2]= "2005-07-17";
$date_array[3]= "2005-07-24";
$date_array[4]= "2005-07-31";

$time_array[0]="08:30:00";
$time_array[1]="11:15:00";
$time_array[2]="14:00:00";
$time_array[3]="16:30:00";
//$time_array[4]="19:00:00";
*/

$exam_centre= $_REQUEST["sCentreCode"];
$Centre_name = $_REQUEST["cc"];
$date_print=$_REQUEST["dt"];
$time_print=$_REQUEST["tm"];
$date_option=$_REQUEST["dto"];
/*
if($date_option=='selected')
	$date_array[0]= $date_print;
elseif($date_option=='all')
	{
		$sql = "select distinct exam_date from iib_candidate_iway";
		$res = mysql_query($sql) or die ("Connection cannot be done error:'".mysql_error ($db)."'");
		$it=0;
		while($num = mysql_fetch_row($res))
		{
			$date_array[$it]=$num[0];
			$it++;
		}
	}

*/
$date_array[0]= "2006-04-26";
/*
$date_array[0]= "2005-12-04";
$date_array[1]= "2005-12-11";
$date_array[2]= "2005-12-18";
$date_array[3]= "2006-01-01";
$date_array[4]= "2006-01-08";
*/
if($time_print != "All")
	$time_array[0]=$time_print;
else
{
	$sql = "select slot_time from iib_exam_slots";
	$res = mysql_query($sql) or die ("Connection cannot be done error:'".mysql_error ($db)."'");
	$it=0;
	while($num = mysql_fetch_row($res))
		{
			$time_array[$it]=$num[0];
			$it++;
		}
}
$iWay_Code="";

/*print_r($date_array);
print_r($time_array);
exit;*/
//$Date = $_REQUEST["dt"];

if ($Date != "")
{
	$aDisp = explode("-",$Date);
	$dispDate = $aDisp[2]."/".$aDisp[1]."/".$aDisp[0];
}
//$Time = $_REQUEST["tm"];
$hrs = array();
$hrs=explode(":",$Time);
$pdf=new PDF_MC_Table();
$pdf->Open();
$pdf->SetFont('Arial','',10);
//$exam_centre=467;
$sql_exam_centre="select exam_centre_name from iib_exam_centres where exam_centre_code='$exam_centre'";
$res_exam_centre=mysql_query($sql_exam_centre);
list($exam_centre_name)=mysql_fetch_row($res_exam_centre);
$strFileName = "AS_".$exam_centre_name.'_'.$aDisp[2].'_'.$hrs[0].'-'.$hrs[1].'.pdf'; 
$sql_iways = "select centre_code from iib_iway_details where exam_centre_code='$exam_centre' and centre_code != '123456A'";
//$sql_iways = "select centre_code from iib_iway_details where exam_centre_code='$exam_centre' and centre_code ='440025A'";
$res_iways = mysql_query($sql_iways);
while(list($iWay_Code)=mysql_fetch_row($res_iways)){
//ob_start();
$strFileName = "AS_".$exam_centre_name.'_'.$aDisp[2].'.pdf';
for($tmp=0;$tmp < count($date_array);$tmp++){
for($tmp_time=0;$tmp_time < count($time_array);$tmp_time++){
$Date=$date_array[$tmp];
$Time=$time_array[$tmp_time];
if ($Date != "")
{
        $aDisp = explode("-",$Date);
        $dispDate = $aDisp[2]."/".$aDisp[1]."/".$aDisp[0];
}

$pdf->AddPage();
$pdf->PutLineBreak();

$sql="select a.membership_no ,b.name, c.exam_name from iib_candidate_iway a , iib_candidate b, iib_exam c where a.exam_date = '$Date' and a.exam_time = '$Time' and a.centre_code = '$iWay_Code'and  b.membership_no = a.membership_no and c.exam_code = a.exam_code";
//echo $sql;
$res=mysql_query($sql);
$num_of_rows=mysql_num_rows($res);
$emsg="";
if(!$res)
	$emsg="Sorry Details Cannot Be Retrieved.Please Enter the Correct Data";
if($num_of_rows == 0)
	$emsg="No Candidate is assigned";
$iWaySql="select iway_name, iway_address1, iway_address2, iway_city, iway_state, iway_pin_code from iib_iway_details where centre_code='$iWay_Code'";
$iWayRes = mysql_query($iWaySql);
if (!$iWayRes)
	$emsg="Sorry Details Cannot Be Retrieved.Please Enter the Correct Data";
$iwaycnt=mysql_num_rows($iWayRes);
if($iwaycnt == 0)
	$emsg="iWay Details not found";
list($iWay_name,$iway_address1, $iway_address2, $iway_city, $iway_state, $iway_pin_code)=@mysql_fetch_row($iWayRes);
$address = "";
if ($iWay_name != "")
$address = $iWay_name."\n";
if (($address != "") && ($iway_address1 != ""))
	$address .= " ".$iway_address1;
if (($address != "") && ($iway_address2 != ""))
	$address .= " ".$iway_address2;
if (($address != "") && ($iway_city != ""))
	$address .= " ".$iway_city;
if (($address != "") && ($iway_state != ""))
	$address .= " ".$iway_state;
if (($address != "") && ($iway_pin_code != ""))
	$address .= " ".$iway_pin_code;
$pdf->PutLineBreak();
$pdf->SetWidths(array(190));
$pdf->Row(array("Candidate Attendence Sheet"),'C','','B');
$pdf->PutLineBreak();
$pdf->SetWidths(array(30,10,150));
$pdf->Row(array("Centre Code",":",$iWay_Code),'L','','');
$pdf->PutLineBreak();
$pdf->SetWidths(array(30,10,150));
$pdf->Row(array("iWay",":",$address),'L','','');
$pdf->PutLineBreak();
$pdf->SetWidths(array(30,10,150));
$pdf->Row(array("Date",":",$dispDate),'L','','');
$pdf->PutLineBreak();
$pdf->SetWidths(array(30,10,150));
$pdf->Row(array("Time",":",substr($Time,0,5)." hrs"),'L','','');
$pdf->PutLineBreak();
if($emsg !="") {
$pdf->SetWidths(array(190));
$pdf->Row(array($emsg),'L','','');
}
elseif($emsg =="") {
	$pdf->SetWidths(array(30,60,40,60));
	$pdf->Row(array("Member Number","Member Name","Exam Name","Signature"),'C','F','');
	while ($num=mysql_fetch_row($res)){
	$pdf->SetWidths(array(30,60,40,60));
	$pdf->Row(array($num["0"],$num["1"],$num["2"],""),'C','F','');
	}	 
	$pdf->PutLineBreak();
	$pdf->PutLineBreak();
	$pdf->PutLineBreak();	
	$pdf->SetWidths(array(190));
	$pdf->Row(array("TA Signature"),'R','','B');
}
}	 

}
}
$pdf->Output($strFileName,true);
ob_end_flush();
?>
