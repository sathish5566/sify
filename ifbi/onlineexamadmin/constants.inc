<?php
	$totalTime = 7200;
	$aSCorrectAnswers = array(1, 3, 2, 1, 4);
	$aSMarks = array(1, 3, 2, 3, 1);
	$aSNegMarks = array(0.25, 0.75, 0.5, 0.75, 0.25);
	$aMedium = array("H"=>"HINDI", "HI"=>"HINDI", "HINDI"=>"HINDI", "E"=>"ENGLISH", "EN"=>"ENGLISH", "ENGLISH"=>"ENGLISH");
	$fpdfPath = "/home/sites/ibpsadmin/fpdf151";
	$aStoredMarks = array(0.5,1, 2, 3);
	$nStoredMarks = count($aStoredMarks);

$aColor = array("#99ffff","#ffffcc","#A6A6FF","green","Red","grey","White");
	/*Constants Used in Admit Card Page*/

	$header_bankName="Indian Overseas Bank";
	$header_bankaddress1="Central Office : P.B.No.3765, 762 Anna Salai, ";
	$header_bankaddress2="Chennai 600 002";
	$header_dept="Personnel Administration Department";
	$header_section="Supervisory Section";

	$header_phone1="Ph:   044 2851 9446";
        $header_phone2="044 2851 9660";
	$header_phone3="Fax :044 2852 9621";

	$disp_number="PAD/SUP/177/";  
	$letter_date="15.04.2006";
	
	$running_no_length="5";	

	$reporting_time="9.30 a.m. ";
	$start_time  = "10.00 a.m. ";
	$end_time   =  "11.15 a.m.";
	
	$candpath="/home/sites/ibps"; 
//Table formation details for response report

	$cand_table_per_row=3;
	$cand_no_of_rows=100;	


?>
