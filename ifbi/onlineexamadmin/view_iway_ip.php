<?php
$admin_flag = 3;
	require_once "sessionchk.php";
	require_once "dbconfig.php";
	
	$examCentreCode = $_POST['exam_centre_code'];
	$centreCode = $_POST['centre_code'];
	//print_r( $_POST);
	if ($examCentreCode != "")
	{
		$sqlCentres = "SELECT centre_code, iway_name FROM iib_iway_details WHERE exam_centre_code='$examCentreCode' and status !='D'";
		$resCentres = @mysql_query($sqlCentres);
		if (mysql_error())
		{
			$err = 1;
			$emsg = "Error in selecting iway details.";
			$displaybug = 'true';	
		}
		else
		{
			$nCentres = mysql_num_rows($resCentres);
		}
	}
	
	if (($examCentreCode != "") && ($centreCode != ""))
	{
		$sqlDetails = "SELECT start_ip,end_ip,is_active FROM iib_ta_details WHERE centre_code='$centreCode' ";
		$resDetails = @mysql_query($sqlDetails);
		if (mysql_error())
		{
			$err=1;
			$emsg="Error in selecting IWay Details.";
			$displaybug='true';	
		}
		$nDetails = mysql_num_rows($resDetails);
		if ($nDetails > 0)
		{
			list($startIp,$endIp,$status) = mysql_fetch_row($resDetails);
		}
	}
	$sql = "select exam_centre_code, exam_centre_name from iib_exam_centres where online='Y' order by exam_centre_name";
	$res = @mysql_query($sql);
	if (!$res)
	{
		$err=1;
		$emsg="Error in selecting exam centres.";
		$displaybug='true';	
	}
	?>
	<HTML>
	<HEAD>
	<script language='JavaScript' src="./includes/validations.js"></script>
	<title>Indian Institute Of Banking &amp; Finance</title>
	<link href="images/iibf.css" rel="stylesheet" type="text/css">
	<STYLE>BODY {
	        FONT-FAMILY: Arial, Verdana
	}
	</STYLE>
	<script language='JavaScript'>
	function validate()
	{
		var frm = document.frmCentres;
		if (frm.exam_centre_code.selectedIndex == 0)
		{
			alert('Please select the Exam Centre');
			frm.exam_centre_code.focus();
			return false;
		}
		if (frm.centre_code.selectedIndex == 0)
		{
			alert('Please select the I Way Code');
			frm.centre_code.focus();
			return false;
		}
		
		frm.submit();
	}
	
	function getCentres()
	{
		frm = document.frmCentres;
		if (frm.exam_centre_code.selectedIndex == 0)
		{
			frm.centre_code.length = 0;
			var opt = new Option('--Select--', '');
	
	  		var sel = frm.centre_code;
	  		sel.options[0] = opt;
	
		}
		else
		{
			document.frmCentres.submit();
		}
	}
	
	function getDetails()
	{	
		frm = document.frmCentres;
		document.frmCentres.submit();
	}
	
	</script>
	</HEAD>
	<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0 onload='javascript:document.frmCentres.exam_centre_code.focus();' onKeyPress='javascript:submitForm()'>
	<center>
	<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
	        <TR><!-- Topnav Image -->
	        <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	        </TR>
	        <TR>
	        <TD bgColor=#7292c5 width=780>&nbsp;</TD>
	        </TR>
	        <tr>
			<Td class="greybluetext10"  background=images/tile.jpg>
			<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
				<TBODY>
				<TR>
				<TD align=left class="greybluetext10">&nbsp;&nbsp;&nbsp;
					<A href="view_iway_ip.php">View IP Details</A>||<A href="edit_iway_ip.php">Edit IP Details</A>||<A href="admin_logout.php">Logout</A>	    
				<br><br><br>
				</TD>
				</TR>
				</TBODY>
			</TABLE>
			</Td>	
		</tr>  
	        <TR>
	            <TD background=images/tile.jpg vAlign=top width=780 align=center>
	        <form name='frmCentres' method="post" onsubmit="return false;">   
	                <table width=780 cellpadding=0 cellspacing=5 border=0>
	                        <tr>
	                                <td colspan=2 align="center"><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2>View IWay IP Details</b></font></td>
	                        </tr>                
			                <tr>
	                                <td align="right" width=390 class="greybluetext10" valign=top>Select Exam Centre :&nbsp;</td>
	                                <td align="left" valign=top>
	                                        <select class="textbox" name="exam_centre_code" style={width:169px} onChange='javascript:getCentres()'>
											<option value=''>-Select-</option>
							<?php
									while(list($exam_centre_code,$exam_centre_name) = mysql_fetch_row($res))
									{
											$exam_centre_name=stripslashes($exam_centre_name);
											if ($exam_centre_code == $examCentreCode) 
											{
												echo "<option value=\"$exam_centre_code\" selected>$exam_centre_code - $exam_centre_name</option>";											
											}
											else
											{
												echo "<option value=\"$exam_centre_code\">$exam_centre_code - $exam_centre_name</option>";
											}
									}
							?>
											</select>
	                                </td>
	                        </tr>                    
							<tr>
	                        	<td align="right" width=390 class="greybluetext10" valign=top>I Way Code :&nbsp;</td>
	                            <td align="left" valign=top>
	                        		<select class="textbox" name="centre_code" style={width:169px} onChange='javascript:getDetails()'>
	                        			<option value=''>--Select--</option>
	                        <?php
	                        	if ($nCentres > 0)
	                        	{
									while(list($centre_code,$centre_name) = mysql_fetch_row($resCentres))
									{
											$centre_name=stripslashes($centre_name);
											if ($centre_code == $centreCode)
											{
												echo "<option value=\"$centre_code\" selected>$centre_code - $centre_name</option>";											
											}
											else
											{
												echo "<option value=\"$centre_code\">$centre_code - $centre_name</option>";
											}
									}
								}
							?>			
	                        		</select>
	                        	</td>
	                        </tr>
							<tr>
	                        	<td align="right" width=390 class="greybluetext10" valign=top>Start IP :&nbsp;</td>
	                            <td align="left" valign=top>
	                            <FONT face="Verdana, Arial, Helvetica, sans-serif" size=1>
	                        		<?=$startIp?>
	                        	</font>
	                        	</td>
	                        </tr> 
	                        <tr>
	                        	<td align="right" width=390 class="greybluetext10" valign=top>End IP :&nbsp;</td>
	                            <td align="left" valign=top>
	                            <FONT face="Verdana, Arial, Helvetica, sans-serif" size=1>
	                        		<?=$endIp?>
	                        	</font>
	                        	</td>
	                        </tr>
	                        <tr>
	                        	<td align="right" width=390 class="greybluetext10" valign=top>Status :&nbsp;</td>
	                            <td align="left" valign=top>
	                            <FONT face="Verdana, Arial, Helvetica, sans-serif" size=1>
	                        			<?php 
	                        				if ($status == '0') 
	                        					print(" Not Activated ");
	                        				elseif ($status == '1') print(" Activated");
	                        				?>
	                        		</font>
	                        	</td>
	                        </tr> 
	                        
							<?php 
								if (isset($err) && $err!='') 
								{
							?>
	                        <tr>
	                                <td colspan=2 align="center" class='errormsg'><b><?echo $emsg;?></b></td>
	                        </tr>
	                		<?php
	                			}
	                		?>
	                		<?if (isset($done) && $done!=''){?>
	                        <tr>
	                                <td colspan=2 align="center" class='alertmsg'><b><?echo $msg;?></b></td>
	                        </tr>
	                		<?}?>                                       
	                </table>
	                </form>
	                </TD>
	        </TR>
	        <TR>
	                <TD bgColor=#7292c5 width=780>&nbsp;</TD>
	        </TR>
	</TABLE>
	</FORM>
	</center>
	</BODY>
	</HTML>
