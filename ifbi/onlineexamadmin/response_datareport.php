<?
/****************************************************
* Application Name            :  IBPS
* Module Name                 :  Data Report
* Revision Number             :  1
* Revision Date               :
* Table(s)  modified          : 
* Tables used for only selects: iib_exam_schedule,iib_exam,iib_exam_subjects,iib_candidate_scores,iib_candidate_test,iib_question_paper_details,iib_question_paper.
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              : Html file and csv file
* Document/Reference Material :
* Created By                  : Karuna moorthy raju K
* Created On 				  : 22/Apr/2006 
* Last Modified By            :  
* Last Modified Date          : 
* Description                 :   This report  gives the question_id,answer and the status of the candidates who are attended the exam
*****************************************************/
ob_start();
require("dbconfig_slave_123.php");
require("sessionchk.php");

set_time_limit(0);

//Values got from Post variables

$examCode = isset($_POST['exam_code']) ? $_POST['exam_code'] : '';
$subjectCode = isset($_POST['subject_code']) ? $_POST['subject_code'] : '';
$examDate = isset($_POST['exam_date']) ? $_POST['exam_date'] : '';
$subject = isset($_POST['subject']) ? $_POST['subject'] : 'false';
$submitted= isset($_POST['submitted']) ? $_POST['submitted'] : 'false';

// Getting values for dowload option.
$ex_flag=isset($_POST['exl']) ? $_POST['exl'] : '0' ;

$tot_questions = "select count(question_id) from iib_question_paper_details where subject_code='$subjectCode' group by question_paper_no";
$res_totquestion = mysql_query($tot_questions);
$total_questions = mysql_fetch_row($res_totquestion);
$t = $total_questions[0];
 
  /** It forms the array for the header information for the excel export   **/

$excel_header[0] ="Membership No";
$excel_header[1] = "Name";
//$excel_header[2] = "Status";
$excel_header[2] = "Question Id";
//   For getting the number of questions in the question paper
$hd_count = 3;
	for($i=1;$i<=$t ;$i++)
	{
		$excel_header[$hd_count]=$i;
		$hd_count++;
	}  // for loop 
// Excel export  file
include_once("excelconfig.php");

//File name for download
$filename="Response_report_e(".$examCode.")_s(".$subjectCode.")_Dt(".$examDate.")";

//Selecting the exam dates for drop down(I/P)
$sqlDate = "SELECT distinct exam_date FROM iib_exam_schedule order by exam_date ";
$resDate = mysql_query($sqlDate);

//Selecting the exams for drop down(I/P)
$sqlexam = "SELECT exam_code,upper(exam_name) FROM iib_exam where online='Y' ";
$resexam = mysql_query($sqlexam);

//Variable for diaplaying the message
$msg='';


		if (($subject == 'true') || ($submitted == 'true'))
		{	
				$sql_subject_sel="select exam_code,subject_code,subject_name from iib_exam_subjects where exam_code='".$examCode."' order by subject_name";
				$res_subject_sel=mysql_query($sql_subject_sel);
			}  // If loop for check the subject

		if($submitted=='true' && $examCode!='' && $subjectCode!='' && $examDate!='')
		{
	//Selecting the name and membership no from iib_candidate
			$sql_pre = "select  a.membership_no,b.name from iib_candidate_iway a,iib_candidate b where a.exam_code='$examCode' and a.subject_code='$subjectCode' and a.exam_date='$examDate' and a.membership_no=b.membership_no";
				$res_pre=mysql_query($sql_pre);
                
			$cnt_cand_pre = mysql_num_rows($res_pre);
			
			if($cnt_cand_pre < 1 )
			{
					$msg="No records found";
			} // If loop for data found
			else
			{
				// Query to fetch the test status of  each candidate.
			    $sql_test = "select membership_no,test_status from iib_candidate_test  where exam_code='$examCode' and subject_code='$subjectCode' and  start_time like '$examDate%' and current_session='Y' ";
               $res_test = mysql_query($sql_test);

			 // It getting the questionid and answer for the respective candidates.    		   
				   while(list($mno,$stat)=mysql_fetch_row($res_test))
					{
						$att_stat[$mno][1]   = $stat; 
					}
						
					$i=0;	
					//Storing the membership no, name, question id,answer for the candidates.
					    while(list($mem_no,$mem_name) = mysql_fetch_row($res_pre))
						 {
									$exce_content[$i][] = $mem_no;
									$exce_content[$i][] = $mem_name;
									//$status = $att_stat[$mem_no][1];
										//if($status=='')
									//	$status = "NA";
								    //$exce_content[$i][] = $status;
									//$exce_content[$i][3] = "Question Id";
								//	$exce_content[$i+1][2] = "Response";
									$exce_content[$i][2] = "Response";
//									 $sql_sect_ques="select a.question_id ,a.answer from iib_question_paper_details a,iib_question_paper b  where a.subject_code ='$subjectCode'  and b.exam_code='$examCode' and a.question_paper_no=b.question_paper_no and b.membership_no='$mem_no' order by display_order";

//									$sql_sect_ques="select a.question_id ,a.answer from iib_question_paper_details a,iib_question_paper b  where a.subject_code ='$subjectCode'  and b.exam_code='$examCode' and a.question_paper_no=b.question_paper_no and b.membership_no='$mem_no' order by question_id";


									$sql_sect_ques = "select question_id, answer from iib_response where id in (select max(id) from iib_response a, iib_question_paper b where b.subject_code='$subjectCode' and b.exam_code='$examCode' and a.question_paper_no = b.question_paper_no and b.membership_no='$mem_no' group by a.question_id )order by display_order";
           							$res_sect_ques=mysql_query($sql_sect_ques);
									 $cnt_f = mysql_num_rows($res_sect_ques);
											while(list($sec_ques_id,$sec_corr_answer)=mysql_fetch_row($res_sect_ques))
											{
												//   $exce_content[$i][] = $sec_ques_id;
														if($sec_corr_answer =='') 
															 $sec_corr_answer='--';
										//			$exce_content[$i+1][] =  $sec_corr_answer;
													$exce_content[$i][] =  $sec_corr_answer;
										                            
											 }  // while loop for get question id and answer  
									$i=$i+1;
								}
						$content_cnt=count($exce_content);
				}  // Else Part of  $cnt_content
	}   //  If loop for the submitted,examcode and subjectcode checking condtions
  	if($ex_flag=='1')
	{
			excelconfig($filename,$excel_header,$exce_content);
	   		 exit;
	}  // Excel config loop

?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
	FONT-FAMILY: Arial, Verdana	
}
.textblk11     { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; text-decoration: none}
</STYLE>
<script language='JavaScript'>
function getsubjects()
{
	if (document.frm_scorereport.exam_code.selectedIndex == 0)
	{		
		alert("Please select the exam ");
		document.frm_scorereport.exam_code.focus();
		return false;
	}
	
	document.frm_scorereport.subject.value='true';
    document.frm_scorereport.exl.value='0';
	document.frm_scorereport.submit();
	
}
function submitForm()
{
	if (document.frm_scorereport.exam_date.selectedIndex == 0)
	{
		alert("Please select the exam date");
		document.frm_scorereport.exam_date.focus();
		return false;
	}
	if (document.frm_scorereport.exam_code.selectedIndex == 0)
	{		
		alert("Please select the exam ");
		document.frm_scorereport.exam_code.focus();
		return false;
	}
	if (document.frm_scorereport.subject_code.selectedIndex == 0)
	{		
		alert("Please select the Subject ");
		document.frm_scorereport.subject_code.focus();
		return false;
	}
	document.frm_scorereport.submitted.value='true';
	document.frm_scorereport.exl.value = '0';
	document.frm_scorereport.submit();
}
function excelsubmit()
{
	var frmname=document.frm_scorereport;
	frmname.exl.value='1';
	document.frm_scorereport.submitted.value='true';
	frmname.submit();
}
</script>
</head>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
		<tr><td><?include("includes/header.php");?> </td></tr>
	    <tr>
		<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>	
	<TR>
	    <TD background=images/tile.jpg vAlign=top width=780 align=center>
<form name='frm_scorereport' method='post'>
<input type='hidden' name='subject' value=''>
<input type='hidden' name='submitted'>
<input type=hidden name=exl value='0'>
<table width=100% border=0 cellspacing=0 cellpadding=5>
	<tr>
		<td colspan=2 align="center"><b><font face="Verdana, Arial, Helvetica, sans-serif" size=2>Response Report</b></font><br><br></td>
    </tr>
    <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Exam Date : </td>
            	<td align="left" valign=top class="greybluetext10">
            	<select name='exam_date' style={width:150px} class=greybluetext10>
				<option value=''>--Select--</option>
				<?php
				if (mysql_num_rows($resDate) > 0)
				{
					while (list($eDate) = mysql_fetch_row($resDate))
					{
						$aDate = explode("-",$eDate);
						$dispDate = $aDate[2]."-".$aDate[1]."-".$aDate[0];
						print("<option value='$eDate' ");
						if ($eDate == $examDate)
							print(" selected ");
						print(">$dispDate</option>");
					}
				}
				
				?>
            		</select>
            	</td>
            </tr> 
             <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Exam : </td>
            	<td align="left" valign=top class="greybluetext10">
            		<select name='exam_code' class="greybluetext10" style={width:275px} onchange='javascript:getsubjects();'>
            			<option value=''>--Select--</option>
            			<?
            				while (list($code,$name) = mysql_fetch_row($resexam))
            				{
	            				print("<option value='$code' ");
	            				if ($examCode == $code)
	            				{
		            				print(" selected ");
	            				}
	            		
	            				
	            				print(">$name</option>");
	            			}
	            		?>
            		</select>
            	</td>
            </tr>   
              <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Subject : </td>
            	<td align="left" valign=top class="greybluetext10">
            		<select name='subject_code' class="greybluetext10" style={width:350px} >
            			<option value=''>--Select--</option>
            			
            			<?
            				while (list($code,$Scode,$Sname) = mysql_fetch_row($res_subject_sel))
            				{
	            
	            				print("<option value='$Scode' ");
	            				if ($subjectCode == $Scode)
	            				{
		            				print(" selected ");
	            				}
	            		
	            					print(">$Sname</option>");
	            			}
	            		?>
            		</select>
            	</td>
            </tr>
            
            
            
     </table>   
    <table width="780" align="left" border="0">
        <tr align="right" width="575">
			<td  align="right" width="550"><input type=button name='sub_assign' value='Submit' class='button' onClick='javascript:submitForm()'>
			
			</td>
        </tr>
        <TR>
			<TD>&nbsp;</TD>
		</TR>
        <tr>
        	<td>
        	<?
        		if($ex_flag!='1')
				{
					$headerLength = count($excel_header);
					if($content_cnt>0)
					{
				// Printing the values in the HTML page 
				
				// Printing the header values from excel header array 
						
							print("<table border=1 align=center cellspacing=0 cellpadding=2><tr>");
							for($a=0;$a<$headerLength;$a++)
							{
									print("<td class=greybluetext10><b>$excel_header[$a]</b></td>");
							}	
								print("</tr>");
								print("<tr>");
							for($i=0;$i<$content_cnt;$i++)
							{
							    for($j=0;$j<$headerLength;$j++)
								{ 
						?>
										<td class=greybluetext10 >&nbsp;<?=$exce_content[$i][$j]?></td>
						 <?
								}	
						   
									print("</tr>");
							}	 
							print("</table>");
				?>
								<table align=center width="780" border="0">
							 			<tr align="right" width="780"><td class=greybluetext10 align="right" width="500">&nbsp;</td></tr>
		         						<tr align="right" width="780"><td class=greybluetext10 class=greybluetext10 align="center" width="780"><input class='button' type='button' name='download' value='Download' onClick='javascript:excelsubmit()'></td></tr>
						            	<tr align="right" width="500"><td class=greybluetext10 align="right" width="500">&nbsp;</td></tr>
								</table>
				<?
				}
			}
			?>		
        				</td>
					</tr>
					<?
					if($msg!='')
					{
					?>
					<tr>
						<td colspan=2 align="center" class='alertmsg'><b><?echo $msg;?></b></td>
					</tr>
				<?}?>
					</table>
				</form>
			</TD>
		</TR>
	</TABLE>
</table>
 <? include("includes/footer.php");?>
 </center>
</BODY>
</HTML>
