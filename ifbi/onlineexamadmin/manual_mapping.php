<?php
set_time_limit(0);
$commondebug = true;
require("mapcandidatedec05.php");
if (count($_FILES) > 0)
{	
	//$filepath = "/home/iibuploadfiles/";
	$filepath = "/tmp/iibuploadfiles/";
	$uploadfile = $filepath . $_FILES['userfile']['name'];
	
	if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
	} else {
    	print "Possible file upload attack!  Here's some debugging info:\n";
    	print_r($_FILES);
		exit;
	}
	
	// get contents of a file into a string
	$filename = $uploadfile;
	$handle = fopen($filename, "r");
	//$contents = fread($handle, filesize($filename));
	while(!feof($handle))
	{
		$aManual[] = fgets($handle);
	}
	 fclose($handle);

	//$aManual = file($contents);
	
	if ($commondebug){
		echo "<pre>";
		print_r($aManual);
		echo "</pre>";
	}	
	//exit;
	$nMapped = 0;
	$nUnMapped = 0;
	foreach ($aManual as $value)
	{
		if ($value != "")
		{
			$val = explode(",",$value);
			$membershipNo = trim($val[0]);
			$examCode = trim($val[1]);
			$subjectCode = trim($val[2]);
			$centreCode = trim($val[3]);
			$examDate = trim($val[4]);
			$examTime = trim($val[5]);
 			if (checkSubject($membershipNo,$examCode,$subjectCode) == false)
                        {
                                $nUnMapped++;
				print("<br>no exam subjects entry for ".$membershipNo.", ".$examCode.", ".$subjectCode);
                                addMappingLogs($centreCode,$membershipNo,$examDate,$examTime,$examCode,$subjectCode,"no exam subjects");
                                continue;
                        }
	
			if (checkVacancy($centreCode, $examDate, $examTime) <= 0)
			{
				$nUnMapped++;
				print("<br>no vacancy for ".$centreCode.", ".$examDate.", ".$examTime);
				addMappingLogs($centreCode,$membershipNo,$examDate,$examTime,$examCode,$subjectCode,"no vacancy");
				continue;
			}
			
			mapCandidate($centreCode,$membershipNo,$examDate,$examTime,$examCode,$subjectCode);
			print("<br>Mapped entry for ".$membershipNo.", ".$examCode.", ".$subjectCode.", ".$centreCode.", ".$examDate.", ".$examTime);
			$nMapped++;
		}
	}
}
print("<br>Mapped Count : $nMapped, UnMapped Count : $nUnMapped");
?>
