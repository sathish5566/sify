<?php
require_once "sessionchk.php";
require_once "dbconfig.php";

$selectedDate = $_REQUEST["sYear"]."-".$_REQUEST["sMonth"]."-".$_REQUEST["sDate"];
$sYear = $_REQUEST["sYear"];
$sMonth = $_REQUEST["sMonth"];
$sDay = $_REQUEST["sDate"];

$updated = $_POST['updated'];
$deleted = $_POST['deleted'];

$examCode = $_POST['exam_code'];
$subjectCode = $_POST['subject_code'];

$errormsg ="";
$msg = "";



if (($examCode != "") && ($subjectCode != "") && ($selectedDate != "") && (($updated == 'Y') || ($deleted == 'Y')))
{
	if ($_POST['updated'] == 'Y')
	{
		$sqlUpdate = "UPDATE iib_exam_schedule SET exam_date='$selectedDate' WHERE exam_code='$examCode' AND subject_code='$subjectCode'";
	}
	else
	if ($_POST['deleted'] == 'Y')
	{
		$sqlUpdate = "UPDATE iib_exam_schedule SET online='N' WHERE exam_code='$examCode' AND subject_code='$subjectCode'";
	}
//	print "update sql :".$sqlUpdate;
	@mysql_query($sqlUpdate);
	if (mysql_error())
	{
		$errormsg = "Update/Delete failed!";
	}
	else
	{
		if ($_POST['updated'] == 'Y')
		{
			$msg = "Exam/Subject schedule successfully updated!";
		}
		else
		if ($_POST['deleted'] == 'Y')
		{
			$msg = "Exam/Subject schedule successfully deleted!";
			$examCode = '';
			$subjectCode ='';
		}
	}
	
}
$sqlExams = "SELECT DISTINCT s.exam_code, exam_name FROM iib_exam e, iib_exam_schedule s WHERE e.online='Y' AND s.online='Y' AND ".
					"s.exam_code=e.exam_code ";
//print "<br>exams : ".$sqlExams;					
$resExams = @mysql_query($sqlExams);
if ($examCode != "")
{
	$sqlSubjects = "SELECT DISTINCT e.subject_code, subject_name FROM iib_exam_subjects e, iib_exam_schedule s WHERE e.online='Y' AND ".
							"s.online='Y' AND e.exam_code='$examCode' AND s.subject_code=e.subject_code ";
	//print "<br>subjects : ".$sqlSubjects;
	$resSubjects = @mysql_query($sqlSubjects);
	$nSubjects = mysql_num_rows($resSubjects);	
}
if (($subjectCode != "") && ($examCode != "") && ($updated != 'Y') && ($deleted != 'Y'))
{
	$sqlDate = "SELECT exam_date FROM iib_exam_schedule WHERE online='Y' AND exam_code='$examCode' AND subject_code='$subjectCode' ";
	//print "<br>date ".$sqlDate;
	$resDate = @mysql_query($sqlDate);
	if (mysql_num_rows($resDate) > 0)
	{
		list($examDate) = mysql_fetch_row($resDate);
		$aExams = explode("-", $examDate);
		$sYear = $aExams[0];
		$sMonth = $aExams[1];
		$sDay = $aExams[2];
	}
}
//Add a Exam
?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
	FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='JavaScript'>
function getSubjects()
{
	frm.deleted.value = '';
	frm.updated.value = '';
	document.addSchedule.submit();
}

function getDate()
{
	frm.deleted.value = '';
	frm.updated.value = '';
	document.addSchedule.submit();
}
function del()
{
	frm = document.addSchedule;
	if (frm.exam_code.selectedIndex == 0)
	{
		alert('Please select the Exam ');
		frm.exam_code.focus();
		return false;
	}
	if (frm.subject_code.selectedIndex == 0)
	{
		alert('Please select the Subject ');
		frm.subject_code.focus();
		return false;
	}
	frm.deleted.value = 'Y';
	frm.updated.value = '';
	frm.submit();
}

function validate()
{
	frm = document.addSchedule;
	if (frm.exam_code.selectedIndex == 0)
	{
		alert('Please select the Exam ');
		frm.exam_code.focus();
		return false;
	}
	if (frm.subject_code.selectedIndex == 0)
	{
		alert('Please select the Subject ');
		frm.subject_code.focus();
		return false;
	}
	frm.updated.value = 'Y';
	frm.deleted.value = '';
	frm.submit();
}

function submitForm()
{
    if (window.event.keyCode == 13)
    {
        validate();
    }
}
</script>
</HEAD>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0 onload="set_cor_date()" onKeyPress='javascript:submitForm()'>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
<!--	<TR><!-- Topnav Image -->
    <!--	<TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR>	  -->
	<tr><td width="780"><?include("includes/header.php");?></td></tr>
	<TR>
    	
	</TR>
	<tr>
		<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>	
	<TR>
	    <TD background=images/tile.jpg vAlign=top width=780 align=center>
     	<form name="addSchedule" method="post">
     	<input type=hidden name='updated' value=''>
     	<input type=hidden name='deleted' value=''>
		<table width=780 cellpadding=0 cellspacing=5 border=0>
			<tr>
				<td colspan=4 align="center" class=graybluetext10><b><font face="Verdana, Arial, Helvetica, sans-serif" size=2>Edit/Delete Exam Schedule</font></b><br><br></td>
			</tr>		
			<tr>
				<td width=15%><br></td>
				<td valign=top class=greybluetext10 width=25% align=right><b>Exam&nbsp;&nbsp;: </b></td>
				<td valign=top class=greybluetext10 width=35% align=left>
					<select name="exam_code" class="textbox" style={width:150px} onChange='javascript:getSubjects()'>
						<option value=''>--Select--</option>
						<?php
						
						if (mysql_num_rows($resExams) > 0)
						{
							while (list($eCode, $eName) = mysql_fetch_row($resExams))
							{
								print("<option value='$eCode'");
								if ($examCode == $eCode)
								{
									print(" selected ");
								}
								print(">".$eCode."->".$eName."</option>\n");
							}
						}						
						?>
					</select>
				</td>
				<td width=25%><br></td>
			</tr>
			<tr>
				<td width=15%><br></td>
				<td valign=top class=greybluetext10 width=25% align=right><b>Subject&nbsp;&nbsp;: </b></td>
				<td valign=top class=greybluetext10 width=35% align=left>
					<select name="subject_code" class="textbox" style={width:150px} onChange='javascript:getDate()'>
						<option value=''>--Select--</option>
						<?php
						
						if ($nSubjects > 0)
						{
							while (list($sCode, $sName) = mysql_fetch_row($resSubjects))
							{
								print("<option value='$sCode'");
								if ($subjectCode == $sCode)
								{
									print(" selected ");
								}
								print(">".$sCode."->".$sName."</option>\n");
							}
						}						
						?>
					</select>
				</td>
				<td width=25%><br></td>
			</tr>
			<tr>
				<td width=15%><br></td>
		    	<TD valign=top class=greybluetext10 align=right><b>Date&nbsp;&nbsp;: </b></td>
		    	<td valign=top class=greybluetext10 align=left>
	    			<select name="sDate" class=textbox>
					<?php
						if ($sDay != "")
						    $chk = $sDay;
						else
							$chk = date("d");
						for ($i = 1; $i<=31 ;$i++){
						 echo '<option value= '.$i;
						 if ($i == $chk)
						 	echo ' selected';
						echo '>'.$i.'</option>';
					} ?>
				</select>
				<select name="sMonth" onchange="set_cor_date()" class=textbox>
					<?php 
					if ($sMonth != "")
					    $chk = $sMonth;
					else
						$chk = date("m");
					for ($i = 1;$i<=12;$i++)
					{
						 echo '<option value= '.$i;
						 if ($i == $chk)
						 	echo ' selected';
						 echo '>'.date("F",mktime(0,0,0,$i,1,date("Y"))).'</option>';
					 }
					 ?>
				</select>
				<select name="sYear" onchange="set_cor_date()" class=textbox>
					<?php
					if ($sYear)
					    $chk = $sYear;
					else
						$chk = date("Y");
					for ($i = -3; $i <= 3;$i++){
					echo '<option value='.($i+date("Y"));
					if (($i+date("Y"))== $chk)
						echo ' selected';
					echo '>'.($i+date("Y")).'</option>';
					} ?>
				</select><br><br></TD>
				<td width=25%><br></td>
			</tr>
			<tr>
				<td colspan=4 align="center"><input type=button name="sub_update" value="Update" class=button onClick='javascript:validate()'>&nbsp;&nbsp;<input type=button name="sub_delete" value="Delete" class=button onClick='javascript:del()'>&nbsp;&nbsp;<input type="reset" class=button name="sub_reset" value="Reset"></td>
			</tr>
			<?php
			if ($errormsg != "")
			{
				print("<tr>
							<td colspan=4 align=center class='errormsg'>$errormsg</td>
						</tr>");
			}
			if ($msg != "")
			{
				print("<tr>
							<td colspan=4 align=center class='alertmsg'>$msg</td>
						</tr>");
			}
			?>
		</table>
		</form>
		</TD>
	</TR>
	<TR>
		<?include("includes/footer.php");?>
	</TR>
</TABLE>
</FORM>
</center>
</BODY>
</HTML>

<script language="javascript">
frm = document.addSchedule;
function set_cor_date()
{
	var yr,month,day;
 	day=31;
 	yr= frm.sYear.options[frm.sYear.selectedIndex].value; 
 	month=(frm.sMonth.options[frm.sMonth.selectedIndex].value);
 	if(month == 2)
 	{
		var isleap = (yr % 4 == 0 && (yr % 100 != 0 || yr % 400 == 0));
		if(!isleap)       
        	day=28;
		else
    		day=29;
 	}
 	if (month==4 || month==6 || month==9 || month==11)
 		day=30;
 
 	for(i=1;i<=((frm.sDate.options.length < day)?frm.sDate.options.length:day);i++)
 	{
 		frm.sDate.options[i-1].value=i;
    	frm.sDate.options[i-1].text=i;
 	}
 	if (day > frm.sDate.options.length)
 	{
		for (i = frm.sDate.options.length + 1;i<= day;i++)
		{
			dayopt = new Option(i,i,false,false);	 
			frm.sDate.options[frm.sDate.length] = dayopt;
		}	
 	}
 	for (j=frm.sDate.length-1;j>=day;j--)
 		frm.sDate.options[j]= null;
}
 
function submitpage()
{
 	frm.action="<?=$_SERVER['PHP_SELF'] ?>";
	frm.submit();
 }
</script>
