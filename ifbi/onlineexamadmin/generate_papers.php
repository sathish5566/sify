<?php

/****************************************************
* Application Name            :  IIB
* Module Name                 :  Qp Generate Page
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  
* Created ON                  :  
* Last Modified By            :  B.Devi
* Last Modified Date          :  27-09-2006
* Description                 :  After QP generate, the message will be displayed in this IM
*****************************************************/
$admin_flag=1;
require("sessionchk.php");
//require("constants.inc");
require_once("dbconfig.php");

//print_r($_POST);
 set_time_limit(0);
 $examCode = $_POST['exam_code'];
 $subjectCode = $_POST['subject_code'];
 $nPapers = $_POST['total_question_paper'];
 $totalMarks = $_POST['total_marks'];
 //$sectionCount = $_POST['section_count'];
 $sectionCount = $_POST['general_section_count'];
 $case_sectionCount = $_POST['case_section_count'];
 $isSample = $_POST['chk_sample'];
  

//$arrCasesection=array();
$aStoredMarks = array();
/*$qryCase="SELECT section_code from iib_subject_sections where subject_code='$subjectCode' and section_type='C'";
$res=mysql_query($qryCase);
while($r=mysql_fetch_object($res)){
	array_push($arrCasesection,$r->section_code);
}
mysql_free_result($res);*/
$impCase=implode(',',$arrCasesection);	
$qrymark="SELECT distinct marks from iib_section_questions where exam_code='$examCode' and subject_code='$subjectCode' and section_code NOT IN ('$impCase')";
$res=mysql_query($qrymark);
while($r=mysql_fetch_object($res)){
	array_push($aStoredMarks,$r->marks);		
}
mysql_free_result($res);
$nStoredMarks=count($aStoredMarks);

?>
<html>
<head>
    <title>Generate Question Paper</title>
</head>
<link rel="stylesheet" href="images/iibf.css" type="text/css">
<body leftmargin="0" topmargin="0">
<center>
<form name=frmgetsubjects method=post>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" >
    <tr><td ><?include("includes/header.php");?></td></tr>
    <tr>
      <td bgcolor="7292C5">&nbsp;</td>
    </tr>
    <tr>
		<Td background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
  </tr>
    <tr>
      <td width="100%"  valign="top">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="24"> <div align="center" ><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Admin
                module - Question Paper(s) Generation</font> </strong></div></td>
          </tr>
          <tr>
            <td height="200" valign="middle" align="center">
<?php
   if ($isSample == "")
    {
        $isSample = 'N';
    }
    $aMarks = array();
    $acMarks = array();
    if ($sectionCount > 0) {
	    for ($j = 0; $j < $nStoredMarks; $j++)
	    {
	        for ($i = 0; $i < $sectionCount; $i++)
	        {
	            $var = "question_count".$i.$j;
	          //  print "<br>General ". $var. " Value ". $_POST["$var"];
	            $aMarks[$j] == "" ? $aMarks[$j] = $j.":".$_POST["$var"] : $aMarks[$j] = $aMarks[$j].",".$_POST["$var"];
	        }
	    }
   }
	// Case Questions
//    print_r($aMarks);
    if (count($aMarks) > 0 or count($acMarks) > 0 )
    {
        $strMarks = implode($aMarks,"-");
        $strCaseMarks = implode($acMarks,"-");
        //print $strMarks;
        //$strAttributesList = "$examCode $subjectCode $nPapers $totalMarks $sectionCount $strMarks $strCaseMarks $isSample";
        //print("/usr/bin/php offline_generate.php $examCode $subjectCode $nPapers $totalMarks $sectionCount $strMarks $isSample &");
        //exec("/usr/local/bin/php offline_generate.php $examCode $subjectCode $nPapers $totalMarks $sectionCount $strMarks $isSample");
        require("offline_generate.php");
        print("<div class='alertmsg'><center>Question Paper(s) Generated<br><!--<a href='get_subject.php?exam_code=$examCode'>Select a different subject</a>&nbsp;&nbsp;<a href='get_sections.php?exam_code=$examCode&subject_code=$subjectCode'>Select different weightages</a>--></center></div>");
    }
    else
    {
        print("<div class='alertmsg'><center>Please Contact the System Administrator</center></div>");
//        return;
    }
?>

          </td>
          </tr>
        </table>
        </td>
    </tr>
    <tr>
      <td width="780" bgcolor="7292C5">&nbsp;</td>
    </tr>

</table>
</form>
</center>
</body>
</html>
