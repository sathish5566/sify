<?php
/****************************************************
* Application Name            :  IIB
* Module Name                 :  Load distribution Report
* Revision Number             :  1
* Revision Date               :
* Table(s)  modified                  :  
* Tables used for only selects:  iib_exam_centres,iib_exam,iib_candidate_iway,iib_candidate,iib_exam_schedule,iib_exam_slots 
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              : CSV file & HTML Page
* Document/Reference Material :
* Created By : Anil Kumar.A
* Created On : 21/03/2006 
* Last Modified By            :  
* Last Modified Date          : 
* Description                 :  Report displaying the load (Mapped)
*****************************************************/
ob_start();
	require("dbconfig.php");
	require("sessionchk.php");
	$commondebug=false;
	$examDate = $_POST['exam_date'];
	
	//getting values when download button is clicked
	
	$ex_flag=isset($_POST['exl']) ? $_POST['exl'] : '0' ;
	//Generic excel file included
	
	include_once("excelconfig.php");
	$filename="Load_distribution_report_ExamDate(".$examDate.")";
	
	//No_of_exams hard coded
	$no_of_exams=3;
	
	//Ratio Hard coded for for calculating actual seats
	$ratioArr[17]=0.80;
	$ratioArr[1]=0.80;
	$ratioArr[21]=0.80;
	
	
	//Exam date selected for drop down
	$sqlDate = "SELECT distinct exam_date FROM iib_exam_schedule order by exam_date ";
	$resDate = mysql_query($sqlDate);
	// Header for Excel
	/*
	$excel_header[0]="City";
	$excel_header[1]="08:30 DBF";
	$excel_header[2]="08:30 JAIIB Old";
	$excel_header[3]="08:30 JAIIB New";
	$excel_header[4]="11:15 DBF";
	$excel_header[5]="11:15 JAIIB Old";
	$excel_header[6]="11:15 JAIIB New";
	$excel_header[7]="14:00 DBF";
	$excel_header[8]="14:00 JAIIB Old";
	$excel_header[9]="14:00 JAIIB New";
	$excel_header[10]="16:30 DBF";
	$excel_header[11]="16:30 JAIIB Old";
	$excel_header[12]="16:30 JAIIB New";
	*/

	// Exam time slot selected 	
	$sql = "SELECT DISTINCT slot_time FROM iib_exam_slots ORDER BY slot_time";
	$res = mysql_query($sql);
	$aSlots = array();
	while ($row = mysql_fetch_row($res))
	{
		$aSlots[] = $row[0];
	}
	
	//Exam code & exam name selected
	$sql = "SELECT  exam_code, exam_name FROM iib_exam ORDER BY exam_name ";
	$res = mysql_query($sql);
	$aExams = array();
	while ($row = mysql_fetch_row($res))
	{
		$aExams[$row[0]] = $row[1];
	}
	
	//Exam city code & exam city name selected
	$sql = "SELECT  exam_centre_code, exam_centre_name FROM iib_exam_centres  ORDER BY exam_centre_name ";
	$res = mysql_query($sql);
	$aExamCentres = array();
	while ($row = mysql_fetch_row($res))
	{
		$aExamCentres[$row[0]] = $row[1];
	}
	
	//Fetching count using the tables iib_candidate , iib_candidate_iway
	/*$sqlSelect = "select  count(1),b.exam_centre_code,exam_date,exam_time,exam_code  from iib_candidate_iway a, iib_candidate b where a.membership_no=b.membership_no group by  b.exam_centre_code,exam_date, exam_time, exam_code";*/
	$sqlSelect = "select  count(1),b.exam_centre_code,exam_date,exam_time,exam_code  from iib_candidate_iway a,  iib_iway_details b where a.centre_code=b.centre_code and a.exam_date='$examDate' group by  b.exam_centre_code, exam_time, exam_code";
	$resSelect = mysql_query($sqlSelect);
	while(list($count,$E_C_Code,$E_Date,$E_Time,$E_Code)=mysql_fetch_row($resSelect))
	{
		$countArr[$E_C_Code][$E_Date][$E_Time][$E_Code]=$count;
	}
	
	//Storing all the values into 2D Array - $resultArr
$a=0;
foreach ($aExamCentres as $examCentreCode=>$examCentreName)
{
	$excel_header[0]=City;
	$resultArr[$a][0]=$examCentreName;
	$b=1;
	foreach ($aSlots as $examTime)
	{
		foreach ($aExams as $examCode=>$examName)
		{
			$timeArr=explode(':',$examTime);
			$time=$timeArr[0].":".$timeArr[1];
			$excel_header[$b]="$time $examName";
			$cnt1=$countArr[$examCentreCode][$examDate][$examTime][$examCode];
			if($cnt1=='') $cnt1=0;
			$resultArr[$a][$b]=$cnt1;
			$b++;
			$totalArr[$examTime][$examCode]+=$cnt1;
		}
	}
	$a++;

}

$b=1;
$resultArr[$a][$b-1]="Total";
$resultArr[$a+1][$b-1]="Actual Seats Utilised";
$resultArr[$a+2][$b-1]="Total for the slot";
foreach ($aSlots as $examTime)
	{
		$temp=0;
		foreach ($aExams as $examCode=>$examName)
		{
			$resultArr[$a][$b]=$totalArr[$examTime][$examCode];
			$resultArr[$a+1][$b]=$totalArr[$examTime][$examCode] * $ratioArr[$examCode];
			$temp+=$resultArr[$a+1][$b];
			$resultArr[$a+2][$b]='';
			$b++;
		}
		$resultArr[$a+2][$b-1]=$temp;
	}
$cnt_addnl=$a;
/*echo "<pre>";
print_r($resultArr);
echo "</pre>";*/
$cnt_result=count($resultArr);
$headerLength=count($excel_header);

// Calculating the total count for each slot
/*for($l=1;$l<$headerLength;$l++)
{
	for($m=0;$m<$cnt_result;$m++)
	{
		$total[$l]+=$resultArr[$m][$l];
	}
}

//  Calculating the actual count for each slot
for($i=1;$i<$headerLength;$i++)
{
	$arrCalc=($i-1)%3;
	if($arrCalc == 0)
	{
		$ratio=$ratioArr[$arrCalc];
	}
	else if($arrCalc == 1)
	{
		$ratio=$ratioArr[$arrCalc];
	}
	else if($arrCalc == 2)
	{
		$ratio=$ratioArr[$arrCalc];
	}
	$actualArr[$i]=$total[$i]  * $ratio ;
}  
// Calculating the total for different slots
$temp=0;
for($i=1;$i<$headerLength;$i++)
{
	$temp+=$actualArr[$i];
	$total_actual[$i]="";
	$i++;
	$temp+=$actualArr[$i];
	$total_actual[$i]="";
	$i++;
	$temp+=$actualArr[$i];
	$total_actual[$i]=$temp;
	$temp=0;
}*/

//checking if the excel flag is set or not
if($ex_flag=='1')
{
		// Calling the excelconfig function fromthe included file excelconfig.php
		excelconfig($filename,$excel_header,$resultArr);
		/*echo "\n";
		echo "Allocated,";
		
		// Loop to display the total count for each slot and exam
		for($i=1;$i<=$headerLength;$i++)
		{
			if($i!=$headerLength)
			{
				echo $total[$i].",";
			}
			else
			{
				echo $total[$i]."\n";
			}
		}		
		
		
		echo "\n";
		echo "Actual seats utilised,";
		for($i=1;$i<=$headerLength;$i++)
		{
			if($i!=($headerLength))
			{
				echo $actualArr[$i].",";
			}
			else
			{
				echo $actualArr[$i]."\n";
			}
		}
		echo "\n";
		echo "Total for the slot,";
		
		// Loop to display the total count for each slot
		for($i=1;$i<=$headerLength;$i++)
		{
			if($i!=$headerLength)
			{
				echo $total_actual[$i].",";
			}
			else
			{
				echo $total_actual[$i]."\n";
			}
		}*/		
		exit;
}
    
	?>
	<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
	FONT-FAMILY: Arial, Verdana	
}
.textblk11     { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; text-decoration: none}
</STYLE>
<script language='JavaScript'>
function submitForm()
{
	if (document.frm.exam_date.selectedIndex == 0)
	{
		alert("Please select the exam date");
		document.frm.exam_date.focus();
		return false;
	}
	document.frm.exl.value='0';
	document.frm.submit();
}
function excelsubmit()
{
	//alert("hi");
	var frmname=document.frm;
	frmname.exl.value='1';
	frmname.submit();
}
</script>
</head>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
<!--	<TR><!-- Topnav Image -->
   <!-- 	<td class=greybluetext10 class=greybluetext10 width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR>	 -->
	<tr><td width="780"><?include("includes/header.php");?></td></tr>
	<TR>
    	
	</TR>
	<tr>
		<td class=greybluetext10 class=greybluetext10 class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>	
	<TR>
	    <td class=greybluetext10 class=greybluetext10 background=images/tile.jpg vAlign=top width=780 align=center>
<form name='frm' method='post'>
<table width=100% border=0 cellspacing=0 cellpadding=5>
	<tr><td class=greybluetext10 class=greybluetext10 align=center><div align=center><b>Load Distribution Report</b></div></td></tr>
	<tr>
		<td class=greybluetext10 class=greybluetext10 align=center class=textblk11>Exam Date : 
			<select name='exam_date' style={width:150px} class=textblk11>
				<option value=''>--Select--</option>
<?php
if (mysql_num_rows($resDate) > 0)
{
	while (list($eDate) = mysql_fetch_row($resDate))
	{
		$aDate = explode("-",$eDate);
		$dispDate = $aDate[2]."-".$aDate[1]."-".$aDate[0];
		print("<option value='$eDate' ");
		if ($eDate == $examDate)
			print(" selected ");
		print(">$dispDate</option>");
	}
}

?>
		</td>
	</tr>
	<tr>
		<td class=greybluetext10 class=greybluetext10 align=center><input class='button' type='button' name='sub' value='Submit' onClick='javascript:submitForm()'></td>
	</tr>
	
</select>

<input type=hidden name=exl value='0'>
</table>
	<?
			
//Checking if the date is null or not
if ($examDate != "")
	{
		//checking the excel flag is set or not
		if($ex_flag!='1')
		{
			// Printing the values in the HTML page 
			
			// Printing the header values from excel header array 
		print("<table border=1 cellspacing=0 cellpadding=2><tr>");
		for($a=0;$a<$headerLength;$a++)
		{
			print("<td class=greybluetext10><b>$excel_header[$a]</b></td>");
		}
		print("</tr>");
		
		// Loop to display the values in the HTML page
		print("<tr>");
			for($i=0;$i<$cnt_result;$i++)
			{
					for($j=0;$j<$headerLength;$j++)
					{
						echo "<td class=greybluetext10 >";
						if(($i >= $cnt_addnl))
						{
							echo "<b>";
						}
						echo "&nbsp;".$resultArr[$i][$j]."</b></td>";
							
					}
				print("</tr>");
			}
			/*print("<tr><td class=greybluetext10 class=greybluetext10><b>Total</b></td>");
			for($i=1;$i<$headerLength;$i++)
			{*/
				?>
					<!--<td class=greybluetext10 class=greybluetext10><b><?=$total[$i]?></b></td>-->
					
			<?//}		
			/*print("</tr>");
			print("<tr><td class=greybluetext10 class=greybluetext10><b>Actual Seats Utilised</b></td>");
			for($i=1;$i<$headerLength;$i++)
			{*/?>
					<!--<td class=greybluetext10 class=greybluetext10><b><?=$actualArr[$i]?></b></td>-->
					
			<?//}		
			/*print("</tr>");		
			print("<tr><td class=greybluetext10 class=greybluetext10><b>Total for the slot</b></td>");
			for($i=1;$i<$headerLength;$i++)
			{
			if($total_actual[$i]!=''){*/
			?>
					<!--<td class=greybluetext10 class=greybluetext10><b><?=$total_actual[$i]?></b></td>-->
					
			<?
			//}else if($total_actual[$i]==''){
			?>
			<!--		<td class=greybluetext10 class=greybluetext10><b>&nbsp;</b></td>-->
			<?/*}
			}*/		
			//print("</tr>");		
		print("</table>");
?>
<table>
 </tr>
 		<tr><td class=greybluetext10>&nbsp;</td></tr>
         <tr><td class=greybluetext10 align="center" class=greybluetext10><input class='button' type='button' name='download' value='Download' onClick='javascript:excelsubmit()'></td></tr>
            <tr><td class=greybluetext10>&nbsp;</td></tr>
</table>
<?	
}
}
?>
<TR>
		                  	<?include("includes/footer.php");?>
	</TR>
</TABLE>
</center>
</BODY>
</HTML>
