<?
ob_start();
$admin_flag = 0;
require_once("dbconfig.php");

class TEXT_Table
{
var $widths;
var $page_height=41;
var $page_width=60;
var $current_height=0;
var $current_width=0;
var $new_page_flag=0;
var $print_page = array();
var $current_page_line = 1;
function TEXT_Table(){
	$page_height=41;
	$page_width=60;
	$current_width=0;
	$current_height=0;
	$new_page_flag=0;
}

function SetWidths($w)
{
    //Set the array of column widths
    $this->widths=$w;
}

function total_length(){
	$text_value = implode ('',$this->print_page);
	return strlen($text_value);
}

function Row($data,$char_val,$align)
{
    $this->CheckPageBreak($h);
    $max_height=0;
    $max_data =count($data);
    $max_lines = $this->max_lines($data);
    unset($text_array);
    unset($temp_array);
    for($i=0;$i<$max_data;$i++){
	$text_value = wordwrap($data[$i],$this->widths[$i],'\-/');
        $text_array[$i] = explode('\-/',$text_value);
	for ($j=0;$j<$max_lines;$j++) {
		$temp= '';
		if ($i==0)
			$text_array[$i][$j]=ltrim($char_val).$text_array[$i][$j];
		$text_len = (array_key_exists($j,$text_array[$i]))?strlen($text_array[$i][$j]):0;
		$str_len_to_pad = $this->widths[$i] - $text_len;
		$temp=str_pad($temp,$str_len_to_pad,' '); 
       		if ($char_val != '')
			$temp .= $char_val;
               	$text_array[$i][$j] .= $temp;
	        if ($max_data - 1 == $i)
        	        $text_array[$i][$j] .= "\r\n";
	}
    }
    for($i=0;$i<$max_lines;$i++) {
	$text_to_print='';
	for($j=0;$j<$max_data;$j++) {
		$text_to_print .= $text_array[$j][$i];
	}
	if ($this->current_page_line > $this->page_height){
                $this->end_page();
                $this->add_page();
        }
	$this->print_page[$this->current_height++]=$text_to_print;
	$this->current_page_line++;
    }
}

function max_lines($data) {
	$max_lines = 0;
    for($i=0;$i<count($data);$i++){
 		$text_value = wordwrap($data[$i],$this->widths[$i],'\-/');
	    $text_array = explode('\-/',$text_value);
	    if ($max_lines < count($text_array))
			$max_lines = count($text_array);
	}
return $max_lines;
}

function put_line(){
        if ($this->current_page_line > $this->page_height){
                $this->end_page();
                $this->add_page();
        }
	$text_to_print = '';
	$print_length = array_sum($this->widths)+(count($this->widths) * 3)-1;
	for ($i=0;$i < $print_length; $i++)
		$text_to_print .= '-';
	$this->print_page[$this->current_height++]=$text_to_print." \r\n";
	$this->current_page_line++;
}
function show_values(){
	$result_value = implode ('',$this->print_page);
	echo $result_value;
}

function CheckPageBreak($h)
{
	if ($this->current_page_height > $this->page_height){

	}
}
function PutLineBreak(){
        if ($this->current_page_line > $this->page_height){
                $this->end_page();
                $this->add_page();
        }
	$this->print_page[$this->current_height++] = "\r\n";
	$this->current_page_line++;
}

function Footer(){
 $this->print_page[$this->current_height++]= chr(12);
  for($i=0;$i<13;$i++){
	   $this->print_page[$this->current_height++]= "-F $i - \r\n";
}
}
function Header(){
  for($i=0;$i<12;$i++){
	   $this->print_page[$this->current_height++]= "-H $i -  \r\n";
	}
}
function end_page(){
// $this->print_page[$this->current_height++]= chr(12);
/*	for ($i=$this->current_page_line;$i <= $this->page_height; $i++){
		//$this->print_page[$this->current_height++]= "\r\n";
		$this->print_page[$this->current_height++]= "- Excess - $i - \r\n";
	}
	$this->Footer();
	$this->current_page_line=1; */
}
function add_page(){
//	$this->Header();
}
}

$exam_centre= $_REQUEST["sCentreCode"];
$Centre_name = $_REQUEST["cc"];
$date_print=$_REQUEST["dt"];
$time_print=$_REQUEST["tm"];
$date_option=$_REQUEST["dto"];
if($date_option=='selected')
	$date_array[0]= $date_print;
elseif($date_option=='all')
	{
		$sql = "select distinct exam_date from iib_candidate_iway";
		$res = mysql_query($sql) or die ("Connection cannot be done error:'".mysql_error ($db)."'");
		$it=0;
		while($num = mysql_fetch_row($res))
		{
			$date_array[$it]=$num[0];
			$it++;
		}
	}
	
if($time_print != "All")
	$time_array[0]=$time_print;
else
{
	$sql = "select slot_time from iib_exam_slots";
	$res = mysql_query($sql) or die ("Connection cannot be done error:'".mysql_error ($db)."'");
	$it=0;
	while($num = mysql_fetch_row($res))
		{
			$time_array[$it]=$num[0];
			$it++;
		}
}
$iWay_Code="";

$txt=new TEXT_Table();

$sql_exam_centre="select exam_centre_name from iib_exam_centres where exam_centre_code='$exam_centre'";
$res_exam_centre=mysql_query($sql_exam_centre);
list($exam_centre_name)=mysql_fetch_row($res_exam_centre);



$sql_iways = "select centre_code from iib_iway_details where exam_centre_code='$exam_centre'";
$res_iways = mysql_query($sql_iways);
$cen_co=0;
$cen_co1=0;
while (list($iWay_Code)=mysql_fetch_row($res_iways)) {
	if($cen_co==0)
	{
		$cen_co=1;
	  	$cen_start=$iWay_Code;
  	}
	$cen_end=$iWay_Code;
	for($tmp=0;$tmp < count($date_array);$tmp++) {
	for($tmp_time=0;$tmp_time < count($time_array);$tmp_time++) {
		$Date=$date_array[$tmp];
		$Time=$time_array[$tmp_time];
		if ($Date != "")
		{
        		$aDisp = explode("-",$Date);
        		$dispDate = $aDisp[2]."/".$aDisp[1]."/".$aDisp[0];
		}
		if($cen_co1==0)
		{
			$cen_co1=1;
	  		$date_start=$aDisp[2];
  		}
		$date_end=$aDisp[2];
	$txt->PutLineBreak();

	$sql="select a.membership_no ,b.name, c.exam_name,b.password from iib_candidate_iway a , iib_candidate b, iib_exam c where a.exam_date = '$Date' and a.exam_time = '$Time' and a.centre_code = '$iWay_Code'and  b.membership_no = a.membership_no and c.exam_code = a.exam_code";
	
	$res=mysql_query($sql);
	$num_of_rows=mysql_num_rows($res);
	$emsg="";
	if (!$res)
		$emsg="Sorry Details Cannot Be Retrieved.Please Enter the Correct Data";
	$ncflag=0;
	if ($num_of_rows == 0)
	{
		$emsg="No Candidate is assigned";
		$ncflag=1;
	}

	$iWaySql = "select iway_name, iway_address1, iway_address2, iway_city, iway_state, iway_pin_code from iib_iway_details where centre_code='$iWay_Code'";
	$iWayRes = mysql_query($iWaySql);
	if (!$iWayRes)
		$emsg="Sorry Details Cannot Be Retrieved.Please Enter the Correct Data";
	$iwaycnt = mysql_num_rows($iWayRes);
	if ($iwaycnt == 0)
		$emsg="iWay Details not found";
	list($iWay_name,$iway_address1, $iway_address2, $iway_city, $iway_state, $iway_pin_code) = @mysql_fetch_row($iWayRes);
	$address = "";
	$address1 = "";
	$address2 = "";
	if ($iWay_name != "")
		$address = $iWay_name.",";
	if (($address != "") && ($iway_address1 != ""))
		$address2 .= " ".$iway_address1;
	if (($address != "") && ($iway_address2 != ""))
		$address1 .= " ".$iway_address2;
	if (($address != "") && ($iway_city != ""))
		$address1 .= " ".$iway_city;
	if (($address != "") && ($iway_state != ""))
		$address1 .= " ".$iway_state;
	if (($address != "") && ($iway_pin_code != ""))
		$address1 .= " ".$iway_pin_code;
	if($ncflag == 0)
	{
	$txt->PutLineBreak();
	$txt->SetWidths(array(25,50));
	$txt->Row(array('',"Candidate Attendence Sheet"),'','C');
	$txt->PutLineBreak();
	$txt->SetWidths(array(15,5,20));
	$txt->Row(array("Centre Code",":",$iWay_Code),'','L');
	$txt->PutLineBreak();
	$txt->SetWidths(array(15,5,75));
	$txt->Row(array("iWay",":",$address),'','L');
	$txt->SetWidths(array(15,5,75));
	$address2 = ltrim($address2);
	$txt->Row(array('','',$address2),'','L');
	$txt->SetWidths(array(15,5,75));
	$address1 = ltrim($address1);
	$txt->Row(array('','',$address1),'','L');
	$txt->PutLineBreak();
	$txt->SetWidths(array(15,5,20));
	$txt->Row(array("Date",":",$dispDate),'','L');
	$txt->PutLineBreak();
	$txt->SetWidths(array(15,5,20));
	$txt->Row(array("Time",":",substr($Time,0,5)." hrs"),'','L');
	$txt->PutLineBreak();
	if ($emsg != "") {
		$txt->SetWidths(array(100));
	$txt->Row(array($emsg),'','L');
	}
	else {
		if ($emsg =="") {
			$txt->SetWidths(array(15,35,20,10));
			$txt->put_line();
			$txt->Row(array("Member Number","Member Name","Exam Name","Password"),' | ','C');
			$txt->put_line();
			while ($num=mysql_fetch_row($res)) {
				$txt->SetWidths(array(15,35,20,10));
				$txt->Row(array($num["0"],$num["1"],$num["2"],$num["3"]),' | ','C');
				$txt->put_line();
			}
			$txt->PutLineBreak();
		}
	}
	$txt->Row(array(chr(12)),'','');

}	 
}
}
}
$strFileName = "ASP_".$exam_centre_name.'_'.$cen_start.'_'.$cen_end.'_'.$date_start."_".$date_end.'.txt';
if(isset($HTTP_ENV_VARS['HTTP_USER_AGENT']) and strpos($HTTP_ENV_VARS['HTTP_USER_AGENT'],'MSIE 5.5'))
	        Header('Content-Type: application/dummy');
        else
       		 Header('Content-Type: application/dummy');
   $text_length = $txt->total_length(); 
    Header('Content-Length:'.$text_length+10000);
        Header('Content-disposition: attachment; filename='.$strFileName);
$txt->show_values();
ob_end_flush();
?>
