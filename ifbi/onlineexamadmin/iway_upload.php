<?

/****************************************************
* Application Name            :  IIB IWAY BULKUPLOAD
* Module Name                 :  Iway Upload Page 
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  Koshy.K.I.
* Created ON                  :  18/03/2006.
* Last Modified By            :  B.Devi
* Last Modified Date          :  29-03-2006
* Description                 :  Interface to upload the excel.
*****************************************************/
 ob_start();

	require_once("sessionchk.php");
	require_once("dbconfig.php");
	require_once("includes/iway_functions.php");
	require_once("includes/settings.php"); 
	
	$flag	= isSet($_POST["flg_submit_chk"]) ? $_POST["flg_submit_chk"] : "";
	$exam_date 	= isSet($_POST["cbo_exam_date"]) ? $_POST["cbo_exam_date"] : "";	

	/*key is got when clicking the url to know the type of upload : This is hardcoded in sify_admin_menu.php*/
	$source_key	= isSet($_POST["key"]) ? $_POST["key"] : "";
	if($source_key=="")
	{	
		$source_key	= isSet($_GET["key"]) ? $_GET["key"] : "";
	}
	if($source_key=="")
	{
		header("Location: errorpage.php?status=13");			
	}
	
		
	$language = "E";//$language is harcoded here
	
	if($flag == "frm_submitted")
	{
			$file = fileupload($_FILES,$filenamepath,$exam_date);
	}
	

		
?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
.textbox        
{ 
font-family: Verdana, Arial, Helvetica, sans-serif; 
font-size: 10px; font-style: normal; 
text-decoration: none; 
height: 17px; 
border: 1px #000000 solid
}
</STYLE>
<script language='JavaScript' src="./includes/validations.js"></script>
<script language="javascript">

function alert_msg()
{	
	var frm=document.frm_upload_iway;		
	alert('Please browse the file');
	return false;
}

function frm_submit()
	{
		var frm=document.frm_upload_iway;
	
		if(frm.cbo_exam_date.selectedIndex == 0)
		{
			alert ("Please Select Date");
			frm.cbo_exam_date.focus();
			return;
		}
		if (frm.iwayexcelfile.value == '')
		{
			alert('Please Select The File');
			frm.iwayexcelfile.focus();
			return;
		}
		if (frm.iwayexcelfile.value == '.xls')
		{
			alert('Please Select a valid File');
			frm.iwayexcelfile.focus();
			return;
		}
		if(!LimitAttach(frm, frm.iwayexcelfile.value))
		{
			return;
		}
		
		frm.action="iway_upload.php";
		frm.flg_submit_chk.value='frm_submitted';
		frm.submit();
	}
</script>
</HEAD>
<BODY leftMargin=0 topMargin=0 LeftMargin=0 marginheight=0 marginwidth=0>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
<!--	<TR><!-- Topnav Image -->
   <!--     <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR> -->
    <tr><td width="780"><?include("includes/header.php");?></td></tr>
    <TR>
        
    </TR>
	<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>
    <TR>
    	<TD background=images/tile.jpg vAlign=top width=780 align=center>
    	<form name='frm_upload_iway' method="post" enctype="multipart/form-data" onsubmit="return false;">        
    	<input type="hidden" name="key" value="<?echo $source_key?>">
        <table width=780 cellpadding=0 cellspacing=5 border=0>
        <?
        	if($file){
        ?>
        	<tr>
				<td colspan=2 align="center"></b><?= $file;?><br><br></td>
            </tr>
         <?
			}         	
         ?>   
        	<tr>
				<td colspan=2 align="center"></b><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2>Upload Iway List</b></font><br><br></td>
            </tr>
             <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Select Date :</td>
            	<td align="left" valign=top class="greybluetext10">
<?
					$res = getdistinctDates();
					if(!$res)
					{
						$emsg="Problem in selecting records.";
					}
?>
            		<select name='cbo_exam_date' class="greybluetext10" style={width:150px} >
            		<option value=select selected>--select--</option>
<?
					while(list($exam_date) = mysql_fetch_row($res))
					{
							echo "<option value=\"$exam_date\">$exam_date</option>";
					}
?>
            		</select>
            	</td>
            </tr>
             <tr>
				<td align="right" width=390 class="greybluetext10" valign=top>Upload Iway Details :</td>
                <td align="left" valign=top class="greybluetext10">
					<input name="iwayexcelfile" type="file"  class="textbox" onkeypress='return alert_msg()'>
				</td>
			</tr>
			<tr>
				<td colspan=2 align=center><br>
				<!--- Hidden flag variable to check whether the form is submit or not --->
				<input type=hidden name=flg_submit_chk value=''>
				<input type="submit" class="button" value="Upload File" onclick='javascript:frm_submit()'>
			</td>
			</tr>
			
			 <tr>
			
		        <td colspan=2 class="greybluetext10">Please refer to the 
		          <a href="download/iway_upload.xls" target="_blank">Template </a> for Iway Details Upload </td>
		      </tr>
		      <tr>
			
		        <td colspan=2 class="greybluetext10">Please refer to the 
		          <a href="download/validation_iwayupload.doc" target="_blank">Document </a> for Iway Details Upload </td>
		      </tr>

			</table>
        </form>
        </TD>
	</TR>
    <TR>
    	<?include("includes/footer.php");?>
    </TR>
</TABLE>
</FORM>
</center>
</BODY>
</HTML>
 <?ob_flush();?>
