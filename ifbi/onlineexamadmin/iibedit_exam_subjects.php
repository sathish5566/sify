<?php
$admin_flag=1;
require_once "sessionchk.php";
require_once "dbconfig.php";

if(!isset($_POST['submitted']))
	$submitted='false';
else
	$submitted=$_POST['submitted'];

$edited=$_POST['edited'];
$deleted=$_POST['deleted'];

if($submitted=='false' && $edited=='true')
{
	$SubjectName=$_POST['S_name'];
	$Exam_Code=$_POST['Exam_code'];
	$S_online=$_POST['E_status'];
	$subjectCode = $_POST['S_Code'];
	
	$SubjectName=trim($SubjectName);
	$SubjectName=addslashes($SubjectName);
	$passMark = trim($_POST['passmark']);
	$totalMarks = trim($_POST['totalmarks']);
	
	$chksql="select count(1) from iib_exam_subjects where online='Y' and exam_code='"
			.$Exam_Code."' and subject_name='".$SubjectName."' and subject_code !='$subjectCode'";	
	$chkres=mysql_query($chksql);
	list($num)=mysql_fetch_row($chkres);

	if($num==0)
	{		
		$sqlcount = "SELECT COUNT(question_paper_no) FROM iib_question_paper WHERE exam_code='$Exam_Code' AND subject_code='$subjectCode'";
		$rescount = mysql_query($sqlcount);
		list($rowcount) = mysql_fetch_row($rescount);
		$hidTotalMarks = $_POST['hid_total_marks'];
		//print $sqlcount;
		if ($rowcount > 0)
		{						
			//print("<script language=JavaScript>alert('Total marks cannot be modified as question papers have already been generated for this subject!');</script>");
			$update="update iib_exam_subjects set subject_name='$SubjectName',exam_code='$Exam_Code', pass_mark='$passMark', ".
				"online='Y' where subject_code='".$_POST['S_Code']."'";
		}
		else
		{
			$update="update iib_exam_subjects set subject_name='$SubjectName',exam_code='$Exam_Code', pass_mark='$passMark', total_marks='$totalMarks', ".
				"online='Y' where subject_code='".$_POST['S_Code']."'";
		}
		echo "<!--Update Qry:--$update-->";
		$update_res=mysql_query($update);
		if(!$update_res)
		{
			$err='update';
			$emsg='Sorry Update Failed!.';		
		}
		else
		{
			$done='update';
			$msg='Record Updated.'; 		
		}
	}
	else
	{
		$err=3;
		$emsg='This Name Already Exists!';
	}
}

if($submitted=='false' && $deleted=='true')
{
	/* code added - Nandita */
	$sqlSubExists = "SELECT subject_code FROM iib_exam_candidate WHERE subject_code='".$_POST['S_Code']."'";
	$res = @mysql_query($sqlSubExists) or die("Query failed");
	$nRows = mysql_num_rows($res);
	if ($nRows > 0)
	{
		$err='delete';
		$emsg='This subject forms part of an exam scheduled to be taken,<br> It cannot be deleted!';
	}
	else
	{
		/* code added - Nandita */
		$delete="update iib_exam_subjects set online='N' where subject_code='".$_POST['S_Code']."'";
		$delres=mysql_query($delete);
		echo "<!--delete Qry : $delete-->";
		if(!$delres)
		{
			$err='delete';
			$emsg='Sorry Delete Failed!.';
		}
		else
		{
			$done='delete';
			$msg='Record Deleted.';
		}
	}
}


//if($_POST["submitted"]=='true')
{	
	$qry="select subject_code, subject_name, pass_mark, total_marks, online from iib_exam_subjects where online='Y' and  exam_code='".$_POST["Exam_code"]."' order by subject_name";
	$result=mysql_query($qry);
	echo "<!-- $qry -->";
	if(!$result)
	{
		$err='selection';
		$emsg='Error in selecting from database!.';
	}
}

$Exam_Code=$_POST['Exam_code'];	
$subjectCode = $_POST['S_Code'];
$sqlcount = "SELECT COUNT(question_paper_no) FROM iib_question_paper WHERE exam_code='$Exam_Code' AND subject_code='$subjectCode'";
$rescount = mysql_query($sqlcount);
list($rowcount) = mysql_fetch_row($rescount);
$hidTotalMarks = $_POST['hid_total_marks'];		

$sql="select subject_code,subject_name from iib_exam_subjects a,iib_exam b where a.online='Y' and b.online='Y' and a.exam_code=b.exam_code order by subject_name";
$res=mysql_query($sql);
if(!$res)
{
	$err=1;
	$emsg="Some problem in selecting records.";
	$displaybug='true';	
}
$selectqry="select b.exam_code,b.exam_name from iib_exam_subjects a, iib_exam b where b.online='Y' and a.online='Y' and a.exam_code=b.exam_code group by a.exam_code order by exam_name";
$selectres=mysql_query($selectqry);

?>
<HTML>
<HEAD>
<script language='JavaScript' src="./includes/validations.js"></script>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='javascript'>

function validate()
{
        var f=document.addExam;
        if(f.Exam_code.value==-1)
        {
                alert('Please Select The Exam Name!');
                f.Exam_code.focus();
                return;
        }
        f.submitted.value='true';
        f.submit();
}

function e_validate()
{

    var f=document.addExam;
	
	if(f.S_name.value=='')
	{
		alert('Please Enter the subject name!');
		f.S_name.focus();
		return;
	}

	if(f.S_Code.value!=-1)
	{
    	myRegExp = new RegExp("[^a-zA-Z0-9 _/&,:#\)(-]");
    	sNAME=f.S_name.value;
    	result=sNAME.match(myRegExp);
    	if(result)
    	{
       		alert('Allowed Character Alphabet,Numbers,/,\\,#,&,(,),-,:');
       		return;
    	}
        myRegExp = new RegExp("^/.*$");
        sNAME=f.S_name.value;
        result=sNAME.match(myRegExp);
        if(result)
        {
        	alert('/ Cannot be the first character!');
        	f.S_name.focus();
            return;
        }
         if (f.totalmarks.value=='')
		{
			alert('Please Enter The Total Marks!');
			f.totalmarks.focus();
			return;
		}	
    	myRegExp = new RegExp("[^0-9]");
    	tMark=f.totalmarks.value;
    	result=tMark.match(myRegExp);
    	if(result)
    	{
        	alert('Enter only numbers');
        	f.totalmarks.focus();
        	return;
   		}
		if (f.totalmarks.value <= 0)
   		{
	 		alert('Total marks must be greater than 0');
        	f.totalmarks.focus();
        	return;
   		}
    
		if (f.passmark.value=='')
		{
			alert('Please Enter The Pass Mark!');
			f.passmark.focus();
			return;
		}	
    	myRegExp = new RegExp("[^0-9]");
    	pMark=f.passmark.value;
    	result=pMark.match(myRegExp);
    	if(result)
    	{
        	alert('Enter only numbers');
        	f.passmark.focus();
        	return;
   		}
   		
		if ((f.passmark.value <= 0) || (f.passmark.value > parseInt(f.totalmarks.value)))		
   		{
	 		alert('Pass mark must be in the range 1-'+f.totalmarks.value);
        	f.passmark.focus();
        	return;
   		}
	}
	else
	{
		alert('Please Select A Subject!');
		return;
	}
/*	
	if(f.hid_variable.value==trim(f.S_name.value))
	{
		alert('Please Change The data!');
		return;
	}
*/
    if(f.S_Code.value==-1)
    {
        alert('Please Select The Section Name!');
        f.S_Code.focus();
        return;
    }
	
	if(f.S_name.value=='' || f.S_Code.value==-1)
	{
		alert('Select Subject name cannot be Empty!');	
		f.S_Code.focus();
		return;	
	}

	if(f.Exam_code.value==-1)
	{
       	alert('Please Choose The Code!');
		f.Exam_code.focus();
		return;
	}
	f.submitted.value='false';
	f.edited.value='true';
	f.submit();
}

function d_validate()
{
    var f=document.addExam;
        if(f.S_Code.value==-1)
        {
                alert('Please Select The Section Name!');
                f.S_Code.focus();
                return;
        }

    if(f.S_name.value=='' || f.S_Code.value==-1)
    {
        alert('Please Enter/Choose The Subject Name!');
        f.E_code.focus();
        return;
    }
	
	if(confirm('Are You Sure?'))
	{
		f.submitted.value='false';
		f.deleted.value='true';
		f.submit();
	}
}

function submitForm()
{
    if (window.event.keyCode == 13)
    {
		if(document.addExam.S_Code.value!=-1 || document.addExam.S_name.value!='')
		{
			e_validate();
		}
		else
		return;
    }
}

function newvalidate()
{
	var f=document.addExam;
	if(f.S_Code.value==-1)
	{
		alert('Please Choose A Subject Code!');
		return;
	}
	f.submitted.value='true';
	f.submit();
}
function clearValues()
{
	var f = document.addExam;
	f.Exam_code.selectedIndex = 0;
	f.S_Code.selectedIndex = 0;
	f.S_name.value ='';
	f.totalmarks.value = '';
	f.passmark.value = '';
	f.Exam_code.focus();
}
</script>
</HEAD>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0 onload='javascript:document.addExam.Exam_code.focus();' >
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
<!--        <TR><!-- Topnav Image -->
      <!--  <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
        </TR>  -->
        <tr><td width="780"><?include("includes/header.php");?></td></tr>
        <TR>
        
        </TR>
		<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
		</tr>		
	    <TR>
            <TD background=images/tile.jpg vAlign=top width=780 align=center>
        <form name=addExam method="post" onsubmit="return false;">
        <table width=780 cellpadding=0 cellspacing=5 border=0>
          <tr>
          <td colspan=2 align="center"><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2>Edit/Delete Subject</b></font></td>
          </tr>
         
          <tr>
				<td align="right" class="greybluetext10" width="390">Select Exam Code To Edit/Delete :&nbsp;</td>
                <td>
                <select name="Exam_code" class="greybluetext10" onchange='javascript:validate();' style={width:150px}>
                <option value=-1>-Select-</option>
<?php
                while(list($exam_code,$exam_name)=mysql_fetch_row($selectres))
                {
                 if($exam_code==$_POST['Exam_code'])
                       echo "<option value=$exam_code selected>$exam_name</option>";
                 else
                       echo "<option value=$exam_code>$exam_name</option>";
                }
?>
                </select>
				</td>
            </tr>
<?
            //if(isset($_POST['submitted']) && $_POST['submitted']=='true') {?>
			<tr>
                <td align="right" width=390 class="greybluetext10" valign=top>
                Subject Name :&nbsp;
                </td>
                <td align="left" valign=top>
                       <select class="textbox" name="S_Code" onchange='javascript:newvalidate();' style={width:150px} onKeyPress='javascript:submitForm()'>
					   <option value=-1>-Select-</option>
						<?
						while (list($subject_code,$subject_name, $pass_mark, $total_marks)=mysql_fetch_row($result))
						{							
							$subject_name=stripslashes($subject_name);
							
							if($_POST['S_Code']==$subject_code) {
								echo "<option value=\"$subject_code\" selected>$subject_name</option>";
								$s_name=$subject_name;
								$passMark = $pass_mark;
								$totalMarks = $total_marks;
							}
							else
								echo "<option value=\"$subject_code\">$subject_name</option>";
						}
						?>
					</select>
					
                 </td>
            </tr>
			<tr>
				<td align="right" class="greybluetext10">New Subject Name :&nbsp;</td>
				<td><input class="textbox" type="text" name="S_name" value="<?=$s_name?>" size=27 maxlength=50 onKeyPress='javascript:submitForm()'>
				</td>
			</tr>
            <tr>
                    <td align="right" class="greybluetext10" valign=top>
                    Total Marks :&nbsp;
                    </td>
                    <td align="left" valign=top><input class="textbox" type="text" name="totalmarks" size=27 maxlength=5 value='<?=$totalMarks ?>' <?php ($rowcount > 0) ? print(" readonly") : print(""); ?> onKeyPress='javascript:submitForm()'>
                    <input type=hidden name='hid_total_marks' value='<?=$totalMarks ?>'>
                    </td>
            </tr>	            
			<tr>
                    <td align="right" class="greybluetext10" valign=top>
                    Pass Mark :&nbsp;
                    </td>
                    <td align="left" valign=top>
                    <input class="textbox" type="text" name="passmark" size=27 maxlength=5 value='<?=$passMark ?>' onKeyPress='javascript:submitForm()'>
                    </td>
            </tr>
            <?php 
            if ($rowcount > 0)
            {
	        ?>
            <tr>
            	<td colspan=2 align=center class='greybluetext10'> Total marks : Non-editable - Question Papers already generated </td>
            </tr>
            <?php
        	}
            ?>		
			<tr>
				<td colspan=2 align=center>
				<input class="button" type="button" name="edit_record" value="Update" onclick="javascript:e_validate();">
				<input class="button" type="button" name="del" value="Delete" onclick="javascript:d_validate();">
				<input type="button" class="button" name="bReset" value="Reset" onClick='javascript:clearValues();'>
				</td>
			</tr>
			 <?if(isset($err) && $err!=''){?>
          <tr>
              <td colspan=2 align="center" class='alertmsg'><b><?echo $emsg;?></b></td>
          </tr>
          <?}?>
          <?if(isset($done) && $done!='') {?>
          <tr>
             <td colspan=2 align="center" class='alertmsg'><b><?echo $msg;?></b></td>
          </tr>
          <? } ?>
<?//}?>
						
			<?//if($submitted=='false'){?>
            <!--<tr>
               <td align="right"><input type="button" class=button name=bSubmit value="Submit" onclick='javascript:validate();'></td>
               <td align="left"><input type="reset" class=button name=bReset value="Cancel"></td>
            </tr>-->
			<?//}?>
            <input type="hidden" name="submitted">
            <input type="hidden" name="edited">
            <input type="hidden" name="deleted">
            <input type="hidden" name="hid_variable" value="<?echo $s_name;?>">
            </table>
            </form>
           </TD>
        </TR>
        <TR>
                <?include("includes/footer.php");?>
        </TR>
</TABLE>
</FORM>
</center>
</BODY>
</HTML>
