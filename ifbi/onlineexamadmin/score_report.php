<?

/****************************************************

* Application Name            :  IBPS

* Module Name                 :  Score Report

* Revision Number             :  1

* Revision Date               :

* Table(s)  modified          : iib_candidate_scores,iib_correct_scores_log(New Table)

* Tables used for only selects: iib_exam_schedule,iib_exam,iib_exam_subjects,iib_candidate_scores,iib_candidate_test,iib_section_questions,iib_question_paper_details,

* View(s)                     :  -

* Stored Procedure(s)         :  -

* Dependant Module(s)         :  -

* Output File(s)              : Report showing the candidates' scores if they are calculated

* Document/Reference Material :

* Created By                  : Anil Kumar.A

* Created On 				  : 18/Apr/2006 

* Last Modified By            :  

* Last Modified Date          : 

* Description                 :  This report is used to display the scores of all the candidates on selecting date,exam,subject. Additional feature is that the output can be downloaded ina csv file

*****************************************************/

ob_start();

require("dbconfig_slave_123.php");

require("sessionchk.php");



set_time_limit(0);



/** It forms the array for the header information for the excel export   **/



  $excel_header=array("Membership No.","Name","Centre","Total Attempt","Total Right","Score");

  $headerLength=count($excel_header);

  

 /*echo "<pre>";

	print_r($_POST);

	echo "</pre>";*/

	

 

//Values got from Post variables

$examCode = isset($_POST['exam_code']) ? $_POST['exam_code'] : '';

$subjectCode = isset($_POST['subject_code']) ? $_POST['subject_code'] : '';

$examDate = isset($_POST['exam_date']) ? $_POST['exam_date'] : '';



//Hidden variables for status

$subject = isset($_POST['subject']) ? $_POST['subject'] : 'false';

$submitted= isset($_POST['submitted']) ? $_POST['submitted'] : 'false';

$ex_flag=isset($_POST['exl']) ? $_POST['exl'] : '0' ;



include_once("excelconfig.php");

 //File name for download

 $filename="Score_report_e(".$examCode.")_s(".$subjectCode.")_Dt(".$examDate.")";



//Selecting the exam dates for drop down(I/P)

$sqlDate = "SELECT distinct exam_date FROM iib_exam_schedule order by exam_date ";

$resDate = mysql_query($sqlDate);



//Selecting the exams for drop down(I/P)

$sqlexam = "SELECT exam_code,upper(exam_name) FROM iib_exam where online='Y' ";

$resexam = mysql_query($sqlexam);



//Variable for diaplaying the message

$msg='';



//Selecting the subjects only if one of the hidden flag is set for drop down(I/P)

if (($subject == 'true') || ($submitted == 'true'))

{

    $sql_subject_sel="select exam_code,subject_code,subject_name from iib_exam_subjects where exam_code='".$examCode."' order by subject_name";

    $res_subject_sel=mysql_query($sql_subject_sel);

}

if($submitted=='true' && $examCode!='' && $subjectCode!='' && $examDate!='')

{

	

	//Selecting the name , mem no , centre code from iib_candidate

	$sql_cand_name="select a.membership_no,a.name,b.centre_code from iib_candidate a , iib_candidate_iway b where a.membership_no = b. membership_no and b.exam_code='$examCode' and b.subject_code='$subjectCode' and b. exam_date='$examDate'";

	$res_cand_name=mysql_query($sql_cand_name);

	while(list($memNo,$memName,$centre)=mysql_fetch_row($res_cand_name))

	{

		$nameArr[$memNo]['name']=$memName;

		$nameArr[$memNo]['centre']=$centre;

	}

	

	//Selcting the the questionId,correct answer for the exam & subject selected

	$sql_sect_ques="select question_id , correct_answer from iib_section_questions where exam_code ='$examCode' and subject_code ='$subjectCode'";

	$res_sect_ques=mysql_query($sql_sect_ques);

	while(list($sec_ques_id,$sec_corr_answer)=mysql_fetch_row($res_sect_ques))

	{

		$secArr[$sec_ques_id]=$sec_corr_answer;

	}

	

	//Selecting the candidates , their question paper nos , score from candidate_scores , iib_candidate_test

	$sql_cand_scores="select c. membership_no , b. question_paper_no , a.score from iib_candidate_scores a , iib_candidate_test b, iib_candidate_iway c where a. membership_no = b. membership_no and a. membership_no = c. membership_no and b.subject_code=c.subject_code and a.exam_code='$examCode' and a. subject_code ='$subjectCode' and trim(a. exam_date) like trim('$examDate%') group by b. question_paper_no";

	$res_cand_scores=mysql_query($sql_cand_scores);

	$cnt_cand_scores=mysql_num_rows($res_cand_scores);

	if($cnt_cand_scores < 1)

	{

		$msg="No records found";

	}

	else

	{

		$i=0;	

		while(list($mem_no,$ques_paper_no,$old_score)=mysql_fetch_row($res_cand_scores))	

		{

			//Selecting the questionId ,answer given by the candidate

			//$sql_cand_ques="select question_id , answer from iib_question_paper_details where question_paper_no='$ques_paper_no' ";
			$sql_cand_ques = "select question_id, answer from iib_response where question_paper_no='$ques_paper_no' and id in ( select  max(id) from iib_response where question_paper_no ='$ques_paper_no' group by question_id) order by display_order";

			$res_cand_ques=mysql_query($sql_cand_ques);

			

			//Variable for calculating the score

			$correct_answer=0;

			$attempt_ques=0;

			//This loop runs for the no.of questions the particular question paper

			while(list($can_ques_id,$cand_answer)=mysql_fetch_row($res_cand_ques))

			{

				$correctAns=$secArr[$can_ques_id];

				

				//Checking the answer given by the candidate and the correct answer

				if($cand_answer!="")

				{

					$attempt_ques++;

					if ($cand_answer == $correctAns)

  					{

			  			$correct_answer++;

		  			}	

				}

			}

			$resultArr[$i][]=$mem_no;

			$resultArr[$i][]=$nameArr[$mem_no]['name'];

			$resultArr[$i][]=$nameArr[$mem_no]['centre'];

			$resultArr[$i][]=$attempt_ques;

			$resultArr[$i][]=$correct_answer;

			$resultArr[$i][]=$old_score;

			$i++;

		}

		$content_cnt=count($resultArr);

	}

	

	//Checking the excel flag for downloading the file

	if($ex_flag=='1')

	{

		excelconfig($filename,$excel_header,$resultArr);

		exit;

	}

}

?>

<HTML>

<HEAD>

<title><?=PAGE_TITLE?></title>

<link href="images/iibf.css" rel="stylesheet" type="text/css">

<STYLE>BODY {

	FONT-FAMILY: Arial, Verdana	

}

.textblk11     { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; text-decoration: none}

</STYLE>

<script language='JavaScript'>

function getsubjects()

{

	if (document.frm_scorereport.exam_code.selectedIndex == 0)

	{		

		alert("Please select the exam ");

		document.frm_scorereport.exam_code.focus();

		return false;

	}

	

	document.frm_scorereport.subject.value='true';

	document.frm_scorereport.exl.value='0';

	document.frm_scorereport.submit();

	

}

function submitForm()

{

	if (document.frm_scorereport.exam_date.selectedIndex == 0)

	{

		alert("Please select the exam date");

		document.frm_scorereport.exam_date.focus();

		return false;

	}

	if (document.frm_scorereport.exam_code.selectedIndex == 0)

	{		

		alert("Please select the exam ");

		document.frm_scorereport.exam_code.focus();

		return false;

	}

	if (document.frm_scorereport.subject_code.selectedIndex == 0)

	{		

		alert("Please select the Subject ");

		document.frm_scorereport.subject_code.focus();

		return false;

	}

	document.frm_scorereport.submitted.value='true';

	document.frm_scorereport.submit();

}

function excelsubmit()

{

	//alert("hi");

	var frmname=document.frm_scorereport;

	frmname.exl.value='1';

	document.frm_scorereport.submitted.value='true';

	frmname.submit();

}

</script>

</head>

<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0>

<center>

<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>

		<tr><td><?include("includes/header.php");?> </td></tr>

	

<!--	<TR>

    	<TD bgColor=#7292c5 width=780>&nbsp;</TD>

	</TR>-->

	<tr>

		<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	

	</tr>	

	<TR>

	    <TD background=images/tile.jpg vAlign=top width=780 align=center>

<form name='frm_scorereport' method='post'>

<input type='hidden' name='subject' value=''>

<input type='hidden' name='submitted'>

<input type=hidden name=exl value='0'>

<table width=100% border=0 cellspacing=0 cellpadding=5>

	<tr>

		<td colspan=2 align="center"><b><font face="Verdana, Arial, Helvetica, sans-serif" size=2>Score Report</b></font><br><br></td>

    </tr>

    <tr>

            	<td align="right" width=390 class="greybluetext10" valign=top>Exam Date : </td>

            	<td align="left" valign=top class="greybluetext10">

            	<select name='exam_date' style={width:150px} class=greybluetext10>

				<option value=''>--Select--</option>

				<?php

				if (mysql_num_rows($resDate) > 0)

				{

					while (list($eDate) = mysql_fetch_row($resDate))

					{

						$aDate = explode("-",$eDate);

						$dispDate = $aDate[2]."-".$aDate[1]."-".$aDate[0];

						print("<option value='$eDate' ");

						if ($eDate == $examDate)

							print(" selected ");

						print(">$dispDate</option>");

					}

				}

				

				?>

            		</select>

            	</td>

            </tr> 

             <tr>

            	<td align="right" width=390 class="greybluetext10" valign=top>Exam : </td>

            	<td align="left" valign=top class="greybluetext10">

            		<select name='exam_code' class="greybluetext10" style={width:275px} onchange='javascript:getsubjects();'>

            			<option value=''>--Select--</option>

            			<?

            				while (list($code,$name) = mysql_fetch_row($resexam))

            				{

	            				print("<option value='$code' ");

	            				if ($examCode == $code)

	            				{

		            				print(" selected ");

	            				}

	            		

	            				

	            				print(">$name</option>");

	            			}

	            		?>

            		</select>

            	</td>

            </tr>   

              <tr>

            	<td align="right" width=390 class="greybluetext10" valign=top>Subject : </td>

            	<td align="left" valign=top class="greybluetext10">

            		<select name='subject_code' class="greybluetext10" style={width:350px} >

            			<option value=''>--Select--</option>

            			

            			<?

            				while (list($code,$Scode,$Sname) = mysql_fetch_row($res_subject_sel))

            				{

	            

	            				print("<option value='$Scode' ");

	            				if ($subjectCode == $Scode)

	            				{

		            				print(" selected ");

	            				}

	            		

	            					print(">$Sname</option>");

	            			}

	            		?>

            		</select>

            	</td>

            </tr>

            

            

            

      </table>   

    <table width=780 >       

        <tr>

			<td colspan=2 align="center"><input type=button name='sub_assign' value='Submit' class='button' onClick='javascript:submitForm()'>

			

			</td>

        </tr>

        <TR>

			<TD>&nbsp;</TD>

		</TR>

        <tr>

        	<td>

        	<?

        		if($ex_flag!='1')

				{

					if($content_cnt>0)

					{

				// Printing the values in the HTML page 

				

				// Printing the header values from excel header array 

				print("<table border=1 align=center cellspacing=0 cellpadding=2><tr>");

				for($a=0;$a<$headerLength;$a++)

				{

					print("<td class=greybluetext10><b>$excel_header[$a]</b></td>");

				}

				print("</tr>");

				print("<tr>");

				for($i=0;$i<$content_cnt;$i++)

				{

						for($j=0;$j<$headerLength;$j++)

						{

							echo "<td class=greybluetext10 >&nbsp;".$resultArr[$i][$j]."</b></td>";

						}

					print("</tr>");

				}

				print("</table>");

				?>

				<table align=center>

		 			<tr><td class=greybluetext10>&nbsp;</td></tr>

		         	<tr><td class=greybluetext10 align="center" class=greybluetext10><input class='button' type='button' name='download' value='Download' onClick='javascript:excelsubmit()'></td></tr>

		            	<tr><td class=greybluetext10>&nbsp;</td></tr>

				</table>

				<?

				}

			}

			?>		

        	</td>

        </tr>

        <?

        if($msg!='')

        {

        ?>

        <tr>

            <td colspan=2 align="center" class='alertmsg'><b><?echo $msg;?></b></td>

        </tr>

        <?}?>

</table>

</form>

</TD>

	</TR>

	 <tr>

    <? include("includes/footer.php");?>

  </tr>



</TABLE>

</center>

</BODY>

</HTML>
