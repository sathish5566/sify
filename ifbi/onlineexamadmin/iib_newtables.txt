-- MySQL dump 8.22
--
-- Host: 210.210.125.27    Database: iiblive
---------------------------------------------------------
-- Server version	3.23.56

--
-- Table structure for table 'iib_centre_change_logs'
--

CREATE TABLE iib_centre_change_logs (
  row_id int(11) NOT NULL auto_increment,
  membership_no varchar(20) default NULL,
  exam_code varchar(20) default NULL,
  subject_code varchar(20) default NULL,
  exam_date date default NULL,
  exam_time time default NULL,
  old_centre_code varchar(20) default NULL,
  new_centre_code varchar(20) default NULL,
  reason varchar(40) default NULL,
  PRIMARY KEY  (row_id)
) TYPE=MyISAM;

--
-- Dumping data for table 'iib_centre_change_logs'
--

--
-- Table structure for table 'iib_subject_change_log'
--

CREATE TABLE iib_subject_change_log (
  rowid int(11) NOT NULL auto_increment,
  membership_no varchar(20) default NULL,
  old_subject int(11) default NULL,
  new_subject int(11) default NULL,
  changed_on timestamp(14) NOT NULL,
  PRIMARY KEY  (rowid)
) TYPE=MyISAM;

--
-- Dumping data for table 'iib_subject_change_log'
--


-- Table structure for table 'iib_change_reason'
--

CREATE TABLE iib_change_reason (
  reason char(40) default NULL
) TYPE=MyISAM;

--
-- Dumping data for table 'iib_change_reason'
--


INSERT INTO iib_change_reason VALUES ('Hall Ticket Does not match');
INSERT INTO iib_change_reason VALUES ('Wrong centre');
INSERT INTO iib_change_reason VALUES ('iWay Problem');
INSERT INTO iib_change_reason VALUES ('Others');

