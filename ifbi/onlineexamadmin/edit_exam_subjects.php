<?php
/****************************************************
* Application Name            :  IBPS
* Module Name                 :  Edit subjects
* Revision Number             :  1.1
* Revision Date               :
* Table(s)                    :  iib_exam_subjects
* Tables used for only selects:  iib_question_paper,iib_exam_schedule,iib_exam_candidate
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              : 
* Document/Reference Material :
* Created By                  : 
* Created ON                  : 
* Last Modified By            : Sathiyanarayanan S
* Last Modified Date          : 28-May-2008
* Description                 : IM to edit the Subjects
*****************************************************/
session_start();
if ($_SESSION["admin_type"]== '0') {
	$admin_flag = 0;
} else if ($_SESSION["admin_type"]== '1'){
	$admin_flag = 1;
}

require_once "sessionchk.php";
require_once "dbconfig.php";
$disp_unchked='';
$disp_chked='checked';
$sub_check='false';

//Added for BOI
/*print "<br>Post<pre>";
print_r($_POST);
print "</pre>";*/
if($_POST['h_subjecttype']=='true')
{
	$SubjectName=$_POST['S_name'];
	$Exam_Code=$_POST['Exam_code'];
	$selectqry="select exam_type from iib_exam where exam_code='$Exam_Code' and online='Y' order by exam_name";
	$selectres=mysql_query($selectqry);
	list($exam_type) = mysql_fetch_row($selectres);

	$S_online=$_POST['E_status'];
	$subjectCode = $_POST['S_Code'];
	$SubjectName=trim($SubjectName);
	$SubjectName=addslashes($SubjectName);
	$passMark = trim($_POST['passmark']);
	$totalMarks = trim($_POST['totalmarks']);
	
	$subjectDuration = trim($_POST['duration_sub']);
	$subjectDuration_sec=min_to_sec($subjectDuration);	//Calling function to convert minutes to seconds
	
	$preventduration= trim($_POST['duration_prevent']);
	$preventduration_sec = min_to_sec($preventduration);	
	
	
	$gracePre = trim($_POST['pre_grace']);
	$gracePre_sec=min_to_sec($gracePre);	//Calling function to convert minutes to seconds
	
	$gracePost = trim($_POST['post_grace']);
	$gracePost_sec=min_to_sec($gracePost);	//Calling function to convert minutes to seconds
	
	$passMsg = trim($_POST['PassTxt']);
	$failMsg = trim($_POST['FailTxt']);
	$displayScore=isSet($_REQUEST["disp_score"]) ? "Y" : "N" ;
	$displayResult=isSet($_REQUEST["disp_result"]) ? "Y" : "N" ;
	$displayResponse=isSet($_REQUEST["disp_resp"]) ? "Y" : "N" ;
	$displayRoundScore=isSet($_REQUEST["roundoff_score"]) ? "Y" : "N" ;
	$displayPrint=isSet($_REQUEST["disp_print"]) ? "Y" : "N" ;
	$display_mark=isSet($_POST["disp_mark"]) ? "Y" : "N" ;

    $question_display=trim($_POST["question_display"]);
    if($question_display == 'M'){
        $noofquestionsperpage = $_POST['noofquestionsperpage'];
    }else if($question_display == 'O'){
        $noofquestionsperpage = 1;
    }else if($question_display == 'A'){	
        $noofquestionsperpage = 0;
    }
    $answersavingtype = trim($_POST["answersavingtype"]);
    $answersavinginterval = trim($_POST["answersavinginterval"]);
    $casedisplay = trim($_POST["casedisplay"]);	


	$subjecttype=$_POST["subjecttype"];
	$sub_check='true';
    if($subjecttype=='Y')
	{
	  $optionalid=$_POST['optionalid'];
	}
	$max_no_questions=$_POST['maxno_ofquestions'];
	$breakDuration_disp = trim($_POST['duration_brk']);
}
//Added for BOI ends here


if(!isset($_POST['submitted']))
	$submitted='false';
else
	$submitted=$_POST['submitted'];

$edited=$_POST['edited'];
$deleted=$_POST['deleted'];



if($submitted=='false' && $edited=='true')
{
	
	$SubjectName=$_POST['S_name'];
	$Exam_Code=$_POST['Exam_code'];
	$S_online=$_POST['E_status'];
	$subjectCode = $_POST['S_Code'];
	
	$SubjectName=trim($SubjectName);
	$SubjectName=addslashes($SubjectName);
	$passMark = trim($_POST['passmark']);
	$totalMarks = trim($_POST['totalmarks']);
	
	$subjectDuration = trim($_POST['duration_sub']);
	$subjectDuration_sec=min_to_sec($subjectDuration);	//Calling function to convert minutes to seconds
	
	$preventduration= trim($_POST['duration_prevent']);
	$preventduration_sec = min_to_sec($preventduration);	
		
	/*$breakDuration = trim($_POST['duration_brk']);
	$breakDuration_sec=min_to_sec($breakDuration,$var_to_sec);	//Calling function to convert minutes to seconds*/
	
	$gracePre = trim($_POST['pre_grace']);
	$gracePre_sec=min_to_sec($gracePre);	//Calling function to convert minutes to seconds
	
	$gracePost = trim($_POST['post_grace']);
	$gracePost_sec=min_to_sec($gracePost);	//Calling function to convert minutes to seconds
	
	$passMsg = trim($_POST['PassTxt']);
	$failMsg = trim($_POST['FailTxt']);
	$displayScore=isSet($_REQUEST["disp_score"]) ? "Y" : "N" ;
	$displayResult=isSet($_REQUEST["disp_result"]) ? "Y" : "N" ;
	$displayResponse=isSet($_REQUEST["disp_resp"]) ? "Y" : "N" ;
	$displayRoundScore=isSet($_REQUEST["roundoff_score"]) ? "Y" : "N" ;
	$displayPrint=isSet($_REQUEST["disp_print"]) ? "Y" : "N" ;
	$display_mark=isSet($_POST["disp_mark"]) ? "Y" : "N" ;
	
    $answer_shuffling=isset($_POST["answer_shuffling"]) ? "Y" : "N" ;
	$display_section=isSet($_POST["disp_section"]) ? "Y" : "N" ;
    $question_display=trim($_POST["question_display"]);
	if($question_display == 'M'){
		$noofquestionsperpage = $_POST['noofquestionsperpage'];
	}else if($question_display == 'O'){
		$noofquestionsperpage = 1;
	}else if($question_display == 'A'){	
		$noofquestionsperpage = 0;
	}
   $answersavingtype = trim($_POST["answersavingtype"]);
	$answersavinginterval = trim($_POST["answersavinginterval"]);
	$casedisplay = trim($_POST["casedisplay"]);

	//Added for BOI starts here
	$subjecttype=$_POST["subjecttype"];
	if($subjecttype=='Y')
	{
		 $optionalid=$_POST['optionalid'];
	}
	$max_no_questions=$_POST['maxno_ofquestions'];
	$breakDuration_disp = trim($_POST['duration_brk']);
	//Added for BOI ends here
	$strLanguages = implode(",",$_POST["languages"]);
	
	$chksql="select count(1) from iib_exam_subjects where online='Y' and exam_code='"
			.$Exam_Code."' and subject_name='".$SubjectName."' and subject_code !='$subjectCode'";	
	$chkres=mysql_query($chksql);
	list($num)=mysql_fetch_row($chkres);

	if($num==0)
	{		
		$sqlcount = "SELECT COUNT(question_paper_no) FROM iib_question_paper WHERE exam_code='$Exam_Code' AND subject_code='$subjectCode'";
		$rescount = mysql_query($sqlcount);
		list($rowcount) = mysql_fetch_row($rescount);
		$hidTotalMarks = $_POST['hid_total_marks'];
		//print $sqlcount;
		if ($rowcount > 0)
		{						
			
            //total_marks='$totalMarks' Not changable after QP generation
            $update="update iib_exam_subjects set 
                     subject_name='$SubjectName',
                     exam_code='$Exam_Code',
                     pass_mark='$passMark',
                     online='Y',
                     subject_duration='$subjectDuration_sec',
                     display_score='$displayScore',
                     display_result='$displayResult',
                     display_response='$displayResponse',
                     roundoff_score='$displayRoundScore',
                     pass_msg='$passMsg',
                     fail_msg='$failMsg',
                     grace_pre='$gracePre_sec',
                     grace_post='$gracePost_sec',
                     answer_shuffling='$answer_shuffling',
                     question_display='$question_display',
                     updatedtime=now(),
                     break_duration='$breakDuration_sec',
                     option_id='$optionalid',
                     option_flag='$subjecttype',
                     max_no_of_questions='$max_no_questions',
                     duration_prevent='$preventduration_sec',
                     display_print='$displayPrint',
                     display_sectionname='$display_section',
                     languages='$strLanguages',
                     display_mark='$display_mark',
                     display_casequestion='$casedisplay',
                     display_noofquestions='$noofquestionsperpage',
                     answer_saving_type='$answersavingtype',
                     answer_saving_interval='$answersavinginterval'                             
                     where subject_code='".$_POST['S_Code']."'";
		}
		else
		{								
               $update="update iib_exam_subjects set 
                     subject_name='$SubjectName',
                     exam_code='$Exam_Code',
                     pass_mark='$passMark',
                     total_marks='$totalMarks',
                     online='Y',
                     subject_duration='$subjectDuration_sec',
                     display_score='$displayScore',
                     display_result='$displayResult',
                     display_response='$displayResponse',
                     roundoff_score='$displayRoundScore',
                     pass_msg='$passMsg',
                     fail_msg='$failMsg',
                     grace_pre='$gracePre_sec',
                     grace_post='$gracePost_sec',
                     answer_shuffling='$answer_shuffling',
                     question_display='$question_display',
                     updatedtime=now(),
                     break_duration='$breakDuration_sec',
                     option_id='$optionalid',
                     option_flag='$subjecttype',
                     max_no_of_questions='$max_no_questions',
                     duration_prevent='$preventduration_sec',
                     display_print='$displayPrint',
                     display_sectionname='$display_section',
                     languages='$strLanguages',
                     display_mark='$display_mark',
                     display_casequestion='$casedisplay',
                     display_noofquestions='$noofquestionsperpage',
                     answer_saving_type='$answersavingtype',
                     answer_saving_interval='$answersavinginterval'                     
                     where subject_code='".$_POST['S_Code']."'";
		}
		$update_res=mysql_query($update);
		if(!$update_res)
		{
			$err='update';
			$emsg='Sorry Update Failed!.';		
		}
		else
		{
			$done='update';
			$msg='Record Updated.'; 		
		}

		//Update optional id for all the subjects for the selected exam
		if($subjecttype=='Y')
		{
			$update_option="update iib_exam_subjects set option_id='$optionalid' where exam_code='$Exam_Code' and option_flag='Y'";
			$res_update_option=mysql_query($update_option);
			echo "<!--Update Qry:--$update_option-->";
		}

	}
	else
	{
		$err=3;
		$emsg='This Name Already Exists!';
	}
}

if($submitted=='false' && $deleted=='true')
{
	$sql_sec_exist = "select count(1) from iib_subject_sections where subject_code='".$_POST["S_Code"]."' and online='Y'";       
		$res_sec_exist  = mysql_query($sql_sec_exist);
		list($num_sec)=mysql_fetch_row($res_sec_exist);
		
		
	if($num_sec>0){
      $err='delete';
      $emsg="This Code can't be deleted. It has sections!.";
    }
	else{

	$sqlSub_schduled = "SELECT  * FROM iib_exam_schedule WHERE subject_code='".$_POST['S_Code']."' ";
	$resSub_schduled = @mysql_query($sqlSub_schduled) or die("Query failed");
	$nRows_scheduled = mysql_num_rows($resSub_schduled);
	if ($nRows_scheduled > 0)
	{
		$err='delete';
		$emsg='This subject forms part of an exam scheduled to be taken,<br> It cannot be deleted!';
	}
	else{
		/* code added - Nandita */
		$sqlSubExists = "SELECT subject_code FROM iib_exam_candidate WHERE subject_code='".$_POST['S_Code']."' ";
		$res = @mysql_query($sqlSubExists) or die("Query failed");
		$nRows = mysql_num_rows($res);
		if ($nRows > 0)
		{
			$err='delete';
			$emsg='This subject forms part of an exam scheduled to be taken,<br> It cannot be deleted!';
		}
		else
		{
			/* code added - Nandita */
			$delete="update iib_exam_subjects set online='N' where subject_code='".$_POST['S_Code']."'";
			$delres=mysql_query($delete);
			//echo "<!--delete Qry : $delete-->";
			if(!$delres)
			{
				$err='delete';
				$emsg='Sorry Delete Failed!.';
			}
			else
			{
				$done='delete';
				$msg='Record Deleted.';
			}
		}
	}
}
}


//if($_POST["submitted"]=='true')
//{	
	//$qry="select subject_code, subject_name, pass_mark, total_marks, online from iib_exam_subjects where online='Y' and  exam_code='".$_POST["Exam_code"]."' order by subject_name";
 $qry="select subject_code, subject_name, pass_mark, total_marks,subject_duration,display_score,display_result,
        pass_msg,fail_msg,grace_pre,grace_post,answer_shuffling,question_display,option_id,option_flag,break_duration,
        max_no_of_questions,duration_prevent,display_print,languages,display_mark,display_sectionname,
        display_casequestion,display_noofquestions,answer_saving_type,answer_saving_interval,display_response,roundoff_score
        from iib_exam_subjects 
        where online='Y' and  exam_code='".$_POST["Exam_code"]."' order by subject_name";
	$result=mysql_query($qry);
	//echo "<!-- $qry -->";

	if(!$result)
	{
		$err='selection';
		$emsg='Error in selecting from database!.';
	}
//}

$Exam_Code=$_POST['Exam_code'];	
$subjectCode = $_POST['S_Code'];
$sqlcount = "SELECT COUNT(question_paper_no) FROM iib_question_paper WHERE exam_code='$Exam_Code' AND subject_code='$subjectCode'";
$rescount = mysql_query($sqlcount);
list($rowcount) = mysql_fetch_row($rescount);
$hidTotalMarks = $_POST['hid_total_marks'];		

$sql="select subject_code,subject_name from iib_exam_subjects a,iib_exam b where a.online='Y' and b.online='Y' and a.exam_code=b.exam_code order by subject_name";
$res=mysql_query($sql);
if(!$res)
{
	$err=1;
	$emsg="Some problem in selecting records.";
	$displaybug='true';	
}
$selectqry="select b.exam_code,b.exam_name from iib_exam_subjects a, iib_exam b where b.online='Y' and a.online='Y' and a.exam_code=b.exam_code group by a.exam_code order by exam_name";
$selectres=mysql_query($selectqry);

function sec_to_min($duration_in_secs)
{
	return $duration_in_secs/60;
}

function min_to_sec($duration_in_secs)
{
	return $duration_in_secs*60;
}

?>
<HTML>
<HEAD>
<script language='JavaScript' src="./includes/validations.js"></script>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='javascript'>

function validate()
{
        var f=document.addExam;
        if(f.Exam_code.value==-1)
        {
                alert('Please Select The Exam Name!');
                f.Exam_code.focus();
                return;
        }
        f.submitted.value='true';
        f.submit();
}

function e_validate()
{

    var f=document.addExam;
	if(f.Exam_code.value==-1)
    {
          alert('Please Select The Exam Name!');
          f.Exam_code.focus();
          return;
   }
   if(f.S_Code.value==-1)
    {
          alert('Please Select The Subject Name!');
          f.S_Code.focus();
          return;
   }
   if(f.subjecttype)
   {
		if(f.subjecttype.value==-1)
		{
			alert('Please Select the Subject Type!');
			f.subjecttype.focus();
			return;
		}
		if(f.subjecttype.value=='Y')
		{
			if(trim(f.optionalid.value)=='')
			{
				alert('Please enter the Optional ID!');
				f.optionalid.focus();
				return;	
			}
			myRegExp = new RegExp("[^0-9]");
			optionalid_val=f.optionalid.value;
			result_optionalid=optionalid_val.match(myRegExp);
			if (result_optionalid)
			{
				alert('Only numbers allowed for optional ID!');
				f.optionalid.focus();
				return;
			}
		}
	   if(trim(f.duration_brk.value)=='')
		{
			alert('Please Enter the Break Duration!');
			f.duration_brk.focus();
			return;
		}
		if(trim(f.duration_brk.value)<0)
		{
			alert('Please Enter valid Duration!');
			f.duration_brk.focus();
			return;
		}
		myRegExp = new RegExp("[^0-9]");
		bdur=trim(f.duration_brk.value);
		result_brk=bdur.match(myRegExp);
		if (result_brk)
		{
			alert('Only numbers allowed!');
			f.duration_brk.focus();
			return;
	   }
		if(trim(f.maxno_ofquestions.value)=='')
		{
			alert('Please Enter the maximum number of Questions!');
			f.maxno_ofquestions.focus();
			return;
		}
		myRegExp = new RegExp("[^0-9]");
		maxno_ofquestions_val=f.maxno_ofquestions.value;
		result_maxno_ofquestions=maxno_ofquestions_val.match(myRegExp);
		if (result_maxno_ofquestions)
		{
			alert('Only numbers allowed!');
			f.maxno_ofquestions.focus();
			return;
		}
		if(trim(f.maxno_ofquestions.value)<='0')
		{
			alert('Please Enter valid value for maximum number of questions!');
			f.maxno_ofquestions.focus();
			return;
		}
	}

	if(trim(f.S_name.value)=='')
	{
		alert('Please Enter the subject name!');
		f.S_name.value='';
		f.S_name.focus();
		return;
	}
	if(f.S_Code.value!=-1)
	{
    	myRegExp = new RegExp("[^a-zA-Z0-9 _/&,:#\\)(-.]");
    	sNAME=f.S_name.value;
    	result=sNAME.match(myRegExp);
    	if(result)
    	{
       		alert('Allowed Character Alphabet,Numbers,/,#,&,(,),-,:,.');
			//alert('Allowed Character Alphabet,Numbers,/,\\,#,&,(,),-,:,.');
       		return;
    	}
        myRegExp = new RegExp("^/.*$");
        sNAME=f.S_name.value;
        result=sNAME.match(myRegExp);
        if(result)
        {
        	alert('/ Cannot be the first character!');
        	f.S_name.focus();
            return;
        }
         if (f.totalmarks.value=='')
		{
			alert('Please Enter The Total Marks!');
			f.totalmarks.focus();
			return;
		}	
    	myRegExp = new RegExp("[^0-9]");
    	tMark=f.totalmarks.value;
    	result=tMark.match(myRegExp);
    	if(result)
    	{
        	alert('Enter only numbers');
        	f.totalmarks.focus();
        	return;
   		}
		if (f.totalmarks.value <= 0)
   		{
	 		alert('Total marks must be greater than 0');
        	f.totalmarks.focus();
        	return;
   		}
    
		if (f.passmark.value=='')
		{
			alert('Please Enter The Pass Mark!');
			f.passmark.focus();
			return;
		}	
    	myRegExp = new RegExp("[^0-9]");
    	pMark=f.passmark.value;
    	result=pMark.match(myRegExp);
    	if(result)
    	{
        	alert('Enter only numbers');
        	f.passmark.focus();
        	return;
   		}
   		
		if ((f.passmark.value <= 0) || (f.passmark.value > parseInt(f.totalmarks.value)))		
   		{
	 		alert('Pass mark must be in the range 1-'+f.totalmarks.value);
        	f.passmark.focus();
        	return;
   		}
   		if(trim(f.duration_sub.value)=='')
		{
			alert('Please Enter the Subject Duration!');
			f.duration_sub.focus();
			return;
		}
		if(trim(f.duration_sub.value)=='0')
		{
			alert('Please Enter valid Duration!');
			f.duration_sub.focus();
			return;
		}
		myRegExp = new RegExp("[^0-9]");
	    sdur=trim(f.duration_sub.value);
	    result1=sdur.match(myRegExp);
	    if (result1)
	    {
	        alert('Only numbers allowed!');
	        f.duration_sub.focus();
	        return;
	   }	   
	   if(trim(f.duration_prevent.value)=='')
	   {
		 alert('Please Enter the Duration to prevent submit');
		 f.duration_prevent.focus();
		 return;
	   }
		myRegExp = new RegExp("[^0-9]");
		sdur2=f.duration_prevent.value;
		result2=sdur2.match(myRegExp);
		if (result2)
		{
			alert('Only numbers allowed!');
			f.duration_prevent.focus();
			return;
	    }
	   if(parseInt(f.duration_prevent.value) > parseInt(f.duration_sub.value))
	   {
		 alert('Duration to prevent submit should be less than or equal to subject duration');
		 f.duration_prevent.focus();
		 return;
	   }
		
	   /*Validation for pre grace time*/
   
	   if(trim(f.pre_grace.value)=='')
		{
			alert('Please Enter the Pre Grace Time!');
			f.pre_grace.focus();
			return;
		}
		if(trim(f.pre_grace.value)=='0')
		{
			alert('Please Enter valid Pre Grace Time!');
			f.pre_grace.focus();
			return;
		}
		
	    pregracedur=trim(f.pre_grace.value);
	    result_pregrace=pregracedur.match(myRegExp);
	    if (result_pregrace)
	    {
	        alert('Only numbers allowed!');
	        f.pre_grace.focus();
	        return;
	   }
   

	   /*Validation for Post grace time*/
   
	   if(trim(f.post_grace.value)=='')
		{
			alert('Please Enter the Post Grace Time!');
			f.post_grace.focus();
			return;
		}
		if(trim(f.post_grace.value)=='0')
		{
			alert('Please Enter valid Post Grace Time!');
			f.post_grace.focus();
			return;
		}
		
	    postgracedur=trim(f.post_grace.value);
	    result_postgrace=postgracedur.match(myRegExp);
	    if (result_postgrace)
	    {
	        alert('Only numbers allowed!');
	        f.post_grace.focus();
	        return;
	    }
	    
		     /*Validation for  Message*/
			
	   	if (trim(f.PassTxt.value)=="")
	    {
		    alert("Please enter the Pass Message");
		    f.PassTxt.focus();
		    return false;
	    }
	    else
	    {
		    //myRegExp = new RegExp("[^a-zA-Z0-9-,&:/ .]+\n\r");    
		    myRegExp = new RegExp("[^a-zA-Z0-9-,&:/ ._]+\n\r");    
		    spass_msg=f.PassTxt.value;
		    result_passmsg=spass_msg.match(myRegExp);
		    
		    
		    if (result_passmsg)
		    {
		        alert('Allowed Character Alphabet,Numbers,/,,,&,-,_,:, ,.');
		        f.PassTxt.focus();
		        return;
		   }
	    } 
	    
	    
	   
	    
	    /*Validation for Fail Message*/
	    
    if (f.FailTxt.value=="")
    {
	    alert("Please enter the Fail Message");
	    f.FailTxt.focus();
	    return false;
    }
    else
    {
	    myRegExp = new RegExp("[^a-zA-Z0-9-,&:/ ._]+\n\r");    
	    sfail_msg=f.FailTxt.value;
	    result_failmsg=sfail_msg.match(myRegExp);
	    
	    
	    if (result_failmsg)
	    {
	        alert('Allowed Character Alphabet,Numbers,/,,,&,-,_,:, ,.');
	        f.failTxt.focus();
	        return;
	   }
    }
    
	/*
    if(!f.rquestion_display[0].checked && !f.rquestion_display[1].checked){
		alert("Please select Question Display");	
		f.rquestion_display[0].focus();
		return false;
	}*/
	
	}
	else
	{
		alert('Please Select A Subject!');
		return;
	}
/*	
	if(f.hid_variable.value==trim(f.S_name.value))
	{
		alert('Please Change The data!');
		return;
	}
*/
    if(f.S_Code.value==-1)
    {
        alert('Please Select The Section Name!');
        f.S_Code.focus();
        return;
    }
	
	if(f.S_name.value=='' || f.S_Code.value==-1)
	{
		alert('Select Subject name cannot be Empty!');	
		f.S_Code.focus();
		return;	
	}

	if(f.Exam_code.value==-1)
	{
       	alert('Please Choose The Code!');
		f.Exam_code.focus();
		return;
	}

	if( f.question_display[2].checked){
		if( f.noofquestionsperpage.value == ''){
			alert("Please enter no.of questions per page");	
			f.noofquestionsperpage.focus();
			return false;    
		}else{
			noofquestion=f.noofquestionsperpage.value;
    		myRegExp = new RegExp("[^0-9]");
    		result_noofquestion = noofquestion.match(myRegExp);
			if (result_noofquestion){
				alert('Only numbers allowed!');
				f.noofquestionsperpage.focus();
				return;
			}									
		}
	}

    if(document.getElementById('languages').checked==0){	
		alert("please select atleast one language to do question paper upload");
		f.languages[0].focus();
		return false;
	}	

    f.submitted.value='false';
	f.edited.value='true';
	f.submit();
}

function check_optional(opt)
{
	var f=document.addExam;
	
	if(opt != '' && opt=='subjecttype')
	{
		f.h_subjecttype_selected.value='true';
	}else{
		f.h_subjecttype_selected.value='false';
	}
	f.h_subjecttype.value="true";
	f.submitted.value='false';
	f.edited.value='false';
	f.submit();
}
function check_id()
{
	var f=document.addExam;
	if(f.S_Code.value==f.optionalid.value)
	{
		alert("Optional ID and subject code cannot be the same");
		f.optionalid.focus();
		return;
	}
}

function d_validate()
{
    var f=document.addExam;
        if(f.S_Code.value==-1)
        {
                alert('Please Select The Subject Name!');
                f.S_Code.focus();
                return;
        }

    if(f.S_name.value=='' || f.S_Code.value==-1)
    {
        alert('Please Enter/Choose The Subject Name!');
		
        f.Exam_code.focus();
        return;
    }
	
	if(confirm('Are You Sure?'))
	{
		f.submitted.value='false';
		f.deleted.value='true';
		f.submit();
	}
}

function submitForm()
{
    if (window.event.keyCode == 13)
    {
		if(document.addExam.S_Code.value!=-1 || document.addExam.S_name.value!='')
		{
			e_validate();
		}
		else
		return;
    }
}

function newvalidate()
{
	var f=document.addExam;
	if(f.S_Code.value==-1)
	{
		alert('Please Choose A Subject Code!');
		return;
	}
	f.submitted.value='true';
	f.submit();
}
function clearValues()
{
	var f = document.addExam;
	f.Exam_code.selectedIndex = 0;
	f.S_Code.selectedIndex = 0;
	f.S_name.value ='';
	f.totalmarks.value = '';
	f.passmark.value = '';
	f.duration_sub.value = '';
	f.pre_grace.value = '';
	f.post_grace.value = '';
	f.PassTxt.value = '';
	f.FailTxt.value = '';
	f.disp_score.checked = false;
	f.disp_result.checked = false;
	f.disp_resp.checked = false;
	f.roundoff_score.checked = false;
	f.Exam_code.focus();
}

function calcprevsub(obj)
{
	var f=document.addExam;
	if(obj.value!='')
	{
		myRegExp = new RegExp("[^0-9]");
    	sdur=obj.value;
	    result1=sdur.match(myRegExp);
		if(!result1){
			f.duration_prevent.value=parseInt((parseInt(sdur)*3)/4);			
		}		
	}
}
function changeQuestionDisplay(){
	var f=document.addExam;
	for(var i=0;i<f.question_display.length;i++){
		if(f.question_display[i].checked == true){
			if(f.question_display[i].value == 'M'){
				document.getElementById('divnoofquestionsperpage').style.display="block";
				document.getElementById('divnoofquestionsperpage').style.visibility="visible";
			}else{
				document.getElementById('divnoofquestionsperpage').style.display="none";
				document.getElementById('divnoofquestionsperpage').style.visibility="hidden";
				f.noofquestionsperpage.value='';	
			}		
		}
	}
}
function changeAnswerSavinType(){
	var f=document.addExam;
	for(var i=0;i<f.answersavingtype.length;i++){
		if(f.answersavingtype[i].checked == true){
			if(f.answersavingtype[i].value == 'T'){
				document.getElementById('spananswersavinginterval').innerHTML=" Minutes";
			}else{
				document.getElementById('spananswersavinginterval').innerHTML= " Question(s)";
			}		
		}
	}
}
</script>
</HEAD>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0 onload='javascript:document.addExam.Exam_code.focus();' >
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
       <!-- <TR> Topnav Image 
        <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
        </TR>-->
        <!-- <TR>
        <TD bgColor=#7292c5 width=780>&nbsp;</TD>
        </TR> -->
		<tr><td width="780"><?include("includes/header.php");?></td></tr>
		<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
		</tr>		
	    <TR>
            <TD background=images/tile.jpg vAlign=top width=780 align=center>
        <form name=addExam method="post" onSubmit="return false;">
        <table width=780 cellpadding=0 cellspacing=5 border=0>
          <tr>
          <td colspan=2 align="center"><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2>Edit/Delete Subject</font></b></td>
          </tr>
         
          <tr>
				<td align="right" class="greybluetext10" width="390">Select Exam Code To Edit/Delete :&nbsp;</td>
                <td>
                <select name="Exam_code" class="greybluetext10" onchange='javascript:validate();' style="{width:250px}">
                <option value=-1>-Select-</option>
<?php 
                while(list($exam_code,$exam_name)=mysql_fetch_row($selectres))
                {
                 if($exam_code==$_POST['Exam_code'])
                       echo "<option value=$exam_code selected>$exam_code --> $exam_name</option>";
                 else
                       echo "<option value=$exam_code>$exam_code --> $exam_name</option>";
                }
?>
                </select>				</td>
            </tr>
<?
            //if(isset($_POST['submitted']) && $_POST['submitted']=='true') {?>
			<tr>
                <td align="right" width=390 class="greybluetext10" valign=top>
                Subject Name :&nbsp;                </td>
                <td align="left" valign=top>
            <select class="textbox" name="S_Code" onchange='javascript:newvalidate();check_optional(this.name);' style={width:150px} onKeyPress='javascript:submitForm()'>
                <option value=-1>-Select-</option>
				<?php
                while (list($subject_code,$subject_name, $pass_mark, $total_marks,$subject_duration,$display_score,$display_result,$pass_msg,
                    $fail_msg,$grace_pre,$grace_post,$answer_shuffling,$question_display,$optional_id,$subject_type,
                    $break_duration,$max_no_of_questions,$duration_prevent,$display_print,$languages,$display_mark,
                    $display_section,$casedisplay,$noofquestionsperpage,$answersavingtype,$answersavinginterval,$display_response,$roundoff_score)=mysql_fetch_row($result))
					{							
                             $subject_name=stripslashes($subject_name);
							
							if($_POST['S_Code']==$subject_code) {
								echo "<option value=\"$subject_code\" selected>$subject_code --> $subject_name</option>";
								$s_name=$subject_name;
								$passMark = $pass_mark;
								$totalMarks = $total_marks;
								
								$subjectDuration=$subject_duration;
								$subjectDuration_disp=sec_to_min($subjectDuration);
								$duration_prevent_disp=sec_to_min($duration_prevent);
								
								$displayScore=$display_score;
								$displayResult=$display_result;
								$displayResponse=$display_response;
								$displayRoundScore=$roundoff_score;
								$passMsg=$pass_msg;
								$failMsg=$fail_msg;
								$gracePre=$grace_pre;
								$gracePre_disp=sec_to_min($gracePre);
								
								$gracePost=$grace_post;
								$gracePost_disp=sec_to_min($gracePost);
								$display_section1=$display_section;
							    $answerShuffling=$answer_shuffling;
								$questionDisplay=$question_display;							  
								$optionalid=$optional_id;
								
								if($_POST['h_subjecttype_selected']=='false')
								{
								   $subjecttype=$subject_type;
								}
								$max_no_questions=$max_no_of_questions;								
								$breakDuration=$break_duration;
								$breakDuration_disp=sec_to_min($breakDuration);
								$displayPrint=$display_print;
								$arrLanguages = explode(",",$languages);
                                $displayMark=$display_mark;
                                $display_section1= $display_section;
                                $noofquestionsperpage1=$noofquestionsperpage;
                                $casedisplay1=$casedisplay;
                                $answersavingtype1=$answersavingtype;
                                $answersavinginterval1=$answersavinginterval;
							}
							else
								echo "<option value=\"$subject_code\">$subject_code --> $subject_name</option>";
						}
						?>
                   </select> 
              </td>
            </tr>
			<tr>
            <td align="right" class="greybluetext10">New Subject Name :&nbsp; </td>
				<td><input class="textbox" type="text" name="S_name" value="<?=$s_name?>" size=27 maxlength=50 onKeyPress='javascript:submitForm()'>				</td>
			</tr>
			<!-- Added for BOI -->
			 <?php
				if($exam_type == 3)
				{
			 ?>
			<tr>
                    <td align="right" class="greybluetext10" valign=top>
                     Subject Type :&nbsp;
                    </td>
                    <td><select name='subjecttype' class='textbox' style={width:150px} onChange="check_optional(this.name)">
						<option value=-1>-Select-</option>

					<?php if($subjecttype=='N' )
					{?>
						<option value='N' selected>Mandatory</option>
						<option value='Y' >Optional</option>
					<?php }
					else if($subjecttype=='Y')
					{?>
						<option value='N' >Mandatory</option>
						<option value='Y' selected>Optional</option>
					<?php }
					else
					{?>
						<option value='N' >Mandatory</option>
						<option value='Y' >Optional</option>
					<?php }?>

					</select>
					</td>
             </tr>  
			 <?php if($subjecttype=='Y' )
			 {?>
				 <tr>
						<td align="right" class="greybluetext10" valign=top>
						 Optional ID :&nbsp;
						</td>
						<td align="left" valign=top>
						<input class="textbox" type="text" name="optionalid" size=27 maxlength=5 onblur='javascript:check_id()'; value=<?php if($optionalid==0) echo ''; else echo $optionalid;?>>
						</td>
				 </tr>  
			<?}?>
             <tr>
                    <td align="right" class="greybluetext10" valign=top>
                    Break Duration :&nbsp;
                    </td>
                    <td align="left" valign=top>
                    <input class="textbox" type="text" name="duration_brk" size=27 maxlength=5 value='<?=$breakDuration_disp ?>' onKeyPress='javascript:submitForm()'>
					<span class="greybluetext10">In Minutes</span>
                    </td>
             </tr>
             <tr>
                    <td align="right" class="greybluetext10" valign=top>
                     Maximum No: of Questions :&nbsp;
                    </td>
                    <td align="left" valign=top>
                    <input class="textbox" type="text" name="maxno_ofquestions" size=27 maxlength=3 value=<?=$max_no_questions?>>
                    </td>
             </tr>
			<?php
				}
			?>
			<!-- Added for BOI ends here-->
            <tr>
                    <td align="right" class="greybluetext10" valign=top>
                    Total Marks :&nbsp;                    </td>
                    <td align="left" valign=top><input class="textbox" type="text" name="totalmarks" size=27 maxlength=5 value='<?=$totalMarks ?>' <?php ($rowcount > 0) ? print(" readonly") : print(""); ?> onKeyPress='javascript:submitForm()'>
                    <input type=hidden name='hid_total_marks' value='<?=$totalMarks ?>'>                    </td>
            </tr>	            
			<tr>
                    <td align="right" class="greybluetext10" valign=top>
                    Pass Mark :&nbsp;                    </td>
                    <td align="left" valign=top>
                    <input class="textbox" type="text" name="passmark" size=27 maxlength=5 value='<?=$passMark ?>' onKeyPress='javascript:submitForm()'>                    </td>
            </tr>
             <tr>
                    <td align="right" class="greybluetext10" valign=top>
                    Subject Duration :&nbsp;                    </td>
                    <td align="left" valign=top>
                    <input class="textbox" type="text" name="duration_sub" size=27 maxlength=5 value='<?=$subjectDuration_disp ?>' onKeyPress='javascript:submitForm()' onBlur="calcprevsub(this)">
					<span class="greybluetext10">In Minutes</span>                    </td>
             </tr>
              <tr>
                <td align="right" valign=top class="greybluetext10">Duration to prevent submit (Default 3/4 subject duratiion): </td>
                <td align="left" valign=top><input class="textbox" type="text" name="duration_prevent" value="<?=$duration_prevent_disp?>" size=27 maxlength=5>
                <span class="greybluetext10">In Minutes</span></td>
              </tr>
             
            <tr>
                    <td align="right" class="greybluetext10" valign=top>
                     Pre Grace Time :&nbsp;                    </td>
                    <td align="left" valign=top>
                    <input class="textbox" type="text" name="pre_grace" size=27 maxlength=5 value='<?=$gracePre_disp ?>' onKeyPress='javascript:submitForm()'>
					<span class="greybluetext10">In Minutes</span>                    </td>
             </tr>
                  
             <tr>
                    <td align="right" class="greybluetext10" valign=top>
                     Post Grace Time :&nbsp;                    </td>
                    <td align="left" valign=top>
                    <input class="textbox" type="text" name="post_grace" size=27 maxlength=5 value='<?=$gracePost_disp ?>' onKeyPress='javascript:submitForm()'>
					<span class="greybluetext10">In Minutes</span>                    </td>
             </tr>        
             
             <tr>
                <td align='right' class='greybluetext10'>Pass Message : </td>
                <td>
                	<textarea  name="PassTxt" cols=30 rows=5 maxlength=500  onKeyPress='javascript:submitForm()'><?echo $passMsg;?></textarea>                </td>
            </tr>
          <tr>
                <td align='right' class='greybluetext10'>Fail Message : </td>
                <td>
                	<textarea  name="FailTxt" cols=30 rows=5 maxlength=500 value=''><?=$failMsg?></textarea>                </td>
            </tr>
            
            <tr>
                    <td align="right" class="greybluetext10" valign=top>
                    Display Score :&nbsp;&nbsp;                    </td>
                    
                    <td align="left" valign=top>
                    <?
                    if(trim($displayScore)=="Y"){?>
                    	<INPUT TYPE=checkbox  name="disp_score" <?=$disp_chked;?>>
                    <?}//if
                    elseif(trim($displayScore)=="N"){?>
	                    <INPUT TYPE=checkbox  name="disp_score" <?=$disp_unchked;?>>
                    <?}
                    ?>                    </td>
             </tr>
             
             <tr>
                    <td align="right" class="greybluetext10" valign=top>
                    Display Result:&nbsp;&nbsp;                    </td>
                    <td align="left" valign=top>
                    <?
                    if(trim($displayResult)=="Y"){?>
                    <INPUT TYPE=checkbox   name="disp_result" <?=$disp_chked;?>>
                    <? }
                    elseif(trim($displayResult)=="N"){?>
                    <INPUT TYPE=checkbox   name="disp_result" <?=$disp_unchked;?>>
                    <? }?>                    </td>
             </tr>
             <tr>
                    <td align="right" class="greybluetext10" valign=top>
                    Display Respose:&nbsp;&nbsp;                    </td>
                    <td align="left" valign=top>
                    <?
                    if(trim($displayResponse)=="Y"){?>
                    <INPUT TYPE=checkbox   name="disp_resp" <?=$disp_chked;?>>
                    <? }
                    elseif(trim($displayResponse)=="N"){?>
                    <INPUT TYPE=checkbox   name="disp_resp" <?=$disp_unchked;?>>
                    <? }?>                    </td>
             </tr>
             <tr>
                    <td align="right" class="greybluetext10" valign=top>
                    Round Off - Score Calculation:&nbsp;&nbsp;                    </td>
                    <td align="left" valign=top>
                    <?
                    if(trim($displayRoundScore)=="Y"){?>
                    <INPUT TYPE=checkbox   name="roundoff_score" <?=$disp_chked;?>>
                    <? }
                    elseif(trim($displayRoundScore)=="N"){?>
                    <INPUT TYPE=checkbox   name="roundoff_score" <?=$disp_unchked;?>>
                    <? }?>                    </td>
             </tr>
			 <tr>
               <td align="right" class="greybluetext10" valign=top>Print button Display in  score(response)  page: </td>
               <td align="left" valign=top>
			    <?php 
                    if(trim($displayPrint)=="Y"){?>
                    <INPUT TYPE=checkbox   name="disp_print" <?=$disp_chked;?> >
                    <? }
                    elseif(trim($displayPrint)=="N"){?>
                    <INPUT TYPE=checkbox   name="disp_print" <?=$disp_unchked;?>>
                    <? }?>   
			      </td>
             </tr>
			  <tr>
               <td align="right" class="greybluetext10" valign=top>Display Marks in Question paper page: </td>
               <td align="left" valign=top>
			    <?php 
                    if(trim($displayMark)=="Y"){?>
                    <INPUT TYPE=checkbox   name="disp_mark" <?=$disp_chked;?> >
                    <? }
                    elseif(trim($displayMark)=="N"){?>
                    <INPUT TYPE=checkbox   name="disp_mark" <?=$disp_unchked;?>>
                    <? }?>   
			      </td>
             </tr>
        	  <tr>
               <td align="right" class="greybluetext10" valign=top>Display Section Name in Question paper page:&nbsp;&nbsp;</td>
               <td align="left" valign=top><INPUT TYPE=checkbox value='Y'  name="disp_section" <?PHP if(trim($display_section1) =='Y') echo 'checked="checked"';?>/></td>
             </tr> 
    		  <tr>
              <td align="right" class="greybluetext10" valign=top>Answer Shuffling:&nbsp;&nbsp;</td>
               <td align="left" valign=top><INPUT TYPE=checkbox value='Y'  name="answer_shuffling"  <?PHP if(trim($answerShuffling) =='Y') echo 'checked="checked"';?></td>
             </tr> 
             <tr>
             <td align="right" width=390 class="greybluetext10" valign=top>Question Display :&nbsp;</td>
                <td align="left" valign=top class="greybluetext10">
                <input type="radio" value='O' name=question_display onclick="changeQuestionDisplay()" <?PHP if(trim($questionDisplay) =='O') echo 'checked="checked"';?> >One question per page <br/>
                <input type="radio" value='A' name=question_display onclick="changeQuestionDisplay()" <?PHP if(trim($questionDisplay) =='A') echo 'checked="checked"';?>>All questions in single page <br/>
                <input type="radio" value='M' name=question_display onclick="changeQuestionDisplay()" <?PHP if(trim($questionDisplay) =='M') echo 'checked="checked"';?> >Parameterize no.of question(s) per page</td>
			</tr>
	        <tr>
            <td  colspan="2" align="left"  class="greybluetext10" valign=top>
            <?PHP if($questionDisplay == 'M') $style='"visibility:visible;display:block;"'; else $style='"visibility:hidden;display:none;"';?>                                    
       <div id="divnoofquestionsperpage" <?php echo $style;?> align=left>
     &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp;	 &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
     &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Enter No.Of Question's per Page: 
        &nbsp; &nbsp; <input class="textbox" type="text" name="noofquestionsperpage" id="noofquestionsperpage" value="<?PHP echo $noofquestionsperpage1;?>"/> 
                </div>    		
	    	</td></tr>			 		
         <tr>
        <td align="right" width=390 class="greybluetext10" valign=top>Answer Saving Type :&nbsp;</td>
                <td align="left" valign=top class="greybluetext10">
			<input type="radio" value='Q' name="answersavingtype"  onclick="changeAnswerSavinType()"  <?PHP if(trim($answersavingtype1) =='Q') echo 'checked="checked"';?>  > Question Based &nbsp;&nbsp;
				<input type="radio" value='T' name="answersavingtype" onclick="changeAnswerSavinType()"  <?PHP if(trim($answersavingtype1) =='T') echo 'checked="checked"';?>> Time Based </td>
			</tr>
        <tr>
        <td align="right" width=390 class="greybluetext10" valign=top> Answer Saving Interval:</td>
				<td align="left" valign=top class="greybluetext10">&nbsp;
     <input class="textbox" type="text" name="answersavinginterval" id="answersavinginterval" Maxlength=5 value="<?PHP echo $answersavinginterval1;?>" />
				<span id="spananswersavinginterval"> Question(s)</span></td>
	  </tr>	
        <tr>
           <td align="right" width=390 class="greybluetext10" valign=top>Display Case Question :&nbsp;</td>
           <td align="left" valign=top class="greybluetext10">
           <input type="radio" value='A' name="casedisplay"  <?PHP if(trim($casedisplay1) =='A') echo 'checked="checked"';?> >All case type questions single page &nbsp;&nbsp;
           <input type="radio" value='S' name="casedisplay"  <?PHP if(trim($casedisplay1) =='S') echo 'checked="checked"';?> >Single case type question  per Page</td>
        </tr>
			 <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Languages :&nbsp;</td>
                <td align="left" valign=top class="greybluetext10">
<?php
	$sqlLang="select lang_code,lang_name from iib_languages where is_active='Y'";
	$resLang=mysql_query($sqlLang);
	$strHTML = '';
	$strHTML .= "<table border='0' cellspacing='0' cellpadding='0'><tr>";
	$colCT=0;
	while (list($langCode, $langName) = @mysql_fetch_row($resLang))
	{
		if(@in_array($langCode, $arrLanguages))
			$checked = 'checked';
		else
			$checked = '';
		$strHTML .= "<td valign=top class='greybluetext10'><input type='checkbox' id='languages' name='languages[]' value='".$langCode."' $checked >".$langName."</td>";
		$colCT++;
		if(($colCT % 3)== 0)
			$strHTML .= "</tr><tr><td colspan='3'>&nbsp;</td></tr><tr>";
	}
	$strHTML .= "</tr></table>";
	echo $strHTML;
?>
				</td>
			</tr>             
           <?php 
            if ($rowcount > 0)
            {
	        ?>
            <tr>
            	<td colspan=2 align=center class='greybluetext10'> Total marks : Non-editable - Question Papers already generated </td>
            </tr>
            <?php
        	}
            ?>		
			<tr>
				<td colspan=2 align=center>
				<input class="button" type="button" name="edit_record" value="Update" onClick="javascript:e_validate();">
				<input class="button" type="button" name="del" value="Delete" onClick="javascript:d_validate();">
				<input type="button" class="button" name="bReset" value="Reset" onClick='javascript:clearValues();'>				</td>
			</tr>
			 <?if(isset($err) && $err!=''){?>
          <tr>
              <td colspan=2 align="center" class='alertmsg'><b><?echo $emsg;?></b></td>
          </tr>
          <?}?>
          <?if(isset($done) && $done!='') {?>
          <tr>
             <td colspan=2 align="center" class='alertmsg'><b><?echo $msg;?></b></td>
          </tr>
          <? } ?>
        <br><br>
		<tr>
				<td colspan=2 align="left" class=greybluetext10><b>REP_MEMBERSHIPNAME - To display Member Name</b></td>
		</tr>
		<tr>
			<td colspan=2 align="left" class=greybluetext10><b>REP_SCORE - To display score</b></td>
		</tr>
		<tr>
			<td colspan=2 align="left" class=greybluetext10><b>REP_TOTALMARK - To display total marks</b></td>
		</tr>
		<tr>
			<td colspan=2 align="left" class=greybluetext10><b>REP_SUBJECTNAME - To display Subject Name</b></td>
		</tr>
<?//}?>
						
			<?//if($submitted=='false'){?>
            <!--<tr>
               <td align="right"><input type="button" class=button name=bSubmit value="Submit" onclick='javascript:validate();'></td>
               <td align="left"><input type="reset" class=button name=bReset value="Cancel"></td>
            </tr>-->
			<?//}?>
            <input type="hidden" name="submitted">
            <input type="hidden" name="edited">
            <input type="hidden" name="deleted">
            <input type="hidden" name="hid_variable" value="<?echo $s_name;?>">
			<input type="hidden" name="h_subjecttype" value="false">
			<input type="hidden" name="h_subjecttype_selected" value="false">
            </table>
            </form>
           </TD>
        </TR>
        <!-- <TR>
                <TD bgColor=#7292c5 width=780>&nbsp;</TD>
        </TR> -->
		<?include("includes/footer.php");?>
</TABLE>
</FORM>
</center>
</BODY>
</HTML>
