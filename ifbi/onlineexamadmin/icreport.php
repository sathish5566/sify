<?php
set_time_limit(0);
/****************************************************
* Application Name            : IIB
* Module Name                 : Incomplete Candidate Details
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :
* Tables used for only selects: iib_exam,iib_exam_subjects,iib_candidate_test,iib_question_paper_details
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              : HTML Page
* Document/Reference Material :
* Created By                  : K.Jyothsna
* Created ON                  : 18-06-2007
* Last Modified By            : K.Jyothsna
* Last Modified Date          : 24-12-2007
* Description                 : This report displays details of that particular incomplete candidate.
*****************************************************/
require_once "sessionchk.php";
require_once("dbconfig.php");

	$examDate= $_REQUEST['examdate'];

	$aDates = explode("-", $examDate);
	$dispDate = $aDates[2]."-".$aDates[1]."-".$aDates[0];
	$examDate1 = $examDate." 00:00:00";
	$examDate2 = $examDate." 23:59:59";



	function downloadData($fileName){
		$file = file_get_contents ($fileName);
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename="'.$fileName.'"');
		readfile($fileName);	
		unlink($fileName);
		exit;
	}

	function getExamType($excode){
		$ex_qry	=	"select exam_type from iib_exam where exam_code='$excode'";
		$ex_res	=	mysql_query($ex_qry);
		list($exam_type)=mysql_fetch_array($ex_res);
		return $exam_type;
	}

	function getMaxNoQuestions($excode,$subcode){
		$q_qry	=	"select max_no_of_questions from iib_exam_subjects where exam_code='$excode' and subject_code='$subcode'";
		$q_res	=	mysql_query($q_qry);
		list($no_of_question)=mysql_fetch_array($q_res);

		if($no_of_question=='' || $no_of_question==0)
			return 0;
		else
			return $no_of_question;
	}

	
function getscoreformat($excode,$subcode){
	$q_qry1	=	"select roundoff_score from iib_exam_subjects where exam_code='$excode' and subject_code='$subcode'";
	$q_res1	=	mysql_query($q_qry1);
	list($roundoff_score)=mysql_fetch_array($q_res1);

	if($roundoff_score=='' || $roundoff_score==0)
		return 0;
	else
		return $roundoff_score;
}


	$filename	=	"IncompleteScore_".$dispDate.".csv";
	$excel_header=array("MembershipNo","ExamCode","SubjectCode","QuestionPaperNo","Score");
	foreach($excel_header as $key => $value){
		$excelTitle.=$value.',';
	}

	$fh = fopen($filename, 'w') or die("can't open file");
	fwrite($fh, $excelTitle."\n");


	$score = 0;	
	$arrMem=array();
	$sql = "SELECT a.membership_no,a.exam_code,a.subject_code,question_paper_no,a.browser_status FROM iib_candidate_test a LEFT JOIN iib_candidate_scores b ON a.membership_no=b.membership_no AND a.exam_code=b.exam_code AND a.subject_code=b.subject_code WHERE b.subject_code IS NULL AND a.start_time between '$examDate1'  AND '$examDate2' order by exam_code,subject_code";
	$res = mysql_query($sql);	
	$nRows = mysql_num_rows($res);

    if ($nRows > 0)
	{												
    	while (list($membershipNo, $examCode, $subjectCode,$QPno,$browserstatus) = mysql_fetch_row($res))
		{											
			$val=trim($membershipNo.'_'.$examCode.'_'.$subjectCode.'_'.$QPno);			
			if(!in_array($val,$arrMem))
			{							
				array_push($arrMem,$val);
				//echo $membershipNo.'_'.$examCode.'_'.$subjectCode.'_'.$QPno."<br>";

					
			$score = 0;	
			$posscore = 0;	
			$negscore=0;
				//********************** Score Calculation Starts here ************************//
			if(getExamType($examCode)==3 && getMaxNoQuestions($examCode,$subjectCode)!=0)
			{
				$max_no_of_questions=getMaxNoQuestions($examCode,$subjectCode);	
				$sql_select = "select answer,question_id from iib_question_paper_details where question_paper_no ='$QPno' order by updated_time";	
				$sql_select = "select answer,question_id from iib_response where question_paper_no ='$QPno' and id IN (select  max(id) from iib_response where question_paper_no ='$QPno' group by question_id)";
					
				$res_select  =mysql_query($sql_select); 
				$score_inc=1;
				while(list($ans,$ques_id)=mysql_fetch_row($res_select))
				{
					$sqlAnswer = "SELECT correct_answer, marks, negative_marks FROM iib_section_questions WHERE question_id=$ques_id";				
					$resAnswer = mysql_query($sqlAnswer);   		
					list($correctAns, $marks, $negMarks) = mysql_fetch_row($resAnswer);  		  		  			
					if(($max_no_of_questions >=$score_inc) && ($ans!=""))
					{
					
						if (trim($ans) == trim($correctAns))
						{	
							$posscore += $marks;
						}  		
						if ((trim($ans) != trim($correctAns)) && (trim($ans) != ""))
						{
							$negscore += $negMarks;
						}
						$score_inc++;
					}	  										
				}
			}else{
				/* $sql_select = "SELECT SUM(A.marks) FROM iib_section_questions A  LEFT JOIN iib_question_paper_details B ON A.question_id=B.question_id where B.question_paper_no ='$QPno' AND A.correct_answer=B.answer";			
				
				$sql_select1 = "SELECT SUM(A.negative_marks) FROM iib_section_questions A  LEFT JOIN iib_question_paper_details B ON A.question_id=B.question_id where B.question_paper_no='$QPno' AND A.correct_answer!=B.answer AND B.answer!=''";
*/
			$roundoff_score=getscoreformat($examCode,$subjectCode);

$sql_select = "SELECT SUM(A.marks) FROM iib_section_questions A  LEFT JOIN iib_response B ON A.question_id=B.question_id where B.question_paper_no ='$QPno' AND B.id IN (select  max(id) from iib_response where question_paper_no ='$QPno' group by question_id)  AND A.correct_answer=B.answer";

        $sql_select1 = "SELECT SUM(A.negative_marks) FROM iib_section_questions A  LEFT JOIN iib_response B ON A.question_id=B.question_id where B.question_paper_no='$QPno' AND B.id IN (select max(id) from iib_response where question_paper_no ='$QPno' group by question_id) AND A.correct_answer!=B.answer AND B.answer!=''";
				$res_select = mysql_query($sql_select); 
				list($posscore)=mysql_fetch_row($res_select);

				$res_select1 = mysql_query($sql_select1); 
				list($negscore)=mysql_fetch_row($res_select1);
			}


				if(isset($negscore) && $negscore > 0)	
				{
					$score = $posscore - $negscore;		
				}else
				  $score = $posscore;	
				
				
				if($roundoff_score=='Y')
					$score = round($score);
				else
					$score = $score;

				if ($score == -0)
					$score = 0;
				//code added to convert negative marks to 0
				if ($score < 0)
					$score = 0;
				//********************** Score Calculation Ends here ************************//
						
				//echo $membershipNo.'_'.$examCode.'_'.$subjectCode.'_'.$QPno.'_'.$score."<br>";
				$excelRows.=$membershipNo.','.
							$examCode.','.
							$subjectCode.','.
							$QPno.','.
							$score."\n";
				fwrite($fh, $excelRows);
				$excelRows='';

			}
        }
		fclose($fh);
		downloadData($filename);
		exit;
	}else{
		header("Location:select_details_incompletescorerep.php");
	}
	
	//echo "<pre>";
	//print_r($arrMem);

	

	/*$sql_IC = "select a.membership_no as MembershipNo, a.subject_code as SubjectCode, a.question_paper_no as QuestionPaperNo, b.score_calc as Score  from iib_candidate_test a, iib_score_calculation b  where b.membership_no = a.membership_no and b.subject_code = a.subject_code and a.question_paper_no = b.question_paper_no and a.test_status='IC' and a.current_session='Y'";
	$res_IC	=	mysql_query($sql_IC);
	

	$filename	=	"IncompleteScore_".$subject_code.".csv";

	$excel_header=array("MembershipNo","SubjectCode","Score");
	foreach($excel_header as $key => $value){
		$excelTitle.=$value.',';
	}

	$fh = fopen($filename, 'w') or die("can't open file");
	fwrite($fh, $excelTitle."\n");

	while($row_IC=mysql_fetch_array($res_IC))
	{
		$excelRows.=$row_IC['MembershipNo'].','.
					$row_IC['SubjectCode'].','.
					$row_IC['Score']."\n";
		fwrite($fh, $excelRows);
		$excelRows='';
	}
	fclose($fh);
	downloadData($filename);
	exit;*/
?>
