<?php
/****************************************************
* Application Name            :  IIBF 
* Module Name                 :  Time change report for IIB Exams
* Revision Number             :  1
* Revision Date               :
* Table(s)                    : 
* Tables used for only selects: iib_time_change_logs,iib_exam,iib_exam_subjects
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              :  CSV file and HTML page
* Document/Reference Material :
* Created By                  :  
* Created ON                  :  
* Last Modified By            :  K.KARUNA MOORTHY RAJU
* Last Modified Date          :  19-03-2006
* Description                 : This report displaying the detials of the time change report for the every candidate with the related information.It can be downloaded as a csv file
*****************************************************/
	ob_start();
	require("dbconfig_slave_123.php");
	require_once "sessionchk.php";
	$commondebug=false;
	$examCode=$_POST['sExam_code'];
	$subjectCode=$_POST['sSub_code'];
	$submitted=$_POST['submitted'];
	$exam=$_POST['exam'];
	$sub=$_POST['sub'];

/******** Get values from the Download button  **********/
 
    $ex_flag=$_POST['exl'];
  
/******** General file for excel download      *********/
  
    include_once("excelconfig.php");

/********* $filename stores the filename of the csv file with unique values ********/

     $filename="time_change_report_Exam(".$examCode.")_sub(".$subjectCode.")";

/********* Storing the excel report header details in an array named $exce_header ********/
   
     $exce_header=array("Membership No","Exam Code","Subject Code","Centre Code","Exam Date","Old Exam Time","New Exam Time","Reason");
   
    $sql_EXAM="SELECT exam_code,exam_name from iib_exam ";
	$resExam=mysql_query($sql_EXAM);
		if ($exam=='true' && $sub=='true')
	{ 
	
	$sql_sub="select subject_code,subject_name from iib_exam_subjects where exam_code='$examCode'";
	$resSub=mysql_query($sql_sub);
 	}
   if ($submitted=='true' && $examCode!='' && $subjectCode!='')
	{  
		
		//$sql_list="select t.membership_no,t.exam_code,t.subject_code,t.exam_date ,t.old_exam_time,t.new_exam_time,t.reason,i.centre_code from iib_time_change_logs t,iib_candidate_iway i where t.membership_no=i.membership_no, t.exam_code='$examCode' and t.subject_code='$subjectCode' and i.subject_code=t.subject_code and t.exam_code=i.exam_code";
		$sql_list="select membership_no,exam_code,subject_code,exam_date ,old_exam_time,new_exam_time,reason,centre_code from iib_time_change_logs  where exam_code='$examCode' and subject_code='$subjectCode'";
		$resList=mysql_query($sql_list);
		$num=mysql_num_rows($resList);
		$i=0;
		
/******** Select the queries from the tables with respect to the submitted values
          and storing it in the array named $exce_content          ***************/
		
		while(list($membership_no,$exam_code,$subject_code,$exam_date ,$old_exam_time,$new_exam_time,$reason,$centre_code)=mysql_fetch_row($resList))
		{
		          $exce_content[$i][]=$membership_no;
                  $exce_content[$i][]=$exam_code;
                  $exce_content[$i][]=$subject_code;
                  $exce_content[$i][]=strtoupper($centre_code);
                  $exce_content[$i][]=$exam_date;
                  $exce_content[$i][]=$old_exam_time;
                  $exce_content[$i][]=$new_exam_time;
                  $exce_content[$i][]=$reason;
                  $i++;
		}
	 
/******* Download the excel file when clicking the download button *************/ 

	if($ex_flag=='1')
	{
  	excelconfig($filename,$exce_header,$exce_content);
  	exit;	 
 	}
 }
?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='javascript'>
function getsubjects()
{
	var frm=document.frmalloc;
	if (frm.sExam_code.selectedIndex == 0){
		alert("Please select Exam ");
		frm.sExam_code.focus();
		return;
	}

	frm.submitted.value='false';
	frm.exam.value='true';
	frm.sub.value='true';
	frm.exl.value='2';
	frm.submit();

}

function GetValues()
{
	var frm=document.frmalloc;

	if (frm.sExam_code.selectedIndex == 0){
		alert("Please select Exam ");
		frm.sExam_code.focus();
		return;
	}
	if (frm.sSub_code.selectedIndex == 0){
		alert("Please select Subject ");
		frm.sSub_code.focus(); 
		return;
	}

	frm.submitted.value='true';
	frm.exam.value='true';
	frm.sub.value='true';
	frm.exl.value='0';
	frm.submit();

}

/* Function for excel download */

function excelsubmit()
{
	var frmname=document.frmalloc;
	frmname.submitted.value='true';
	frmname.exl.value='1';
	frmname.submit();
}
</script>
</HEAD>
<BODY leftMargin=0 topMargin=0 LeftMargin=0 marginheight=0 marginwidth=0 >
<center>
<form name='frmalloc' method='post'>
<input type='hidden' name='submitted'>
<input type=hidden name='exam' value=''>
<input type=hidden name='sub' value=''>
<input type=hidden name=exl value='0'>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780 background=images/tile.jpg >
<!--<TABLE border=0 cellPadding=0 cellSpacing=0 width=780 height=500>-->
<!--	<TR><!-- Topnav Image -->
      <!--  <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR> -->
	<tr><td width="780"><?include("includes/header.php");?></td></tr>
    <TR>

    </TR>
	<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>
	</tr>
	<TR>
        <TD  width=780>&nbsp;</TD>
    </TR>
    <TR>
    	<TD background=images/tile.jpg vAlign=top width=780 align=center>

        <table width=700 cellpadding=3 cellspacing=2 border=0 >
        	<tr>
				<td colspan=2 align="center"><b><font face="Verdana, Arial, Helvetica, sans-serif" size=2>Time change Report</b></font><br><br></td>
            </tr>
			<tr>
                <td align="center" class="greybluetext10">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Exam :&nbsp;&nbsp;
	        <select name='sExam_code' style={width:250px} class=greybluetext10 onchange='javascript:getsubjects()'>
        	        <option value='0'>--Select--</option>
			<?php
			if (mysql_num_rows($resExam) > 0){
			        while (list($ecode,$ename) = mysql_fetch_row($resExam)){
			                print("<option value='$ecode' ");
			                if ($ecode == $examCode)
			                        print(" selected ");
			                print(">$ename</option>");
				}
			}
			?>
			</select>
                </td>
        	</tr>
        	<tr>
                <td align="center" class="greybluetext10">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Subject : &nbsp;
	        <select name='sSub_code' style={width:300px} class=greybluetext10>
        	        <option value='0'>--Select--</option>
			<?php
			if (mysql_num_rows($resSub) > 0){
			        while (list($sCode,$Sname) = mysql_fetch_row($resSub)){
			                print("<option value='$sCode' ");
			                if ($sCode == $subjectCode)
			                        print(" selected ");
			                print(">$Sname</option>");
				}
			}
			?>
			</select>
            	    </td>
        </tr>
		<tr>
		<td   align="center"><input type=button name='sub_assign' value='Submit' class='button' onClick='javascript:GetValues()'></td>
		</tr> <?
		if (($submitted=='true') && ($num<1))
		 {?>
            
         	<tr>
				<td width=780 align=center class=alertmsg>No Records to Display</td>
        	</tr>     
	    <? } 

/****** Displaying the report in the html page *****/		
		
		  if ($submitted=='true' && $examCode!='' && $subjectCode!='' && $num > 0 )
		  {
			  $headerlength = count($exce_header);
             ?>
             <tr>
            	<td align="center" width=100% class="greybluetext10" valign=top colspan="2">
            <?	
				print("<table border=1 cellspacing=0 cellpadding=2><tr>");
					for($a=0;$a<$headerlength;$a++)
					{
				print("<td class=greybluetext10><b>$exce_header[$a]</b></td>");
					}
			    print("</tr>");	 
						
/******* Showing the report details such as Membership name,Exam code,Subject code,Centre code,
          Exam date,Old exam time,new exam time and Reason  ********/						
				print("<tr>");				
				for($j=0;$j<$num;$j++)
             		{
	          		for($k=0;$k<$headerlength;$k++)
	           			{
		          			?><td class="greybluetext10"><?=$exce_content[$j][$k]?></td>
		          			<?
	           			}
	        	print("</tr>");
             		} 
                 	  
				?>
				</table>
	            
				</td>
            </tr>
           <tr><td class=greybluetext10>&nbsp;</td></tr> 
          <tr><td class=greybluetext10 align="center" class=greybluetext10><input class='button' type='button' name='download' value='Download' onClick='javascript:excelsubmit()'></td></tr>
            <tr><td class=greybluetext10>&nbsp;</td></tr> 
             <?  } ?>		
            </table>
<TR>
   	<?include("includes/footer.php");?> 
</TR>
</form>
</center>
</body>
</html>
