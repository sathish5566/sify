<?php
require_once "sessionchk.php";
require_once("dbconfig.php");
$admin_flag = 0;
?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='JavaScript' src="./includes/validations.js"></script>
<script language="javascript">

function populatesubject(){
	frm = document.frmMember;
	tmp="";
	for (i = 0; i < frm.sSubjectCode.length;i++)
		if (frm.sSubjectCode[i].selected==true)
		if (tmp == "")
			tmp = frm.sSubjectCode[i].value;
		else
			tmp = tmp+","+frm.sSubjectCode[i].value;
	frm.hSubjectCodes.value =tmp;
	frm.action="add_candidate.php#examcode";
	frm.method="post";
	frm.submit();
}
function chksplchar(value2chk){
myRegExp = new RegExp("[^a-zA-Z0-9 ]");
   	val=value2chk;
	result=val.match(myRegExp);
	if (result)
		return true;
	else
		return false;
}
function chkchar(value2chk){
myRegExp = new RegExp("[^a-zA-Z ]");
   	val=value2chk;
	result=val.match(myRegExp);
	if (result)
		return true;
	else
		return false;
}
function chknum(value2chk){
myRegExp = new RegExp("[^0-9]");
   	val=value2chk;
	result=val.match(myRegExp);
	if (result)
		return true;
	else
		return false;
}

function validate(){
	frm = document.frmMember;
	tmp="";
	if (trim(frm.tMemNo.value) == ""){
		alert("Member Number Should Not Be Empty");
		frm.tMemNo.focus();
		return;
	}
	if (chksplchar(trim(frm.tMemNo.value))){
		alert("Member Number Can accept only Alphanumeric text");
		frm.tMemNo.focus();
		return;
	}
	if (trim(trim(frm.tMemName.value)) == ""){
		alert("Member Name Should Not Be Empty");
		frm.tMemName.focus();
		return false;
	}
	if (chkchar(trim(frm.tMemName.value))){
		alert("Member Name Can accept only Alphanumeric text");
		frm.tMemName.focus();
		return;
	}
	if(!frm.rSex[0].checked && !frm.rSex[0].checked){
		alert("Please select Gender");	
		frm.rSex[0].focus();
		return false;
	}
	a = frm.taAddress1.value+'a';
	a = trim(a);
//	if (trim(frm.taAddress1.value) == null)
	if (a.length == 1){
		alert("Address1 Should Not Be Empty");
		frm.taAddress1.focus();
		return false;
	}
/*	if (trim(frm.taAddress2.value) == ""){
		alert("Address2 Should Not Be Empty");
		frm.taAddress2.focus();
		return false;
	}
	if (trim(frm.taAddress3.value) == ""){
		alert("Address3 Should Not Be Empty");
		frm.taAddress3.focus();
		return false;
	}
	if (trim(frm.taAddress4.value) == ""){
		alert("Address4 Should Not Be Empty");
		frm.taAddress4.focus();
		return false;
	}	*/
	if (trim(frm.tCity.value) == ""){
		alert("City Should Not Be Empty");
		frm.tCity.focus();
		return false;
	}
	if (chksplchar(frm.tCity.value)){
		alert("City Can accept only Alphanumeric text");
		frm.tCity.focus();
		return;
	}	
	if (trim(frm.tState.value) == ""){
		alert("State Should Not Be Empty");
		frm.tState.focus();
		return false;
	}
	if (chksplchar(frm.tState.value)){
		alert("State Can accept only Alphanumeric text");
		frm.tState.focus();
		return;
	}		
	if (trim(frm.tStateCode.value) == ""){
		alert("StateCode Should Not Be Empty");
		frm.tStateCode.focus();
		return false;
	}
	if (chksplchar(frm.tStateCode.value)){
		alert("StateCode Can accept only Alphanumeric text");
		frm.tStateCode.focus();
		return;
	}			
	if (trim(frm.tPinCode.value) == ""){
		alert("PinCode Should Not Be Empty");
		frm.tPinCode.focus();
		return false;
	}
	if (chknum(frm.tPinCode.value)){
		alert("PinCode Can accept only Numeric text");
		frm.tPinCode.focus();
		return;
	}				
	
	if (trim(frm.tZoneCode.value) == ""){
		alert("ZoneCode Should Not Be Empty");
		frm.tZoneCode.focus();
		return false;
	}
	if (chksplchar(frm.tZoneCode.value)){
		alert("ZoneCode Can accept only Alphanumeric text");
		frm.tZoneCode.focus();
		return;
	}		
	if (frm.sExamCode.selectedIndex == 0){
		alert("Select a Exam");
		frm.sExamCode.focus();
		return false;
	}
//	alert(frm.sSubjectCode.length);
	if (!frm.sSubjectCode.length){
	alert("No Subjects Listed, Please check the Exam");
		frm.sExamCode.focus();
		return false;
	}	
	else{
		j=0;
		for (i=0;i<frm.sSubjectCode.length;i++)
			if (frm.sSubjectCode[i].selected == true)
				j++;
		if (j == 0){
			alert("At least one Subject has to be selected");
			frm.sSubjectCode.focus();
		return false;
		}
	}
	if (frm.sCentreCode.selectedIndex == 0){
		alert("Select a Centre Code");
		frm.sCentreCode.focus();
		return false;
	}
	if (frm.sMedium.selectedIndex == 0){
		alert("Select a Medium");
		frm.sMedium.focus();
		return false;
	}
	if (trim(frm.tInstitutionCode.value) == ""){
		alert("Institution Code Should Not Be Empty");
		frm.tInstitutionCode.focus();
		return false;
	}
	if (chksplchar(frm.tInstitutionCode.value)){
		alert("Institution Code Can accept only Alphanumeric text");
		frm.tInstitutionCode.focus();
		return;
	}		
	if (trim(frm.tInstitutionName.value) == ""){
		alert("Institution Name Should Not Be Empty");
		frm.tInstitutionName.focus();
		return false;
	}		
/*	if (chksplchar(frm.tInstitutionName.value)){
		alert("Institution Name Can accept only Alphanumeric text");
		frm.tInstitutionName.focus();
		return;
	}		*/
		for (i = 0; i < frm.sSubjectCode.length;i++)
		if (frm.sSubjectCode[i].selected==true)
		if (tmp == "")
			tmp = frm.sSubjectCode[i].value;
		else
			tmp = tmp+","+frm.sSubjectCode[i].value;
	frm.hSubjectCodes.value =tmp;
	tmp = Math.round(((1 - 0.5) +( ( (5+0.49999) - (1-0.5) ) * Math.random() ) )*100000);
	frm.hTmp.value = tmp;
	frm.action="inscandidate.php?id=1";
	frm.method="post";
	frm.submit();
}
</script>
</HEAD>
<BODY leftMargin=0 topMargin=0 LeftMargin=0 marginheight=0 marginwidth=0>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
	<TR><!-- Topnav Image -->
<!--        <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR> -->
	<TR><TD WIDTH=780><? include("includes/header.php");?></TD></TR>
    <TR>
        
    </TR>
	<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>
    <TR>
    	<TD background=images/tile.jpg vAlign=top width=780 align=center>
    	<form name='frmMember' method="post" onsubmit="return false;">        
        <table width=780 cellpadding=0 cellspacing=5 border=0>
        	<tr>
	           	<td colspan=2 align="center"><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2>Add New Candidate</b></font></td>
            </tr>                
			<tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Member Number : </td>
                <td align="left" valign=top><input type="text" class="textbox" size=30 name="tMemNo" value='<? echo $_REQUEST["tMemNo"]; ?>' maxlength=20></td>
			</tr>
			<tr>
                <td align="right" width=390 class="greybluetext10" valign=top>Member Name : </td>
                <td align="left" valign=top><input type="textbox" class="textbox" size=30 name="tMemName" value='<? echo $_REQUEST["tMemName"]; ?>'></td>
			</tr> 
			<tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Gender :&nbsp;</td>
                <td align="left" valign=top class="greybluetext10">
				<input type="radio" value='M' name=rSex <? if ($_REQUEST["rSex"] == 'M') echo "checked"; ?>> Male &nbsp;&nbsp;
				<input type="radio" value='F' name=rSex <? if ($_REQUEST["rSex"] == 'F') echo "checked"; ?>>Female</td>
			</tr>   
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top> Address1 :&nbsp;</td>
                <td align="left" valign=top><textarea class="textarea" name="taAddress1" cols=30 rows=5 ><? echo $_REQUEST["taAddress1"]; ?></textarea></td>
			</tr>
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top> Address2 :&nbsp;</td>
				<td align="left" valign=top><textarea class="textarea" name="taAddress2" cols=30 rows=5><? echo $_REQUEST["taAddress2"]; ?></textarea></td></td>
			</tr>
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top> Address3 :&nbsp;</td>
                <td align="left" valign=top><textarea class="textarea" name="taAddress3" cols=30 rows=5><? echo $_REQUEST["taAddress3"]; ?></textarea></td>
			</tr>
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top> Address4 :&nbsp;</td>
                <td align="left" valign=top><textarea class="textarea" name="taAddress4" cols=30 rows=5><? echo $_REQUEST["taAddress4"]; ?></textarea></td>
			</tr>
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top> Address5/City :&nbsp;</td>
                <td align="left" valign=top><input type="textbox" class="textbox" size=30 name="tCity" value='<? echo $_REQUEST["tCity"]; ?>'></td>
			</tr>
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top> Address6/State :&nbsp;</td>
                <td align="left" valign=top><input type="textbox" class="textbox" size=30 name="tState" value='<? echo $_REQUEST["tState"]; ?>'></td>
			</tr>
            <tr>
				<td align="right" width=390 class="greybluetext10" valign=top> State Code :&nbsp;</td>
                <td align="left" valign=top><input type="text" class="textbox" size=30 name="tStateCode" value='<? echo $_REQUEST["tStateCode"]; ?>' maxlength=20></td>
            </tr>    
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top> Pin Code : </td>
                <td align="left" valign=top><input type="text" class="textbox" size=30 name="tPinCode" value='<? echo $_REQUEST["tPinCode"]; ?>' maxlength=20></td>
			</tr>
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top> Zone Code : </td>
                <td align="left" valign=top><input type="text" class="textbox" size=30 name="tZoneCode" value='<? echo $_REQUEST["tZoneCode"]; ?>' maxlength=20></td>
			</tr>
			<tr>
               	<td align="right" width=390 class="greybluetext10" valign=top><a name=examcode></a> Exam Code : </td>
            	<td align="left" valign=top><select name="sExamCode" onchange="populatesubject()" class="textbox" style="width:165;">
				<option value=select selected>-Select-</option>
				<?
				$sql =	"select distinct exam_code,exam_name from iib_exam where online = 'Y'";
				$res=mysql_query($sql);
				while (list($exam_code,$exam_name)=mysql_fetch_row($res)){
					if ($exam_code == $_REQUEST["sExamCode"])
						echo "<option value='$exam_code' selected>$exam_code --> $exam_name</option>";
					else
						echo "<option value='$exam_code'>$exam_code --> $exam_name</option>";
				}
				?>
				</select>
				</td>
			</tr>
			<tr>
               	<td align="right" width=390 class="greybluetext10" valign=top> Subjects Appearing : </td>
            	<td align="left" valign=top><select name="sSubjectCode" multiple class="textbox" style="width:165;height:100"> 
				<?
				if ($_REQUEST["sExamCode"]<> NULL){
				$sql =	"select distinct subject_code,subject_name from iib_exam_subjects where exam_code='".$_REQUEST["sExamCode"]."' and online = 'Y'";
				$res=mysql_query($sql);
				while (list($subject_code,$subject_name)=mysql_fetch_row($res)){
					if ($subject_code == $_REQUEST["sSubjectCode"])
						echo "<option value='$subject_code' selected>$subject_code --> $subject_name</option>";
					else
						echo "<option value='$subject_code'>$subject_code --> $subject_name</option>";
				}
				}
				?>
				</select>
				</td>
			</tr>
			<tr>
				<td align="right" width=390 class="greybluetext10" valign=top>Exam Centre Code : </td>
                <td align="left" valign=top>
				<select name="sCentreCode" class="textbox" style="width:165">
				<option value=select selected>-Select-</option>
				<?
				$centre_sql =	"select exam_centre_code,exam_centre_name from iib_exam_centres where online = 'Y'";
				$centre_res=mysql_query($centre_sql);
				while (list($exam_centre_code,$exam_centre_name)=mysql_fetch_row($centre_res)){
					if ($exam_centre_code == $_REQUEST["sCentreCode"])
						echo "<option value='$exam_centre_code' selected>$exam_centre_code --> $exam_centre_name</option>";
					else
						echo "<option value='$exam_centre_code'>$exam_centre_code --> $exam_centre_name</option>";
				}
				?>
				</select>
				 </td>
			</tr>                    
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top> Medium : </td>
                <td align="left" valign=top>
				<select name=sMedium class="textbox" style="width:165">
				<option>-Select-</option>
				<option value="H" <? if ($_REQUEST["sMedium"]=="H") echo "selected";?>>HINDI</option>
				<option value="E" <? if ($_REQUEST["sMedium"]=="E") echo "selected";?>>ENGLISH</option>				
				</select>
				</td>
			</tr>
            <tr>
               	<td align="right" width=390 class="greybluetext10" valign=top> Institution Code : </td>
                <td align="left" valign=top><input type="text" class="textbox" size=30 name="tInstitutionCode" value='<? echo $_REQUEST["tInstitutionCode"]; ?>'></td>
			</tr>
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top> Institution Name : </td>
                <td align="left" valign=top><input type="text" class="textbox" size=30 name="tInstitutionName" value='<? echo $_REQUEST["tInstitutionName"] ; ?>'></td>
			</tr>
			<input type=hidden name=hSubjectCodes value="">
			<input type=hidden name=hTmp>							
			<tr>
				<td colspan=2 align=center>
				<input class="button" type="button" name="sub_save" value="Save" onclick="validate();">
				<input class="button" type="reset" name="reset" value="Reset" ></td>
			</tr>
		</table>
        </form>
        </TD>
	</TR>
    <TR>
    	<?include("includes/footer.php");?>
    </TR>
</TABLE>
</FORM>
</center>
</BODY>
</HTML>