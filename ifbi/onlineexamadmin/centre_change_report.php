<?php

/****************************************************
* Application Name            :  IIB
* Module Name                 :  Centre change report
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :
* Tables used for only selects: iib_exam,iib_exam_subjects,iib_centre_change_logs
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              :  CSV file and HTML page
* Document/Reference Material :
* Created By                  :
* Created ON                  :
* Last Modified By            :  K.KARUNA MOORTHY RAJU
* Last Modified Date          :  19-03-2006
* Description                 : This report displaying the details about the candidates who are changing their exam centres with related information. It can be downloaded as a csv file.
*****************************************************/
	ob_start();
	require("dbconfig_slave_123.php");
	require_once "sessionchk.php";
/***** Including the file for downloading the excel file **********/
	include_once("excelconfig.php");
	$sql_EXAM="SELECT exam_code,exam_name from iib_exam order by exam_code";
	$resExam=mysql_query($sql_EXAM);
		$examCode=$_POST['sExam_code'];
		$subjectCode=$_POST['sSub_code'];
		$submitted=$_POST['submitted'];
		$exam=$_POST['exam'];
		$sub=$_POST['sub'];

/****** Getting the value when clicking the download button   ************/
    	$ex_flag=$_POST['exl'];
/****** Name of the excel file which is to be downloaded       *********/

	$filename="centre_change_report_Exam(".$examCode.")_sub(".$subjectCode.")";

/****** Array stores the header information of the report	 *********/

	$exce_header=array("Membership No","Exam Code","Subject Code","Exam Date","Exam Time","Old Centre Code","New Centre Code","Reason");

		if ($exam=='true' && $sub=='true')
    	{
		$sql_sub="select subject_code,subject_name from iib_exam_subjects where exam_code='$examCode'";
		$resSub=mysql_query($sql_sub);

		}

/******* Executing queries with examcode and subjectcode values with respect to $examCode and subjectcode. ********/


       	if ($submitted=='true' && $examCode!='' && $subjectCode!='')
		{
		$sql_list="select membership_no,exam_code,subject_code,exam_date,exam_time,old_centre_code,new_centre_code,reason from iib_centre_change_logs where exam_code='$examCode' and subject_code='$subjectCode'";
		$resList=mysql_query($sql_list);
		$num=mysql_num_rows($resList);

/******* Storing the data such as Membership number,Exam code,Subject code,Exam date,
   Exam Time,Old centrecode,new centrecode into the array - $exce_content ********/

   	$i=0;
		while(list($memno,$examcode,$subcode,$examdate,$examtime,$o_centrecode,$n_centrecode,$reason)=mysql_fetch_row($resList))
		{
			 if ($reason=='') $reason="---";

	  		 $exce_content[$i][]=$memno;
             $exce_content[$i][]=$examcode;
             $exce_content[$i][]=$subcode;
             $exce_content[$i][]=$examdate;
             $exce_content[$i][]=$examtime;
             $exce_content[$i][]=strtoupper($o_centrecode);
             $exce_content[$i][]=strtoupper($n_centrecode);
             $exce_content[$i][]=$reason;
          $i++;
        }

/* Execution of excel download function */

        if($ex_flag=='1')
 		{
	 		excelconfig($filename,$exce_header,$exce_content);
	 		exit;
 		}
    }

?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='javascript'>
function getsubjects()
{
	var frm=document.frmalloc;

	if (frm.sExam_code.selectedIndex == 0){
		alert("Please select Exam ");
		frm.sExam_code.focus();
		return;
	}
	frm.submitted.value='false';
	frm.exam.value='true';
	frm.sub.value='true';
	frm.exl.value='0';
	frm.submit();

}

function GetValues()
{
	var frm=document.frmalloc;

	if (frm.sExam_code.selectedIndex == 0){
		alert("Please select Exam ");
		frm.sExam_code.focus();
		return;
	}
	if (frm.sSub_code.selectedIndex == 0){
		alert("Please select Exam ");
		frm.sExam_code.focus();
		return;
	}

	frm.submitted.value='true';
	frm.exam.value='true';
	frm.sub.value='true';
	frm.exl.value='0';
	frm.submit();

}
function excelsubmit()
{
	var frmname=document.frmalloc;
	frmname.submitted.value='true';
	frmname.exl.value='1';
	frmname.submit();
}
</script>
</HEAD>
<BODY leftMargin=0 topMargin=0 LeftMargin=0 marginheight=0 marginwidth=0 >
<center>
<form name='frmalloc' method='post'>
<input type='hidden' name='submitted'>
<input type=hidden name='exam' value=''>
<input type=hidden name='sub' value=''>
<input type=hidden name=exl value='0'>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
<!--	<TR><!-- Topnav Image -->
      <!--  <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR> -->
     <tr><td width="780"><?include("includes/header.php");?></td></tr>
    <TR>

    </TR>

	<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>
	</tr>

	<TR>
    	<TD background=images/tile.jpg vAlign=top width=780 align=center>

        	<table width=780 cellpadding=0 cellspacing=5 border=0 >
        		<tr>
					<td colspan=2 align="center"><b><font face="Verdana, Arial, Helvetica, sans-serif" size=2>Center change Report</b></font><br><br></td>
            	</tr>

            	<TR>
        			<TD  width=780>&nbsp;</TD>
    			</TR>

    			<tr>
                	<td align="center" class="greybluetext10"colspan=0>Exam :
	        			<select name='sExam_code' style={width:250px} class=greybluetext10 onchange='javascript:getsubjects()'>
        	        		<option value='0'>--Select--</option>
						<?php
							if (mysql_num_rows($resExam) > 0){
			        		while (list($ecode,$ename) = mysql_fetch_row($resExam)){
			                	print("<option value='$ecode' ");
			                		if ($ecode == $examCode)
			                        	print(" selected ");
			                			print(">$ename</option>");
								}
							}
			?>
						</select>
                	</td>
        		</tr>

        	    <tr>
            	    <td align="center" class="greybluetext10" colspan=4>&nbsp;&nbsp;&nbsp;&nbsp;Subject :
	        			<select name='sSub_code' style={width:300px} class=greybluetext10>
        	        		<option value='0'>--Select--</option>
						<?php
								if (mysql_num_rows($resSub) > 0){
			        			while (list($sCode,$Sname) = mysql_fetch_row($resSub)){
			                		print("<option value='$sCode' ");
			                	if ($sCode == $subjectCode)
			                        print(" selected ");
			                		print(">$Sname</option>");
								}
							}
						?>
						</select>
                </td>
       	   </tr>

        </table>

        <table width=780>
			<tr>
				<td colspan=3 align="center"><input type=button name='sub_assign' value='Submit' class='button' onClick='javascript:GetValues()'></td>
			</tr>
           <!-- <tr>
            	<td align="center" width=100% class="greybluetext10" valign=top colspan="2">   -->
            	<?

/*** It checks with the flag value and it retrieving the data from the array and displaying the report in the
     html page  **/


			if ($submitted=='true' && $num < 1){
	?>
			<tr>
				<td colspan=3 align="center" class=alertmsg><b>No Records Found</b></td>
			</tr>

	<?   }      ?>

		</table>
<?
       $headerlength=count($exce_header);
	if ($submitted=='true' && $num >0)
		{
	print("<table border=1 cellspacing=0 cellpadding=2><tr>");
		for($a=0;$a<$headerlength;$a++)
		{
			print("<td class=greybluetext10><b>$exce_header[$a]</b></td>");
		}
		print("</tr>");
        print("<tr>");

/** Displaying the output in the table format **/


				for($j=0;$j<$num;$j++)
             	{
	          		for($k=0;$k<$headerlength;$k++)
	           		{
		         ?><td class=greybluetext10><?=$exce_content[$j][$k]?></td>
		          <?
	           		}
	      print("</tr>");
             	}
		   ?>
	</table>
    <table>
	    <tr><td class=greybluetext10>&nbsp;</td></tr>
	    <tr class=greybluetext10><td align="center" class=greybluetext10><input class='button' type='button' name='download' value='Download' onClick='javascript:excelsubmit()'>
	    	</td>
	    </tr>
	    <tr><td class=greybluetext10>&nbsp;</td></tr>
	    </table>
	    <? }  ?>

     </TD>
  </TR>
  <TR>
   	<?include("includes/footer.php");?>
  </TR>

</TABLE>
</form>
</center>
</body>
</html>