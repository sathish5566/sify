<?php
$admin_flag=1;
require("sessionchk.php");
require("constants.inc");
?>
<html>
<head>
    <title><?=PAGE_TITLE ?></title>
</head>
<link rel="stylesheet" href="images/iibf.css" type="text/css">
<body leftmargin="0" topmargin="0">
<center>
<form name=frmgetsubjects method=post>
  <table width="780" border="0" cellspacing="0" cellpadding="0" >
    <tr>
      <td width="780"><img src="images/logo1.jpg" width="136" height="59"><img src="images/logo2.gif" width="284" height="59"><img src="images/logo3.gif" width="360" height="59"></td>
    </tr>
    <tr>
      
    </tr>
    <tr>
		<Td background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
  </tr>
    <tr>
      <td width="780" background="images/tile.jpg" valign="top">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="24"> <div align="center" ><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Admin
                module - Offline Question Paper(s) Generation</font> </strong></div></td>
          </tr>
          <tr>
            <td height="200" valign="middle" align="center">
<?php
    //print_r($_POST);
    set_time_limit(0);
    $examCode = $_POST['exam_code'];
    $mediumCode = $_POST['medium_code'];
    $subjectCode = $_POST['subject_code'];
    $nPapers = $_POST['total_question_paper'];
    $totalMarks = $_POST['total_marks'];
    $sectionCount = $_POST['section_count'];
    $isSample = $_POST['chk_sample'];
    if ($isSample == "")
    {
        $isSample = 'N';
    }
    $aMarks = array();
    for ($j = 0; $j < $nStoredMarks; $j++)
    {
        for ($i = 0; $i < $sectionCount; $i++)
        {
            $var = "question_count".$i.$j;
            //print $var;
            $aMarks[$j] == "" ? $aMarks[$j] = $j.":".$_POST["$var"] : $aMarks[$j] = $aMarks[$j].",".$_POST["$var"];
        }
    }
//    print_r($aMarks);
    if (count($aMarks) > 0)
    {
        $strMarks = implode($aMarks,"-");
        //print $strMarks;
        $strAttributesList = "$examCode $subjectCode $nPapers $totalMarks $sectionCount $strMarks $isSample";
        //print("/usr/bin/php offline_generate.php $examCode $subjectCode $nPapers $totalMarks $sectionCount $strMarks $isSample &");
        //exec("/usr/local/bin/php offline_generate.php $examCode $subjectCode $nPapers $totalMarks $sectionCount $strMarks $isSample");
        require("generate_offline.php");
        print("<div class='alertmsg'><center>Question Paper(s) Generated<br><!--<a href='get_subject.php?exam_code=$examCode'>Select a different subject</a>&nbsp;&nbsp;<a href='get_sections.php?exam_code=$examCode&subject_code=$subjectCode'>Select different weightages</a>--></center></div>");
    }
    else
    {
        print("<div class='alertmsg'><center>Please Contact the System Administrator</center></div>");
//        return;
    }
?>
           </td>
          </tr>
        </table>
        </td>
    </tr>
    <tr>
      <? include("includes/footer.php");?>
    </tr>
  </table>
</form>
</center>
</body>
</html>