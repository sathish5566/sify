<?php
error_reporting(0);
/*
Filename : dbconfig.php

purpose : store all the variables related to mysql DB (iib_online_exam) 
		  connections and also the access for that DB.
*/

//variables needed for the db connections
DEFINE (PAGE_TITLE,"Sify Online Exam");

$host="221.135.110.217";
$username="onlineexamuser";
$password="onlineexampwd";
$db="onlineexam_generic";
//this is used for the connection establishment with the mysql server
$connect=@mysql_pconnect($host,$username,$password) or die("Mysql Server may not be running . Please Contact the System administrator for the help. ");

//this is done to select the database for use.
$select_db=@mysql_select_db($db) or die("Database iib_online_exam may not exists. Please Contact the System administrator for the help.");
?>
