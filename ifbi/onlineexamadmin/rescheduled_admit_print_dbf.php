<?php
set_time_limit(0);
ob_start();
$admin_flag = 0;
//require_once("sessionchk.php");
require_once("constants.inc");
require_once("dbconfig.php");

class TEXT_Table
{
var $widths;
var $page_height=41;
var $page_width=60;
var $current_height=0;
var $current_width=0;
var $new_page_flag=0;
var $print_page = array();
var $current_page_line = 1;
function TEXT_Table(){
	$page_height=41;
	$page_width=60;
	$current_width=0;
	$current_height=0;
	$new_page_flag=0;
}

function SetWidths($w)
{
    //Set the array of column widths
    $this->widths=$w;
}

function total_length(){
	$text_value = implode ($this->print_page);
	return strlen($text_value);
}

function Row($data,$char_val,$align)
{
    $this->CheckPageBreak($h);
    $max_height=0;
    $max_data =count($data);
    $max_lines = $this->max_lines($data);
    unset($text_array);
    unset($temp_array);
    for($i=0;$i<$max_data;$i++){
	$text_value = wordwrap($data[$i],$this->widths[$i],'\-/');
        $text_array[$i] = explode('\-/',$text_value);
	for ($j=0;$j<$max_lines;$j++){
		$temp= '';
		if ($i==0)
			$text_array[$i][$j]=ltrim($char_val).$text_array[$i][$j];
		$text_len = (array_key_exists($j,$text_array[$i]))?strlen($text_array[$i][$j]):0;
		$str_len_to_pad = $this->widths[$i] - $text_len;
		$temp=str_pad($temp,$str_len_to_pad,' ');  //,STR_PAD_BOTH);
       		if ($char_val != '')
			$temp .= $char_val;
               	$text_array[$i][$j] .= $temp;
	        if ($max_data - 1 == $i)
        	        $text_array[$i][$j] .= "\r\n";
	}
    }
    for($i=0;$i<$max_lines;$i++){
	$text_to_print='';
	for($j=0;$j<$max_data;$j++){
		$text_to_print .= $text_array[$j][$i];
	}
	if ($this->current_page_line > $this->page_height){
                $this->end_page();
                $this->add_page();
        }
	//$this->print_page[$this->current_height++]="- $this->current_page_line - ".$text_to_print;
	$this->print_page[$this->current_height++]=$text_to_print;
	$this->current_page_line++;
    }
}

function max_lines($data){
	$max_lines = 0;
    for($i=0;$i<count($data);$i++){
 		$text_value = wordwrap($data[$i],$this->widths[$i],'\-/');
	    $text_array = explode('\-/',$text_value);
	    if ($max_lines < count($text_array))
			$max_lines = count($text_array);
	}
return $max_lines;
}

function put_line(){
        if ($this->current_page_line > $this->page_height){
                $this->end_page();
                $this->add_page();
        }
	$text_to_print = '';
	$print_length = array_sum($this->widths)+(count($this->widths) * 3)-1;
	for ($i=0;$i < $print_length; $i++)
		$text_to_print .= '-';
	//$this->print_page[$this->current_height]=$text_to_print."- $this->current_page_line - \r\n";
	$this->print_page[$this->current_height++]=$text_to_print." \r\n";
	$this->current_page_line++;
}
function show_values(){
	$result_value = implode ($this->print_page);
	echo $result_value;
}

function CheckPageBreak($h)
{
	if ($this->current_page_height > $this->page_height){

	}
}
function PutLineBreak(){
        if ($this->current_page_line > $this->page_height){
                $this->end_page();
                $this->add_page();
        }
	//$this->print_page[$this->current_height++] = "- $this->current_page_line \r\n";
	$this->print_page[$this->current_height++] = "\r\n";
	$this->current_page_line++;
}

function Footer(){
 $this->print_page[$this->current_height++]= chr(12);
  for($i=0;$i<13;$i++){
	 // $this->print_page[$this->current_height++]= "\r\n";
	  $this->print_page[$this->current_height++]= "-F $i - \r\n";
}
}
function Header(){
  for($i=0;$i<12;$i++){
	 // $this->print_page[$this->current_height++]= "\r\n";
	  $this->print_page[$this->current_height++]= "-H $i -  \r\n";
	}
}
function end_page(){
// $this->print_page[$this->current_height++]= chr(12);
/*	for ($i=$this->current_page_line;$i <= $this->page_height; $i++){
		//$this->print_page[$this->current_height++]= "\r\n";
		$this->print_page[$this->current_height++]= "- Excess - $i - \r\n";
	}
	$this->Footer();
	$this->current_page_line=1; */
}
function add_page(){
//	$this->Header();
}
}
/*
$memno = array(
100061690,
400091113
);*/
/*$memno = array(
66231,
66232,
66233,
66234,
66235,
66236,
66237
);*/


$offset = $_REQUEST["hOffset"];
$pagecount = $_REQUEST["hPageCount"];
$page = $_REQUEST["hPage"];
$each_link = $_REQUEST["hEachLink"];
$link_per_page = $_REQUEST["hLinkPerPage"];
$index = $_REQUEST["hIndex"];
$diff = $_REQUEST["hDiff"];
$centre_code = $_REQUEST["sCentreCode"];
$sExamCode=isset($_REQUEST["sExamCode"])?$_REQUEST["sExamCode"]:"";
$rec_per_page=$each_link*$link_per_page;
$displacement = $offset + ($index*$each_link);
//$sExamTime='08:30:00';
$examDate = $_REQUEST["hexamDate"];
if ($sExamCode== 0)
        $sql_iway_can = "select distinct(membership_no) from iib_candidate_iway  a, iib_iway_details b where b.exam_centre_code = '$centre_code'  and a.centre_code = b.centre_code and exam_date='$examDate' order by membership_no limit $displacement,$diff";
elseif ($sExamCode != "")
        $sql_iway_can = "select distinct(a.membership_no) from iib_candidate_iway a, iib_iway_details b where a.exam_code='$sExamCode' and b.exam_centre_code = '$centre_code'  and a.centre_code = b.centre_code and exam_date='$examDate' order by membership_no limit $displacement,$diff";


//echo $sql_iway_can;

$res_iway_can=mysql_query($sql_iway_can);
$num_can = @mysql_num_rows($res_iway_can);
if ($num_can < 1){
 echo "$sql_iway_can";
	 exit;
}
$j=0;
while ($j < $num_can){
list($memno[$j])= mysql_fetch_row($res_iway_can);
$j++;
}
$iway_array=array();
$add=1;
$num_can = count($memno);
$st_memno = $memno[0];
$end_memno = $memno[$num_can-1];
$memlist = implode('\',\'',$memno);
$sql_can = "select membership_no,name,password,address1,address2,address3,address4,address5,address6,pin_code from iib_candidate where membership_no in ('$memlist')";
$res_can = mysql_query($sql_can);
$txt=new TEXT_Table();
//$txt->Open();
$strFileName = 'Admit_'.$st_memno.'_'.$end_memno.'.txt'; 
$i=0;
while (count($memno) > $i ){
	list($c_no,$c_name,$c_passwd,$c_addr1,$c_addr2,$c_addr3,$c_addr4,$c_addr5,$c_addr6,$c_pin)=mysql_fetch_row($res_can);
	$caddress6 = "";
	if ($c_addr6 != "")
		$caddress6 .= $c_addr6." ".$c_pin;	
	else
		$caddress6 .= $c_pin;
	//$txt->add_page();
	$txt->PutLineBreak();
	/*$txt->setWidths(array(75));
	$txt->Row(array(chr(12)),'',''); */

	$sqlExams = "SELECT exam_name FROM iib_exam WHERE exam_code=$sExamCode";
	$resExams = mysql_query($sqlExams);
	list($eName)= mysql_fetch_row($resExams);

	
	$today = date('d/m/Y');
	$txt->SetWidths(array(60,15));
	$txt->Row(array("IIBF/J05/85/730/05","".$today),'','L');
	$txt->PutLineBreak();
	$txt->SetWidths(array(30,5,40));
	if ($eName == "DBF Examination")
		$txt->Row(array("Registration No",":",$c_no),'','L');
	else
		$txt->Row(array("Membership No",":",$c_no),'','L');
		
	//$txt->PutLineBreak();
	$txt->SetWidths(array(75));
	$txt->Row(array($c_name),'','C');
	if ($c_addr1 != "")
	{
		$txt->SetWidths(array(75));
		$txt->Row(array($c_addr1),'','L');
	}
	if ($c_addr2 != "")
	{
		$txt->SetWidths(array(75));
		$txt->Row(array($c_addr2),'','L');
	}
	if ($c_addr3 != "")
	{
		$txt->SetWidths(array(75));
		$txt->Row(array($c_addr3),'','L');
	}
	if ($c_addr4 != "")
	{
		$txt->SetWidths(array(75));
		$txt->Row(array($c_addr4),'','L');
	}
	if ($c_addr5 != "")
	{
		$txt->SetWidths(array(75));
		$txt->Row(array($c_addr5),'','L');
	}
	if ($caddress6 != "")
	{
		$txt->SetWidths(array(75));
		$txt->Row(array($caddress6),'','L');
	}
	$txt->PutLineBreak();
	$txt->PutLineBreak();
	
	
	$sqlSubjectCode = "SELECT subject_code FROM iib_candidate_iway WHERE membership_no = '$c_no' and exam_date='$examDate'" ;
	$resSubjectCode = mysql_query($sqlSubjectCode);
	list($eSubjectCode)= mysql_fetch_row($resSubjectCode);
	
	$sqlSubjects = "SELECT subject_name FROM iib_exam_subjects WHERE subject_code = $eSubjectCode" ;
	$resSubjects = mysql_query($sqlSubjects);
	list($eSubject)= mysql_fetch_row($resSubjects);
	
	$txt->SetWidths(array(75));
	$txt->Row(array("Dear Candidate,"),'','L');
	$txt->PutLineBreak();
	$txt->SetWidths(array(5,70));
	$txt->Row(array("Re:  ","".$eName),'','L');
	
	$txt->SetWidths(array(5,70));
	$txt->Row(array("","Date: 7th August 2005 (originally scheduled for 3rd July 2005),"),'','L');
	$txt->SetWidths(array(5,70));
	$txt->Row(array("","Centre: Patna"),'','L');
	$txt->SetWidths(array(5,70));
	$txt->Row(array("","Subject:Indian Financial System & Commercial Banking / Business Communication & Customer Relations"),'','L');
	
	$txt->PutLineBreak();
	$txt->SetWidths(array(80));
	$txt->Row(array("Due to technical problems, the Institute could not conduct the DBF on line examination in the above subject on 7th August 2005 at Patna."),'','L');
	
	$txt->PutLineBreak();
	$txt->SetWidths(array(80));
	$txt->Row(array("It has been decided to conduct the said examination on 21st August 2005 in the same I-Way at the same time. The user name and passwords will also remain the same."),'','L');
	
	$txt->PutLineBreak();
	$txt->SetWidths(array(80));
	$txt->Row(array("We request you to appear at the said examination on 21st August 2005. You may carry a copy of this letter along with the admit card and membership id card while appearing at the examination."),'','L');
	
	$txt->PutLineBreak();
	$txt->SetWidths(array(80));
	$txt->Row(array("We sincerely regret for the inconvenience caused to you in this regard."),'','L');
	
	$txt->PutLineBreak();
	$txt->SetWidths(array(80));
	$txt->Row(array("Yours faithfully,"),'','L');
	$txt->PutLineBreak();
	$txt->PutLineBreak();
	$txt->PutLineBreak();
	$txt->PutLineBreak();
	$txt->SetWidths(array(80));
	$txt->Row(array("DIRECTOR OF EXAMINATIONS"),'','L');

	
	$txt->PutLineBreak();
	$txt->SetWidths(array(70));
    $txt->Row(array("Since this is a computer generated letter, no signature is required."),'','');
    $txt->PutLineBreak();
	
    $i++;
	/*$txt->SetWidths(array(30,5,40));
	$txt->Row(array("Password",":",$c_passwd),'','C');	*/
	$txt->SetWidths(array(75));
	$txt->Row(array(chr(12)),'','');
}	
       if(isset($HTTP_ENV_VARS['HTTP_USER_AGENT']) and strpos($HTTP_ENV_VARS['HTTP_USER_AGENT'],'MSIE 5.5'))
	        Header('Content-Type: application/dummy');
        else
       		 Header('Content-Type: application/dummy');
   $text_length = $txt->total_length(); 
    Header('Content-Length:'.$text_length+10000);
        Header('Content-disposition: attachment; filename='.$strFileName);
/*for($i=0;$i<100;$i++){
	echo "$i \r\n";
} */
$txt->show_values();
ob_end_flush(); 
?>
