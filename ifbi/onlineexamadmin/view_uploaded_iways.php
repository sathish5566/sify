<?php
/**
* Application Name            :  IIB BULKUPLOAD
* Module Name                 :  View uploaded files
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  excelfiles
* Tables used for only selects:  excelfiles
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  Koshy.K.I.
* Created ON                  :  18/03/2006.
* Last Modified By            :  B.Devi
* Last Modified Date          :  24-03-2006
* Description                 :  View All Excel Files after Uploading 
*****************************************************/
require_once("includes/settings.php"); 
/*key is got when clicking the url to know the type of upload : This is hardcoded in sify_admin_menu.php*/


$source_key	= isSet($_POST["key"]) ? $_POST["key"] : "";
if($source_key=="")
{	
	$source_key	= isSet($_GET["key"]) ? $_GET["key"] : "";
}
if($source_key=="")
{
	header("Location: errorpage.php?status=13");			
}
	

	require_once("sessionchk.php");
	require_once("dbconfig.php");
	require_once("includes/iway_functions.php");
	require_once("includes/functions.php");
	require_once("includes/settings.php");

/** Common Debug Variable for debugging the code which prints the data */
$commonDebug = false;

/** Deletion of the file if post happens */
if($_POST){
	foreach($_POST as $key=>$value){
	
		$sql_file = "select file_name from ".TABLE_EXCEL_FILES." where file_id=$key";
		if ($commonDebug){
			echo $sql_file;
		}
		$upload_user = $_SESSION["admin_id"];
		$result_file = @mysql_query($sql_file);		
		while (list ($file_name,$exam_date)=mysql_fetch_row($result_file)){
			if (file_exists($filenamepath.$file_name)){
				if(unlink($filenamepath.$file_name)){
					$errormsg .= $file_name . " Deleted Successfully! <br>";
					$sql_file_delete = "update ".TABLE_EXCEL_FILES." set is_active='".INACTIVE_FLAG."',deleted_by='$upload_user',deleted_on=NOW() where file_id=$key";
					@mysql_query($sql_file_delete);
				}// end of if(unlink)
			}// end of if(file_exists)
		}// end of while			
	}//end of foreach
}//end ($_POST)
/**
*		fetching the uploaded file from the table
*		Table used: excelfiles
*/

$sql_file = "select file_id,file_name,exam_date,file_type,file_size,language,question_sid,question_eid,is_imported,is_active,uploaded_on,uploaded_by,deleted_by,deleted_on,imported_on,imported_by from ".TABLE_EXCEL_FILES." where source_key = '$source_key'";
$sql_file .= " order by uploaded_on desc";

if ($commonDebug){
	echo $sql_file;
}
$result_file = @mysql_query($sql_file);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
.textbox        
{ 
font-family: Verdana, Arial, Helvetica, sans-serif; 
font-size: 10px; font-style: normal; 
text-decoration: none; 
height: 17px; 
border: 1px #000000 solid
}
</STYLE>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/validations.js"></script>
</head>

<BODY leftMargin=0 topMargin=0 LeftMargin=0 marginheight=0 marginwidth=0>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780 align=center>
<!--	<TR><!-- Topnav Image -->
      <!--  <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR> -->
	<tr><td width="780"><?include("includes/header.php");?></td></tr>
    <TR>
       
    </TR>
	<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>
    <TR>
    	<TD background=images/tile.jpg vAlign=top width=780 align=center>
    	
<table cellspacing="0" cellpadding="0" width="100%" border="0" align="center" >
  <tr>
    <td width="5%" valign="top"></td>
    <td width="90%">
      <table width="95%"  border="0" align="center" cellpadding="5" cellspacing="1">
  <tr>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="95%" align="left" cellpadding="2" >
        <tr >
          <td colspan="2">
            <p class="pageTitle">View Uploaded Files </p></td>
        </tr>
        </table>      </td>
    </tr>
  <tr>
    <td valign="top">		
    <table width="100%"  border="0" cellspacing="1" cellpadding="1">
		<form name="listfile" method="post">
      <tr>
        <td colspan="2">
					<table width="95%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#BEBEBE">
          	<tr align="center" bgcolor="#F9F9F9">
							<td align="center">&nbsp;</td>
            	<td align="center">File Name</td>
							<td align="center">File Type</td>
							<td align="center">File Size</td>
							<td align="center">Exam Date</td>
							<td align="center">Imported to DB</td>
							<td align="center">Uploaded On</td>
							<td align="center">Uploaded By</td>
							<td align="center">Deleted On</td>
							<td align="center">Deleted By</td>
							<td align="center">Imported On</td>
							<td align="center">Imported By</td>
          	</tr>
						<?php
						while(list($file_id,$file_name,$exam_date,$file_type,$file_size,$language,$question_sid,$question_eid,$is_imported,$is_active,$uploaded_on,$uploaded_by,$deleted_by,$deleted_on,$imported_on,$imported_by) = mysql_fetch_row($result_file)){
								$img = '<img src="images/imgMSExcelLogo.gif" width="30" height="32">'; 
								if ($is_active == INACTIVE_FLAG){
									$link = $file_name;
									$checkbox = "<img src='images/trash.png'>";
								}else{
									
									//$link = '<a href="viewfile.php?fileid='.$file_id.'">'.$file_name.'</a>';
									$link = '<a href="'.$viewfile_redirect[$source_key].'?fileid='.$file_id.'">'.$file_name.'</a>';
									$checkbox='<input type="checkbox" name="'.$file_id.'">';	
								}
								if ($is_imported == INACTIVE_FLAG){
									$is_imported = "<img src='images/server1.gif'>";										
								}else if ($is_imported == ACTIVE_FLAG){
									//$is_imported = "<a href='viewdb.php?fileid=".$file_id."&sid=".$question_sid."&eid=".$question_eid."'><img src='images/database.gif' border='0'></a>";
									//$link = "<a href='viewdb.php?fileid=".$file_id."&sid=".$question_sid."&eid=".$question_eid."'>$file_name</a>";									
									$is_imported = "<a href='".$viewdb_redirect[$source_key]."?fileid=".$file_id."&sid=".$question_sid."&eid=".$question_eid."'><img src='images/database.gif' border='0'></a>";
									$link = "<a href='".$viewdb_redirect[$source_key]."?fileid=".$file_id."&sid=".$question_sid."&eid=".$question_eid."'>$file_name</a>";									
								}

								if ($language == "E"){
									$language = "English";
								}elseif ($language== "H"){
									$language = '<img src="images/hindi.gif" align="absmiddle">';;
								}else{
									$language = "-";	
								}
								
						$exam_date		= explode(" ",$exam_date);//Exam Date selected in Combo
							?>
						<tr align="center" bgcolor="#FFFFFF">
            	<td align="center"><?php echo $checkbox; ?></td>
							<td align="center"><?php echo $link; ?></td>
							<td align="center"><?php echo $img; ?></td>
							<td align="center"><?php echo filesizeinwords($file_size); ?></td>
							<td align="center"><?php echo $exam_date[0]; ?></td>
							<td align="center"><?php echo $is_imported; ?></td>
							<td align="center"><?php echo $uploaded_on; ?></td>
							<td align="center"><?php echo $uploaded_by; ?></td>
							<td align="center"><?php echo $deleted_on; ?></td>
							<td align="center"><?php echo $deleted_by; ?></td>
							<td align="center"><?php echo $imported_on; ?></td>
							<td align="center"><?php echo $imported_by; ?></td>
          	</tr>
						<?php
						}//end of while						
						?>
       		</table>
				</td>
      </tr>
			<tr align="center">
				<td align="left">
					<a href="javascript:clearall_click(0);" onClick="javascript:clearall_click(0);return false;"><img src="images/clearall.gif" border="0"></a> 
					<a href="javascript:checkall_click(0);" onClick="javascript:checkall_click(0);return false;"><img src="images/checkall.gif" border="0"></a> 
					<a href="javascript:delete_click(0);" onclick="javascript:delete_click(0);return false;"><img src="images/delete.gif" border="0"></a>
				</td>
				<td>
					<img src="images/trash.png" align="absmiddle"> Deleted
					<img src="images/server1.gif" align="absmiddle"> In Server
					<img src="images/database.gif" align="absmiddle"> In Database
				</td>
      </tr>
			</form>
    </table></td>
  </tr>
</table>

</td>
 <td width="5%" valign="top"></td>
  </tr>
  <TR>
    	<?include("includes/footer.php");?>
    </TR>
</table>
        </TD>
	</TR>
    
</TABLE>
</body>
</html>
