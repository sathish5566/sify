<?php
$admin_flag=2;
require("dbconfig_slave_123.php");
require("sessionchk.php");
set_time_limit(0);

$submitted= isset($_POST['submitted']) ? $_POST['submitted'] : 'false';
$ex_flag=isset($_POST['exl']) ? $_POST['exl'] : '0' ;
$examDate = $_POST['exam_date'];
$sqlDate = "SELECT distinct exam_date FROM iib_exam_schedule order by exam_date ";
$resDate = mysql_query($sqlDate);



$res_content='';
if ($examDate != "")
{
	$login_process_Y_Cnt=0;
	$login_process_N_Cnt=0;
	$system_work_Y_Cnt=0;
	$system_work_N_Cnt=0;
	$talogin_issue_Y_Cnt=0;
	$talogin_issue_N_Cnt=0;
	$tech_prob_Y_Cnt=0;
	$tech_prob_N_Cnt=0;
	$adeq_time_Y_Cnt=0;
	$adeq_time_N_Cnt=0;
	$navigate_issue_Y_Cnt=0;
	$navigate_issue_N_Cnt=0;

	$q_rating_easy=0;
	$q_rating_difficult=0;
	$q_rating_relevant=0;
	$q_rating_notrelevant=0;
	$q_rating_cantsay=0;

	$rating_excellent=0;
	$rating_vgood=0;
	$rating_good=0;
	$rating_average=0;
	$rating_poor=0;


	$aDates = explode("-", $examDate);
	$dispDate = $aDates[2]."-".$aDates[1]."-".$aDates[0];
	$sql = " SELECT a.login_process, a.system_work, a.talogin_issue, a.tech_prob, a.q_rating, a.adeq_time, a.navigate_issue, a.rating FROM iib_feedback a,iib_candidate_iway b WHERE b.exam_date = '$examDate' and a.membership_no=b.membership_no and a.exam_code=b.exam_code and a.subject_code=b.subject_code";
	$res = mysql_query($sql);
	$nRows = mysql_num_rows($res);
	if ($nRows > 0)
	{		
		$res_content .= "<table width=100% border=0 cellspacing=1 cellpadding=1>";
		$res_content .= "<tr><td>";
		$res_content .= "<table width=100% border=1 cellspacing=1 cellpadding=1>";
		$res_content .= "<tr><td width=50% class=textblk11><b>Feedback Option</b></td><td width=25% class=textblk11><b>Yes</b></td><td width=25% class=textblk11><b>No</b></td></tr>";
		while (list($login_process, $system_work, $talogin_issue, $tech_prob, $q_rating, $adeq_time, $navigate_issue, $rating) = mysql_fetch_row($res))
		{
		
			if($login_process=='Y')	$login_process_Y_Cnt++;	elseif($login_process=='N')	$login_process_N_Cnt++;
			if($system_work=='Y')	$system_work_Y_Cnt++;	elseif($system_work=='N')	$system_work_N_Cnt++;
			if($talogin_issue=='Y')	$talogin_issue_Y_Cnt++;	elseif($talogin_issue=='N')	$talogin_issue_N_Cnt++;
			if($tech_prob=='Y')		$tech_prob_Y_Cnt++;		elseif($tech_prob=='N')		$tech_prob_N_Cnt++;
			if($adeq_time=='Y')		$adeq_time_Y_Cnt++;		elseif($adeq_time=='N')		$adeq_time_N_Cnt++;
			if($navigate_issue=='Y')$navigate_issue_Y_Cnt++;elseif($navigate_issue=='N')$navigate_issue_N_Cnt++;

			if($q_rating=='easy')
				$q_rating_easy++;
			elseif($q_rating=='difficult')
				$q_rating_difficult++;
			elseif($q_rating=='relevant')
				$q_rating_relevant++;
			elseif($q_rating=='not relevant')
				$q_rating_notrelevant++;
			elseif($q_rating=='cant say')
				$q_rating_cantsay++;

			if($rating=='excellent')
				$rating_excellent++;
			elseif($rating=='very good')
				$rating_vgood++;
			elseif($rating=='good')
				$rating_good++;
			elseif($rating=='average')
				$rating_average++;
			elseif($rating=='poor')
				$rating_poor++;

		}		
			$login_process_Y_Per	=	number_format( ($login_process_Y_Cnt/$nRows)*100,2,'.','' );
			$login_process_N_Per	=	number_format( ($login_process_N_Cnt/$nRows)*100,2,'.','' );

			$system_work_Y_Per		=	number_format( ($system_work_Y_Cnt/$nRows)*100,2,'.','' );
			$system_work_N_Per		=	number_format( ($system_work_N_Cnt/$nRows)*100,2,'.','' );

			$talogin_issue_Y_Per	=	number_format( ($talogin_issue_Y_Cnt/$nRows)*100,2,'.','' );
			$talogin_issue_N_Per	=	number_format( ($talogin_issue_N_Cnt/$nRows)*100,2,'.','' );

			$tech_prob_Y_Per		=	number_format( ($tech_prob_Y_Cnt/$nRows)*100,2,'.','' );
			$tech_prob_N_Per		=	number_format( ($tech_prob_N_Cnt/$nRows)*100,2,'.','' );

			$adeq_time_Y_Per		=	number_format( ($adeq_time_Y_Cnt/$nRows)*100,2,'.','' );
			$adeq_time_N_Per		=	number_format( ($adeq_time_N_Cnt/$nRows)*100,2,'.','' );

			$navigate_issue_Y_Per	=	number_format( ($navigate_issue_Y_Cnt/$nRows)*100,2,'.','' );
			$navigate_issue_N_Per	=	number_format( ($navigate_issue_N_Cnt/$nRows)*100,2,'.','' );


			$res_content .= "<tr>";		
			$res_content .= "<td class=textblk11>Login Process was smooth</td>";
			$res_content .= "<td class=textblk11>$login_process_Y_Per %</td>";
			$res_content .= "<td class=textblk11>$login_process_N_Per %</td>";
			$res_content .= "</tr>";	

			$res_content .= "<tr>";		
			$res_content .= "<td class=textblk11>System was working fine during the exam</td>";
			$res_content .= "<td class=textblk11>$system_work_Y_Per %</td>";
			$res_content .= "<td class=textblk11>$system_work_N_Per %</td>";
			$res_content .= "</tr>";

			$res_content .= "<tr>";		
			$res_content .= "<td class=textblk11>Did you face any TA login issue</td>";
			$res_content .= "<td class=textblk11>$talogin_issue_Y_Per %</td>";
			$res_content .= "<td class=textblk11>$talogin_issue_N_Per %</td>";
			$res_content .= "</tr>";

			$res_content .= "<tr>";		
			$res_content .= "<td class=textblk11>Did you face any technical problem</td>";
			$res_content .= "<td class=textblk11>$tech_prob_Y_Per %</td>";
			$res_content .= "<td class=textblk11>$tech_prob_N_Per %</td>";
			$res_content .= "</tr>";

			$res_content .= "<tr>";		
			$res_content .= "<td class=textblk11>Did you have adequate time to complete the questions</td>";
			$res_content .= "<td class=textblk11>$adeq_time_Y_Per %</td>";
			$res_content .= "<td class=textblk11>$adeq_time_N_Per %</td>";
			$res_content .= "</tr>";

			$res_content .= "<tr>";		
			$res_content .= "<td class=textblk11>Did you face any issues while navigating the screen to the next question</td>";
			$res_content .= "<td class=textblk11>$navigate_issue_Y_Per %</td>";
			$res_content .= "<td class=textblk11>$navigate_issue_N_Per %</td>";
			$res_content .= "</tr>";

			$res_content .= "</table></td></tr>";

			$res_content .= "<tr><td height=10px></td></tr>";


		//QUESTION RATING 
		$q_rating_easy_per			=	number_format( ($q_rating_easy/$nRows)*100,2,'.','' );
		$q_rating_difficult_per		=	number_format( ($q_rating_difficult/$nRows)*100,2,'.','' );
		$q_rating_relevant_per		=	number_format( ($q_rating_relevant/$nRows)*100,2,'.','' );
		$q_rating_notrelevant_per	=	number_format( ($q_rating_notrelevant/$nRows)*100,2,'.','' );
		$q_rating_cantsay_per		=	number_format( ($q_rating_cantsay/$nRows)*100,2,'.','' );

		$rating_excellent_per		=	number_format( ($rating_excellent/$nRows)*100,2,'.','' );
		$rating_vgood_per			=	number_format( ($rating_vgood/$nRows)*100,2,'.','' );
		$rating_good_per			=	number_format( ($rating_good/$nRows)*100,2,'.','' );
		$rating_average_per			=	number_format( ($rating_average/$nRows)*100,2,'.','' );
		$rating_poor_per			=	number_format( ($rating_poor/$nRows)*100,2,'.','' );

			$res_content .= "<tr><td>";
			$res_content .= "<table width=100% border=1 cellspacing=1 cellpadding=1>";
			$res_content .= "<tr><td width=25% class=textblk11><b>Feedback Option</b></td><td width=15% class=textblk11><b>Easy</b></td><td width=15% class=textblk11><b>Difficult</b></td><td width=15% class=textblk11><b>Relevant</b></td><td width=15% class=textblk11><b>Not relevant</b></td><td width=15% class=textblk11><b>Can't say</b></td></tr>";			
			$res_content .= "<tr><td width=25% class=textblk11>How will you rate the questions</td><td width=15% class=textblk11>$q_rating_easy_per %</td><td width=15% class=textblk11>$q_rating_difficult_per %</td><td width=15% class=textblk11>$q_rating_relevant_per %</td><td width=15% class=textblk11>$q_rating_notrelevant_per %</td><td width=15% class=textblk11>$q_rating_cantsay_per %</td></tr>";
			$res_content .= "</table></td></tr>";

			$res_content .= "<tr><td height=10px></td></tr>";

			$res_content .= "<tr><td>";
			$res_content .= "<table width=100% border=1 cellspacing=1 cellpadding=1>";			
			$res_content .= "<tr><td width=25% class=textblk11><b>Feedback Option</b></td><td width=15% class=textblk11><b>Excellent</b></td><td width=15% class=textblk11><b>Very Good</b></td><td width=15% class=textblk11><b>Good</b></td><td width=15% class=textblk11><b>Average</b></td><td width=15% class=textblk11><b>Poor</b></td></tr>";			
			$res_content .= "<tr><td width=25% class=textblk11>Your rating on online exam methodology</td><td width=15% class=textblk11>$rating_excellent_per %</td><td width=15% class=textblk11>$rating_vgood_per %</td><td width=15% class=textblk11>$rating_good_per %</td><td width=15% class=textblk11>$rating_average_per %</td><td width=15% class=textblk11>$rating_poor_per %</td></tr>";
			$res_content .= "</table></td></tr>";

		$res_content .= "</table><br><br>";

		if($ex_flag=='1')
        {
			$myFile='Summary_Report_'.time().'.xls';
			$fh = fopen($myFile, 'w') or die("can't open file");
			fwrite($fh, $res_content);
			fclose($fh);
			$file = file_get_contents ($myFile);
			header("Cache-Control: no cache,must-revalidate");
			header("Content-Type:application/dummy"); 
			header("Content-Disposition: attachment; filename=$myFile");
			readfile($myFile);	
			unlink($myFile);
			exit;
		}
	}
	else
	{
		$res_content .= "<br><div align=center class=textblk11>No records found</div><br><br>";
	}
}

?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
	FONT-FAMILY: Arial, Verdana	
}
.textblk11     { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; text-decoration: none}
</STYLE>
</head>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
<tr><td width="780"><?php include("includes/header.php");?></td></tr>
	<TR>
    	
	</TR>
	<tr>
		<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>	
	<TR>
	    <TD background=images/tile.jpg vAlign=top width=780 align=center>

<form name='frm' method='post'>
<input type=hidden name=exl value='0'>
<input type='hidden' name='submitted'>
<table width=100% border=0 cellspacing=0 cellpadding=5>
	<tr><td align=center><div align=center><b>Summary Report</b></div></td></tr>
	<tr>
		<td align=center class=textblk11>Exam Date : 
			<select name='exam_date' style={width:150px} class=textblk11>
				<option value=''>--Select--</option>
<?php
if (mysql_num_rows($resDate) > 0)
{
	while (list($eDate) = mysql_fetch_row($resDate))
	{
		$aDate = explode("-",$eDate);
		$dispDate = $aDate[2]."-".$aDate[1]."-".$aDate[0];
		print("<option value='$eDate' ");
		if ($eDate == $examDate)
			print(" selected ");
		print(">$dispDate</option>");
	}
}

?>
	</select>
		</td>
	</tr>
	<tr>
		<td align=center><input type='button' class=button name='sub' value='Submit' onClick='javascript:submitForm()'>
		<?PHP if($nRows > 0 ){
		echo "<input class='button' type='button' name='download' value='Download' onClick='javascript:excelsubmit()'>";
		 } ?>
		</td>
	</tr>
</table>
</form>
<script language='JavaScript'>
function submitForm()
{
	if (document.frm.exam_date.selectedIndex == 0)
	{
		alert("Please select the exam date");
		document.frm.exam_date.focus();
		return false;
	}
	document.frm.submit();
}

function excelsubmit()
{
	var frmname=document.frm;
	frmname.exl.value='1';
	document.frm.submitted.value='true';
	frmname.submit();
}
</script>
	<?php echo $res_content; ?>
	</TD>
	</TR>
	<TR>
		<?php include("includes/footer.php");?>
	</TR>
</TABLE>
</center>
</BODY>
</HTML>
