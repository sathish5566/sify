<?php
/****************************************************
* Application Name            :  IBPS
* Module Name                 :  Printing Admit card
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  iib_candidate,iib_candidate_iway,iib_exam,iib_exam_subjects
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  constants.inc 
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  Devi B
* Created ON                  :  13/04/2006.
* Last Modified By            :  B.Devi
* Last Modified Date          :  13/04/2006.
* Description                 :  Admit card generation file
*****************************************************/

header ("Content-type: application/msword"); 
set_time_limit(0);
ob_start();
//require_once("sessionchk.php");
require_once("constants.inc");
require_once ("dbconfig_slave_123.php");
$offset = $_REQUEST["hOffset"];
$pagecount = $_REQUEST["hPageCount"];
$page = $_REQUEST["hPage"];
$each_link = $_REQUEST["hEachLink"];
$link_per_page = $_REQUEST["hLinkPerPage"];
$index = $_REQUEST["hIndex"];
$diff = $_REQUEST["hDiff"];
$centre_code = $_REQUEST["sCentreCode"];	
$sExamCode=isset($_REQUEST["sExamCode"])?$_REQUEST["sExamCode"]:"";
$rec_per_page=$each_link*$link_per_page;
$displacement = $offset + ($index*$each_link);


if ($sExamCode== 0)
        $sql_iway_can = "select distinct(membership_no) from iib_candidate_iway  a, iib_iway_details b where b.exam_centre_code = '$centre_code' and a.centre_code = b.centre_code order by membership_no limit $displacement,$diff";
elseif ($sExamCode != "")
        $sql_iway_can = "select distinct(a.membership_no) from iib_candidate_iway a, iib_iway_details b where a.exam_code='$sExamCode' and b.exam_centre_code = '$centre_code' and a.centre_code = b.centre_code order by membership_no limit $displacement,$diff";

$res_iway_can=@mysql_query($sql_iway_can);
if (mysql_error()){
	echo "Error in Selection of Mapped Candidates".mysql_error();
	exit;	
}

$num_can = @mysql_num_rows($res_iway_can);
if ($num_can < 1){
	echo "No Details found!";
	header("Content-Disposition: attachment; filename=admit_nodetails.doc");
	 exit;
}
$j=0;
while ($j < $num_can){
	list($memno[$j])= @mysql_fetch_row($res_iway_can);
	$j++;
}//while($j)

$add=1;
$num_can = count($memno);
$st_memno = $memno[0];
$end_memno = $memno[$num_can-1];
$memlist = implode('\',\'',$memno);

/*Fetching the Candidates for fixing the running numbers*/

$sql_running_mem="select * "
							." from iib_candidate";
							
$res_running_mem = @mysql_query($sql_running_mem);
if (mysql_error()){
	echo "Error in Selection of Candidates".mysql_error();
	exit;	
}
$arunning_mem=array();
$r=1;
while($for_running = mysql_fetch_assoc($res_running_mem))
{
  $running_mem_no = $for_running["membership_no"];
	$r_no=str_pad($r, $running_no_length, "0", STR_PAD_LEFT);  //Length of running number is specified in constants
	$arunning_mem[$running_mem_no]=$r_no;
	$r++;
}//while($running_mem_no)

$sql_can = "select name,password,address1,address2,address3,address5,address6 from iib_candidate where membership_no in ('$memlist')";
$res_can = mysql_query($sql_can);
if (mysql_error()){
	echo "Error in Selection of Candidates".mysql_error();
	exit;	
}
$i=0;
header("Content-Disposition: attachment; filename=admit_".$st_memno."_".$end_memno.".doc");
while ($memno[$i]!=""){
	
	$c_no = $memno[$i];	
	list($c_name,$c_passwd,$c_addr1,$c_addr2,$c_addr3,$c_addr5,$c_addr6)=mysql_fetch_row($res_can);


?>
<STYLE>
<!--
	@page { size: 8.5in 11in; margin: 0.79in }
	TD P { margin-bottom: 0in }
	TH P { margin-bottom: 0in; font-style: italic }
	P { margin-bottom: 0.01in }
-->
</STYLE>
<TABLE WIDTH=115% BORDER=0  CELLPADDING=0 CELLSPACING=0>
<TR>
	<TD>
		<TABLE border=0 cellspacing=0 cellpadding=0 width=100%>
			<TR>
				<TD>
					<FONT SIZE=5 STYLE="font-size: 11pt" ALIGN=LEFT><FONT FACE="Arial, sans-serif"><b><?=$header_bankName;?></b></FONT><br>
					<FONT SIZE=5 STYLE="font-size: 10pt" ALIGN=CENTRE><FONT FACE="Arial, sans-serif"><?=$header_bankaddress1;?></FONT><br>
					<FONT SIZE=5 STYLE="font-size: 10pt" ALIGN=CENTRE><FONT FACE="Arial, sans-serif"><?=$header_bankaddress2;?></FONT><br>
					<FONT SIZE=5 STYLE="font-size: 11pt" ALIGN=CENTRE><FONT FACE="Arial, sans-serif"><b><?=$header_dept;?></b></FONT><br>
	     			<FONT SIZE=5 STYLE="font-size: 11pt" ALIGN=CENTRE><FONT FACE="Arial, sans-serif"><b><?=$header_section;?></b></FONT><br>
				</TD>
			</TR>
		</TABLE>
		</TD>
	<TD>
		<TABLE  border=0 cellspacing=0 cellpadding=0 width=100%>
			<TR>
				<TD>
					<br><br><br>
					<FONT SIZE=5 STYLE="font-size: 10pt" ALIGN=LEFT><FONT FACE="Arial, sans-serif"><?=$header_phone1;?></FONT><br>
					<FONT SIZE=5 STYLE="font-size: 10pt" ALIGN=CENTRE><FONT FACE="Arial, sans-serif"><?=$header_phone2;?></FONT><br>
					<FONT SIZE=5 STYLE="font-size: 10pt" ALIGN=CENTRE><FONT FACE="Arial, sans-serif"><?=$header_phone3;?></FONT><br><br><br>
					<FONT SIZE=5 STYLE="font-size: 10pt" ALIGN=RIGHT><?=$disp_number;?><?=$arunning_mem[$c_no];?><FONT FACE="Arial, sans-serif"></FONT><br>
				    <FONT SIZE=5 STYLE="font-size: 10pt" ALIGN=RIGHT>Date :<FONT FACE="Arial, sans-serif"><?=$letter_date;?></FONT>
				</TD>
			</TR>
		</TABLE>
	</TD>
</TR>

<tr>
	<td>
		<table border=0 cellspacing=0 cellpadding=0>
		<tr>
		  <td>
			<FONT SIZE=5 STYLE="font-size: 12pt" ALIGN=RIGHT>Shri/Smt: <?=$c_name;?></font><br>
			<FONT SIZE=5 STYLE="font-size: 12pt" ALIGN=RIGHT>Emp id:<?=$c_addr1;?></font><br>
			<FONT SIZE=5 STYLE="font-size: 12pt" ALIGN=RIGHT><?=$c_addr2;?></font><br>
			<FONT SIZE=5 STYLE="font-size: 12pt" ALIGN=RIGHT>Branch:<?=$c_addr5;?> </font><br>
			<FONT SIZE=5 STYLE="font-size: 12pt" ALIGN=RIGHT>Region: <?=$c_addr6;?></font><br>
	 	  </td>
	 	</tr>
	 	</table>
	 </td>
	<td>
		<TABLE  border=1 cellspacing=0  width =100% BORDERCOLOR="#000000" CELLPADDING=4>
		<TR>
		  <TD width=100%>
		     <FONT SIZE=5 STYLE="font-size: 12pt" ><b>User name/OnLine Test No :<?=$c_no;?></b></font><br> 
		     <FONT SIZE=5 STYLE="font-size: 12pt" ><b>Pass word:<?=$c_passwd;?> </b> </font>
		  </TD>
		 </TR>
		</TABLE>
	<TD>
<tr>
<?

/*Fetching Candidate Details*/
$sql_sel_can="select membership_no, exam_code,subject_code,UNIX_TIMESTAMP(exam_date),exam_time,centre_code from iib_candidate_iway where membership_no='$memno[$i]' order by exam_date,exam_time";
$res_sel_can=@mysql_query($sql_sel_can);
if (mysql_error()){
	echo "Error in Selection of Candidates".mysql_error();
	exit;	
}
$num_sub=@mysql_num_rows($res_sel_can);

if ($num_sub==0){
	$errmsg="No Subjects !";
}//end of if

list($membershipNo,$exam_code,$subj_code,$exam_date,$exam_time,$centre_code)=mysql_fetch_row($res_sel_can);	
$disp_exam_date=strftime("%A , %B %e , %Y ",$exam_date);	//display date is the needed format Day, month date, year


$sql_sel_exam="select exam_name from iib_exam where exam_code='$exam_code'";
$res_sel_exam=@mysql_query($sql_sel_exam);
if (mysql_error()){
	echo "Error in Selection of Exam Name".mysql_error();
	exit;	
}
list($exam_name)=@mysql_fetch_row($res_sel_exam);

$sql_sel_sub="select subject_name from iib_exam_subjects where exam_code='$exam_code' and subject_code='$subj_code'";
$res_sel_sub=@mysql_query($sql_sel_sub);
if (mysql_error()){
	echo "Error in Selection of Subject Name".mysql_error();
	exit;	
}
list($subj_name)=@mysql_fetch_row($res_sel_sub);

$sql_iway_det = "select iway_name, iway_address1, iway_address2, iway_city, iway_state, iway_pin_code from iib_iway_details where centre_code='$centre_code'";
$res_iway_det = @mysql_query($sql_iway_det); 
if (mysql_error()){
	echo "Error in Selection of Iway Details".mysql_error();
	exit;	
}
list($iway_name,$iway_address1, $iway_address2, $iway_city, $iway_state,$iway_pin_code)=mysql_fetch_row($res_iway_det);
$address = "";
if ($iway_name != "")
	$address = $iway_name.",";
if (($address != "") && ($iway_address1 != ""))
        $address .= " ".$iway_address1;
if (($address != "") && ($iway_city != ""))
        $address .= " ".$iway_city;
if (($address != "") && ($iway_state != ""))
        $address .= " ".$iway_state;
if (($address != "") && ($iway_pin_code != ""))
        $address .= " ".$iway_pin_code;
if (($address != "") && ($iway_address2 != ""))
        $address .= " , Contact No. :  ".$iway_address2;
?>
<tr>
	<td>
		<FONT SIZE=5 STYLE="font-size: 10pt" >Dear Sir/Madam,</font><br> <br>
		<FONT SIZE=5 STYLE="font-size: 10pt" >Sub:&nbsp;<?=$exam_name;?></font><br> 
		<FONT SIZE=5 STYLE="font-size: 10pt" >&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;<?=$subj_name;?></font><br> 
	</td>
<tr>

<TBODY>
</table>

<TABLE WIDTH=100% BORDER=0  CELLPADDING=0 CELLSPACING=0>
	<tr>
		<td width=100%><FONT SIZE=5 STYLE="font-size: 10pt" >You are hereby advised to appear for "OnLine Test" on <?=$disp_exam_date;?> at <?=$start_time;?> at the following Sify-i-way centre :</font><br>
	<FONT SIZE=5 STYLE="font-size: 10pt" ><?=$address;?></font><br><br>
		</td>
	</tr>
<?
$i++;//increment for the membership_number

?>	
<TR>
	<TD WIDTH=100% VALIGN=TOP>
		<FONT SIZE=5 STYLE="font-size: 12pt">Please note that:</FONT><br>
		<FONT SIZE=5 STYLE="font-size: 10pt"><b>(1)</b>	Your Username/ <b>OnLine Test No.</b> is <b><?=$c_no;?></b></font><br>
		<FONT SIZE=5 STYLE="font-size: 10pt"><b>(2)</b>	Key in the above username and password to enter into the online exam site after Sify test administrator logs in</font><br>
		<FONT SIZE=5 STYLE="font-size: 10pt"><b>(3)</b>	This number alone should be used for the above Online Test.</font><br>
		<FONT SIZE=5 STYLE="font-size: 10pt"><b>(4)</b>	You should bring this call letter with you, duly attested by the Branch Head / Department </font><br>
		<FONT SIZE=5 STYLE="font-size: 10pt"><b>(5)</b>	You should also carry your identity card issued by the Bank to the examination center (In case identity card is not readily available / issued, you should carry a certificate with your photograph and signature duly attested by the Branch Head / Department Head with his stamp).</font><br>
		<FONT SIZE=5 STYLE="font-size: 10pt"><b>(6)</b>	The reporting time is <?=$reporting_time;?> and the test would be from <?=$start_time;?>  to <?=$end_time;?><font><br>
		<FONT SIZE=5 STYLE="font-size: 10pt"><b>(7)</b>	Only outstation candidates are eligible for travelling allowances and halting allowances, as per rules.</font><br>
		<FONT SIZE=5 STYLE="font-size: 10pt"><b>(8)</b>	Information hand-out with indicative syllabus is enclosed.</font><br><br>
	</TD>
</TR>

<tr>
	<td><FONT SIZE=5 align=left STYLE="font-size: 10pt">With best wishes,</font></td>
</tr>

<tr><td>&nbsp;</td></tr>

<tr>
	<td align=right><FONT SIZE=5 align=left  STYLE="font-size: 10pt">Yours faithfully, </font></td>
</tr>

<tr><td>&nbsp;</td></tr>

<tr>
	<td align=right><FONT SIZE=5 align=left  STYLE="font-size: 10pt">Chief Manager</font></td>
</tr>

<tr>
	<td>
	<FONT SIZE=5 align=left STYLE="font-size: 10pt">--------------</font><br>
	<FONT SIZE=5 align=left STYLE="font-size: 10pt">(Member's Signature)</font><br>
	<FONT SIZE=5 align=left STYLE="font-size: 10pt">Specimen Signature No.</font><br><br><br>
	<FONT SIZE=5 align=left STYLE="font-size: 10pt">--------------</font><br>
	<FONT SIZE=5 align=left STYLE="font-size: 10pt">(Attestation signature with stamp)</font><br>
	</td>
</tr>
</TBODY>
</TABLE>
<?
}//end of while ($memno[$i]!="")
ob_end_flush();
?>

