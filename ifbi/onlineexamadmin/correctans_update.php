<?php
ob_start();//header will work inspite of the echo's used

//Code included to use this link both admin pages
session_start();
if ($_SESSION["admin_type"]== '0') {
	$admin_flag = 0;
} else if ($_SESSION["admin_type"]== '1'){
	$admin_flag = 1;
}
/**
* Application Name            :  IBPS ADMIN
* Module Name                 :  Correctanswer Update
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  iib_section_questions
* Tables used for only selects:  
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  functions.php,settings.php,errorpage.php
* Output File(s)              :
* Document/Reference Material :
* Created By	                :  Devi B
* Created ON                  :  24-04-2006
* Last Modified By            :  Devi B
* Last Modified Date          :  24-04-2006
* Description                 :  After Candidate Taking Test IBPS will update Correct Answer by excel files, which is actual 																	answers.
*****************************************************/

require_once("sessionchk.php");
require_once("dbconfig.php");
require_once("includes/settings.php"); 
require_once("includes/functions.php"); 

require_once("includes/reader.php");//required for reading excel

$commondebug = false;

$flag	= isSet($_POST["flag"]) ? $_POST["flag"] : "";
$flag_disp	= isSet($_POST["flag_disp"]) ? $_POST["flag_disp"] : "";

$error_flag=2;//if any error error_flag is set to 2


?>

<html>
<head>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<script language='javascript'>

function alert_msg()
{	
	var frm=document.frm_correctans;		
	alert('Please browse the file');
	return false;
}

function validate()
{
	var frm=document.frm_correctans;

	if (frm.excelfile.value == '')
	{
		alert('Please Select The File');
		frm.excelfile.focus();
		return;
	}

if(confirm("Do You really want to Update?"))
{
	frm.action="correctans_update.php";
	frm.flag.value="update";
	frm.submit();
}
}


</script>
<META HTTP-EQUIV="refresh" >
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<center>
<table width="780" border="0" cellspacing="0" cellpadding="0">
<!--<tr> 
	   <td width="780"><img src="images/logo1.jpg" width="136" height="59"><img src="images/logo2.gif" width="284" height="59"><img src="images/logo3.gif" width="360" height="59"></td>
</tr>-->
<tr><td width="780"><?include("includes/header.php");?></td></tr>
<tr>
 	    
</tr>
<tr>
		<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>
</tr>
<tr>
  <td width="780" background="images/tile.jpg" height="315" valign="top">
 	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
 	  <tr>
 	    <td width="150">&nbsp;</td>
 	    	<td>	
 	    	
 	    	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    	       		<tr>
    	       	    	<td>&nbsp;</td>
    	       	   	</tr>
 	    	       	<tr>
	    	       	    <form name='frm_correctans' method=post enctype="multipart/form-data">
	    	       	    <input type="hidden" name="flag" value="">
	    	       	    <td class="greybluetext10" colspan="2" height="35" bgcolor="#D1E0EF" align=center>
		       	    	<b>Upload Correct Answers</b></td>
 	    	       	 </tr>
 	    	       	 <tr>
						<td align="center" class="greybluetext10" valign=top bgcolor="#E8EFF7" colspan=2>
						<b>Upload Correct Answers <b> &nbsp; :						
						<input name="excelfile" type="file"  style={width:250px} class="textbox" onkeypress='return alert_msg()'>
						</td>
					</tr>
					<tr><td bgcolor="#E8EFF7" >&nbsp;</td></tr>
					<tr>
						<td align="center" class="greybluetext10" valign=top bgcolor="#E8EFF7">
						<input name="Submit" type="submit" class="button" value="Upload" onClick="javascript:validate();return false;">			
					</tr>
					<tr>
   	             	    <td bgcolor="#E8EFF7" colspan=2><br></td>
   	             	</tr>
   	             
		       </table>	 


<?
if($flag == "update"){
	
	$file = file_upload($_FILES,$correctans_excelpath);
	
	$afile		= explode(",",$file);	
	$file_name=$afile[0];	//seperate the filename
	$error_message =$afile[1];	//seperate the message
	if($error_message!="")		
		echo "<div id='message' style='position:relative;top:-125;' align=center class='greybluetext10'><b>$error_message</b></div>"; 	
	else{
	if($file_name!=""){
		/**
		*     Excel reader class usage starts here
		*     Instantiating the Excel reader Class
		*/
		
		
		$data = new Spreadsheet_Excel_Reader();
		
		/** setting the encoding format */
		$data->setOutputEncoding('CP1251');
		
		/** This will read from the excel file  */
		$data->read($correctans_excelpath.$file_name);
		
		$no_of_sheets = count($data->sheets);//count of sheets
		
		$sheetno=0;//$sheetno is hardcoded since only one sheet is used
		$no_of_rows = $data->sheets[$sheetno]['numRows'];
		$no_of_cols = $data->sheets[$sheetno]['numCols'];
		$cells=$data->sheets[$sheetno]['cells'];
								
		if($commonDebug){
			echo $file."<br>";
			echo "ROWS:".$no_of_rows."COLS:".$no_of_cols."<br>";
		}
		if(($no_of_cols ==0) && ($no_of_rows ==0)){
			header("Location: errorpage.php?status=11");	
			exit;
		}
		
		/*Check made to make sure the number of sheets should be 1*/
		if($no_of_sheets > 1){					
			header("Location: errorpage.php?status=8");	
			exit;
		}//end of if
		
		
		
		if($no_of_cols ==$constant_cols){
			
			$actualDataArray = array();
			$Unq_ques_id_array = array();
			$aOptions=array();
			for($j=1;$j<=$no_of_rows;$j++){
				for($k=1;$k<=$no_of_cols;$k++){
					$actualDataArray[$j][$k] = trim($data->sheets[$sheetno]['cells'][$j][$k]);
					if($j!=1){
						if($k==1){
							$Uques_id =$actualDataArray[$j][$k];										
						}
															
					}//end of if($j!=1)									
				}//end of  for($k)
				if($j!=1)
				$Unq_ques_id_array[]=$Uques_id;//array formed to check the uniqueness of question_id in excel
			}//end of for($j)
			
			/*First Table Starts Here*/			
			echo '<table width="100%" border="1" cellspacing="1" cellpadding="1">';
				echo '<tr align="center">';
	          		echo '<td align="center" class="greybluetext10" bgcolor="#FF3300"> <b>Invalid cell &amp; Invalid Header</b></td>';
					echo '<td align="center" class="greybluetext10"  bgcolor="#D1E0EF"> <b>Valid cell &amp; Valid Header Cell</b></td>';
				echo '</tr>';
	        echo '</table>';
	        /*First Table Ends Here*/
	        
	        /*Second Table Starts Here*/			
			echo '<table width="100%" border="1" cellspacing="1" cellpadding="1"><tr><td>';
			echo '<table width="100%" border="1" cellspacing="1" cellpadding="1">';
			echo '<tr><td align="center" class="greybluetext10" bgcolor="#D1E0EF" colspan=2><b>DATA FROM EXCEL<b></td></tr>';
			for($j=1;$j<=$no_of_rows;$j++){
				echo '<tr>';
					for($k=1;$k<=$no_of_cols;$k++){											
						$celldata = trim($data->sheets[$sheetno]['cells'][$j][$k]);
						if ($j == 1){
							$errormsg = header_update_validate($k,$celldata);
							$celldata="<b>".$celldata."</b>";										
							if ($errormsg == 0){
								//Error in Row $j Column $k Header. Invalid Sheet
								echo '<td align="center" bgcolor="#FF3300">'.$celldata.'</td>';
								$flag_no_error[]=2;
							}//end of $errormsg
					}//$j=1 : header validation ends here

					if ($j > 1 && $k <=2) {
						
						if($k == 1){	
							$ques_id_store=$celldata;
							$errormsg = UniqueCheck($celldata,$Unq_ques_id_array);	//seperate function for question id checks																																			
							// if the function UniqueCheck return error ie 0
							if ($errormsg == 0) {
								//Error in Row $j Column $k Header. Invalid Sheet
								echo '<td align="center" class="greybluetext10" bgcolor="#FF3300">'.$celldata.'</td>';
								$errormsg = "";
								$flag_no_error[]=2;
							}//end of $quesid_check
							
																			
						}//end of if($k=1)
						
						if($k == 2){							
							$check_option=select_quesid_details($ques_id_store);	//function called to get the old details relating to question id 
							
							
							$aOptions[1]=trim($check_option[option_1]);
							$aOptions[2]=trim($check_option[option_2]);
							$aOptions[3]=trim($check_option[option_3]);
							$aOptions[4]=trim($check_option[option_4]);	
							$aOptions[5]=trim($check_option[option_5]);								
							$errormsg = rowval($k,$celldata);
							
							// if the function UniqueCheck return error ie 0
							if ($errormsg == 1) {								
								if($aOptions[$celldata]==""){									
									$errormsg=0;
								}
								else{
									$errormsg=1;
								}
							}							
							if ($errormsg == 0) {
								//Error in Row $j Column $k Header. Invalid Sheet
								echo '<td align="center" class="greybluetext10" bgcolor="#FF3300" >'.$celldata.'</td>';
								$errormsg = "";
								$flag_no_error[]=2;
							}//end of $errormsg
						
																			
						}//end of if($k=1)
					
					}//($j)
					
					if ($errormsg == 1 ){
						echo '<td align="center" class="greybluetext10" bgcolor="D1E0EF" >'.$celldata.'</td>';
						$errormsg = "";						
					}	//end of $errormsg

					}//for($k)
				echo '</tr>';
			}//($j)
			echo '</table></td>';
		}//$no_of_cols=2
		else{
			header("Location: errorpage.php?status=12");	
			exit;
		}
	
 					
if(!in_array($error_flag,$flag_no_error)){
	
/*Dbcolumns of iib_section_questions*/
$dbcolumns	='question_id,correct_answer';
$dbcolumns_log	='id,question_id,old_correct_answer,new_correct_answer,exam_code,subject_code,section_code,excel_filename,updated_on';
$arrayquestion_id=array();
for($j=2;$j<=$no_of_rows;$j++){
	$ques_id="";
	$correct_ans="";
	for($k=1;$k<=$no_of_cols;$k++){
		if($k == 1){
			$ques_id = trim($data->sheets[$sheetno]['cells'][$j][$k]);				
		}
		else if($k==2){
			$correct_ans = trim($data->sheets[$sheetno]['cells'][$j][$k]);
		}
		
	}//end of $k
		
	$aoldDetails=select_quesid_details($ques_id);	//function called to get the old details relating to question id 
	
	$question_id_log=trim($aoldDetails[question_id]);
	$exam_code_log=trim($aoldDetails[exam_code]);
	$subj_code_log=trim($aoldDetails[subject_code]);
	$sec_code_log=trim($aoldDetails[section_code]);
	$old_correctans_log=trim($aoldDetails[correct_answer]);
	
	$sql_update_ans = "UPDATE ".TABLE_SECTION_QUESTIONS." SET correct_answer='".trim($correct_ans)."' WHERE question_id='".trim($ques_id)."' ";			
	$res_update_ans = @mysql_query($sql_update_ans);
	if ($commondebug){
		print $sql_update_ans;
		echo mysql_error();
	}
	if (mysql_error()){
		echo mysql_error();
		exit;	
	}//end of if
	
	if($res_update_ans){
				
		$anewDetails=select_quesid_details($ques_id);	//function called to get the new details relating to question id 
		$new_correctans_log=$anewDetails[correct_answer];
		
		$sql_update_ans = "INSERT INTO ".TABLE_QP_CORRECTANS_UPD_LOG." "
									." ($dbcolumns_log) "
									." VALUES "
									." ('', "
									." '$question_id_log', "
									." '$old_correctans_log', "
									." '$new_correctans_log', "
									." '$exam_code_log', "
									." '$subj_code_log', "
									." '$sec_code_log', "									
									." '$file_name' , "
									." now() ) ";								

		$sql_update_ans = @mysql_query($sql_update_ans);
		if ($commondebug){
			print $sql_update_ans;
			echo mysql_error();
		}
		if (mysql_error()){
			echo mysql_error();
			exit;	
		}//end of if		
		$arrayquestion_id[]=$ques_id;
		$flag_disp="import";
	}//end of if
}//end of($j)


if($flag_disp=="import"){	
$message= "Imported Successfully";			
echo "<div id='message' style='position:relative;top:-155;' align=center class='greybluetext10'><b>$message</b></div>"; 	
	$disp_value=select_ques();	
	echo '<td><table width="100%" border="1" cellspacing="1" cellpadding="1">';
	echo '<tr><td align="center" class="greybluetext10" bgcolor="#D1E0EF" colspan=2><B>DATA FROM DATABASE<B></td></tr>';
	echo '<tr><td align="center" class="greybluetext10" bgcolor="#D1E0EF" ><b>question_id</b></td><td align="center" class="greybluetext10" bgcolor="#D1E0EF" ><b>correct_answer</b></td></tr>';
	foreach($arrayquestion_id as $key => $q_id)
	{
		echo '<tr>';				
		echo '<td align="center" class="greybluetext10" bgcolor="#D1E0EF" >'.$q_id.'</td>';
		echo '<td align="center" class="greybluetext10" bgcolor="#D1E0EF" >'.$disp_value[$q_id].'</td>';
		echo '</tr>';
	}	
	echo '</table>';
	
	echo '</tr></table>';
	
	/*Second Table Ends Here*/			
	
}

}//!in_array()
}//$file!=""
}//else of $error_message

}//end of $flag=update
					
$asecques_data=dispdata_question();	
if($flag!="update" && $flag_disp!="import"){					
	echo '<table width="100%" border="0" cellspacing="1" cellpadding="1">';
	echo '<tr><td align="center" class="greybluetext10" bgcolor="#D1E0EF" colspan=3><B>Question Count<B></td></tr>';
	echo '<tr><td align="center" class="greybluetext10" bgcolor="#D1E0EF" ><b>Exam Code</b></td><td align="center" class="greybluetext10" bgcolor="#D1E0EF" ><b>Subject Code</b></td><td align="center" class="greybluetext10" bgcolor="#D1E0EF" ><b>Count</b></td></tr>';
	$total_quescount=0;
	foreach($asecques_data as $key)
	{	
		
		$total_ques=$key[ques_count];												
		echo '<tr>';				
		echo '<td align="center" class="greybluetext10" bgcolor="#D1E0EF" >'.$key[exam_code].'</td>';
		echo '<td align="center" class="greybluetext10" bgcolor="#D1E0EF" >'.$key[subject_code].'</td>';
		echo '<td align="center" class="greybluetext10" bgcolor="#D1E0EF" >'.$key[ques_count].'</td>';
		echo '</tr>';
		$total_quescount+=$total_ques;
	}	
	echo '<tr>';				
	echo '<td align="center" class="greybluetext10" bgcolor="#D1E0EF" colspan=2><b>Total Number Of Questions</b></td>';
	echo '<td align="center" class="greybluetext10" bgcolor="#D1E0EF" ><b>'.$total_quescount.'</b></td>';
	echo '</tr>';				
	
	echo '</table>';
}
					

					?>

  	             		    			
		     <table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td colspan="2" class="greybluetext10" colspan="2" height="35" align=center></td></tr></table>
 	      </td>
 	    <td width="150">&nbsp;</td>
 	 </tr>
<tr><td>&nbsp;</td></tr>
 <tr>
			<td colspan=2 class="greybluetext10">Please refer to the 
  <a href="download/correct_ans_upload.xls" target="_blank">Excel Template </a> for Correct Answer Upload </td>
</tr>
<tr>

<td colspan=2 class="greybluetext10">Please refer to the 
  <a href="download/validation_correct_ans.doc" target="_blank">Validation Document </a> for Correct Answer Upload </td>
</tr>

	 </table>
 </td>
</tr>
<tr>
     	<?include("includes/footer.php");?>
	</tr>
</table>

</form>
</center>
</body>
</html>

