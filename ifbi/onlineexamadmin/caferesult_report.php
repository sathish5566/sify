<?php
/****************************************************
* Application Name            :  IIB
* Module Name                 :  Caferesult report
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :
* Tables used for only selects: iib_exam_centres,iib_exam_schedule,iib_exam_slots,iib_iway_details,iib_candidate_iway,
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              : CSV file and HTML page
* Document/Reference Material :
* Created By                  :
* Created ON                  :
* Last Modified By            : K.KARUNA MOORTHY RAJU
* Last Modified Date          : 01-04-2006
* Description                 : This report displaying the Exams result with the related information and it can be downloaded as a csv file
*****************************************************/
    ob_start();
	require_once "sessionchk.php";
	require_once("dbconfig_slave_123.php");
	require_once("constants.inc");
/* Include file for excel download */
	include_once("excelconfig.php");
	$sqlcentre = "SELECT exam_centre_code,upper(exam_centre_name) FROM iib_exam_centres where online='Y' ";
	$rescentre = mysql_query($sqlcentre);
	$sqlDate = "SELECT distinct exam_date FROM iib_exam_schedule order by exam_date";
	$resDate = mysql_query($sqlDate);
	$nDates = mysql_num_rows($resDate);
    $sqltim  = "SELECT slot_time FROM iib_exam_slots order by slot_time";
	$restim  = mysql_query($sqltim);
    $colour = 0;
	while(list($sltime)=mysql_fetch_row($restim))
	{
		$tim_array[$sltime]=$sltime;
        $bgcolor_array[$sltime]=$aColor[$colour];
		$colour++;
	}
    
/* Modifying the Date format of the Examdate   */
	if ($nDates > 0)
		{
			while (list($eDate) = mysql_fetch_row($resDate))
			{
				$aDate = explode("-",$eDate);
				$aDispDate[$eDate] = $aDate[2]."-".$aDate[1]."-".$aDate[0];
			}
		}
	$sqlTime ="SELECT slot_time FROM iib_exam_slots order by slot_time";
	$resTime = mysql_query($sqlTime);
	$nTime = mysql_num_rows($resTime);
		if ($nTime > 0)
		{
			while (list($eTime) = mysql_fetch_row($resTime))
			{
				$aDispTime[$eTime] = substr($eTime,0,5);
			}
		}

/*** '$ex_flag' is for getting the values when clicking download button and '$filename' is used for
downloading the file with its value, '$exce_header' to store the header information ***/

	$examCentreCode = $_POST['exam_centre_code'];
	$examDate = $_POST['exam_date'];
	$examTime = $_POST['exam_time'];
    $submitted= $_POST['submitted'];
    $ex_flag=$_POST['exl'];
    if($examTime!='all')
    {
    $extime = explode(':',$examTime);
    $extime = $extime[0]."-".$extime[1];
    }
    else
    $extime = $examTime;
	$filename="Caferesult_report_Examcentre(".$examCentreCode.")_date(".$examDate.")_time(".$extime.")";

/*** exce_header array content is varies depends upon the examTime and examCentreCode
 Variable name - $exce_header
 ***/
    	if($examTime!='all')
       		$exce_header = array("S.No","City","Cafe Id","Exam Date","No. of Candidates Allocated","No. of Candidates appeared","No of Candidates Passed");
       	else
       	    $exce_header = array("S.No","City","Cafe Id","Exam Date","Exam Time","No. of Candidates Allocated","No. of Candidates appeared","No of Candidates Passed");
       			if ($examCentreCode !='')
    			{
					if ($examCentreCode !='all')
					{

	    			$sql_centrecode="select distinct a.centre_code from iib_iway_details a,iib_exam_centres b where a.exam_centre_code=b.exam_centre_code and b.exam_centre_code='".$examCentreCode."' and online='Y' order by a.centre_code ";
	    		    $res_centrecode=mysql_query($sql_centrecode);
	    			$centre_code='';

						while(list($centrecode)=mysql_fetch_row($res_centrecode))
						{
						    $centrecode = strtoupper($centrecode);
							if ($centre_code =='')
								$centre_code = "(centre_code='$centrecode' ";
							else
								$centre_code .= " or centre_code='$centrecode'";
						}
					$centre_code .= " ) ";
					}
				}
/**** Select the centre code,Exam time,exam date depends on examCentrecode and examDate    ****/
		if (($examCentreCode != "") && ($examDate != "") && ($examTime != "")&& $submitted=='true')
		{
       		 switch($examCentreCode)
        	{

            	case "all":

                	if($examTime=='all')
                	{

                    	//$sqlFormat = "select a.centre_code,upper(a.iway_city),a.actual_seats,b.exam_time from iib_iway_details a,iib_candidate_iway b where a.centre_code=b.centre_code and b.exam_date='".$examDate."' group by a.centre_code,a.actual_seats order by a.iway_city ";
                    	$sqlFormat = "select distinct centre_code,exam_date,exam_time from iib_candidate_iway  where exam_date='".$examDate."'   order by centre_code,exam_date,exam_time ";
                    	//echo $sqlFormat;

                	}
                	else
                	{
                    	//$sqlFormat = "select a.centre_code,upper(a.iway_city),a.actual_seats,b.exam_time from iib_iway_details a,iib_candidate_iway b where a.centre_code=b.centre_code  and b.exam_date='".$examDate."' and b.exam_time='".$examTime."' group by a.centre_code,a.actual_seats order by a.iway_city ";
                    	$sqlFormat = "select distinct centre_code,exam_date,exam_time from iib_candidate_iway  where exam_date='".$examDate."' and exam_time='".$examTime."'  order by centre_code,exam_date,exam_time  ";
                    	//echo $sqlFormat;
                	}

                	break;

            	default :

                	if($examTime=='all')
                	{

                    	//$sqlFormat = "select a.centre_code,upper(a.iway_city),a.actual_seats,exam_time from iib_iway_details a,iib_candidate_iway b where $centre_code and a.centre_code=b.centre_code and b.exam_date='".$examDate."' group by a.centre_code,a.actual_seats order by a.iway_city ";
                    	$sqlFormat = "select distinct centre_code,exam_date,exam_time from iib_candidate_iway  where $centre_code and exam_date='".$examDate."' order by centre_code,exam_date,exam_time ";
                    	//echo "SQL FORMAT ".$sqlFormat;
                	}
               		else
                	{

                    	//$sqlFormat = "select a.centre_code,upper(a.iway_city),a.actual_seats,exam_time from iib_iway_details a,iib_candidate_iway b where $centre_code and a.centre_code=b.centre_code and b.exam_date='".$examDate."' and b.exam_time='".$examTime."' group by a.centre_code,a.actual_seats order by a.iway_city ";
                    	$sqlFormat = "select distinct centre_code,exam_date,exam_time from iib_candidate_iway where $centre_code and exam_date='".$examDate."' and exam_time='".$examTime."'  order by centre_code,exam_date,exam_time  ";
                    	//echo "SQL FORMAT ".$sqlFormat;
                	}
                	break;

        	}


        $resFormat = mysql_query($sqlFormat);
        $ncount=mysql_num_rows($resFormat);
		$sql_city="select trim(exam_centre_code),exam_centre_name  from iib_exam_centres";
		$res_city=mysql_query($sql_city);

/**** Getting the city name into an array named cityArr     ****/

			while(list($examCentre_Code,$cityName)=mysql_fetch_row($res_city))
			{
				$cityArr[$examCentre_Code]=$cityName;
			}

		$sqlcand="select count(1),centre_code,exam_date,exam_time from iib_candidate_iway group by centre_code,exam_date,exam_time ";
        $rescand=mysql_query($sqlcand);

/**** Getting the allocated seats into an array named allocArr     ****/

	          while(list($alloc,$centre_Code,$exam_date,$exam_time)=mysql_fetch_row($rescand))
	          {
	              $centre_Code = strtoupper($centre_Code);
		          $allocArr[$centre_Code][$exam_date][$exam_time]=$alloc;
	          }

	    	  if ($examCentreCode=='all')
	    	  {
		   		 if ($examTime=='all')
		    	 {
	    			$sql_appeared="select count(distinct a.membership_no,a.subject_code),a.centre_code,a.exam_date,a.exam_time from iib_candidate_iway a,iib_candidate_test b where a.membership_no=b.membership_no and a.exam_code=b.exam_code and a.subject_code=b.subject_code and a.exam_date='".$examDate."' group by a.centre_code,a.exam_time";

	    			$sql_result="select count(distinct a.membership_no,a.subject_code),b.exam_date,b.exam_time,b.centre_code from iib_candidate_scores a,iib_candidate_iway b where a.membership_no=b.membership_no and a.exam_code=b.exam_code and a.subject_code=b.subject_code and a.result='P' and b.exam_date='".$examDate."' group by b.centre_code,b.exam_time";
    			}
    			else
    			{
	    			$sql_appeared="select count(distinct a.membership_no,a.subject_code),a.centre_code,a.exam_date,a.exam_time from iib_candidate_iway a,iib_candidate_test b where a.membership_no=b.membership_no and a.exam_code=b.exam_code and a.subject_code=b.subject_code and a.exam_date='".$examDate."' and a.exam_time='".$examTime."' group by a.centre_code";

	    			$sql_result="select count(distinct a.membership_no,a.subject_code),b.exam_date,b.exam_time,b.centre_code from iib_candidate_scores a,iib_candidate_iway b where a.membership_no=b.membership_no and a.exam_code=b.exam_code and a.subject_code=b.subject_code and a.result='P' and b.exam_date='".$examDate."' and b.exam_time='".$examTime."' group by b.centre_code";
	    		}
    		}
    		else
    		{
	    	$sql_centrecode="select distinct a.centre_code from iib_iway_details a,iib_exam_centres b where a.exam_centre_code=b.exam_centre_code and b.exam_centre_code='".$examCentreCode."' and online='Y' order by a.centre_code";
	    	$res_centrecode=mysql_query($sql_centrecode);
	    	$centre_code='';

				while(list($centrecode)=mysql_fetch_row($res_centrecode))
				{
				    $centrecode = strtoupper($centrecode);
					if ($centre_code =='')
						$centre_code = "(a.centre_code='$centrecode' ";
					else
						$centre_code .= " or a.centre_code='$centrecode'";
				}
			$centre_code .= " ) ";

	    		if ($examTime=='all')
	    		{
	    			$sql_appeared="select count(distinct a.membership_no,a.subject_code),a.centre_code,a.exam_date,a.exam_time from iib_candidate_iway a,iib_candidate_test b where a.membership_no=b.membership_no and a.exam_code=b.exam_code and a.subject_code=b.subject_code and $centre_code and a.exam_date='".$examDate."' group by a.centre_code,a.exam_time";

	    			$sql_result="select count(distinct b.membership_no,b.subject_code),a.exam_date,a.exam_time,a.centre_code from iib_candidate_scores b,iib_candidate_iway a where b.membership_no=a.membership_no and b.exam_code=a.exam_code and b.subject_code=a.subject_code and b.result='P' and $centre_code and a.exam_date='".$examDate."' group by a.centre_code,a.exam_time";

    			}
    			else
    			{
	    			$sql_appeared="select count(distinct a.membership_no,a.subject_code),a.centre_code,a.exam_date,a.exam_time from iib_candidate_iway a,iib_candidate_test b where a.membership_no=b.membership_no and a.exam_code=b.exam_code and a.subject_code=b.subject_code and $centre_code and a.exam_time='".$examTime."' and a.exam_date='".$examDate."' group by a.centre_code";

	    			$sql_result="select count(distinct b.membership_no,b.subject_code),a.exam_date,a.exam_time,a.centre_code from iib_candidate_scores b,iib_candidate_iway a where b.membership_no=a.membership_no and b.exam_code=a.exam_code and b.subject_code=a.subject_code and b.result='P' and $centre_code and a.exam_date='".$examDate."' and a.exam_time='".$examTime."' group by a.centre_code";
    			}
		  }
	      $res_appeared=mysql_query($sql_appeared);
	    	while(list($appeared,$centre_code,$exam_date,$exam_time)=mysql_fetch_row($res_appeared))
	    	{
	    	    $centre_code = strtoupper($centre_code);
				$appearedArr[$centre_code][$exam_date][$exam_time]=$appeared;
	    	}

	      $res_result=mysql_query($sql_result);

	      	while(list($passed,$exam_date,$exam_time,$centre_code)=mysql_fetch_row($res_result))
	    	{
	    	    $centre_code = strtoupper($centre_code);
				$resultArr[$centre_code][$exam_date][$exam_time]=$passed;
	    	}
	    $sql_exam_centre_code="select centre_code,trim(exam_centre_code) from iib_iway_details";
	    $res_exam_centre_code=mysql_query($sql_exam_centre_code);

        	while(list($centreCode,$examCentre_Code)=mysql_fetch_row($res_exam_centre_code))
        	{
	        	$centreCode=strtoupper($centreCode);
	       		$examArr[$centreCode]=$examCentre_Code;
       		}
         $i=0;

/**** Getting the centrecode,examcentrecode,actualseats,examdate,examtime into an array named exce_content[][].      ****/

            while(list($centre_code,$exam_date,$exam_time)=mysql_fetch_row($resFormat))
            {
	             $centre_code = strtoupper($centre_code);
	             $exam_centre_code=$examArr[$centre_code];
	                if($appearedArr[$centre_code][$exam_date][$exam_time]=='')
	                    $appearedArr[$centre_code][$exam_date][$exam_time]='0';
    	   	        if($resultArr[$centre_code][$exam_date][$exam_time]=='')
        	            $resultArr[$centre_code][$exam_date][$exam_time]='0';
               	$exce_content[$i][0] = $i+1;
                $exce_content[$i][1] = strtoupper($cityArr[$exam_centre_code]);
                $exce_content[$i][2] = $centre_code;
                $exce_content[$i][3] = $exam_date;
           		 if($examTime=='all')
                	 {
                		$exce_content[$i][4] = $exam_time;
                		$exce_content[$i][5] = $allocArr[$centre_code][$exam_date][$exam_time];
                		$exce_content[$i][6] = $appearedArr[$centre_code][$exam_date][     		$exam_time];
                		$exce_content[$i][7] = $resultArr[$centre_code][$exam_date][$exam_time];
                	  }
                	  else
                	  {
	            		$exce_content[$i][4] = $allocArr[$centre_code][$exam_date][$exam_time];
                		$exce_content[$i][5] = $appearedArr[$centre_code][$exam_date][$exam_time];
                		$exce_content[$i][6] = $resultArr[$centre_code][$exam_date][$exam_time];
						$exce_content[$i][7] = $exam_time;
					 }
                $i++;
                }
/* Execution of excel download function */
     if($ex_flag=='1')
        {
	    	 excelconfig($filename,$exce_header,$exce_content);
	     	 exit;
        }
 }
?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='JavaScript' src="./includes/validations.js"></script>
<script language='JavaScript'>
function submitForm()
{
	if (document.frmalloc.exam_centre_code.selectedIndex == 0)
	{
		alert("Please select the exam centre");
		document.frmalloc.exam_centre_code.focus();
		return false;
	}
	if (document.frmalloc.exam_date.selectedIndex == 0)
	{
		alert("Please select the exam date");
		document.frmalloc.exam_date.focus();
		return false;
	}
    document.frmalloc.submitted.value='true';
    document.frmalloc.exl.value='0';
	document.frmalloc.submit();
}

function excelsubmit()
{

	var frmname=document.frmalloc;
	frmname.exl.value='1';
    frmname.submitted.value='true';
	frmname.submit();
}


</script>

</HEAD>
<BODY leftMargin=0 topMargin=0 LeftMargin=0 marginheight=0 marginwidth=0 >
<center>
<form name='frmalloc' method='post'>
<input type='hidden' name='populatecenter'>
<input type='hidden' name='submitted'>
<input type='hidden' name='exl' value='0'>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780 background=images/tile.jpg >

<!--	<TR><!-- Topnav Image -->
      <!--  <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR>-->
	<tr><td width="780"><?include("includes/header.php");?></td></tr>
    <TR>

    </TR>
	<tr>
		<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>
	</tr>
    <TR>
    	<TD background=images/tile.jpg vAlign=top width=780 align=center>

        <table width=780 cellpadding=0 cellspacing=5 border=0 >
        	<tr>
				<td colspan=2 align="center"><b><font face="Verdana, Arial, Helvetica, sans-serif" size=2>Cafe-Result Report</b></font><br><br></td>
            </tr>
            <!--<tr>
            	<td colspan=2 align="center" class="greybluetext10"><b><?=$msg ?></b></td>
            </tr>-->
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Exam Centre : </td>
            	<td align="left" valign=top class="greybluetext10">
            		<select name='exam_centre_code' class="greybluetext10" style={width:150px} <!--onchange='javascript:selcentre();'-->>
            			<option value=''>--Select--</option>
            			<?
            					if ($examCentreCode == "all")
	            				{
		            				$selectedAll = "selected";
	            				}
            			?>
            			<option value='all' <?=$selectedAll?>>--All--</option>
            			<?


            				while (list($code,$name) = mysql_fetch_row($rescentre))
            				{
	            				print("<option value='$code' ");
	            				if ($examCentreCode == $code)
	            				{
		            				print(" selected ");
	            				}


	            				print(">$name</option>");
	            			}
	            		?>
            		</select>
            	</td>
            </tr>
             <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Exam Date : </td>
            	<td align="left" valign=top class="greybluetext10">
            		<select name='exam_date' class="greybluetext10" style={width:150px} >
            			<option value=''>--Select--</option>
            			<?php
            				foreach ($aDispDate as $key=>$val)
            				{
	            				print("<option value='$key' ");
	            				if ($examDate == $key)
	            				{
		            				print(" selected ");
	            				}
	            				print(">$val</option>");
            				}
            			?>
            		</select>
            	</td>
            </tr>
              <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Exam Time : </td>
            	<td align="left" valign=top class="greybluetext10">
            		<select name='exam_time' class="greybluetext10" style={width:150px} >
            			<option value='all'>All Shifts</option>
            			<?php
            				foreach ($aDispTime as $key=>$val)
            				{
	            				print("<option value='$key' ");
	            				if ($examTime == $key)
	            				{
		            				print(" selected ");
	            				}
	            				print(">$val</option>");
            				}
            			?>
            		</select>
            	</td>
            </tr>



      </table>
    <table width=780 >
        <tr>
			<td colspan=2 align="center"><input type=button name='sub_assign' value='Submit' class='button' onClick='javascript:submitForm()'>

			</td>
        </tr>
       <? if (($submitted=='true') &&($ncount < 1)){?>
       <tr>
       	<td width=780 align=center class=alertmsg>No Records to display</td>
       	</tr><?}?>
    </table>
    <?if(($submitted=='true') && ($ncount >=1 ))
    {

        	// Printing the values in the HTML page

			// Printing the header values from excel header array
		$header_count = count($exce_header);
		print("<table border=1 cellspacing=0 cellpadding=2><tr>");
		for($a=0;$a<$header_count;$a++)
		{
			print("<td class=greybluetext10><b>$exce_header[$a]</b></td>");
		}
		print("</tr>");

		// Loop to display the values in the HTML page
		print("<tr>");
		     for($j=0;$j<$ncount;$j++)
            	 {
	          		for($k=0;$k<$header_count;$k++)
	           		{
		             $chek = $header_count-4;
					 
/*** Background color of the each row is changed depends on conditions ***/
		       
				   	    $bgcolor = $bgcolor_array[$exce_content[$j][$chek]];  
			    		$fontc   ="black";
						if($bgcolor=='')
                           $bgcolor="red";
						if($examTime!='all')
						{
							$chek = $header_count;
							$bgcolor = $bgcolor_array[$exce_content[$j][$chek]];  
			    		  $fontc   ="black";
						}
													    		
		               ?><td  class="greybluetext10" bgcolor=<?=$bgcolor?>><font color=<?=$fontc?>><?=$exce_content[$j][$k]?>&nbsp;</font></td>
		          <?
	           }
	           print("</tr>");
       } ?>
         </table>

       	<table width=300  cellspacing=5 border=0 align=center >

				<tr><td>&nbsp;</td></tr>
				<? 
				 foreach($bgcolor_array as $disp_tim => $disp_exam_time) 
		            {
                 ?>
			    <tr>
				<td colspan=2 bgcolor="<?=$bgcolor_array[$disp_tim]?>"></td> 
				<td class="greybluetext10" colspan=4><?=$disp_tim?></td>
				</tr> 
                  <?
					 }   
				?>
                 <tr>
				<td colspan=2 bgcolor=red></td>
				<td class="greybluetext10" colspan=4>Other Shifts</td>
				</tr>
				<tr><td>&nbsp;</td></tr>

      </table>
      <table>
            <tr><td align="center" class=greybluetext10>
            <input class='button' type='button' name='download' value='Download' onClick='javascript:excelsubmit()'></td></tr>
            <tr><td align="center" class=greybluetext10>
            </table>
   <?  }
    ?>
         </td>
       </tr>
       <tr>
       		<?include("includes/footer.php");?>
       </tr>


</TABLE>
</form>
</center>
</BODY>
</HTML>
