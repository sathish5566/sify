<?php
ob_start();
$admin_flag=1;
require_once("constants.inc");
require_once("sessionchk.php");
$fontPath = $fpdfPath."/font/";
$fpdfFile = $fpdfPath."/fpdf.php";
define('FPDF_FONTPATH',$fontPath);
require($fpdfFile);
set_time_limit(0);

class PDF extends FPDF
{
//Constructor (mandatory with PHP3)
function PDF()
{
	$this->FPDF();
}

//Page header
function Header()
{
	//Logo
	//function Image($file,$x,$y,$w,$h=0,$type='')
	$this->Image('images/logo.png',10,8,200,20);
	//Arial bold 15
	$this->SetFont('Arial','B',15);
	//Move to the right
	$this->Cell(80);
	//Title
	//$this->Cell(30,10,'Title',1,0,'C');
	//Line break
	$this->Ln(20);
}

//Page footer
function Footer()
{
	//Position at 1.5 cm from bottom
	$this->SetY(-15);
	//Arial italic 8
	$this->SetFont('Arial','I',8);
	//Page number
	$this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
}
}

//Instanciation of inherited class
require_once("dbconfig.php");
$aQuestionPaper = $_POST['question_paper'];
$questionPaperNo = $_POST['qp_no'];
$examCode = $_POST['exam_code'];
$subjectCode = $_POST['subject_code'];
$totalMarks = $_POST['total_marks'];
$mediumCode = $_POST['medium_code'];
$altMedium = $_POST['alt_medium'];

if ($mediumCode == 'E') 
{
	if ($altMedium == 1)
		$tableName = "iib_section_questions_hindi";
	else
		$tableName = "iib_section_questions";
}

if ($mediumCode == 'H')
{
	if ($altMedium == 1)
		$tableName = "iib_section_questions";
	else
		$tableName = "iib_section_questions_hindi";
}
if ($tableName == "iib_section_questions")
	$mediumCode = "E";
else
	$mediumCode = "H";
$sqlExam = "SELECT exam_name FROM iib_exam WHERE exam_code='$examCode' ";
$resExam = @mysql_query($sqlExam);
list($examName) = @mysql_fetch_row($resExam);

$sqlSubject = "SELECT subject_name FROM iib_exam_subjects WHERE exam_code='$examCode' AND subject_code='$subjectCode' ";
$resSubject = @mysql_query($sqlSubject);
list($subjectName) = @mysql_fetch_row($resSubject);

$nPapers = count($aQuestionPaper);

$pdf=new PDF();
$pdf->Open();
$pdf->SetFont('Arial','B',10);


	$pdf->AddPage();
	$strHead = "Exam : ".$examName;
	$pdf->MultiCell(0,5,$strHead,0,1);	
	$strHead = "Subject : ".$subjectName;
	$pdf->MultiCell(0,5,$strHead,0,1);	
	$strHead = "Total Marks : ".$totalMarks;
	$pdf->Cell(0,5,$strHead,0,1);
	$strHead = "Total Time : 2 hrs";
	$pdf->Cell(0,5,$strHead,0,1);
	$strHead = "Question Paper : ".$questionPaperNo;
	$pdf->Cell(0,5,$strHead,0,1);
	$strFileName = 'QP_'.$questionPaperNo."_".$mediumCode.".pdf";
	if ($mediumCode == 'H')
		$pdf->AddFont('DVBWTTYogeshENBold','B','DVBWTTYogeshENBold.php');
	
	$alpha = array("a","b","c","d","e");
	$sql = "SELECT question_id FROM iib_question_paper_details WHERE question_paper_no=$questionPaperNo  ORDER BY display_order ";
	$res = @mysql_query($sql);
	
	if (@mysql_num_rows($res) > 0)
	{
		$nRow = 0;
		while (list($questionID) = @mysql_fetch_row($res))
		{
			$nRow++;
			$sqlSelect = "SELECT question_text, option_1, option_2, ".
							"option_3, option_4, option_5, marks FROM $tableName ".
							"WHERE question_id='$questionID' ";
								
			$result1 = @mysql_query($sqlSelect) or die("Query failed ".mysql_error());
			$nQuestions = @mysql_num_rows($result1);				
			
								
			while ($row = @mysql_fetch_array($result1, MYSQL_ASSOC))
			{								
				$questionText = $row['question_text'];				
				$questionMarks = round($row['marks'], 1);		
				$strQn = trim($nRow.". ".$questionText." [Mark/s - ".$questionMarks."] ");
				//$strQn = $nRow.". ".$questionText;
				if ($mediumCode == 'H')
					$pdf->SetFont('DVBWTTYogeshENBold','B',12);
				else
					$pdf->SetFont('Arial','B',10);
				$pdf->Cell(0,2," ",0,1);	
				$pdf->MultiCell(0,5,$strQn,0,1);
				if ($mediumCode == 'H')		
					$pdf->SetFont('DVBWTTYogeshENBold','B',12);
				else
					$pdf->SetFont('Arial','',10);
				for ($k=1; $k <= 5; $k++)
				{														
					$var = "option".$k;
					$$var = $row["option_".$k];
					
					if ($$var != "")
					{
						$strAns = $alpha[$k-1].") ".$$var;
						$pdf->MultiCell(0,5,$strAns,0,1);
					}																			
				}				
			}		
		}		
	}

$pdf->Output($strFileName,true);
ob_end_flush();
?>
