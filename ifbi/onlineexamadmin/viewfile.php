<?php
	
/****************************************************
* Application Name            :  IIB BULKUPLOAD
* Module Name                 :  View Excel File after uploading
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  functions.php and all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  Bala
* Created ON                  :  
* Last Modified By            :  B.Devi
* Last Modified Date          :  24-03-2006
* Description                 :  View Excel File after uploading (if any errors cell is marked in red)
*****************************************************/
	require_once("sessionchk.php");
	require_once("dbconfig.php");
	
	$fileid = $_GET['fileid'];
	require_once("includes/settings.php");
	require_once("includes/iway_functions.php");
	require_once("includes/functions.php");
	require_once("includes/reader.php");
	require_once("includes/logger.php");

/** Common Debug Variable for debugging the code which prints the data */
$commonDebug = false;

//If no entry in slots redirected to error page
if(count($aslot_time)==0){
	header("Location: errorpage.php?status=10");
	exit;
}//end of if


/*All Validations made in viewfile.php should be repeated in db_import.php before insertion*/

/**
*			Excel reader class usage starts here
*			Instantiating the Excel reader Class
*/

$data = new Spreadsheet_Excel_Reader();

/** setting the encoding format */
$data->setOutputEncoding('CP1252');

/** This will read from the excel file  */
$data->read($filenamepath.$file_name);

$no_of_sheets = count($data->sheets);//count of sheets

/*Check made to make sure the number of sheets should be 1*/
if($no_of_sheets > 1){					
	header("Location: errorpage.php?status=8");	
}//end of if

?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
.textbox        
{ 
font-family: Verdana, Arial, Helvetica, sans-serif; 
font-size: 10px; font-style: normal; 
text-decoration: none; 
height: 17px; 
border: 1px #000000 solid
}
</STYLE>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/validations.js"></script>
</head>

<BODY leftMargin=0 topMargin=0 LeftMargin=0 marginheight=0 marginwidth=0>
<script language="javascript" src="includes/javascript.js"></script>
<script language="javascript">

/** Swapping sheets in Excel */
function swap_sheet(sheet){

	var no_of_sheet = <?php echo $no_of_sheets; ?>;
	for(i=0;i<no_of_sheet;i++){
		
		var objSheet = document.getElementById('sheet'+i);
		objSheet.style.display="none";
	
	}	
	var objSheet = document.getElementById('sheet'+sheet);
	objSheet.style.display="block";
	var objSheetNo = document.getElementById('sheetno');
	objSheetNo.value=sheet;
	var objrows_sheet = document.getElementById('rows_sheet'+sheet);
	var objcols_sheet = document.getElementById('cols_sheet'+sheet);
	var objNo_of_cols = document.getElementById('no_of_cols');
	var objNo_of_rows = document.getElementById('no_of_rows');
	objNo_of_cols.value = objcols_sheet.value;
	objNo_of_rows.value = objrows_sheet.value;
}//end of function swap_sheet

/** form submit */
function submit_page(){

	var frm = document.viewform;
	var objSheetNo = document.getElementById('sheetno');
	var no_of_sheets = <?php echo $no_of_sheets; ?>;
	var objRowSheet = document.getElementById('rows_sheet'+objSheetNo.value);
	var objColSheet = document.getElementById('cols_sheet'+objSheetNo.value);
	var objCellError = document.getElementById('cellerror'+objSheetNo.value);

	if(no_of_sheets > 1)
	{
		alert ("This file contains multiple sheets.. \nOnly single sheet is allowed, please remove other sheets");
		return false;
	}// end of if

	if (objCellError.value > 0){
		alert("This Sheet Cannot be Imported. Sheet is Invalid!.\n It contains invalid Cells.");
		return false;
	}// end of if 
	if (parseInt(objRowSheet.value) > 1 && parseInt(objColSheet.value) == 11){
		frm.action = "db_import.php";
		frm.submit();
	}else{
		alert("This Sheet Cannot be Imported. Sheet is Invalid!");
		return false;
	}
}// end of function submit_page 
</script>

</head>


<BODY leftMargin=0 topMargin=0 LeftMargin=0 marginheight=0 marginwidth=0>

<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
<!--	<TR><!-- Topnav Image -->
      <!--  <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR> -->
	<tr><td width="780"><?include("includes/header.php");?></td></tr>
    <TR>
        
    </TR>
	<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>
    <TR>
    	<TD background=images/tile.jpg vAlign=top width=780 align=center>
    	
<table cellspacing="0" cellpadding="0" width="100%" border="0" align="center" >
  <tr>
    <td width="5%"  valign="top"></td>
    <td width="90%">
      <table width="95%"  border="0" align="center" cellpadding="5" cellspacing="1">
  <tr>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" align="center" cellpadding="2" >
    
          <td>
         
            <p class="pageTitle">View Uploaded File </p>
						File Name: <?php echo $file_name; ?><br>
						Language : 	<?php echo $languageText;?><br>
						Exam Date: <?php echo  $excel_exam_date[0];?>
					</td> 
				</tr>
        <tr>
				</table>
				
				<table width="70%" align="left" cellpadding="3" cellspacing="1" bgcolor="#BEBEBE">
				<tr align="center">
          <td bgcolor="#FF3300"> Invalid cell &amp; Invalid Header</td>
					<td bgcolor="#FFFFFF"> Valid cell</td>
       		<td bgcolor="#F9F9F9"> Valid Header cell</td>
       		<td bgcolor="$EFA631">Conflict with already entered Iway Details</td>
				</tr>
        </table>
		</td>
  </tr>
  <tr>
    <td valign="top"><table border="0" cellspacing="1" cellpadding="1">
		
		</table>
		
    <table width="100%"  border="0" cellspacing="1" cellpadding="1">
      <tr>
        <td colspan="2">
				<?php

				/** Writing the logic for print on the screen from the array $data->sheets */
				/** we use this flag to check for the no of errors in the cell */
				$cellerror = 0;
				for($i=0; $i<1; $i++)
				{
						$no_of_rows = $data->sheets[$i]['numRows'];
						$no_of_cols = $data->sheets[$i]['numCols'];

						/** printing the no. of rows and columns  */
						if ($commonDebug) {
							echo "ROWS: ".$no_of_rows . " COLS: ".$no_of_cols."<br>";
						}
						
						if ($i == 0)
							$displaytype="block";
						else
							$displaytype="none";

						echo '<div id="sheet'.$i.'" style="display:'.$displaytype.'">';
						echo '<input type="hidden" name="rows_sheet'.$i.'" id="rows_sheet'.$i.'" value="'.$no_of_rows.'">';
						echo '<input type="hidden" name="cols_sheet'.$i.'" id="cols_sheet'.$i.'" value="'.$no_of_cols.'">';
						echo '<table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#BEBEBE">';
						
						/** rights now hard coded here for no of cols check limit to 11*/
						if ($no_of_cols == 11){
							$actualDataArray = array();
							$UnqCentre_array = array();
							for($j=1;$j<=$no_of_rows;$j++){
								for($k=1;$k<=$no_of_cols;$k++){
									$actualDataArray[$j][$k] = $data->sheets[$i]['cells'][$j][$k];
									if($j!=1){
										if($k==1){
											$Ucentrecode =$actualDataArray[$j][$k];										
											$Ucentrecode=strtoupper($Ucentrecode);
										}
																			
									}//end of if($j!=1)									
								}//end of  for($k)
								if($j!=1)
								$UnqCentre_array[]=$Ucentrecode;//array formed to check the uniqueness of centre code in excel
							}//end of for($j)
							
													
							/** Looping through each rows */
							for($j=1;$j<=$no_of_rows;$j++){

		          			echo '<tr align="center" bgcolor="#F9F9F9">';
							
								/** alternate color for displaying the questions */
								
								if ($j == 1)
									$bgcolor = "#F9F9F9";
								else
									$bgcolor = "#FFFFFF";

								for($k=1;$k<=$no_of_cols;$k++){

										$celldata = $data->sheets[$i]['cells'][$j][$k];
										
										if ($j == 1){
											$errormsg = headervalidate($k,$celldata);
											if ($errormsg == 0){
												//Error in Row $j Column $k Header. Invalid Sheet
												echo '<td align="center" bgcolor="#FF3300">'.$celldata.'</td>';
												$cellerror++;
												$errormsg = "";
											}//end of $errormsg
										}//$j=1 : header validation ends here
										

										if ($j > 1 && $k <=9 || $j > 1 && $k>=10) {
											if($k == 1){	
												$centre_code_store=strtoupper(trim($celldata));											
																																
												$centrecheck = countCheck($celldata,$UnqCentre_array);	//seperate function for centre code checks																																			
												// if the function countCheck return error ie 0
												if ($centrecheck == 0) {
														
													//Error in Row $j Column $k Header. Invalid Sheet
													echo '<td align="center" bgcolor="#FF3300" '.$ansClass.'>'.$celldata.'</td>';
													$cellerror++;
													$centrecheck = "";
												}//end of $centrecheck
																								
											}//end of if($k=1)
											if($k!=1){//if $k is other than 1
												$errormsg = rowvalidate($k,$celldata);
												
												/*Code added by Devi :
												if the Centre Code already added in Iway Details then the related details should match with 
												Excel Sheet.
												array_key_exists($cntseats) check done to find whether the centrecode is already 
												present in database
												If the center code is already present in database then the details of excel is compared
												with the excel data by using $cntseats.
												If there is any difference in details then to the display the error in green the $errormsg is assigned as 2
												*/
												if($k== 2){
													if($errormsg==1){										
																																																										
														if (array_key_exists($centre_code_store, $cntseats)) 												
														{
															if(($cntseats[$centre_code_store][$var_iwayname])==strtoupper(trim($celldata)))
																$errormsg=1;
															else
																$errormsg=2;
																//$errormsg=0;
														}//end of if(array_key_exists)
													}//if($errormsg==1)
												}//end of if($k==2)
												
												if($k== 3){
													$actual_seats_excel=$celldata;
													if($errormsg==1){																																								
														if (array_key_exists($centre_code_store, $cntseats)) 												
														{
															if(($cntseats[$centre_code_store][$var_actualseats])==trim($celldata))
																$errormsg=1;
															else
																$errormsg=2;
														}//end of if(array_key_exists)
													}//end of if($errormsg)
												}//end of if($k==3)
												
												if($k== 4){
													if($errormsg==1){																																								
														if (array_key_exists($centre_code_store, $cntseats))
														{														
															if(($cntseats[$centre_code_store][$var_no_of_seats])==trim($celldata))
																$errormsg=1;
															else
																$errormsg=2;
														}//end of if(array_key_exists)
														else{
															//Actual Seats should  be less than No of Seats
															if($actual_seats_excel<=$celldata)
																$errormsg=1;
															else
																$errormsg=0;
														}
													}//end of if($errormsg)
												}//end of if($k==4)
												
												if($k== 5){
													if($errormsg==1){																																								
														if (array_key_exists($centre_code_store, $cntseats))
														{														
															if(($cntseats[$centre_code_store][$var_status])==trim($celldata))
																$errormsg=1;
															else
																$errormsg=2;
														}//end of if(array_key_exists)
													}//end of if($errormsg)
												}//end of if($k==5)
												
												if($k== 8){
													if($errormsg==1){																																								
														if (array_key_exists($centre_code_store, $cntseats))
														{														
															if(($cntseats[$centre_code_store][$var_iway_city])==strtoupper(trim($celldata)))
																$errormsg=1;
															else
																$errormsg=2;
														}//end of if(array_key_exists)
													}//end of if($errormsg)
												}//end of if($k==8)
												
												if($k== 9){
													if($errormsg==1){																																								
														if (array_key_exists($centre_code_store, $cntseats))
														{														
															if(($cntseats[$centre_code_store][$var_iway_state])==strtoupper(trim($celldata)))
																$errormsg=1;
															else
																$errormsg=2;																
														}//end of if(array_key_exists)
													}//end of if($errormsg)
												}//end of if($k==9)
												
												if($k== 10){
													if($errormsg==1){
														//Pincode should match with first five digit of centrecode																																																						
														$pincode_check=substr($centre_code_store,0,6);
														if($pincode_check==trim($celldata))
															$errormsg=1;
														else
															$errormsg=0;
													}//end of if($errormsg)
												}//end of if($k==10)
												
												if($k== 11){
													if($errormsg==1){																																								
														if (array_key_exists($centre_code_store, $cntseats))
														{														
															if(($cntseats[$centre_code_store][$var_exam_centre_code])==trim($celldata))
																$errormsg=1;
															else
																$errormsg=2;
														}//end of if(array_key_exists)
													}//end of if($errormsg)
												}//end of if($k==11)
												
												if ($errormsg == 0) {
													//Error in Row $j Column $k Header. Invalid Sheet
													echo '<td align="center" bgcolor="#FF3300" '.$ansClass.'>'.$celldata.'</td>';
													$cellerror++;
													$errormsg = "";
												}
												
												if ($errormsg == 2) {
													//Error in Row $j Column $k Header. Invalid Sheet
													echo '<td align="center" bgcolor="$EFA631" '.$ansClass.'>'.$celldata.'</td>';
													$cellerror++;
													$errormsg = "";
												}
											}//end of if($k!=1)
										}// end of if($j > 1 && $k <=9 || $j > 1 && $k>=10)
										
										if (($errormsg == 1)||($centrecheck == 1)){
            								echo '<td align="center" bgcolor='.$bgcolor." ".$ansClass.'>'.$celldata.'</td>';
											$errormsg = "";
											$centrecheck="";
										}
										
								}// end of if($k=1)
          		echo '</tr>';
								
							}// end of if($j=1)
							
						}// end of if($no_of_cols)
						else{
						
          		echo '<tr align="center" bgcolor="#F9F9F9">';
							
							/** Adding error msg if no of cols != 11 */
							echo '<td align="center" bgcolor="#FFFFFF">You have Given less no of Columns in this Sheet<br></td>';
          		echo '</tr>';
							
						}						
        		echo '</table>';
						echo '<input type="hidden" name="cellerror'.$i.'" id="cellerror'.$i.'" value="'.$cellerror.'">';

						echo '</div>';
				
				}//end of for($i)
				
				?>
				</td>
      </tr>
    </table></td>
  </tr>
  <tr>	
    <td valign="top"> 
			<form name="viewform" method="post">

			<input type="hidden" name="sheetno" id="sheetno" value="0">
			<input type="hidden" name="fileid" id="fileid" value="<?=$fileid?>">
			<input type="hidden" name="cellerror" id="cellerror" value="<?=$cellerror?>">
			

			<input type="hidden" name="no_of_rows" id="no_of_rows" value="<?php echo $data->sheets[0]['numRows']?>">
			<input type="hidden" name="no_of_cols" id="no_of_cols" value="11">
			<p align=center><input type="submit" class="button" value="Import to Database" onclick="javascript:submit_page();return false;"></p>
			</form>
			
		</td>
  </tr>
</table>

</td>
    <td width="5%" class="rightColumn" valign="top"></td>
  </tr>
</table>
       </TD>
	</TR>
    <TR>
    	<?include("includes/footer.php");?>
    </TR>
</TABLE>
</body>
</html>