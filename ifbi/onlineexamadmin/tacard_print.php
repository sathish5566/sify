<?php
ob_start();
$admin_flag = 0;
require_once("sessionchk.php");
require_once("constants.inc");
$fontPath = $fpdfPath."/font/";
$fpdfFile = $fpdfPath."/fpdf.php";
define('FPDF_FONTPATH',$fontPath);
require_once("dbconfig.php");
require($fpdfFile);
class PDF_MC_Table extends FPDF
{
var $widths;
function PDF_MC_Table(){
	$this->FPDF();
}
function SetWidths($w)
{
    //Set the array of column widths
    $this->widths=$w;
}

function Row($data,$aln,$optFill,$style)
{
    //Calculate the height of the row
    $nb=0;
    for($i=0;$i<count($data);$i++)
        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
    $h=5*$nb;
    //Issue a page break first if needed
    $this->CheckPageBreak($h);
    //Draw the cells of the row
    for($i=0;$i<count($data);$i++)
    {
        $w=$this->widths[$i];
        //Save the current position
        $x=$this->GetX();
        $y=$this->GetY();
        //Draw the border
		//$col = $pdf->GetFillColor();
		if ($optFill == ''){
			$this->SetFillColor(255,255,255);
	        $this->Rect($x,$y,$w,$h,'F');
		}
		else
        	$this->Rect($x,$y,$w,$h);
        //Print the text
     	$this->SetFont('Arial',$style,10);
	    $this->MultiCell($w,5,$data[$i]." ".$col,0,$aln);
        //Put the position to the right of the cell
        $this->SetXY($x+$w,$y);
    }
    //Go to the next line
    $this->Ln($h);
}

function CheckPageBreak($h)
{
    //If the height h would cause an overflow, add a new page immediately
    if($this->GetY()+$h>$this->PageBreakTrigger)
        $this->AddPage($this->CurOrientation);
}
function PutLineBreak(){
	//put a line break 
    $this->Ln(3);
}
function NbLines($w,$txt)
{
    //Computes the number of lines a MultiCell of width w will take
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);
    if($nb>0 and $s[$nb-1]=="\n")
        $nb--;
    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $nl=1;
    while($i<$nb)
    {
        $c=$s[$i];
        if($c=="\n")
        {
            $i++;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
            continue;
        }
        if($c==' ')
            $sep=$i;
        $l+=$cw[$c];
        if($l>$wmax)
        {
            if($sep==-1)
            {
                if($i==$j)
                    $i++;
            }
            else
                $i=$sep+1;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
        }
        else
            $i++;
    }
    return $nl;
}
function Header(){
	$this->Image('images/logo.png',10,8,200,20);
	$this->SetFont('Arial','B',15);
	$this->Cell(80);
	$this->Ln(20);
}
}

$offset = $_REQUEST["hOffset"];
$pagecount = $_REQUEST["hPageCount"];
$page = $_REQUEST["hPage"];
$each_link = $_REQUEST["hEachLink"];
$link_per_page = $_REQUEST["hLinkPerPage"];
$index = $_REQUEST["hIndex"];
$diff = $_REQUEST["hDiff"];
$centre_code = $_REQUEST["sCentreCode"];	
if ($each_link <= $diff)
	$act_diff = $each_link;
else
	$act_diff = $diff;
if ($index == 0)
	$displacement = 0;
else
	$displacement = $index * $each_link;
$sql_iway_can = "select distinct(a.centre_code) from iib_iway_details a,iib_ta_iway b where exam_centre_code = '$centre_code' and a.centre_code = b.centre_code   order by (a.centre_code) limit $displacement,$act_diff";
//$centre_list_array = "'700073A','520007B','520010C','625010A','625020B','305001G','305001H','305001F','302023A','302020B','302021B','160047A','160054A','160060B','160062A','160062B','226003A','226001H','226019A','641038B','641041A','641002A','636016B','636004A','636007B','620018B','620006A','620001C'";
//$sql_iway_can = "select distinct(a.centre_code) from iib_iway_details a,iib_ta_iway b where a.centre_code in ($centre_list_array) and a.centre_code = b.centre_code order by (a.centre_code)";
//echo $sql_iway_can;
$res_iway_can=mysql_query($sql_iway_can);
$sql_exam_centre = "select exam_centre_name from iib_exam_centres where exam_centre_code = $centre_code";
$res_exam_centre = mysql_query($sql_exam_centre);
list($exam_centre_code)=mysql_fetch_row($res_exam_centre);
//$exam_centre_code = "back_up_cafe";
$num_can = @mysql_num_rows($res_iway_can);
if ($num_can < 1)
	exit;
$j=0;
while ($j < $num_can){
list($cenid[$j])= mysql_fetch_row($res_iway_can);
$j++;
}
//$cenid=array('400039A');
$st_cenid = $cenid[0];
$end_cenid = $cenid[--$j];
/*$cenlist = implode('\',\'',$cenid);
$cenlist = '\''.$cenlist.'\'';
$num_of_centrs= mysql_num_rows($res_can); */
$pdf=new PDF_MC_Table();
$pdf->Open();
$pdf->SetFont('Arial','',10);
$strFileName = 'TA_'.$exam_centre_code.'_'.$st_cenid.'_'.$end_cenid.'.pdf';  
$i=0;
while ($cenid[$i]!=""){
	$sql_can ="select iway_name,iway_address1,iway_address2,iway_city,iway_state,iway_pin_code,ta_name,c.ta_login from iib_iway_details a,iib_ta_iway b, iib_ta_details c where a.centre_code = '$cenid[$i]' and b.centre_code=a.centre_code and c.ta_login=b.ta_login";
	$res_can = mysql_query($sql_can);
	list($iway_name,$iway_address1,$iway_address2,$iway_city,$iway_state,$iway_pin_code,$ta_name,$ta_login)=mysql_fetch_row($res_can);
	$Address = "";
	if ($iway_address1 != "")
		$Address .= $iway_address1;
	if (($Address != "") && ($iway_address1 != ""))
		$Address .= " ";
	if ($iway_address2 != "")
		$Address .= $iway_address2;
	if (($Address != "") && ($iway_address2 != ""))
		$Address .= " ";
	if ($iway_city != "")
		$Address .= $iway_city;
	if (($Address != "") && ($iway_city != ""))
		$Address .= " ";
	if ($iway_pin_code != "")
		$Address .= $iway_pin_code;
	$sqlPass = "SELECT login_password FROM iib_ta_password WHERE ta_login='$ta_login' ";
	$resPass = @mysql_query($sqlPass);
	list($ta_password) = @mysql_fetch_row($resPass);
	$pdf->AddPage();
	$pdf->PutLineBreak();
	$pdf->SetWidths(array(150));
	$pdf->Row(array("TA Card For Online Examination"),'C','','B');
	$pdf->PutLineBreak();
	/*
	$pdf->SetWidths(array(70,10,70));
	$pdf->Row(array("TA Name"," : ",$ta_name),'L','','');
	$pdf->PutLineBreak();*/
	$pdf->SetWidths(array(70,10,70));
	$pdf->Row(array("iWay Name"," : ",$iway_name),'L','','');
	$pdf->PutLineBreak();
	$pdf->SetWidths(array(70,10,70));
	$pdf->Row(array("iWay Address"," : ",$Address),'L','','');
	$pdf->PutLineBreak();
	$pdf->SetWidths(array(150));
	$pdf->Row(array("Instructions for TA"),'L','','B');	
	$pdf->PutLineBreak();
	$data="The TA should open the cafe by 8:00 A.M on the day of the examination.";
	$pdf->SetWidths(array(190));
	$pdf->Row(array($data),'J','','');	
	$pdf->PutLineBreak();
	$data="The cafe should be sealed and no person other than the eligible candidates should be allowed to enter.";
	$pdf->SetWidths(array(190));
	$pdf->Row(array($data),'J','','');	
	$pdf->PutLineBreak();
	$data="Before each shift the TA should be ready with the attendance sheet. The TA will have to get signatures of the candidates on attendance sheets.";
	$pdf->SetWidths(array(190));
	$pdf->Row(array($data),'J','','');	
	$pdf->PutLineBreak();
	$data="The TA should not leave the cafe at any point during the conduct of the exam.";
	$pdf->SetWidths(array(190));
	$pdf->Row(array($data),'J','','');	
	$pdf->PutLineBreak();
	$data="TA should ensure that no candidates keeps books, Mobile phone etc. with him / her in the exam hall or use mathematical / log tables etc., during the exam.".
	$pdf->SetWidths(array(190));
	$pdf->Row(array($data),'J','','');	
	$pdf->PutLineBreak();
	$pdf->PutLineBreak();
	$pdf->SetWidths(array(150));
	$pdf->Row(array("TA to give following Instructions to the Candidates"),'L','','B');	
	$pdf->PutLineBreak();
	$data="Please note that this is \"ON-LINE\" Exam and the candidate has to reply the answer by mouse-clicking the correct choice.";
	$pdf->SetWidths(array(190));
	$pdf->Row(array($data),'J','','');	
	$pdf->PutLineBreak();
	$data="The candidates would be allowed to undergo \"Sample Test\" before they start the actual exam. This is to familiarize the candidates for the On-line Exam. The result will be generated immediately after the closure of the Exam.";
	$pdf->SetWidths(array(190));
	$pdf->Row(array($data),'J','','');	
	$pdf->PutLineBreak();
	$data="TA will brief the candidates about the process of on-line exam and will also announce the following";
	$pdf->SetWidths(array(190));
	$pdf->Row(array($data),'J','','');	
	$pdf->PutLineBreak();
	$data="* 	Not to click on the Browser BACK button during the exam.";
	$pdf->SetWidths(array(20,150));
	$pdf->Row(array(" ",$data),'J','','');	
	$data="* 	The Candidate will not be able to re-appear for the Sample Test.";
	$pdf->SetWidths(array(20,150));
	$pdf->Row(array(" ",$data),'J','','');	
	$pdf->PutLineBreak();
	$data="Candidates to be informed that any queries during actual exam will not be entertained, however in exceptional cases necessary clarifications may be given to the candidates.";
	$pdf->SetWidths(array(190));
	$pdf->Row(array($data),'J','','');	
	$pdf->PutLineBreak();
	$data="Announcement is required to be made by the TA in relation to the process of negative marking. In this regard inform the candidates that they need not answer a particular question in case they are not sure about the answer. Specific instructions are to be given to the candidates as to how they can correct / cancel the answer wrongly marked.";
	$pdf->SetWidths(array(190));
	$pdf->Row(array($data),'J','','');	
	$pdf->PutLineBreak();
	$data="It is possible that some candidates are not familiar with computers and mouse clicking , In such cases the TA may identify such candidates and explain to them the log-in procedure as well as the technique of 'Mouse-clicking'.";
	$pdf->SetWidths(array(190));
	$pdf->Row(array($data),'J','','');	
	$pdf->PutLineBreak();
	$pdf->PutLineBreak();	
	$pdf->SetWidths(array(70,10,70));
	$pdf->Row(array("Site URL", " : ","www.sifyitest.com"),'L','','');	
	$pdf->PutLineBreak();
	$pdf->SetWidths(array(70,10,70));
	$pdf->Row(array("TA Login", " : ",$ta_login),'L','','');	
	$pdf->PutLineBreak();
	$pdf->SetWidths(array(70,10,70));
	$pdf->Row(array("Password", " : ",$ta_password),'L','','');	
	$pdf->PutLineBreak();
	$i++;
}		
$pdf->Output($strFileName,true);
ob_end_flush(); 
?>
