<?
/****************************************************
* Application Name            :  IIB ADMIN
* Module Name                 :  Bulk Centre Change
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  iib_candidate_iway,iib_iway_vacancy,iib_centre_change_logs,iib_time_change_logs
* Tables used for only selects:  iib_exam_centres,iib_iway_details,iib_exam_schedule,iib_exam_slots 
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  B.Devi
* Created ON                  :  21-03-2006
* Last Modified By            :  B.Devi
* Last Modified Date          :  21-03-2006
* Description                 :  Move the Candidate to Other Centre in Bulk
*****************************************************/

include "dbconfig.php";
require_once "sessionchk.php";
$examCentreCode = isset($_POST['sExam_centre_code'])?$_POST['sExam_centre_code']:0;
$centreCode = isset($_POST['sCentre_code'])?$_POST['sCentre_code']:0;
$examTime=isset($_POST['sExam_time'])?$_POST['sExam_time']:0;
$examDate=isset($_POST['sExam_date'])?$_POST['sExam_date']:0;
$GET_disp_can=isset($_GET['disp_can'])?$_GET['disp_can']:0;
$moveExamCentreCode = isset($_POST['sMove_exam_centre_code'])?$_POST['sMove_exam_centre_code']:0;
$moveCentreCode = isset($_POST['sMove_centre_code'])?$_POST['sMove_centre_code']:0;
$moveExam_time=isset($_POST['smoveExam_time'])?$_POST['smoveExam_time']:0;
$sReason=isset($_POST['sReason'])?$_POST['sReason']:0;
$GET_move=isset($_GET['move'])?$_GET['move']:0;
$mem_list=isset($_POST['chk'])?$_POST['chk']:array(0);



//$mem_list will have all  checked membership_no with subject code
if(!is_array($mem_list))
{
	$mem_list1[0]=$mem_list;
	$mem_list='';
	$mem_list[0]=$mem_list1;
	//print_r($mem_list);
}
$reason = $_REQUEST["hReason"];

//Checked members are in $mem_list
//print_r($mem_list);
if ($GET_move == 1){

//Select the List of Candidates related to the selected details in Combo : Source Iway Details Details
 	$sql_move_candidate = "select membership_no,exam_code,subject_code from iib_candidate_iway where centre_code = '$centreCode' and exam_date = '$examDate' and exam_time = '$examTime'";
	$res_move_candidate = mysql_query($sql_move_candidate) or die ("Error in Selecting Re
	cords from Candidate Iway".mysql_error());
	
	//Code added for checking seats in iib_iway_vacancy
	$sql_vacancy= "select remaining "
						   ." from iib_iway_vacancy "
						   ." where centre_code = '$moveCentreCode' "
						   ." and exam_date='$examDate'and exam_time='$moveExam_time'";					   
	
	$res_vacancy = mysql_query($sql_vacancy) or die ("Error in Selecting Records from Vacancy ".mysql_error());
	
	list($remaining)=mysql_fetch_row($res_vacancy);		
	
	while(list($move_mem_no, $move_exam_code,$move_subject_code)=mysql_fetch_row($res_move_candidate)){			
				$str_to_chk = $move_mem_no."_".$move_subject_code;	
					
		//Check for Remaining Seats in Destination Iway 
			if(trim($remaining)>0){
				if (in_array($str_to_chk,$mem_list)){							
					
					/*If the candidate is already mapped to same Date and  Time then cancel Update
					Candidate should be allowed to transfer from source to destination(destination may be source)
					with different Time
					*/
					
					$sql_time_check = "select count(*) "
												  ."  from iib_candidate_iway "
												  ." where membership_no='$move_mem_no' "							  
												  ." and exam_date='$examDate' "
												  ." and exam_time='$moveExam_time' "											  
												  ." and subject_code!='$move_subject_code'";						
											  
					$res_time_check = mysql_query($sql_time_check)  or die ("Error in Selecting Records from Candidate Iway ".mysql_error());					
																	  
					list($nTimeCount) = mysql_fetch_row($res_time_check);																								

					if($nTimeCount==0){
					//Update IIB Candidate Iway Table			
					$sql_update_candidate = "update iib_candidate_iway "
														 ." set centre_code = '$moveCentreCode' , "
														 ." exam_time='$moveExam_time'  "												
														 ." where membership_no = '$move_mem_no' "
														 ." and exam_code = '$move_exam_code'  "
														 ." and subject_code = '$move_subject_code' "
														 ." and exam_time='$examTime' "
														 ." and exam_date = '$examDate' "
														 ." and centre_code='$centreCode'";	
			
				 	$res_update_candidate = mysql_query($sql_update_candidate)  or die ("Error in Update of Candidate Iway".mysql_error());
				 	
					if($res_update_candidate){
						$remaining--;
						//when candidateiway is updated log is inserted						
						$sql_log = "insert into iib_centre_change_logs ("
															." row_id , "
															." membership_no , "
															." exam_code , "
															." subject_code , "
															." exam_date , "
															." exam_time , "
															." old_centre_code , "
															." new_centre_code , "
															." reason , "
															." modified_on) "										
															."  values ( "
															." '' , "
															." '$move_mem_no' , "
															." '$move_exam_code' , "
															." '$move_subject_code' , "
															." '$examDate' , "
															." '$examTime' , "
															." '$centreCode' , "
															." '$moveCentreCode' , "
															." '$reason', "
															." now() )";
																				
						$sql_insert = "insert into iib_time_change_logs ( "
															." membership_no , "
															." exam_code , "
															." subject_code , "
															." exam_date , "
															." centre_code , "
															." old_exam_time , "
															." new_exam_time , "
															." reason , "
															." modified_on ) "
															." values ( "
															." '$move_mem_no' , "
															." '$move_exam_code' , "
															." '$move_subject_code' , "
															." '$examDate' , "
															." '$centreCode' , "
															." '$examTime' , "
															." '$moveExam_time' , "
															." '$reason' , "
															." now() ) ";												 
						mysql_query($sql_log) or die("Insert centre change change log failed".mysql_error());
						mysql_query($sql_insert) or die("Insert time change log failed".mysql_error());
													
						//Updating Destination Iways iib_iway_vacancy Table
						$sql_update_mvvacancy= " update iib_iway_vacancy "
														." set filled=filled+1 , "
														." remaining=if((remaining-1)<0,0,remaining-1) "
														." where centre_code='$moveCentreCode' "
														." and exam_date='$examDate' "
														." and exam_time ='$moveExam_time'";												
						$res_update_mvvacancy = @mysql_query($sql_update_mvvacancy) or die ("Error in Updating Destination Centre Vacancy ".mysql_error());
																				
						
						//Updating Source Iways iib_iway_vacancy Table
						$sql_update_vacancy= " update iib_iway_vacancy "
														." set filled=if((filled-1)<0,0,filled-1) , "
														." remaining=remaining+1 "
														." where centre_code='$centreCode' "
														." and exam_date='$examDate' "
														." and exam_time ='$examTime'";																						
						
						$res_update_vacancy = @mysql_query($sql_update_vacancy) or die ("Error in Updating Source Centre Vacancy ".mysql_error());
																				
					}//end of if : $res_update_candidate
					else{
						die("Error in Updating Records From Candidate Iway".mysql_error());
					}
																
				}//TimeCheck
				else{
					$Time_member[]=$move_mem_no;
				}
				}//is_array check
				
			}//$remanining		
			}//end of while
				
	}//end of if: $GET_move


$TIMEPBM_MEMNO = implode(",", $Time_member);


$sqlIWay = "SELECT centre_code, iway_name FROM iib_iway_details WHERE exam_centre_code='$examCentreCode' order by centre_code ";
$resIWay = @mysql_query($sqlIWay)  or die ("Error in Selecting Records from Iway Details".mysql_error());
$nIWay = mysql_num_rows($resIWay);

$sql_exam_centre="select exam_centre_code, exam_centre_name from iib_exam_centres where online='Y' order by exam_centre_name";
//echo $sql_exam_centre;
$res_exam_centre=mysql_query($sql_exam_centre)or die ("Error in Selecting Records from Exam Centre ".mysql_error());

$sqlDate = "SELECT distinct exam_date FROM iib_exam_schedule order by exam_date ";
$resDate = mysql_query($sqlDate) or die ("Error in Selecting Records from Exam Schedule ".mysql_error());
$sqlTime ="SELECT slot_time FROM iib_exam_slots order by slot_time";
$resTime = mysql_query($sqlTime) or die ("Error in Selecting Records from Exam Slots ".mysql_error());
$nTime = mysql_num_rows($resTime);

if ($nTime > 0){
	while (list($eTime) = mysql_fetch_row($resTime)){
		$aDispTime[$eTime] = substr($eTime,0,5);
	}//end of while
}//end of if


?>
<html>
<head>
<script language='JavaScript' src="./includes/validations.js"></script>
<script language='JavaScript'>

function getCentres()
{
        document.move_iway_preexam.action = "move_iway_preexam.php?disp_can=0&move=0";
        document.move_iway_preexam.submit();
}
function getIwayDetails()
{
        document.move_iway_preexam.action = "move_iway_preexam.php?disp_can=0&move=0";
	document.move_iway_preexam.submit();
}
function moveGetCentres()
{
        document.move_iway_preexam.action = "move_iway_preexam.php?disp_can=1&move=0";
        document.move_iway_preexam.submit();
}
function moveGetIwayDetails()
{	
        document.move_iway_preexam.action = "move_iway_preexam.php?disp_can=1&move=0";
        document.move_iway_preexam.submit();
}

function moveGetRemaining()
{		
        document.move_iway_preexam.action = "move_iway_preexam.php?disp_can=1&move=0";
        document.move_iway_preexam.submit();
}


function chkothers()
{
	var frm = document.move_iway_preexam; 
	if(frm.sReason.value == "Others"){
		frm.tOthers.disabled = false;
	}
	else{
		frm.tOthers.disabled = true;
	}
}

function SetChecked(chkName)
{
        var val = document.move_iway_preexam.chkall.checked;
        dml=document.move_iway_preexam;
        len = dml.elements.length;
        var i=0;
        for( i=0 ; i<len ; i++)
        {
                if (dml.elements[i].name==chkName)
                {
                        dml.elements[i].checked=val;
                }
        }
}

function GetCandidate(){
	frm=document.move_iway_preexam;
	if (frm.sExam_centre_code.selectedIndex == 0){
		alert("Please select a Exam centre");
		frm.sExam_centre_code.focus();
		return;
	}	
	if (frm.sCentre_code.selectedIndex == 0){
		alert("Please select an iway");
		frm.sCentre_code.focus();
		return;
	}
 	if (frm.sExam_date.selectedIndex == 0){
		alert("Please select a date");
		frm.sExam_date.focus();
		return;
	}
 	if (frm.sExam_time.selectedIndex == 0){
		alert("Please select a time");
		frm.sExam_time.focus();
		return;
	}
	
	document.move_iway_preexam.action = "move_iway_preexam.php?disp_can=1&move=0";
	document.move_iway_preexam.submit();
}
function MoveCandidate(){
        var frm=document.move_iway_preexam;
        len =frm.chk.length;
        var i=0;
        var cnt=false;
		var candidate_count = 0;
        if (len > '1')
        {
	        for( i=0 ; i<len; i++)
	        {
				    if (frm.chk[i].checked)
	                {
		                cnt=true;
		                candidate_count++;
	                }

	     	}
 		}
 		else
 		{
	 		if (frm.chk.checked)
	        {
		    	cnt=true;
				candidate_count++;
	        }
 		}
	            if (!cnt)
                {
	               alert ("Please Select candidate(s) to unassign");
	               return false;
                }


	if (frm.sMove_exam_centre_code.selectedIndex == 0){
		alert("Please select Exam centre code ");
		frm.sMove_exam_centre_code.focus();
		return;
	}
	
	
	if (frm.sMove_centre_code.selectedIndex == 0){
		alert("Please select Centre code");
		frm.sMove_centre_code.focus();
		return;
	}
	
 	if (frm.smoveExam_time.selectedIndex == 0){
		alert("Please select a time");
		frm.smoveExam_time.focus();
		return;
	}	
	if((frm.sExam_centre_code.value== frm.sMove_exam_centre_code.value) && (frm.sCentre_code.value== frm.sMove_centre_code.value)&& (frm.sExam_time.value== frm.smoveExam_time.value))
	{
				alert ("Source and Destination are with same Details.");
				frm.sReason.focus();
				return;
	}
        if (frm.sReason.selectedIndex == 0){
				alert ("Reason cannot be empty");
				frm.sReason.focus();
				return;
		}

if ((frm.sReason[frm.sReason.selectedIndex].value == "Others") && (trim(frm.tOthers.value) == "")){
	alert ("Please mention the reason in others");
	frm.sReason.focus();
	return;
}
if (trim(frm.tOthers.value) == "")
	{
		frm.hReason.value=frm["sReason"][frm["sReason"].selectedIndex].value;
	}
	else
	{
		if ((frm["sReason"].selectedIndex > 0) && (frm["sReason"].selectedIndex < 4))
		{
			frm.hReason.value = 	frm["sReason"][frm["sReason"].selectedIndex].value;
		}
		if (frm.hReason.value == "")
		{
			frm.hReason.value=frm.tOthers.value;
		}
		else
		{
			frm.hReason.value=frm.hReason.value+", "+frm.tOthers.value;
		}
	}
	
	if(confirm("Candidates selected = "+candidate_count+" and the remaning seats in the destination = "+frm.hRemaining.value+".\nAre You sure to submit?"))
	{
        document.move_iway_preexam.action = "move_iway_preexam.php?disp_can=1&move=1";
        document.move_iway_preexam.submit();
	}
	else
	{
		return;
	}
}
</script>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
</STYLE>
</head>
<body leftMargin=0 topMargin=0 marginheight=0 marginwidth=0>
<center>
<form name=move_iway_preexam method=post>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
<!--	<tr>
               <TD width=780 colspan=2><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
        </TR> -->
		<tr><td width="780"><?include("includes/header.php");?></td></tr>
        <TR>

        </TR>
         <TABLE border=0 cellPadding=0 cellSpacing=0 width=780 background=images/tile.jpg>
        <tr>
                <Td class="greybluetext10" colspan=2><? include("admin_menu.php") ?></Td>
        </tr>
                <TR>
        <TD  width=780 colspan=2>&nbsp;</TD>       
        </TR>
         <?if(isset($TIMEPBM_MEMNO)){?>
       <tr><td align="center" colspan=2 class="greybluetext10"><font color=blue><b> Membership Numbers <?print $TIMEPBM_MEMNO;?> are already mapped to the selected Time</b></font></td></tr>
       <?}?>
	<tr>	
		<td colspan=2 align="center" colspan=2><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2>Bulk Centre Change</b></font></td>
		
		<td></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	
	<tr><td align="center" width=700 colspan=2><FONT face="sans-serif" size=1><b>Source Details  </b></font></td></tr>
	<tr><td>&nbsp;</td></tr>
        <tr>
        	<td align="right" width=390 class="greybluetext10" valign=top>Select Exam Centre :&nbsp;</td>
                <td align="left" valign=top>
                <select class="greybluetext10" name="sExam_centre_code" style={width:250px} onChange='javascript:getCentres()'>
               		<option value=''>-Select-</option>
                       	<?
                       	while(list($exam_centre_code,$exam_centre_name) = mysql_fetch_row($res_exam_centre)){
                       		$exam_centre_name=stripslashes($exam_centre_name);
                                if ($exam_centre_code == $examCentreCode){
                                	echo "<option value=\"$exam_centre_code\" selected> $exam_centre_name</option>";
                               }
                               else{
                                  echo "<option value=\"$exam_centre_code\">$exam_centre_name</option>";
                               }
                        }//end of while
                        ?>
		</select>
 		</td>
	</tr>
	<tr>
        	<td align="right" width=40% class="greybluetext10" valign=top>Select I Way :&nbsp;</td>
                <td align="left" valign=top>
                <select class="greybluetext10" name="sCentre_code" style={width:250px} onChange='javascript:getIwayDetails()'>
                	<option value='0'>-Select-</option>
                        <?php
                        if ($nIWay > 0){
                        	while (list($centre_code, $iway_name) = mysql_fetch_row($resIWay)){
	                                if ($centre_code == $centreCode){
						echo "<option value=\"$centre_code\" selected>$centre_code - $iway_name</option>";
                                        }
                               		else{
                                        	echo "<option value=\"$centre_code\">$centre_code - $iway_name</option>";
                                        }
                        	}//end of while
                       	}//end of if: $nIWay
                	?>
		</td>
	</tr>
	<tr>
                <td align=right class="greybluetext10">Exam Date : </td>
	        <td class="greybluetext10"><select name='sExam_date' style={width:150px} class=greybluetext10>
        	        <option value='0'>--Select--</option>
			<?php
			if (mysql_num_rows($resDate) > 0){
			        while (list($eDate) = mysql_fetch_row($resDate)){
			                $aDate = explode("-",$eDate);
			                $dispDate = $aDate[2]."-".$aDate[1]."-".$aDate[0];
			                print("<option value='$eDate' ");
			                if ($eDate == $examDate)
			                        print(" selected ");
			                print(">$dispDate</option>");
				}//end of while
			}
			?>
                </td>
        </tr>
		<tr>
        	<td align="right" width=390 class="greybluetext10" valign=top>Exam Time : </td>
                <td align="left" valign=top class="greybluetext10">
                        <select name='sExam_time' class="greybluetext10" style={width:150px} >
                                <option value='0'>Select</option>
                                <?php
                                        foreach ($aDispTime as $key=>$val)
                                        {
                                                print("<option value='$key' ");
                                                if ($examTime == $key)
                                                {
                                                        print(" selected ");
                                                }
                                                print(">$val</option>");
                                        }//end of foreach
                                ?>
                        </select>
                </td>
	</tr>
	<tr><td colspan=2>&nbsp;</td></tr>
	<tr>
		<td colspan=2 align="center"><input type=button name='sub_assign' value='Submit' class='button' onClick='javascript:GetCandidate()'></td>
	</tr>
	<tr><td colspan=2><br></td></tr>
	<?
	if ($GET_disp_can == '1'){
		//Check if no candidates are mapped
		$sql_count="select count(1) from iib_candidate_iway where centre_code = '$centreCode' and exam_date = '$examDate' and exam_time = '$examTime'";
		$res_count=mysql_query($sql_count);
		list($count)=mysql_fetch_row($res_count);
		if ($count == 0 and $msg !=1){?>
			<tr>
                <td colspan=2 align="center" > <font class =\"alertmsg\" color=blue> No Candidates Mapped </font> </td>
        	</tr>
			<?}
		else if($count > 0){
			?>
			<input type=hidden name=count value='<?=$count?>'><?
		echo "<tr><td colspan=2><TABLE border=1 cellPadding=0 cellSpacing=0 width=500 background=images/tile.jpgi align=center>";		
		if ($_POST['chkall']== 'on')
			$chk = 'checked';
		echo "<tr class=greybluetext10 style=font-weight:bold><td>S. No.</td><td><input type=checkbox name=chkall onclick=javascript:SetChecked('chk[]') /*$chk*/></td><td> Membership No</td><td> Exam Code</td><td> Subject Code </td></tr>";
		$sql_candidate = "select membership_no,exam_code,subject_code from iib_candidate_iway where centre_code = '$centreCode' and exam_date = '$examDate' and exam_time = '$examTime'";
		$res_candidate = mysql_query($sql_candidate);
		$count_mem=0;
		while(list($mem_no,$exam_code,$subject_code) = mysql_fetch_row($res_candidate)){
			//Check whether the candidate has taken test
			$sql_scores = "select count(1) from iib_candidate_scores where membership_no = '$mem_no' and exam_code = '$exam_code' and subject_code='$subject_code'";
			$res_scores = mysql_query($sql_scores);
			list($count_scores)=mysql_fetch_row($res_scores);
			$count_mem++;
			$str_to_chk = $mem_no."_".$subject_code;
			$checked = '';
			if (in_array($str_to_chk,$mem_list))
				$checked = " checked ";
			$check_box='';
			if ($count_scores == 0)
                               	$check_box = "<input type=checkbox id=chk name=chk[] value=".$mem_no."_".$subject_code." $checked>";
				echo "<tr class=greybluetext10><td>$count_mem</td><td width=10>$check_box</td><td width=400>$mem_no</td><td width=100>$exam_code</td><td width=100>$subject_code</td></tr>";
			}
		echo "</table></td></tr>";
		
		/*This is done to make sure that while changing the centre he should not be mapped to some other date, since the exam is not conducted on that date.		 
		*/
		//Fetch Centre Code based on source Iways Date.
		$sql_centre="select distinct centre_code from iib_iway_vacancy where exam_date='$examDate'";
		$res_centre = @mysql_query($sql_centre) or die ("Error in Selecting Records from Vacancy ".mysql_error());
		while(list($LCentreCode) = mysql_fetch_row($res_centre)){
			$acentreCode .= "'".$LCentreCode."',";			
			
		}
		
		$acentreCode = substr($acentreCode , 0 ,-1);
		
		//Fetch the exam_centre_code based on Source Iway Date
		echo $sql_examcentre="select distinct exam_centre_code from iib_iway_details where centre_code in ($acentreCode)";		
		$res_examcentre = @mysql_query($sql_examcentre) or die ("Error in Selecting Records from Iway Details ".mysql_error());
		while(list($LExamCentre) = mysql_fetch_row($res_examcentre)){
			$aexamCentre .= "'".$LExamCentre."',";
		}
		$aexamCentre = substr($aexamCentre , 0 ,-1);		
		
		//Fetch Iway based on source Iway Date		
		$sql_move_IWay = "SELECT centre_code, iway_name FROM iib_iway_details WHERE centre_code in($acentreCode) and exam_centre_code='$moveExamCentreCode' order by centre_code ";		
		$res_move_IWay = @mysql_query($sql_move_IWay) or die ("Error in Selecting Records from Iway Details ".mysql_error());		
		$nIWay = mysql_num_rows($res_move_IWay);
		
		//Fetch Exam Centres based on source Iways Date
		//$sql_move_exam_centre="select exam_centre_code, exam_centre_name from iib_exam_centres where online='Y' order by exam_centre_name";
		$sql_move_exam_centre="select exam_centre_code, exam_centre_name from iib_exam_centres where online='Y' and exam_centre_code in ($aexamCentre) order by exam_centre_name";		
		$res_move_exam_centre=mysql_query($sql_move_exam_centre) or die ("Error in Selecting Records from Exam Centres ".mysql_error());				
		
		//Fetching Date,Remaining Seats Based on Centre Code,Iway
		$sql_remaining="SELECT remaining "
							." from iib_iway_vacancy "
							." where centre_code='$moveCentreCode' "
							." and exam_date='$examDate' and exam_time='$moveExam_time'";
		$res_remaining = mysql_query($sql_remaining) or die ("Error in Selecting Records from Iway Vacancy ".mysql_error());
		list($remaining_seats)=mysql_fetch_row($res_remaining);				
		
		
	?>
        </tr>
        <tr><td>&nbsp;</td></tr>
		<tr><td align=right width=450 colspan=2><FONT face=sans-serif size=1 ><b>Destination Details</b></font></td></tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
                <td align="right" width=390 class="greybluetext10" valign=top>Select Exam Centre :&nbsp;</td>
                <td align="left" valign=top>
                <select class="greybluetext10" name="sMove_exam_centre_code" style={width:250px} onChange='javascript:moveGetCentres()'>
                        <option value=''>-Select-</option>
                        <?
                        while(list($exam_centre_code,$exam_centre_name) = mysql_fetch_row($res_move_exam_centre)){
                                $exam_centre_name=stripslashes($exam_centre_name);
                                if ($exam_centre_code == $moveExamCentreCode){
                                        echo "<option value=\"$exam_centre_code\" selected>$exam_centre_name</option>";
                               }
                               else{
                                  echo "<option value=\"$exam_centre_code\">$exam_centre_name</option>";
                               }
                        }//end of while
                        ?>
                </select>
                </td>
        </tr>
		
        <tr>
                <td align="right" width=40% class="greybluetext10" valign=top>Select I Way :&nbsp;</td>
                <td align="left" valign=top>
                <select class="greybluetext10" name="sMove_centre_code" style={width:250px} onChange='javascript:moveGetIwayDetails()'>
                        <option value='0'>-Select-</option>
                        <?php
                        if ($nIWay > 0){
                                while (list($centre_code, $iway_name) = mysql_fetch_row($res_move_IWay)){
                                        if ($centre_code == $moveCentreCode){
                                                echo "<option value=\"$centre_code\" selected>$centre_code - $iway_name</option>";
                                        }
                                        else{
                                                echo "<option value=\"$centre_code\">$centre_code - $iway_name</option>";
                                        }
                                }//$end of while
                        }//end of if : $nIWay
                        ?>
                </td>
        </tr>
		
       <tr>
            <td align=right class="greybluetext10">Exam Date : </td>
	        <td class="greybluetext10"><b>
	        <?if ($moveExamCentreCode != 0 and $moveCentreCode != 0){
		         $ades_Date = explode("-",$examDate);
			     $destination_Date = $ades_Date[2]."-".$ades_Date[1]."-".$ades_Date[0];
	     	     echo $destination_Date;
        	}//end of if
        	else
        	echo "--";
        	?></b>
                </td>
        </tr>  
		
        <tr>
        	<td align="right" width=390 class="greybluetext10" valign=top>Exam Time : </td>
                <td align="left" valign=top class="greybluetext10">
                        <select name='smoveExam_time' class="greybluetext10" style={width:150px} onChange='javascript:moveGetRemaining()' >
                                <option value='0'>Select</option>
                                <?php
                                        foreach ($aDispTime as $key=>$val)
                                        {
                                                print("<option value='$key' ");
                                                if ($moveExam_time == $key)
                                                {
                                                        print(" selected ");
                                                }
                                                print(">$val</option>");
                                        }//foreach ends here
                                ?>
                        </select>
                </td>
	</tr>
	
	<input type=hidden name=hRemaining value=<?=$remaining_seats?>>
	 <tr>
        <td align=right class="greybluetext10">Remaining Seats : </td>
        <td class="greybluetext10"><b>
        <?if($moveExamCentreCode != "")
        echo $remaining_seats;
        else 
        echo "--";
        ?></b>
		</td>
    </tr>  
	
	<input type=hidden name=hReason>
        
        <tr class='textblk11'>
			<td align="right" width=390 class="greybluetext10" valign=top>	Reason : </td>
			<td align="left" valign=top class="greybluetext10" >
				<select name='sReason' class='greybluetext10' style='width:150;' onChange='javascript:chkothers();'>
				 <option value=0>--Select--</option>
				 <?
				 $sql_reason = "select reason from iib_time_change_reason";
				 $res_reason = mysql_query($sql_reason);
				 while(list($reason)=mysql_fetch_row($res_reason)){
					$sel="";
					if ($reason==$sReason)
					 	$sel = "selected";
					 echo "<option value='$reason' $sel>$reason</option>";
				}//end of while
				 ?>
				</select>
				<?
				
				if($sReason=="Others")
					$check_disabled = "  ";
				else
					$check_disabled = " disabled ";
										
					
						echo "<tr><td align=\"right\" width=390 class=\"greybluetext10\" valign=top>Others&nbsp;&nbsp;:</td><td <td align=\"left\" width=390 class=\"greybluetext10\" valign=top> <input type=text value='' name='tOthers' style='width:75px' class='textbox' maxlength=40 $check_disabled></td></tr>";
					

				?>
			 </td>
			</tr>
			<tr><td colspan=2>&nbsp;</td></tr>
			<?if($remaining_seats>0){?>
			  <tr>
		                <td colspan=2 align="center"><input type=button name='bMove' value='Move Candidate' class='button' onClick='javascript:MoveCandidate()'></td>
		        </tr>
		    <?}
		    else{?>
				<tr>
		                <td colspan=2 align="center"><input type=button name='bMove' value='No Seats Available' class='button'></td>
				</tr>
				
		    <?}?>
			<tr><td colspan=2>&nbsp;</td></tr>
	<?
	}
}
	?>

        <tr>
                	<?include("includes/footer.php");?>
	</tr>
</table>
</table>
</center>
</body>
</html>
