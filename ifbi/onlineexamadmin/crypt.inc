<?php
/* code got from in.php.net/mcrypt in user comments . added initially to store the
   folderlist in an encrypted format in the COOKIE. the key used here is the bx
   cookie value that is returned by checkcrypt and set initially */

function Encrypt($string, $key)
{
        $result = '';
        for($i=1; $i<=strlen($string); $i++)
        {
                $char = substr($string, $i-1, 1);
                $keychar = substr($key, ($i % strlen($key))-1, 1);
                $char = chr(ord($char)+ord($keychar));
                $result.=$char;
        }
        return $result;
}

function Decrypt($string, $key)
{
        $result = '';
        for($i=1; $i<=strlen($string); $i++)
        {
                $char = substr($string, $i-1, 1);
                $keychar = substr($key, ($i % strlen($key))-1, 1);
                $char = chr(ord($char)-ord($keychar));
                $result.=$char;
        }
        return $result;
}
?>
