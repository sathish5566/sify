<?php
/****************************************************
* Application Name            :  IIB IWAY BULKUPLOAD
* Module Name                 :  View Uploaded Data 
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects: iib_iway_vacancy,iib_iway_details 
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  Balachandar Muruganantham
* Created ON                  :  Feb 16 2005
* Last Modified By            :  B.Devi
* Last Modified Date          :  27-03-2006
* Description                 :  For viewing the Uploaded data from the database.
*****************************************************/
require_once("sessionchk.php");
require_once("dbconfig.php");
$sid = $_GET['sid'];
$eid = $_GET['eid'];
$fileid = $_GET['fileid'];

require("includes/session_handle.php");
require("includes/reader.php");
require("includes/functions.php");
include("includes/logger.php");

/** Opening the cryption handler */
//openHandler();

/** Common Debug Variable for debugging the code which prints the data */
$commondebug = false;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
.textbox        
{ 
font-family: Verdana, Arial, Helvetica, sans-serif; 
font-size: 10px; font-style: normal; 
text-decoration: none; 
height: 17px; 
border: 1px #000000 solid
}
</STYLE>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/validations.js"></script>
</head>

<BODY leftMargin=0 topMargin=0 LeftMargin=0 marginheight=0 marginwidth=0>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
<!--	<TR><!-- Topnav Image -->
      <!--  <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR> -->
     <tr><td width="780"><?include("includes/header.php");?></td></tr>
    <TR>
        
    </TR>
	<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>
    <TR>
    	<TD background=images/tile.jpg vAlign=top width=780 align=center>

<table cellspacing="0" cellpadding="0" width="100%" border="0" align="center" >

  <tr>
    <td width="5%"></td>
    <td width="90%">
      <table width="95%"  border="0" align="center" cellpadding="5" cellspacing="1">
  <tr>
    <td valign="top">&nbsp;</td>
  </tr>

  <tr>
    <td valign="top">
			<table width="95%" align="left" cellpadding="2">
        <tr>
          <td> <p class="pageTitle">View Imported Data </p> File Name: <?php echo $file_name; ?></td></tr>
        <tr>
			</table>
		</td>
  </tr>
  <tr>
    <td valign="top">
		
    <table width="100%"  border="0" cellspacing="1" cellpadding="1">
      <tr>
        <td colspan="2">
					<table width="95%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#BEBEBE">
						<tr align="center" bgcolor="#F9F9F9"><b>
							<td align="center"><b>id</b></td>
							<td align="center"><b>centre_code</b></td>
							<td align="center"><b>exam_date</b></td>
							<td align="center"><b>exam_time</b></td>
							<td align="center"><b>actual_seats</b></td>
							<td align="center"><b>no_of_seats</b></td>
							<td align="center"><b>filled</b></td>
							<td align="center"><b>remaining</b></td>
							<td align="center"><b>iway_name</b></td>							
							<td align="center"><b>status</b></td>
							<td align="center"><b>iway_address1</b></td>
							<td align="center"><b>iway_address2</b></td>
							<td align="center"><b>iway_city</b></td>
							<td align="center"><b>iway_state</b></td>
							<td align="center"><b>iway_pincode</b></td>
							<td align="center"><b>exam_centre_code</b></td>							
							
					 </tr>
						<?php
							/** Fetching the uploaded data from the database and also data from iway upload and vacancy table*/
							$sql_iway_details= "select a.id , "
													." a.centre_code , "
													." a.exam_date , "
													." a.exam_time , "
													." a.no_of_seats , "
													." a.filled , "
													." a.remaining , "
													." b.iway_name, "
													." b.status , "
													." b.iway_address1 , "
													." b.iway_address2 , "
													." b.iway_city , "
													." b.iway_state , "
													." b.iway_pin_code , "
													." b.exam_centre_code , "
													." b.actual_seats "
													." from ".TABLE_IWAY_VACANCY ." a , "
													." iib_iway_details b "
													." where a.id between $sid and $eid "
													." and a.centre_code=b.centre_code group by id";
								

							if ($commondebug){
								echo $sql_iway_details;
							}							
							
							$res_iway_details = @mysql_query($sql_iway_details);
							$nCount = @mysql_num_rows($res_iway_details);
							
							if ($commondebug){
								print $sql_iway_details;
								echo mysql_error();
							}
							if (mysql_error()){
								echo mysql_error();
								exit;	
							}
							
							if($nCount>0){
								while(list($id,$centre_code,$exam_date,$exam_time,$no_of_seats,$filled,$remaining,$iway_name,$status,$iway_address1,$iway_address2,$iway_city,$iway_state,$iway_pin_code,$exam_centre_code,$actual_seats)=mysql_fetch_row($res_iway_details)){							
									echo '<tr align="center" bgcolor="#F9F9F9">';
											echo '<td align="center">'.$id.'</td>';
											echo '<td align="center">'.$centre_code.'</td>';
											echo '<td align="center">'.$exam_date.'</td>';
											echo '<td align="center">'.$exam_time.'</td>';
											echo '<td align="center">'.$actual_seats.'</td>';
											echo '<td align="center">'.$no_of_seats.'</td>';										
											echo '<td align="center">'.$filled.'</td>';										
											echo '<td align="center">'.$remaining.'</td>';		
											echo '<td align="center">'.$iway_name.'</td>';		
											echo '<td align="center">'.$status.'</td>';		
											echo '<td align="center">'.$iway_address1.'</td>';		
											echo '<td align="center">'.$iway_address2.'</td>';		
											echo '<td align="center">'.$iway_city.'</td>';		
											echo '<td align="center">'.$iway_state.'</td>';		
											echo '<td align="center">'.$iway_pin_code.'</td>';		
											echo '<td align="center">'.$exam_centre_code.'</td>';												
									echo '</tr>';							
								}// end of while
							}//$nCount>0
							else{
								echo '<tr align="center" bgcolor="#F9F9F9"><td align="center" colspan=16><b>No Records Found</b></td></tr>';
							}
							
						?>
					</table>
				</td>
      </tr>
    </table></td>
  </tr>
</table>

</td>
    <td width="5%"  valign="top"></td>
  </tr>
</table>
 </TD>
	</TR>
    <TR>
    	<?include("includes/footer.php");?>
    </TR>
</body>
</html>
