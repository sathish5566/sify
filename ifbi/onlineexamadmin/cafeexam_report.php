<?php
/****************************************************
* Application Name            :  IIB
* Module Name                 :  Exam-cafewise report
* Revision Number             :  1
* Revision Date               :
* Table(s)                    : 
* Tables used for only selects: iib_exam_centres,iib_exam,iib_exam_schedule,iib_exam_slots,
                                iib_iway_details,iib_candidate_iway.
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              :
* Document/Reference Material :
* Created By                  :  
* Created ON                  :  
* Last Modified By            : K KARUNA MOORTHY RAJU
* Last Modified Date          : 01-04-06
* Description                 : This report displaying the cafewise report for the different examinations and it can be downloaded as a csv file.
*****************************************************/

    ob_start();
	require_once "sessionchk.php";
	require_once "constants.inc";
	require_once("dbconfig_slave_123.php");
	
/* Include file for excel download */	

	include_once("excelconfig.php");
	$sqlcentre = "SELECT exam_centre_code,upper(exam_centre_name) FROM iib_exam_centres where online='Y' ";
	$rescentre = mysql_query($sqlcentre);
	$sqlexam = "SELECT exam_code,upper(exam_name) FROM iib_exam where online='Y' ";
	$resexam = mysql_query($sqlexam);
	$exam_count = mysql_num_rows($resexam);
	$color_count = count($aColor);
	if($exam_count >  $color_count)
	{
		echo "Please ask the developers to include more colours";
		exit;
	}
	$colour = 0;
	while(list($exam_code,$exam_name) = mysql_fetch_row($resexam))
	{
		 
		$exam_array[$exam_code] = $exam_name;
		$bgcolor_array[$exam_code] = $aColor[$colour];
		$colour++;
	}
	$sqlDate = "SELECT distinct exam_date FROM iib_exam_schedule order by exam_date";
	$resDate = mysql_query($sqlDate);
	$nDates = mysql_num_rows($resDate);
 
/* Modifying the date format when the records are available  */		

	if ($nDates > 0)
	{
		while (list($eDate) = mysql_fetch_row($resDate))
		{
			$aDate = explode("-",$eDate);
			$aDispDate[$eDate] = $aDate[2]."-".$aDate[1]."-".$aDate[0];
		}
	}
	$sqlTime ="SELECT slot_time FROM iib_exam_slots order by slot_time";
	$resTime = mysql_query($sqlTime);
	$nTime = mysql_num_rows($resTime);
 
/* Modifying the time format if the records are available   */
 		
	if ($nTime > 0)
	{
		while (list($eTime) = mysql_fetch_row($resTime))
		{
			$aDispTime[$eTime] = substr($eTime,0,5);
		}
	}

	$examCentreCode = $_POST['exam_centre_code'];
	$examCode = $_POST['exam_code'];
	$examDate = $_POST['exam_date'];
	$examTime = $_POST['exam_time'];
    $submitted= $_POST['submitted'];
/*  Getting values when clicking download button */           
    $ex_flag=$_POST['exl'];
    
/* Filename of the downloaded excel file */
   if($examTime!='all')
  {
    $extime = explode(':',$examTime);
    $extime = $extime[0]."-".$extime[1];
  }
  else
    $extime = $examTime;
        
    $filename="cafeexam_report(".$examCentreCode.")_sub(".$examCode.")_Date(".$examDate.")_Time(".$extime.")";
    
/* Content header will be changing depends on the $exam code and examTime */    
    
    	if($examCode=='all')
    	{
     		if($examTime=='all')
       			 $exce_header=array("S.No","City","Cafe id","Exam Name","Exam Time","No.of candidates allocated");    
      		else
       			 $exce_header=array("S.No","City","Cafe id","Exam Name","No.of candidates allocated");         
    	}
   		else
    	{
	    	if($examTime!='all')
	    	   	$exce_header =  array("S.No","City","Cafe id","No.of candidates allocated");
	    	else
	    	    $exce_header=array("S.No","City","Cafe id","Exam Time","No.of candidates allocated");            	
    	}
        if ($examCentreCode !='')
    	   {
		    if ($examCentreCode !='all')
			  {
        		$sql_centrecode="select distinct a.centre_code from iib_iway_details a,iib_exam_centres b where a.exam_centre_code=b.exam_centre_code and b.exam_centre_code='".$examCentreCode."' and online='Y' ";
	    		$res_centrecode=mysql_query($sql_centrecode);
	    		$centre_code='';
				while(list($centrecode)=mysql_fetch_row($res_centrecode))
				{
					$centrecode = strtoupper($centrecode);
					if ($centre_code =='')
						$centre_code = "(centre_code='$centrecode'";
					else
						$centre_code .= " or centre_code='$centrecode'";
				}
			     $centre_code .= " ) ";
			}
	  }

   /* Select the centrecode,actual seats,examcode and other details depends on exam time and exam date      */    	

    if (($examCentreCode != "") && ($examCode != "" ) && ($examDate != "") && $submitted=='true')
	{
        	switch($examCentreCode)
        	{

    	        case "all":
	
        	        if($examTime=='all')
            	    {
	            	    if ($examCode=='all')
	                	{
                    //$sqlFormat = "select a.centre_code,upper(a.iway_city),a.actual_seats,b.exam_time from iib_iway_details a,iib_candidate_iway b where a.centre_code=b.centre_code and b.exam_date='".$examDate."' group by a.centre_code,a.actual_seats order by a.iway_city ";
                    //$sqlFormat = "select distinct a.centre_code,a.exam_centre_code,a.actual_seats,b.exam_time,b.exam_code from iib_iway_details a,iib_candidate_iway b where a.centre_code=b.centre_code and b.exam_date='".$examDate."'   order by a.centre_code,b.exam_time ";
                    		$sqlFormat = "select distinct centre_code,exam_time,exam_code from iib_candidate_iway  where  exam_date='".$examDate."'   order by centre_code,exam_time ";
                    		
                    	}
                		else
                		{
	                	//$sqlFormat = "select distinct a.centre_code,a.exam_centre_code,a.actual_seats,b.exam_time,b.exam_code from iib_iway_details a,iib_candidate_iway b where a.centre_code=b.centre_code and b.exam_date='".$examDate."' and b.exam_code='".$examCode."'  order by a.centre_code,b.exam_time ";
	                	$sqlFormat = "select distinct centre_code,exam_time,exam_code from iib_candidate_iway where exam_date='".$examDate."' and exam_code='".$examCode."'  order by centre_code,exam_date,exam_time ";
	                	
                    	}

                	}
               		else
                	{
	                	if ($examCode=='all')
	                	{
                    	//$sqlFormat = "select a.centre_code,upper(a.iway_city),a.actual_seats,b.exam_time from iib_iway_details a,iib_candidate_iway b where a.centre_code=b.centre_code  and b.exam_date='".$examDate."' and b.exam_time='".$examTime."' group by a.centre_code,a.actual_seats order by a.iway_city ";
                    	//$sqlFormat = "select distinct a.centre_code,a.exam_centre_code,a.actual_seats,b.exam_time,b.exam_code from iib_iway_details a,iib_candidate_iway b where a.centre_code=b.centre_code  and b.exam_date='".$examDate."' and b.exam_time='".$examTime."'  order by a.centre_code,b.exam_time  ";
                    	$sqlFormat = "select distinct centre_code,exam_time,exam_code from iib_candidate_iway  where  exam_date='".$examDate."' and exam_time='".$examTime."'  order by centre_code,exam_time  ";
                    	
                	    }
                		else
                		{
	                		//$sqlFormat = "select distinct a.centre_code,a.exam_centre_code,a.actual_seats,b.exam_time,b.exam_code from iib_iway_details a,iib_candidate_iway b where a.centre_code=b.centre_code  and b.exam_date='".$examDate."' and b.exam_time='".$examTime."' and b.exam_code='".$examCode."' order by a.centre_code,b.exam_time  ";
	                		$sqlFormat = "select distinct centre_code,exam_time,exam_code from iib_candidate_iway  where exam_date='".$examDate."' and exam_time='".$examTime."' and exam_code='".$examCode."' order by centre_code,exam_time  ";
	                		
                    	}
                	}
                break;
           default :
                if($examTime=='all')
                {
                    if ($examCode=='all')
                    {
                    //$sqlFormat = "select a.centre_code,upper(a.iway_city),a.actual_seats,exam_time from iib_iway_details a,iib_candidate_iway b where $centre_code and a.centre_code=b.centre_code and b.exam_date='".$examDate."' group by a.centre_code,a.actual_seats order by a.iway_city ";
                    //$sqlFormat = "select distinct a.centre_code,a.exam_centre_code,a.actual_seats,exam_time,b.exam_code from iib_iway_details a,iib_candidate_iway b where $centre_code and a.centre_code=b.centre_code and b.exam_date='".$examDate."' order by a.centre_code,b.exam_time ";
                    $sqlFormat = "select distinct centre_code,exam_time,exam_code from iib_candidate_iway  where $centre_code   and exam_date='".$examDate."' order by centre_code,exam_time ";
                    
                  	}
                	else
                	{
	                	//$sqlFormat = "select distinct a.centre_code,a.exam_centre_code,a.actual_seats,exam_time,b.exam_code from iib_iway_details a,iib_candidate_iway b where $centre_code and a.centre_code=b.centre_code and b.exam_date='".$examDate."' and b.exam_code='".$examCode."' order by a.centre_code,b.exam_time ";
	                	$sqlFormat = "select distinct centre_code,exam_time,exam_code from iib_candidate_iway  where $centre_code and exam_date='".$examDate."' and exam_code='".$examCode."' order by centre_code,exam_time ";
	                	
  	                }
                }
                else
                {
                    if($examCode=='all')
                    {
                    //$sqlFormat = "select a.centre_code,upper(a.iway_city),a.actual_seats,exam_time from iib_iway_details a,iib_candidate_iway b where $centre_code and a.centre_code=b.centre_code and b.exam_date='".$examDate."' and b.exam_time='".$examTime."' group by a.centre_code,a.actual_seats order by a.iway_city ";
                    //$sqlFormat = "select distinct a.centre_code,exam_centre_code,a.actual_seats,exam_time,b.exam_code from iib_iway_details a,iib_candidate_iway b where $centre_code and a.centre_code=b.centre_code and b.exam_date='".$examDate."' and b.exam_time='".$examTime."'  order by a.centre_code,b.exam_time  ";
                    $sqlFormat = "select distinct centre_code,exam_time,exam_code from iib_candidate_iway  where $centre_code and exam_date='".$examDate."' and exam_time='".$examTime."'  order by centre_code,exam_time  ";
                    
                  	}
                	else
                	{
	                	//$sqlFormat = "select distinct a.centre_code,exam_centre_code,a.actual_seats,exam_time,b.exam_code from iib_iway_details a,iib_candidate_iway b where $centre_code and a.centre_code=b.centre_code and b.exam_date='".$examDate."' and b.exam_time='".$examTime."' and b.exam_code='".$examCode."' order by a.centre_code,b.exam_time  ";
	                	$sqlFormat = "select distinct centre_code,exam_time,exam_code from iib_candidate_iway  where $centre_code and  exam_date='".$examDate."' and exam_time='".$examTime."' and exam_code='".$examCode."' order by centre_code,exam_time  ";
	                	
                   	}
                }
                break;

  }      
    	$resFormat = mysql_query($sqlFormat);
        $ncount=mysql_num_rows($resFormat);
  		$sql_city="select a.centre_code,b.exam_centre_name  from iib_iway_details a, iib_exam_centres b where a.exam_centre_code=b.exam_centre_code";
		$res_city=mysql_query($sql_city);
		
/**** Storing the city name,Examcentrecode and actual seats  into two different array  ****/		
 		while(list($Centre_Code,$cityName)=mysql_fetch_row($res_city))
		{
			$Centre_Code = strtoupper($Centre_Code);
			$cityArr[$Centre_Code]=$cityName;
		}
    	$sql_exam_centre_code="select centre_code,exam_centre_code,actual_seats from iib_iway_details";
    	$res_exam_centre_code=mysql_query($sql_exam_centre_code);
    
        while(list($centreCode,$examCentre_Code,$actualSeats)=mysql_fetch_row($res_exam_centre_code))
        {
	       $centreCode = strtoupper($centreCode); 
	       $examArr[$centreCode]=$examCentre_Code;
	       $examArr[$centreCode]=$actualSeats;
	    }
	$sqlcand="select count(1),exam_date,exam_time,exam_code,centre_code from iib_candidate_iway group by exam_date,exam_time,exam_code,centre_code ";
	$rescand=mysql_query($sqlcand);
	while(list($cnt,$ED,$ET,$EC,$CC)=mysql_fetch_row($rescand))
	{
		$cen = strtoupper($CC);
		
		$candArr[$ED][$ET][$EC][$cen]=$cnt;
	}
	
	$sql_exam="select upper(exam_name),exam_code from iib_exam order by exam_code";
    $res_exam=mysql_query($sql_exam);
	while(list($EName,$ECode)=mysql_fetch_row($res_exam))
	{
		$examsubArr[$ECode]=$EName;
	}
    $i=0;
/* Storing the content such as centre code,City,Exam time and number of candidates */
             
        while(list($centre_code,$exam_time,$exam_code)=mysql_fetch_row($resFormat)){
	        
	       $centre_code = strtoupper($centre_code);     
	       $exam_centre_code=$examArr[$centre_code];
	       $actual_seats==$examArr[$centre_code];
	       $city=$cityArr[$centre_code];
	       $city=strtoupper($city);
    		if($examTime=='all'){
	    		$alloc=$candArr[$examDate][$exam_time][$exam_code][$centre_code];
    		}
    		else if($examTime!='all'){
	    		$alloc=$candArr[$examDate][$examTime][$exam_code][$centre_code];
    		}
    		$exam_name=$examsubArr[$exam_code];
	        $exce_content[$i][0]= $i+1;
	        $exce_content[$i][1]= $city;
	        $exce_content[$i][2]= $centre_code;

/* Output is depends on the examcode,examtime,so the array content changed on conditions */
	        	          
	      		 if($examCode=='all')
	       		{
	          		$exce_content[$i][3]= $exam_name;
	          		
	         			 if($examTime=='all')
	         			{
	          				$exce_content[$i][4]= $exam_time;
	          				$exce_content[$i][5]= $exam_code;
             			}
	        			 else
	         			{
	          				$exce_content[$i][4]=$alloc;  
	          				$exce_content[$i][5]= $exam_code;
            			}
            	$exce_content[$i][5]= $alloc;
	          	$exce_content[$i][6]= $exam_code;
         		}
         		 else
          		{
	         			if($examTime=='all') 
	         			{
	          				$exce_content[$i][3]= $exam_time;
	          				$exce_content[$i][4]= $alloc;
	          				$exce_content[$i][5]= $exam_code;
             			}
	          			else
	          			{
	          				$exce_content[$i][3]= $alloc; 
	          				$exce_content[$i][4]= $exam_code;
	         
             			}
          		}
	    $i++;      
     }    
   

/* Execution of excel download function */
  if($ex_flag=='1')
   {
/*  Excel download function  */
    excelconfig($filename,$exce_header,$exce_content);
    exit;	 
   }	
}

?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='JavaScript' src="./includes/validations.js"></script>
<script language='JavaScript'>
function submitForm()
{
	if (document.frmalloc.exam_code.selectedIndex == 0)
	{
		alert("Please select the exam ");
		document.frmalloc.exam_code.focus();
		return false;
	}
	if (document.frmalloc.exam_centre_code.selectedIndex == 0)
	{
		alert("Please select the exam centre");
		document.frmalloc.exam_centre_code.focus();
		return false;
	}
	if (document.frmalloc.exam_date.selectedIndex == 0)
	{
		alert("Please select the exam date");
		document.frmalloc.exam_date.focus();
		return false;
	}
    document.frmalloc.submitted.value='true';
    document.frmalloc.exl.value='0';
	document.frmalloc.submit();
}

function excelsubmit()
{
	var frmname=document.frmalloc;
	frmname.submitted.value='true';
	frmname.exl.value='1';
	frmname.submit();
}


</script>

</HEAD>
<BODY leftMargin=0 topMargin=0 LeftMargin=0 marginheight=0 marginwidth=0 >
<center>
<form name='frmalloc' method='post'>
<input type='hidden' name='populatecenter'>
<input type='hidden' name='submitted'>
<input type=hidden name=exl value='0'>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
<!--<TABLE border=0 cellPadding=0 cellSpacing=0 width=780 height=500>-->
<!--	<TR><!-- Topnav Image -->
      <!--  <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR> -->
	<tr><td width="780"><?include("includes/header.php");?></td></tr>
    <TR>

    </TR>
	<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>
	</tr>
    <TR>
    	<TD background=images/tile.jpg vAlign=top width=780 align=center>

        <table width=780 cellpadding=0 cellspacing=5 border=0 >
        	<tr>
				<td colspan=2 align="center"><b><font face="Verdana, Arial, Helvetica, sans-serif" size=2>Exam-Cafe Wise Report</b></font><br><br></td>
            </tr>
            <!--<tr>
            	<td colspan=2 align="center" class="greybluetext10"><b><?=$msg ?></b></td>
            </tr>-->
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Exam : </td>
            	<td align="left" valign=top class="greybluetext10">
            		<select name='exam_code' class="greybluetext10" style={width:250px} <!--onchange='javascript:selcentre();'-->>
            			<option value=''>--Select--</option>
            			<?
            					if ($examCode == "all")
	            				{
		            				$selectedAll = "selected";
	            				}
            			?>
            			<option value='all' <?=$selectedAll?>>--All--</option>
            			<?

							foreach($exam_array as $code => $name)
            				{
	            				print("<option value='$code' ");
	            				if ($examCode == $code)
	            				{
		            				print(" selected ");
	            				}
	            				print(">$name</option>");
	            			}
	            		?>
            		</select>
            	</td>
            </tr>
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Exam Centre : </td>
            	<td align="left" valign=top class="greybluetext10">
            		<select name='exam_centre_code' class="greybluetext10" style={width:150px} <!--onchange='javascript:selcentre();'-->>
            			<option value=''>--Select--</option>
            			<?
            					if ($examCentreCode == "all")
	            				{
		            				$selectedAll = "selected";
	            				}
            			?>
            			<option value='all' <?=$selectedAll?>>--All--</option>
            			<?


            				while (list($code,$name) = mysql_fetch_row($rescentre))
            				{
	            				print("<option value='$code' ");
	            				if ($examCentreCode == $code)
	            				{
		            				print(" selected ");
	            				}


	            				print(">$name</option>");
	            			}
	            		?>
            		</select>
            	</td>
            </tr>
             <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Exam Date : </td>
            	<td align="left" valign=top class="greybluetext10">
            		<select name='exam_date' class="greybluetext10" style={width:150px} >
            			<option value=''>--Select--</option>
            			<?php
            				foreach ($aDispDate as $key=>$val)
            				{
	            				print("<option value='$key' ");
	            				if ($examDate == $key)
	            				{
		            				print(" selected ");
	            				}
	            				print(">$val</option>");
            				}
            			?>
            		</select>
            	</td>
            </tr>
              <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Exam Time : </td>
            	<td align="left" valign=top class="greybluetext10">
            		<select name='exam_time' class="greybluetext10" style={width:150px} >
            			<option value='all'>All Shifts</option>
            			<?php
            				foreach ($aDispTime as $key=>$val)
            				{
	            				print("<option value='$key' ");
	            				if ($examTime == $key)
	            				{
		            				print(" selected ");
	            				}
	            				print(">$val</option>");
            				}
            			?>
            		</select>
            	</td>
            </tr>
    </table>
    <table width=780 >
        <tr>
			<td colspan=2 align="center"><input type=button name='sub_assign' value='Submit' class='button' onClick='javascript:submitForm()'>

			</td>
        </tr>
<?        
/*** It checks with the flag value and it retrieving the data from the array and displaying the report in the 
     html page  **/
        if(($submitted=='true') && ($ncount < 1)){?>
       <tr>
       	<td width=780 align=center class=alertmsg>No Records to display</td>
       	</tr><?}?>
   </table> 
      <?if(($submitted=='true') && ($ncount >= 1 )){
	      
           $header_count = count($exce_header);    
           // Printing the values in the HTML page 
			
			// Printing the header values from excel header array 
			
        print("<table border=1 cellspacing=0 cellpadding=2><tr>");
		for($a=0;$a<$header_count;$a++)
		{
			print("<td class=greybluetext10><b>$exce_header[$a]</b></td>");
		}
		print("</tr>");
        print("<tr>");
/* Displaying the Cafe capacity report in the html part. Retrieving the data from the array
 	          */
           if(($examCode=='all') && ($examTime!='all'))
                 $chek=$header_count+1; 
		   else
				$chek=$header_count;
				 
	          for($j=0;$j<$ncount;$j++)
	          {
					for($k=0;$k<$header_count;$k++)
		        	{ 
			     					     
/* Background color of the ecah column is changed depends on conditions */			    
			          	$bgcolor = $bgcolor_array[$exce_content[$j][$chek]];  
			    		$fontc   ="black";
			  	  ?>
			      <td class="greybluetext10" bgcolor="<?=$bgcolor?>"><font color="<?=$fontc?>"><?=$exce_content[$j][$k]?>&nbsp;</font></td>    
			      <?
                }
                print("</tr>");
              }   
          
            
           ?></table> 
		<table width=300  cellspacing=5 border=0 align=center >
	<?
		foreach($exam_array as $disp_exam_code => $disp_exam_name)
    	{	
			?>
				<tr>
					<td colspan=2 bgcolor= <?=$bgcolor_array[$disp_exam_code]?>></td>
					<td class="greybluetext10" colspan=4><?=$disp_exam_name?></td>
				</tr>
			<?	
			} 
			?>
				<!--<tr>
				<td colspan=2 bgcolor=white></td>
				<td class="greybluetext10" colspan=4>Alloted < 90% </td>
				</tr>-->
		</table> 
	     <table>
	          <tr><td class=greybluetext10>&nbsp;</td></tr>	    
		      <tr><td class=greybluetext10 align="center" class=greybluetext10><input class='button' type='button' name='download' value='Download' onClick='javascript:excelsubmit()'></td></tr>
		      <tr><td class=greybluetext10>&nbsp;</td></tr>
		 </table>
        <? }   ?>
        </TD>
    </TR>          
    <TR>
    	<?include("includes/footer.php");?>
    </TR>
    
</TABLE>
</form>
</center>
</BODY>
</HTML>
