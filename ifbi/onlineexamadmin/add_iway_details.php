<?php
require_once "sessionchk.php";
require_once "dbconfig.php";

$examCentreCode = $_POST['exam_centre_code'];
$centreCode = $_POST['txtiwaycode'];
$iwayName= $_POST['txtiwayname'];
$nSeats = $_POST['txtseats'];
$status = $_POST['lststatus'];
$address1 = $_POST['txtiwayaddress1'];
$address2 = $_POST['txtiwayaddress2'];
$city = $_POST['txtiwaycity'];
$state = $_POST['txtiwaystate'];
$pincode = $_POST['txtiwaypincode'];

//print_r($_POST);

$nDetails = 0;
$nIWay = 0;
if ($examCentreCode != "")
{
	$sqlExists = "SELECT centre_code FROM iib_iway_details WHERE centre_code='$centreCode' ";
	$resExists = @mysql_query($sqlExists);
	if (mysql_error())
	{
		$err=1;
		$emsg="Error in selecting centre code.";
		$displaybug='true';	
	}
	$nExists = mysql_num_rows($resExists);
	if ($nExists > 0)
	{
		$err=1;
		$emsg="Centre Code already exists.";
		$examCentreCode = '';
		$centreCode = '';
		$displaybug='true';	
	}
	else
	{
		$sqlInsert = "INSERT INTO iib_iway_details (centre_code, iway_name, no_of_seats, status, iway_address1, iway_address2, ".
		"iway_city, iway_state, iway_pin_code, exam_centre_code) VALUES ('$centreCode', '$iwayName', '$nSeats', '$status', '$address1', '$address2', ".
		"'$city', '$state', '$pincode', '$examCentreCode')";
		//print $sqlInsert;
		@mysql_query($sqlInsert);
		if (mysql_error())
		{
			$err=1;
			$emsg="Error in inserting iway details.";
			$displaybug='true';	
		}
		else
		{
			$done='add';
			$msg='Record Added.';
			$examCentreCode = '';
		}
	}
}

$sql = "select exam_centre_code, exam_centre_name from iib_exam_centres where online='Y' order by exam_centre_name";
$res = @mysql_query($sql);
if (!$res)
{
	$err=1;
	$emsg="Error in selecting exam centres.";
	$displaybug='true';	
}
?>
<HTML>
<HEAD>
<script language='JavaScript' src="./includes/validations.js"></script>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='JavaScript'>
function validate()
{
	var frm = document.frmCentres;
	if (frm.exam_centre_code.selectedIndex == 0)
	{
		alert('Please select the Exam Centre');
		frm.exam_centre_code.focus();
		return false;
	}
	if (frm.txtiwaycode.value == '')
	{
		alert('Please enter the I Way Code');
		frm.txtiwaycode.focus();
		return false;
	}
	myRegExp = new RegExp("[^a-zA-Z0-9]");
   	val=frm.txtiwaycode.value;
	result=val.match(myRegExp);
	if (result)
	{
	 	alert('Allowed Characters for I Way Code are alphanumeric');
	 	frm.txtiwaycode.focus();
		return false;
	}
	if (frm.txtiwayname.value == '')
	{
		alert('Please enter the IWay Name');
		frm.txtiwayname.focus();
		return false;
	}
	myRegExp = new RegExp("[^a-zA-Z0-9 ]");
   	val=frm.txtiwayname.value;
	result=val.match(myRegExp);
	if (result)
	{
	 	alert('Allowed Characters for I Way Name are alphanumeric');
	 	frm.txtiwaycode.focus();
		return false;
	}
	if (frm.txtseats.value == '')
	{
		alert('Please enter the No. of seats available');
		frm.txtseats.focus();
		return false;
	}
	myRegExp = new RegExp("[^0-9]");
   	val=frm.txtseats.value;
	result=val.match(myRegExp);
	if (result)
	{
	 	alert('Only numbers allowed for no.of seats' );
	 	frm.txtseats.focus();
		return false;
	}
	val = trim(frm.txtiwayaddress1.value);
	if (val == '')
	{
		alert('Please enter the Address');
		frm.txtiwayaddress1.focus();
		return false;
	}
	if (val != "")
	{
		myRegExp = new RegExp("[^a-zA-Z0-9  _/&,:#\)(-.]");    
    	result=val.match(myRegExp);
    	if(result)
    	{
        	alert('Allowed Characters for Address1 - Alphabet,Numbers,./,\\,#,&,(,),-,:,.');
        	frm.txtiwayaddress1.focus();
        	return;
   		}
	}
	val = trim(frm.txtiwayaddress2.value);
	if (val != "")
	{
		myRegExp = new RegExp("[^a-zA-Z0-9  _/&,:#\)(-.]");    
    	result=val.match(myRegExp);
    	if(result)
    	{
        	alert('Allowed Characters for Address2 - Alphabet,Numbers,./,\\,#,&,(,),-,:,.');
        	frm.txtiwayaddress2.focus();
        	return;
   		}
	}
	val = trim(frm.txtiwaycity.value);
	if (val == '')
	{
		alert('Please enter the City');
		frm.txtiwaycity.focus();
		return false;
	}
	if (val != "")
	{
		myRegExp = new RegExp("[^a-zA-Z ]");    
    	result=val.match(myRegExp);
    	if(result)
    	{
        	alert('Only alphabets allowed for city');
        	frm.txtiwaycity.focus();
        	return;
   		}
	}
	val = trim(frm.txtiwaystate.value);
	if (val == '')
	{
		alert('Please enter the State');
		frm.txtiwaystate.focus();
		return false;
	}
	if (val != "")
	{
		myRegExp = new RegExp("[^a-zA-Z ]");    
    	result=val.match(myRegExp);
    	if(result)
    	{
        	alert('Only alphabets allowed for state');
        	frm.txtiwaystate.focus();
        	return;
   		}
	}
	
	if (frm.lststatus.selectedIndex == 0)
	{
		alert('Please select the Status');
		frm.lststatus.focus();
		return false;
	}
	if (frm.txtiwaypincode.value == '')
	{
		alert('Please enter the Pin Code');
		frm.txtiwaypincode.focus();
		return false;
	}	
	myRegExp = new RegExp("[^0-9]");
   	val=frm.txtiwaypincode.value;
	result=val.match(myRegExp);
	if (result)
	{
	 	alert('Only numbers allowed for PinCode');
	 	frm.txtiwaypincode.focus();
		return false;
	}
	frm.submit();
}

function submitForm()
{
    if (window.event.keyCode == 13)
    {	    	   
        validate();
    }
}
</script>
</HEAD>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0 onload='javascript:document.frmCentres.exam_centre_code.focus();' onKeyPress='javascript:submitForm()'>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
       <!-- <TR><!-- Topnav Image -->
       <!-- <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
        </TR> -->
		<tr><td width="780"><?include("includes/header.php");?></td></tr>
        <TR>
        
        </TR>
		<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
		</tr>
        <TR>
            <TD background=images/tile.jpg vAlign=top width=780 align=center>
        <form name='frmCentres' method="post" onsubmit="return false;">        
                <table width=780 cellpadding=0 cellspacing=5 border=0>
                        <tr>
                                <td colspan=2 align="center"><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2>Add IWay Details</b></font></td>
                        </tr>                
		                <tr>
                                <td align="right" width=390 class="greybluetext10" valign=top>Select Exam Centre :&nbsp;</td>
                                <td align="left" valign=top>
                                        <select class="textbox" name="exam_centre_code" style={width:169px} >
										<option value=''>-Select-</option>
						<?php
								while(list($exam_centre_code,$exam_centre_name) = mysql_fetch_row($res))
								{
										$exam_centre_name=stripslashes($exam_centre_name);
										if ($exam_centre_code == $examCentreCode) 
										{
											echo "<option value=\"$exam_centre_code\" selected>$exam_centre_code - $exam_centre_name</option>";											
										}
										else
										{
											echo "<option value=\"$exam_centre_code\">$exam_centre_code - $exam_centre_name</option>";
										}
								}
						?>
										</select>
                                </td>
                        </tr>                    
						<tr>
                        	<td align="right" width=390 class="greybluetext10" valign=top>I Way Code :&nbsp;</td>
                            <td align="left" valign=top>
                        		<input type="textbox" class="textbox" size=30 name="txtiwaycode" value='' maxlength=20>
                        	</td>
                        </tr>
						<tr>
                        	<td align="right" width=390 class="greybluetext10" valign=top>I Way Name :&nbsp;</td>
                            <td align="left" valign=top>
                        		<input type="textbox" class="textbox" size=30 name="txtiwayname" value='' maxlength=100>
                        	</td>
                        </tr> 
                        <tr>
                        	<td align="right" width=390 class="greybluetext10" valign=top>No.of seats :&nbsp;</td>
                            <td align="left" valign=top>
                        		<input type="textbox" class="textbox" size=30 name="txtseats" value='' maxlength=10>
                        	</td>
                        </tr>
                        <tr>
                        	<td align="right" width=390 class="greybluetext10" valign=top>Select Status :&nbsp;</td>
                            <td align="left" valign=top>
                        		<select class="textbox" name="lststatus" style={width:169px}>
                        			<option value=''>--Select--</option>
                        			<option value='N'>New</option>
                        			<option value='A'>Activated</option>                        			
                        		</select>
                        	</td>
                        </tr>   
                        <tr>
                        	<td align="right" width=390 class="greybluetext10" valign=top> Address1 :&nbsp;</td>
                            <td align="left" valign=top>
                        		<input type="textbox" class="textbox" size=30 name="txtiwayaddress1" value='' maxlength=100>
                        	</td>
                        </tr>
                        <tr>
                        	<td align="right" width=390 class="greybluetext10" valign=top> Address2 :&nbsp;</td>
                            <td align="left" valign=top>
                        		<input type="textbox" class="textbox" size=30 name="txtiwayaddress2" value='' maxlength=100>
                        	</td>
                        </tr> 
                        <tr>
                        	<td align="right" width=390 class="greybluetext10" valign=top> City :&nbsp;</td>
                            <td align="left" valign=top>
                        		<input type="textbox" class="textbox" size=30 name="txtiwaycity" value='' maxlength=100>
                        	</td>
                        </tr>
                        <tr>
                        	<td align="right" width=390 class="greybluetext10" valign=top> State :&nbsp;</td>
                            <td align="left" valign=top>
                        		<input type="textbox" class="textbox" size=30 name="txtiwaystate" value='' maxlength=100>
                        	</td>
                        </tr>    
                        <tr>
                        	<td align="right" width=390 class="greybluetext10" valign=top> Pin Code :&nbsp;</td>
                            <td align="left" valign=top>
                        		<input type="textbox" class="textbox" size=30 name="txtiwaypincode" value='' maxlength=20>
                        	</td>
                        </tr>
						<tr>
							<td colspan=2 align=center>
								<input class="button" type="button" name="sub_save" value="Save" onclick="javascript:validate();">								
								<input class="button" type="reset" name="reset" value="Reset" >
							</td>
						</tr>
						<?php 
							if (isset($err) && $err!='') 
							{
						?>
                        <tr>
                                <td colspan=2 align="center" class='errormsg'><b><?echo $emsg;?></b></td>
                        </tr>
                		<?php
                			}
                		?>
                		<?if (isset($done) && $done!=''){?>
                        <tr>
                                <td colspan=2 align="center" class='alertmsg'><b><?echo $msg;?></b></td>
                        </tr>
                		<?}?>                                       
                </table>
                </form>
                </TD>
        </TR>
        <TR>
                <?include("includes/footer.php");?>
        </TR>
</TABLE>
</FORM>
</center>
</BODY>
</HTML>