<?php
$admin_flag = 1;
require_once "sessionchk.php";
require_once "dbconfig.php";
include("constants.inc");
$exam_code		= isSet($_REQUEST["hExamCode"]) ? $_REQUEST["hExamCode"] : " ";
$subject_code 		= isSet($_REQUEST["hSubjectCode"]) ? $_REQUEST["hSubjectCode"] : " ";
$section_code 		= isSet($_REQUEST["hSectionCode"]) ? $_REQUEST["hSectionCode"] : " ";
$question_code 		= isSet($_REQUEST["hQuestionCode"]) ? $_REQUEST["hQuestionCode"] : " ";
$question_mark		= isSet($_REQUEST["hQuestionMark"]) ? $_REQUEST["hQuestionMark"] : " ";
$exam_name=isSet($_REQUEST["hExamName"]) ? $_REQUEST["hExamName"] : " ";
$subject_name=isSet($_REQUEST["hSubjectName"]) ? $_REQUEST["hSubjectName"] : " ";
$section_name=isSet($_REQUEST["hSectionName"]) ? $_REQUEST["hSectionName"] : " ";
?>
<html>
<head>
<script type="text/javascript" language="javascript" src="includes/validations.js"></script> 
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<title><?=PAGE_TITLE ?></title>
</head>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
<!--	<TR>
    	<TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR> -->
	<tr><td width="780"><?include("includes/header.php");?></td></tr>
	<TR>
    	
	</TR>
	<tr>
		<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>	
	<TR>
	    <TD background=images/tile.jpg vAlign=top width=780 align=center>
        <form name=addqusfrm method="post" action="addquestions.php?id=1">
		<table border="0" width="720" cellpadding="0" cellspacing="5">
			<tr>
				<td colspan=3 class=greybluetext10><br><b><a href="javascript:choicepage(1)">Exam : <? echo $exam_name;  ?> :&nbsp;<?php echo ($exam_code);?></a>&gt;&gt;<a href="javascript:choicepage(2)"> Subject : <? echo $subject_name;  ?> :&nbsp;<?php echo ($subject_code);  ?></a>&gt;&gt;<a href="javascript:choicepage(3)">Section : <? echo $section_name;  ?> :&nbsp;<?php echo ($section_code);?></a>&gt;&gt;<a href="javascript:choicepage(4)">Mark Weightage :&nbsp;<?php echo ($question_mark);?></a></b><br><br></td>
			</tr>
			<tr>
				<td class=greybluetext10 valign=top>Question Text :</td>		
				<td><textarea name=taQuestion rows=5 cols=50 class=textarea></textarea></td>
			</tr>
			<tr>
				<td class=greybluetext10 valign=top>Option 1 :</td>		
				<td><textarea name=taOption1 rows=4 cols=35 class=textarea></textarea></td>
			</tr>
			<tr>
				<td class=greybluetext10 valign=top>Option 2 :</td>		
				<td><textarea name=taOption2 rows=4 cols=35 class=textarea></textarea></td>
			</tr>
			<tr>
				<td class=greybluetext10 valign=top>Option 3 :</td>		
				<td><textarea name=taOption3 rows=4 cols=35 class=textarea></textarea></td>
			</tr>
			<tr>
				<td class=greybluetext10 valign=top>Option 4 :</td>		
				<td><textarea name=taOption4 rows=4 cols=35 class=textarea></textarea></td>
			</tr>
			<tr>
				<td class=greybluetext10 valign=top>Option 5 :</td>		
				<td><textarea name=taOption5 rows=4 cols=35 class=textarea></textarea></td>
			</tr>
			<tr>
				<td class=greybluetext10 valign=top>Correct Answer :</td>		
				<td><select name=sCorrectAns onchange="chkvalidcorrect()" class=textbox>
					<option vlaue="select">Select</option>
				<? 
				 for ( $i = 1; $i <= 5 ; $i++){
						echo "<option value='$i'>$i</option>";
				}
				?>
				</select></td>
			</tr>
			<tr>
				<td class=greybluetext10 valign=top>Marks :</td>		
				<td><select name=sMarks class=textbox>
					<option value=select>Select</option>
			                <? for($marks_i = 0 ; $marks_i < $nStoredMarks;$marks_i++){
						if ($question_mark == $aStoredMarks[$marks_i])
                                        		 $chk_selected= " Selected";
	                                        echo "<option value='".$aStoredMarks[$marks_i]."' $chk_selected>".$aStoredMarks[$marks_i]."</option>";
                                         $chk_selected='';
                                         } ?>
				</select></td>
			</tr>
<!--			<tr>
				<td>Negative marks :</td>		

			</tr> -->
			<!--<tr>
				<td class=greybluetext10 valign=top>Online/Offline :</td>		
				<td><select name=sOnOff class=textbox>
					<option vlaue="select">Select</option>
					<option value="Y">Online</option>
					<option value="N">Offline</option>
				</select></td>
			</tr>-->
			<input type=hidden name='sOnOff' value='Y'>
				<Td colspan=3 align=left><br><input type="button" name=bBack value=Back class=button onclick="goback()">&nbsp;&nbsp;&nbsp;<input type="button" name=bAdd value=Add class=button onclick="checkall()">&nbsp;&nbsp;&nbsp;<input type="button" name=bReset value=Reset class=button onclick=resetfrm()></td>				
			</tr>
		</table>
		</TD>
	</TR>
	<TR>
		<input type=hidden name=hExamCode value=<?php echo ($exam_code);?>>
		<input type=hidden name=hSubjectCode value=<?php echo ($subject_code);?>>
		<input type=hidden name=hSectionCode value=<?php echo ($section_code);?>>
		<input type=hidden name=hQuestionCode value=<?php echo ($question_code);?>>											
		<input type=hidden name=hQuestionMark value=<?php echo ($question_mark);?>>
		<INPUT type=hidden name=hSelectedQus value=<?php echo ($question_id);?>>
		<input type=hidden name=hNegativeMark>
		<input type=hidden name=hTmp>							
		<input type=hidden name=hExamName value='<?php echo ($exam_name);?>'>
		<input type=hidden name=hSubjectName value='<?php echo ($subject_name);?>'>
		<input type=hidden name=hSectionName value='<?php echo ($section_name);?>'>
		<?include("includes/footer.php");?>
	</TR>
</TABLE>
</FORM>
</center>
</BODY>
</html>
<SCRIPT language="javascript">
var frm;
frm = document.addqusfrm;

function choicepage(a){
actval="addquestions.php?choice=3&pos="+a;
document.addqusfrm.action =actval;
document.addqusfrm.submit();
}

function checkall(){
if (trim(frm.taQuestion.value)==""){
	alert ("Question text should not be empty");
	frm.taQuestion.focus();
	return;
}
for (i=1;i<3;i++){
	if (trim(frm["taOption"+i].value)==""){
		alert ("First two Options should not be empty");
		frm["taOption"+i].focus();
		return;
	}
}
chktmp=0;
for (i=1;i<6;i++){
	if ((chktmp==1) && (trim(frm["taOption"+i].value)!="")){
		alert ("Options cannot have blank values inbetween");
		frm["taOption"+i].focus();
		return;
	}
	if (trim(frm["taOption"+i].value)=="")
		chktmp=1;
}

if (!(chkvalidcorrect()))
	return;
if (frm.sCorrectAns.options.selectedIndex == 0){
	alert("Choose the correct answer");
	frm.sCorrectAns.focus();
	return;
}
if (frm.sMarks.options.selectedIndex == 0){
	alert("Choose the Marks");
	frm.sMarks.focus();
	return;
}
/*
if (frm.sOnOff.options.selectedIndex == 0){
	alert("Choose Online/Offline");
	frm.sOnOff.focus();
	return;
}
*/
calculateNegMark();
tmp = Math.round(((1 - 0.5) +( ( (5+0.49999) - (1-0.5) ) * Math.random() ) )*100000);
frm.hTmp.value = tmp;
frm.hQuestionMark.value=frm.sMarks.options[frm.sMarks.selectedIndex].value;
frm.action="process_ques.php?id=1";
//alert (frm.hNegativeMark.value);
frm.submit();
frm.hTmp.value = tmp++;
}

function getcount(){
cnt=0;
for (i=1;i<6;i++)
if(trim(frm["taOption"+i].value) != "" )
		++cnt;
return cnt;
}

function calculateNegMark(){
cnt = getcount();
tmpno = (parseFloat(frm.sMarks.options[frm.sMarks.options.selectedIndex].value)/cnt);
var tmpno = Math.round(tmpno*1000)/1000;
frm.hNegativeMark.value = tmpno;
}

function chkvalidcorrect(){
if (frm.sCorrectAns.options.selectedIndex == 0){
	alert ("Select Correct Answer");
	frm.sCorrectAns.focus();
	return;
}
selectval = parseInt(frm.sCorrectAns.options[frm.sCorrectAns.options.selectedIndex].value);
if (trim(frm["taOption"+selectval].value)==""){
	alert("The selected option is empty");
	frm["taOption"+selectval].focus();
	return false;
}
return true;
}
function resetfrm(){
if(!confirm("Are you sure - Reset the page to Default values"))
	return;
frm.reset();
}
function goback(){
	frm.action="edit_questions.php";
	frm.submit();
}

function checktext(comp){ 	//Regurlar expression for checking Chking contents of textarea
myRegExp = new RegExp("[^a-zA-Z0-9_/&,:#\)(-]");
txt = comp.value;
result=txt.match(myRegExp);
if(result){
    alert('Sorry Invalid data!');
	comp.focus();
	return;
}
}
</SCRIPT>
