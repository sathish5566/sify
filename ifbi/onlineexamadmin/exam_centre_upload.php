<?
//$admin_flag = 0;
//new condition included on 10/25/05
session_start();
if ($_SESSION["admin_type"]== '0') {
	$admin_flag = 0;
} else if ($_SESSION["admin_type"]== '1'){
	$admin_flag = 1;
}

require_once "sessionchk.php";
require("constants.inc");
require_once "dbconfig.php";
$header_candidate=array("EXAM_CTR_CODE","EXAM_CTR_NAME"); 

$validation_alphanum= '/^[a-z A-Z0-9]*$/i'; //alphabets & numerals
$validation_num= '/^[0-9]*$/i'; //alphabets & numerals
include("includes/icicipru_functions.php");
include("includes/excelreader/reader.php");
$filepath="includes/exam_centre_files/";

$commondebug = false;
$flag	= isSet($_POST["flag"]) ? $_POST["flag"] : "";
$update_flag=0;
$nUpdatecount=0;
$validate=1;
if($flag == "update"){
$file_name = fileupload($_FILES,$filepath);


if($file_name!=""){
/**
*     Excel reader class usage starts here
*     Instantiating the Excel reader Class
*/

$data = new Spreadsheet_Excel_Reader();


/** setting the encoding format */
$data->setOutputEncoding('CP1251');
/** This will read from the excel file  */
$data->read($filepath.$file_name);


$no_of_sheets = count($data->sheets);
$sheetno=0;
$no_of_rows = $data->sheets[0]['numRows'];
$no_of_cols = $data->sheets[0]['numCols'];


if($commonDebug){
			echo $file_name."<br>";
			echo "ROWS:".$no_of_rows."COLS:".$no_of_cols."<br>";
		}

if(($no_of_cols ==0) || ($no_of_rows ==0)){
	$errormsg = "Error in Excel file. No Records Found";
}
else
{
if($no_of_cols !=2 || $no_of_sheets >1)
{
	if($no_of_cols !=2){
		$errormsg = "Error in Excel file(Should have 2 columns)";
		//disp_error($error_msg);
	}
	else if($no_of_sheets >1){
		$errormsg = "Error in Excel file(Should not have more than one sheet)";
		disp_error($error_msg);
	}
}
else
{
	$header_disp=array();
	for($j=1;$j<=$no_of_rows;$j++)
	{
		for($k=1;$k<=$no_of_cols;$k++)
		{
			$cell_info = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
			if($j==1)
			{
				//echo $cell_info."<br>";
				$headercheck = header_examcentrevalidate($k,$cell_info);
				$header_disp[]=$headercheck;
			}
			
		}
	}
	//echo "<pre>";
	//print_r($header_disp);
	//echo "</pre>";
	if (in_array('0',$header_disp))
	{
		$errormsg = "Error in Header";
		$headerflag=1;
	}

	else
	{
	$dup_memno='';
	for($j=2;$j<=$no_of_rows;$j++)
	{
		$exam_centre_code="";
		$exam_centre_name="";
		$dup_memno='';
		
		for($k=1;$k<=$no_of_cols;$k++)
		{
			if($k == 1){
				$exam_centre_code = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
					$i++;
			}
			else if($k==2){
				$exam_centre_name = addslashes($data->sheets[$sheetno]['cells'][$j][$k]);
			}
		}//inner for loop
		
		//exit;
				if((trim($exam_centre_code) !="")&& (trim($exam_centre_name) !="")) 
		{
				$check=(preg_match($validation_num,trim($exam_centre_code))) && (preg_match($validation_alphanum,trim($exam_centre_name)))&&(strlen(trim($exam_centre_code))<=20) && (strlen(trim($exam_centre_name))<=255);

				//$check=preg_match($validation_alphanum,trim($exam_centre_code)) && $check;
				//$check=preg_match($validation_alphanum,trim($exam_centre_name)) && $check;
				
				/*echo "exam".preg_match($member_nums,trim($exam_code))."<br>";
				echo "mem".preg_match($member_nums,trim($memno))."<br>";
				echo "examcen".preg_match($member_nums,trim($exam_centre_code))."<br>";
				echo "name".preg_match($validation_alpha,trim($name))."<br>";
				echo "gender".preg_match($validation_alpha,trim($gender))."<br>";echo "check ".$check."<br>";
				$check =(preg_match($member_nums,trim($subject_code)))&&(preg_match($member_nums,trim($exam_code)))&&
				(preg_match($member_nums,trim($memno)))&&(preg_match($member_nums,trim($exam_centre_code)))&&
				(preg_match($validation_alpha,trim($name)))&&(preg_match($validation_alpha,trim($gender)));*/
				
				if($check)
				{
					$sql_check="select count(1) from iib_exam_centres where exam_centre_code='$exam_centre_code'";
					
					if ($commonDebug)
					{
						echo $sql_check;
					}
					else
					{
						$res_check = mysql_query($sql_check);
						if (mysql_error())
						{
							$errormsg="Error in query";
						}
						else
						{

							list($count)=mysql_fetch_row($res_check);
							if($count==0)
							{
								//select membership_no from iib_candidate order by membership_no 
								$dbcolumns_cand = " exam_centre_id, exam_centre_code , exam_centre_name , online ";
								
								
								if($dup_memno!=$exam_centre_code || $dup_memno=='')
									
								{	
								
									$sql_cnt = "select max(exam_centre_id) from iib_exam_centres ";
									$res_sql_cnt = mysql_query($sql_cnt);
									list($exam_centre_id) = mysql_fetch_row($res_sql_cnt);
									$exam_centre_id++;
									$insert_cand=0;
									$sql_insert_cand="insert into iib_exam_centres ($dbcolumns_cand)"
													 ." values "
													 ." ('$exam_centre_id' , "
													 ." '$exam_centre_code' , "
													 ." '$exam_centre_name' , "
													 ." 'Y') ";
								
									
									$dup_memno=$exam_centre_code;
									$insert_cand=1;
									$update_flag=1;
									$nUpdatecount++;
								}
								if ($commonDebug)
								{
									echo $sql_insert_cand;
								}
								else
								{
									if($insert_cand==1)
									{
										$res_insert_cand= mysql_query($sql_insert_cand);
									}
									
									if (mysql_error())
									{
										$error_row[]=$j-1;
									}
								}
							}//end of count
							else{
							$duplicate_memno[]="$exam_centre_code|$exam_centre_name";
							}
							
						}//end of else mysql error
					}//end of else common debug
						
				}//end of !check
				else
				{
					$validate_rec[]="$exam_centre_code|$exam_centre_name";
					$validate=0;
				}
			}//if trim
							//else{
								//$duplicate_memno[]="$memno|$name|$email_id|$address1|$address2|$address3|$address4|$city|$state|$pincode";
								//$duplicate_memno[]="$memno|$exam_centre_code|$gender|$address1|$address2|$address3|$address4|$address5|$address6|$state|$zone_code|$name|$pincode|$institution_code|$institution_name|$exam_code|$subject_code|$medium_code";
							//}
			else
			{
				//$column_null[]="$memno|$name|$email_id|$address1|$address2|$address3|$address4|$city|$state|$pincode";
				$column_null[]="$exam_centre_code|$exam_centre_name";
			}
	}//end of outer for loop
	if(is_array($error_row))
	{
		$error_msg = "Error in row(s) ".implode(" | ",$error_row);
	}

}
}//else
}//else no records
}//$file!=''
else{
	$errormsg=$excelerrormsg;
}
if(!$excelerrormsg){
	//$error_msg="Uploaded Successfully";
}
if($update_flag==0){
	//echo $update_flag;
	$error_msg="Records already exists in the database";
}//if($nUpdatecount)
if(($update_flag==0) || $validate==0){
	//echo $update_flag."is update flag value";
	//echo $validate."is validate value";
	$error_msg="Error in Insertion of Records";
}//if($nUpdatecount)

else if($nUpdatecount==1){
	$error_msg="$nUpdatecount Record has been Inserted";
}//if($nUpdatecount)
else if($nUpdatecount>1){
	$error_msg="$nUpdatecount Records has been Inserted";
}
}//$flag update

?>
<html>
<head>
<title><?=PAGE_TITLE ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<script language='javascript'>
function validate()
{
	var frm=document.upload_candidate;

	if (frm.excelfile.value == '')
	{
		alert('Please Select The File');
		frm.excelfile.focus();
		return;
	}
	frm.action="exam_centre_upload.php";
	frm.flag.value="update";
	frm.submit();
}
function alert_msg()
{	
	var frm=document.upload_candidate;		
	alert('Please browse the file');
	return false;
}

function newwin()
{
	
    window.open('exam_centre_validation_pbm.php','win1','left=2,top=2,status=no,config="width="+newWidth+" height="+newHeight+",toolbar=no,scrollbars=no,resizable=yes');
}

function newwin1()
{
	
    window.open('exam_centre_null_pbm.php','win1','left=2,top=2,status=no,config="width="+newWidth+" height="+newHeight+",toolbar=no,scrollbars=no,resizable=yes');
}
</script>
</head>
<BODY leftMargin=0 topMargin=0 LeftMargin=0 marginheight=0 marginwidth=0>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
	<!--<TR>Topnav Image 
        <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR>
    <TR>
        <TD bgColor=#7292c5 width=780>&nbsp;</TD>
    </TR>-->
<tr><td width="780"><?include("includes/header.php");?></td></tr>

	<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>
	<?
	echo "<tr><td>";
	disp_error($errormsg);
	echo "</td></tr>";

	If(!$errormsg){
		echo "<tr><td>";
		disp_error($error_msg);
		echo "</td></tr>";
	}
		?>
		<TR>
    	<TD background=images/tile.jpg vAlign=top width=780 align=center>
        <table width=780 cellpadding=0 cellspacing=5 border=0>
        	<tr>
	           	<td colspan=2 align="center"><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2> Exam Centre  Details Upload</b></font></td>
            </tr>    
            </tr>
            <td colspan=2 align=center class="greybluetext10">
            <!--<form name=frmtaupload enctype="multipart/form-data" action="taupload.php" method="POST">-->
            <form name=upload_candidate method="post" enctype="multipart/form-data">
			<input type="hidden" name="flag" value="">
			Send this file: <input name="excelfile" type="file" class="textbox" onkeypress='return alert_msg()'>
			<input name="Submit" type="submit" class="button" value="Upload" onClick="javascript:validate();return false;">
     	
     	
		<!--<table width=780 cellpadding=0 cellspacing=5 border=0>
			<tr>
				<td colspan=2 align="center" ><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2>
			</tr>

			<tr>
				<td colspan=2 align="center" ><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2>Upload Candidate List</b></font></td>
			</tr>


			<tr>
				<td align="right" width=390 class="greybluetext10">
				Upload Candidate Details&nbsp; :
				</td>
				<td align="left">
				<input name="excelfile" type="file"  class="textbox">
				</td>
			</tr>
			<tr>
				<td align="right">
				<input name="Submit" type="submit" class="button" value="Upload" onClick="javascript:validate();return false;">
			</tr>-->

<?
$count_duplicate=count($duplicate_memno);
//$count_email=count($email_invalid);
$count_validate=count($validate_rec);
$count_column_null=count($column_null);
//if(($count_duplicate !=0) || ($count_email !=0) || ($count_column_null !=0) ){
if(($count_duplicate !=0) || ($count_column_null !=0) || $count_validate!=0){
?>

		<tr>
            <td colspan=2  class="greybluetext10" height="35" align=center>
            <table border=0 cellpadding=2 cellspacing=2 bordercolorlight=white width=100%>
            <tr bgcolor="#D1E0EF" class="greybluetext10"><td  align='center' colspan=10><b>Records With Error In Excel</b></td></tr>
			<tr bgcolor="#D1E0EF" class="greybluetext10">
				<td align=center><b>Exam Centre Code</b></td>
				<td align=center><b>Exam Centre Name</b></td>
		    </tr>
			<?
			if($count_duplicate >0)
			{?>
				<tr bgcolor="#D1E0EF" class="greybluetext10"><td align='center' colspan=10><font color="#FF0000"><b>Exam Centre Code Already Exists</font></b></td></tr>

			<?
			}//$count_duplicate

			foreach($duplicate_memno as $key => $value){
				$value1=$value;
				$avalue=explode('|',$value1);
				echo "<tr bgcolor=#D1E0EF class=greybluetext10>";
				foreach($avalue as $key1 => $value2){
					echo "<td align=center width=30%>";
					echo $value2;
					echo "</td>";
				}
				echo "</tr>";
			}

			if($count_validate >0){?>
			
		    <tr bgcolor="#D1E0EF" class="greybluetext10"><td align='center' colspan=10><font color="#FF0000"><a href="javascript:newwin();"><b>Validation Problem</b></a></font></td></tr>

			<?
			}//$count_validate
			foreach($validate_rec as $key => $value){
				$value1=$value;
				$avalue=explode('|',$value1);
				echo "<tr bgcolor=#D1E0EF class=greybluetext10>";
				foreach($avalue as $key1 => $value2){
					echo "<td align=center width=30%>";
					echo $value2;
					echo "</td>";
				}
				echo "</tr>";
			}

			
			if($count_column_null >0){?>
		    <tr bgcolor="#D1E0EF" class="greybluetext10"><td align='center' colspan=10><font color="#FF0000"><a href="javascript:newwin1();"><b>Null Values</b></a></font></td></tr>

			<?
			}//$count_nCountarray
			foreach($column_null as $key => $value){
				$value1=$value;
				$avalue=explode('|',$value1);
				echo "<tr bgcolor=#D1E0EF class=greybluetext10>";
				foreach($avalue as $key1 => $value2){
					echo "<td align=center>";
					echo $value2;
					echo "</td>";
				}
				echo "</tr>";
			}


		?>
		</table></td><tr>
		<tr class="greybluetext10">
            <td colspan=3  class="greybluetext10" height="35" align=center><b>Note: Please Recheck the above mentioned records and upload it. </b>
            </td>
          </tr>
		<?}//count?>
		<tr class="greybluetext10">
       		 <td colspan=3  class="greybluetext10" height="35" align=left><b>Note: Please Upload the Exam Centre Details with the <a href="includes/download/exam_centre_upload.xls" target="_blank" class="greybluetext10"><font color="blue">Template File</font></a></b>
        </td>
          </tr>

		</table>
		</form>
		</TD>
	</TR>
		<TR>
    	<!--<TD bgColor=#7292c5 width=780>&nbsp;</TD>-->
<?include("includes/footer.php");?>

    	</TR>
</table>
</CENTER>
</body>
</html>