<?php
require_once "sessionchk.php";
require_once "dbconfig.php";
//Add Application / Technology Issues
if ($_POST["submitted"]=='true')
{
	$memno = trim($_POST['memno']);
	$centreCode = trim($_POST['centrecode']);
	$reason = trim($_POST['reason']);
	if($reason == 'Others')
		$reason = trim($_POST['others']);
	$examDate = date('Y-m-d');
	$examTime = date('H:i:s');
	
	
	$sql = "INSERT INTO iib_appln_issues_logs  (membership_no,centre_code,reason,exam_date,exam_time) VALUES ('$memno','$centreCode','$reason','$examDate','$examTime')"; 
	//echo $sql;
	$res = mysql_query($sql) or die ("Connection cannot be done3 error:'".($sql)."'");
	if(!$res)
		{
			$err=1;
			$emsg="There was an error in inserting the record.";
			$displaybug='true';
		}
		else
		{
			$done=1;
			$msg="New record created.";
		}
}
?>
<HTML>
<HEAD>
<title>Indian Institute Of Banking &amp; Finance</title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
	FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='JavaScript' src='./includes/validations.js'></script>
<script language='javascript'>
function validate()
{
	var f=document.addApplnIssues;
    var eNum = trim(f.memno.value);
	if (eNum == '')
	{
		alert('Please Enter The Membership Number!');
		f.memno.focus();
		return;
	}
	
    if(f.centrecode.value=='')
	{
		alert('Please Enter The Centre Code!');
		f.centrecode.focus();
		return;
	}
	
	if(f.reason.value=='')
	{
		alert('Please Select The Reason');
		f.reason.focus();
		return;
	}
	
if ((f.reason.value == "Others") && (trim(f.others.value) == "")){
		alert ("Please mention the reason in others");
		f.others.focus();
		return;
	}
	
	f.submitted.value='true';
	f.submit();
}

function submitForm()
{
    if (window.event.keyCode == 13)
    {
        validate();
    }
}
</script>
</HEAD>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0 onload='javascript:document.addApplnIssues.memno.focus();' onKeyPress='javascript:submitForm()'>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
	<TR><!-- Topnav Image -->
    	<TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR>	
	<TR>
    	<TD bgColor=#7292c5 width=780>&nbsp;</TD>
	</TR>
	<tr>
		<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>
	<TR>
	    <TD background=images/tile.jpg vAlign=top width=780 align=center>
     	<form name=addApplnIssues method="post">
		<table width=780 cellpadding=0 cellspacing=5 border=0>
			<tr>
				<td colspan=2 align="center" ><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2>Application / Technology Issues</b></font></td>
			</tr>		
		
			<tr>
				<td align="right" class="greybluetext10">
					Membership  Number :&nbsp;
				</td>
				<td align="left">
					<input class="textbox" type="text" name="memno" maxlength="10">
				</td>
			</tr>
			<tr>
				<td align="right" class="greybluetext10">
					Centre Code :&nbsp;
				</td>
				<td align="left">
					<input class="textbox" type="text" name="centrecode">
				</td>
			</tr>

			<tr>	
			<tr>
				<td align="right" class="greybluetext10">Reason :&nbsp;</td>
				<td align="left">
					<select name='reason' class="greybluetext10">
					<option value=''>-- Select --</option>
					<?
					 $sql_reason = "select reason from iib_appln_issues";
					 $res_reason = mysql_query($sql_reason);
					 while(list($reason)=mysql_fetch_row($res_reason)){
						$sel="";
						 if ($reason==$sReason)
						 	$sel = "selected";
						 echo "<option value='$reason' $sel>$reason</option>";
					 }
					  ?>
					</select>
							<INPUT type="hidden" name=tCode value=<? echo $_REQUEST["code"]?> />
				</td> 
			</tr>
			
			<tr>
				<td align="right" class="greybluetext10">
					Others  :&nbsp;
				</td>
				<td align="left">
					<input class="textbox" type="text" name="others">
				</td>
			</tr>

			<tr>
				<td align="right"><input type="button" class=button name=bSubmit value="Save" onclick='javascript:validate();'></td>
				<td align="left"><input type="reset" class=button name=bReset value="Reset"></td> 
			</tr>
			<?if(isset($err)){?>	
			<tr>
				<td colspan=2 align="center" class=errormsg><?echo $emsg;?></td>
			</tr>		
		<?}?>
		<?if(isset($done)){?>	
			<tr>
				<td colspan=2 align="center" class='alertmsg'><?echo $msg;?></td>
			</tr>		
		<?}?>
					<input type="hidden" name="submitted">
		</table>
		</form>
		</TD>
	</TR>
	<TR>
		<TD bgColor=#7292c5 width=780>&nbsp;</TD>
	</TR>
</TABLE>
</FORM>
</center>
</BODY>
</HTML>