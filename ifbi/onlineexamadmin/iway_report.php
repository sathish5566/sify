<?php
/****************************************************
* Application Name            :  IIB
* Module Name                 :  Iway report
* Revision Number             :  1
* Revision Date               :
* Table(s)  modified                  :  
* Tables used for only selects:  iib_exam_centres,iib_candidate_iway,iib_candidate_scores,iib_franchisee_map,iib_iway_details
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              : CSV file & HTML Page
* Document/Reference Material :
* Created By : 
* Created On :  
* Last Modified By            :  Anil Kumar.A
* Last Modified Date          : 21/03/2006
* Description                 :  Report displaying the iways and thier corresponding filled seats for various timings (Mapped) and can be downloaded in csv file too.
*****************************************************/
ob_start();
$admin_flag=2;
require_once "sessionchk.php";
require_once "dbconfig_slave_123.php";
$sExamCentre =($_REQUEST["sCentreCode"])?$_REQUEST["sCentreCode"]:"";
//$exam_date = $_REQUEST["sYear"]."-".$_REQUEST["sMonth"]."-".$_REQUEST["sDate"];
$exam_date = $_REQUEST["exam_date"];
$exam_time = $_REQUEST["sTime"];
$submitted=isset($_POST['submitted']) ? $_POST['submitted'] : '0' ;
	//getting values when download button is clicked
	$ex_flag=isset($_POST['exl']) ? $_POST['exl'] : '0' ;
	
	//Generic excel file included
	include_once("excelconfig.php");
	
	$filename="iway_report_Centre(".$sExamCentre.")_ExamDate(".$exam_date.")";

	$sqlDate = "SELECT distinct exam_date FROM iib_exam_schedule order by exam_date";
	$resDate = mysql_query($sqlDate);
	$nDates = mysql_num_rows($resDate);
		if ($nDates > 0)
		{
			while (list($eDate) = mysql_fetch_row($resDate))
			{
				$aDate = explode("-",$eDate);
				$aDispDate[$eDate] = $aDate[2]."-".$aDate[1]."-".$aDate[0];
			}
		}
	
	if($sExamCentre == 'ALL')
				$sql = "select exam_time from iib_candidate_iway where exam_date = '$exam_date' group by exam_time";
			else
				$sql = "select exam_time from iib_candidate_iway a, iib_iway_details b where exam_date = '$exam_date' and exam_centre_code = '$sExamCentre' and  a.centre_code=b.centre_code group by exam_time";
			$res = mysql_query($sql) or die ("Connection cannot be done error:'".mysql_error ($db)."'");
			$time = array();
			$inc=0;
			while(list($time[$inc++]) = mysql_fetch_row($res)){}
			unset ($time[$inc]);
			$num_time=mysql_num_rows($res);
			if ($submitted =='true' && $sExamCentre != "" and $exam_date != ""){
			$centre_array = array();
			$centre_capacity= array();

                if ( trim($sExamCentre) == 'ALL' ){
					$sql_centre = "select a.centre_code,b.no_of_seats from iib_iway_details a,iib_franchisee_map b where b.centre_code=a.centre_code and a.centre_code != '999999A' order by centre_code";
					$sql_alloted="select a.centre_code,a.exam_time, count(1) from iib_candidate_iway a, iib_iway_details b where a.exam_date='$exam_date' and a.centre_code = b.centre_code  and b.centre_code != '999999A' group by a.centre_code,exam_time order by a.centre_code,a.exam_time";

					$sql_filled="select centre_code, exam_time, count(distinct a.membership_no) from iib_candidate_iway a, iib_candidate_scores b where a.exam_date='$exam_date' and b.membership_no = a.membership_no and b.exam_code = a.exam_code and b.subject_code = a.subject_code";

			    }
                else{
					$sql_centre = "select a.centre_code,b.no_of_seats from iib_iway_details a,iib_franchisee_map b where b.centre_code=a.centre_code and a.centre_code != '999999A'  and exam_centre_code='$sExamCentre' order by centre_code";
					$sql_alloted="select a.centre_code,a.exam_time, count(1) from iib_candidate_iway a, iib_iway_details b where a.exam_date='$exam_date' and b.exam_centre_code = '$sExamCentre' and a.centre_code = b.centre_code group by a.centre_code,exam_time order by a.centre_code,a.exam_time";

					$sql_filled="select centre_code, exam_time, count(distinct a.membership_no) from iib_candidate_iway a, iib_candidate_scores b where a.exam_date='$exam_date' and b.membership_no = a.membership_no and b.exam_code = a.exam_code and b.subject_code = a.subject_code";
				}
				$res_centre=mysql_query($sql_centre);
				$res_alloted=mysql_query($sql_alloted);
	

// Header for Excel
$excel_header[0]="iWay Centre Code";
$excel_header[1]="No of Seats";
$m=0;
$hd_count=2;
while ($time[$m]){
	$excel_header[$hd_count]=$time[$m]." Alloted";
	$hd_count++;
	$excel_header[$hd_count]=$time[$m]." Filled";
	$hd_count++;
	$m++; 
	}
	$excel_header[$hd_count]=" Total";							

	$iwaylist = "";
	while (list($centre_code,$no_of_seats) = mysql_fetch_row($res_centre)){
		$centre_code=strtoupper($centre_code);
					while($time[$m])
								$centre_array["$centre_code"][$time[$m++]]=array();
					$centre_capacity["$centre_code"]=$no_of_seats;							
					if ($iwaylist == "")
						$iwaylist=$centre_code;				
					else
						$iwaylist=$iwaylist."','".$centre_code;
					}
					
					while(list($centre_code,$alloted_time,$alloted)=mysql_fetch_row($res_alloted)){
						$centre_code=strtoupper($centre_code);
					$val=$centre_capacity["$centre_code"];
						$centre_array["$centre_code"]["$alloted_time"]=array(0=>$val,1=>$alloted,2=>"");
					}
	  	 		    if ( trim($sExamCentre) == 'ALL' )
						$sql_filled=$sql_filled." group by centre_code, a.exam_time";
					else
						$sql_filled=$sql_filled." and a.centre_code in ('$iwaylist') group by centre_code, a.exam_time order by centre_code, exam_time";
					$res_filled=mysql_query($sql_filled);
					while(list($centre_code,$filled_time,$filled)=mysql_fetch_row($res_filled)){
						$centre_code=strtoupper($centre_code);
					$centre_array["$centre_code"]["$filled_time"][2]=$filled;
					}
					$num_iway=count($centre_array);
					$cnt=0;
					$tot_fill=0;
					$i=0;
					
					while (list($tmp_key,$value) = each($centre_array)){
						$m=0;
							list($no_of_seats,$alloted,$filled)=current($value);
							$resultArr[$i][0]=$tmp_key;
							$resultArr[$i][1]=$no_of_seats;
						$iway_tot=0;
						$j=2;
						while ($time[$m]){
							list($no_of_seats,$alloted,$filled)=$value[$time[$m]];
							if ($no_of_seats == "")
								$no_of_seats=0;
							if ($alloted == "")
								$alloted=0;
							if ($filled == "")
								$filled=0;
							$tot_fill = $tot_fill + $filled;
							$iway_tot = $iway_tot + $filled;
							$resultArr[$i][$j]=$alloted;
							$j++;
							$resultArr[$i][$j]=$filled;
							$m++;
							$j++;
						}
						$resultArr[$i][$j]=$iway_tot;
						$i++;
					}
	$cnt_result=count($resultArr);
	$headerLength=count($excel_header);
	
	//if excel download button is clicked write to the CSV file by calling excelconfig method
	  if($ex_flag==1)
		{
			
			excelconfig($filename,$excel_header,$resultArr);
			exit;
		}

}
?>
<html>
<head>
<title><?=PAGE_TITLE ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language=javascript>

function validate(){
frm = document.frm_allot;
frm.submitted.value='true';
frm.exl.value='0';
frm.action = "iway_report.php";
frm.target = "_self";
frm.submit();
}
function excelsubmit()
{
	//alert("hi");
	var frmname=document.frm_allot;
	frm.submitted.value='true';
	frmname.exl.value='1';
	frmname.submit();
}
</script>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<center>
<table  background=images/tile.jpg>
<tr>
<td>
<table width="780" border="0" cellspacing="0" cellpadding="0">
 <!--	<tr> 
		<td width="780"><img src="images/logo1.jpg" width="136" height="59"><img src="images/logo2.gif" width="284" height="59"><img src="images/logo3.gif" width="360" height="59"></td>
	</tr> -->
	<tr><td width="780"><?include("includes/header.php");?></td></tr>
	<tr>
	 	
	</tr>
	<tr>
		<Td class="greybluetext10"><? include("admin_menu.php") ?></Td>
	</tr>
	<tr>
		<form name='frm_allot' method=post>
		<td class="greybluetext10" height="35"align=center>
	</tr>
	<tr>
		<td>
		<table border=0 cellspacing=0 cellpadding=0>
		    <tr>
				<td colspan=2 align="center"><b><font face="Verdana, Arial, Helvetica, sans-serif" size=2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Iway Report</b></font><br><br></td>
            </tr>
			<tr>
        		<td align="right" class="greybluetext10" valign=top><b>Select Exam Centre&nbsp;:&nbsp;</b></td>
        		<td align="left"><select class="textbox" name="sCentreCode" style={width:250px}>
					<option value='ALL'>-ALL-</option>
					<?
					$sql_exam= "select exam_centre_code,exam_centre_name from iib_exam_centres where online='Y'";
					$res_exam=mysql_query($sql_exam);
					while(list($exam_centre_code,$exam_centre_name) = mysql_fetch_row($res_exam)){
						$exam_centre_name=stripslashes($exam_centre_name);
						if ($exam_centre_code == $sExamCentre) 
							echo "<option value=\"$exam_centre_code\" selected>$exam_centre_code - $exam_centre_name</option>";											
						else
							echo "<option value=\"$exam_centre_code\">$exam_centre_code - $exam_centre_name</option>";
					}
					?>
					</select><br>
    			</td>
			</tr>
			<tr>
				<td><br></td>
			</tr>
			<tr>
            	<td align="right" width=390 class="greybluetext10" valign=top><b>Exam Date : </b></td>
            	<td align="left" valign=top class="greybluetext10">
            		<select name='exam_date' class="greybluetext10" style={width:150px} >
            			<option value=''>--Select--</option>
            			<?php
            				foreach ($aDispDate as $key=>$val)
            				{
	            				print("<option value='$key' ");
	            				if ($exam_date == $key)
	            				{
		            				print(" selected ");
	            				}
	            				print(">$val</option>");
            				}
            			?>
            		</select>
            	</td>
            </tr>   
            <tr>
				<td><br></td>
			</tr>
			<tr> 
            <td align="right"><input class='button' type='button' value="Submit" name="go" onclick="javascript:validate();"><br><br></b></b></td>
		</tr>
			</table>
			</td>
		</tr>
		<!--<tr> 
            <td align="center"><input class='button' type='button' value="Submit" name="go" onclick="javascript:validate();"><br><br></b></b></td>
		</tr>		-->
		<input type=hidden name=exl value='0'>
		<input type=hidden name=submitted value=''>
		<tr>
			<td align=center>
			<?
			if( $submitted == 'true')
			{
				if( mysql_num_rows($res_centre)!=0 && mysql_num_rows($res_alloted)!=0 )
				{
					$i=0;
					echo "<table border=1 bordercolor=black cellspacing=0 cellpadding=2>";
					for($a=0;$a<$headerLength;$a++)
					{
						print("<td class=greybluetext10><b>$excel_header[$a]</b></td>");
					}
					print("</tr>");
					?>
					
					<!--<tr>
						<td width=100 align=center class='greybluetext10'>iWay Centre Code</td>
						<td width=50 align=center class='greybluetext10'>No of Seats</td>-->
						<?					
						/*$excel_header[0]="iWay Centre Code";
						$excel_header[1]="No of Seats";
						$m=0;
						$hd_count=2;
						while ($time[$m]){*/
						?>
						<!--<td width=50 align=center colspan='2'><? echo $time[$m];?><br>
						<table border=0 cellpading=0 cellspacing=0 width=100%>
							<tr>
								<td align=center class='greybluetext10'>Alloted</td>	
								<td align=center class='greybluetext10'>Filled</td>	
							</tr>
						</table></td>					-->
						<? 
						/*$excel_header[$hd_count]=$time[$m]."Alloted";
						$hd_count++;
						$excel_header[$hd_count]=$time[$m]."Filled";
						$hd_count++;
						$m++; 
						}
						 $excel_header[$hd_count]="Total";*/
					?>
					<!--<td class='greybluetext10'>Total</td>
					</tr>-->
					
					<?
				
	  			for($disp_row=0;$disp_row<$cnt_result;$disp_row++)
	  			{
		  			for($disp_col=0;$disp_col<$headerLength;$disp_col++)
		  			{
			  			?>
			  		<td class=greybluetext10 class=greybluetext10><?=$resultArr[$disp_row][$disp_col]?></td>
				<?}
				print("</tr>");
	  			}
				echo "</table><br>";
				?>
				</td>
			</tr>
			<tr><td class=greybluetext10 align=center>Total Filled : <?=$tot_fill?></td></tr>
			<tr><td class=greybluetext10>&nbsp;</td></tr>
	         <tr><td class=greybluetext10 align="center" class=greybluetext10><input class='button' type='button' name='download' value='Download' onClick='javascript:excelsubmit()'></td></tr>
	            <tr><td class=greybluetext10>&nbsp;</td></tr>
            <?}else{?>
           <tr>
       	<td align=center class=alertmsg>No Records to display</td>
       	</tr> 
        <?}
    }
       // }
       ?> 
	
</table>
   
</td>
</tr>
 <TR>
                  	<?include("includes/footer.php");?>
</TR>
</table>
 
	</form>
</center>
</body>
<script language="javascript">
frm = document.frm_allot;
function set_cor_date(){
 var yr,month,day;
 day=31;
 yr= frm.sYear.options[frm.sYear.selectedIndex].value; 
 month=(frm.sMonth.options[frm.sMonth.selectedIndex].value);
 if(month == 2){
	var isleap = (yr % 4 == 0 && (yr % 100 != 0 || yr % 400 == 0));
	if(!isleap)       
        day=28;
	else
    	day=29;
 }
 if (month==4 || month==6 || month==9 || month==11)
 	day=30;
 
 for(i=1;i<=((frm.sDate.options.length < day)?frm.sDate.options.length:day);i++){
 	frm.sDate.options[i-1].value=i;
    frm.sDate.options[i-1].text=i;
 }
 if (day > frm.sDate.options.length){
	 for (i = frm.sDate.options.length + 1;i<= day;i++){
		dayopt = new Option(i,i,false,false);	 
		frm.sDate.options[frm.sDate.length] = dayopt;
	}	
 }
 for(j=frm.sDate.length-1;j>=day;j--)
 	frm.sDate.options[j]= null;
 }
 
 function submitpage(){
 	frm.action="date_wise_view.php?id=1";
	frm.submit();
 }
</script>
</html>

