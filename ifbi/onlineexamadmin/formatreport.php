<?php
/****************************************************
* Application Name            :  IIB
* Module Name                 :  Format report
* Revision Number             :  1
* Revision Date               :
* Table(s)                    : 
* Tables used for only selects: iib_exam_centres,iib_exam_schedule,iib_exam_slots,iib_iway_details,iib_format,iib_candidate
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              :
* Document/Reference Material :
* Created By                  :  
* Created ON                  :  
* Last Modified By            :  K.KARUNA MOORTHY RAJU
* Last Modified Date          :  28-03-2006
* Description                 : This Report displaying the format report for every cafe and it can be downloaded as a CSV file.
*****************************************************/
    ob_start();
	$admin_flag=2;
	require_once "sessionchk.php";
	require_once("dbconfig_slave_123.php");
/*** Include file for excel download  ***/	
	include_once("excelconfig.php");
	$sqlcentre = "SELECT exam_centre_code,exam_centre_name FROM iib_exam_centres where online='Y' ";
	$rescentre = mysql_query($sqlcentre);
    $sqlDate = "SELECT distinct exam_date FROM iib_exam_schedule order by exam_date";
	$resDate = mysql_query($sqlDate);
	$nDates = mysql_num_rows($resDate);
/*** Getting the Examdate and change its format  ***/	
    	if ($nDates > 0)
		{
			while (list($eDate) = mysql_fetch_row($resDate))
			{
				$aDate = explode("-",$eDate);
				$aDispDate[$eDate] = $aDate[2]."-".$aDate[1]."-".$aDate[0];
			}
		}
		
	$sqlTime ="SELECT slot_time FROM iib_exam_slots order by slot_time";
	$resTime = mysql_query($sqlTime);
	$nTime = mysql_num_rows($resTime);
	
/*** Getting the Examtime and change its format  ***/	

		if ($nTime > 0)
		{
			while (list($eTime) = mysql_fetch_row($resTime))
			{			
				$aDispTime[$eTime] = substr($eTime,0,5);
			}	
		}
		
	$examCentreCode = $_POST['exam_centre_code'];
	$examDate       = $_POST['exam_date'];
	$examTime       = $_POST['exam_time'];
    $populatecenter = $_POST['populatecenter'];
    $submitted      = $_POST['submitted'];
    $iway_code      = $_POST['iway_code'];
    $forma_type     =  $_POST['format'];
    
/*** Variables - $ex_flag, $filename, $exce_header,$exce_content.
     $ex_flag is for getting the values when clicking download button and $filename is used for 
downloading the file with its value
  ***/
    
    $ex_flag=$_POST['exl'];
    if($examTime!='all')
  {
    $extime = explode(':',$examTime);
    $extime = $extime[0]."-".$extime[1];
  }
  else
    $extime = $examTime;
    $filename="Format_report(".$examCentreCode.")_sub(".$examDate.")_Time(".$extime.")_format(".$forma_type.")_iway(".$iway_code.")";
      if(($examTime=='all') && ($forma_type=='all') && ($iway_code=='all'))
      {
	       $exce_header=array("TA LoginId","Centre code","Exam Time","Iway Address","Membership No","Name","Reasons","Others","Format_type");
      }
      else if(($examTime=='all') && ($forma_type=='all') && ($iway_code!='all'))
      {
	      $exce_header=array("TA LoginId","Centre code","Exam Time","Iway Address","Membership No","Name","Reasons","Others");
      }
      else if(($examTime=='all') && ($forma_type!='all') && ($iway_code=='all'))
      {
	      $exce_header=array("TA LoginId","Centre code","Exam Time","Iway Address","Membership No","Name","Reasons","Others");
      }
      else if(($examTime!='all') && ($forma_type=='all') && ($iway_code=='all'))
      {
	      $exce_header=array("TA LoginId","Centre code","Iway Address","Membership No","Name","Reasons","Others","Format Type");
      }
      else if(($examTime=='all') && ($forma_type!='all') && ($iway_code!='all'))
      {
	      $exce_header=array("TA LoginId","Exam Time","Iway Address","Membership No","Name","Reasons","Others");
      }
      else if(($examTime!='all') && ($forma_type!='all') && ($iway_code=='all'))
      {
	       $exce_header=array("TA LoginId","Centre code","Iway Address","Membership No","Name","Reasons","Others");
      }
      else if(($examTime!='all') && ($forma_type=='all') && ($iway_code!='all'))
      {
	       $exce_header=array("TA LoginId","Iway Address","Membership No","Name","Reasons","Others","Format_type");
	       
      }
      else if(($examTime!='all') && ($forma_type!='all') && ($iway_code!='all'))
      {
	       $exce_header=array("TA LoginId","Iway Address","Membership No","Name","Reasons","Others");
	       
      }
      
      
    		
    	if($populatecenter=='true' || $submitted=='true')
    	{
        	$sqlCenter = "select centre_code from iib_iway_details where exam_centre_code='".$examCentreCode."'";
        	$resCenter = @mysql_query($sqlCenter);

        	while(list($Iways)=mysql_fetch_row($resCenter))
        	{
	        	$Iways = strtoupper($Iways);
            	$aDispIways[$Iways] = $Iways;
            	
        	}
    	}
    	
/*** Execute the queries depends upon the examcentrecode and examdate ***/
    	
    	
	if (($examCentreCode != "") && ($examDate != "") && $submitted=='true')
	  {
        	switch($iway_code)
        	{

            	case "all":

                	if($examTime=='all')
                	{
                    	$sqlFormat = "select ta_login,a.centre_code,iway_address1,iway_address2,iway_city,membership_no,reasons,others,format_type,exam_time from iib_iway_details a,iib_format b where a.exam_centre_code='".$examCentreCode."' and a.centre_code=b.centre_code and b.exam_date='".$examDate."'";
                    	
                	}
                	else
                	{
                    	$sqlFormat = "select ta_login,a.centre_code,iway_address1,iway_address2,iway_city,membership_no,reasons,others,format_type,exam_time from iib_iway_details a,iib_format b where a.exam_centre_code='".$examCentreCode."' and a.centre_code=b.centre_code and b.exam_date='".$examDate."' and exam_time='".$examTime."'";
                    	
                	}
                	break;

            	default :

                	if($examTime=='all')
                	{
                   		 $sqlFormat = "select ta_login,a.centre_code,iway_address1,iway_address2,iway_city,membership_no,reasons,others,format_type,exam_time from iib_iway_details a,iib_format b where a.exam_centre_code='".$examCentreCode."' and a.centre_code=b.centre_code and b.exam_date='".$examDate."' and b.centre_code='".$iway_code."'";
                   		
                   	}
                	else
                	{
                    	$sqlFormat = "select ta_login,a.centre_code,iway_address1,iway_address2,iway_city,membership_no,reasons,others,format_type,exam_time from iib_iway_details a,iib_format b where a.exam_centre_code='".$examCentreCode."' and a.centre_code=b.centre_code and b.exam_date='".$examDate."' and exam_time='".$examTime."' and b.centre_code='".$iway_code."'";
                    	
                   }
                	break;
        	}   
        	
        if( ($_POST["format"] != "") && ($_POST["format"] != 'all'))
          	$sqlFormat .= " and format_type = '". $_POST["format"] ."'";

        	$sqlFormat .= " order by iway_city, a.centre_code, format_type ";
	        $resFormat = mysql_query($sqlFormat);
	        $ncount    = mysql_num_rows($resFormat);
		}
	$i=0;
	
/*** Store the retreiving values into an array $exce_content  ***/	
      if($ncount>0)
      {
		while(list($lid,$code,$iadd,$contactNo,$icity,$mem,$reasons,$others,$type,$etime) = mysql_fetch_row($resFormat))
		{
			$code = strtoupper($code); 
            $sqlName = "select name from iib_candidate where membership_no = '".$mem."'";
            $resName = @mysql_query($sqlName);
            list($Name) = mysql_fetch_row($resName);
            $iadd = str_replace('"',"'",$iadd);
		    $iadd="\"$iadd\"";
		    $Name = str_replace('"',"'",$Name);
		    $Name ="\"$Name\"";
            $exce_content[$i][0] = stripslashes($lid);
             if(($examTime=='all') && ($forma_type=='all') && ($iway_code=='all'))
             {
	                    $exce_content[$i][1] = stripslashes($code);
	           			$exce_content[$i][2] = stripslashes($etime);  
	           			$exce_content[$i][3] = stripslashes($iadd)." Contact No. :".stripslashes($contactNo);
               			$exce_content[$i][4] = stripslashes($mem);
			   			$exce_content[$i][5] = stripslashes($Name);
			   		if($reasons!='')
               			$exce_content[$i][6] = stripslashes($reasons); 
               		else
               		    $exce_content[$i][6] = "---";
               			$exce_content[$i][7] = stripslashes($others);
               			$exce_content[$i][8] = stripslashes($type);
             }
             else if(($examTime=='all') && ($forma_type=='all') && ($iway_code!='all'))
             {
	                    $exce_content[$i][1] = stripslashes($etime);  
	           			$exce_content[$i][2] = stripslashes($iadd)." Contact No. :".stripslashes($contactNo);
               			$exce_content[$i][3] = stripslashes($mem);
			   			$exce_content[$i][4] = stripslashes($Name);
			   		if($reasons!='')
               			$exce_content[$i][5] = stripslashes($reasons); 
               		else
               		    $exce_content[$i][5] = "---";	
               			$exce_content[$i][6] = stripslashes($others);
               			$exce_content[$i][7] = stripslashes($type);
             }
             else if(($examTime=='all') && ($forma_type!='all') && ($iway_code=='all'))
             {
	                    $exce_content[$i][1] = stripslashes($code);
	           			$exce_content[$i][2] = stripslashes($etime);  
	           			$exce_content[$i][3] = stripslashes($iadd)." Contact No. :".stripslashes($contactNo);
               			$exce_content[$i][4] = stripslashes($mem);
			   			$exce_content[$i][5] = stripslashes($Name);
               		if($reasons!='')
               			$exce_content[$i][6] = stripslashes($reasons); 
               		else
               		    $exce_content[$i][6] = "---";	
               			$exce_content[$i][7] = stripslashes($others);
             }
             else if(($examTime!='all') && ($forma_type=='all') && ($iway_code=='all'))
             {
	                    $exce_content[$i][1] = stripslashes($code);
	           			$exce_content[$i][2] = stripslashes($iadd)." Contact No. :".stripslashes($contactNo);
               			$exce_content[$i][3] = stripslashes($mem);
			   			$exce_content[$i][4] = stripslashes($Name);
               		if($reasons!='')
               			$exce_content[$i][5] = stripslashes($reasons); 
               		else
               		    $exce_content[$i][5] = "---";		
               			$exce_content[$i][6] = stripslashes($others);
               			$exce_content[$i][7] = stripslashes($type);
               			
             }
             else if(($examTime!='all') && ($forma_type!='all') && ($iway_code=='all'))
             {
	          			$exce_content[$i][1] = stripslashes($code);
	           			$exce_content[$i][2] = stripslashes($iadd)." Contact No. :".stripslashes($contactNo);
               			$exce_content[$i][3] = stripslashes($mem);
			   			$exce_content[$i][4] = stripslashes($Name);
               		if($reasons!='')
               			$exce_content[$i][5] = stripslashes($reasons); 
               		else
               		    $exce_content[$i][5] = "---";		
               			$exce_content[$i][6] = stripslashes($others);
               			$exce_content[$i][7] = stripslashes($type);   
             }
             else if(($examTime=='all') && ($forma_type!='all') && ($iway_code!='all'))
             {
	              		$exce_content[$i][1] = stripslashes($etime);  
	             		$exce_content[$i][2] = stripslashes($iadd)." Contact No. :".stripslashes($contactNo);
               			$exce_content[$i][3] = stripslashes($mem);
			   			$exce_content[$i][4] = stripslashes($Name);
               		if($reasons!='')
               			$exce_content[$i][5] = stripslashes($reasons); 
               		else
               		    $exce_content[$i][5] = "---";		
               			$exce_content[$i][6] = stripslashes($others);
             }
             else if(($examTime!='all') && ($forma_type=='all') && ($iway_code!='all'))
             {
	              		$exce_content[$i][1] = stripslashes($iadd)." Contact No. :".stripslashes($contactNo);
               			$exce_content[$i][2] = stripslashes($mem);
			   			$exce_content[$i][3] = stripslashes($Name);
               		if($reasons!='')
               			$exce_content[$i][4] = stripslashes($reasons); 
               		else
               		    $exce_content[$i][4] = "---";		
               			$exce_content[$i][5] = stripslashes($others);
               			$exce_content[$i][6] = stripslashes($type);   
             }
             else if(($examTime!='all') && ($forma_type!='all') && ($iway_code=='all'))
             {
	             		$exce_content[$i][1] = stripslashes($code);  
	             		$exce_content[$i][2] = stripslashes($iadd)." Contact No. :".stripslashes($contactNo);
               			$exce_content[$i][3] = stripslashes($mem);
			   			$exce_content[$i][4] = stripslashes($Name);
               		if($reasons!='')
               			$exce_content[$i][5] = stripslashes($reasons); 
               		else
               		    $exce_content[$i][5] = "---";		
               			$exce_content[$i][6] = stripslashes($others);
               			
             }
             else if(($examTime!='all') && ($forma_type!='all') && ($iway_code!='all'))
             {
	             		$exce_content[$i][1] = stripslashes($iadd)." Contact No. :".stripslashes($contactNo);
               			$exce_content[$i][2] = stripslashes($mem);
			   			$exce_content[$i][3] = stripslashes($Name);
               		if($reasons!='')
               			$exce_content[$i][4] = stripslashes($reasons); 
               		else
               		    $exce_content[$i][4] = "---";	
               			$exce_content[$i][5] = stripslashes($others);
	                             		
             }
	        $i++;        
  		}
	 
  		 
/*** Execution of excel download function   ***/

		if($ex_flag=='1')
   		{
  			excelconfig($filename,$exce_header,$exce_content);
  			exit;	 
    	}	
  }
?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='JavaScript' src="./includes/validations.js"></script>
<script language='JavaScript'>
function submitForm()
{
	if (document.frmmap.exam_centre_code.selectedIndex == 0)
	{		
		alert("Please select the exam centre");
		document.frmmap.exam_centre_code.focus();
		return false;
	}
	if (document.frmmap.exam_date.selectedIndex == 0)
	{		
		alert("Please select the exam date");
		document.frmmap.exam_date.focus();
		return false;
	}
    document.frmmap.submitted.value='true';
    document.frmmap.exl.value='0';
	document.frmmap.submit();
}

function selcentre()
{
    var f=document.frmmap;
    if(f.exam_centre_code.value!="")
    {
        f.populatecenter.value='true';
        f.exl.value='2';
        f.submit();
    }
    else
    {
        alert('Please Select A city!');
        return;
    }
}

function excelsubmit()
{
	var frmname=document.frmmap;
	frmname.submitted.value='true';
	frmname.exl.value='1';
	frmname.submit();
}


</script>

</HEAD>
<BODY leftMargin=0 topMargin=0 LeftMargin=0 marginheight=0 marginwidth=0>
<center>
<form name='frmmap' method='post'>
<input type='hidden' name='populatecenter'>
<input type='hidden' name='submitted'>
<input type=hidden name=exl value='0'>

<TABLE border=0 cellPadding=0 cellSpacing=0 width=780 align=center>
<!--	<TR><!-- Topnav Image -->
<!--        <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR> -->
	<tr><td width="780"><?include("includes/header.php");?></td></tr>
    <TR>
      
    </TR>
	<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>
    <TR>
    	<TD background=images/tile.jpg vAlign=top width=780 align=center>
    	
        	<table width=780 cellpadding=0 cellspacing=5 border=0>
        		<tr>
					<td colspan=2 align="center"><b><font face="Verdana, Arial, Helvetica, sans-serif" size=2>Format Report</b></font><br><br></td>
            	</tr>
            	<tr>
            		<td colspan=2 align="center" class="greybluetext10"><b><?=$msg ?></b></td>
            	</tr>
            	<tr>
	            	<td align="right" width=390 class="greybluetext10" valign=top>Exam Centre : </td>
    	        	<td align="left" valign=top class="greybluetext10">
        	    		<select name='exam_centre_code' class="greybluetext10" style={width:150px} onchange='javascript:selcentre();'>
            				<option value=''>--Select--</option>
            				<?php
            					while (list($code,$name) = mysql_fetch_row($rescentre))
            					{
	            					print("<option value='$code' ");
	            						if ($examCentreCode == $code)
	            						{
		            					print(" selected ");
	            						}
	            					print(">$name</option>");
            					}
            			?>
            			</select>
            	</td>
           </tr> 
           <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Exam Date : </td>
            	<td align="left" valign=top class="greybluetext10">
            		<select name='exam_date' class="greybluetext10" style={width:150px} >
            			<option value=''>--Select--</option>
            			<?php
            				foreach ($aDispDate as $key=>$val)
            				{
	            				print("<option value='$key' ");
	            				if ($examDate == $key)
	            				{
		            				print(" selected ");
	            				}
	            				print(">$val</option>");
            				}
            			?>
            		</select>
            	</td>
          </tr>   
          <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Exam Time : </td>
            	<td align="left" valign=top class="greybluetext10">
            		<select name='exam_time' class="greybluetext10" style={width:150px} >
            			<option value='all'>All Shifts</option>
            			<?php
            				foreach ($aDispTime as $key=>$val)
            				{
	            				print("<option value='$key' ");
	            				if ($examTime == $key)
	            				{
		            				print(" selected ");
	            				}
	            				print(">$val</option>");
            				}
            			?>
            		</select>
            	</td>
           </tr>
            <?
                $formatArray = array("A"=>"Format A","F" =>"Format F", "D" => "Format D");
            ?>
            <tr><td class="greybluetext10"  align=right>Format Type :</td>
                <td>
            		<select name='format' class="greybluetext10" style={width:150px} >
            			<option value='all'>All</option>
                        <?
            				foreach ($formatArray as $key=>$val)
                               if($key == $_POST ["format"]) 
                                    echo "<option selected value='$key'>$val</option>";
                               else
                                    echo "<option value='$key'>$val</option>";
                        ?>
                    </select>
                </td>
            </tr>
            <? if($populatecenter=='true' || $submitted=='true'){?>
              <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Center Code(Iway) : </td>
            	<td align="left" valign=top class="greybluetext10">
            		<select name='iway_code' class="greybluetext10" style={width:150px} >
            			<option value='all'>All Iways</option>
            			<?php
            				foreach ($aDispIways as $key=>$val)
            				{
	            				print("<option value='$key' ");
	            				if ($iway_code == $key)
	            				{
		            				print(" selected ");
	            				}
	            				print(">$val</option>");
            				}
            			?>
            		</select>
            	</td>
            </tr>
            <?}?>
      </table>   
    <table width=780 >       
        <tr>
			<td colspan=2 align="center"><input type=button name='sub_assign' value='Submit' class='button' onClick='javascript:submitForm()'>
			<!--&nbsp;&nbsp;<input type=button name='sub_unassign' value='UnAssign' class='button' onClick='javascript:unassignIways()'>-->
			</td>
        </tr>
        <?if(($submitted=='true') && ($ncount <1 )){?>
        <tr>
		<td width=780 align=center class=alertmsg>No Records to Display</td>
        </tr><?}?>
    </table>
    <?
/*** It checks with the flag value and it retrieving the data from the array and displaying the  Format report in the html page  **/
    $header_count = count($exce_header);
    if(($submitted=='true') && ($ncount > 0))
        {
        print("<table border=1 cellspacing=0 cellpadding=2>");
		for($a=0;$a<$header_count;$a++)
		{
			print("<td class=greybluetext10><b>$exce_header[$a]</b></td>");
		}
		print("</tr>");
		print("<tr>");
        
        /*** Displaying the Cafe capacity report in the html part. Retrieving the data from the array ***/
            
           
           for($j=0;$j<$ncount;$j++)
             {
	          for($k=0;$k<$header_count;$k++)
	           {
		           $chek = $header_count;
		           
		          ?><td class="greybluetext10"><?=str_replace('"','',$exce_content[$j][$k])?></td>
		          <?
	           }
	           print("</tr>");
             } 
                    
           ?>
            </table>
            <table width=780 background=images/tile.jpg>  
		    <tr><td class=greybluetext10>&nbsp;</td></tr>
         <tr><td class=greybluetext10 align="center" class=greybluetext10><input class='button' type='button' name='download' value='Download' onClick='javascript:excelsubmit()'></td></tr>
            <tr><td class=greybluetext10>&nbsp;</td></tr></table>
                        
       <?} ?>
         </td>      
    
	</tr>
    <TR>
    	<?include("includes/footer.php");?>
    </TR> 
	 
    	
	</TABLE>
</form>
</center>
</BODY>
</HTML>
