<?php
require_once "sessionchk.php";
require_once("dbconfig.php");
$subarray=array();
if ($_REQUEST["tMemNo"] == NULL){
	$memno ="";
	$centrecode ="";
	$sex ="";
	$address1 ="";
	$address2 ="";
	$address3 ="";
	$address4 ="";
	$city ="";
	$state ="";
	$zonecode ="";
	$statecode ="";
	$memname ="";
	$pincode ="";
	$inscode ="";
	$insname ="";
	$examcode="";
	$subjectcode="";
	$medium="";	
}
elseif ( $_REQUEST["disp"] == 1 ){
	$can_sql = "select distinct exam_code, a.membership_no,exam_centre_code,gender,address1,address2,address3,address4,city,state,zone_code,state_code,name,pin_code,a.institution_code,institution_name,medium_code from iib_candidate a,iib_exam_candidate b where a.membership_no='".$_REQUEST["tMemNo"]."' and b.membership_no = a.membership_no";
	$can_res = mysql_query($can_sql);
	if (mysql_num_rows($can_res)<1)	{
	$memno ="";
	$centrecode ="";
	$sex ="";
	$address1 ="";
	$address2 ="";
	$address3 ="";
	$address4 ="";
	$city ="";
	$state ="";
	$zonecode ="";
	$statecode ="";
	$memname ="";
	$pincode ="";
	$inscode ="";
	$insname ="";
	$examcode="";
	$subjectcode="";
	$medium="";	
	$errmsg = "No Record found for the Member number : ".$_REQUEST["tMemNo"];
	}
	else{
		list($examcode,$memno,$centrecode,$sex,$address1,$address2,$address3,$address4,$city,$state,$zonecode,$statecode,$memname,$pincode,$inscode,$insname,$medium)=mysql_fetch_row($can_res);	
		$cnt_sql = "select count(membership_no) from iib_candidate_test where membership_no = '".$_REQUEST["tMemNo"]."'";
		$cnt_res = mysql_query($cnt_sql);
		list($cnt_val) = mysql_fetch_row($cnt_res);
		$sql_can_cnt = "select count(membership_no) from iib_candidate_iway where membership_no = '".$_REQUEST["tMemNo"]."'";
		$res_can_cnt = mysql_query($sql_can_cnt);
		list($can_cnt) = mysql_fetch_row($res_can_cnt);
	}
}
else{	
	$memno = $_REQUEST["tMemNo"];
	$centrecode = $_REQUEST["sCentreCode"];
	$sex = $_REQUEST["rSex"];
	$address1 = $_REQUEST["taAddress1"];
	$address2 = $_REQUEST["taAddress2"];
	$address3 = $_REQUEST["taAddress3"];
	$address4 = $_REQUEST["taAddress4"];
	$city = $_REQUEST["tCity"];
	$state = $_REQUEST["tState"];
	$zonecode = $_REQUEST["tZoneCode"];
	$statecode = $_REQUEST["tStateCode"];
	$memname = $_REQUEST["tMemName"];
	$pincode = $_REQUEST["tPinCode"];
	$inscode = $_REQUEST["tInstitutionCode"];
	$insname = $_REQUEST["tInstitutionName"];
	$examcode=$_REQUEST["sExamCode"];
	$subjectcode=$_REQUEST["sSubjectCode"];
	$medium=$_REQUEST["sMedium"];
	$cnt_val=$_REQUEST["hCountVal"];
	$sublist=$_REQUEST["hSubList"];
	$can_cnt=$_REQUEST["hCountCan"];	
}
?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='JavaScript' src="./includes/validations.js"></script>
<script language="javascript">
function getvalues(){
frm = document.frmMember;
if (event.keyCode == 13 && frm.tMemNo.value != "" && frm.tMemNo.readOnly == false ){
frm.action="edit_candidate.php?disp=1";
frm.submit();
}
}
function populatesubject(){
	frm = document.frmMember;
	tmp="";
	for (i = 0; i < frm.sSubjectCode.length;i++)
		if (frm.sSubjectCode[i].selected==true)
		if (tmp == "")
			tmp = frm.sSubjectCode[i].value;
		else
			tmp = tmp+","+frm.sSubjectCode[i].value;
	frm.hSubjectCodes.value =tmp;
	frm.action="edit_candidate.php#examcode";
	frm.method="post";
	frm.submit();
}
function clearval(){
	frm = document.frmMember;
	frm.tMemNo.value = "";
	frm.action="edit_candidate.php";
	frm.submit();
}

/*
function validate(){
	frm = document.frmMember;
	tmp="";
	for (i = 0; i < frm.sSubjectCode.length;i++)
		if (frm.sSubjectCode[i].selected==true)
		if (tmp == "")
			tmp = frm.sSubjectCode[i].value;
		else
			tmp = tmp+","+frm.sSubjectCode[i].value;
	frm.hSubjectCodes.value =tmp;
	frm.action="inscandidate.php?id=2";
	frm.method="post";
	frm.submit();
} */
function chksplchar(value2chk){
myRegExp = new RegExp("[^a-zA-Z0-9 ]");
   	val=value2chk;
	result=val.match(myRegExp);
	if (result)
		return true;
	else
		return false;
}
function chkchar(value2chk){
myRegExp = new RegExp("[^a-zA-Z ]");
   	val=value2chk;
	result=val.match(myRegExp);
	if (result)
		return true;
	else
		return false;
}
function chknum(value2chk){
myRegExp = new RegExp("[^0-9]");
   	val=value2chk;
	result=val.match(myRegExp);
	if (result)
		return true;
	else
		return false;
}

function Deleteval(){
	frm = document.frmMember;
	if (trim(frm.tMemNo.value) == ""){
	alert ("Sorry no record selected, Nothing to delete");
	frm.tMemNo.focus();
	return false;
	}
	if (frm.hCountVal.value > 0){
		alert("Sorry, this user has already taken up a exam, or currently attending a exam. Cant delete details");
		return false;
	}
	if (confirm("Are u sure?")){
		tmp = Math.round(((1 - 0.5) +( ( (5+0.49999) - (1-0.5) ) * Math.random() ) )*100000);
		frm.hTmp.value = tmp;
		frm.action="inscandidate.php?id=3";
		frm.method="post";
		frm.submit();
	}
}
function validate(){
	frm = document.frmMember;
	tmp="";
/*	if (trim(frm.tMemNo.value) == ""){
		alert("Please type a member number and press enter to view the details");
		frm.tMemNo.focus();
		return;
	}
	if (chksplchar(frm.tMemNo.value)){
		alert("Member Number Can accept only Alphanumeric text");
		frm.tMemNo.focus();
		return;
	}
	if (trim(frm.tMemName.value) == ""){
		alert("Member Name Should Not Be Empty");
		frm.tMemName.focus();
		return false;
	}
	if (chkchar(frm.tMemName.value)){
		alert("Member Name Can accept only Alphabets text");
		frm.tMemName.focus();
		return;
	}
	if(!frm.rSex[0].checked && !frm.rSex[1].checked){
		alert("Please select Gender");	
		frm.rSex[0].focus();
		return false;
	}
	a = frm.taAddress1.value+'a';
	a = trim(a);
//	if (trim(frm.taAddress1.value) == null)
	if (a.length == 1){
		alert("Address1 Should Not Be Empty");
		frm.taAddress1.focus();
		return false;
	}
	if (trim(frm.taAddress2.value) == ""){
		alert("Address2 Should Not Be Empty");
		frm.taAddress2.focus();
		return false;
	}
	if (trim(frm.taAddress3.value) == ""){
		alert("Address3 Should Not Be Empty");
		frm.taAddress3.focus();
		return false;
	}
	if (trim(frm.taAddress4.value) == ""){
		alert("Address4 Should Not Be Empty");
		frm.taAddress4.focus();
		return false;
	}	
	if (trim(frm.tCity.value) == ""){
		alert("City Should Not Be Empty");
		frm.tCity.focus();
		return false;
	}
/*	if (chksplchar(frm.tCity.value)){
		alert("City Can accept only Alphanumeric text");
		frm.tCity.focus();
		return;
	}	 
	if (trim(frm.tState.value) == ""){
		alert("State Should Not Be Empty");
		frm.tState.focus();
		return false;
	}
/*	if (chksplchar(frm.tState.value)){
		alert("State Can accept only Alphanumeric text");
		frm.tState.focus();
		return;
	}		 
	if (trim(frm.tStateCode.value) == ""){
		alert("StateCode Should Not Be Empty");
		frm.tStateCode.focus();
		return false;
	}
/*	if (chksplchar(frm.tStateCode.value)){
		alert("StateCode Can accept only Alphanumeric text");
		frm.tStateCode.focus();
		return;
	}			 
	if (trim(frm.tPinCode.value) == ""){
		alert("PinCode Should Not Be Empty");
		frm.tPinCode.focus();
		return false;
	}
	if (chknum(frm.tPinCode.value)){
		alert("PinCode Can accept only Alphanumeric text");
		frm.tPinCode.focus();
		return;
	}				
	
	if (trim(frm.tZoneCode.value) == ""){
		alert("ZoneCode Should Not Be Empty");
		frm.tZoneCode.focus();
		return false;
	} 
	if (chksplchar(frm.tZoneCode.value)){
		alert("ZoneCode Can accept only Alphanumeric text");
		frm.tZoneCode.focus();
		return;
	}		
	if (frm.sExamCode.selectedIndex == 0){
		alert("Select a Exam");
		frm.sExamCode.focus();
		return false;
	}
//	alert(frm.sSubjectCode.length);
	if (!frm.sSubjectCode.length){
	alert("No Subjects Listed, Please check the Exam");
		frm.sExamCode.focus();
		return false;
	}	
	else{
		j=0;
		for (i=0;i<frm.sSubjectCode.length;i++)
			if (frm.sSubjectCode[i].selected == true)
				j++;
		if (j == 0){
			alert("At least one Subject has to be selected");
			frm.sSubjectCode.focus();
		return false;
		}
	}
	if (frm.sCentreCode.selectedIndex == 0){
		alert("Select a Centre Code");
		frm.sCentreCode.focus();
		return false;
	}
	if (frm.sMedium.selectedIndex == 0){
		alert("Select a Medium");
		frm.sMedium.focus();
		return false;
	}
	if (trim(frm.tInstitutionCode.value) == ""){
		alert("Institution Code Should Not Be Empty");
		frm.tInstitutionCode.focus();
		return false;
	}
	if (chksplchar(frm.tInstitutionCode.value)){
		alert("Institution Code Can accept only Alphanumeric text");
		frm.tInstitutionCode.focus();
		return;
	}		
	if (trim(frm.tInstitutionName.value) == ""){
		alert("Institution Name Should Not Be Empty");
		frm.tInstitutionName.focus();
		return false;
	}		
/*	if (chksplchar(frm.tInstitutionName.value)){
		alert("Institution Name Can accept only Alphanumeric text");
		frm.tInstitutionName.focus();
		return;
	}		*/
	tmp="";
	for (i = 0; i < frm.sSubjectCode.length;i++)
		if (frm.sSubjectCode[i].selected==true)
		if (tmp == "")
			tmp = frm.sSubjectCode[i].value;
		else
			tmp = tmp+","+frm.sSubjectCode[i].value;
	frm.hSubjectCodes.value =tmp;
//	alert(frm.hSubjectCodes.value+" - "+frm.hSubList.value);
	if ((frm.hCountCan.value > 0)&&(trim(frm.hSubjectCodes.value) != trim(frm.hSubList.value))){
		alert("Exam and Subjects cannot be edited as the candidate is already alloted centre");
		frm.sExamCode.focus();
		return;	
	}
	tmp = Math.round(((1 - 0.5) +( ( (5+0.49999) - (1-0.5) ) * Math.random() ) )*100000);
	frm.hTmp.value = tmp;
	frm.action="inscandidate.php?id=2";
	frm.method="post";
	frm.submit();
}
</script>
</HEAD>
<BODY leftMargin=0 topMargin=0 LeftMargin=0 marginheight=0 marginwidth=0>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
<!--	<TR><!-- Topnav Image -->
<!--        <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR> -->
	<tr><td width="780"><?include("includes/header.php");?></td></tr>
    <TR>
        
    </TR>
	<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>
    <TR>
    	<TD background=images/tile.jpg vAlign=top width=780 align=center>
    	<form name='frmMember' method="post" onsubmit="return false;">        
        <table width=780 cellpadding=0 cellspacing=5 border=0>
        	<tr>
				<td colspan=2 align="center"><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2>Edit/Delete Candidate Details</b></font></td>
            </tr>                
			<? if ($errmsg <> NULL ){ ?>
        	<tr>
				<td colspan=2 align="center" class="alertmsg"><? echo $errmsg; ?></td>
            </tr>                
			<? }
			?>
			<tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Member Number : </td>
                <td align="left" valign=top><input type="text" class="textbox" size=30 name="tMemNo" value=<? echo "'".$memno."'"; if ($memno<> NULL) echo " readonly"; ?> onkeypress="getvalues()"; maxlength=20></td>
			</tr>
			<tr>
                <td align="right" width=390 class="greybluetext10" valign=top>Member Name : </td>
                <td align="left" valign=top><input type="textbox" class="textbox" size=30 name="tMemName" value='<? echo $memname; ?>'></td>
			</tr> 
			<tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Gender :&nbsp;</td>
                <td align="left" valign=top class="greybluetext10" >
				<input type="radio" value='M' name=rSex <? if ($sex == 'M') echo "checked"; ?>> Male &nbsp;&nbsp;
				<input type="radio" value='F' name=rSex <? if ($sex == 'F') echo "checked"; ?>>Female</td>
			</tr>   
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top> Address1 :&nbsp;</td>
                <td align="left" valign=top><textarea class="textarea" name="taAddress1" cols=30 rows=5 ><? echo $address1; ?></textarea></td>
			</tr>
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top> Address2 :&nbsp;</td>
				<td align="left" valign=top><textarea class="textarea" name="taAddress2" cols=30 rows=5><? echo $address2; ?></textarea></td></td>
			</tr>
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top> Address3 :&nbsp;</td>
                <td align="left" valign=top><textarea class="textarea" name="taAddress3" cols=30 rows=5><? echo $address3; ?></textarea></td>
			</tr>
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top> Address4 :&nbsp;</td>
                <td align="left" valign=top><textarea class="textarea" name="taAddress4" cols=30 rows=5><? echo $address4; ?></textarea></td>
			</tr>
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top> Address5/City :&nbsp;</td>
                <td align="left" valign=top><input type="textbox" class="textbox" size=30 name="tCity" value='<? echo $city; ?>'></td>
			</tr>
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top> Address6/State :&nbsp;</td>
				
                <td align="left" valign=top><input type="textbox" class="textbox" size=30 name="tState" value='<? echo $state; ?>'></td>
			</tr>
            <tr>
				<td align="right" width=390 class="greybluetext10" valign=top> State Code :&nbsp;</td>
                <td align="left" valign=top><input type="text" class="textbox" size=30 name="tStateCode" value='<? echo $statecode; ?>' maxlength=20></td>
            </tr>    
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top> Pin Code : </td>
                <td align="left" valign=top><input type="text" class="textbox" size=30 name="tPinCode" value='<? echo $pincode; ?>' maxlength=20></td>
			</tr>
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top> Zone Code : </td>
                <td align="left" valign=top><input type="text" class="textbox" size=30 name="tZoneCode" value='<? echo $zonecode; ?>' maxlength=20></td>
			</tr>
			<tr>
               	<td align="right" width=390 class="greybluetext10" valign=top><a name=examcode></a> Exam Code : </td>
            	<td align="left" valign=top><select name="sExamCode" onchange="populatesubject()" class="textbox" style="width:165;">
				<option value=select selected>-Select-</option>
				<?
				$exam_sql =	"select distinct exam_code,exam_name from iib_exam where online = 'Y'";
				$exam_res=mysql_query($exam_sql);
				while (list($all_exam_code,$exam_name)=mysql_fetch_row($exam_res)){
					if ($all_exam_code == $examcode)
						echo "<option value='$all_exam_code' selected>$all_exam_code --> $exam_name</option>";
					else
						echo "<option value='$all_exam_code'>$all_exam_code --> $exam_name</option>";
				}
				?>
				</select>
				</td>
			</tr>
			
			<tr>
               	<td align="right" width=390 class="greybluetext10" valign=top>Subjects Appearing : </td>
            	<td align="left" valign=top><select name="sSubjectCode" multiple class="textbox" style="width:165;height:100">
				<?
				
				if ($examcode <> NULL){
				$all_sub_sql = "select subject_code,subject_name from iib_exam_subjects where exam_code='$examcode' and online = 'Y'";
				$mem_sub_sql = "select subject_code from iib_exam_candidate where membership_no='$memno' and exam_code='$examcode'";
				$all_sub_res=mysql_query($all_sub_sql);
				$mem_sub_res=mysql_query($mem_sub_sql);				
				$i=0;
				while (list($subarray[$i]) = mysql_fetch_row($mem_sub_res)){
				if ($i==0)
					$sublist = $subarray[$i++];				
				else
					$sublist = $sublist.",".$subarray[$i++];
				}
				while (list($subject_code,$subject_name)=mysql_fetch_row($all_sub_res)){
					$avail = 1;
					$j = 0;
					while ($subarray[$j] != NULL){
							if ($subarray[$j] == $subject_code){
							$avail = 2;
						}
					$j++;						
					}
					if ($avail == 2)
						echo "<option value='$subject_code' selected>$subject_code --> $subject_name</option>";
					elseif ($avail==1)
						echo "<option value='$subject_code'>$subject_code --> $subject_name</option>";
				}
				}
				?>
				</select>
				</td>
			</tr>
			<tr>
				<td align="right" width=390 class="greybluetext10" valign=top>Exam Centre Code : </td>
                <td align="left" valign=top>
				<select name="sCentreCode" class="textbox" style="width:165;">
				<option value=select selected>-Select-</option>
				<?
				$centre_sql =	"select exam_centre_code,exam_centre_name from iib_exam_centres where online = 'Y'";
				$centre_res=mysql_query($centre_sql);
				while (list($exam_centre_code,$exam_centre_name)=mysql_fetch_row($centre_res)){
					if ($exam_centre_code == $centrecode)
						echo "<option value='$exam_centre_code' selected>$exam_centre_code --> $exam_centre_name</option>";
					else
						echo "<option value='$exam_centre_code'>$exam_centre_code --> $exam_centre_name</option>";
				}
				?>
				</select>
				</td>
			</tr>                    
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top> Medium : </td>
                <td align="left" valign=top>
				<select name=sMedium class="textbox" style="width:165;">
				<option value=select selected>-Select-</option>
				<option value="H" <? if ($medium=="H" || $medium=="HINDI") echo "selected";?>>HINDI</option>
				<option value="E" <? if ($medium=="E" || $medium=="ENGLISH") echo "selected";?>>ENGLISH</option>				
				</select>
				</td>
			</tr>
            <tr>
               	<td align="right" width=390 class="greybluetext10" valign=top> Institution Code : </td>
                <td align="left" valign=top><input type="text" class="textbox" size=30 name="tInstitutionCode" value='<? echo $inscode; ?>'></td>
			</tr>
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top> Institution Name : </td>
                <td align="left" valign=top><input type="text" class="textbox" size=30 name="tInstitutionName" value='<? echo $insname ; ?>'></td>
			</tr>
			<input type=hidden name=hSubjectCodes value="">
			<tr>
				<td colspan=2 align=center>
				<input class="button" type="button" name="sub_save" value="Save" onclick="javascript:validate();">&nbsp;&nbsp;
				<input class="button" type="reset" name="reset" value="Reset" >&nbsp;&nbsp;
				<input class="button" type="button" name="bDelete" value="Delete" onclick="return Deleteval();">&nbsp;&nbsp;
				<input class="button" type="button" name="bCancel" value="Cancel" onclick="javascript:clearval();">
				</td>
			</tr>
		<input type=hidden name=hCountVal value='<? echo $cnt_val; ?>' readonly>
		<input type=hidden name=hSubList value='<? echo $sublist; ?>' readonly>
		<input type=hidden name=hCountCan value='<? echo $can_cnt; ?>' readonly>		
		<input type=hidden name=hTmp>									
		</table>
        </form>
        </TD>
	</TR>
    <TR>
    	<?include("includes/footer.php");?>
    </TR>
</TABLE>
</FORM>
</center>
</BODY>
</HTML>