<?php
$admin_flag=1;
require_once("sessionchk.php");
require_once("dbconfig.php");
require("constants.inc");
set_time_limit(0);
$page = ($_REQUEST["page"])?$_REQUEST["page"]:1;
$examCode = $_POST['exam_code'];
$mediumCode = $_POST['medium_code'];
$subjectCode = $_POST['subject_code'];
$sql_cnt = "SELECT count(question_paper_no) FROM iib_question_paper WHERE exam_code='$examCode' AND subject_code='$subjectCode' ".
"AND medium_code='$mediumCode' AND online='N' AND assigned='N' ";
$res_cnt = @mysql_query($sql_cnt);
list($num_ques) = @mysql_fetch_row($res_cnt);
$pagecount = $num_ques;
$num_rec_per_page = 25;
?>
<html>
<head>
    <title><?=PAGE_TITLE ?></title>
    <script language='JavaScript'>
    function downloadPaper(qp,alt_m)
    {
	    document.frmpaper.qp_no.value = qp;
	    document.frmpaper.alt_medium.value = alt_m;
	    document.frmpaper.action = 'offline_paper.php';
	    document.frmpaper.submit();
    }
    function downloadAnswer(qp)
    {
	    document.frmpaper.qp_no.value = qp;	    
	    document.frmpaper.action = 'offline_answerkey.php';
	    document.frmpaper.submit();
    }
	function callpage(a)
	{	
		document.frmpaper.action = "offline_papers_display.php?page="+a;
		document.frmpaper.submit();
	}
    </script>
</head>
<link rel="stylesheet" href="images/iibf.css" type="text/css">
<body leftmargin="0" topmargin="0">
<center>
<form name=frmpaper method=post>
  <table width="780" border="0" cellspacing="0" cellpadding="0" >
    <tr>
<!--      <td width="780"><img src="images/logo1.jpg" width="136" height="59"><img src="images/logo2.gif" width="284" height="59"><img src="images/logo3.gif" width="360" height="59"></td>
    </tr> -->
    <tr>
         <? include("includes/header.php"); ?>
    </tr>
    <tr>
		<Td background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
  </tr>
    <tr>
      <td width="780" background="images/tile.jpg" valign="top">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="24"> <div align="center" ><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">View Offline Question Paper(s)</font> </strong></div></td>
          </tr>
          <tr>
            <td height="200" valign="middle" align="center">
<?php      
	print("<input type='hidden' name='exam_code' value='$examCode'>");
    print("<input type='hidden' name='medium_code' value='$mediumCode'>");    
    print("<input type='hidden' name='subject_code' value='$subjectCode'>");
    
    $sqlMarks = "SELECT total_marks FROM iib_exam_subjects WHERE exam_code='$examCode' AND subject_code='$subjectCode' ";
    $resMarks = @mysql_query($sqlMarks);
 	list($totalMarks) = @mysql_fetch_row($resMarks);
    print("<input type='hidden' name='total_marks' value='$totalMarks'>");

	if ($page == 1)	
		$startpos = 0;
	 else
	 	$startpos = ($page-1) * $num_rec_per_page;
	$sqlQP = "SELECT question_paper_no FROM iib_question_paper WHERE exam_code='$examCode' AND subject_code='$subjectCode' ".
    	" AND medium_code='$mediumCode' AND online='N' AND assigned='N' ORDER BY question_paper_no DESC  limit $startpos, $num_rec_per_page";
	$resQP = @mysql_query($sqlQP);
	$nPapers = @mysql_num_rows($resQP);
	if ($nPapers > 0)
	{	
        print("<br><table width=75% align=center cellpadding=3 cellspacing=0 border=1>");
        print("<tr><td class=greybluetext10 colspan=3>To download, click link(s) provided below</td></tr>");
        print("<tr><td class=greybluetext10 colspan=3>Selected Medium : {$aMedium[$mediumCode]}</td></tr>");
        if (($mediumCode == "E") || ($mediumCode == "ENGLISH"))
        {
	        $altMedium = "H";
        }
        else
        {
	        $altMedium = "E";
        }
		print("<tr><td class=greybluetext10>{$aMedium[$mediumCode]}</td><td class=greybluetext10>Equivalent {$aMedium[$altMedium]}</td><td class=greybluetext10>Answer Key</td></tr>");		
       	while (list($questionPaperNo) = @mysql_fetch_row($resQP))
        {	        
	        print("<tr><td class=greybluetext10 ><a href='javascript:downloadPaper(".$questionPaperNo.",0)'>Question Paper ".$questionPaperNo."</a></td>".
	        "<td class=greybluetext10 ><a href='javascript:downloadPaper(".$questionPaperNo.",1)'>Question Paper ".$questionPaperNo."</a></td>".
	        "<td class=greybluetext10><a href='javascript:downloadAnswer(".$questionPaperNo.")'>Answer Key ".$questionPaperNo."</a></td>".
	        "</tr>");
        }
        print("</table>");
        print("<input type=hidden name=qp_no value=''>");
        print("<input type=hidden name=alt_medium value=''>");
		?>
		<tr>
			<td><br></td>
		</tr>
		<tr align="center">
			<td  class="greybluetext10">
		 	<?php
			if ($nPapers > 0){
				$pagecount = ceil($pagecount / $num_rec_per_page);			
		 		$pagecounter = 0;
		 		$pageend = 0;
		 		$middle = $page;
		 		if (($page - 2) < 1) {
			 		$pagecounter = 1;
			 		$middle = 3;
			 	} 
			 	else
			 		$pagecounter = $page - 2;
				if (($middle + 2) > $pagecount){
   	       	        $pageend = $pagecount;
					$middle = $pagecount-2;
				 	$pagecounter = $middle - 2;
				 } 
				 else	
				 	$pageend = $middle + 2 + $pageend;
				 if ($pagecounter < 1)
				 	$pagecounter = 1;
				 if ($pageend > $pagecount)
				 	$pageend = $pagecount;
				 if ($page > 1)
				 	echo "<a href=javascript:callpage(" . ($page - 1) . ")><<</a>" ;
				 else
				 	echo "<<";
				 for($i = $pagecounter; $i <= $pageend; $i++) {
					if ($i == $page)		
						echo "[$i]";
					 else
					 	echo "<a href=javascript:callpage(" . $i . ")>[$i]</a>";
					 } 
				if ($page < $pagecount)
					echo "<a href='javascript:callpage(" . ($page + 1) . ")'>>></a>";
				else
					echo ">>";
				}
				?>
			</td>
		</tr>
	<?
    }
    else
    {
        print("<div class='alertmsg'><center>No Question Papers generated for the selected subject</center></div>");
//        return;
    }
?>
           </td>
          </tr>
        </table>
        </td>
    </tr>
    <tr>
    	<td width="780" background="images/tile.jpg" valign="top"><br><br></td>	
    </tr>
   <!-- <tr>
      <? include("includes/footer.php");?>
    </tr> -->
  </table>
   <table width="100%"  border=0 bgcolor="004a80">
  <tr height="10" width="100%">
       <td width="100%">  <? include("includes/footer.php");?> </td>
 	</tr>
</table>
</form>
</center>
</body>
</html>