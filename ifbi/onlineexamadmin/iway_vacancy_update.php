<?
/*******************************************************
* Application Name            :  IIB
* Module Name                 : Iway vacancy update
* Revision Number             :  1
* Revision Date               :
* Table(s)  modified                  :  
* Tables used for only selects :  iib_exam_centres,iib_exam_schedule,iib_exam_slots, iib_iway_details,iib_iway_vacancy,
* Tables used for only modified/inserted : iib_iway_vacancy,iib_iway_vacancy_log
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              : 
* Document/Reference Material :
* Created By : Anil Kumar.A
* Created On : 27/03/2006 
* Last Modified By            :  
* Last Modified Date          : 
* Description                 :  Interface to update the iway vacancy - remaining seats
*******************************************************/
include "dbconfig.php";
require_once "sessionchk.php";
//posted values
$seat_add=isset($_POST['seat_add'])?$_POST['seat_add']:0;
$examCentreCode = isset($_POST['sExam_centre_code'])?$_POST['sExam_centre_code']:0;
$centreCode = isset($_POST['sCentre_code'])?$_POST['sCentre_code']:0;
$examTime=isset($_POST['sExam_time'])?$_POST['sExam_time']:0;
$examDate=isset($_POST['sExam_date'])?$_POST['sExam_date']:0;

//variables got from query string
$GET_disp_can=isset($_GET['disp_can'])?$_GET['disp_can']:0;
$GET_move=isset($_GET['move'])?$_GET['move']:0;


//hidden variable holding remaining seats
$rem=isset($_POST['remaining'])?$_POST['remaining']:0;
$update_flag=isset($_POST['updated'])?$_POST['updated']:0;

//to calculate the total remaining seats to be updated in the table-iway_vacancy
$total_rem=$seat_add+$rem;

//Intializing the msg_disp variable
$msg_disp=0;


$sql_exam_centre="select exam_centre_code, exam_centre_name from iib_exam_centres where online='Y' order by exam_centre_name";
//echo $sql_exam_centre;
$res_exam_centre=mysql_query($sql_exam_centre);
if (!$res_exam_centre)
{
        $err=1;
        $emsg="Error in selecting exam centres.";
        $displaybug='true';
}
if($examCentreCode!='')
{
$sqlIWay = "SELECT centre_code, iway_name FROM iib_iway_details WHERE exam_centre_code='$examCentreCode' order by centre_code ";
$resIWay = @mysql_query($sqlIWay);
if (mysql_error()){
	$err=1;
        $emsg="Error in selecting I Ways.";
        $displaybug='true';
}
$nIWay = mysql_num_rows($resIWay);

$sqlDate = "SELECT distinct exam_date FROM iib_exam_schedule order by exam_date ";
$resDate = mysql_query($sqlDate);

$sqlTime ="SELECT slot_time FROM iib_exam_slots order by slot_time";
$resTime = mysql_query($sqlTime);
$nTime = mysql_num_rows($resTime);
if ($nTime > 0){
	while (list($eTime) = mysql_fetch_row($resTime)){
		$aDispTime[$eTime] = substr($eTime,0,5);
	}
}
}

// selection of remaining seats will happen here
if($GET_disp_can=='1' && $GET_move=='0')
{
	//echo "hi";
	//print_r($_POST);
	$sql_select_iway="select remaining from iib_iway_vacancy where centre_code ='$centreCode' and exam_date ='$examDate' and exam_time ='$examTime' ";
	$res_select_iway=mysql_query($sql_select_iway);
	if(mysql_num_rows($res_select_iway) > 0)
	{
		list($rem)=mysql_fetch_row($res_select_iway);
	}
	else
	{
		$msg_disp=2;
	}
}

// Update will happen here
if($update_flag=='true' && $GET_disp_can=='1' && $GET_move=='1')
{
	$sql_update_iway="update iib_iway_vacancy set remaining='$total_rem' where centre_code='$centreCode' and exam_date ='$examDate' and exam_time ='$examTime' ";
	$res_update_iway=mysql_query($sql_update_iway);
	
	//if update statement executed sucessfully then insert into log table and select remaining seats and display it
	if($res_update_iway==true)
	{
		//inserting log for updates on iway vacancy
		$sql_insert_log="insert into iib_iway_vacancy_log (centre_code,exam_date,exam_time,old_remaining,seats_added,updatedtime) values ('$centreCode','$examDate','$examTime','$rem','$seat_add',now())";
		$res_insert_log=mysql_query($sql_insert_log);
		
		//selecting the remaining seats iway vacancy
		$sql_select_iway_after_update="select remaining from iib_iway_vacancy where centre_code ='$centreCode' and exam_date ='$examDate' and exam_time ='$examTime' ";
		$res_select_iway_after_update=mysql_query($sql_select_iway_after_update);
		list($rem_after_update)=mysql_fetch_row($res_select_iway_after_update);
		
		//display msg on sucessful operations
		$msg_disp=1;
	}
	else
	{
		//display error msg about updation
		$msg_disp=3;
	}
}



//Error messages defined here
if($msg_disp==1)
{
	$msg="Record sucessfully updated. The remaining seats are $rem_after_update ";
}
else if($msg_disp==2)
{
	$msg="No records found";
}
else if($msg_disp==3)
{
	$msg="Error in updation ";
}

?>
<html>
<head>
<script language='JavaScript' src="./includes/validations.js"></script>
<script language='JavaScript'>
/*function getCentres()
{
        document.iway_vacancy.action = "iway_vacancy_update.php?disp_can=0&move=0";
        document.iway_vacancy.submit();
}*/
function getIwayDetails()
{
        document.iway_vacancy.action = "iway_vacancy_update.php?disp_can=0&move=0";
	document.iway_vacancy.submit();
}
function GetCandidate(){
	//alert("GetCandidate");
	frm=document.iway_vacancy;
	if (frm.sExam_centre_code.selectedIndex == 0){
		alert("Please select a Exam centre");
		frm.sExam_centre_code.focus();
		return false;
	}
	if (frm.sCentre_code.selectedIndex == 0){
		alert("Please select a cafe");
		frm.sCentre_code.focus();
		return false;
	}
 	if (frm.sExam_date.selectedIndex == 0){
		alert("Please select a date");
		frm.sExam_date.focus();
		return false;
	}
 	if (frm.sExam_time.selectedIndex == 0){
		alert("Please select a time");
		frm.sExam_time.focus();
		return false;
	}
	document.iway_vacancy.action = "iway_vacancy_update.php?disp_can=1&move=0";
	document.iway_vacancy.updated.value='false';
	document.iway_vacancy.submit();
}
function updateVacancy()
{
	//alert("updateVacancy");
	frm=document.iway_vacancy;
	if (frm.sExam_centre_code.selectedIndex == 0){
		alert("Please select a Exam centre");
		frm.sExam_centre_code.focus();
		return false;
	}
	if (frm.sCentre_code.selectedIndex == 0){
		alert("Please select a cafe");
		frm.sCentre_code.focus();
		return false;
	}
 	if (frm.sExam_date.selectedIndex == 0){
		alert("Please select a date");
		frm.sExam_date.focus();
		return false;
	}
 	if (frm.sExam_time.selectedIndex == 0){
		alert("Please select a time");
		frm.sExam_time.focus();
		return false;
	}
	if(frm.seat_add.value=='')
	{
		alert("Please enter the no. of seats to be added");
		frm.seat_add.focus();
		return false;
	}
	else
	{
		myRegExp = new RegExp("^-?[0-9]*$");
		val=frm.seat_add.value;
		if (!myRegExp.test(val))
		{
			alert("No.of seats to be added can accept only +ve or -ve Number ");
			frm.seat_add.focus();
			return false;
		}
		else
		{
			if(val.substr(2,1)=='-'  || val.substr(1,1)=='-' )
			{
					alert("No.of seats to be added can accept only +ve or -ve Number ");
					frm.seat_add.focus();
					return false;
			}
	
			sum_of_seats= eval(frm.remaining.value) + eval(frm.seat_add.value);
			if(eval(sum_of_seats) < 0 )
			{
				alert("Cannot change the no.of remaining seats to lesser than zero");
				frm.seat_add.focus();
				return false;
			}
		}
		
	}
	//frm.total_rem.value=sum_of_seats;
	document.iway_vacancy.action = "iway_vacancy_update.php?disp_can=1&move=1";
	document.iway_vacancy.updated.value='true';
	document.iway_vacancy.submit();
}

function frmsub()
{
  	frm=document.iway_vacancy;
	  if (window.event.keyCode == 13)
		{
			if(!updateVacancy())
				return false; 
		}
			
}

</script>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
</STYLE>
</head>
<body leftMargin=0 topMargin=0 marginheight=0 marginwidth=0>
<center>
<form name=iway_vacancy method=post>
<input type=hidden name=updated value=''>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
<!--	<tr>
               <TD width=780 colspan=2><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
        </TR> -->
		 <tr><td width="780"><?include("includes/header.php");?></td></tr>
        <TR>
        
        </TR>
      <TABLE border=0 cellPadding=0 cellSpacing=0 width=780 background=images/tile.jpg>
        <tr>
                <Td class="greybluetext10" colspan=2 background=images/tile.jpg><? include("admin_menu.php") ?></Td>
        </tr>
                <TR>
        <TD  width=780 colspan=2>&nbsp;</TD>
        </TR>
	<tr>
		<td colspan=2 align="center" colspan=2><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2>Edit IWay Vacancy</b></font></td>
		<td></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
        <tr>
        	<td align="right" width=390 class="greybluetext10" valign=top>Select Exam Centre :&nbsp;</td>
                <td align="left" valign=top>
                <select class="greybluetext10" name="sExam_centre_code" style={width:250px} onChange='javascript:getIwayDetails()'>
               		<option value=''>-Select-</option>
                       	<?
                       	while(list($exam_centre_code,$exam_centre_name) = mysql_fetch_row($res_exam_centre)){
                       		$exam_centre_name=stripslashes($exam_centre_name);
                                if ($exam_centre_code == $examCentreCode){
                                	echo "<option value=\"$exam_centre_code\" selected> $exam_centre_name</option>";
                               }
                               else{
                                  echo "<option value=\"$exam_centre_code\">$exam_centre_name</option>";
                               }
                        }
                        ?>
		</select>
 		</td>
	</tr>
	<tr>
        	<td align="right" width=40% class="greybluetext10" valign=top>Select I Way :&nbsp;</td>
                <td align="left" valign=top>
                <select class="greybluetext10" name="sCentre_code" style={width:250px} onChange='javascript:getIwayDetails()'>
                	<option value='0'>-Select-</option>
                        <?php
                        if ($nIWay > 0){
                        	while (list($centre_code, $iway_name) = mysql_fetch_row($resIWay)){
	                                if ($centre_code == $centreCode){
						echo "<option value=\"$centre_code\" selected>$centre_code - $iway_name</option>";
                                        }
                               		else{
                                        	echo "<option value=\"$centre_code\">$centre_code - $iway_name</option>";
                                        }
                        	}
                       	}
                	?>
		</td>
	</tr>
	 <tr>
                <td align=right class="greybluetext10">Exam Date : </td>
	        <td class="greybluetext10"><select name='sExam_date' style={width:150px} class=greybluetext10 onChange='javascript:getIwayDetails()'>
        	        <option value='0'>--Select--</option>
			<?php
			if (mysql_num_rows($resDate) > 0){
			        while (list($eDate) = mysql_fetch_row($resDate)){
			                $aDate = explode("-",$eDate);
			                $dispDate = $aDate[2]."-".$aDate[1]."-".$aDate[0];
			                print("<option value='$eDate' ");
			                if ($eDate == $examDate)
			                        print(" selected ");
			                print(">$dispDate</option>");
				}
			}
			?>
                </td>
        </tr>
        <tr>
        	<td align="right" width=390 class="greybluetext10" valign=top>Exam Time : </td>
                <td align="left" valign=top class="greybluetext10">
                        <select name='sExam_time' class="greybluetext10"    style={width:150px}  onChange='javascript:GetCandidate()'>
                                <option value='0'>Select</option>
                                <?php
                                        foreach ($aDispTime as $key=>$val)
                                        {
                                                print("<option value='$key' ");
                                                if ($examTime == $key)
                                                {
                                                        print(" selected ");
                                                }
                                                print(">$val</option>");
                                        }
                                ?>
                        </select>
                </td>
	</tr>
	<?
	//on changing the exam time,it should display the remaining seats if any
	if($GET_disp_can=='1' && $GET_move=='0' && $msg_disp!=2)
		{
	?>
		<tr>
        	<td align="right" width=390 class="greybluetext10" valign=top>Remaining : </td>
        	<td align="left" valign=top class="greybluetext10"><?=$rem?></td>
        	<input type=hidden name=remaining value=<?=$rem?>>
        </tr>
        <tr>
        	<td align="right" width=390 class="greybluetext10" valign=top>No. of Seats to be added : </td>
        	<td align="left" valign=top class="greybluetext10"><input type=text class=greybluetext10  name=seat_add maxlength=3  value='' onKeyPress='return frmsub()'></td>
        </tr>
        <TR>
        <TD  width=780 colspan=2>&nbsp;</TD>
        </TR>
        <tr>
			<td colspan=2 align="center"><input type='button' name='sub_assign' value='Submit' class='button' 
			onClick='return updateVacancy()'></td>
		</tr>
        <?
    	}
    	else
    	{
        ?>
        <TR>
        <TD  width=780 colspan=2>&nbsp;</TD>
        </TR>
        <tr>
		<td colspan=2 align="center"><input type='button' name='sub_assign' value='Submit' class='button' 
		onClick='return GetCandidate()'></td>
	</tr>
	<?}
	if ($msg_disp!=''){?>
	<tr><td >&nbsp;</td></tr>
	<tr>
       	<td align=center colspan=2 class=alertmsg><?=$msg?></td>
     </tr> 
     <?
     }
     ?>
	<tr><td colspan=2><br></td></tr>
	<tr>
                   	<?include("includes/footer.php");?>
	</tr>
</table>
</table>
</form>
</center>
</body>
</html>
