<?php
require("dbconfig.php");
$examCode = 1;
$examName = "JAIIB";
$examCentreCode = "610";
$examCentreName = "NEWDELHI";
$currentDate = date("d/m/Y");
$fp = fopen("/tmp/centrewisereports/jcentrewise_".$examCentreCode."_".$examCentreName.".txt","wb");
fwrite($fp,"\n\n");
fwrite($fp,"\t\tThe Indian Institute of Banking and Finance\n\n");
fwrite($fp,"\t\tCentre wise Member Listing - online exam (".$examName.") \n\n");
$str = "";
$str = "Date : ".$currentDate."\t\t	Page : 1\n\n";
$str .= "Centre Code : ".$examCentreCode."\n";
$str .= "Centre Name : ".$examCentreName."\n\n";

//Srl.No. Membership No. Sub.Cd. Medium    Date/Time	     Venue Name and Address
$str .= "Srl.No.\tMembership No.\tSub.Cd.\tMedium\tDate/Time\tVenue Name and Address\n\n";

fwrite($fp, $str);
$sql = " SELECT DISTINCT a.membership_no, a.subject_code, b.medium_code, a.exam_date, a.exam_time, a.centre_code FROM ".
" iib_candidate_iway a, iib_exam_candidate b, iib_candidate c WHERE a.exam_code='$examCode' AND ".
" c.exam_centre_code='$examCentreCode' AND a.membership_no=c.membership_no ".
" AND a.membership_no=b.membership_no AND a.exam_code=b.exam_code AND a.subject_code=b.subject_code ".
" ORDER By a.membership_no,a.subject_code";
print $sql;
$res = mysql_query($sql);
$seqNo = 0;
//$aCount = array("1"=>0,"2"=>0,"3"=>0,"4"=>0);
$aCount = array("71"=>0,"72"=>0,"73"=>0,"74"=>0,"75"=>0,"76"=>0);
$prevMemNo=  "";
while (list($membershipNo, $subjectCode, $mediumCode, $examDate, $examTime, $centreCode) = mysql_fetch_row($res))
{
	
	$aCount[$subjectCode] = $aCount[$subjectCode]+1;
	$sqlIWay = "SELECT iway_name,iway_address1, iway_address2, iway_city, iway_state, iway_pin_code FROM iib_iway_details ".
					" WHERE centre_code='$centreCode'";				
	$resIWay = @mysql_query($sqlIWay) or die("select from iib_iway_details failed ".mysql_error());
	$row = mysql_fetch_array($resIWay);
	$iwayAddress = "";
	
	$row['iway_name'] != "" ? $iwayAddress .= " ".str_replace("\n","",$row['iway_name']) : $iwayAddress .= "";
	$row['iway_address1'] != "" ? $iwayAddress .= " ".str_replace("\n","",$row['iway_address1']) : $iwayAddress .= "";
	$row['iway_address2'] != "" ? $iwayAddress .= " ".str_replace("\n","",$row['iway_address2']) : $iwayAddress .= "";
	$row['iway_city'] != "" ? $iwayAddress .= " ".str_replace("\n","",$row['iway_city']) : $iwayAddress .= "";
	$row['iway_pin_code'] != "" ? $iwayAddress .= " ".str_replace("\n","",$row['iway_pin_code']) : $iwayAddress .= "";
	$row['iway_state'] != "" ? $iwayAddress .= " ".str_replace("\n","",$row['iway_state']) : $iwayAddress .= "";
	$dispTime = substr($examTime,0,5);
	$aDate = explode("-",$examDate);
	$dispDate = $aDate[2]."/".$aDate[1]."/".substr($aDate[0],2,2);
	//9999    xxxxxxxxx     71        x      xx/xx/xx HH:MM      xxxxxxxxxxxxxxxxxxxxx
	if ($prevMemNo != $membershipNo)
	{		
		$seqNo++;
		$prevMemNo = $membershipNo;
		$strSeq = $seqNo;
		$strMem = $membershipNo;
	}
	else
	{		
		$strSeq = fillSpace(strlen($seqNo));
		$strMem = fillSpace(strlen($membershipNo));
	}
	$str = $strSeq."\t".$strMem."\t".$subjectCode."\t".$mediumCode."\t".$dispDate." ".$dispTime."\t".$iwayAddress."\n";
	fwrite($fp, $str);
}
fwrite($fp,"\nSummary :\n\n");

$str = "Total Members : ".$seqNo."\n";
foreach ($aCount as $subjectCode =>$nCount)
{
	$str .= "Total for Subject ".$subjectCode." :  ".$nCount."\n";
}
fwrite($fp, $str);
fclose($fp);

function fillSpace($len)
{
	$str = "";
	for ($i=0; $i < $len; $i++)
		$str .=" ";
	return $str;
}
?>