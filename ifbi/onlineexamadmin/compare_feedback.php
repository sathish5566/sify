<?php
require("dbconfig.php");
?>
<html>
<head>
	<title><?=PAGE_TITLE ?></title>
	<style>
	.c1{
		font-family : Arial,Verdana;		
		font-size : 10pt
	}
	</style>
</head>
<body>	
<?php
$aRating = array("excellent"=> 1, "good"=> 1, "very good"=> 1, "fair"=> 0, "poor"=> 0);
$sql = " SELECT a.membership_no, a.rating, a.subject_code, b.score FROM iib_feedback a, iib_candidate_scores b".
		  " WHERE a.membership_no=b.membership_no AND a.subject_code=b.subject_code AND ".
		  " a.exam_code=b.exam_code ORDER BY a.membership_no, a.exam_code, a.subject_code ";
$res = mysql_query($sql);
$aBadRating = array();
if (mysql_num_rows($res) > 0)
{
	while (list($membershipNo,$rating,$subjectCode,$score) = mysql_fetch_row($res))
	{
		$aRateMember[$membershipNo][$subjectCode] = $rating."/".$score;
		if ($aRated[$membershipNo] == "")
		{
			$aRated[$membershipNo] = $aRating[$rating];
		}
		else
		{			
			if (($aRated[$membershipNo] == 1) && ($aRating[$rating] == 0))
			{
				$aBadRating[] = $membershipNo;
			}
			if ($aRated[$membershipNo] == 0)
			{
				$aRated[$membershipNo] = $aRating[$rating];
			}
		}
	}
	$aBadRating = array_unique($aBadRating);
	if (count($aBadRating) > 0)
	{
		print("<table align=center width=60% border=1 cellspacing=1 cellpadding=1  class='c1'>");
		
		foreach ($aBadRating as $membershipNo)
		{			
			foreach ($aRateMember[$membershipNo] as $subjectCode=>$value)
			{
				$aValue = explode("/", $value);
				print("<tr><td class=c1>$membershipNo</td>");
				print("<td class=c1>$subjectCode</td><td class=c1>".$aValue[0]."</td>");
				print("<td class=c1>".$aValue[1]."</td>");
				print("</tr>");
			}
			print("</tr>");
		}
		print("</table>");
	}
}
?>
</body>
</html>				