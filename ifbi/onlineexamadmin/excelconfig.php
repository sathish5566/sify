<?php
/****************************************************
* Application Name            :  IIB
* Module Name                 :  Excel configuration file
* Revision Number             :  1
* Revision Date               : -
* Table(s)  modified                  : -  
* Tables used for only selects:  -
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              : 
* Document/Reference Material :
* Created By : Anil Kumar.A
* Created On : 18/03/2006 
* Last Modified By            :  
* Last Modified Date          : 
* Description                 :  Used to write contents passed to this function to a csv file
*****************************************************/
function excelconfig($filename,$excel_header,$excel_content)
{
	$count=count($excel_content);
	ob_end_clean();
 header("Cache-Control: no cache,must-revalidate");
 header("Content-Type:application/dummy"); 
 header("Content-Disposition: attachment; filename=$filename.csv");
 $i=0;
 $headerLength=count($excel_header);
	for($i=0;$i<$headerLength;$i++)
	{
		if($i!=($headerLength-1))
		{
			echo trim($excel_header[$i]).",";
		}
		else
		{
			echo trim($excel_header[$i])."\n";
		}
	}
   
    for($j=0;$j<$count;$j++)
    {
	    for($k=0;$k<$headerLength;$k++)
	    {
		    if($k!=($headerLength-1))
		    {
		    	echo '"'.$excel_content[$j][$k]."\",";
	    	}
	    	else
	    	{
		    	echo '"'.$excel_content[$j][$k]."\"\n";
	    	}
	    }
    } 
 }


function excelconfig_xls($filename,$excel_header,$excel_content)
{
        echo $count=count($excel_content);
        ob_end_clean();
 header("Cache-Control: no cache,must-revalidate");
 header("Content-Type:application/dummy");
 header("Content-Disposition: attachment; filename=$filename.xls");
 $i=0;
 $headerLength=count($excel_header);
	echo "<table border='1'><tr>";
        for($i=0;$i<$headerLength;$i++)
        {
                if($i!=($headerLength-1))
                {
			echo "<td>$excel_header[$i]</td>";
                }
                else
                {
			echo "<td>$excel_header[$i]</td></tr>";
                }
        }

    for($j=0;$j<$count;$j++)
    {
	echo "<tr>";
            for($k=0;$k<$headerLength;$k++)
            {
                if($k!=($headerLength-1))
                {
			echo "<td>".$excel_content[$j][$k]."</td>";
                }
                else
                {
			echo "<td>".$excel_content[$j][$k]."</td></tr>";
                }
            }
    }
	echo "</table>";
 }


function excelconfig_sectionscore($filename,$excel_header,$excel_content)
{
	$count=count($excel_content);
	ob_end_clean();
 header("Cache-Control: no cache,must-revalidate");
 header("Content-Type:application/dummy"); 
 header("Content-Disposition: attachment; filename=$filename.csv");
 $i=0;
 $headerLength=count($excel_header);
	for($i=0;$i<$headerLength;$i++)
	{
		if($i!=($headerLength-1))
		{
			echo trim($excel_header[$i]).",";
		}
		else
		{
			echo trim($excel_header[$i])."\n";
		}
	}
   
    for($j=0;$j<$count;$j++)
    {
	    for($k=0;$k<$headerLength;$k++)
	    {
			if($excel_content[$j][$excel_header[$k]]=='' || $excel_content[$j][$excel_header[$k]]<0)
				$excel_content[$j][$excel_header[$k]] = 0;
		    if($k!=($headerLength-1))
		    {
		    	echo '"'.$excel_content[$j][$excel_header[$k]]."\",";
	    	}
	    	else
	    	{
		    	echo '"'.$excel_content[$j][$excel_header[$k]]."\"\n";
	    	}
	    }
    } 
 }


?>	 
