<?php
require ("sessionchk.php");
require ("dbconfig.php");

$memno          = $_POST["tMemNo"];
$frm_sub_code   = $_POST["sFromSubject"];
$to_sub_code    = $_POST["sToSubject"];
$err_msg        = "";
$chkok          = 0;

if ($_POST["hUpdate"]=='1' and ($frm_sub_code != NULL) and ($to_sub_code != NULL))
{
	$sqlSelect = "SELECT count(1) FROM iib_candidate_test WHERE membership_no='$memno' AND subject_code='$frm_sub_code' ";
	$resSelect = mysql_query($sqlSelect);
	list($testCount) = mysql_fetch_row($resSelect);
	if ($testCount > 0)
	{
		$err_msg = "Candidate '$memno' has already started/completed the test";
		$chkok = 0;
	}
	else
	{
	    $sql_update_exam = "update iib_exam_candidate set subject_code='$to_sub_code' where membership_no = '$memno' and subject_code='$frm_sub_code'";
	   	mysql_query($sql_update_exam);
	    if (mysql_affected_rows($connect) > 0)
	    {
	    		$sql_update_iway = "update iib_candidate_iway set subject_code='$to_sub_code' where membership_no = '$memno' and subject_code='$frm_sub_code'";
	    		mysql_query($sql_update_iway);
		     	if (mysql_affected_rows($connect) > 0)
		    	{
	    			$sql_insert_log = "insert into iib_subject_change_log values (NULL,'$memno','$frm_sub_code','$to_sub_code',NULL)";
	    			mysql_query($sql_insert_log);
	    			if (mysql_affected_rows($connect) > 0)
		  				$err_msg="Updated successfully";
					else	
		    			$err_msg=$err_msg."- Error inserting in subject_change_log table -";	    
	    		}
	    		else
	    			$err_msg = " No Rows affected in changing the subject from $frm_sub_code to $to_sub_code in iib_candidate_iway";
	    }
	    else
	    	$err_msg="No Rows affected in changing the subject code from $frm_sub_code to $to_sub_code in iib_exam_candidate";
	}
//	echo $sql_update_exam."<br>".$sql_update_iway."<br>".$sql_insert_log."<br>";
	$memno = '';
}

if ($memno)
{
    $exam_code_list = $subject_code_list = "";
    
    $sql_exam_can = "select a.exam_code, b.exam_name from iib_exam_candidate a, iib_exam b where a.exam_code=b.exam_code and membership_no='$memno'";

	$res_exam_can = mysql_query($sql_exam_can);
	$num_sub = mysql_num_rows($res_exam_can);
	if (!$res_exam_can)
		$err_msg="Err getting details of the candidate";
	else{
		if ($num_sub == 0)
			$err_msg="No exam for this candidate today ";
		else
            list($exam_code,$exam_name)=mysql_fetch_row($res_exam_can);            
	}
}
?>
<HTML>
<HEAD>
<script language='JavaScript' src="./includes/validations.js"></script>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='javascript'>

function validate(){
	frm=document.frmSubjectChange;
	if(trim(frm.tMemNo.value) == ""){
		alert('Please Enter Membership Number');
        frm.tMemNo.focus();
        return;
    }
	if(frm.sFromSubject.selectedIndex == 0){
		alert('Please select From Subject');
        frm.sFromSubject.focus();
        return;
    }
	if(frm.sToSubject.selectedIndex == 0){
		alert('Please select From Subject');
        frm.sToSubject.focus();
        return;
    }
    if (frm.sFromSubject.options[frm.sFromSubject.selectedIndex].value == frm.sToSubject.options[frm.sToSubject.selectedIndex].value)
    {
	    alert('From and To Subjects are same');
	    frm.sToSubject.focus();
        return;
    }
	frm.hUpdate.value = '1';
    frm.submit();
}

function getSubjects()
{
	frm=document.frmSubjectChange;
	if (frm.exam_code.selectedIndex == 0)
	{
		alert('Please select the exam');
		frm.exam_code.focus();
		return false;
	}
	else
	{
		frm.submit();
	}
}

function submitpage(){
	if (event.keyCode != 13)
		return;
	frm=document.frmSubjectChange;
	if(trim(frm.tMemNo.value) == ""){
		alert('Please Enter Membership Number');
        frm.tMemNo.focus();
        return;
    }
    frm.submit();
}

</script>
</HEAD>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0 onkeypress='submitpage()'>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
        <TR><!-- Topnav Image -->
    <!--    <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>   -->
	           	<td width=780><?include("includes/header.php");?></td>

        </TR>
        <TR>
        
        </TR>
		<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
		</tr>		
			
	    <TR>
            <TD background=images/tile.jpg vAlign=top width=780 align=center>
        <form name=frmSubjectChange method="post" onsubmit="return false;">
        <table width=780 cellpadding=0 cellspacing=5 border=0>
          <tr>
          <td colspan=2 align="center"><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2>Change Subject Code</b><br><br></font></td>
          </tr>
          <? if ($err_msg != "") {?>
		<tr>
			<Td class='alertmsg' colspan=2 align=center><? echo $err_msg ?></td>	
		</tr>		
		<? } ?>	
		<tr>
			<td colspan=2 align=center class=greybluetext10><b>Fill Membership No. and press Enter</b></td>
		</tr>
          <tr>
				<td align="right" class="greybluetext10" width="390">Membership Number :&nbsp;</td>
                <td><input type='text' name=tMemNo class=textbox value='<? echo $memno; ?>'></td>
          </tr>
          <tr>
				<td align="right" class="greybluetext10" width="390">Exam Code :&nbsp;</td>
                <td class="greybluetext10">                
                <?php
                	if ($memno) 
                	{                		
						print($exam_code."->".$exam_name);                		
                	}
               	?>               		
                </td>
          </tr>
	 	  <tr>
                <td align="right" width=390 class="greybluetext10" valign=top>From Subject :&nbsp;
                <select class="textbox" name="sFromSubject" style={width:150px}>
 			    <option value=-1>-Select-</option>
				<?
				if ($memno){
					$sql_subjects="select a.subject_code, subject_name from iib_exam_subjects a, iib_candidate_iway b where b.membership_no='$memno' and a.subject_code=b.subject_code and b.exam_date = current_date";
					$res_subjects=mysql_query($sql_subjects);
					while (list($subject_code,$subject_name)=mysql_fetch_row($res_subjects)){
						$subject_name=stripslashes($subject_name);
						if($_POST['sFromSubject']==$subject_code)
							echo "<option value=\"$subject_code\" selected>$subject_code -> $subject_name</option>";
						else
							echo "<option value=\"$subject_code\">$subject_code -> $subject_name</option>";
					}
				}
				?>
				</select></td>
				<td align="left" class="greybluetext10">&nbsp;&nbsp;To Subject :&nbsp;
                <select class="textbox" name="sToSubject" style={width:150px}>
 			    <option value=-1>-Select-</option>
				<?
				if ($memno)
				{
					$sql_subjects="select a.subject_code, a.subject_name from iib_exam_subjects a, iib_exam_schedule b where a.subject_code=b.subject_code and b.exam_date=current_date and a.online='Y' and  a.exam_code='".$exam_code."'";
					$res_subjects=mysql_query($sql_subjects);					
					while (list($subject_code,$subject_name)=mysql_fetch_row($res_subjects)){
						$subject_name=stripslashes($subject_name);
						if($_POST['sToSubject']==$subject_code)
							echo "<option value=\"$subject_code\" selected>$subject_code -> $subject_name</option>";
						else
							echo "<option value=\"$subject_code\">$subject_code -> $subject_name</option>";
					}
				}
				?>
				</td>
			</tr>
            <tr>
               <td align="right"><input type="button" class=button name=bSubmit value="Submit" onclick='javascript:validate();'></td>
               <td align="left"><input type="reset" class=button name=bReset value="Cancel"></td>
            </tr>
            <input type="hidden" name="hUpdate" value='0'>
            </table>
            </form>
           </TD>
        </TR>
        <TR>
                <?include("includes/footer.php");?>
        </TR>
</TABLE>
</FORM>
</center>
</BODY>
</HTML>
