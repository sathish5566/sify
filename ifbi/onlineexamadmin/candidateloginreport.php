<?php
@session_start();
if ($_SESSION["admin_type"]== '5') {
	$admin_flag = 5;
} else {
	$admin_flag = 2;
}
require("dbconfig_slave_123.php");
require("sessionchk.php");
set_time_limit(0);

$examDate = $_POST['exam_date'];
$sqlDate = "SELECT distinct exam_date FROM iib_exam_schedule order by exam_date ";
$resDate = mysql_query($sqlDate);

function sec2hms ($sec, $padHours = false) {

    $hms = "";    
    // there are 3600 seconds in an hour, so if we
    // divide total seconds by 3600 and throw away
    // the remainder, we've got the number of hours
    $hours = intval(intval($sec) / 3600); 

    // add to $hms, with a leading 0 if asked for
    $hms .= ($padHours) 
          ? str_pad($hours, 2, "0", STR_PAD_LEFT). ':'
          : $hours. ':';
     
    // dividing the total seconds by 60 will give us
    // the number of minutes, but we're interested in 
    // minutes past the hour: to get that, we need to 
    // divide by 60 again and keep the remainder
    $minutes = intval(($sec / 60) % 60); 

    // then add to $hms (with a leading 0 if needed)
    $hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT). ':';

    // seconds are simple - just divide the total
    // seconds by 60 and keep the remainder
    $seconds = intval($sec % 60); 

    // add to $hms, again with a leading 0 if needed
    $hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT);

    return $hms;
}


?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
	FONT-FAMILY: Arial, Verdana	
}
.textblk11  { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; text-decoration: none}
</STYLE>
</head>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
	<tr><td width="780"><?php include("includes/header.php");?></td></tr>
	<TR>    	
	</TR>
	<tr>
	<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>	
	<TR>
	    <TD background=images/tile.jpg vAlign=top width=780 align=center>
	    <form name='frm' method='post'>
<table width=100% border=0 cellspacing=0 cellpadding=5>
	<tr><td align=center><div align=center><b>Logged in but  exam not started candidates   Report</b></div></td></tr>
	<tr>
		<td align=center class=textblk11>Exam Date : 
			<select name='exam_date' style='{width:150px}' class='textblk11'>
				<option value=''>--Select--</option>
<?php
if (mysql_num_rows($resDate) > 0)
{
	while (list($eDate) = mysql_fetch_row($resDate))
	{
		$aDate = explode("-",$eDate);
		$dispDate = $aDate[2]."-".$aDate[1]."-".$aDate[0];
		print("<option value='$eDate' ");
		if ($eDate == $examDate)
			print(" selected ");
		print(">$dispDate</option>");
	}
}

?>
		</select>
		</td>
	</tr>
	<tr>
		<td align=center><input class='button' type='button' name='sub' value='Submit' onClick='javascript:submitForm()'></td>
	</tr>
</table>
</form>
<script language='JavaScript'>
function submitForm()
{
	if (document.frm.exam_date.selectedIndex == 0)
	{
		alert("Please select the exam date");
		document.frm.exam_date.focus();
		return false;
	}
	document.frm.submit();
}
function win_open(a,b)
{
	window.open('tadetails.php?id='+a+'&cid='+b,'win1','left=10,top=10,width=600,height=700,toolbar=no,menubar=no,addressbar=no,resizable=1');		
}
</script>
<?php
if ($examDate != "")
{
	$arrMem=array();
	$aDates = explode("-", $examDate);
	$dispDate = $aDates[2]."-".$aDates[1]."-".$aDates[0];
	$examDate1 = $examDate." 00:00:00";
	$examDate2 = $examDate." 23:59:59";
    $arrScore=array();
    $sql1 = "select membership_no from iib_candidate_scores";
    $res1 = mysql_query($sql1);
    while($rs=mysql_fetch_object($res1)){
        array_push($arrScore,$rs->membership_no);
    }

    $sql = "select A.cafe_id,A.membership_no,A.host_ip,A.updated_time from iib_candidate_tracking A LEFT JOIN iib_candidate_test B ON A.membership_no=B.membership_no  and B.start_time > A.updated_time where B.membership_no is NULL AND A.updated_time between '$examDate1'  AND '$examDate2'  order by A.updated_time desc  ";	
	$res = mysql_query($sql);
	
	$nRows = mysql_num_rows($res);		
	if ($nRows > 0)
	{												
		print("<table width=100% border=1 cellspacing=1 cellpadding=1>");
		print("<tr><td width=10% class=textblk11><b>Membership No</b></td><td width=10% class=textblk11><b>Centre Code</b></td><td width=10% class=textblk11><b>IP Address</b></td><td width=20% class=textblk11><b>Last Logged Time</b></td><td width=10% class=textblk11><b>Idle Time(HH:MM:SS)</b></td></tr>");
		while (list($iway,$membershipNo,$ip,$updateTime) = mysql_fetch_row($res))
		{														
           if( (!in_array($membershipNo,$arrScore)) && (!in_array($membershipNo,$arrMem)) ) 
            {							
				array_push($arrMem,$membershipNo);
			
				$iw_iway="iwfr_".$iway;
				
				$latime=strtotime($updateTime);														
				$curtime=strtotime("now");
				$diff=$curtime-$latime;				
				$idletime=sec2hms($diff);
				
				if(($diff > 300) && ($diff <= 900) )
					$color="bgcolor='#FFFFC1'"; //yellow
				else if($diff > 900)
					$color="bgcolor='#FF8533'";	 //red
				else
				   $color="bgcolor='#FFFFFF'";	
																															
				print("<tr $color>");	
				print("<td class=textblk11><a href='candidateentries.php?cname=$membershipNo' target='_blank'>$membershipNo</a></td>");
				print("<td class=textblk11><a href=javascript:win_open('$iw_iway','$iway')>$iway</a></td>");							
				print("<td class=textblk11>$ip</td>");	
				print("<td class=textblk11>$updateTime</td>");				
				print("<td class=textblk11>$idletime</td>");			
				print("</tr>");				
			 }
		}			
		print("</table><br><br>");
		echo "<span  class=textblk11>No.Of Records:".count($arrMem).'</span>';	
		print("<br><div align=left class=textblk11>Color codes:<br>Idletime more than 15 mins Orange color <br>Idle time more than 5 mins and less than 15 mins light Yellow Color<br></div><br><br>");
	}		
	else
	{
		print("<br><div align=center class=textblk11>No records found</div><br><br>");
		
	}
}
?>
		</TD>
	</TR>
	<TR>
		<?php include("includes/footer.php");?>
	</TR>
</TABLE>
</center>
</BODY>
</HTML>
