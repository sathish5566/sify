<?php
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
                                                     // always modified
	header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP/1.1
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");                          // HTTP/1.0
	/*print("session");
	print_r($_SESSION);*/
	include("session_handle.php");
	require_once "session1.inc";	
	if ($_SESSION['sess_memno'] != "")
	{
		header("Location: login.php?reason=2");
		exit;
	}
	include("constants.inc");
	require_once "dbconfig.php";
	$commonDebug = false;
	$lang=isset($_SESSION["lang"])?$_SESSION["lang"]:"E";	
	$memno=$HTTP_POST_VARS["memno"];
	$membershipNo = $memno;
	$hid_lefttime=$HTTP_POST_VARS['hid_lefttime'];
	$taOverride = $_REQUEST['ta_override'];
	$loginOverride = $_REQUEST['login_override'];
	if ($loginOverride == '')
	{
		$loginOverride = 'N';
	}
	if ($memno == "")
	{
		header("Location: login.php");
		exit;
	}
	
	$todaydate=date('Y-m-d');
	$currTimeStamp = mktime(date("H"),date("i"), date("s"),date("m"),date("d"),date("Y"));
	$sqlTime = "SELECT exam_time from iib_candidate_iway where membership_no='$memno' and exam_date='$todaydate' order by exam_time";
	$resTime = @mysql_query($sqlTime);
	$nTime = @mysql_num_rows($resTime);
	if ($nTime > 1)
	{
    	$nCntTime = 0;
    	while (list($eTime) = mysql_fetch_row($resTime))
    	{
        	$aTime[$nCntTime] = $eTime;
        	$aTmp = array();
        	$aTmp = explode(":", $eTime);
        	$aTimeStamp[$nCntTime] = mktime($aTmp[0],$aTmp[1],$aTmp[2],date("m"),date("d"),date("Y"));
        	$nCntTime++;
    	}
    	if ($nCntTime > 0)
    	{		        	
        	if ($currTimeStamp > $aTimeStamp[1])
        	{
	        	$selExamTime = $aTime[1];
        	}
        	else
        	{
	        	$selExamTime = $aTime[0];
        	}		        	
    	}
    	$sql = "select exam_date,subject_code,exam_code from iib_candidate_iway where membership_no='$memno' and exam_date='$todaydate' ".
    					"and exam_time = '$selExamTime' ";	        						        					
	}
	else
	{
    	$sql = "select exam_date,subject_code,exam_code from iib_candidate_iway where membership_no='$memno' and exam_date='$todaydate'";
	}
	
	$res=mysql_query($sql);
	list($examDate,$subjectCode,$examCode)=mysql_fetch_row($res);
	$sess_memno=$memno;
	$sess_examdate = $examDate;
	$sess_examcode = $examCode;
	$sess_subjectcode = $subjectCode;
	
//	session_start();
//	session_register("sess_memno");	
//	session_register("sess_examdate");
//	session_register("sess_examcode");
//	session_register("sess_subjectcode");
	$_SESSION['sess_memno'] = $sess_memno;
	$_SESSION['sess_examdate'] = $sess_examdate;
	$_SESSION['sess_examcode'] = $sess_examcode;
	$_SESSION['sess_subjectcode'] = $sess_subjectcode;
	
	if ($taOverride == 'Y')
	{
		$sqlQuestionPaper = "SELECT question_paper_no FROM iib_candidate_test ".
	                        " WHERE exam_code='$examCode' AND subject_code='$subjectCode' ".
	                       " AND test_status='IC' AND membership_no='$membershipNo' ORDER BY test_id DESC LIMIT 1";	                       
	}
	else
	{
		
		$sqlUpdate = "UPDATE iib_question_paper SET assigned='Y', membership_no='$membershipNo'  WHERE ".
            "exam_code='$examCode' AND subject_code='$subjectCode' AND assigned='N' AND online='Y' LIMIT 1";
        if ($commonDebug)
        {
        	print("\n".$sqlUpdate);
        }
        @mysql_query($sqlUpdate) or die("update iib_question_paper failed ".mysql_error());
 		$nAffectedRows = @mysql_affected_rows($connect);
 		
 		if ($nAffectedRows == 0)
 		{
?>
<html>
<head>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<script language='JavaScript'>
history.go(1);
</script >
</head>
<body>
<center>
<table width="780" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td><img src="images/logo1.jpg" width="136" height="59"><img src="images/logo2.gif" width="284" height="59"><img src="images/logo3.gif" width="360" height="59">
		</td>
	</tr>
	<tr>
		
	</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="780" background="images/tile.jpg">
	<tr>
		<td width="780" class=greybluetext10 align=center><br><br><b>Question Paper Not Available. Please contact the Test Administrator</b><br><br></td>
	</tr>
	<tr>
		<? include("includes/footer.php");?>
	</tr>
</table>
</center>
</body>
</html>
<?php	 		
	 		
			exit;
 		}
		$sqlQuestionPaper = "SELECT question_paper_no FROM iib_question_paper ".
	                        "WHERE exam_code='$examCode' AND subject_code='$subjectCode' ".
	                        "AND sample='N' AND enabled='Y' AND online='Y' AND assigned='Y' ".
	                        "AND complete='Y' AND membership_no='$membershipNo'  LIMIT 1";
	         
    }
	if ($commonDebug)
	{
    	print("\nqp:".$sqlQuestionPaper);
    }

	$result = @mysql_query($sqlQuestionPaper) or die("select from iib_question_paper  failed ".mysql_error());
	$nRows = mysql_num_rows($result);
	if ($nRows > 0)
    {
        list($questionPaperNo) = mysql_fetch_array($result);        
	}
	if ($taOverride == '')
	{
		$taOverride = 'N';
	}
	if ($hid_lefttime != "")
	{
		$totalTime = $hid_lefttime;
	}
	$sqlM = "SELECT total_marks FROM iib_exam_subjects WHERE exam_code='$examCode' and subject_code='$subjectCode' ";
	$resM = mysql_query($sqlM) or die ("Connection cannot be done3 error:'".($sqlM)."'");
	list($totalMarks) = mysql_fetch_row($resM);
	
	$sql = "select sum(no_of_questions) from iib_qp_weightage where exam_code='$examCode' and subject_code='$subjectCode' group by question_paper_no limit 1"; 
	$res = mysql_query($sql) or die ("Connection cannot be done3 error:'".($sql)."'");
	$num=mysql_fetch_row($res);
	$num_of_ques = $num[0];

	//$num_of_ques = "";
	if ((trim($num_of_ques) != "") && (trim($num_of_ques) != 0))
	{
		$sqlInsert = "INSERT INTO iib_candidate_test (membership_no, exam_code, subject_code, question_paper_no, ".
			    	"test_status, ta_override, login_override, start_time, last_updated_time, total_time) VALUES ('$membershipNo', '$examCode', '$subjectCode', ".
		    		"'$questionPaperNo', 'IC', '$taOverride', '$loginOverride', now(), now(), '$totalTime')";
		if ($commonDebug)
		{
			print("\n".$sqlInsert);
		}
		@mysql_query($sqlInsert) or die("insert into iib_candidate_test failed ".mysql_error());
		$testID = mysql_insert_id();		   	
	}
	else
	{
		
?>
<html>
<head>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<script language='JavaScript'>
history.go(1);
</script >
</head>
<body>
<center>
<table width="780" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td><img src="images/logo1.jpg" width="136" height="59"><img src="images/logo2.gif" width="284" height="59"><img src="images/logo3.gif" width="360" height="59">
		</td>
	</tr>
	<tr>
		<? include("includes/footer.php");?>
	</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="780" background="images/tile.jpg">
	<tr>
		<td width="780" class=greybluetext10 align=center><br><br><b>Question Paper Not Available. Please contact the Test Administrator</b><br><br></td>
	</tr>
	<tr>
		<? include("includes/footer.php");?>
	</tr>
</table>
</center>
</body>
</html>
<?php	
	
	return;	
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<script language='JavaScript'>
history.go(1);
</script >
<STYLE TYPE="text/css">
  @font-face {
    font-family: DVBW-TTYogeshEN;
    font-style:  normal;
    font-weight: normal;
    src: url(http://sifyitest.com/eot/DVBWTTY0.eot);
  }
 .HindiBold{font-family:'DVBW-TTYogeshEN';font-weight:bold;font-size:17.9px}
 .HindiNormal{font-family:'DVBW-TTYogeshEN';font-weight:normal;font-size:18px}
}
</STYLE>
</head>
<body>
<center>
<table width="780" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td><img src="images/logo1.jpg" width="136" height="59"><img src="images/logo2.gif" width="284" height="59"><img src="images/logo3.gif" width="360" height="59">
		</td>
	</tr>
	<tr>
		<? include("includes/footer.php");?>
	</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="780" background="images/tile.jpg">
	<tr>
		<td>
		<br>
		<table border="0" cellspacing="0" cellpadding="10" width="80%" align=center>
			<tr>
				<td colspan="3" class="greybluetext10" bgcolor="#D1E0EF" style={text-align:justify}>
				<?
				if ($lang=="E"){
				?>
				<b class="arial11a">Instructions</b><br><br>
				<b>About Question Papers:</b><br><br>
				<ol>
				<li>The Question Paper consists of objective type questions only.</li><br><br>
				<li>Total Marks for the Question Paper are <? echo $totalMarks; ?>.  Marks for each Question are indicated just next to the Question. The questions would carry 1 to 2 marks each depending on the difficulty level of the question.</li><br><br>
				<li>For every question, four or more alternative options have been given on the computer screen.</li><br><br>
				<li>Please note that there is Negative Marking in the inverse proportion to the number of options given in the question i.e. in case there are 4 options given for the question and the question carries 1 mark, the wrong answer will fetch (-) 0.25 marks. If the same question carries 2 marks it will fetch (-) 0.50 marks for the wrong answer.</li><br><br>
				<li>The Question Paper contains 70 to 90 questions to be attempted in two hours duration. The Time left for the Examination is displayed on the right Top Clock displayed on the Screen. Time left is also displayed on the left Bottom Status bar.</li><br><br>
				</ol>
				<b>About answering the questions:</b><br><br>
				<ol>
				<li>The candidates are requested to follow the instructions of the "Test Administrator" carefully.  If any candidate does not follow the instructions / rules, it would treated as a case of misconduct / adoption of unfair means and such candidate would be liable for debarment from appearing at the examinations for a period as decided by the Institute.</li><br><br>
				<li>The candidates may ask the Test Administrator their doubts or questions only before the commencement of the test.  No query shall be entertained after the commencement of the examination.</li><br><br>
				<li>The candidates have been given "Sample Test Questions" for practice purpose before they start answering the "Examination Questions".</li><br><br>
				<li>The candidate must 'Mouse-click' the alternative he feels appropriate / correct.  The said alternative will be highlighted and shall be treated as the answer given by the candidate for the question.  In case the candidate does not wish to attempt the question then he will not click for any option of that Question.</li><br><br>
				<li>The candidate can choose to deselect the answer by clicking the "Erase Answer" link provided against the question.</li><br><br>
				<li>6. The questions may be answered in any order within the given time.  However, care needs to be taken that the answer marked in the answer sheet is against the related question number.<br>
					The candidates can make changes in their choice of alternative only before clicking the "Submit" button on the Preview Page.  To move back and forth between the questions candidates can use the 'scroll bar'.  To recheck the answers, after attempting all the questions, click on the "Back" button.</li><br><br>
				</ol>
				<b>Back up sheet:</b><br><br>The Test Administrator provides the candidates with a back up sheet before the examination begins. Candidates should record the options mouse clicked against the related question number in the back up sheet.  The back up sheet may be used in case there is a power failure or the computer shuts down due to some technical reason to record the mouse clicks for options selected by the candidate for the respective questions.  The back-up sheets are to be handed over to the Test Administrator once the examination is over.  The candidates would be compensated for any time loss due to power or system failure subject to the discretion of the Test Administrator<br><br>
				<b>About the Preview and Submission:</b><br><br>
				<ol>
				<li>The page gets automatically refreshed every 15 minutes in order to save the answers.</li><br><br>
				<li>After all questions are attempted, the candidate must click on "Preview and Submit" button at the bottom of the question paper to submit the questions.</li><br><br>
				<li>The candidates can make changes in their choice of alternative only before clicking the "Submit" button on the Preview Page.  To move back and forth between the questions candidates can use the 'scroll bar'.  To recheck the answers, after attempting all the questions, click on the "Back" button.</li><br><br>
				<li>After the expiry of two hours, the candidates will not be able to attempt any question or check their answers.  The answers of the candidate would be saved automatically by the computer system even if he has not clicked the "Submit" button on the Preview screen.</li><br><br>
				<li>Once "Preview and Submit" button is clicked the Unattempted and Attempted Questions are shown. By clicking on the respective Question Number, will take the Candidate to that specific Question on the Question Paper page.</li><br><br>
				<li>After completing the Examination, press the 'Submit' button on the Preview Page. Click on the "Submit" Button, If there are any Unattempted Question/s then warning message will be displayed saying "You have Unattempted Questions. Do you really want to Submit?" If the Candidate Clicks on "Cancel" Button then he will be displayed the Preview page. If he clicks on "OK" Button then Result is displayed to the Candidate.</li><br><br>
				<li>Printout The candidate can Click on "Print" Button if he wishes to take the of his result else he will click on "Exit" Button.</li><br><br>
				</ol>
				<? }
				else {
				?>
				<font class=HindiNormal>
				<b>��������</b><br><br>				
				<b>��Ͽ�֯֡��� ��� ��ָ�� ��� </b><br><br>
				<ol>
				<li>��Ͽ�֯֡� ��� ����ֻ� ��ß��׮�� ���Ͽ�� ��� �</li><br><br>
				<li>��Ͽ�֯֡� ����� <? echo $totalMarks; ?>  ������� ��� ��� � ��ϟ����� ��Ͽ�� ��� ���� ��Ͽ�� ��� ���Ʈ�� ����  �����Ե�� �ֵ�� ��� �  ��Ͽ�� ��� ��ך��֟�� ��� ßָ� ��� ������ָ� ��ϟ����� ��Ͽ�� ���� 1 ��� 2 ���� פ���� �ֵ�� ��� �</li><br><br>
				<li>��ϟ����� ��Ͽ�� ��� ��ָ� ��� ׻֋ ���������� ��� ����߮� �ָ� 4 ��� ����� ׾������ פ���� �ֵ�� ��� �</li><br><br>
				<li>����ֵ�� ������ ��֮� ����� ��� �ֻ֟� ��ָ� ��� ׻֋ ���� ��֙�� o�֋����, o��� ׾�������� ��� ���ןֻ����� �����֟� ��� �������� ����ԟ�� ��פ� ������ ��Ͽ�� ��� ׻֋ 1 ���� ���� ���� ������ ׻֋ 4 ׾������ פ���� �ֵ�� ���� �ֲ� ������ �ֻ֟� ��ָ� ������ �ָ� ������ ׻֋ (-) 0.25 ���� פ���� o�ֵ����օ ����� ��Ͽ�� ��ָ� 2 ������ ��� ��� ���� ������ ׻֋ (-) 0.5 ���� פ���� o�֋��� �</li><br><br>
				<li>��Ͽ�֯֡� ��� ����� 70 ��� 90 ���� ��Ͽ�� �������, �o֮���� ׻֋ �֯����� ���� '�ә�� ��� �ִֵ� ״ֻ����� � �����  ��ָ� ������ ��� ׻֋ ��"�� ����� �ִֵ� ����߮� ��� ���ָ� ��ֻ�� ������� ��� ��������� ���� פ���ֵ�� o�֋��օ ��ָ� ������ ��� ׻֋ ��"�� ����� �ִֵ� ��ֵ�� ���� ���"�� ��� Ù������ ��ָ� ��� ��� פ���� �ֵ�� ����</li><br><br>
				</ol>
				<font class=HindiBold>��Ͽ���� ��� ��ָ� ������ ��� ��ָ�� ��� :<br><br></font>
				<ol>
				<li>�ָ�����-��Ͽ������ (���Ù� ���״�׮�Ù�������) ��ָ�� ��� �ֵ�� ���"֮�ֆ�� ��� ��֮֯������ ��ֻ֮� ����� � ��פ� ����� ��׸���֣��� ���������� / ׮ִֵ��� ��� ��ֻ֮� ����� ������� ��� ���� ���� �����"�ָ� / �����"֟� �ָ����� ��֮�֮�� �ִ�-�� o�֋��� ���� ��׸���֣��� ���� �ָ������ ������ ��� ��֮�� �ִֵ� ��� ׻֋ ����"֟� ���� פ���� o�ֵ�����, �o֮֟�� �ִֵ� ��� ׮���Ե� ��Ù�ߙ������ ������</li><br><br>
				<li>�ָ�����֣��� ��֮�� ��Ӥ���� ��� �־�ֻ� ���Ù� ���״�׮�Ù������ ��� �ָ����� ���� ������ ��� �������� ���"� ������� ���� �ָ������ ���� ������ �ָ� �֯���� ������ ��� ��Ӥ���� / �־�ֻ� ��� ��ָ� ����� פ���� o�֋���� �</li><br><br>
				<li>�ָ�����֣��Ե��� ���� �ָ������ ���� ������ ��� ������� '�ִ���� �ָ������ �֡�' ������� ��� ׻֋ פ���� o�֋��� �</li><br><br>
				<li>�ָ�����֣��� ���� פ���� �ֵ�� ׾�������� ��� ��� ����� ��ָ� "������� ��� �ָ�  '��ֈ�� ׌����' ������� ��� � ��ֈ�� ׌���� ������� ��� ���� ׾������ "ֵ֮� ������ ����� פ���և� ������ ���� ���� �ָ����֣��� ��ָ� ��֮�� o�֋��� � ��ָ� �ָ�����֣��� ������ ��Ͽ�� ��� ��ָ� ������ ����� "�������, ���� ���� ������ ��� ׾������ �ָ� ��ֈ�� ׌���� �� ������ </li><br><br>
				<li>��ָ� �ָ�����֣��� ��� ������ ��Ͽ�� ��� ��ָ� �ָ� ��ֈ�� ׌���� ������ ��� ���� ���� ��� ��ָ� ���� ���� ���� ��� ��Ͽ�� ��� ��ָ� ����� ������ "������� ���� ��� ��Ͽ�� ��� ��ִ֮�� ׻���� ���� "erase answer" �ָ� ��ֈ�� ׌���� ������ �o������ פ�� �֋ ��ָ� ��� ��ֈ�� ׌���� "ֵ֮� ���� ���� o�֋��� �</li><br><br>
				<li>פ�� �֋ �ִֵ� ��� ��Ͽ���� ��� ��ָ� ������ ��� ����� ��� פ���� o�� ������� ���� �������� ���� ��֮� ���֮�� �־ֿ���� ��� ��� ����� ��� ��ߙ� ��� ׻���� �ֵ�� ��ָ� ��Ӳ���֟� ��Ͽ�� ���ܵ�� ��� ��� ��� �<br>
				   �ָ�����֣��� ��֮�� ��ָ��� ��� "ֵ֮� Preview Page �ָ� "Submit" �֙��� �ָ� ׌���� ������� ��� ������� �֤��� ������� ��� � ������ֻ�� ��� ���"����ֻ�� ��Ͽ���� ���� ����֮�� ��� ׻֋ ����߮� ��� ��������� ���� �����Ե�� �ֵ�� ������� ��ָ�� ��� ��ֵ����� ���� ������� ���Ӆ </li><br><br>
				</ol>
				<b>����� ��� ��ߙ�</b><br><br>���Ù� ���״�׮�Ù������� �ָ�����٣ֵ��� ���� �ָ����� ����� ������ ��� ������� ��� ����� ��� ��ߙ� ������ �  �ָ�����٣ֵ��� ���� ��ֈ�� ׌���� ������� ���������� ��� פ���� �ֵ�� ��֮�� ��ָ,� ����� ��� ��ߙ� ��� ������� ������� "������ �  ��� ����� ��� ��ߙ� ��� ��ϵ����� ײ�oֻ�� "ֻ�� o�֮�� �ָ� ��� ���������� ��ָ��� ��� ���������� ��Ӥ� ���� o�֮�� �ָ� ��֮�� ׾�������� �ָ� oֹ����� ���� ������ ױ������ ��ֈ�� ׌���� ������� ������ ������� "������ � ����� ��� ��ߙ� �ָ����� �ִ�֯�� ������ �ָ� ���Ù� ���״�׮�Ù������� ���� ������� ������� ��� �  ײ�oֻ�� o�֮�� ��� �ִֵ� ��� o��� ���׮� ����� ��� ������ ��ן֯��ٟ� ��� o�ֵ����� �  ���� ׮���Ե�, ���Ù� ���״�׮�Ù������� �������� �<br><br>
				<b>Preview ���� Submit ������� </b><br><br>
				<ol>
				<li>��Ͽ��/��ָ� ��ס���� ���� 15 ״֮֙� ��� ��֮�� �֯� ���� �֣�� ׸������� ���� o�֋��� �</li><br><br>
				<li>�ֳ�� ��Ͽ���� ��� ��ָ� "ֵ֮� ������� ��� ��֤� �ָ����֣�� ���� "Preview & Submit" �֙���, o��� ��� ��Ͽ�֯֡� ��� ���"�� פ���� �ֵ�� ���, �ָ� ��ֈ�� ��ֿ�� ׌���� ������� "������ � </li><br><br>
				<li>�ָ�����֣��� ���ָ� ��� �ֵ�� �֨�ן� ��� ������ָ� ��֮�� פ�� �֋ ��ָ��� ��� "ֵ֮� Preview page �ָ� "Submit" �֙��� ���� ׌���� ������� ��� ������� �֤��� ������� ��� � ������ֻ�� ��� ���"����ֻ�� ��Ͽ���� ���� ����֮�� ��� ׻֋ ����߮� ��� ��������� ���� �����Ե�� �ֵ�� ������� ��ָ�� ��� ��ֵ����� ���� ������� ���Ӆ �ֳ�� ��Ͽ���� ��� ��ָ� ������ ��� ��֤� ���ք o���"� ������� ��� ׻֋ "Back" �֙��� �ָ� ׌���� ������</li><br><br>
				<li>���� '�ә�� ��� �ִֵ� ��ߟ֮�� ��� ��֤� �ָ����֣�� ������ ��� ��Ͽ�� ��� ��ָ� ����� ��� �������� ���� �� ��� o���"� ���� �������� ��ָ� �ָ����֣�� Preview page �ָ� "Submit"  �֙��� �ָ� ׌���� ����� ��� �������� ���� ��� ���������� �����ֻ�� ������ ��ָ��� ���� ��֮�� �֯� ���� ���� ������ �</li><br><br>
				<li>"Preview and Submit" �֙��� �ָ� ��� ��ָ� ׌���� ���� o�֮�� �ָ� �o֮� ��Ͽ���� ��� ��ָ� פ���� �ֵ�� ���� ���� �o֮���� ��ָ� ����� פ���� �ֵ�� ����, פ���ֵ�� ������ ���Ӆ ��Ӳ���֟� ��Ͽ�� ���ܵ�� �ָ� ׌���� ������� �ָ����֣�� ��Ͽ�� �֡� ��� ���� �ָ� ��� ׾�׿�� ��Ͽ�� �ָ� ������"� o�֟�� ����</li><br><br>
				<li>�ָ����� ��� ���������� ����� ������ ��� ��֤� "Preview & Submit Page" �ָ� פ���� �ֵ�� "Submit" �֙��� �ָ� ������ ������� ��� � ��פ� ���"� ��Ͽ���� ��� ��ָ� ����� פ���� �ֵ�� ��� ���� ���� "���־֮�� ��Ӥ���� ״ֻ����� - "You have unattempted ----- questions . Do you really want to Submit ? " � ��� �ã�ן� ��� ��� "cancel" �֙��� �ָ� ׌���� ������� Preview Page �ָ� o�� ������� �� "OK" �֙��� �ָ� ׌���� ������� ��� �ָ����� ���� ��Ϥ�ٿ֟� ���� o�ֵ����օ  </li><br><br>
				<li>د�ϙ��ֈ�� : ��פ� �ָ����֣�� "����� ���� "د�ϙ�" �֙��� �ָ� ׌���� ������� ��֮�� �ָ����ֱ��� ��� د�ϙ� ��� ������� ��� (��� ��ָ�� ��� oֹ����� ������ �ָ� �ָ����� ��Ͽ������ ����ֵ� �������� �), ���֣�� "EXIT" �֙��� �ָ� ׌���� ������</li><br><br>
				</ol>
				</font>
				<? } ?>
				</td>
			</tr>
		</table><br>
</center>
<form name=frmname method=post action="online_exam.php" target='CallMePopUp'>
	<input type=hidden name="time_left" value="<?echo $hid_lefttime;?>">
	<input type=hidden name="memno" value="<?echo $memno;?>">
	<input type=hidden name="subject_code" value="<?echo $subjectCode;?>">
	<input type=hidden name="exam_code" value="<?echo $examCode;?>">
	<input type=hidden name="question_paper_no" value="<?echo $questionPaperNo;?>">
	<input type=hidden name="test_id" value="<?echo $testID;?>">
	<input type=hidden name="first_submit" value="Y">
	<input type=hidden name="ta_override" value="<?=$taOverride ?>">	
</form>	
<?

echo <<<EOD
<script language="javascript">
	var newWidth = screen.availWidth-8;
    var newHeight = screen.availHeight-55;
    var now = new Date();
	var hours = now.getHours();
	var minutes = now.getMinutes();
	var seconds = now.getSeconds();
	var winname = new String('ExamPopUp'+hours+minutes+seconds);
	window.name='intermediate';	
	
	var w =window.open('loading.html',winname,config="width="+newWidth+" height="+newHeight+",top=2,left=2,status=yes,scrollbars=yes,titlebar=no,resizable=no,menubar=no,toolbar=no");
	//setTimeout('alterWindow(w)', 1);	
	var set_timeout1=setTimeout("document.frmname.target='"+winname+"'",100);
	var set_timeout=setTimeout("document.frmname.submit();",200);	
	w.focus();	
	//self.close();	
</script>
<script language="JavaScript"><!--
function fullwin() 
{
     var newWidth = screen.availWidth-8;
     var newHeight = screen.availHeight-55;
     window.open("newWindow.html","newWindow", config="width="+newWidth+" height="+newHeight+",scrollbars=yes,titlebar=yes,status=yes")
}
function maximiseWindow(winRef) {
  if (window.screen) {
    winRef.moveTo(0,0);
    winRef.outerHeight = screen.availHeight;
    winRef.outerWidth = screen.availWidth;
  }
}

function alterWindow(winRef) {
  maximiseWindow(winRef);
  winRef.focus();
}

//var windowHandle = window.open('a.html','winname','fullscreen=yes');

//--></script>
EOD;
?>
</center>
</body>
</html>
