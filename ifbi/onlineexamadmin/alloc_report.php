<?php
/****************************************************
* Application Name            :  IIB
* Module Name                 :  Allocation report
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :
* Tables used for only selects: iib_exam_centres,iib_exam_schedule,iib_exam_slots,
                                iib_iway_details,iib_candidate_iway
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              : CSV file & HTML Page
* Document/Reference Material :
* Created By                  :
* Created ON                  :
* Last Modified By            :  K KARUNA MOORTHY RAJU
* Last Modified Date          :  01-04-2006
* Description                 : This report displaying the Allocation report for the candidates with their related information and also downloaded as csv file.
*****************************************************/

    ob_start();
	require_once "sessionchk.php";
	require_once("dbconfig_slave_123.php");

/* Include file for excel download */

	include_once("excelconfig.php");
	$sqlcentre = "SELECT trim(exam_centre_code),upper(exam_centre_name) FROM iib_exam_centres where online='Y' ";
	$rescentre = mysql_query($sqlcentre);
	$sqlDate = "SELECT distinct exam_date FROM iib_exam_schedule order by exam_date";
	$resDate = mysql_query($sqlDate);
	$nDates = mysql_num_rows($resDate);
		if ($nDates > 0)
		{
			while (list($eDate) = mysql_fetch_row($resDate))
			{
				$aDate = explode("-",$eDate);
				$aDispDate[$eDate] = $aDate[2]."-".$aDate[1]."-".$aDate[0];
			}
		}
	$sqlTime ="SELECT slot_time FROM iib_exam_slots order by slot_time";
	$resTime = mysql_query($sqlTime);
	$nTime = mysql_num_rows($resTime);
		if ($nTime > 0)
		{
			while (list($eTime) = mysql_fetch_row($resTime))
			{
				$aDispTime[$eTime] = substr($eTime,0,5);
			}
		}

/* $ex_flag is for getting the values when clicking download button and $filename is used for
downloading the file with its value **/

	$examCentreCode = $_POST['exam_centre_code'];
	$examDate = $_POST['exam_date'];
	$examTime = $_POST['exam_time'];
    $submitted= $_POST['submitted'];
    $ex_flag=$_POST['exl'];
 if($examTime!='all')
  {
    $extime = explode(':',$examTime);
    $extime = $extime[0]."-".$extime[1];
  }
  else
    $extime = $examTime;

    $filename="alloc_report(".$examCentreCode.")_sub(".$examDate.")_Time(".$extime.")";

    	if($examCentreCode=='all')
    	{
       			if($examTime=='all')
       				$exce_header=array("Cafe Id","City","Exam Time","Capacity","No. of Candidates allocated","% of capacity utilised");
	 			else
	   				$exce_header=array("Cafe Id","City","Capacity","No. of Candidates allocated","% of capacity utilised");
    	}
    	else if($examCentreCode!='all')
     	{
	  			if($examTime=='all')
       				$exce_header=array("Cafe Id","Exam Time","Capacity","No. of Candidates allocated","% of capacity utilised");
	 			else
	   				$exce_header=array("Cafe Id","Capacity","No. of Candidates allocated","% of capacity utilised");
 		 }

    	if ($examCentreCode !='')
    	{
				if ($examCentreCode !='all')
				{

	    			$sql_centrecode="select distinct a.centre_code from iib_iway_details a,iib_exam_centres b where a.exam_centre_code=b.exam_centre_code and b.exam_centre_code='".$examCentreCode."' and online='Y' ";
	    			//echo $sql_centrecode;
	    			$res_centrecode=mysql_query($sql_centrecode);
	    			$centre_code='';

						while(list($centrecode)=mysql_fetch_row($res_centrecode))
						{
						    $centrecode = strtoupper($centrecode);
							if ($centre_code =='')
								$centre_code = "(b.centre_code='$centrecode' ";
							else
								$centre_code .= " or b.centre_code='$centrecode'";
						}
					$centre_code .= " ) ";
				}
		}

    /*   if($populatecenter=='true' || $submitted=='true')
    	{
        	$sqlCenter = "select centre_code from iib_iway_details where exam_centre_code='".$examCentreCode."'";
       		$resCenter = @mysql_query($sqlCenter);
        } */
/**** Select the centre code,actualseats,exam date and other details depends in theexamCentrecode and examDate    ****/

  if (($examCentreCode != "") && ($examDate != "") && $submitted=='true')
  {
   		switch($examCentreCode)
        		{

            	case "all":

                	if($examTime=='all')
                	{

                    	//$sqlFormat = "select a.centre_code,upper(a.iway_city),a.actual_seats,b.exam_time from iib_iway_details a,iib_candidate_iway b where a.centre_code=b.centre_code and b.exam_date='".$examDate."' group by a.centre_code,a.actual_seats order by a.iway_city ";
                    	$sqlFormat = "select distinct a.centre_code,a.exam_centre_code,a.actual_seats,b.exam_date,b.exam_time from iib_iway_details a,iib_candidate_iway b where a.centre_code=b.centre_code and b.exam_date='".$examDate."'   order by a.centre_code,b.exam_time ";
                    	//echo $sqlFormat;

                	}
                	else
                	{
                    	//$sqlFormat = "select a.centre_code,upper(a.iway_city),a.actual_seats,b.exam_time from iib_iway_details a,iib_candidate_iway b where a.centre_code=b.centre_code  and b.exam_date='".$examDate."' and b.exam_time='".$examTime."' group by a.centre_code,a.actual_seats order by a.iway_city ";
                    	$sqlFormat = "select distinct a.centre_code,a.exam_centre_code,a.actual_seats,b.exam_date,b.exam_time from iib_iway_details a,iib_candidate_iway b where a.centre_code=b.centre_code  and b.exam_date='".$examDate."' and b.exam_time='".$examTime."'  order by a.centre_code,b.exam_time  ";
                    	//echo $sqlFormat;
               		 }

    	            break;

        		    default :

                	if($examTime=='all')
                	{

                    	//$sqlFormat = "select a.centre_code,upper(a.iway_city),a.actual_seats,exam_time from iib_iway_details a,iib_candidate_iway b where $centre_code and a.centre_code=b.centre_code and b.exam_date='".$examDate."' group by a.centre_code,a.actual_seats order by a.iway_city ";
                    	$sqlFormat = "select distinct a.centre_code,a.exam_centre_code,a.actual_seats,b.exam_date,b.exam_time from iib_iway_details a,iib_candidate_iway b where $centre_code and a.centre_code=b.centre_code and b.exam_date='".$examDate."' order by a.centre_code,b.exam_time ";
                    	//echo "SQL FORMAT ".$sqlFormat;
                	}
               		else
                	{

                    	//$sqlFormat = "select a.centre_code,upper(a.iway_city),a.actual_seats,exam_time from iib_iway_details a,iib_candidate_iway b where $centre_code and a.centre_code=b.centre_code and b.exam_date='".$examDate."' and b.exam_time='".$examTime."' group by a.centre_code,a.actual_seats order by a.iway_city ";
                    	$sqlFormat = "select distinct a.centre_code,exam_centre_code,a.actual_seats,b.exam_date,b.exam_time from iib_iway_details a,iib_candidate_iway b where $centre_code and a.centre_code=b.centre_code and b.exam_date='".$examDate."' and b.exam_time='".$examTime."'  order by a.centre_code,b.exam_time  ";
                    	//echo "SQL FORMAT ".$sqlFormat;
                	}
                	break;

        	}
        $resFormat = mysql_query($sqlFormat);
        $sql_city="select trim(exam_centre_code),exam_centre_name  from iib_exam_centres";
		$res_city=mysql_query($sql_city);
		$ncount =mysql_num_rows($resFormat);

/**** Getting the city name into an array named cityArr     ****/

			while(list($examCentre_Code,$cityName)=mysql_fetch_row($res_city))
			{
				$cityArr[$examCentre_Code]=$cityName;
			}

/**** select depends on examTime  ****/

			if($examTime=='all')
			{
	          	$sqlcand="select count(1),centre_code,exam_date,exam_time from iib_candidate_iway group by centre_code,exam_date,exam_time";
	        }
          	else if($examTime!='all')
          	{
	          	$sqlcand="select count(1),centre_code,exam_date,exam_time from iib_candidate_iway group by centre_code,exam_date,exam_time";
            }

	    $rescand=mysql_query($sqlcand);

/**** Getting the allocated seats into an array named allocArr     ****/

	        while(list($cnt,$CentreCode,$ExamDate,$ExamTime)=mysql_fetch_row($rescand))
	        {
		        $CentreCode = strtoupper($CentreCode);
		        $allocArr[$CentreCode][$ExamDate][$ExamTime]=$cnt;
	        }
	        $i=0;
         	if($examCentreCode=='all')
         	{

/**** Getting the centrecode,examcentrecode,actualseats,examdate,examtime into an array named exce_content[][].      ****/

		   		while(list($centre_code,$exam_centre_code,$actual_seats,$exam_date,$exam_time)=mysql_fetch_row($resFormat))
		   		{
                    $centre_code = strtoupper($centre_code);
	            	$city=$cityArr[$exam_centre_code];
		            $exce_content[$i][]=$centre_code;
		            $exce_content[$i][]=strtoupper($city);


	    		       if($examTime=='all')
			   				$exce_content[$i][]=$exam_time;
	            			$exce_content[$i][]=$actual_seats;
	            			$alloc=$allocArr[$centre_code][$exam_date][$exam_time];
	            			$exce_content[$i][]=$alloc;
	            			$perUtil=(($alloc/$actual_seats)*100);
	            			$exce_content[$i][]=round($perUtil,2);
	            $i++;
               }
        }
        else if($examCentreCode!='all')
           {

		   	while(list($centre_code,$exam_centre_code,$actual_seats,$exam_date,$exam_time)=mysql_fetch_row($resFormat))
		   		{

                    $centre_code = strtoupper($centre_code);
	        		$city=$cityArr[$exam_centre_code];
	            	$exce_content[$i][]=$centre_code;

	        			if($examTime=='all')
	         				$exce_content[$i][]=$exam_time;
	            			$exce_content[$i][]=$actual_seats;
	            			$alloc=$allocArr[$centre_code][$exam_date][$exam_time];
	            			$exce_content[$i][]=$alloc;
	            			$perUtil=(($alloc/$actual_seats)*100);
	            			$exce_content[$i][]=round($perUtil,2);
	        $i++;
            }
         }



/* Execution of excel download function */

	if($ex_flag=='1')
   		{
	   	 	excelconfig($filename,$exce_header,$exce_content);
	   		exit;
    	}
   }
?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='JavaScript' src="./includes/validations.js"></script>
<script language='JavaScript'>
function submitForm()
{
	if (document.frmalloc.exam_centre_code.selectedIndex == 0)
	{
		alert("Please select the exam centre");
		document.frmalloc.exam_centre_code.focus();
		return false;
	}
	if (document.frmalloc.exam_date.selectedIndex == 0)
	{
		alert("Please select the exam date");
		document.frmalloc.exam_date.focus();
		return false;
	}
    document.frmalloc.submitted.value='true';
    document.frmalloc.exl.value='0';
	document.frmalloc.submit();
}

function excelsubmit()
{
	var frmname=document.frmalloc;
	frmname.submitted.value='true';
	frmname.exl.value='1';
	frmname.submit();
}


</script>

</HEAD>
<BODY leftMargin=0 topMargin=0 LeftMargin=0 marginheight=0 marginwidth=0 >
<center>
<form name='frmalloc' method='post'>
       <input type='hidden' name='populatecenter'>
       <input type='hidden' name='submitted'>
       <input type=hidden name=exl value='0'>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
	 <tr><td width="780"><?include("includes/header.php");?></td></tr>
    <TR>
    
    </TR>

	<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>
	</tr>

    <TR>
    	<TD background=images/tile.jpg vAlign=top width=780 align=center>

         <table width=780 cellpadding=0 cellspacing=5 border=0 >
        	<tr>
				<td colspan=2 align="center"><b><font face="Verdana, Arial, Helvetica, sans-serif" size=2>Allocation Report</b></font><br><br></td>
            </tr>

            <!--<tr>
            	<td colspan=2 align="center" class="greybluetext10"><b><?=$msg ?></b></td>
            </tr>-->

            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Exam Centre : </td>
            	<td align="left" valign=top class="greybluetext10">
            		<select name='exam_centre_code' class="greybluetext10" style={width:150px} <!--onchange='javascript:selcentre();'-->>
            			<option value=''>--Select--</option>
            			<?
            					if ($examCentreCode == "all")
	            				{
		            				$selectedAll = "selected";
	            				}
            			?>
            			<option value='all' <?=$selectedAll?>>--All--</option>
            			<?


            				while (list($code,$name) = mysql_fetch_row($rescentre))
            				{
	            				print("<option value='$code' ");
	            				if ($examCentreCode == $code)
	            				{
		            				print(" selected ");
	            				}


	            				print(">$name</option>");
	            			}
	            		?>
            		</select>
            	</td>
           </tr>
           <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Exam Date : </td>
            	<td align="left" valign=top class="greybluetext10">
            		<select name='exam_date' class="greybluetext10" style={width:150px} >
            			<option value=''>--Select--</option>
            			<?php
            				foreach ($aDispDate as $key=>$val)
            				{
	            				print("<option value='$key' ");
	            				if ($examDate == $key)
	            				{
		            				print(" selected ");
	            				}
	            				print(">$val</option>");
            				}
            			?>
            		</select>
            	</td>
            </tr>
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Exam Time : </td>
            	<td align="left" valign=top class="greybluetext10">
            		<select name='exam_time' class="greybluetext10" style={width:150px} >
            			<option value='all'>All Shifts</option>
            			<?php
            				foreach ($aDispTime as $key=>$val)
            				{
	            				print("<option value='$key' ");
	            				if ($examTime == $key)
	            				{
		            				print(" selected ");
	            				}
	            				print(">$val</option>");
            				}
            			?>
            		</select>
            	</td>
            </tr>



      	</table>
    	<table width=780 >
        	<tr>
				<td colspan=2 align="center"><input type=button name='sub_assign' value='Submit' class='button' onClick='javascript:submitForm()'>
				</td>
        	</tr>
        <?
/*** It checks with the flag value and it retrieving the data from the array and displaying the report in the
     html page  **/

          if (($submitted=='true') && ($ncount<1)){?>

         	<tr>
				<td width=780 align=center class=alertmsg>No Records to Display</td>
        	</tr> <?}?>
    	</table>

    <?if (($submitted=='true')&& $ncount>=1){

          $header_count = count($exce_header);
          // Printing the values in the HTML page

			// Printing the header values from excel header array
		print("<table border=1 cellspacing=0 cellpadding=2><tr>");
		for($a=0;$a<$header_count;$a++)
		{
			print("<td class=greybluetext10><b>$exce_header[$a]</b></td>");
		}
		print("</tr>");

		// Loop to display the values in the HTML page
		print("<tr>");

              for($j=0;$j<$ncount;$j++)
             	{
	          		for($k=0;$k<$header_count;$k++)
	           		{
		          		$chek = $header_count-1;

/* Background color of the each column is changed depends on conditions */

		        		if($exce_content[$j][$chek] < 50)
	          			{
		          	        $bgcolor="#DF0063";
	            			$fontc  ="black";
	            		}
               			else if (($exce_content[$j][$chek]>= 50)  && ($exce_content[$j][$chek] < 75))
	           			{
		           			$bgcolor="#A2D389";
		           			$fontc  ="black";
	           			}
            	        else if (($exce_content[$j][$chek] >= 75) && ($exce_content[$j][$chek] < 90))
	        		      {
		                      $bgcolor="#A6A6FF";
		            		  $fontc  ="white";
	              		  }

           				else if ($exce_content[$j][$chek] >= 90 )
	         			  {

	               			 $bgcolor="white";
	               			 $fontc=" ";
	                      }

		         ?>
                  <td class="greybluetext10" bgcolor="<?=$bgcolor?>"><font color="<?=$fontc?>"><?=$exce_content[$j][$k]?>&nbsp;</font></td>
		          <?
	            }
	           print("</tr>");
	          }

             ?></table>

		<table width=300  cellspacing=5 border=0 align=center >
			<? if ($perUtil !=''){?>
				<tr>
					<td>&nbsp;</td></tr>
				<tr>
					<td colspan=2 bgcolor= #DF0063></td>
					<td class="greybluetext10" colspan=4>Alloted < 50%</td>
				</tr>
				<tr>
					<td colspan=2 bgcolor=#A2D389></td>
					<td class="greybluetext10" colspan=4>Alloted >= 50% and < 75% </td>
				</tr>
				<tr>
					<td colspan=2 bgcolor=#A6A6FF></td>
					<td class="greybluetext10" colspan=4>Alloted >= 75% and < 90% </td>
				</tr>
				<tr>
					<td colspan=2 bgcolor=white></td>
					<td class="greybluetext10" colspan=4>Alloted >= 90% </td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				 <? } ?>
		   </table>

	 <?

/* It checks the availability of records */ ?>
		   <table>
	  		<tr class=greybluetext10><td align="center" class=greybluetext10><input class='button' type='button' name='download' value='Download' onClick='javascript:excelsubmit()'></td></tr>
	  		<tr><td class=greybluetext10>&nbsp;</td></tr>
	       </table>
	  <?  }   ?>

	  </td>
  </TR>

   <TR>
     	<?include("includes/footer.php");?>
   </TR>
 </TABLE>


</form>
</center>
</BODY>
</HTML>