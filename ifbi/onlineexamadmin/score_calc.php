<?php
/****************************************************
* Application Name            :  IBPS
* Module Name                 :  Score Calculation
* Revision Number             :  1
* Revision Date               :
* Table(s)  modified          : iib_candidate_scores,iib_correct_scores_log(New Table)
* Tables used for only selects: iib_exam_schedule,iib_exam,iib_exam_subjects,iib_candidate_scores,iib_candidate_test,iib_section_questions,iib_question_paper_details,
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              : Information regarding the update
* Document/Reference Material :
* Created By                  : Anil Kumar.A
* Created On 				  : 18/Apr/2006 
* Last Modified By            :  
* Last Modified Date          : 
* Description                 :  Calculating the scores after new set of keys has been uploaded.
*****************************************************/
require("dbconfig.php");
require("sessionchk.php");
set_time_limit(0);

//Values got from Post variables
$examCode = isset($_POST['exam_code']) ? $_POST['exam_code'] : '';
$subjectCode = isset($_POST['subject_code']) ? $_POST['subject_code'] : '';
$examDate = isset($_POST['exam_date']) ? $_POST['exam_date'] : '';

//Hidden variables for status
$subject = isset($_POST['subject']) ? $_POST['subject'] : 'false';
$submitted= isset($_POST['submitted']) ? $_POST['submitted'] : 'false';

//Selecting the exam dates for drop down(I/P)
$sqlDate = "SELECT distinct exam_date FROM iib_exam_schedule order by exam_date ";
$resDate = mysql_query($sqlDate);

//Selecting the exams for drop down(I/P)
$sqlexam = "SELECT exam_code,upper(exam_name) FROM iib_exam where online='Y' ";
$resexam = mysql_query($sqlexam);

//Variable for diaplaying the message
$msg='';

//Selecting the subjects only if one of the hidden flag is set for drop down(I/P)
if (($subject == 'true') || ($submitted == 'true'))
{
    $sql_subject_sel="select exam_code,subject_code,subject_name from iib_exam_subjects where exam_code='".$examCode."' order by subject_name";
    $res_subject_sel=mysql_query($sql_subject_sel);
}

//Calcualtion,Updation,Insertion Happens here if all the condns are satisfied
if($submitted=='true' && $examCode!='' && $subjectCode!='' && $examDate!='')
{
	//Datetime for Log insertion
	$date=date("Y-m-d");
	$time=date("H:i:s");
	$datetime=$date." ".$time;
	
	//Selecting the grace mark,pass mark for score calculation
	$sql_grace="select grace_mark,pass_mark from iib_exam_subjects where exam_code='$examCode' and  subject_code='$subjectCode'";
	$res_grace=mysql_query($sql_grace);
	list($graceMark,$pass_mark)=mysql_fetch_row($res_grace);
	
	//Selcting the the questionId,correct answer, actual marks ,-ve marks for the question
	$sql_sect_ques="select question_id , correct_answer ,marks, negative_marks from iib_section_questions where exam_code ='$examCode' and subject_code ='$subjectCode'";
	$res_sect_ques=mysql_query($sql_sect_ques);
	while(list($sec_ques_id,$sec_corr_answer,$sec_marks , $sec_neg_mark)=mysql_fetch_row($res_sect_ques))
	{
		$secArr[$sec_ques_id][0]=$sec_corr_answer;
		$secArr[$sec_ques_id][1]=$sec_neg_mark;
		$secArr[$sec_ques_id][2]=$sec_marks;
	}
	
	//Selecting the candidates , their question paper nos 
	$sql_cand_scores="select a. membership_no , b. question_paper_no , a.score from iib_candidate_scores a , iib_candidate_test b where a. membership_no = b. membership_no and a.exam_code='$examCode' and a. subject_code ='$subjectCode' and trim(a. exam_date) like trim('$examDate%') group by b. question_paper_no";
	$res_cand_scores=mysql_query($sql_cand_scores);
	$cnt_cand_scores=mysql_num_rows($res_cand_scores);
	if($cnt_cand_scores < 1)
	{
		$msg="No records found";
	}
	else
	{
		//Variable for counting the no.of candidates for whom the update has happened successfully
		$cnt=0;	
		while(list($mem_no,$ques_paper_no,$old_score)=mysql_fetch_row($res_cand_scores))	
		{
			//Selecting the questionId ,answer given by the candidate
			$sql_cand_ques="select question_id , answer from iib_question_paper_details where question_paper_no='$ques_paper_no' ";
			$res_cand_ques=mysql_query($sql_cand_ques);
			
			//Variable for calculating the score
			$score=0;
			
			//This loop runs for the no.of questions the particular question paper
			while(list($can_ques_id,$cand_answer)=mysql_fetch_row($res_cand_ques))
			{
				$correctAns=$secArr[$can_ques_id][0];
				$neg_mark=$secArr[$can_ques_id][1];
				$Marks=$secArr[$can_ques_id][2];
				
				//Checking the answer given by the candidate and the correct answer
				if ($cand_answer == $correctAns)
  				{
			  		$score += $Marks;
		  		}
		  		if (($cand_answer != $correctAns) && ($cand_answer != ""))
		  		{
			  		$score -= $neg_mark;
		  		}
		  		
			}
			
			//Checking if the score including the grace mark is more than pass mark
			if (($score+$graceMark) >= $pass_mark)
			{
				$examResult = 'P';
			}
			else
			{
				$examResult = 'F';
			}
			
			//Updating the score , result thus obtained in iib_candidate_scores table
			$sql_upd_scores="update iib_candidate_scores set score='$score',result='$examResult' where membership_no='$mem_no' and exam_code='$examCode' and subject_code='$subjectCode'";
			$res_upd_scores=mysql_query($sql_upd_scores);
			
			//If update has happened successfully then increment cnt variable
			if($res_upd_scores)
			{
				//Inserting certain details in the iib_correct_scores_log table for tracking purpose
				$sql_score_log="insert into iib_correct_scores_log (membership_no,exam_code,subject_code,old_score,new_score,result,updatedtime) values ('$mem_no','$examCode','$subjectCode','$old_score','$score','$examResult','$datetime')";
				$res_score_log=mysql_query($sql_score_log);
				
				//Updated candidates count incremented here
				$cnt++;
			}
		}
		$msg="Score has been calculated for ".$cnt." Candidates.<br>Please use Score Report link for viewing the score details of the candidates.";
	}
}
?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
	FONT-FAMILY: Arial, Verdana	
}
.textblk11     { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; text-decoration: none}
</STYLE>
<script language='JavaScript'>
function getsubjects()
{
	if (document.frm_scorecalc.exam_code.selectedIndex == 0)
	{		
		alert("Please select the exam ");
		document.frm_scorecalc.exam_code.focus();
		return false;
	}
	
	document.frm_scorecalc.subject.value='true';
	document.frm_scorecalc.submit();
	
}
function submitForm()
{
	if (document.frm_scorecalc.exam_date.selectedIndex == 0)
	{
		alert("Please select the exam date");
		document.frm_scorecalc.exam_date.focus();
		return false;
	}
	if (document.frm_scorecalc.exam_code.selectedIndex == 0)
	{		
		alert("Please select the exam ");
		document.frm_scorecalc.exam_code.focus();
		return false;
	}
	if (document.frm_scorecalc.subject_code.selectedIndex == 0)
	{		
		alert("Please select the Subject ");
		document.frm_scorecalc.subject_code.focus();
		return false;
	}
	if (!confirm ("Make sure that Correct answers are Updated!"))
	{
		return;
	}
	else
	{
		document.frm_scorecalc.submitted.value='true';
		document.frm_scorecalc.submit();
	}
}
</script>
</head>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
	<tr><td><?include("includes/header.php");?> </td></tr>
	<tr>
		<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>	
	<TR>
	    <TD background=images/tile.jpg vAlign=top width=780 align=center>
<form name='frm_scorecalc' method='post'>
<input type='hidden' name='subject' value=''>
<input type='hidden' name='submitted'>
<table width=100% border=0 cellspacing=0 cellpadding=5>
	<tr>
		<td colspan=2 align="center"><b><font face="Verdana, Arial, Helvetica, sans-serif" size=2>Score Calculation</b></font><br><br></td>
    </tr>
	<!--<tr><td align=right><div align=right><b>Score Calculation</b></div></td></tr>-->
	<tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Exam Date : </td>
            	<td align="left" valign=top class="greybluetext10">
            	<select name='exam_date' style={width:150px} class=greybluetext10>
				<option value=''>--Select--</option>
				<?php
				if (mysql_num_rows($resDate) > 0)
				{
					while (list($eDate) = mysql_fetch_row($resDate))
					{
						$aDate = explode("-",$eDate);
						$dispDate = $aDate[2]."-".$aDate[1]."-".$aDate[0];
						print("<option value='$eDate' ");
						if ($eDate == $examDate)
							print(" selected ");
						print(">$dispDate</option>");
					}
				}
				
				?>
            		</select>
            	</td>
            </tr> 
             <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Exam : </td>
            	<td align="left" valign=top class="greybluetext10">
            		<select name='exam_code' class="greybluetext10" style={width:275px} onchange='javascript:getsubjects();'>
            			<option value=''>--Select--</option>
            			<?
            				while (list($code,$name) = mysql_fetch_row($resexam))
            				{
	            				print("<option value='$code' ");
	            				if ($examCode == $code)
	            				{
		            				print(" selected ");
	            				}
	            		
	            				
	            				print(">$name</option>");
	            			}
	            		?>
            		</select>
            	</td>
            </tr>   
              <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Subject : </td>
            	<td align="left" valign=top class="greybluetext10">
            		<select name='subject_code' class="greybluetext10" style={width:350px} >
            			<option value=''>--Select--</option>
            			
            			<?
            				while (list($code,$Scode,$Sname) = mysql_fetch_row($res_subject_sel))
            				{
	            
	            				print("<option value='$Scode' ");
	            				if ($subjectCode == $Scode)
	            				{
		            				print(" selected ");
	            				}
	            		
	            					print(">$Sname</option>");
	            			}
	            		?>
            		</select>
            	</td>
            </tr>
            
            
            
      </table>   
    <table width=780 >       
        <tr>
			<td colspan=2 align="center"><input type=button name='sub_assign' value='Calculate Scores' class='button' onClick='javascript:submitForm()'>
			
			</td>
        </tr>
        <?
        if($msg!='')
        {
        ?>
        <tr>
            <td colspan=2 align="center" class='alertmsg'><b><?echo $msg;?></b></td>
        </tr>
        <?}?>
</table>
</form>
</TD>
	</TR>
	 <tr>
    <? include("includes/footer.php");?>
  </tr>
</TABLE>
</center>
</BODY>
</HTML>