<?php
require_once "sessionchk.php";
require_once "dbconfig.php";

$selectedDate = $_REQUEST["sYear"]."-".$_REQUEST["sMonth"]."-".$_REQUEST["sDate"];
$examCode = $_POST['exam_code'];

$errormsg ="";
$msg = "";
$changed = $_POST['changed'];
$sqlExams = "SELECT exam_code, exam_name FROM iib_exam WHERE online='Y' order by exam_name";
$resExams = @mysql_query($sqlExams);
if ($examCode != "")
{
	$sqlSubjects = "SELECT subject_code, subject_name FROM iib_exam_subjects WHERE online='Y' AND exam_code='$examCode' order by subject_name";
	$resSubjects = @mysql_query($sqlSubjects);
	$nSubjects = mysql_num_rows($resSubjects);	
}
$subjectCode = $_POST['subject_code'];
if (($examCode != "") && ($subjectCode != "") && ($selectedDate != "") && ($changed != 'Y'))
{
	$sqlChk = "SELECT exam_code, subject_code FROM iib_exam_schedule WHERE exam_code='$examCode' AND subject_code='$subjectCode'";
	$resChk = @mysql_query($sqlChk);
	$nChk = mysql_num_rows($resChk);
	if ($nChk > 0)
	{
		$errormsg = 'Exam/Subject already scheduled';
	}
	else
	{
		$sqlInsert = "INSERT INTO iib_exam_schedule (exam_code,subject_code, exam_date) VALUES ('$examCode','$subjectCode','$selectedDate')";
		@mysql_query($sqlInsert);
		if (mysql_error())
		{
			$errormsg = "Insert failed!";
		}
		else
		{
			$msg = "Exam/Subject successfully scheduled!";
		}
		$examCode = '';
		$subjectCode = '';
	}
}

//Add a Exam
?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
	FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='JavaScript'>
function getSubjects()
{
	document.addSchedule.changed.value ='Y';
	document.addSchedule.submit();
}

function validate()
{
	frm = document.addSchedule;
	
	if (frm.exam_code.selectedIndex == 0)
	{
		alert('Please select the Exam ');
		frm.exam_code.focus();
		return false;
	}
	if (frm.subject_code.selectedIndex == 0)
	{
		alert('Please select the Subject ');
		frm.subject_code.focus();
		return false;
	}
	frm.changed.value = 'N';
	frm.submit();
}

function submitForm()
{
    if (window.event.keyCode == 13)
    {
        validate();
    }
}
</script>
</HEAD>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0 onload="setmonth_val()" onKeyPress='javascript:submitForm()'>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
<!--	<TR><!-- Topnav Image -->
    <!--	<TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR>	 -->
	<tr><td width="780"><?include("includes/header.php");?></td></tr>
	<TR>
    	
	</TR>
	<tr>
		<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>	
	<TR>
	    <TD background=images/tile.jpg vAlign=top width=780 align=center>
     	<form name="addSchedule" method="post">
     	<input type=hidden name='changed' value=''>
		<table width=780 cellpadding=0 cellspacing=5 border=0>
			<tr>
				<td colspan=4 align="center" class=graybluetext10><b><font face="Verdana, Arial, Helvetica, sans-serif" size=2>Add Exam Schedule</font></b><br><br></td>
			</tr>		
			<tr>
				<td width=15%><br></td>
				<td valign=top class=greybluetext10 width=25% align=right><b>Exam&nbsp;&nbsp;: </b></td>
				<td valign=top class=greybluetext10 width=35% align=left>
					<select name="exam_code" class="textbox" style={width:150px} onChange='javascript:getSubjects()'>
						<option value=''>--Select--</option>
						<?php
						
						if (mysql_num_rows($resExams) > 0)
						{
							while (list($eCode, $eName) = mysql_fetch_row($resExams))
							{
								print("<option value='$eCode'");
								if ($examCode == $eCode)
								{
									print(" selected ");
								}
								print(">$eName</option>\n");
							}
						}						
						?>
					</select>
				</td>
				<td width=25%><br></td>
			</tr>
			<tr>
				<td width=15%><br></td>
				<td valign=top class=greybluetext10 width=25% align=right><b>Subject&nbsp;&nbsp;: </b></td>
				<td valign=top class=greybluetext10 width=35% align=left>
					<select name="subject_code" class="textbox" style={width:150px}>
						<option value=''>--Select--</option>
						<?php
						
						if ($nSubjects > 0)
						{
							while (list($sCode, $sName) = mysql_fetch_row($resSubjects))
							{
								print("<option value='$sCode'");
								if ($subjectCode == $sCode)
								{
									print(" selected ");
								}
								print(">$sName</option>\n");
							}
						}						
						?>
					</select>
				</td>
				<td width=25%><br></td>
			</tr>
			<tr>
				<td width=15%><br></td>
		    	<TD valign=top class=greybluetext10 align=right><b>Date&nbsp;&nbsp;: </b></td>
		    	<td valign=top class=greybluetext10 align=left>
	    			<select name="sDate" class=textbox onchange="set_cur_date()">
					<?php
						if (isset($_REQUEST["sDate"]))
						    $chk = $_REQUEST["sDate"];
						else
							$chk = date("d");
						for ($i = 1; $i<=31 ;$i++){
						 echo '<option value= '.$i;
						 if ($i == $chk)
						 	echo ' selected';
						echo '>'.$i.'</option>';
					} ?>
				</select>
				<select name="sMonth" onchange="setmonth_val()" class=textbox>
					<?php 
					if (isset($_REQUEST["sMonth"]))
					    $chk = $_REQUEST["sMonth"];
					else
						$chk = date("m");
					for ($i = 1;$i<=12;$i++)
					{
						 echo '<option value= '.$i;
						 if ($i == $chk)
						 	echo ' selected';
						 echo '>'.date("F",mktime(0,0,0,$i,1,date("Y"))).'</option>';
					 }
					 ?>
				</select>
				<select name="sYear" onchange="setdate()"  class=textbox>
					<?php
					if (isset($_REQUEST["sYear"]))
					    $chk = $_REQUEST["sYear"];
					else
						$chk = date("Y");
					for ($i = 0; $i <= 5;$i++){
					echo '<option value='.($i+date("Y"));
					if (($i+date("Y"))== $chk)
						echo ' selected';
					echo '>'.($i+date("Y")).'</option>';
					} ?>
				</select><br><br></TD>
				<td width=25%><br></td>
			</tr>
			<tr>
				<td colspan=4 align="center"><input type=button name="sub_add" value="Save" class=button onClick='javascript:validate()'>&nbsp;&nbsp;<input type="reset" class=button name="sub_reset" value="Reset"></td>
			</tr>
			<?php
			if ($errormsg != "")
			{
				print("<tr>
							<td colspan=4 align=center class='errormsg'>$errormsg</td>
						</tr>");
			}
			if ($msg != "")
			{
				print("<tr>
							<td colspan=4 align=center class='alertmsg'>$msg</td>
						</tr>");
			}
			?>
		</table>
		<input type="hidden" name=hDate>
					<input type="hidden" name=hMonth>
					<input type="hidden" name=hYear>										
					<input type="hidden" name=hCurDate value=<? echo date("d"); ?>>
					<input type="hidden" name=hCurMonth value=<? echo date("m"); ?>>
					<input type="hidden" name=hCurYear value=<? echo date("Y"); ?>>			
		</form>
		</TD>
	</TR>
	<TR>
		<?include("includes/footer.php");?>
	</TR>
</TABLE>

</center>
</BODY>
</HTML>

<script language="javascript">
frm = document.addSchedule;
var month_names = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
function checkval(){
frm.hDate.value  = frm.sDate.options[frm.sDate.selectedIndex].value;
frm.hMonth.value = frm.sMonth.options[frm.sMonth.selectedIndex].value;
frm.hYear.value = frm.sYear.options[frm.sYear.selectedIndex].value;
}

function set_cur_date(){
frm.hDate.value = frm.sDate.options[frm.sDate.selectedIndex].value; 

}

function setmonth_val(){
frm.hMonth.value = frm.sMonth.options[frm.sMonth.selectedIndex].value ; 
setdate();
}

function setdate() {  // change the date according to month and year
var yr,month,day;
var tmp_date = new Date();
 day=31;
 yr= frm.sYear.options[frm.sYear.selectedIndex].value; 
 month=frm.sMonth.options[frm.sMonth.selectedIndex].value;
 frm.sMonth.length=0;

 if (frm.hCurYear.value == frm.sYear.options[frm.sYear.selectedIndex].value)
 	startmonth=frm.hCurMonth.value;
 else 
	 startmonth = 1;

 if (parseInt(frm.hMonth.value) < startmonth){
 	frm.hMonth.value = startmonth;
}

 for (i=startmonth;i < 13;i++){
 if (parseInt(frm.hMonth.value) == i){
	frm.hMonth.value = i; 
 	dayopt = new Option(month_names[i-1],i,true,true);	 
 }	 
 else	
  	dayopt = new Option(month_names[i-1],i,false, false);	 
		frm.sMonth.options[frm.sMonth.length] = dayopt;
}
 month=frm.sMonth.options[frm.sMonth.selectedIndex].value;
 if(month == 2){
	var isleap = (yr % 4 == 0 && (yr % 100 != 0 || yr % 400 == 0));
	if(!isleap)       
        day=28;
	else
    	day=29;
 }
 if (month==4 || month==6 || month==9 || month==11)
 	day=30;
// window.defaultStatus = "day : " + day + " Month : "+ frm.hMonth.value + " " + startmonth + " startdate : " + frm.hDate.value + " Year : " + yr;
 if ((parseInt(frm.hCurYear.value) == yr) && (parseInt(frm.hCurMonth.value) == month)){
	startdate=frm.hCurDate.value;
	frm.hDate.value = startdate;
 }
 else 
	 startdate = 1;
  frm.sDate.length=0;
  for(i=startdate;i<=day;i++){
  	if (frm.hDate.value == i){
		dayopt = new Option(i,i,true,true);	 
	}
	else	
		dayopt = new Option(i,i,false,false);	 	
		frm.sDate.options[frm.sDate.length] = dayopt; 
 }
if (startdate == 1)
	frm.hDate.value = frm.sDate.options[frm.sDate.options.selectedIndex].value;

}

function set_cor_date()
{
	var yr,month,day;
 	day=31;
 	yr= frm.sYear.options[frm.sYear.selectedIndex].value; 
 	month=(frm.sMonth.options[frm.sMonth.selectedIndex].value);
 	if(month == 2)
 	{
		var isleap = (yr % 4 == 0 && (yr % 100 != 0 || yr % 400 == 0));
		if(!isleap)       
        	day=28;
		else
    		day=29;
 	}
 	if (month==4 || month==6 || month==9 || month==11)
 		day=30;
 
 	for(i=1;i<=((frm.sDate.options.length < day)?frm.sDate.options.length:day);i++)
 	{
 		frm.sDate.options[i-1].value=i;
    	frm.sDate.options[i-1].text=i;
 	}
 	if (day > frm.sDate.options.length)
 	{
		for (i = frm.sDate.options.length + 1;i<= day;i++)
		{
			dayopt = new Option(i,i,false,false);	 
			frm.sDate.options[frm.sDate.length] = dayopt;
		}	
 	}
 	for (j=frm.sDate.length-1;j>=day;j--)
 		frm.sDate.options[j]= null;
}
 
function submitpage()
{
 	frm.action="<?=$_SERVER['PHP_SELF'] ?>";
	frm.submit();
 }
</script>
