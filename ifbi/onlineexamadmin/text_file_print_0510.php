<?php
set_time_limit(0);
ob_start();
$admin_flag = 0;
require_once("sessionchk.php");
require_once("constants.inc");
require_once("dbconfig.php");

class TEXT_Table
{
var $widths;
var $page_height=41;
var $page_width=60;
var $current_height=0;
var $current_width=0;
var $new_page_flag=0;
var $print_page = array();
var $current_page_line = 1;
function TEXT_Table(){
	$page_height=41;
	$page_width=60;
	$current_width=0;
	$current_height=0;
	$new_page_flag=0;
}

function SetWidths($w)
{
    //Set the array of column widths
    $this->widths=$w;
}

function total_length(){
	$text_value = implode ($this->print_page);
	return strlen($text_value);
}

function Row($data,$char_val,$align)
{
    $this->CheckPageBreak($h);
    $max_height=0;
    $max_data =count($data);
    $max_lines = $this->max_lines($data);
    unset($text_array);
    unset($temp_array);
    for($i=0;$i<$max_data;$i++){
	$text_value = wordwrap($data[$i],$this->widths[$i],'\-/');
        $text_array[$i] = explode('\-/',$text_value);
	for ($j=0;$j<$max_lines;$j++){
		$temp= '';
		if ($i==0)
			$text_array[$i][$j]=ltrim($char_val).$text_array[$i][$j];
		$text_len = (array_key_exists($j,$text_array[$i]))?strlen($text_array[$i][$j]):0;
		$str_len_to_pad = $this->widths[$i] - $text_len;
		$temp=str_pad($temp,$str_len_to_pad,' ');  //,STR_PAD_BOTH);
       		if ($char_val != '')
			$temp .= $char_val;
               	$text_array[$i][$j] .= $temp;
	        if ($max_data - 1 == $i)
        	        $text_array[$i][$j] .= "\r\n";
	}
    }
    for($i=0;$i<$max_lines;$i++){
	$text_to_print='';
	for($j=0;$j<$max_data;$j++){
		$text_to_print .= $text_array[$j][$i];
	}
	if ($this->current_page_line > $this->page_height){
                $this->end_page();
                $this->add_page();
        }
	//$this->print_page[$this->current_height++]="- $this->current_page_line - ".$text_to_print;
	$this->print_page[$this->current_height++]=$text_to_print;
	$this->current_page_line++;
    }
}

function max_lines($data){
	$max_lines = 0;
    for($i=0;$i<count($data);$i++){
 		$text_value = wordwrap($data[$i],$this->widths[$i],'\-/');
	    $text_array = explode('\-/',$text_value);
	    if ($max_lines < count($text_array))
			$max_lines = count($text_array);
	}
return $max_lines;
}

function put_line(){
        if ($this->current_page_line > $this->page_height){
                $this->end_page();
                $this->add_page();
        }
	$text_to_print = '';
	$print_length = array_sum($this->widths)+(count($this->widths) * 3)-1;
	for ($i=0;$i < $print_length; $i++)
		$text_to_print .= '-';
	//$this->print_page[$this->current_height]=$text_to_print."- $this->current_page_line - \r\n";
	$this->print_page[$this->current_height++]=$text_to_print." \r\n";
	$this->current_page_line++;
}
function show_values(){
	$result_value = implode ($this->print_page);
	echo $result_value;
}

function CheckPageBreak($h)
{
	if ($this->current_page_height > $this->page_height){

	}
}
function PutLineBreak(){
        if ($this->current_page_line > $this->page_height){
                $this->end_page();
                $this->add_page();
        }
	//$this->print_page[$this->current_height++] = "- $this->current_page_line \r\n";
	$this->print_page[$this->current_height++] = "\r\n";
	$this->current_page_line++;
}

function Footer(){
 $this->print_page[$this->current_height++]= chr(12);
  for($i=0;$i<13;$i++){
	 // $this->print_page[$this->current_height++]= "\r\n";
	  $this->print_page[$this->current_height++]= "-F $i - \r\n";
}
}
function Header(){
  for($i=0;$i<12;$i++){
	 // $this->print_page[$this->current_height++]= "\r\n";
	  $this->print_page[$this->current_height++]= "-H $i -  \r\n";
	}
}
function end_page(){
// $this->print_page[$this->current_height++]= chr(12);
/*	for ($i=$this->current_page_line;$i <= $this->page_height; $i++){
		//$this->print_page[$this->current_height++]= "\r\n";
		$this->print_page[$this->current_height++]= "- Excess - $i - \r\n";
	}
	$this->Footer();
	$this->current_page_line=1; */
}
function add_page(){
//	$this->Header();
}
}
/*
$memno = array(
100061690,
400091113
);*/
/*$memno = array(
7280499,
7281020,
7288343,
'V11212',
'V11400'
); */ 

$offset = $_REQUEST["hOffset"];
$pagecount = $_REQUEST["hPageCount"];
$page = $_REQUEST["hPage"];
$each_link = $_REQUEST["hEachLink"];
$link_per_page = $_REQUEST["hLinkPerPage"];
$index = $_REQUEST["hIndex"];
$diff = $_REQUEST["hDiff"];
$centre_code = $_REQUEST["sCentreCode"];
$sExamCode=isset($_REQUEST["sExamCode"])?$_REQUEST["sExamCode"]:"";
$rec_per_page=$each_link*$link_per_page;
$displacement = $offset + ($index*$each_link);

if ($sExamCode== 0)
        $sql_iway_can = "select distinct(membership_no) from iib_candidate_iway  a, iib_iway_details b where b.exam_centre_code = '$centre_code' and a.centre_code = b.centre_code order by membership_no limit $displacement,$diff";
elseif ($sExamCode != "")
        $sql_iway_can = "select distinct(a.membership_no) from iib_candidate_iway a, iib_iway_details b where a.exam_code='$sExamCode' and b.exam_centre_code = '$centre_code' and a.centre_code = b.centre_code order by membership_no limit $displacement,$diff";

//echo $sql_iway_can;
$res_iway_can=mysql_query($sql_iway_can);
$num_can = @mysql_num_rows($res_iway_can);
if ($num_can < 1){
 echo "$sql_iway_can";
	 exit;
}
$j=0;
while ($j < $num_can){
list($memno[$j])= mysql_fetch_row($res_iway_can);
$j++;
}
$iway_array=array();
$add=1;
$num_can = count($memno);
$st_memno = $memno[0];
$end_memno = $memno[$num_can-1];
$memlist = implode('\',\'',$memno);
$sql_can = "select membership_no,name,password,address1,address2,address3,address4,address5,address6,pin_code from iib_candidate where membership_no in ('$memlist')";
$res_can = mysql_query($sql_can);
$txt=new TEXT_Table();
//$txt->Open();
$strFileName = 'Admit_'.$st_memno.'_'.$end_memno.'.txt'; 
$i=0;
while (count($memno) > $i ){
	list($c_no,$c_name,$c_passwd,$c_addr1,$c_addr2,$c_addr3,$c_addr4,$c_addr5,$c_addr6,$c_pin)=mysql_fetch_row($res_can);
	$caddress6 = "";
	if ($c_addr6 != "")
		$caddress6 .= $c_addr6." ".$c_pin;	
	else
		$caddress6 .= $c_pin;
	//$txt->add_page();
	$txt->PutLineBreak();
	/*$txt->setWidths(array(75));
	$txt->Row(array(chr(12)),'',''); */
	$txt->SetWidths(array(30,5,40));
	$txt->Row(array("Registration No",":",$c_no),'','L');
	$txt->PutLineBreak();
	$txt->SetWidths(array(75));
	$txt->Row(array($c_name),'','C');
	if ($c_addr1 != "")
	{
		$txt->SetWidths(array(75));
		$txt->Row(array($c_addr1),'','L');
	}
	if ($c_addr2 != "")
	{
		$txt->SetWidths(array(75));
		$txt->Row(array($c_addr2),'','L');
	}
	if ($c_addr3 != "")
	{
		$txt->SetWidths(array(75));
		$txt->Row(array($c_addr3),'','L');
	}
	if ($c_addr4 != "")
	{
		$txt->SetWidths(array(75));
		$txt->Row(array($c_addr4),'','L');
	}
	if ($c_addr5 != "")
	{
		$txt->SetWidths(array(75));
		$txt->Row(array($c_addr5),'','L');
	}
	if ($caddress6 != "")
	{
		$txt->SetWidths(array(75));
		$txt->Row(array($caddress6),'','L');
	}
	$sql_sel_can="select membership_no, exam_code,subject_code,exam_date,exam_time,centre_code from iib_candidate_iway where membership_no='$c_no' order by exam_date,exam_time";
	$res_sel_can=mysql_query($sql_sel_can);
	$num_sub=@mysql_num_rows($res_sel_can);
	if ($num_sub==0){
		$errmsg="No Subjects !";
	}
	$chk = 0;
	$txt->SetWidths(array(30,8,10,6,10));
	$txt->PutLineBreak();
	$txt->put_line();
	$txt->Row(array("Subject Name","Medium","Date","Time","Venue Code"),' | ','C');
	$txt->put_line();
	unset($iway_array);
	while ($chk < $num_sub){
		list($membershipNo,$exam_code,$subj_code,$exam_date,$exam_time,$centre_code)=mysql_fetch_row($res_sel_can);		
                $sql_medium = "select medium_code from iib_exam_candidate where membership_no='$memno[$i]' and exam_code='$exam_code' and subject_code='$subj_code'";
                $res_medium = mysql_query($sql_medium);
                list($medium_code)= mysql_fetch_row($res_medium);
		$sql_sel_sub="select subject_name from iib_exam_subjects where exam_code='$exam_code' and subject_code='$subj_code'";
		$res_sel_sub=mysql_query($sql_sel_sub);
		list($subj_name)=mysql_fetch_row($res_sel_sub);
		$tmpDate = $exam_date;
		$tmpTime = $exam_time;
		$tmpDate = explode('-',$tmpDate);
		$tmpTime =  explode(':',$tmpTime);
		$exam_date =  $tmpDate[2]."/".$tmpDate[1]."/".$tmpDate[0];
		$exam_time = $tmpTime[0].":".$tmpTime[1];
		
  		//echo "\r\n $i -> Row(array($subj_name,$aMedium[$medium_code],$exam_date,$exam_time,$centre_code),' | ','L')\r\n";
		if ($medium_code != "")
  			$txt->Row(array($subj_name,$aMedium[$medium_code],$exam_date,$exam_time,$centre_code),' | ','L');
		$iway_array[$chk]= $centre_code;
		$txt->put_line();
		$chk++;
	}
	$centre_i=0;

	$txt->PutLineBreak();
	$iway_array= array_values(array_unique($iway_array));
	$txt->putLineBreak();
	$txt->SetWidths(array(20,50));
	$txt->put_line();
	$txt->Row(array('Venue Code','Venue Address'),' | ','C');
	$txt->put_line();
	while($centre_code = $iway_array[$centre_i]){
	        $sql_iway_det = "select iway_name, iway_address1, iway_address2, iway_city, iway_state, iway_pin_code from iib_iway_details where centre_code='$centre_code'";
        	$res_iway_det = mysql_query($sql_iway_det);
	        list($iway_name,$iway_address1, $iway_address2, $iway_city, $iway_state,$iway_pin_code)=mysql_fetch_row($res_iway_det);
        	$sql_medium = "select medium_code from iib_exam_candidate where membership_no='$memno[$i]' and exam_code='$exam_code' and subject_code='$subj_code'";
	        $res_medium = mysql_query($sql_medium);
	        list($medium_code)= mysql_fetch_row($res_medium);
        	$address = "";
		if ($iway_name != "")
        		$address = $iway_name;
	        if (($address != "") && ($iway_address1 != ""))
        	        $address .= " ".$iway_address1;
	        if (($address != "") && ($iway_address2 != ""))
        	        $address .= " ".$iway_address2;
	        if (($address != "") && ($iway_city != ""))
        	        $address .= " ".$iway_city;
	        if (($address != "") && ($iway_state != ""))
        	        $address .= " ".$iway_state;
	        if (($address != "") && ($iway_pin_code != ""))
        	        $address .= " ".$iway_pin_code;
		$txt->Row(array($centre_code,$address),' | ','L');
		$centre_i++;
		$txt->put_line();
	}
	$i++;
//	$txt->SetWidths(array(75));
//	$txt->Row(array("Note: For Telephone Number of the Venue visit www.iibf.org.in"),'','');
//	$txt->PutLineBreak();
	$txt->SetWidths(array(75));
	$txt->Row(array("Note: Please read the instructions printed overleaf"),'','');
	$txt->PutLineBreak();
	$txt->SetWidths(array(30,5,40));
	$txt->Row(array("Password",":",$c_passwd),'','C');	
	$txt->SetWidths(array(75));
	$txt->Row(array(chr(12)),'','');
}	
       if(isset($HTTP_ENV_VARS['HTTP_USER_AGENT']) and strpos($HTTP_ENV_VARS['HTTP_USER_AGENT'],'MSIE 5.5'))
	        Header('Content-Type: application/dummy');
        else
       		 Header('Content-Type: application/dummy');
   $text_length = $txt->total_length(); 
    Header('Content-Length:'.$text_length+10000);
        Header('Content-disposition: attachment; filename='.$strFileName);
/*for($i=0;$i<100;$i++){
	echo "$i \r\n";
} */
$txt->show_values();
ob_end_flush(); 
?>
