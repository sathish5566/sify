<?PHP session_start();
set_time_limit(0);
$admin_flag=1;
require_once("sessionchk.php");
require_once("dbconfig.php");

if(isset($_POST['submitted']) && ($_POST['submitted']==2))
{	
	$exam_date=mysql_escape_string($_POST['seldate']);	
	
	$qry2="select membership_no,exam_code,subject_code from iib_candidate_iway where exam_date='".$exam_date."' and qp_assigned='N' and qp_generated ='N' limit 10000";
	if($res2=mysql_query($qry2))
	{	
		if( mysql_num_rows($res2) == 0 ){
			$msg="The candidates are not uploaded for selected exam date .";
		}else
		{								
				$assigned_now = 0;
				$assigned_already = 0;
				while($r2=mysql_fetch_object($res2))
				{			
					$mem_no = $r2->membership_no;
					$exam_code=	$r2->exam_code;
					$subject_code=$r2->subject_code;
					
					$selqry1="SELECT count(1) from iib_question_paper where membership_no='$mem_no' and exam_code='$exam_code' AND subject_code='$subject_code'";					
					$res = mysql_query($selqry1);
					list($assign)=mysql_fetch_row($res);
					if($assign > 0)
					{
						$assigned_already++;						
					}else
					{
						$upqry1="UPDATE iib_question_paper set membership_no='$mem_no',assigned='Y' where exam_code='$exam_code' AND subject_code='$subject_code' AND assigned='N' AND online='Y' LIMIT 1";
						mysql_query($upqry1) or die("update iib_question_paper failed ".mysql_error());
						$nAffectedRows = mysql_affected_rows($connect);
						if ($nAffectedRows == 0){
							$msg1="Question Papers are not available for other users";
							break 1;
						}else{
							$upqry2="UPDATE iib_candidate_iway set qp_assigned='Y' where membership_no='$mem_no' and exam_code='$exam_code' AND subject_code='$subject_code'";
							mysql_query($upqry2) or die("update iib_exam_candidate failed ".mysql_error());
							$assigned_now++;
						}	
					}
			   }
			   $msg=$assigned_now." Users QP mapped successfully";
		}
	}
	$qry="select count(1) from iib_question_paper where exam_code='$exam_code' AND subject_code='$subject_code' AND assigned='N' AND online='Y'";
	$res=mysql_query($qry);
	list($qpcnt)=mysql_fetch_row($res);
}
/*
echo "assigned:".$assigned_already;
echo "<br/>";
echo "unassigned:".$assigned_now;
echo $msg1;
*/
?>
<html>
<head>
<title>The Indian Institute of Banking &amp; Finance</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<script language="javascript">
function trim(value) {
   var temp = value;
   var obj = /^(\s*)([\W\w]*)(\b\s*$)/;
   if (obj.test(temp)) { temp = temp.replace(obj, '$2'); }
   var obj = / +/g;
   temp = temp.replace(obj, " ");
   if (temp == " ") { temp = ""; }
   return temp;
}

function validate()
{
	loginname=document.loginfrm.seldate.value;
	loginname=trim(loginname);
	if(loginname==''){
		alert('Please select exam date');
		document.loginfrm.seldate.focus();
		return;
	}
	document.loginfrm.submitted.value=2;
	document.loginfrm.submit();
}
function captureEnter()
{
	if (window.event.keyCode == 13)
	{
			validate();
	}
}
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="javascript:document.loginfrm.seldate.focus();" onKeyPress='javascript:captureEnter();'>
<center>
<table width="780" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="780"><img src="images/logo1.jpg" width="136" height="59"><img src="images/logo2.gif" width="284" height="59"><img src="images/logo3.gif" width="360" height="59"></td>
  </tr>
  <tr> 
    <td width="780" bgcolor="7292C5">&nbsp;</td>
  </tr>
  <tr> 
    <td width="780" background="images/tile.jpg" height="315" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr>
		<Td height="70" align="left" valign="top"  background=images/tile.jpg class="greybluetext10"><? include("admin_menu.php") ?></Td>	
	</tr>
              <tr> 
                <td> 
                  <form name='loginfrm' method="post">
                    <table width="76%" border="0" align="center" cellpadding="5" cellspacing="0">
                      <tr>
                        <td height="35" colspan="3" align="center" class="ColumnHeader"><strong class="ColumnHeader">Question Paper Mapping To Candidates (Bulk Mode -10000 users at a time) </strong></td>
                        </tr>
                      <tr>
                        
                        <td width="251" height="11" align="left" valign="top" bgcolor="#E8EFF7" class="greybluetext10">Exam Date:<span class="arial11orange"> *</span></td>
                        <td width="389" height="11" align="left" valign="top" bgcolor="#E8EFF7"><select name="seldate" class="textbox" id="seldate">
                          <option value="">Select Date</option>
                          <?PHP
					 	 $qry5="SELECT distinct exam_date from iib_exam_schedule";
						$rs5=mysql_query($qry5);
						while($r5=mysql_fetch_object($rs5))
						{
							if(isset($exam_date) && $exam_date!='' && $exam_date==$r5->exam_date)
								echo '<option value="'.$r5->exam_date.'" selected>'.$r5->exam_date.'</option>';
							else							
								echo '<option value="'.$r5->exam_date.'">'.$r5->exam_date.'</option>';							
														
						}
					 ?>
                        </select></td>
                      </tr>
                      <tr>
                        <td height="3" colspan="2" align="center" valign="top" bgcolor="#E8EFF7" class="greybluetext10"><input class='button' type='button' value="Map" name="go" onClick="javascript:validate();" />
                          <input type="hidden" name="submitted" /></td>
                        </tr>
                      <tr>
                        <td height="3" colspan="2" align="center" valign="top" bgcolor="#E8EFF7" class="arial11orange">
						<?PHP if(isset($msg)) echo $msg ?>						</td>
                        </tr>
                    <tr>
                        <td height="3" colspan="2" align="center" valign="top" bgcolor="#E8EFF7" class="arial11orange">
						<?PHP if(isset($msg1)) echo $msg1 ?>						</td>
                        </tr>
					  <tr>
                        
						 <?PHP if(isset($qpcnt)){ ?>
                        <td colspan="2" align="left" bgcolor="#D1E0EF"><span class="greybluetext10">Total no.of Unmapped Question Papers: <?PHP echo $qpcnt;?></span></td>
						 <?PHP } ?>   
                      </tr>
                                        
                      <tr>
                        <td colspan="2" align="left" bgcolor="#D1E0EF" height="3">
						<strong class="arial11orange">Note: The Unmapped Question papers count should be greater than or equal to unmapped candidates count.</strong></td>
                      </tr>
                    </table>
                  </form>                </td>
                </tr>
            </table></td>
          </tr>
      </table>
    </td>
  </tr>
  <tr>
  <td width="780" bgcolor="7292C5">&nbsp;</td>
  </tr>
</table>
</center>
</body>
</html>