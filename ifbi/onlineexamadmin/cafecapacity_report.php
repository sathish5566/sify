<?php

/****************************************************
* Application Name            :  IIB
* Module Name                 :  Cafecapacity report
* Revision Number             :  1
* Revision Date               :
* Table(s)                    : 
* Tables used for only selects: iib_exam_centres,iib_iway_details,iib_candidate_iway,iib_franchisee_map.
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              : CSV file and HTML page
* Document/Reference Material :
* Created By                  :  
* Created ON                  :  
* Last Modified By            :  K KARUNA MOORTHY RAJU
* Last Modified Date          :  01-04-2006
* Description                 : This Report displaying the Cafe capacity report for every cafe with all its related information.It can be downloaded too.
*****************************************************/

    ob_start(); 
	require_once "sessionchk.php";
	require_once("dbconfig_slave_123.php");
	
/* Include file for excel download */
	
    include_once("excelconfig.php");
	
      $sqlcentre = "SELECT exam_centre_code,upper(exam_centre_name) FROM iib_exam_centres where online='Y' ";
      $rescentre = mysql_query($sqlcentre);
      $examCentreCode = $_POST['exam_centre_code'];
      
/*  Array For for the excel header */
      
      $exce_header = array("S.No","City","Cafe Id","Cafe Address","Contact no. of TA","Contact no. of FM","Capacity");
      
      $submitted= $_POST['submitted'];
      
/*  Getting values when clicking download button */       

      $ex_flag=$_POST['exl'];
      
/* Filename of the downloaded excel file */
      
      $filename="Cafecapacity_report_Examcentre(".$examCentreCode.")";

/* select the centre details from table with submitted value.
   It only executing when examCentreCode is satisfy the conditions */             

       if ($examCentreCode!='')
        {
	    
			if ($examCentreCode!='all')
			{
			

	    	$sql_centrecode="select distinct a.centre_code from iib_iway_details a,iib_candidate_iway b  where a.exam_centre_code='".$examCentreCode."' and a.centre_code=b.centre_code order by centre_code ";
	
    		}
    		else if($examCentreCode =='all')
			{
		  
			$sql_centrecode="select distinct a.centre_code from iib_iway_details a,iib_candidate_iway b  where  a.centre_code=b.centre_code order by a.iway_city ";
	
    		}
	    $res_centrecode=mysql_query($sql_centrecode);
	    $centre_code='';

/* Retrieving the unique centre codes. It stores into an variable named - $centre_code    */
	    	    
			while(list($centrecode)=mysql_fetch_row($res_centrecode))
			{
				$centrecode = strtoupper($centrecode);
				if ($centre_code =='')
					$centre_code = "(centre_code='$centrecode' ";
				else
					$centre_code .= " or centre_code='$centrecode'";
			
			}
		$centre_code .= " ) ";
     }
/* Executing queries with exam centre code values with respect to $examCentreCode.    */   		
	if (($examCentreCode != "")  && $submitted=='true')
 	{
       	switch($examCentreCode)
        	{
            case "all":
                    $sqlFormat = "select distinct centre_code,iway_address1,iway_address2,actual_seats from iib_iway_details where $centre_code order by iway_city";
                break;
            default :
                    $sqlFormat = "select distinct centre_code,iway_address1,iway_address2,actual_seats from iib_iway_details  where $centre_code  order by iway_city ";
               break;
        	}
       	 	$resFormat = mysql_query($sqlFormat);
        	$ncount=mysql_num_rows($resFormat);
        
	$sql_city="select a.centre_code,b.exam_centre_name  from iib_iway_details a, iib_exam_centres b where a.exam_centre_code=b.exam_centre_code";
	$res_city=mysql_query($sql_city);
	
/* Storing the city names into an array named $cityArr[] */
	
		while(list($Centre_Code,$cityName)=mysql_fetch_row($res_city))
		{
			$Centre_Code = strtoupper($Centre_Code);
			$cityArr[$Centre_Code]=$cityName;
		}
     $sql_exam_centre_code="select centre_code,exam_centre_code from iib_iway_details";
     $res_exam_centre_code=mysql_query($sql_exam_centre_code);

/* Storing the city names into an array named $examArr[] */         

        while(list($centreCode,$examCentre_Code)=mysql_fetch_row($res_exam_centre_code))
        {
	       $centreCode = strtoupper($centreCode);
	       $examArr[$centreCode]=$examCentre_Code;
	    }
	    $sql_fmcontact="select  centre_code,phone from iib_franchisee_map";
	    $res_fmcontact=mysql_query($sql_fmcontact);
        
/*  /* Storing the Exam centre code and address into an array named $contactArr[]   */        

        while(list($centreCode,$contact)=mysql_fetch_row($res_fmcontact))
        {
	       $centreCode = strtoupper($centreCode); 
	       $contactArr[$centreCode]=$contact;
	    }
	    $i=0;
/* Storing the content such as centre code,iway address,TA contact number,Available seats,
   cafe id,City name,FM contact number into the array - $exce_content */
	    
         if($ncount>0)
          {
         while(list($centre_code,$iway_address,$ta_contact,$actual_seats)=mysql_fetch_row($resFormat))
            {
	         $centre_code = strtoupper($centre_code);   
	         if($iway_address!='')
	         {
		        $iway_address=str_replace('"',"'",$iway_address);
		       // $exce_content[$i][3]="\"$iway_address\"";
				$exce_content[$i][3]=substr("\"$iway_address\"",1,strlen("\"$iway_address\"")-2);
		     }
	         else
	            $exce_content[$i][3]="---";
	            
	         if($ta_contact!='')
	         {
		        $ta_contact=str_replace('"',"'",$ta_contact);
		     //   $exce_content[$i][4]="\"$ta_contact\"";
				$exce_content[$i][4]=substr("\"$ta_contact\"",1,strlen("\"$ta_contact\"")-2);
		     }
	         else
	            $exce_content[$i][4]="---";   
		     if($contactArr[$centre_code]!='')
	         {
		        $contactArr[$centre_code]=str_replace('"',"'",$contactArr[$centre_code]);
		        //$exce_content[$i][5]="\"$contactArr[$centre_code]\"";
				$exce_content[$i][5]=substr("\"$contactArr[$centre_code]\"",1,strlen("\"$contactArr[$centre_code]\"")-2);
		     }
	         else
	            $exce_content[$i][5]="---";  
	         $exce_content[$i][0]=$i+1;   
	         $exce_content[$i][1]=$cityArr[$centre_code];
	         $exce_content[$i][2]=$centre_code;
	         $exce_content[$i][6]=$actual_seats;
	       $i++;        
            }
          } 
      
/* Execution of excel download function */
  if($ex_flag=='1')
        {
	    excelconfig_xls($filename,$exce_header,$exce_content);
		exit;	  
       }
   }
?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='JavaScript' src="./includes/validations.js"></script>
<script language='JavaScript'>
function submitForm()
{
	/*if (document.frmalloc.exam_code.selectedIndex == 0)
	{
		alert("Please select the exam ");
		document.frmalloc.exam_code.focus();
		return false;
	}*/
	if (document.frmalloc.exam_centre_code.selectedIndex == 0)
	{
		alert("Please select the exam centre");
		document.frmalloc.exam_centre_code.focus();
		return false;
	}
	/*if (document.frmalloc.exam_date.selectedIndex == 0)
	{
		alert("Please select the exam date");
		document.frmalloc.exam_date.focus();
		return false;
	}*/
    document.frmalloc.submitted.value='true';
    document.frmalloc.exl.value='0';
	document.frmalloc.submit();
}
/ * Function for downloading excel file  */

function excelsubmit()
{
	
	var frmname=document.frmalloc;
	frmname.exl.value='1';
    frmname.submitted.value='true'; 	
	frmname.submit();
}

</script>

</HEAD>
<BODY leftMargin=0 topMargin=0 LeftMargin=0 marginheight=0 marginwidth=0 >
<center>
<form name='frmalloc' method='post'>
<input type='hidden' name='populatecenter'>
<input type='hidden' name='submitted'>
<input type='hidden' name='exl' value='0'>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780 background=images/tile.jpg>
<!--<TABLE border=0 cellPadding=0 cellSpacing=0 width=780 height=500>-->
	<!--<TR><!-- Topnav Image -->
      <!--  <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR>  -->
	<tr><td width="780"><?include("includes/header.php");?></td></tr>
    <TR>
       
    </TR>
	<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>
	</tr>
    <TR>
    	<TD background=images/tile.jpg vAlign=top width=780 align=center>

        <table width=780 cellpadding=0 cellspacing=5 border=0 >
        	<tr>
				<td colspan=2 align="center"><b><font face="Verdana, Arial, Helvetica, sans-serif" size=2>Cafe-Capacity Report</b></font><br><br></td>
            </tr>
            <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Exam Centre : </td>
            	<td align="left" valign=top class="greybluetext10">
            		<select name='exam_centre_code' class="greybluetext10" style={width:150px} <!--onchange='javascript:selcentre();'-->>
            	 		<option value='0'>--Select--</option>
        			  
            			<?
        			         		     	    
        		     	if ($examCentreCode == "all")
	            				{
		            				$selectedAll = "selected";
	            				}
                 	 	?>
              			<option value='all' <?=$selectedAll?>>--All--</option>            			<?
                           while (list($code,$name) = mysql_fetch_row($rescentre))
            				{
	            				print("<option value='$code'");
	            				if ($examCentreCode == $code)
	            				  	print(" selected ");
	            			  	print(">$name</option>");
	               			} ?>
            	 </select>
             </td>
         </tr>
    </table>
    <table width=780 >
       <tr>
	    	<td colspan=2 align="center"><input type=button name='sub_assign' value='Submit' class='button' onClick='javascript:submitForm()'></td>
       </tr>
        <?if(($submitted=='true') && ($ncount <1 )){?>
        <tr>
		<td width=780 align=center class=alertmsg>No Records to Display</td>
        </tr><?}?>
    </table>
    <?if(($ncount >= 1) && ($submitted=='true')) 
    {      
    
	    $header_count = count($exce_header);
	    // Printing the values in the HTML page 
			
			// Printing the header values from excel header array 
		print("<table border=1 cellspacing=0 cellpadding=2><tr>");
		for($a=0;$a<$header_count;$a++)
		{
			print("<td class=greybluetext10><b>$exce_header[$a]</b></td>");
		}
		print("</tr>");
		
		// Loop to display the values in the HTML page
		print("<tr>");
		
	     for($j=0;$j<$ncount;$j++)
             {
	          for($k=0;$k<$header_count;$k++)
	           {
		         				if($k==3 || $k==4 || $k==5) 
					  //	$result=substr($exce_content[$j][$k],1,strlen($exce_content[$j][$k])-2);
					    $result=$exce_content[$j][$k];
					  else
					    $result=$exce_content[$j][$k];
               ?><td  class="greybluetext10"><?=$result?>&nbsp;</td>
		          <?
	           }
	   print("</tr>");
            }      ?>
     </table>    
    <?
/* It checks the availability of records */    
   	 ?>    	<tr><td class=greybluetext10>&nbsp;</td></tr> 
    	  	<tr><td class=greybluetext10 align="center" class=greybluetext10><input class='button' type='button' name='download' value='Download' onClick='javascript:excelsubmit()'></td></tr>
          	<tr><td class=greybluetext10>&nbsp;</td></tr> 
    <?  }  ?>
    	</td>
	</tr>	
	<TR>
    	<?include("includes/footer.php");?>
    </TR>
</TABLE>
</form>
</center>
</BODY>
</HTML>
