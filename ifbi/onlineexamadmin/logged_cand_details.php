<?php
@session_start();
$admin_flag=0;
require("dbconfig_slave_123.php");
require("sessionchk.php");
set_time_limit(0);
extract($_GET);
if(isset($_GET['exam_code']) && isset($_GET['sub_code']) && isset($_GET['exam_date']) && isset($_GET['exam_time']) && isset($_GET['rpt_type']))
{
	$rpttype=mysql_escape_string($_GET['rpt_type']);
	$intExCode = $_GET['exam_code'];
	$intSubCode = $_GET['sub_code'];
	$examDate = $_GET['exam_date'];
	$tExamTime = $_GET['exam_time'];
	$tPostGraceTime = $_GET['post_grace_time'];
	$aDates = explode("-", $examDate);
	$dispDate = $aDates[2]."-".$aDates[1]."-".$aDates[0];

	//Get the candidate ids logged in before the exam time
	$arrCandIDs = array();

	$sqlGetCandIDs = "SELECT DISTINCT a.membership_no FROM iib_candidate_tracking a WHERE date_format(a.updated_time,'%Y-%m-%d')='$examDate' AND date_format(a.updated_time,'%T') < '$tExamTime' AND a.exam_code='$intExCode' AND a.subject_code='$intSubCode'";
	$resGetCandIDs = mysql_query($sqlGetCandIDs);
	while($rowCandIDs = mysql_fetch_array($resGetCandIDs))
	{
		$arrCandIDs[] = $rowCandIDs['membership_no'];
	}
	$arrCandIDs = implode(",",$arrCandIDs);

	//Get the candidate ids logged in  between start of test and grace time
	$arrCandIDs1 = array();

	$sqlGetCandIDs1 = "SELECT DISTINCT a.membership_no FROM iib_candidate_tracking a WHERE date_format(a.updated_time,'%Y-%m-%d')='$examDate' AND date_format(a.updated_time,'%T') >= '$tExamTime' AND date_format(a.updated_time,'%T') <= (SELECT SEC_TO_TIME( TIME_TO_SEC( '$tExamTime' ) + TIME_TO_SEC( '$tPostGraceTime' ) )) AND a.exam_code='$intExCode' AND a.subject_code='$intSubCode' ";
	
	if(is_array($arrCandIDs) && count($arrCandIDs) > 0)
				$sqlGetCandIDs1 .= " AND a.membership_no NOT IN ($arrCandIDs)";
	
	$resGetCandIDs1 = mysql_query($sqlGetCandIDs1);
	while($rowCandIDs1 = mysql_fetch_array($resGetCandIDs1))
	{
		$arrCandIDs1[] = $rowCandIDs1['membership_no'];
	}
	$arrCandIDs1 = implode(",",$arrCandIDs1);


	//Get the candidate ids logged in  after grace time
	$arrCandIDs2 = array();

	$sqlGetCandIDs2 = "SELECT DISTINCT a.membership_no FROM iib_candidate_tracking a WHERE date_format(a.updated_time,'%Y-%m-%d')='$examDate' AND date_format(a.updated_time,'%T') > (SELECT SEC_TO_TIME( TIME_TO_SEC( '$tExamTime' ) + TIME_TO_SEC( '$tPostGraceTime' ) )) AND a.exam_code='$intExCode' AND a.subject_code='$intSubCode' ";
	if(is_array($arrCandIDs) && is_array($arrCandIDs1) && count($arrCandIDs) > 0 && count($arrCandIDs1) > 0)
		$sqlGetCandCTAfterGraceTime .= "  AND a.membership_no NOT IN($arrCandIDs,$arrCandIDs1)";

	
	
	$resGetCandIDs2 = mysql_query($sqlGetCandIDs2);
	while($rowCandIDs2 = mysql_fetch_array($resGetCandIDs2))
	{
		$arrCandIDs2[] = $rowCandIDs2['membership_no'];
	}
	$arrCandIDs2 = implode(",",$arrCandIDs2);

	if($rpttype == 'before'){
		$title="Candidates Logged In Before Start Of Exam Time";
		$sql = " SELECT a.membership_no,a.centre_code,a.exam_date,a.exam_time FROM iib_candidate_iway a WHERE a.membership_no IN ($arrCandIDs) ";
	}else if($rpttype == 'beforegrace'){
		$title="Candidates Logged In After Exam Time AND Before Grace Time";
		$sql = "  SELECT a.membership_no,a.centre_code,a.exam_date,a.exam_time FROM iib_candidate_iway a WHERE a.membership_no IN ($arrCandIDs1)  ";
	}else if($rpttype == 'aftergrace'){
		$title = "Candidates Logged In After Exam Time AND Before Grace Time";	
		$sql = "  SELECT a.membership_no,a.centre_code,a.exam_date,a.exam_time FROM iib_candidate_iway a WHERE a.membership_no IN ($arrCandIDs2)  ";
	}					
}
$res = mysql_query($sql);
$ct= mysql_num_rows($res);

			
?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>
BODY {
	FONT-FAMILY: Arial, Verdana	
}
.textblk11  { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; text-decoration: none}
</STYLE>
<script language="javascript" type="text/javascript">
function win_open(a,b)
{
	window.open('tadetails.php?id='+a+'&cid='+b,'win1','left=10,top=10,width=600,height=700,toolbar=no,menubar=no,addressbar=no,resizable=1');		
}
</script>
</head>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
	<tr><td width="780"><?php include("includes/header.php");?></td></tr>
	<TR>    	
	</TR>
	<tr>
	<Td class="greybluetext10"  background=images/tile.jpg><? //include("admin_menu.php") ?></Td>	
	</tr>	
	<TR>
	    <TD background=images/tile.jpg vAlign=top width=780 align=center>
	    <form name='frmtimchange' id="frmtimchange" method='post'>
<table width=100% border=0 cellspacing=0 cellpadding=5>
	<tr><td width="100%" align=center><div align=center><strong>Logged Candidates Report </strong></div></td></tr>
	<tr>
	  <td align=center valign="top" class="arial14a" ><div align="left" class="arial11a"><strong>Report Description : <?PHP echo ucwords($title)?> </strong></div></td>
	  </tr>
	<tr>
	  <td align=center valign="top" class="arial14a" ><div align="left" class="arial11a"><strong>Exam Date : <?PHP echo $dispDate?> </strong></div></td>
	  </tr>
	<tr>
	  <td align=center valign="top" class="arial14a">
	  <div align="left" class="arial11a"><strong>Exam Time : <?PHP echo $tExamTime?> </strong></div>
		 </td>
	  </tr>
	<tr>
	  <td align=center valign="top">&nbsp;</td>
	  </tr>
	<?PHP 
	if($ct > 0)
	{		
		echo "<tr><td colspan=7 align=center class=alertmsg><div id='idafrows'>No.Of Records:$ct</div></td></tr>";
	?>	
	<tr>
	  <td align=center>
	  <div id="content" style="visibility:visible;display:block">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="24" align="left" valign="top" bgcolor="#000066">
		  <table width="100%" border="1" align="left" cellpadding="5" cellspacing="0" bordercolor="#000066" bgcolor="#FFFFFF" style="border-left-width:thin">
            <tr>
              <td width="6%" bgcolor="#0080FF" class="whitecolor"><div align="center"><strong>S.No</strong></div></td>
              <td width="22%" bgcolor="#0080FF" class="whitecolor"><div align="center"><strong>Membership No</strong></div></td>
              <td width="25%" bgcolor="#0080FF" class="whitecolor"><div align="center"><strong>Centre Code </strong></div></td>
              <td width="24%" bgcolor="#0080FF" class="whitecolor"><div align="center"><strong>Logged In Time</strong></div></td>
            </tr>
            <?PHP	$sno=1;														
					while($rs = mysql_fetch_array($res)){            		         
					   $memno = $rs['membership_no'];
						$ccode = $rs['centre_code'];
						$examdate = $rs['exam_date'];
						$aDates = explode("-", $examdate);
						$dispDate = $aDates[2]."-".$aDates[1]."-".$aDates[0];
						$examtime = $rs['exam_time'];
						
						//Get the candidate logged in time
						$sqlGetCandIDLoggedTime = "SELECT date_format(a.updated_time,'%T') as loggedintime FROM iib_candidate_tracking a WHERE date_format(a.updated_time,'%Y-%m-%d')='$examDate' AND a.membership_no='".$memno."' order by a.updated_time LIMIT 1";
						$resGetCandIDLoggedTime = mysql_query($sqlGetCandIDLoggedTime);
						list($rowGetCandIDLoggedTime) = mysql_fetch_row($resGetCandIDLoggedTime);
						


						$iw_iway="iwfr_".$ccode;										
						echo '<tr>';							 
						echo "<td class='greybluetext10'>".$sno++."</td>
							  <td class='greybluetext10'><a href='candidateentries.php?cname=$memno' target='_blank'>$memno</a></td>
							  <td class='greybluetext10'><a href=javascript:win_open('$iw_iway','$ccode')>$ccode</td>
							  <td class='greybluetext10'>$rowGetCandIDLoggedTime</td>
						</tr>";
		  			}
			 
			  ?>
		  </table></td>
        </tr>
      </table>
	  </div>	  </td>
	  </tr>
	<?PHP }
?>
</table>
</form>
</TD>
</TR>
<TR>
<?php include("includes/footer.php");?>
</TR>
</TABLE>
</center>
</BODY>
</HTML>
