<?php
require_once "sessionchk.php";

/*
*		@author		: balachandar m
*		@project	:	iiblive
*		@purpose	:	applying the various mapping ratio inputted by the user!
*/

require_once ("dbconfig.php");

/*
*		enable this as true for debugging purpose
*		usage: if($commonDebug){}
*/

$commonDebug=false;

/*
*		setting defining the constant values
*/

$updatedtime="UNIX_TIMESTAMP()";
$zero = "0";

/*
*		fetching the city
*/
$sqlcentre = "SELECT exam_centre_code,upper(exam_centre_name) FROM iib_exam_centres where online='Y' ";
$rescentre = @mysql_query($sqlcentre);
		

if($_POST){

	/*
	*		I should calculate the percentage here. This would come 
	*		from the dropdown box as post variables
	*/

	$ratio = $_POST['oldratio'];
	$newratio = $_POST['newratio'];
	$exam_centre_code = $_POST['exam_centre_code'];

	/*
	*		fetching the iway vacancy details from the respective table	
	*		checking with 5 records
	*/

	$sql_iway_vacancy = "select a.* from iib_iway_vacancy a,iib_iway_details b where a.centre_code=b.centre_code and exam_centre_code = ".$exam_centre_code;
//	echo $sql_iway_vacancy."<br>";

	$res_iway_vacancy = @mysql_query($sql_iway_vacancy);

	while(list($centre_code,$exam_date,$exam_time,$no_of_seats,$filled,$remaining) = mysql_fetch_row($res_iway_vacancy)){


		if ($commonDebug){
				echo " <br>Centre Code ".$centre_code
						." <br> Exam Date ".$exam_date
						." <br> Exam Time ".$exam_time
						." <br> No of Seats ".$no_of_seats
						." <br> Filled ".$filled
						." <br> Remaining ".$remaining."<br>";
		}

		/*
		*		if remaining is zero, that means cafe is fully 
		*		allocated. then contine with next entry.
		*/
		
		if($remaining == $zero){
			continue;
		}
	/*
		*		I am getting the actual seat of the centre code	
		*		i dont think it will be useful here. but let me have that for any reference 	
		*/
		
		$sql_iib_iway_details = "select actual_seats from iib_iway_details where centre_code='$centre_code'";
		$res_iib_iway_details = @mysql_query($sql_iib_iway_details);
		list($total) = @mysql_fetch_row($res_iib_iway_details);

		/*
		*		Calculating the filled percentage by taking the remaining seats
		*		rounding it to 2 decimal value
		*/

		$actual_filled = $filled*$ratio;
		$actual_remaining = $total - $actual_filled;
		
		$remaining_blown_up = floor($actual_remaining/$newratio);
		
		if ($commonDebug){
			echo "Total ".$actual_filled."<br>";
			echo "Remaining ".$actual_remaining."<br>";
			echo "My Remaining - total ".$myremaining."<br>";
			echo "Remaining Blown Up ".$remaining_blown_up."<br>";
		}

		
		/*
		*		Let me insert the values into the mapping tracking table
		*		for reverting if anything goes wrong
		*/

		$mapping_tracking = "'$centre_code','$exam_date','$exam_time',$no_of_seats,$filled,$remaining,$actual_filled,$actual_remaining,$ratio";

		$mapping_columns="centre_code,exam_date,exam_time,no_of_seats,filled,remaining,actual_filled,actual_remaining,ratio";
		
		$sql_mapping_tracking = "insert into iib_iway_mapping_tracking ($mapping_columns) values($mapping_tracking)";
		//echo $sql_mapping_tracking."<br>";
		@mysql_query($sql_mapping_tracking);

		/*
		*	Iway Vacancy Update table
		*/
		$sql_iway_vacancy= "update iib_iway_vacancy set no_of_seats=$remaining_blown_up,filled=$zero,remaining=$remaining_blown_up where exam_date='$exam_date' and exam_time='$exam_time' and centre_code='$centre_code'";
	//	echo $sql_iway_vacancy."<br>";
		@mysql_query($sql_iway_vacancy);
		
	}

} //$_POST Ends here
?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='JavaScript' src="./includes/validations.js"></script>
<script language='javascript'>
function submitForm()
{
	var frm = document.frmcafe;
	 
	if (frm.oldratio.value == 0){
		alert("Please select the Old Ratio");
		document.frmcafe.oldratio.focus();
		return false;
	}
	if (frm.newratio.value == 0){
		alert("Please select the New Ratio");
		document.frmcafe.newratio.focus();
		return false;
	} 
	 
/*	if (frm.newratio.value <= frm.oldratio.value){
		alert("Always New Ratio should be greater than or Equal to Old Ratio!");
		return false
	}*/
	if(confirm("Are you sure want to update?")){
		document.frmcafe.submitted.value='true';
		document.frmcafe.submit();
	}
	
}
	
    
function checktext(comp){ 	//Regular expression for checking Chking contents of textarea
	myRegExp = new RegExp("^[0-9a-zA-Z]+");
	txt = comp.value;
	result=txt.match(myRegExp);
	if(!result){
		alert('Sorry, Only alphabets and numbers are allowed!');
		comp.focus();
		return false;
	}
return true;
}
</script>
</HEAD>
<BODY leftMargin=0 topMargin=0 LeftMargin=0 marginheight=0 marginwidth=0 >
<center>
<form name='frmcafe' method='post'>
<input type='hidden' name='submitted'>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780 >
<!--	<TR><!-- Topnav Image -->
<!--        <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR> -->
	<tr><td width="780"><?include("includes/header.php");?></td></tr>
    <TR>
        
    </TR>
	<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>
    <TR>
    	<TD background=images/tile.jpg vAlign=top width=780 align=center height=300>

        <table width=780 cellpadding=0 cellspacing=5 border=0  valign="top">
        	<tr>
				<td  align="center"><b><font face="Verdana, Arial, Helvetica, sans-serif" size=2>Mapping Ratio Generator</b></font><br><br></td>
            </tr>
						 <tr>
               <td  valign=top class="greybluetext10" align="center">Exam Centre :
                 <select name='exam_centre_code' class="greybluetext10" style="{width:150px}">
                   <option value=''>--Select--</option>
							<?																																																																																																																					                     while (list($code,$name) = mysql_fetch_row($rescentre)){																																																																																									                       print("<option value='$code' ");
																																																																																																																																																					                       if ($examCentreCode == $code)
																																																																																																																																																																                       {
																																																																																																																																																																											                         print(" selected ");
																																																																																																																																																																																							                       }


																																																																																																																																																																																																		                       print(">$name</option>");
																																																																																																																																																																																																													                     }
																																																																																																																																																																																																																							                   ?>
																																																																																																																																																																																																																																                 </select>
																																																																																																																																																																																																																																								               </td>
																																																																																																																																																																																																																																															             </tr>

            <tr>
            	<td  align="center" valign="middle" class="greybluetext10" valign=top>Please Select the Ratio to be Generated: <br><br>
				 <select name="oldratio" class="greybluetext10">
 					<option value="0">Select your Old Ratio</option>
					<option value="0.4">40%</option>
					<option value="0.6">60%</option>
					<option value="0.75">75%</option>
					
				 </select>
				<select name="newratio" class="greybluetext10">
 					<option value="0">Select your New Ratio</option>
					<option value="0.4">40%</option>
					<option value="0.6">60%</option>
					<option value="0.75">75%</option>
				 </select>
							</td>
            </tr>
		<tr>
		<td align="center"><input type=button name='sub_cafe' value='Submit' class='button' onClick='javascript:submitForm()'>
			</td>
            </tr>           
            <TR>
    			<?include("includes/footer.php");?>
    		</TR>
</table>
</form>
</center>
</body>
</html>
