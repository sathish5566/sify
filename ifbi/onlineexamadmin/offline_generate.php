<?php
/****************************************************
* Application Name            :  IIB
* Module Name                 :  Offline Generate Page
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  iib_subject_sections,iib_question_paper,iib_qp_weightage
* Tables used for only selects:  
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  
* Created ON                  :  
* Last Modified By            :  B.Devi
* Last Modified Date          :  27-09-2006
* Description                 :  Called in QP Generation
*****************************************************/
//	require_once("dbconfig.php");
	set_time_limit(0);
	
	//usage : offline_generate.php exam_code subject_code no_of_papers total_marks section_count marks_string is_sample	
	$commonDebug = false;	
	// General Questions
	
	$aMarks = array();
	if ($strMarks != "")
	{
		$aMarks = explode("-", $strMarks);
	}
	
 	$nMarks = count($aMarks);
	$aMarksCount = array();
	for ($i = 0; $i < $nMarks; $i++)
	{
		$aCount = array();
		$aCount = explode(":", $aMarks[$i]);
		$aMarksCount[$aCount[0]] = $aCount[1];
	}
	
	$ncMarks = count($acMarks);
	
	$arrOptionShuffle=array('none of these','none of the above','all of the above','noneofthese','noneoftheabove','alloftheabove');
	
	$sql1 = "SELECT answer_shuffling  FROM iib_exam_subjects WHERE subject_code='$subjectCode' and online='Y'";
	if ($commonDebug)
	{
		print "query :".$sql1;
	}
	$result1 = @mysql_query($sql1) or die("Query failed");
	$nRows1 = mysql_num_rows($result1);
	if ($nRows1 == 0)
	{
		print("<br>Subject not avilable<br>");
		return;
	}	
	list($answershuffling) = mysql_fetch_row($result1);
	
	$sql = "SELECT section_code, section_type FROM iib_subject_sections WHERE subject_code='$subjectCode' and online='Y' ORDER BY section_code";
	if ($commonDebug)
	{
		print "query :".$sql;
	}
	$result = @mysql_query($sql) or die("Query failed");
	$nRows = mysql_num_rows($result);
	if ($nRows == 0)
	{
		print("<br>This subject has no sections<br>");
		return;
	}

	$sqlOptions = "SELECT option_1, option_2, option_3, option_4, option_5,question_id FROM iib_section_questions WHERE ".
				" exam_code='$examCode' AND subject_code='$subjectCode'";
	$resOptions = @mysql_query($sqlOptions) or die("cannot execute query");

	while($options = mysql_fetch_row($resOptions))	{
		$input = array();
		$rand_keys = array();									
		for ($optCnt = 1; $optCnt <= 5; $optCnt++)
		{	
			$options[$optCnt-1];
			if ($options[$optCnt-1] != "")
			{							
				$input[$optCnt] = $optCnt;								
				if(in_array(strtolower($options[$optCnt-1]),$arrOptionShuffle))
					$input[$optCnt];
			}																	
		}		
		$rand_keys = array_rand($input, count($input));
		$arrRAND[$options[5]]=$rand_keys;
	}




	// General Questions
	$aSectionCode = array();
	$aSectionType = array();
	
	// Case Questions
	$acSectionCode = array();
	$acSectionType = array();
	
	while (list($sectionCode,$sectionType) = mysql_fetch_row($result))
	{
		if($sectionType == 'G' ){
			$aSectionCode[] = $sectionCode;
			$aSectionType[] = $sectionType;
		}
		elseif($sectionType == 'C' ) {
			$acSectionCode[] = $sectionCode;
			$acSectionType[] = $sectionType;
		}
	}
		
	for ($nCount = 0; $nCount < $nPapers; $nCount++)
	{
		$sqlQuestions = "SELECT question_paper_no FROM  iib_question_paper WHERE complete='N' ".
		" AND exam_code='$examCode' AND subject_code='$subjectCode' ";
		if ($commonDebug)
		{
			print "query :".$sqlQuestions;
		}
		$resQuestions = @mysql_query($sqlQuestions) or die("select on iib_question_paper failed");
		$nIncomplete = mysql_num_rows($resQuestions);
		if ($nIncomplete > 0)
		{
			$strIncomplete = "";
			while (list($incompleteQns) = mysql_fetch_row($resQuestions))
			{
				($strIncomplete == "") ? $strIncomplete = $incompleteQns : $strIncomplete .= ", ".$incompleteQns;
			}
			$sqlDelete = "DELETE FROM iib_question_paper WHERE question_paper_no IN ($strIncomplete)";
			if ($commonDebug)
			{
				print "query :".$sqlDelete;
			}
			@mysql_query($sqlDelete) or die("Delete on iib_question_paper failed");
			$sqlDelete = "DELETE FROM iib_qp_weightage WHERE question_paper_no IN ($strIncomplete)";
			if ($commonDebug)
			{
				print "query :".$sqlDelete;
			}
			@mysql_query($sqlDelete) or die("Delete on iib_qp_weightage failed");			
			$sqlDelete = "DELETE FROM iib_question_paper_details WHERE question_paper_no IN ($strIncomplete)";
			if ($commonDebug)
			{
				print "query :".$sqlDelete;
			}
			@mysql_query($sqlDelete) or die("Delete on iib_question_paper_details failed");			
		}
		$sqlInsert = "INSERT INTO iib_question_paper ". "(exam_code,subject_code,total_marks,sample,enabled,online,assigned) ".
		"VALUES ('$examCode','$subjectCode','$totalMarks','$isSample','Y','Y','N')";
		if ($commonDebug)
		{
			print "query :".$sqlInsert;
		}
		@mysql_query($sqlInsert) or die("Insert on iib_question_paper failed");
		$questionPaperNo = mysql_insert_id();
		
		// General Questions
		$aSectionWiseCount = array();
		for ($j = 0; $j < $nStoredMarks; $j++)
		{
			$strMarks = $aMarksCount[$j];
			$aSectionMarks = array();
			$aSectionMarks = explode(",", $strMarks);
			for ($i = 0; $i < $sectionCount; $i++)
			{
				$aSectionWiseCount[$i][$j] = $aSectionMarks[$i];
			}
		}
		//radomization
		$aAllQuestions = array();		
		// General Questions
		$aQuestions = array();
		$cnt = 0;
		$sqlInsert1 = "";
		for ($j = 0; $j < $nStoredMarks; $j++)
		{
			$sqlInsert1 = "";
			for ($i = 0; $i < $sectionCount; $i++)
			{
				$sectionCode = $aSectionCode[$i];
				$sectionMarks = $aStoredMarks[$j];
				$questionCount = $aSectionWiseCount[$i][$j];
				if ($sqlInsert1=="")
					$sqlInsert1 = "INSERT INTO iib_qp_weightage (question_paper_no, exam_code, subject_code, section_code, marks, no_of_questions) VALUES ('$questionPaperNo', '$examCode', '$subjectCode', '$sectionCode', '$sectionMarks', '$questionCount')";
				else
					$sqlInsert1.= ", ('$questionPaperNo', '$examCode', '$subjectCode', '$sectionCode', '$sectionMarks', '$questionCount')";
				
				if ($questionCount > 0)
				{
					$sqlQuestions = "SELECT question_id, question_code FROM iib_section_questions WHERE ".
					" exam_code='$examCode' AND subject_code='$subjectCode' AND section_code='$sectionCode' ".
					" AND marks='$sectionMarks' ORDER BY rand() LIMIT $questionCount";
					if ($commonDebug)
					{
						print("\n".$sqlQuestions);
					}
				
					$result = @mysql_query($sqlQuestions) or die("select from iib_section_questions failed");
					$nRows = mysql_num_rows($result);
					
					if ($nRows > 0)
					{
						while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
						{
							$questionID = $row['question_id'];
							$questionCode = $row['question_code'];
							$aQuestions[$questionID][0] = $questionID;
							$aQuestions[$questionID][1] = $sectionCode;
							$aQuestions[$questionID][2] = $sectionMarks;
							$aQuestions[$questionID][3] = 0;
							
							$aAllQuestions[$cnt][0] = $questionID;
							$aAllQuestions[$cnt][1] = $questionID;
							$aAllQuestions[$cnt][2] = 0;
							
							$cnt++;
							
							
						}
					}
				} // end of if question count > 0
			} // end of for section count
			if ($commonDebug)
			{
				print("\n".$sqlInsert1);
			}
			@mysql_query($sqlInsert1) or die("Insert on iib_qp_weightage failed");
			$sqlInsert1='';

		} // end of for marks		
			
		if ($commonDebug)
		{
			print ("<br>questions :");
			print_r($aQuestions);
		}		
		$nQuestions = count($aQuestions);
		$nQs = count($aAllQuestions);
		srand ((float) microtime() * 10000000);
		$aRandQuestions = array_rand($aAllQuestions, $nQs);			
		if ($commonDebug)
		{
			print ("<br>questions 2 :");
			print_r($aRandQuestions);
		}
		
		//$totalQuestions = $nQuestions + $nCaseQuestions;
		$iCount = 1;
		$sqlInsert2='';
		for ($qCount = 0; $qCount < $nQs; $qCount++)
		{
			$qCount+1;
			
			if(trim($_POST['secshuffle']) == 1)
			  $index = $qCount;
			else			
			  $index = $aRandQuestions[$qCount];		    
			$actualQuestions = array();
		 	$strQuestionIDs = $aAllQuestions[$index][1];
			$actualQuestions = explode(",",$strQuestionIDs);
			if(is_array($actualQuestions))
			{
				$actualCnt = count($actualQuestions);
				srand ((float) microtime() * 10000000);
				$actualRandQuestions = array_rand($actualQuestions, $actualCnt);
			}
			else
				$actualCnt = 1;
			for($cntIDs=0;$cntIDs < $actualCnt;$cntIDs++){
				if (is_array($actualRandQuestions)){
					$caseIndex = $actualRandQuestions[$cntIDs];
				}else{
					$caseIndex = $actualRandQuestions;
				}
				
				if(is_array($actualQuestions))
					$questionID = $actualQuestions[$caseIndex];
				else
					$questionID = $actualQuestions;
					
				if($questionID != '') {
					$sectionCode = $aQuestions[$questionID][1];
					$sectionMarks = $aQuestions[$questionID][2];
					$caseID = $aQuestions[$questionID][3];
					$strOptOrder = "";
										
					$rand_keys = $arrRAND[$questionID];
					if ($commonDebug)
					{
						print("<br>rand keys: ");
						print_r($input);
						print_r($rand_keys);
						print("<br>");
					}
					if($answershuffling == 'Y'){
						shuffle($rand_keys);
					}					
					$strOptOrder = implode($rand_keys,",");
										
					if($sqlInsert2 == '')
					{
						 $sqlInsert2 = "INSERT INTO iib_question_paper_details (question_paper_no, subject_code, section_code, question_id, answer_order, display_order,case_id) VALUES ('$questionPaperNo', '$subjectCode', '$sectionCode', '$questionID', '$strOptOrder', $iCount,$caseID)";	
					}
					else
					{
						$sqlInsert2.=",('$questionPaperNo', '$subjectCode', '$sectionCode', '$questionID', '$strOptOrder', $iCount,$caseID)";
					}					
					$iCount++;
				}
			}
		
		} // end of for qCount
		if ($commonDebug)
		{
			print("\n".$sqlInsert2);
		}					
		@mysql_query($sqlInsert2) or die("Insert on iib_question_paper_details failed");

		
		//CASE STUDY				
		$arrcaseID=array();
		if(isset($_POST["caseid"])&& is_array($_POST['caseid'])){
			$listCaseID=implode(',',$_POST['caseid']);
			$arrcaseID= explode(',',$listCaseID);		
			$arrCqid=array();
			$arrCase=array();
			$qrycmark1="SELECT question_id,case_id from iib_section_questions where exam_code='$examCode' and subject_code='$subjectCode' and case_id IN($listCaseID)";
			$res1=mysql_query($qrycmark1);
			while($r1=mysql_fetch_object($res1)){
				if($arrCqid[$r1->case_id] != '')
					$arrCqid[$r1->case_id] = $arrCqid[$r1->case_id].','.$r1->question_id;
				else
					$arrCqid[$r1->case_id] = $r1->question_id;
			}
			mysql_free_result($res1);
			$qrycmark="SELECT case_id,sum(marks) as totmarks ,count(1) as qpcount,section_code  from iib_section_questions where exam_code='$examCode' and subject_code='$subjectCode' and case_id IN($listCaseID) group by case_id";
			$res=mysql_query($qrycmark);
			while($r=mysql_fetch_object($res)){
				$arrCase[$r->case_id]['marks']=$r->totmarks;
				$arrCase[$r->case_id]['questionscount']=$r->qpcount;
				$arrCase[$r->case_id]['sectioncode']=$r->section_code;
			}		
			mysql_free_result($res);
			$csqlInsert2='';
			shuffle($arrcaseID);
			for($i=0;$i < count($arrcaseID);$i++){
				$caseid=$arrcaseID[$i];
				$csectionCode=$arrCase[$caseid]['sectioncode'];
				$cquestionCount=$arrCase[$caseid]['questionscount'];
				$csectionMarks=$arrCase[$caseid]['marks'];
				if ($csqlInsert1=="")
					$csqlInsert1 = "INSERT INTO iib_qp_weightage (question_paper_no, exam_code, subject_code, section_code, marks, no_of_questions,case_id) VALUES ('$questionPaperNo', '$examCode', '$subjectCode', '$csectionCode', '$csectionMarks', '$cquestionCount','$caseid')";
				else
					$csqlInsert1.= ", ('$questionPaperNo', '$examCode', '$subjectCode', '$csectionCode', '$csectionMarks', '$cquestionCount','$caseid')";			
				if ($commonDebug)
				{
					print("\n".$csqlInsert1);
				}
				@mysql_query($csqlInsert1) or die("Insert on iib_qp_weightage failed");
				$csqlInsert1='';
				$arrquestionid=explode(',',$arrCqid[$caseid]);		
				shuffle($arrquestionid);
				for($k=0;$k < count($arrquestionid);$k++){							
					$cquestionID=$arrquestionid[$k];
					if($questionID != '') {
							$strOptOrder = "";											
							$rand_keys = $arrRAND[$cquestionID];						
							if ($commonDebug)
							{
								print("<br>rand keys: ");
								print_r($input);
								print_r($rand_keys);
								print("<br>");
							}
							if($answershuffling == 'Y'){
								shuffle($rand_keys);
							}					
							$strOptOrder = implode($rand_keys,",");
												
							if($csqlInsert2 == '')
							{
								 $csqlInsert2 = "INSERT INTO iib_question_paper_details (question_paper_no, subject_code, section_code, question_id, answer_order, display_order,case_id) VALUES ('$questionPaperNo', '$subjectCode', '$csectionCode', '$cquestionID', '$strOptOrder', $iCount,$caseid)";	
							}
							else
							{
								$csqlInsert2.=",('$questionPaperNo', '$subjectCode', '$csectionCode', '$cquestionID', '$strOptOrder', $iCount,$caseid)";
							}					
							$iCount++;
								
						}//questionID
					}//case For $k
			
			}//case for $i								
			if ($commonDebug)
			{
				print("\n".$csqlInsert2);
			}					
			mysql_query($csqlInsert2) or die("Insert on iib_question_paper_details(CASE STUDY) failed");
		
		} //CASE Check
			$sqlUpdate = "UPDATE iib_question_paper SET complete='Y' WHERE question_paper_no='$questionPaperNo' ";
			if ($commonDebug)
			{
				print "query :".$sqlUpdate;
			}		
			@mysql_query($sqlUpdate) or die("Update on iib_question_paper failed");
					
	}//end of for-papercount
		   
	$sqlInsert = "INSERT INTO iib_question_paper_generation (attributes_list, transaction_time) VALUES ('$strAttributesList', now())";
	if ($commonDebug)
	{
		print("\n".$sqlInsert);
	}	
	@mysql_query($sqlInsert) or die("Insert on iib_question_paper_generation  failed");
?>
