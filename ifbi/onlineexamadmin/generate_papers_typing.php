<?php
$admin_flag=1;
set_time_limit(0);
//require("sessionchk.php");
require("includes/constants.inc");
require("includes/dbconfig.php");
require("includes/settings.php");
$commonDebug = false;
?>
<html>
<head>
    <title><?=PAGE_TITLE ?></title>
</head>
<link rel="stylesheet" href="includes/exl.css" type="text/css">
<body leftmargin="0" topmargin="0">
<center>
<form name=frmgetsubjects method=post>
  <table width="780" border="0" cellspacing="0" cellpadding="0" >
    <tr>
      <td width="780"><? include("header.php"); ?></td>
    </tr>
    <tr>
      <td width="780" >&nbsp;</td>
    </tr>
    <tr>
		<Td ><? include("admin_menu.php") ?></Td>	
  </tr>
    <tr>
      <td width="780"  valign="top">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="24"> <div align="center" ><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Admin
                module - Question Paper(s) Generation</font> </strong></div></td>
          </tr>
          <tr>
            <td height="200" valign="middle" align="center">
<?php
    $examCode = $_POST['exam_code'];
    $subjectCode = $_POST['subject_code'];
    $nPapers = $_POST['total_question_paper'];
	$totalMarks = $_POST['total_marks'];
    if ($nPapers > 0)
    {
        $strAttributesList = "$examCode $subjectCode $nPapers $totalMarks Typing ";
		$sql = "SELECT section_code FROM oe_subject_sections_desc WHERE subject_code=$subjectCode and online='Y' ORDER BY section_code";
		if ($commonDebug)
		{
			print "query :".$sql;
		}
		$result = @mysql_query($sql) or die("Query failed");
		$nRows = mysql_num_rows($result);
		if ($nRows == 0)
		{
			print("<br>This subject has no sections<br>");
			return;
		}
		list($sectionCode) = mysql_fetch_row($result);
		for ($nCount = 0; $nCount < $nPapers; $nCount++)
		{
			$sqlQuestions = "SELECT question_paper_no FROM  oe_question_paper WHERE complete='N' ".
			" AND exam_code=$examCode AND subject_code=$subjectCode ";
			if ($commonDebug)
			{
				print "query :".$sqlQuestions;
			}
			$resQuestions = @mysql_query($sqlQuestions) or die("select on oe_question_paper failed");
			$nIncomplete = mysql_num_rows($resQuestions);
			if ($nIncomplete > 0)
			{
				$strIncomplete = "";
				while (list($incompleteQns) = mysql_fetch_row($resQuestions))
				{
					($strIncomplete == "") ? $strIncomplete = $incompleteQns : $strIncomplete .= ", ".$incompleteQns;
				}
				$sqlDelete = "DELETE FROM oe_question_paper WHERE question_paper_no IN ($strIncomplete)";
				if ($commonDebug)
				{
					print "query :".$sqlDelete;
				}
				@mysql_query($sqlDelete) or die("Delete on oe_question_paper failed");
				$sqlDelete = "DELETE FROM oe_qp_weightage_desc WHERE question_paper_no IN ($strIncomplete)";
				if ($commonDebug)
				{
					print "query :".$sqlDelete;
				}
				@mysql_query($sqlDelete) or die("Delete on oe_qp_weightage failed");			
				$sqlDelete = "DELETE FROM oe_question_paper_details_desc WHERE question_paper_no IN ($strIncomplete)";
				if ($commonDebug)
				{
					print "query :".$sqlDelete;
				}
				@mysql_query($sqlDelete) or die("Delete on oe_question_paper_details failed");			
			} //end of incomplete
			$sqlInsert = "INSERT INTO oe_question_paper (exam_code,subject_code,total_marks,enabled,online,assigned) ".
							"VALUES ($examCode,$subjectCode,$totalMarks,'Y','Y','N')";
			if ($commonDebug)
			{
				print "query :".$sqlInsert;
			}	
			@mysql_query($sqlInsert) or die("Insert on oe_question_paper failed");
			$questionPaperNo = mysql_insert_id();
			$sectionMarks = $totalMarks;

			$sqlInsert1 = "INSERT INTO oe_qp_weightage_desc ".
				"(question_paper_no, exam_code, subject_code, section_code, marks, ".
				" no_of_questions) VALUES ".
				"($questionPaperNo, $examCode, $subjectCode, $sectionCode, $sectionMarks, 1)";
			if ($commonDebug)
			{
				print("\n".$sqlInsert1);
			}
			@mysql_query($sqlInsert1) or die("Insert on oe_qp_weightage_desc failed");
			$sqlQuestions = "SELECT question_id FROM oe_section_questions_desc WHERE ".
					" exam_code=$examCode AND subject_code=$subjectCode AND section_code=$sectionCode ".
					" AND marks=$sectionMarks ORDER BY rand() LIMIT 1";
			if ($commonDebug)
			{
				print("\n".$sqlQuestions);
			}
			
			$result = @mysql_query($sqlQuestions) or die("select from oe_section_questions failed");
			$nRows = mysql_num_rows($result);
			if ($nRows == 0)
			{
		        print("<div class='alertmsg'><center>Please Contact the System Administrator</center></div>");
				return;
			}
			else
			{
				list($questionID) = mysql_fetch_row($result);
				$sqlInsert2 = "INSERT INTO oe_question_paper_details_desc ".
							"(question_paper_no, subject_code, section_code, question_id, display_order) ".
							"VALUES ($questionPaperNo, $subjectCode, $sectionCode, ".
							"$questionID,1)";
				if ($commonDebug)
				{
					print("\n".$sqlInsert2);
				}

				@mysql_query($sqlInsert2) or die("Insert on oe_question_paper_details failed");
				$sqlUpdate = "UPDATE oe_question_paper SET complete='Y' WHERE question_paper_no=$questionPaperNo ";
				if ($commonDebug)
				{
					print "query :".$sqlUpdate;
				}
				@mysql_query($sqlUpdate) or die("Update on oe_question_paper failed");

				$sqlInsert = "INSERT INTO oe_question_paper_generation (attributes_list, transaction_time) VALUES ('$strAttributesList', now())";
				if ($commonDebug)
				{
					print("\n".$sqlInsert);
				}
				@mysql_query($sqlInsert) or die("Insert on oe_question_paper_generation  failed");

			}
		}
       	print("<div class='alertmsg'><center>Question Paper(s) Generated<br></center></div>");

    }
    else
    {
        print("<div class='alertmsg'><center>Please Contact the System Administrator</center></div>");
    }
?>
           </td>
          </tr>
        </table>
        </td>
    </tr>
    <tr>
      <td width="780" ><? include("footer.php"); ?></td>
    </tr>
  </table>
</form>
</center>
</body>
</html>