<?php
/****************************************************
* Application Name            :  IIB
* Module Name                 :  HTML generate IM
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              : 
* Document/Reference Material :
* Created By                  : 
* Created ON                  : 
* Last Modified By            : Bala Murugan.S
* Last Modified Date          : 19-01-10
* Description                 : IM to generate html files
*****************************************************/
session_start();
set_time_limit(0);
$admin_flag=1;
require_once("sessionchk.php");
require_once("dbconfig.php");
$postsubcode=$_POST['subjectcode'];
$postexamcode=$_POST['examcode'];
$status=$_REQUEST['status'];
$medium=$_REQUEST['medium'];

if($status=='1')
{
	$postexamcode=$_REQUEST['ec'];
	$postsubcode=$_REQUEST['sc'];
	$qpcount=$_REQUEST['count'];
	$min=$_REQUEST['min_qp'];
	$max=$_REQUEST['max_qp'];
	$medium=$_REQUEST['medium'];
	
	$success=$qpcount." Question Papers generated successfully from Qp no: ".$min." to ".$max ;
}
else if($status=='0')
{
	$postexamcode=$_REQUEST['ec'];
	$postsubcode=$_REQUEST['sc'];
	$success="No Question Papers generated for this subject";
}
$qpmsg='';
$no_of_qp=0;
if($_POST['showSubject']=='true' || $status=='1' || $status=='0')
{
	$Sql="select b.subject_name,b.subject_code,a.exam_name from iib_exam a,iib_exam_subjects b where a.online='Y' and b.online='Y' and a.exam_code=b.exam_code and a.exam_code='".$postexamcode."' group by b.subject_code order by exam_name,subject_name";
	
	$Res=mysql_query($Sql);
	if($postsubcode!='')
	{
		
		$sel="select count(question_paper_no) from iib_question_paper where exam_code='$postexamcode' and subject_code='$postsubcode' and path is not null";
		//echo $sel;
		$res=mysql_query($sel);
		list($no_of_qp)=mysql_fetch_row($res);
		$qpmsg="Question Papers already generated : ".$no_of_qp;
	}
}
$sql="SELECT distinct (a.exam_code), a.exam_name FROM iib_exam a,iib_section_questions b WHERE a.online='Y' and a.exam_code=b.exam_code order by exam_name";
$res=mysql_query($sql);
?>
<HTML>
<HEAD>
<title>Indian Institute Of Banking &amp; Finance</title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
    FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='javascript'>
function chooseSubject()
{
	var f=document.addSection;
	if(f.examcode.value==-1)
	{
		alert('Please Select the Exam Name!');
		f.examcode.focus();
		return;
	}
	f.showSubject.value='true';
	f.submit();	
}
function qpSubject()
{
	var f=document.addSection;
	f.showSubject.value='true';
	f.submit();
}
function displayText()
{

}
function validate()
{
	var f=document.addSection;
    if(f.examcode.value==-1)
    {
        alert('Please Select the Exam Name!');
        f.examcode.focus();
        return;
    }
    if (!f.subjectcode)
    {
	    return;
    }
    
    if(f.subjectcode.value==-1)
    {
        alert('Please Select the Subject Name!');
        f.subjectcode.focus();
        return;
    }
    else
    {    	
    	f.showSubject.value='true';
    	f.showSection.value='true';
    	f.submitted.value='true';	
		f.action="qp_gen_script.php";
		f.submit();
	}
}

function clearValues()
{
	var f=document.addSection;
	f.examcode.selectedIndex = 0;
	f.subjectcode.selectedIndex = 0;
	f.examcode.focus();
}

function submitForm()
{
	if (window.event.keyCode == 13)
	{
		validate();
	}
}
</script>
</HEAD>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0 onKeyPress='javascript:submitForm()'>
<center>
<form name="addSection" method="post">	
<TABLE width=780 border=0 align="center" cellPadding=0 cellSpacing=0  background=images/tile.jpg>
    <TR>
	      <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
    </TR>
    <TR>
        <TD bgColor=#7292c5 width=780>&nbsp;</TD>
    </TR>
    <tr>
        <Td class="greybluetext10" ><? include("admin_menu.php") ?></Td>
    </tr>
    <TR>
        <TD vAlign=top width=780 align=center>
        	
        <table width=75% cellpadding=1 cellspacing=1 border=0>
            <tr>
                <td height="22" colspan=2 align="center" class="ColumnHeader">Questionpaper generation as HTML files (bulk)</td>
            </tr>        
      	<tr>
		<td width=272 align=right bgcolor="#E8EFF7"><b class="greybluetext10">Exam Name :&nbsp;</b></td>
		<td width="298" bgcolor="#E8EFF7">
		<select name='examcode' class='textbox' onchange='javascript:chooseSubject()' style='{width:250px}'>
			<option value=-1>-Select-</option>
		<?php 		
		while(list($eid,$ecode)=mysql_fetch_row($res))
		{
			if($postexamcode==$eid)
				echo "<option value=\"$eid\" selected>$eid-->$ecode</option>";
			else
				echo "<option value=\"$eid\">$eid-->$ecode</option>";
		}
		?>
		  </select>		</td>
		</tr>		
		<tr>
			<td width=272 align='right' bgcolor="#E8EFF7"><b class="greybluetext10">Subject Name :&nbsp;</b></td>
			<td bgcolor="#E8EFF7">
			 <select name='subjectcode' class='textbox' style='{width:250px}' onchange='javascript:qpSubject();'>
				<option value=-1>-Select-</option>
		<?php
				while(list($subjname,$subjcode,$ecode)=mysql_fetch_row($Res))
				{
   		         if($postsubcode==$subjcode)
        		        echo "<option value=\"$subjcode\" selected>$subjcode-->$subjname</option>";
            	else
                		echo "<option value=\"$subjcode\">$subjcode-->$subjname</option>";
				}				
			?>
		</select>		</td></tr>
		<tr>
		  <td align='right' bgcolor="#E8EFF7"><b class="greybluetext10">Medium  :&nbsp;</b></td>
		  <td bgcolor="#E8EFF7"><select class='textbox' style='{width:250px}' name="medium" id="medium">
		  <option value="E">English</option>
		  <option value="H">Hindi</option>		
		    </select>
		  </td>
		</tr>			
		<?php if($no_of_qp!=0){?>					
			<tr>			
				<td bgcolor="#E8EFF7">&nbsp;</td> 
                <td colspan=2 align="left" bgcolor="#E8EFF7" class='greybluetext10'><b><?php echo $qpmsg;?></b>				</td>
           </tr>		
		   <?PHP } ?>		
			<tr>
				<td bgcolor="#E8EFF7">&nbsp;</td>
				<td  align=left bgcolor="#E8EFF7">
				<input type='button' class='button' name='addrecord' value='Submit' onclick='javascript:validate();'>
				<input type='button' class='button' name='Rese' value='Reset' onClick='javascript:clearValues()'>			  </td>
			</tr>
			<?php if(isset($err)){?>
            <tr>
                <td colspan=2 align="center" bgcolor="#E8EFF7" class='errormsg'><b><?echo $emsg;?></b></td>
            </tr>
        <? }?>
        <? if(isset($done)){?>
            <tr>
                <td colspan=2 align="center" bgcolor="#E8EFF7" class='alertmsg'><b><?echo $msg;?></b></td>
            </tr>
        <? }?>
		<?php if(isset($status)){ ?>		
            <tr>
				<td bgcolor="#E8EFF7">&nbsp;</td>
                <td colspan=2 align="left" bgcolor="#E8EFF7" class='alertmsg'><b><?php echo "Question Papers generated : ".$qpcount;?>
				</b></td>
            </tr>
			<tr>
				<td bgcolor="#E8EFF7">&nbsp;</td>
                <td colspan=2 align="left" bgcolor="#E8EFF7" class='alertmsg'><b><?php echo "QP Range : ".$min." to ".$max ; ?></b></td>
            </tr>
			<? }?>	
		</table>
        <br/>
		</TD>
	</TR>		
</table>
<input type='hidden' name='showSubject'>
<input type='hidden' name='showSection'>
<input type='hidden' name='submitted'>
<input type='hidden' name='hid_variable' value="<?=$SECTIONNAME?>">
</form>
</body>
</html>
