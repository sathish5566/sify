<?php

/****************************************************

* Application Name            :  IBPS

* Module Name                 :  Printing Response Report

* Revision Number             :  1

* Revision Date               :

* Table(s)                    :  

* Tables used for only selects:  iib_candidate,iib_candidate_iway,iib_exam_subjects,iib_candidate_test

* View(s)                     :  -

* Stored Procedure(s)         :  -

* Dependant Module(s)         :  constants.inc 

* Output File(s)              :

* Document/Reference Material :

* Created By	              :  Devi B

* Created ON                  :  02/05/2006.

* Last Modified By            :  B.Devi

* Last Modified Date          :  02/05/2006.

* Description                 :  Generating Response Report

*****************************************************/



header ("Content-type: application/msword"); 

set_time_limit(0);

ob_start();

//require_once("sessionchk.php");

require_once("constants.inc");

require_once ("dbconfig_slave_123.php");



//Harcoded for number rows in a table and tables in a row

$table_per_row = $cand_table_per_row;

$no_of_rows = $cand_no_of_rows;



$offset = $_REQUEST["hOffset"];

$pagecount = $_REQUEST["hPageCount"];

$page = $_REQUEST["hPage"];

$each_link = $_REQUEST["hEachLink"];

$link_per_page = $_REQUEST["hLinkPerPage"];

$index = $_REQUEST["hIndex"];

$diff = $_REQUEST["hDiff"];

$centre_code = $_REQUEST["sCentreCode"];	

$sExamCode=isset($_REQUEST["sExamCode"])?$_REQUEST["sExamCode"]:"";

$rec_per_page=$each_link*$link_per_page;

$displacement = $offset + ($index*$each_link);





if ($sExamCode== 0)

        $sql_iway_can = "select distinct(membership_no) from iib_candidate_iway  a, iib_iway_details b where b.exam_centre_code = '$centre_code' and a.centre_code = b.centre_code order by membership_no limit $displacement,$diff";

elseif ($sExamCode != "")

        $sql_iway_can = "select distinct(a.membership_no) from iib_candidate_iway a, iib_iway_details b where a.exam_code='$sExamCode' and b.exam_centre_code = '$centre_code' and a.centre_code = b.centre_code order by membership_no limit $displacement,$diff";



$res_iway_can=@mysql_query($sql_iway_can);

if (mysql_error()){

	echo "Error in Selection of Mapped Candidates".mysql_error();

	exit;	

}



$num_can = @mysql_num_rows($res_iway_can);

if ($num_can < 1){

	echo "No Details found!";

	header("Content-Disposition: attachment; filename=responsereport_nodetails.doc");

	 exit;

}

$j=0;

while ($j < $num_can){

	list($memno[$j])= @mysql_fetch_row($res_iway_can);

	$j++;

}//while($j)



$add=1;

$num_can = count($memno);

$st_memno = $memno[0];

$end_memno = $memno[$num_can-1];

$memlist = implode('\',\'',$memno);







$sql_can = "select name from iib_candidate where membership_no in ('$memlist')";

$res_can = mysql_query($sql_can);

if (mysql_error()){

	echo "Error in Selection of Candidates".mysql_error();

	exit;	

}

$i=0;

header("Content-Disposition: attachment; filename=reponsereport_".$st_memno."_".$end_memno.".doc");

while ($memno[$i]!=""){

	

	$c_no = $memno[$i];	

	list($c_name)=mysql_fetch_row($res_can);



	$sql_sel_subj="select subject_code from iib_candidate_iway where membership_no='$c_no' ";

	$res_sel_subj=mysql_query($sql_sel_subj);

	list($subj_code)=mysql_fetch_row($res_sel_subj);

	

	$sqlCentre="SELECT a.centre_code centre_code, DATE_FORMAT(a.exam_date, '%d/%m/%Y') examdate,  TIME_FORMAT(a.exam_time, '%H:%i') examtime , b.question_paper_no question_paper_no FROM iib_candidate_iway a , iib_candidate_test b WHERE  a. membership_no= b. membership_no  and a.subject_code='$subj_code' AND a.exam_code='$sExamCode' AND a.membership_no='$memno[$i]' group by b. membership_no";	

	$resCentre = @mysql_query($sqlCentre) or die("select from iib_candidate_iway failed ".mysql_error());

	$nrows=mysql_num_rows($resCentre);

	

	$row = mysql_fetch_array($resCentre);

	

	$centreCode = $row['centre_code'];

	$examDate = $row['examdate'];

	$examTime = $row['examtime'];

	$questionPaperNo=$row['question_paper_no'];

	

	$sql_question="select answer , question_id from iib_question_paper_details where  subject_code='$subj_code' and question_paper_no='$questionPaperNo' order by display_order";

	$res_question=mysql_query($sql_question);

	$result_cnt=mysql_num_rows($res_question);

	$no_of_tables=ceil($result_cnt/$no_of_rows);

	

	$a=0;

	$attempt_cnt=0;

	while(list($ans,$ques_id)=mysql_fetch_row($res_question))

	{

		if($ans=='')

		{

			$ans='---';

		}

			else

	  	{

		  	$attempt_cnt++;

	  	}

		$result_arr[$a][0]=$ans;

		$result_arr[$a][1]=$ques_id;

		$a++;

	}

	

        $total = count($result_arr);

        $unattempt  = $total - $attempt_cnt;





      $sqlIWay = "SELECT iway_address1, iway_address2,  exam_centre_name, iway_state, iway_pin_code FROM iib_iway_details a , iib_exam_centres b  WHERE a. exam_centre_code = b. exam_centre_code and a. centre_code='$centreCode'";

      

      $resIWay = @mysql_query($sqlIWay) or die("select from iib_iway_details failed ".mysql_error());

		$row_iway = mysql_fetch_array($resIWay);

		$iwayAddress = "";

		$row_iway['iway_address1'] != "" ? $iwayAddress .= " ".$row_iway['iway_address1'] : $iwayAddress .= "";

		$row_iway['exam_centre_name'] != "" ? $iwaycity = $row_iway['exam_centre_name'] : $iwaycity= "";

		$row_iway['iway_pin_code'] != "" ? $iwayAddress .= " ".$row_iway['iway_pin_code'] : $iwayAddress .= "";

		$row_iway['iway_state'] != "" ? $iwayAddress .= " ".$row_iway['iway_state'] : $iwayAddress .= "";

		$row_iway['iway_address2'] != "" ? $iwayAddress .= ",Contact No. :  ".$row_iway['iway_address2'] : $iwayAddress .= "";

		

?>

<STYLE>

<!--

	@page { size: 8.5in 11in; margin: 0.79in }

	TD P { margin-bottom: 0in }

	TH P { margin-bottom: 0in; font-style: italic }

	P { margin-bottom: 0.01in }

-->

</STYLE>



<table border="0" cellspacing="0" cellpadding="0" width="640" background="images/tile.jpg">

		<tr>

			<td>

			<!--<br>-->

			<table border="0" cellspacing="0" cellpadding="5" width="100%" align=center>

				<tr>

				



					<td colspan="3" class="greybluetext10" bgcolor="#D1E0EF" align=center><b class="arial11a"></u>Response Report of the Candidate</u></b></td>

					

				</tr>

				<?if($nrows>0){?>

				<tr>

					<td class="greybluetext10" bgcolor="#E8EFF7"><b>Date</b></td>

					<td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>

					<td bgcolor="#E8EFF7" class="greybluetext10"><?=$examDate ?></td>

				</tr>

				<tr> 

					<td width="35%" class="greybluetext10" bgcolor="#E8EFF7"><b>Membership No</td>

					<td width="2%" class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>

					<td width="53%" bgcolor="#E8EFF7" class="greybluetext10"><?=$c_no ?></td>

				</tr>

				<tr> 

					<td class="greybluetext10" bgcolor="#E8EFF7"><b>Name</b></td>

					<td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>

					<td bgcolor="#E8EFF7" class="greybluetext10"><?=$c_name ?></td>

				</tr>

				<tr>

					<td class="greybluetext10" bgcolor="#E8EFF7"><b>Centre</b></td>

					<td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>

					<td bgcolor="#E8EFF7" class="greybluetext10"><?=$iwaycity ?></td>

				</tr>

				<tr>

					<td class="greybluetext10" bgcolor="#E8EFF7"><b>Iway</b></td>

					<td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>

					<td bgcolor="#E8EFF7" class="greybluetext10"><?=$iwayAddress ?></td>

				</tr>

				<?}else{?>

				<tr> 

					<td width="35%" class="greybluetext10" bgcolor="#E8EFF7"><b>Membership No</td>

					<td width="2%" class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>

					<td width="53%" bgcolor="#E8EFF7" class="greybluetext10"><?=$c_no ?></td>

				</tr>

				<tr> 

					<td class="greybluetext10" bgcolor="#E8EFF7"><b>Name</b></td>

					<td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>

					<td bgcolor="#E8EFF7" class="greybluetext10"><?=$c_name ?></td>

				</tr>

				<tr>

					<td class="greybluetext10" bgcolor="#E8EFF7" colspan=3 align=center><b>Candidate has not taken the test</b></td>					

				</tr>

				<?}?>

			</table>

    

			</td>

		</tr>

		<?if($nrows>0){?>

		<tr>

		<td>

		<table border="0" cellspacing="0" cellpadding="0" bgcolor="#E8EFF7" width="100%" background="images/tile.jpg">

		<?

		$quescnt=1;

		$array_index=0;

		$tab_cnt=0;

		?>

		<tr>

		<?

		for($n=0;$n<$no_of_tables;$n++)

		{

			$tr_flag=false;

				if($tab_cnt==$table_per_row)

				{

					$tr_flag=true;

					$tab_cnt=0;

					echo "<tr><td>&nbsp;</td></tr>";

					echo "<tr>";

				}

				

				echo "<td class='greybluetext10' colspan=2 bgcolor='#E8EFF7'>";

				echo "<table border='1' cellspacing='0' cellpadding='0' width='200' background='images/tile.jpg'>";

				echo "<tr><td class='greybluetext10' bgcolor='#E8EFF7'><b>Q.No</b></td><td class='greybluetext10' bgcolor='#E8EFF7'><b>Response</b></td><td class='greybluetext10' bgcolor='#E8EFF7'><b>Q.id</b></td></tr>";

				for($j=0;$j<$no_of_rows;$j++)

				{

			?>

				<tr>

					<td class="greybluetext10" bgcolor='#E8EFF7'>

						&nbsp;<?if($result_arr[$array_index][1]!='') echo $quescnt;?>			

					</td>

					<td class="greybluetext10" bgcolor='#E8EFF7'> 

						<?echo "&nbsp;".$result_arr[$array_index][0];?>					

					</td>

					<td class="greybluetext10" bgcolor='#E8EFF7'>

						<?echo "&nbsp;".$result_arr[$array_index][1];?>	

					</td>

				</tr>

				<?

				$array_index++;

				$quescnt++;

				}

				echo "</table>";

				echo "</td>";

				if($tr_flag)

				echo "</tr>"; 

		  $tab_cnt++;

		}

		?>

		</tr>

		</table>

		</td>

		</tr>

		

		<tr><td bgcolor='#E8EFF7'>&nbsp;</td></tr>

			

		<tr>

			<td class="greybluetext10" align=left bgcolor='#E8EFF7'><b>Total Number of questions Attempted: <?=$attempt_cnt?></b>

				</td>

			</tr>

			<tr>

			<td class="greybluetext10" align=left bgcolor='#E8EFF7'><b>Total Number of questions Un Attempted: <?=$unattempt?></b>

				</td>

			</tr>

		<?}?>

		

</table>





<TABLE WIDTH=100% BORDER=0  CELLPADDING=0 CELLSPACING=0>

<?

$i++;//increment for the membership_number



?>	





</TBODY>

</TABLE>

<?

}//end of while ($memno[$i]!="")

ob_end_flush();

?>



