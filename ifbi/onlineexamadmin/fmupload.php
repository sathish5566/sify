<?
require("dbconfig.php");
//print_r($_POST);
$commondebug=false;
$msg=0;
$time=date(jF_Gis);
	$filepath = "./fm_files/";
	
	// getting the extension of the file
	$ext=explode('.',$_FILES['userfile']['name']);
	
	// only if the file is of .txt format proceed further
	if($ext[1]=='txt')
	{
		$uploadfile = $filepath . $time .$_FILES['userfile']['name'];
	
		if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
		} else {
			print "Possible file upload attack!  Here's some debugging info:\n";
			print_r($_FILES);
			exit;
		}
		// get contents of a file into a string
		$filename = $uploadfile;
		if($filename!='')
		{
			$sql_log_insert="insert into iib_fm_log (file,updatedtime) values('$filename',now())";
			$res_log_insert=mysql_query($sql_log_insert);
		}
		$handle = fopen($filename, "r");
		//Writing the comtents of the file to an ARRAY named $aManual
		while(!feof($handle))
		{
			$aManual[] = fgets($handle);
		}
		 fclose($handle);
		
		if ($commondebug){
			echo "<pre>";
			print_r($aManual);
			echo "</pre>";
		}
		$i=0;
		// Exploding the comma seperated values into an array named $row
		foreach ($aManual as $value)
		{
			if($value!='')
			{	
				$row[$i]=explode(',',$value);
			}
			$i++;
		}	
	
		$cnt= count($row);
		
		//Query to get the centre code and actual seats respectively
		$sql_actual="select centre_code,actual_seats from iib_iway_details order by centre_code";
		$res_actual=mysql_query($sql_actual);
		$c=0;
		// Array formation to contain actual seats & centre code details 
		while(list($CCode,$actual)=mysql_fetch_row($res_actual))
		{
			$CCode=strtoupper($CCode);
			$actualArr[$CCode]=$actual;
			$CentreArr[$c]=$CCode;
			$c++;
		}
	
		$break='false';
		$actual_less='false';
		$dup_code='';
		$result_vali='false';
		for($l=0;$l<$cnt;$l++)
		{
		$row[$l][0]=strtoupper(trim($row[$l][0]));
		
		// Validating the centre code to contain 6 digits and an alphabet, seats to contain numbers , contact no and fmname to be a string
		//if ((ereg("^[0-9]{6}[a-zA-Z]+$", $row[$l][0])) && (is_numeric($row[$l][1])) && (is_string($row[$l][3])) && (is_string($row[$l][2])))
		if ((ereg("^[0-9]{6}[a-zA-Z]+$", $row[$l][0])) && (ereg("^[0-9][0-9]*$",$row[$l][1])) && (ereg("^[a-zA-Z][a-zA-Z ]*$",$row[$l][2])) && (ereg("^[0-9][0-9\\\/-]*$",trim($row[$l][3]))))
		{
			$result_vali='true';
			$actual_seats = $actualArr[$row[$l][0]];
		
		//if the actual seats in the file and the actual seats in the DB are equal proceed
			if($row[$l][1] == $actual_seats)
			{
				if($l=='0') 
				{
					$code[$l]=$row[$l][0];
				}
				else
				{
					//Checking for duplicates
					if((in_array($row[$l][0],$code)))
					{
						$msg=4;
						$dup_code=$row[$l][0];
						$break='true';	
					}
					else
					{
						$code[$l]=$row[$l][0];
					}
				}
				if($break=='true') break;
				$arrcnt[$l]=sizeof($row[$l]);
				
			}
			
			// when actual seats in the file and the actual seats in the DB are not equal
			else
			{
				$actual_less='true';
				$actual_code=$row[$l][0];
				$break='true';
			}
		}
		//If the validations are not satisfied
		else
		{
			$result_vali='false';
			$result_CCode=$row[$l][0];
			$break='true';	
			
		}
			if($break=='true') break;
		}//end for loop
		
		$less_data='false';
		if($result_vali=='true')
		{
			if($msg!=4)
			{
				//Checking the flag to see if the condition satisfies or not
				if($actual_less!='true')
				{
					if(is_array($code) || is_array($CentreArr))
					{
						$resultArr=array_diff($CentreArr,$code);
						if(count($resultArr) > 0 )
						{
							$less_data='true';
						}
					}
				}
			}
		}
		
	
		$sql_sel="select count(1),centre_code from iib_iway_details group by centre_code";
		$res_sel=mysql_query($sql_sel);
		while(list($No,$Centre_Code)=mysql_fetch_row($res_sel))
		{
			$Centre_Code=strtoupper($Centre_Code);
			$countArr[$Centre_Code]=$No;
		}
		
		$fields='false';
		$no_flag='false';
		$dup_flag='false';
		$break='false';
		for ($chk=0;$chk<$cnt;$chk++)
		{
			$row_code=strtoupper($row[$chk][0]);
			$cnt_data=$countArr[$row_code];
			if($cnt_data=='') $cnt_data=0;
			// to check the no of columns in the file
			if($arrcnt[$chk] > 4)
			{
				$fields='true'	;
				$break='true';
				break;
			}
			if($cnt_data == 0)
			{
				$no_Ccode=$row_code;
				$no_flag='true';
				$break='true';
				break;
			}
			else if ($cnt_data > 1)
			{
				$dup_Ccode=$row_code;
				$dup_flag='true';
				$break='true';
				break;
			}
			if($break=='true') break;
		}
		//if the no of rows in the file  is != 0 then proceed
		if($cnt!='0')
		{
		  // if actual seats are equal then proceed
		
		  if($result_vali=='true')
		  {
			  if($less_data!='true')
			  {
			  	if($no_flag!='true')
			  	{
					if($dup_flag!='true')
			  		{	  
						// checking for duplicates
						if($msg!='4')
						{
							if($fields!='true')
							{
								if($actual_less!='true')
								{	
							//truncating the tables iib_temp , iib_franchisee , iib_franchisee_map , iib_franchisee_iway each time it if all the condns are satisfied
							
							$sql_truncate_temp="truncate iib_temp";
							$res_truncate_temp=mysql_query($sql_truncate_temp);
							
							$sql_truncate_franchisee="truncate iib_franchisee";
							$res_truncate_franchisee=mysql_query($sql_truncate_franchisee);
							
							$sql_truncate_franchisee_map="truncate iib_franchisee_map";
							$res_truncate_franchisee_map=mysql_query($sql_truncate_franchisee_map);
							
							$sql_truncate_franchisee_iway="truncate iib_franchisee_iway";
							$res_truncate_franchisee_iway=mysql_query($sql_truncate_franchisee_iway);
							
							$val_flag=false;
							$temp_insert=false;
							for($k=0;$k<$cnt;$k++)
							{
								//assigning the values from the array to the variables 
								$c_Code=trim($row[$k][0]);
								$seats=$row[$k][1];
								$name=trim($row[$k][2]);
								$mob=trim($row[$k][3]);
								$no=$countArr[$c_Code];
								$result_vali=false;
								
								// Validating the centre code to contain 6 digits and an alphabet, seats to contain numbers , contact no and fmname to be a string
							//	if ((ereg("^[0-9]{6}[a-zA-Z]+$", $c_Code)) && (is_numeric($seats)) && (is_string($mob)) && (is_string($name)))
							/*if ((ereg("^[0-9]{6}[a-zA-Z]+$", $c_Code)) && (ereg("^[0-9][0-9]*$",$seats)) && (ereg("^[a-zA-Z][a-zA-Z ]*$",$name)) && (ereg("^[0-9][0-9\\\/-]*$",trim($mob))))
								{
									// If Validations satisfy then set $result_vali to true
									$result_vali='true';
								}
								else
								{
									// If Validations does not satisfy then set $result_vali to false
									$result_vali='false';
								}*/
								
									//Checking the no of the centre code details in the DB
									if($no == '1')
									{
										// insert into iib_temp
										$sql_insert_temp="insert into iib_temp (centre_code,no_of_seats,fm_name,mobile) values('$c_Code','$seats','$name','$mob')";
										$res_insert_temp=mysql_query($sql_insert_temp);
										if($res_insert_temp)
										{
											$temp_insert='true';
										}
									}
									else if($no == 0)
									{
										$no_flag='true';
									}
									else if($no > 1)
									{
										$dup_flag='true';
									}
								
							}
							if($temp_insert=='true')
							{
								// insert into iib_franchisee
								$sql_insert_franchisee="insert into iib_franchisee (row_id,name,mobile,exam_centre_code)  select distinct '',a.fm_name,a.mobile,b.exam_centre_code from iib_temp a, iib_iway_details b where a.centre_code=b.centre_code";
								$res_insert_franchisee=mysql_query($sql_insert_franchisee);
																	
								// insert into iib_franchisee_map
								$sql_insert_franchisee_map="insert into iib_franchisee_map (FranchiseeId,centre_code,no_of_seats,city,phone)  select distinct a.row_id,b.centre_code,b.no_of_seats,c.iway_city,b.mobile from iib_franchisee a, iib_temp b, iib_iway_details c where b.centre_code=c.centre_code and a.mobile=b.mobile and a.name=b.fm_name and a.exam_centre_code=c.exam_centre_code";
								$res_insert_franchisee_map=mysql_query($sql_insert_franchisee_map);
										
								//insert into iib_franchisee_iway
								$sql_insert_franchisee_iway="insert into iib_franchisee_iway (fm_id,centre_code)  select FranchiseeId , centre_code from iib_franchisee_map";
								$res_insert_franchisee_iway=mysql_query($sql_insert_franchisee_iway);
							}
							
						
							if($no_flag!='true' || $dup_flag!='true' || $result_vali=='true')
							{
								
									//form the header,if uploading operations are done successfully
									header("Location:fmfile_upload.php?upd=true&msg=1");
							}
						}
						// if actual seats are not equal then else
						else
						{
							//form the header,if actual seats are not equal
							header("Location:fmfile_upload.php?upd=false&msg=9&actls=$actual_code");
						}
						}
						else
						{
							//form the header,if there are more than 4 fields in the file
							header("Location:fmfile_upload.php?upd=false&msg=8");
						}
						
					}
						else
						{
							//form the header,if there are duplicate records in the file
							header("Location:fmfile_upload.php?upd=false&msg=4&dup=$dup_code");
						}
				}
			else if ($dup_flag=='true')
			{
				//form the header,if there are duplicate records in the DB
				header("Location:fmfile_upload.php?upd=false&msg=5&duptbl=$dup_Ccode");
			}	
			}
			else if ($no_flag=='true')
			{
				//form the header,if there are no records for the centre code in the DB
				header("Location:fmfile_upload.php?upd=false&msg=6&norec=$no_Ccode");
			}
		}
		else
		{
			//form the header,if the data in the file is less than the records in iway table
			header("Location:fmfile_upload.php?upd=true&msg=10&fn=$uploadfile");
		}
		}
		else
		{
			//form the header,if there are validation problems
			header("Location:fmfile_upload.php?upd=false&msg=7&vali_code=$result_CCode");
		}
		
		//}
	
		
		}
		else
		{
			//form the header,if there are no contents in the file
			header("Location:fmfile_upload.php?upd=false&msg=2");
		}
	}
	else
	{
		//form the header,if the uploaded file is other than text(.txt) file
		header("Location:fmfile_upload.php?upd=false&msg=3");
	}
?>