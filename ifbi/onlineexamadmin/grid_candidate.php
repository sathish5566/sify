<?php
function getBrowser($useragent){
    if(trim($useragent) == 'Sify Browser'){
		$browser='Sify Browser';					
        $version='1.0';
    }
    else if (preg_match('|MSIE ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
		$browser_version=$matched[1];
		$browser = 'IE';
	}else if(preg_match( '|Opera ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
		$browser_version=$matched[1];
		$browser = 'Opera';
	} elseif(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched)) {
			$browser_version=$matched[1];
			$browser = 'Firefox';
	}else if(preg_match('|Chrome/([0-9\.]+)|',$useragent,$matched)) {
			$browser_version=$matched[1];
			$browser = 'Google Chrome';
	}else if(preg_match('|Safari/([0-9\.]+)|',$useragent,$matched)) {
			$browser_version=$matched[1];
			$browser = 'Safari';
    } else {
		// browser not recognized!
		$browser_version = 0;
        $browser= 'other';
    }
   
	return $browser.'&nbsp;'.$browser_version;
}

$admin_flag=2;
//Get to know the status of a candidate on the date of exam.
require("dbconfig_slave_123.php");
require("sessionchk.php");
$currentDate = date('Y-m-d');
$cname=$_REQUEST['id'];
if($cname!=""){
    $cname = trim($cname);
    $arrExamCentre=array();
    $sql_centre = "select exam_centre_code,exam_centre_name from iib_exam_centres";
    if($res_centre = @mysql_query($sql_centre)){
        while($rcentre = mysql_fetch_object($res_centre)){
            $arrExamCentre[$rcentre->exam_centre_code]=$rcentre->exam_centre_name;
        }
    }
    $arrLang=array();
    $arrLang['E']='ENGLISH';
    $sql_lang = "select lang_code,lang_name from iib_languages";
    if($res_lang = @mysql_query($sql_lang)){
        while($rlang = mysql_fetch_object($res_lang)){
            if( ($rlang->lang_code == "HI") || ($rlang->lang_code == "HISU") || ($rlang->lang_code == "HISH") || ($rlang->lang_code == "HIDV") ){        
                $arrLang[$rlang->lang_code]='HINDI'.' ('.$rlang->lang_code.')';
            }else
                $arrLang[$rlang->lang_code]=$rlang->lang_name.' ('.$rlang->lang_code.')';
        }
    }
    //Select the entry from candidate table.
    $sql_candidate = "select name,exam_centre_code,institution_code,institution_name,address1,address2,address3,"
                     ."address4,address5,address6,city,pin_code from iib_candidate where membership_no='".$cname."'";
	$res_candidate = @mysql_query($sql_candidate);
	$num_iib_candidate = @mysql_num_rows($res_candidate);

	//Select the entry from exam_candidate table.
	$sql_exam_candidate = "select membership_no,exam_code,subject_code,medium_code,institution_code,alloted from iib_exam_candidate where membership_no='".$cname."'";
		
	$res_exam_candidate = @mysql_query($sql_exam_candidate);
	$num_exam_candidate = @mysql_num_rows($res_exam_candidate);

	//Select the entry from candidate_iway table.
 	$sql_candidate_iway = "select membership_no,centre_code,a.exam_code,a.subject_code,exam_date,exam_time,exam_name,subject_name from iib_candidate_iway a,iib_exam b,iib_exam_subjects c where membership_no='".$cname."' and b.exam_code=a.exam_code and c.subject_code=a.subject_code";				
	$res_candidate_iway = @mysql_query($sql_candidate_iway);
	$num_candidate_iway = @mysql_num_rows($res_candidate_iway);

	//Select the entry from question_paper table.
	$sql_question_paper = "select question_paper_no,exam_code,subject_code,assigned,complete,membership_no,medium_code from iib_question_paper where membership_no ='".$cname."'";
	$res_question_paper = @mysql_query($sql_question_paper);
	$num_question_paper = @mysql_num_rows($res_question_paper);

	//Select the entry from iib_candidate_track table.
 	$sql_candidate_track = "select ta_login,membership_no,host_ip,session_id,updated_time,browser_details,exam_code,subject_code,user_status from iib_candidate_tracking where membership_no ='".$cname."' order by updated_time";
    $res_candidate_track = @mysql_query($sql_candidate_track);
	$num_candidate_track = @mysql_num_rows($res_candidate_track);
	
	//Select the entry from iib_candidate_test table.
	$sql_candidate_test = "select test_id,exam_code,subject_code,question_paper_no,ta_override,test_status,start_time,last_updated_time,end_time,total_time,time_taken,time_left,browser_status,time_extended from iib_candidate_test where membership_no ='".$cname."'";
    $res_candidate_test = @mysql_query($sql_candidate_test);
	$num_candidate_test = @mysql_num_rows($res_candidate_test);


	//Select the entry from iib_candidate_scores table.	
	$sql_candidate_scores = "select exam_code,subject_code,score,exam_date,time_taken,result,auto_submit from iib_candidate_scores where membership_no='".$cname."'";
	$res_candidate_scores = @mysql_query($sql_candidate_scores);
	$num_candidate_scores = @mysql_num_rows($res_candidate_scores);	
}
?>
<html>
<head>
<STYLE>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>
.textblk11 { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; text-decoration: none}
.greybluetextans {
font-family:Verdana,Arial,Helvetica,sans-serif;
font-size:11px;
font-style:normal;
font-weight:400;
color:#395157;
text-decoration:none;
cursor:default;
}
.ColumnHeader {
font-family:Verdana,Arial,Helvetica,sans-serif;
font-size:12px;
background-color:#72A9D3;
font-weight:700;
color:#fff;
border-color:#fff;
white-space: nowrap;
}
.greybluetext {
font-family:Verdana,Arial,Helvetica,sans-serif;
font-size:11px;
font-style:normal;
font-weight:bold;
color:#395157;
text-decoration:none;
text-align:center;
}
</STYLE>
</head>
<BODY leftMargin=0 >
<center>
 <TABLE border=0 cellPadding=0 cellSpacing=0 width=1000> 
       <TR>
    	<TD  vAlign=top width=1000 align=center>					
		<?php
		if($cname!=""){			
            echo "<span class=greybluetext><h2>MEMBERSHIP NO : ".$cname."</h2></span>";            
            echo "<span class=greybluetext>Records in Candidate Table is : ".$num_iib_candidate."</span>";
			echo "<table width=\"100%\" border=\"0\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\">
                  <tr><td class=\"ColumnHeader\"><table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"5\">";
            echo "<tr class=ColumnHeader align=\"center\">
                 <td>Name</td>
                 <td>Address</td>
                 <td>Exam Centre</td>
                 <td>Institution Code</td>
                 <td>Institution Name</td></tr>";             
             while($row=mysql_fetch_object($res_candidate)){				
                $ADDRESS='';
                if($row->address1!='')
                    $ADDRESS.=$row->address1;
                if($row->address2!='')
                    $ADDRESS.='<br/>'.$row->address2;
                if($row->address3!='')
                    $ADDRESS.='<br/>'.$row->address3;
                if($row->address4!='')
                    $ADDRESS.='<br/>'.$row->address4;
                if($row->address5!='')
                    $ADDRESS.='<br/>'.$row->address5;
                if($row->address6!='')
                    $ADDRESS.='<br/>'.$row->address6;
                if($row->city!='')
                    $ADDRESS.='<br/>'.$row->city;
                 if($row->pin_code!='')
                    $ADDRESS.='<br/>pin:'.$row->pin_code;
                 echo "<tr bgcolor=\"#FFFFFF\" align=\"center\" class=\"greybluetextans\">
                       <td width='20%'>$row->name</td>
                       <td width='40%'>$ADDRESS</td>
                       <td width='10%'>".$arrExamCentre[$row->exam_centre_code].' ('.$row->exam_centre_code.')'."</td>
                       <td width='10%'>$row->institution_code</td>
                       <td width='20%'>$row->institution_name</td></tr>";
			}
            echo "</table></td></tr></table><br/>";
            echo '</td></tr><tr><td>';
           
            echo " <span class=greybluetext>Records in Exam Candidate Table is : ".$num_exam_candidate."</span>";
			echo "<table width=\"100%\" border=\"0\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\">
                  <tr><td class=\"ColumnHeader\"><table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"5\">";
            echo "<tr class=ColumnHeader align=\"center\">
                 <td>Ex Code</td>
                 <td>Sub Code</td>
                 <td>Medium</td>
                 <td>Alloted</td>
                 </tr>";             
            while($row=mysql_fetch_object($res_exam_candidate)){				
                       echo "<tr bgcolor=\"#FFFFFF\" align=\"center\" class=\"greybluetextans\">
                       <td width='20%'>$row->exam_code</td>
                       <td width='20%'>$row->subject_code</td>
                       <td width='20%'>".$arrLang[$row->medium_code]."</td>
                       <td width='10%'>$row->alloted</td>
                      </tr>";
			}
			echo "</table></td></tr></table><br/>";
            echo '</td></tr><tr><td>';
    
           echo "<span class=greybluetext>Records in Candidate Iway Table is : ".$num_candidate_iway."</span>";
			echo "<table width=\"100%\" border=\"0\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\">
                  <tr><td class=\"ColumnHeader\"><table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"5\">";
            echo "<tr class=ColumnHeader align=\"center\">
                 <td>Ex Code</td>
                 <td>Sub Code</td>
                 <td>Centre Code</td>
                 <td>Exam Date</td>
                 <td>Exam time</td>
                 </tr>";             
            while($row=mysql_fetch_object($res_candidate_iway)){				
                       echo "<tr bgcolor=\"#FFFFFF\" align=\"center\" class=\"greybluetextans\">
                       <td width='20%'>$row->exam_code</td>
                       <td width='20%'>$row->subject_code</td>
                       <td width='20%'>$row->centre_code</td>
                       <td width='10%'>$row->exam_date</td>
                       <td width='10%'>$row->exam_time</td>
                      </tr>";
			}
			echo "</table></td></tr></table><br/>";
            echo '</td></tr><tr><td>';

            echo "<span class=greybluetext>Records in Question Paper Table is : ".$num_question_paper."</span>";
			echo "<table width=\"100%\" border=\"0\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\">
                  <tr><td class=\"ColumnHeader\"><table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"5\">";
            echo "<tr class=ColumnHeader align=\"center\">
                 <td>Ex Code</td>
                 <td>Sub Code</td>
                 <td>QP.No</td>
                 <td>Assigned</td>
                 <td>HTML_QP</td>
                 </tr>";             
            while($row=mysql_fetch_object($res_question_paper)){				
                    $QPNO=$row->question_paper_no;
                    echo "<tr bgcolor=\"#FFFFFF\" align=\"center\" class=\"greybluetextans\">
                       <td width='20%'>$row->exam_code</td>
                       <td width='20%'>$row->subject_code</td>
                       <td width='20%'>$row->question_paper_no</td>
                       <td width='10%'>$row->assigned</td>
                       <td width='10%'>$row->html_qp_generated</td>
                      </tr>";
			}
            echo "</table></td></tr></table><br/>";
            echo '</td></tr><tr><td>';

             echo "<span class=greybluetext>Records in Candidate Track Table is : ".$num_candidate_track."</span>";
			echo "<table width=\"100%\" border=\"0\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\">
                  <tr><td class=\"ColumnHeader\"><table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"5\">";
            echo "<tr class=ColumnHeader align=\"center\">
                 <td>TA</td>
                 <td>IP</td>
                 <td>Browser</td>
                 <td>Session ID</td>
                 <td>Updated Time</td>
                 <td>User Status</td>
                 <td>Ex Code</td>
                 <td>Sub Code</td>
                  </tr>";             
            while($row=mysql_fetch_object($res_candidate_track)){				
                $browser=getBrowser($row->browser_details);
                echo "<tr bgcolor=\"#FFFFFF\" align=\"center\" class=\"greybluetextans\">
                       <td width='10%'>$row->ta_login</td>
                       <td width='10%'>$row->host_ip</td>
                       <td width='10%'>$browser</td>
                       <td width='20%'>$row->session_id</td>
                       <td width='20%'>$row->updated_time</td>
                       <td width='10%'>$row->User_status</td>
                       <td width='10%'>$row->exam_code</td>
                       <td width='10%'>$row->subject_code</td>
                       </tr>";
			}
			echo "</table></td></tr></table><br/>";
            echo '</td></tr><tr><td>';


		    echo "<span class=greybluetext>Records in Candidate Test Table is : ".$num_candidate_test."</span>";
			echo "<table width=\"100%\" border=\"0\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\">
                  <tr><td class=\"ColumnHeader\"><table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"5\">";
            echo "<tr class=ColumnHeader align=\"center\"><td>Test ID</td>
                 <td>Ex Code</td>
                 <td>Sub Code</td>
                 <td>QP.NO</td>
                 <td>TA Override</td>
                 <td>Current Session</td>
                 <td>Browser Status</td>
                 <td>Extended Time</td>                
                 <td>Total Time</td>
                 <td>Start Time</td>
                 <td>Last Updated Time</td>
                 </tr>";             
            while($row=mysql_fetch_object($res_candidate_test)){				
                
                $browser=getBrowser($row->browser_details);
                  echo "<tr bgcolor=\"#FFFFFF\" align=\"center\" class=\"greybluetextans\"><td>$row->test_id</td>
                       <td width='20%'>$row->exam_code</td>
                       <td width='20%'>$row->subject_code</td>
                       <td width='20%'>$row->question_paper_no</td>
                       <td width='10%'>$row->ta_override</td>
                       <td width='10%'>$row->test_status</td>
                       <td width='10%'>$row->browser_status</td>
                       <td width='10%'>$row->time_extended</td>
                       <td width='10%'>$row->total_time</td>
                       <td width='10%'>$row->start_time</td>
                       <td width='10%'>$row->last_updated_time</td>
                      </tr>";
			}
            echo "</table></td></tr></table><br/>";
            echo '</td></tr><tr><td>';
		                $sqlQnsIds = "SELECT question_id FROM iib_question_paper_details WHERE question_paper_no='$QPNO' ORDER BY display_order";
                $resQnsIds = @mysql_query($sqlQnsIds);
                while(list($quesIdsVar) = @mysql_fetch_row($resQnsIds)) {
                        $quesIdsArr[] = $quesIdsVar;
                }

	    	//$sql_question = "select display_order,answer,updated_time,question_id  from iib_question_paper_details where question_paper_no='".$QPNO."' order by display_order";
		$sql_question = "select display_order,answer,updatedtime,question_id  from iib_response where id in ( select  max(id) from iib_response where question_paper_no ='$QPNO' group by question_id) order by display_order";
            $res_question = @mysql_query($sql_question);
            //$totquestions=mysql_num_rows($res_question);
		$totquestions=count($quesIdsArr);
            $att=0;
            echo "<span class=greybluetext>Candidate Answer Details</span>";
			echo "<table width=\"100%\" border=\"0\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\">
                  <tr><td class=\"ColumnHeader\"><table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"5\">";
            echo "<tr class=ColumnHeader align=\"center\">
                 <td>S.No</td>
                 <td>Answer</td>
                 <td>Updated Time </td>
                 <td>Question ID</td>
                 </tr>";             
            while($row=mysql_fetch_object($res_question)){
                if( $row->answer!='')
                    $att++;

                       echo "<tr bgcolor=\"#FFFFFF\" align=\"center\" class=\"greybluetextans\">
                       <td width='20%'>$row->display_order</td>
                       <td width='20%'>$row->answer</td>
                       <td width='20%'>$row->updated_time</td>
                       <td width='10%'>$row->question_id</td>
                      </tr>";
            }
            $unatt=$totquestions - $att;
            echo "<tr bgcolor=\"#FFFFFF\" align=\"left\" class=\"greybluetextans\"><td colspan=4><b>No.of Attempted Questions:$att</b></td></tr>";
            echo "<tr bgcolor=\"#FFFFFF\" align=\"left\" class=\"greybluetextans\"><td colspan=4><b>No.of Unattempted Questions:$unatt</b></td></tr>";
            echo "</table></td></tr></table><br/>";
            echo '</td></tr><tr><td>';


            echo "<span class=greybluetext>Records in Candidate Scores Table is : ".$num_candidate_scores."</span>";
			echo "<table width=\"100%\" border=\"0\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\">
                  <tr><td class=\"ColumnHeader\"><table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"5\">";
            echo "<tr class=ColumnHeader align=\"center\">
                 <td>Ex Code</td>
                 <td>Sub Code</td>
                 <td>Score</td>
                 <td>Result</td>
                 <td>Auto Submit</td>
                 <td>Date</td>
                 </tr>";             
            while($row=mysql_fetch_object($res_candidate_scores)){				
                       echo "<tr bgcolor=\"#FFFFFF\" align=\"center\" class=\"greybluetextans\">
                       <td width='20%'>$row->exam_code</td>
                       <td width='20%'>$row->subject_code</td>
                       <td width='20%'>$row->score</td>
                       <td width='10%'>$row->result</td>
                       <td width='10%'>$row->auto_submit</td>
                       <td width='10%'>$row->exam_date</td>
                      </tr>";
			}
            echo "</table></td></tr></table><br/>";
            echo '</td></tr><tr><td>';
    
		}		
		?>		
</TD>
</TR>
</TABLE>
<p></p>
<p></p>
<p></p>
</center>
</BODY>
</HTML>
