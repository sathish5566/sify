<?php
$admin_flag=0;
require_once "sessionchk.php";
require_once("dbconfig.php");
$tmp = $_REQUEST["hTmp"]?$_REQUEST["hTmp"]:"";
$okflag=0;
if ($tmp!=""){
	if (isset($_SESSION["randno"])){
		if($_SESSION["randno"]!=$_REQUEST["hTmp"]){
			$_SESSION["randno"]=$_REQUEST["hTmp"];
			$okflag=1;
		}
		else $okflag=2;
	}
	else{
//		session_register("randno");
		$_SESSION["randno"] = $_REQUEST["hTmp"];
		$okflag = 1;
	}
}
$mem_no= $_REQUEST["tMemNo"];
$val = $_REQUEST["val"]?$_REQUEST["val"]:0;
$old_subject_code = $_REQUEST["sSubjectCode"]?$_REQUEST["sSubjectCode"]:"";
//$new_time_slot =  $_REQUEST["hNewTime"]?$_REQUEST["hNewTime"]:"";
$old_exam_centre_code=$_REQUEST["examCentre"];
$old_iway_centre_code=$_REQUEST["iwayCentre"];
$new_time = $_REQUEST["newTime"];

if ($mem_no != ""){
	$sql_can  = "select distinct exam_code from iib_exam_candidate where membership_no='$mem_no'";
	$res_can=mysql_query($sql_can);
	$num_can = mysql_num_rows($res_can);
	if ($num_can == 0){
		$errmsg = "No entries found for this candidate";
	}
	elseif ($num_can!=1){
		$errmsg = "Inconsistant Database entries found";
	}
	else
		list($exam_code)=mysql_fetch_row($res_can);
}

if ($okflag == 1){
if ($val == 1){
	if ($new_time != "" and $mem_no != "" and $exam_code != "" and $old_iway_centre_code!=""){
//		$sql_chk = "select exam_time from iib_candidate_iway where membership_no ='$mem_no' and ";
		if($old_subject_code !='All')
			$sql_upd = "update iib_candidate_iway set centre_code='$old_iway_centre_code',exam_time='$new_time' where membership_no ='$mem_no' and exam_code = '$exam_code' and subject_code = '$old_subject_code'";
		else
			$sql_upd = "update iib_candidate_iway set centre_code='$old_iway_centre_code',exam_time='$new_time' where membership_no ='$mem_no' and exam_date >= current_date";
		$res_upd = mysql_query($sql_upd);
		//echo $sql_upd;
		if (!$res_upd)
			$errmsg="Sorry, Request Failed. Please try again";
		else
			$errmsg="Centre & Time successfuly changed.";
	}
	else
		$errmsg="Invalid Selection.";
}
}
else if ($okflag==2)
	$errmsg="Please do not refresh the page.";
?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='JavaScript' src="./includes/validations.js"></script>
<script language="javascript">
function getvalues(){
	frm = document.frmMember;
	if (event.keyCode == 13 && frm.tMemNo.value != ""){
		frm.action="change_centre_time.php";
		frm.submit();
	}
}

function changeTime(){
frm = document.frmMember;
frm.action="change_centre_time.php";
frm.submit();
}
function changeCentre(){
frm = document.frmMember;
frm.action="change_centre_time.php";
frm.submit();
}
function saveData(){
frm = document.frmMember;
if (frm.hAdminFlag.value == '2'){
	alert ("You do not have enough privillages to alter data");
	return;
}

if (frm.sSubjectCode.selectedIndex == 0){
	alert ("Please select the exam");
	frm.sSubjectCode.focus();
	return;
}

if (frm.examCentre.selectedIndex == 0){
	alert ("Please select the exam Centre");
	frm.examCentre.focus();
	return;
}
if (frm.iwayCentre.selectedIndex == 0){
	alert ("Please select the New Centre Code");
	frm.iwayCentre.focus();
	return;
}
if (frm.newTime.selectedIndex == 0){
	alert ("Please select the New Time");
	frm.newTime.focus();
	return;
}

tmp = Math.round(((1 - 0.5) +( ( (5+0.49999) - (1-0.5) ) * Math.random() ) )*100000);
frm.hTmp.value = tmp;
frm.action="change_centre_time.php?val=1";
frm.submit();
}
</script>
</HEAD>
<BODY leftMargin=0 topMargin=0 LeftMargin=0 marginheight=0 marginwidth=0>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
<!--	<TR><!-- Topnav Image -->
    <!--    <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR>   -->
	<tr><td width="780"><?include("includes/header.php");?></td></tr>
    <TR>
        
    </TR>
	<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>
    <TR>
    	<TD background=images/tile.jpg vAlign=top width=780 align=center>
    	<form name='frmMember' method="post" onsubmit="return false;">        
        <table width=780 cellpadding=0 cellspacing=5 border=0>
        	<tr>
				<td colspan=2 align="center"></b><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2>Modify Centre & Time</b></font><br><br></td>
            </tr>                
			<? if ($errmsg <> NULL ){ ?>
        	<tr>
				<td colspan=2 align="center" class="alertmsg"><? echo $errmsg; ?></td>
            </tr>                
			<? } ?>
			<tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Member Number : </td>
                <td align="left" valign=top><input type="text" class="textbox" size=30 name="tMemNo" value=<? echo "'".$mem_no."'"; ?> onkeypress="getvalues()"; maxlength=20></td>
			</tr>
			<tr>
               	<td align="right" width=390 class="greybluetext10" valign=top><a name=examcode></a> Exam Code : </td>
            	<td align="left" valign=top class="greybluetext10">
				<?
				if ($mem_no <> NULL){
					$sql_can  = "select exam_name from iib_exam where online = 'Y' and exam_code='$exam_code'";
					$res_can=mysql_query($sql_can);
					list($exam_name)=mysql_fetch_row($res_can);
					if ($exam_name != "")
						echo "<b>".$exam_code ." -> ".$exam_name."<b>";
				}	
				?>
				</select>
				</td>
			</tr>
			<tr>
               	<td align="right" width=390 class="greybluetext10" valign=top>Subjects Appearing : </td>
            	<td align="left" valign=top><select name="sSubjectCode" class="textbox" style="width:165;height:100" onchange="changeTime()">
				<option value=select selected>-Select-</option>
				<option value='All' <?if($old_subject_code == 'All')echo "selected"?>>-ALL-</option>
				<?
				if ($mem_no <> NULL){
					$sub_sql = "select b.subject_code,subject_name from iib_candidate_iway a, iib_exam_subjects b where membership_no='$mem_no' and a.exam_code='$exam_code' and  b.subject_code = a.subject_code and b.online = 'Y'";
					$sub_res=mysql_query($sub_sql);
					while (list($subject_code,$subject_name)=mysql_fetch_row($sub_res)){
						if ($old_subject_code==$subject_code)
							echo "<option value='$subject_code' selected>$subject_code --> $subject_name</option>";
						else
							echo "<option value='$subject_code'>$subject_code --> $subject_name</option>";
					}
				}
				?>
				</select>
				</td>
			</tr>
			
			<tr>
               	<td align="right" width=390 class="greybluetext10" valign=top><a name=examcode></a> Alloted Centre : </td>
            	<td align="left" valign=top class="greybluetext10">
				<?
				if ($mem_no <> NULL and $old_subject_code != ""){
					if($old_subject_code != 'All')
					{
						$sql_centre_code =	"select centre_code,exam_time from iib_candidate_iway where membership_no='$mem_no' and exam_code='$exam_code' and subject_code='$old_subject_code'";
						$res_centre_code=mysql_query($sql_centre_code);
						list($act_centre_code,$exam_time)=mysql_fetch_row($res_centre_code);
						echo "<b>".$act_centre_code."<b>";
					}
					else
					{
						echo "<b>Un Defined<b>";
						$exam_time="Un Defined";
					}
				}	
				?>
				</select>
				</td>
			</tr>
			<tr>
               	<td align="right" width=390 class="greybluetext10" valign=top><a name=examcode></a> Alloted Time : </td>
            	<td align="left" valign=top class="greybluetext10">
				<?
					echo "<b>".$exam_time."<b>";
				?>
				</select>
				</td>
			</tr>
			<tr>
               	<td align="right" width=390 class="greybluetext10" valign=top>Exam Centre : </td>
            	<td align="left" valign=top><select name="examCentre" class="textbox" style="width:165;height:100" onchange="changeCentre()">
				<option value=select selected>-Select-</option>
				<?
				if ($mem_no <> NULL){
					$exam_centre_sql = "select exam_centre_code,exam_centre_name from iib_exam_centres";
					$exam_centre_res=mysql_query($exam_centre_sql);
					while (list($exam_centre_code,$exam_centre_name)=mysql_fetch_row($exam_centre_res)){
						if ($old_exam_centre_code==$exam_centre_code)
							echo "<option value='$exam_centre_code' selected>$exam_centre_code --> $exam_centre_name</option>";
						else
							echo "<option value='$exam_centre_code'>$exam_centre_code --> $exam_centre_name</option>";
					}
				}
				?>
				</select>
				</td>
			</tr>
			<tr>
               	<td align="right" width=390 class="greybluetext10" valign=top>New Centre : </td>
            	<td align="left" valign=top><select name="iwayCentre" class="textbox" style="width:165;height:100" >
				<option value=select selected>-Select-</option>
				<?
				//echo "exam centre".$old_exam_centre_code;
				if ($old_exam_centre_code <> NULL){
					$iway_centre_sql = "select centre_code from iib_iway_details where exam_centre_code = '$old_exam_centre_code'";
					//echo $iway_centre_sql;
					$iway_centre_res=mysql_query($iway_centre_sql);
					while (list($centre_code)=mysql_fetch_row($iway_centre_res)){
						if ($old_iway_centre_code==$iway_centre_code)
							echo "<option value='$centre_code' selected>$centre_code</option>";
						else
							echo "<option value='$centre_code'>$centre_code</option>";
					}
				}
				?>
				</select>
				</td>
			</tr>

			<tr>
               	<td align="right" width=390 class="greybluetext10" valign=top>New Time : </td>
            	<td align="left" valign=top><select name="newTime" class="textbox" style="width:70;height:100" >
				<option value=select selected>-Select-</option>
				<?
				if ($mem_no <> NULL){
					$slots_sql = "select slot_time from iib_exam_slots";
					$slots_res=mysql_query($slots_sql);
					while (list($slot_time)=mysql_fetch_row($slots_res)){
							echo "<option value='$slot_time'>$slot_time</option>";
					}
				}
				?>
				</select>
				</td>
			</tr>                
			<tr>
				<td colspan=2 align=center><br>
				<input class="button" type="button" name="sub_save" value="Save" onclick="javascript:saveData()">&nbsp;&nbsp;
				<input class="button" type="reset" name="reset" value="Reset" >&nbsp;&nbsp;
				</td>
			</tr>
		<input type=hidden name=hCountVal value='<? echo $cnt_val; ?>' readonly>
		<input type=hidden name=hTmp>
		<input type=hidden name=hNewTime>
		<input type=hidden name=hAdminFlag value='<? echo $_SESSION["admin_type"];	?>'>
		</table>
        </form>
        </TD>
	</TR>
    <TR>
    	<?include("includes/footer.php");?>
    </TR>
</TABLE>
</FORM>
</center>
</BODY>
</HTML>
