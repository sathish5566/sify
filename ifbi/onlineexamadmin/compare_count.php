<?php
require("dbconfig.php");
?>
<html>
<head>
	<title><?=PAGE_TITLE ?></title>
	<style>
	.c1{
		font-family : Arial,Verdana;
		font-size : 10pt
	}
	</style>
</head>
<body>	
<?php
$sqlFirst = " SELECT membership_no FROM iib_candidate_scores WHERE exam_code='1' ".
				" AND exam_date between '2004-01-18 00:00:00' and '2004-01-25 23:59:59' group by membership_no ";
//print $sqlFirst;
$resFirst = mysql_query($sqlFirst);
if (mysql_num_rows($resFirst) > 0)
{
	$aFirst = array();
	while (list($membershipNo) = mysql_fetch_row($resFirst))
	{
		$aFirst[] = strtoupper($membershipNo);
	}
	$sqlFilterFirst = " SELECT membership_no FROM iib_candidate_iway WHERE exam_code='1' ".
							" AND exam_date  IN ('2004-02-01', '2004-02-08') group by membership_no ";
	//print $sqlFilterFirst;
	$resFilterFirst = mysql_query($sqlFilterFirst);
	if (mysql_num_rows($resFilterFirst) > 0)
	{
		$aFilterFirst = array();
		while (list($membershipNo) = mysql_fetch_row($resFilterFirst))
		{
			$aFilterFirst[] = strtoupper($membershipNo);
		}
		$aFilter = array_intersect($aFirst,$aFilterFirst);
		
		$sqlSecond = " SELECT membership_no FROM iib_candidate_scores WHERE exam_code='1' ".
							" AND exam_date between '2004-02-01 00:00:00' and '2004-02-08 23:59:59' group by membership_no ";
		//print $sqlSecond;	
		$resSecond = mysql_query($sqlSecond);
		if (mysql_num_rows($resSecond) > 0)
		{
			$aSecond = array();
			while (list($membershipNo) = mysql_fetch_row($resSecond))
			{
				$aSecond[] = strtoupper($membershipNo);
			}			
			$aDiff = array_diff($aFilter,$aSecond);			
			//print("Count : ".count($aDiff));
			if (count($aDiff) > 0)
			{
				print("<div align=center class='c1'><br>List of Candidates who appeared for the first and second but didn't appear for the third and fourth exams<br><br>Distinct Member Count : ".count($aDiff)."<br><br></div>\n");
				print("<table align=center width=60% border=1 cellspacing=1 cellpadding=1  class='c1'>");
				print("<tr><td  class='c1'>Membership No</td><td  class='c1'>Subject Code</td><td  class='c1'>Exam Date</td><td  class='c1'>Score</td><td  class='c1'>Result</td></tr>");
				foreach ($aDiff as $membershipNo)
				{
					$sql = " SELECT subject_code, DATE_FORMAT(exam_date,'%d-%m-%Y'), score, result FROM ".
							  " iib_candidate_scores WHERE membership_no='$membershipNo' ORDER BY subject_code ";
					$res = mysql_query($sql);
					if (mysql_num_rows($res) > 0)
					{					
						while (list($subjectCode,$examDate,$score,$result) = mysql_fetch_row($res))
						{
							print("<tr><td  class='c1'>$membershipNo</td><td  class='c1'>$subjectCode</td><td  class='c1'>$examDate</td><td  class='c1'>$score</td><td  class='c1'>$result</td></tr>");
						}
					}
				} // end of foreach
				print("</table>");
			}
		} // end of second
	} // end of filter
} // end of first
?>
</body>
</html>