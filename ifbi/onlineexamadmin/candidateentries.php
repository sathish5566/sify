<?
$admin_flag=2;
//Get to know the status of a candidate on the date of exam.
require("dbconfig_slave_123.php");
require("sessionchk.php");
$currentDate = date('Y-m-d');
//$currentDate = "2004-01-25";
if(isset($_REQUEST)){
	 extract($_REQUEST);
}
$cname=$_REQUEST['cname'];
if ($hMemNo != "" and $hExamCode !="" and $hSubjectCode != "" and $hCentreCode != ""){
	$sqlSelect = "SELECT exam_date,exam_time,centre_code FROM iib_candidate_iway WHERE membership_no = '$hMemNo' and exam_code='$hExamCode' and subject_code='$hSubjectCode' ";
	$resSelect = mysql_query($sqlSelect);
	if (mysql_num_rows($resSelect) > 0)
	{
		list($examDate,$examTime,$oldCentreCode) = mysql_fetch_row($resSelect);
		$sql_update = "update iib_candidate_iway set centre_code = '$hCentreCode' where membership_no = '$hMemNo' and exam_code='$hExamCode' and subject_code='$hSubjectCode'";
		require_once("dbconfig.php");
		mysql_query($sql_update);
		$nAffectedRows = mysql_affected_rows($connect);
		if ($nAffectedRows > 0)
		{	
			$err_msg = "Centre Code changed";
			$slash_reason = addslashes($hReason);
			$sqlLogs = "INSERT INTO iib_centre_change_logs (membership_no,exam_code,subject_code,exam_date,exam_time,old_centre_code,new_centre_code,reason) VALUES ('$hMemNo','$hExamCode','$hSubjectCode','$examDate','$examTime','$oldCentreCode','$hCentreCode','$slash_reason') ";
			//print $sqlLogs;
			mysql_query($sqlLogs);
		}
		else
		{
			$err_msg = "Centre Code not changed";
		}
	}
}
if($cname!=""){
	$cname = trim($cname);
	//Select the entry from candidate table.
	$sql_candidate = "select name,password,exam_centre_code,city,pin_code,institution_code,institution_name from iib_candidate where membership_no='".$cname."'";
	$res_candidate = @mysql_query($sql_candidate);
	$num_iib_candidate = @mysql_num_rows($res_candidate);

	//Select the entry from exam_candidate table.
	$sql_exam_candidate = "select membership_no,exam_code,subject_code,medium_code,institution_code,alloted from iib_exam_candidate where membership_no='".$cname."'";
		
	$res_exam_candidate = @mysql_query($sql_exam_candidate);
	$num_exam_candidate = @mysql_num_rows($res_exam_candidate);

	//Select the entry from candidate_iway table.
	$sql_candidate_iway = "select membership_no,centre_code,a.exam_code,a.subject_code,exam_date,exam_time,exam_name,subject_name from iib_candidate_iway a,iib_exam b,iib_exam_subjects c where membership_no='".$cname."' and b.exam_code=a.exam_code and c.subject_code=a.subject_code";
				
	$res_candidate_iway = @mysql_query($sql_candidate_iway);
	$num_candidate_iway = @mysql_num_rows($res_candidate_iway);

	//Select the entry from question_paper table.
	$sql_question_paper = "select question_paper_no,exam_code,subject_code,assigned,complete,membership_no,medium_code from iib_question_paper where membership_no ='".$cname."'";
	$res_question_paper = @mysql_query($sql_question_paper);
	$num_question_paper = @mysql_num_rows($res_question_paper);

	//Select the entry from iib_candidate_track table.
	$sql_candidate_track = "select ta_login,membership_no,host_ip,session_id,updated_time from iib_candidate_tracking where membership_no ='".$cname."' order by updated_time";
    $res_candidate_track = @mysql_query($sql_candidate_track);
	$num_candidate_track = @mysql_num_rows($res_candidate_track);
	
	//Select the entry from iib_candidate_test table.
	$sql_candidate_test = "select test_id,membership_no,exam_code,subject_code,question_paper_no,ta_override,test_status,start_time,last_updated_time,end_time,total_time,time_taken,time_left,browser_status,time_extended from iib_candidate_test where membership_no ='".$cname."'";
    $res_candidate_test = @mysql_query($sql_candidate_test);
	$num_candidate_test = @mysql_num_rows($res_candidate_test);


	//Select the entry from iib_candidate_scores table.	
	$sql_candidate_scores = "select membership_no,exam_code,subject_code,score,exam_date,time_taken,result,auto_submit from iib_candidate_scores where membership_no='".$cname."'";
	$res_candidate_scores = @mysql_query($sql_candidate_scores);
	$num_candidate_scores = @mysql_num_rows($res_candidate_scores);	
}
?>
<html>
<head>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
FONT-FAMILY: Arial, Verdana 
}
.textblk11     { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; text-decoration: none}
</STYLE>
<script language='JavaScript' src="./includes/validations.js"></script>
<script language='javascript'>
function val()
{
	var f=document.fname;
	c = f.cname.value;
	if(c=='')
	{
		alert('please enter a name!');
		return;
	}
	f.submit();
}
function resetPass(no)
{
	if(confirm("Are You sure to change the Password")) {
	var winname = new String('ResetPass');
	window.name='resetPass';	
	var w = window.open('resetCandidatePass.php?memno='+no,winname,config="width=250,height=100,status=no,scrollbars=yes,titlebar=yes,resizable=no,menubar=no,toolbar=no")
	}
	return false;
}
function submitpage(memno,exam_date,exam_time,i){
	frm = document.fname;
	if (frm.hAdminFlag.value == '2'){
		alert ("You do not have enough privillages to alter data");
		return;
	}
	if (frm["tCentre"+i].value == ""){
		alert ("Centre code cannot be empty");
		frm["tCentre"+i].focus();
		return;
	}
	if (frm["sReason"+i].selectedIndex == 0){
		alert ("Reason cannot be empty");
		frm["sReason"+i].focus();
		return;
	}
	if ((frm["sReason"+i][frm["sReason"+i].selectedIndex].value == "Others") && (trim(frm["tOthers"+i].value) == "")){
		alert ("Please mention the reason in others");
		frm["tOthers"+i].focus();
		return;
	}
	frm.hMemNo.value=memno;
	frm.hExamCode.value=exam_date;
	frm.hSubjectCode.value=exam_time;
	if (trim(frm["tOthers"+i].value) == "")
	{
		frm.hReason.value=frm["sReason"+i][frm["sReason"+i].selectedIndex].value;	
	}
	else		
	{
		if ((frm["sReason"+i].selectedIndex > 0) && (frm["sReason"+i].selectedIndex < 4))
		{
			frm.hReason.value = 	frm["sReason"+i][frm["sReason"+i].selectedIndex].value;
		}
		if (frm.hReason.value == "")
		{
			frm.hReason.value=frm["tOthers"+i].value;
		}
		else
		{
			frm.hReason.value=frm.hReason.value+", "+frm["tOthers"+i].value;
		}
	}
	frm.hCentreCode.value=frm["tCentre"+i].value;
	frm.action = "candidateentries.php";
	frm.submit();
}

// background=images/tile.jpg 
</script>
</head>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0>
<center>
<!-- <TABLE border=0 cellPadding=0 cellSpacing=0 width=780 background=images/tile.jpg >   -->
 <TABLE border=0 cellPadding=0 cellSpacing=0 width=780> 
	<tr><td width="780"><?php include("includes/header.php");?></td></tr>
    <TR>    	
    </TR>
	<tr>
		<Td class="greybluetext10"  background=images/tile.jpg><? include("sify_admin_menu.php")?></Td>	
	</tr>	
    <TR>
    	<TD background=images/tile.jpg vAlign=top width=780 align=center>
		<form name='fname' method='post'>
		<br><br>
		<table align=center>
			<tr class='textblk11'>
				<td>Enter the Candidate Number : </td>
				<td><input type='text' name='cname' class='textbox' value='<? echo $cname; ?>'></td>
			</tr>
			<tr>
				<td colspan=2 align=center><input type='button' class='button' name='ok' value='Get Status' onclick='javascript:val();'><br><br></td>
			</tr>
			<? if ($err_msg != ""){?>
			<tr>
				<td colspan=2 align=center class="alertmsg"><? echo $err_msg;?></td>
			</tr>
			<? } ?>						
		</table>
		<?
		if($cname!=""){
			echo "<font class='textblk11'>Records in Candidate Table is : ".$num_iib_candidate;
			echo "<table border=0 align=center width=100% cellspacing=0>";
			while($row=mysql_fetch_row($res_candidate)){
				echo "<tr class='textblk11' style='font-weight: bold;'><td align=center colspan=2><input type=button class='button' value='Reset Password' onclick ='javascript:resetPass(\"$cname\")'></td></tr>";
				echo "<tr class='textblk11' style='font-weight: bold;'><td align=right width=50%>name : </td><td align=left>$row[0]<br></td></tr>";
				//echo "<tr class='textblk11' style='font-weight: bold;'><td align=right>password : </td><td align=left>$row[1]<br></td></tr>";
				echo "<tr class='textblk11' style='font-weight: bold;'><td align=right>exam_centre_code : </td><td align=left>$row[2]<br></td></tr>";
				echo "<tr class='textblk11' style='font-weight: bold;'><td align=right>city : </td><td align=left>$row[3]<br></td></tr>";
				echo "<tr class='textblk11' style='font-weight: bold;'><td align=right>pin_code : </td><td align=left>$row[4]<br></td></tr>";
				echo "<tr class='textblk11' style='font-weight: bold;'><td align=right>institution_code : </td><td align=left>$row[5]<br></td></tr>";
				echo "<tr class='textblk11' style='font-weight: bold;'><td align=right>institution_name : <br></td><td align=left>$row[6]</td></tr>";
			}
			echo "</table><br>";

			echo "Records in Exam Candidate Table is : ".$num_exam_candidate;
			echo "<table align=center width=100% border=1 cellspacing=0>";
			echo "<tr class='textblk11' style='font-weight: bold;'><td>membership_no</td><td>exam_code</td><td>subject_code</td><td>medium_code</td><td>institution_code</td><td>alloted</td></tr>";
			while($row=mysql_fetch_row($res_exam_candidate)){
				echo "<tr class='textblk11'><td>$row[0]<br></td><td>$row[1]<br></td><td>$row[2]<br></td><td>$row[3]<br></td><td>$row[4]<br></td><td>$row[5]<br></td></tr>";
			}
			echo "</table><br>";
			
			echo "Records in Candidate Iway Table is : ".$num_candidate_iway;
			echo "<table align=center width=100% border=1 cellspacing=0>";
			echo "<tr class='textblk11' style='font-weight: bold;'><td>membership_no</td><td>centre_code</td><td>exam_code</td><td>subject_code</td><td>exam_date</td><td>exam_time</td></tr>";
			$i=0;
			while($row=mysql_fetch_row($res_candidate_iway)){
				$i++;
				echo "<tr class='textblk11'><td>$row[0]<br></td><td><b>$row[1]</b><br>";
				if ($row[4]== $currentDate && $_SESSION["admin_type"] != '2'){
					echo "Centre &nbsp;: <input type=text value='' name='tCentre$i' style='width:75px' class='textbox' maxlength=7>";
					 ?>
					 <br>Reason : <select name='sReason<? echo $i; ?>' class='textbox' style='width:150;'>
					 <option value=0>--Select--</option>
					 <?
					 $sql_reason = "select reason from iib_change_reason";
					 $res_reason = mysql_query($sql_reason);
					 while(list($reason)=mysql_fetch_row($res_reason)){
						$sel="";
						 if ($reason==$sReason)
						 	$sel = "selected";
						 echo "<option value='$reason' $sel>$reason</option>";
					 }
					  ?>
					 </select>
					 <?
					echo "<br>Others&nbsp;&nbsp;: <input type=text value='' name='tOthers$i' style='width:75px' class='textbox' maxlength=40>&nbsp;&nbsp;&nbsp;<input type=button value='Change' name='bButton$i' onclick='submitpage(\"$row[0]\",\"$row[2]\",\"$row[3]\",\"$i\")' class='button'>";
				}
				echo "</td><td width=100>$row[2] -> $row[6]</td><td width=150>$row[3] -> $row[7]</td><td>$row[4]</td><td>$row[5]</td></tr>";
			}
			echo "</table><br>";
			
			echo "Records in Question Paper Table is : ".$num_question_paper;
			echo "<table align=center width=780 border=1 cellspacing=0>";
			echo "<tr class='textblk11' style='font-weight: bold;'><td>question_paper_no</td><td>exam_code</td><td>subject_code</td><td>assigned</td><td>complete</td><td>membership_no</td><td>medium_code</td></tr>";
			while($row=mysql_fetch_row($res_question_paper)){
				echo "<tr class='textblk11'><td>$row[0]<br></td><td>$row[1]<br></td><td>$row[2]<br></td><td>$row[3]<br></td><td>$row[4]<br></td><td>$row[5]<br></td><td>$row[6]<br></td></tr>";
			}
			echo "</table><br>";
			
			echo "Records in  Candidate Track Table is : ".$num_candidate_track;
			echo "<table align=center width=100% border=1 cellspacing=0>";
			echo "<tr class='textblk11' style='font-weight: bold;'><td>Ta login</td><td>Membership No</td><td>host_ip</td><td>session id</td><td>LoggedTime</td></tr>";
			while($row=mysql_fetch_row($res_candidate_track)){
				echo "<tr class='textblk11'><td>$row[0]<br></td><td>$row[1]<br></td><td>$row[2]<br></td><td>$row[3]<br></td><td>$row[4]<br></td></tr>";			
			}			
			echo "</table><br>";
	
				
			echo "Records in  Candidate Test Table is : ".$num_candidate_test;
			echo "<table align=center width=100% border=1 cellspacing=0>";
			echo "<tr class='textblk11' style='font-weight: bold;'><td>test id</td><td>Membership No</td><td>exam code</td><td>subject code</td><td>question paper no</td><td>ta override</td><td>test status</td><td>start time</td> <td>last updated time</td><td>end time</td><td>total time</td><td>time taken</td><td>time left</td><td>browser status</td><td>Extended Time</td></tr>";
			while($row=mysql_fetch_row($res_candidate_test)){
				echo "<tr class='textblk11'><td>$row[0]<br></td><td>$row[1]<br></td><td>$row[2]<br></td><td>$row[3]<br></td><td>$row[4]<br></td><td>$row[5]<br></td><td>$row[6]<br></td><td>$row[7]<br></td><td>$row[8]<br></td><td>$row[9]<br></td><td>$row[10]<br></td><td>$row[11]<br></td><td>$row[12]<br></td><td>$row[13]<br></td><td>$row[14]<br></td></tr>";
			$QPno=$row[4];
			}			
			echo "</table><br>";
			
			echo "Records in  Candidate Scores Table is : ".$num_candidate_scores;
			echo "<table align=center width=100% border=1 cellspacing=0>";
			echo "<tr class='textblk11' style='font-weight: bold;'><td>membership_no</td><td>exam_code</td><td>subject_code</td><td>score</td><td>exam_date</td><td>time_taken</td><td>result</td><td>Auto submit</td></tr>";
			while($row=mysql_fetch_row($res_candidate_scores)){
				echo "<tr class='textblk11'><td>$row[0]<br></td><td>$row[1]<br></td><td>$row[2]<br></td><td>$row[3]<br></td><td>$row[4]<br></td><td>$row[5]<br></td><td>$row[6]<br></td><td>$row[7]</td></tr>";
			}
			echo "</table><br>";			
			//$sql_question = "select display_order,updated_time,question_id  from iib_question_paper_details where question_paper_no='".$QPno."' and updated_time is not null order by updated_time desc limit 1";
			$sql_question = "select display_order, updatedtime,question_id from iib_response where question_paper_no='".$QPno."' and id in ( select  max(id) from iib_response where question_paper_no ='$QPno' group by question_id) order by updatedtime desc limit 1";
			//echo $sql_question;
			$res_question = @mysql_query($sql_question);

			echo "Candidate latest answer Details :";
			echo "<table align=center width=100% border=1 cellspacing=0>";
			echo "<tr class='textblk11' style='font-weight: bold;'><td>Display order(S.No)</td><td>Updated Time</td><td>Question ID</td></tr>";
			while($row=mysql_fetch_row($res_question)){
				echo "<tr class='textblk11'><td>$row[0]<br></td><td>$row[1]<br></td><td>$row[2]<br></td></tr>";
			}
			echo "</table><br>";
			
		}		
		?>	
		<input type=hidden name=hMemNo>
		<input type=hidden name=hExamCode>
		<input type=hidden name=hSubjectCode>
		<input type=hidden name=hCentreCode>
		<input type=hidden name=hReason>
		<input type=hidden name=hAdminFlag value='<? echo $_SESSION["admin_type"];	?>'>		
    	<?php include("includes/footer.php");?>		
</FORM></TD>
	</TR>
</TABLE>
</center>
</BODY>
</HTML>
