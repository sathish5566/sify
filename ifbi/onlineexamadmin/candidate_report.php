<?php
$admin_flag=0;
require_once "sessionchk.php";
require_once "dbconfig.php";
require_once("constants.inc");

$examCode = $_POST['exam_code'];
$membershipNo = $_POST['membership_no'];
$examName = $_POST['hid_exam_name'];
$subjectName = $_POST['hid_subject_name'];
$hidExamCode = $_POST['hid_exam_code'];
$hidSubjectCode = $_POST['hid_subject_code'];

$errormsg ="";
$msg = "";
$commonDebug = false;

$changed = $_POST['changed'];

 $sqlExams = " SELECT DISTINCT e.exam_code, e.exam_name FROM iib_exam e, iib_candidate_scores s WHERE s.exam_code=e.exam_code ".
					" AND online='Y' AND membership_no='$membershipNo' ";

if ($commonDebug)
{					
	print $sqlExams;
}
$resExams = @mysql_query($sqlExams);

if ($examCode != "")
{
	$sqlSubjects = " SELECT DISTINCT e.subject_code, e.subject_name FROM iib_exam_subjects e, iib_candidate_scores s WHERE e.subject_code=s.subject_code ".
						   " AND online='Y' AND e.exam_code='$examCode' AND membership_no='$membershipNo' ";
	if ($commonDebug)
	{					
		print $sqlSubjects;
	}
	$resSubjects = @mysql_query($sqlSubjects);
	$nSubjects = mysql_num_rows($resSubjects);	
}
$subjectCode = $_POST['subject_code'];
if ($changed == 'N')
{
	//question paper
	$sqlQuestions = "SELECT question_paper_no FROM iib_candidate_test WHERE exam_code='$examCode' AND subject_code='$subjectCode' ".
		" AND test_status='C' ";
	
	if ($commonDebug)
	{
		print("\n".$sqlQuestions);
	}
	$resQuestions = @mysql_query($sqlQuestions);
	list($questionPaperNo) = @mysql_fetch_row($resQuestions);
	if ($questionPaperNo == "")
	{
		$errormsg = "Candidate has not completed taking the exam";
	}
}
?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<script language='JavaScript' src='./includes/validations.js'></script>
<script language='JavaScript'>
function getSubjects()
{
	document.frmReports.changed.value ='Y';
	document.frmReports.action = '<?=$_SERVER['PHP_SELF'] ?>';
	document.frmReports.target = '_self';
	document.frmReports.submit();
}

function validate()
{
	frm = document.frmReports;
	if (frm.membership_no.value == "")
	{
		alert('Please enter the Membership No. ');
		frm.membership_no.focus();
		return false;
	}
	if (frm.exam_code.selectedIndex == 0)
	{
		alert('Please select the Exam ');
		frm.exam_code.focus();
		return false;
	}
	if (frm.subject_code.selectedIndex == 0)
	{
		alert('Please select the Subject ');
		frm.subject_code.focus();
		return false;
	}
	var ename = frm.exam_code.options[frm.exam_code.selectedIndex].text;
	var sname = frm.subject_code.options[frm.subject_code.selectedIndex].text;
	frm.hid_exam_code.value = frm.exam_code.options[frm.exam_code.selectedIndex].value;
	frm.hid_subject_code.value = frm.subject_code.options[frm.subject_code.selectedIndex].value;
	frm.hid_exam_name.value = ename;
	frm.hid_subject_name.value = sname;
	frm.changed.value = 'N';	
	document.frmReports.target='_self';
	frm.submit();
}

function submitForm()
{
	frm = document.frmReports;
	frm.changed.value = 'Y';
	frm.action = '<?=$_SERVER['PHP_SELF'] ?>'
	frm.target = '_self';
    if (window.event.keyCode == 13)
    {
        frm.submit();
    }
}

</script>
</HEAD>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0 <?php if ($changed != 'N') print("onLoad='document.frmReports.membership_no.focus()'"); ?>>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
<!--	<TR><!-- Topnav Image -->
   <!-- 	<TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR>	   -->
	<tr><td width="780"><?include("includes/header.php");?></td></tr>
	<TR>
    	
	</TR>
	<tr>
		<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>	
	<TR>
	    <TD background=images/tile.jpg vAlign=top width=780 align=center>
     	<form name="frmReports" method="post" onLoad='document.frmReports.membership_no.focus()'>
     	<input type=hidden name='changed' value=''>
     	<input type=hidden name='memno' value='<?=$membershipNo ?>'>     	    	    	
		<table width=780 cellpadding=0 cellspacing=5 border=0>
			<tr>
				<td colspan=4 align="center" class=graybluetext10><b><font face="Verdana, Arial, Helvetica, sans-serif" size=2>Candidate Report</font></b><br><br></td>
			</tr>	
			<tr>				
				<td colspan=3 valign=top class=greybluetext10 align=right><b>Fill the Membership No. and press Enter &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>				
				<td width=25%><br></td>
			</tr>	
			<tr>
				<td width=15%><br></td>
				<td valign=top class=greybluetext10 width=25% align=right><b>Membership No.&nbsp;&nbsp;: </b></td>
				<td valign=top class=greybluetext10 width=35% align=left>
					<input type='text' name='membership_no'  class=textbox size=27 value='<?=$membershipNo ?>' onKeyPress='javascript:submitForm()' >
				</td>
				<td width=25%><br></td>
			</tr>	
			<tr>
				<td width=15%><br></td>
				<td valign=top class=greybluetext10 width=25% align=right><b>Exam&nbsp;&nbsp;: </b></td>
				<td valign=top class=greybluetext10 width=35% align=left>
					<select name="exam_code" class="textbox" style={width:150px} onChange='javascript:getSubjects()'>
						<option value=''>--Select--</option>
						<?php						
						if (mysql_num_rows($resExams) > 0)
						{
							while (list($eCode, $eName) = mysql_fetch_row($resExams))
							{
								print("<option value='$eCode'");
								if ($examCode == $eCode)
								{
									print(" selected ");
								}
								print(">$eName</option>\n");
							}
						}						
						?>
					</select>
				</td>
				<input type=hidden name='hid_exam_code' value='<?=$hidExamCode ?>'>
				<input type=hidden name='hid_exam_name' value='<?=$examName ?>'>
				<td width=25%><br></td>
			</tr>
			<tr>
				<td width=15%><br></td>
				<td valign=top class=greybluetext10 width=25% align=right><b>Subject&nbsp;&nbsp;: </b></td>
				<td valign=top class=greybluetext10 width=35% align=left>
					<select name="subject_code" class="textbox" style={width:150px}>
						<option value=''>--Select--</option>
						<?php						
						if ($nSubjects > 0)
						{
							while (list($sCode, $sName) = mysql_fetch_row($resSubjects))
							{
								print("<option value='$sCode'");
								if ($subjectCode == $sCode)
								{
									print(" selected ");
								}
								print(">$sName</option>\n");
							}
						}						
						?>
					</select>
				</td>
				<input type=hidden name='hid_subject_code' value='<?=$hidSubjectCode ?>'>
				<input type=hidden name='hid_subject_name' value='<?=$subjectName ?>'>
				<td width=25%><br></td>
			</tr>
			
			<tr>
				<td colspan=4 align="center"><input type=button name="sub_gen" value="Generate" class=button onClick='javascript:validate()'></td>
			</tr>			
		</table>		
		<?php
     	if ($errormsg != "")
     	{
	     	print("<div align=center class=greybluetext10><b>$errormsg</b><br><br></div>");
     	}     		
		?>		
		</TD>
	</TR>
	<TR>
		<?include("includes/footer.php");?>
	</TR>
</TABLE>
</FORM>
</center>
<?php
if (($errormsg == "") && ($changed == 'N'))
{
	
echo <<<EOD
<script language="javascript">	
	var newWidth = 640;
    var newHeight = screen.availHeight-55;
    var now = new Date();
	var hours = now.getHours();
	var minutes = now.getMinutes();
	var seconds = now.getSeconds();
	var winname = new String('ReportPopUp'+hours+minutes+seconds);
	frm = eval('document.frmReports');
	frm.exam_code.length=0;
	var opt = new Option('--Select--', '');
  	var sel = frm.exam_code;
  	sel.options[0] = opt;
  	
	frm.subject_code.length = 0;
	opt = new Option('--Select--', '');
    sel = frm.subject_code;
  	sel.options[0] = opt;
  	frm.membership_no.value = '';
	var w = window.open('loadingreport.html',winname,config="width="+newWidth+" height="+newHeight+",top=2,left=2,status=yes,scrollbars=yes,titlebar=no,resizable=no,menubar=no,toolbar=no");	
	document.frmReports.action='display_candidate_report.php';
	document.frmReports.target=winname;
	document.frmReports.submit();
	w.focus();	
	document.frmReports.target='_self';
	document.frmReports.action='';
</script>
EOD;
exit;
}
?>
</BODY>
</HTML>
