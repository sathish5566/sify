<?php
/****************************************************
* Application Name            :  IIB& IBPS
* Module Name                 :  Tiem change option
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :
* Tables used for only selects:  -
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              :  -
* Document/Reference Material :
* Created By                  : BalaMurugan.S
* Created ON                  : 25-06-2010
* Last Modified By            :  
* Last Modified Date          :  
* Description                 : Bulk time change 
*****************************************************/
@session_start();
$admin_flag=0;
require("dbconfig_slave_123.php");
require("sessionchk.php");
set_time_limit(0);
if(isset($_GET['exam_date']) && isset($_GET['exam_time']) && isset($_GET['rpt_type']) && isset($_GET['tot']))
{
	$examdate=mysql_escape_string($_GET['exam_date']);	
	$examtime=mysql_escape_string($_GET['exam_time']);
	$rpttype=mysql_escape_string($_GET['rpt_type']);
	$tot=mysql_escape_string($_GET['tot']);
	$aDates = explode("-", $examdate);
	$dispDate = $aDates[2]."-".$aDates[1]."-".$aDates[0];
	if($tot==0)
		$subqry="and a.exam_time='$examtime'";
	else
	  	$subqry='';
	
	if($rpttype == 'alloted'){
		$title="Alloted Candidate Details";
		$sql = " SELECT a.membership_no,a.centre_code,a.exam_date,a.exam_time FROM iib_candidate_iway a WHERE a.exam_date = '$examdate' $subqry ";
	}else if($rpttype == 'attended'){
		$title="Attended Candidate Details";	
		$sql = " select a.membership_no,a.centre_code,a.exam_date,a.exam_time from iib_candidate_iway a, iib_candidate_test b where a.membership_no = b.membership_no and a.exam_code = b.exam_code and a.subject_code = b.subject_code and a.exam_date = '$examdate' and b.current_session='Y'  $subqry order by a.centre_code ";
	}else if($rpttype == 'unattended'){
		$title="Un Attended Candidate Details";	
		$sql = "select a.membership_no,a.centre_code,a.exam_date,a.exam_time from iib_candidate_iway a left join iib_candidate_test b on a.membership_no=b.membership_no and a.exam_code=b.exam_code and a.subject_code=b.subject_code where b.membership_no is null and a.exam_date='$examdate' $subqry order by a.centre_code";
	}else if($rpttype == 'incomplete'){
		$title = "Incomplete Candidate Details";	
		$sql = " select a.membership_no,a.centre_code,a.exam_date,a.exam_time from iib_candidate_iway a, iib_candidate_test b where a.membership_no = b.membership_no and b.test_status='IC' and current_session='Y' and a.exam_code = b.exam_code and a.subject_code = b.subject_code and a.exam_date = '$examdate'  $subqry order by a.centre_code ";
	}else if($rpttype == 'complete'){
		$title="Completed Candidate Details";	
		 $sql = " select a.membership_no,a.centre_code,a.exam_date,a.exam_time from iib_candidate_iway a, iib_candidate_scores b where a.membership_no = b.membership_no and a.exam_code = b.exam_code and a.subject_code = b.subject_code and a.exam_date = '$examdate' $subqry order by a.centre_code";				
	}						
}

$res = mysql_query($sql);
$ct= mysql_num_rows($res);

			
?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>
BODY {
	FONT-FAMILY: Arial, Verdana	
}
.textblk11  { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; text-decoration: none}
</STYLE>
<script language="javascript" type="text/javascript">
function win_open(a,b)
{
	window.open('tadetails.php?id='+a+'&cid='+b,'win1','left=10,top=10,width=600,height=700,toolbar=no,menubar=no,addressbar=no,resizable=1');		
}
</script>
</head>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
	<tr><td width="780"><?php include("includes/header.php");?></td></tr>
	<TR>    	
	</TR>
	<tr>
	<Td class="greybluetext10"  background=images/tile.jpg><? //include("admin_menu.php") ?></Td>	
	</tr>	
	<TR>
	    <TD background=images/tile.jpg vAlign=top width=780 align=center>
	    <form name='frmtimchange' id="frmtimchange" method='post'>
<table width=100% border=0 cellspacing=0 cellpadding=5>
	<tr><td width="100%" align=center><div align=center><strong>Attendance Linked Report </strong></div></td></tr>
	<tr>
	  <td align=center valign="top" class="arial14a" ><div align="left" class="arial11a"><strong>Report Description : <?PHP echo ucwords($title)?> </strong></div></td>
	  </tr>
	<tr>
	  <td align=center valign="top" class="arial14a" ><div align="left" class="arial11a"><strong>Exam Date : <?PHP echo $dispDate?> </strong></div></td>
	  </tr>
	<tr>
	  <td align=center valign="top" class="arial14a">
	 <?PHP if($tot==0)
			  echo "<div align=\"left\" class=\"arial11a\"><strong>Exam Time : $examtime </strong></div>";
			?> 
		 </td>
	  </tr>
	<tr>
	  <td align=center valign="top">&nbsp;</td>
	  </tr>
	<?PHP 
	if($ct > 0)
	{		
		echo "<tr><td colspan=7 align=center class=alertmsg><div id='idafrows'>No.Of Records:$ct</div></td></tr>";
	?>	
	<tr>
	  <td align=center>
	  <div id="content" style="visibility:visible;display:block">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="24" align="left" valign="top" bgcolor="#000066">
		  <table width="100%" border="1" align="left" cellpadding="5" cellspacing="0" bordercolor="#000066" bgcolor="#FFFFFF" style="border-left-width:thin">
            <tr>
              <td width="6%" bgcolor="#0080FF" class="whitecolor"><div align="center"><strong>S.No</strong></div></td>
              <td width="22%" bgcolor="#0080FF" class="whitecolor"><div align="center"><strong>Membership No</strong></div></td>
              <td width="25%" bgcolor="#0080FF" class="whitecolor"><div align="center"><strong>Centre Code </strong></div></td>
              <td width="24%" bgcolor="#0080FF" class="whitecolor"><div align="center"><strong>Exam Date</strong></div></td>
              <td width="23%" bgcolor="#0080FF" class="whitecolor"><div align="center"><strong>Exam Time</strong></div></td>
            </tr>
            <?PHP	$sno=1;														
					while($rs = mysql_fetch_array($res)){            		         
					   $memno = $rs['membership_no'];
						$ccode = $rs['centre_code'];
						$examdate = $rs['exam_date'];						
						$examtime = $rs['exam_time'];
						$iw_iway="iwfr_".$ccode;										
						echo '<tr>';							 
						echo "<td class='greybluetext10'>".$sno++."</td>
							  <td class='greybluetext10'><a href='candidateentries.php?cname=$memno' target='_blank'>$memno</a></td>
							  <td class='greybluetext10'><a href=javascript:win_open('$iw_iway','$ccode')>$ccode</td>
							  <td class='greybluetext10'>$examdate</td>
							  <td class='greybluetext10'>$examtime</td>
						</tr>";
		  			}
			 
			  ?>
		  </table></td>
        </tr>
      </table>
	  </div>	  </td>
	  </tr>
	<?PHP }
?>
</table>
</form>
</TD>
</TR>
<TR>
<?php include("includes/footer.php");?>
</TR>
</TABLE>
</center>
</BODY>
</HTML>
