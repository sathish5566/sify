<?php
/****************************************************
* Application Name            :  IBPS
* Module Name                 :  FM list upload
* Revision Number             :  1
* Revision Date               :
* Table(s)  modified          : iib_temp,iib_franchisee,iib_franchisee_iway,iib_franchisee_map(In fmupload.php)
* Tables used for only selects: iib_franchisee_iway,iib_franchisee,iib_iway_details
* Page Navigation			  :	fmupload.php
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              : Information regarding the upload
* Document/Reference Material :
* Created By                  : Anil Kumar.A
* Created On 				  : 6/Apr/2006 
* Last Modified By            :  
* Last Modified Date          : 
* Description                 :  Uploading the Franchisee Manager list into the database
*****************************************************/
require_once "sessionchk.php";
require_once("dbconfig.php");
$upd=isset($_REQUEST["upd"]) ? $_REQUEST["upd"] : '';
$msg=isset($_REQUEST["msg"]) ? $_REQUEST["msg"] : '';
$dup=isset($_REQUEST["dup"]) ? $_REQUEST["dup"] : '';
$act_less=isset($_REQUEST["actls"]) ? $_REQUEST["actls"] : '';
$dup_table=isset($_REQUEST["duptbl"]) ? $_REQUEST["duptbl"] : '';
$norecord=isset($_REQUEST["norec"]) ? $_REQUEST["norec"] : '';
$filename=isset($_REQUEST["fn"]) ? $_REQUEST["fn"] : '';
$vali_code=isset($_REQUEST["vali_code"]) ? $_REQUEST["vali_code"] : '';

// Set the emsg according to the type got from fmupload.php
$emsg[1]="File has been uploaded into the database";
$emsg[2]="File not uploaded,There are no contents in the file";
$emsg[3]="Only Text file should be uploaded";
$emsg[4]="Text file contains duplicate values for Centre Code - <font color=red>".$dup."</font>";
$emsg[5]="Duplicate Entries in the IWAY Details table for Centre Code - ".$dup_table;
$emsg[6]="No Entries for <font color=red>".$norecord." </font>in the IWAY Details table  <br>Please check and reupload the details again";
$emsg[7]="Validations Problem in the line bearing centre code <font color=red>".$vali_code."</font> <br>Please check and upload the details again";
$emsg[8]="Only 4 fields are allowed";
$emsg[9]="Actual seats for the centre code  <font color=red>".$act_less."</font> does not match with Database Entries.<br>Please Check actual seats for all the centres and upload the details again";
$emsg[10]="There are less no.of records in the file compared to that of the Database Entries.<br>Please check and upload the details again";

?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='JavaScript' src="./includes/validations.js"></script>
<script language="javascript">
function alert_msg()
{	
	var frm=document.frm_upload_iway;		
	alert('Please browse the file');
	return false;
}
	function checkupload(){
		var frm=document.frmtaupload;
		//alert(frm.userfile.value);
		if(frm.userfile.value==""){
			alert("file cannot be empty");
			return false;
		}
	frm.action="fmupload.php";	
	frm.submit();	
	}
</script>
</HEAD>
<BODY leftMargin=0 topMargin=0 LeftMargin=0 marginheight=0 marginwidth=0>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
	<!--<TR>	
        <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR>-->
	<tr><td width="780"><?include("includes/header.php");?></td></tr>

    <!--<TR>
        <TD bgColor=#7292c5 width=780>&nbsp;</TD>
    </TR>-->
	<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>
    <TR>
    	<TD background=images/tile.jpg vAlign=top width=780 align=center>
        <table width=780 cellpadding=0 cellspacing=5 border=0>
        <tr>
			<td>&nbsp;</td>
		</tr>
        	<tr>
	           	<td colspan=2 align="center"><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2> FM Details Upload</b></font></td>
            </tr>    
            </tr>
            <td colspan=2 align=center>
            <!--<form name=frmtaupload enctype="multipart/form-data" action="fmupload.php" method="POST">-->
            <form name=frmtaupload enctype="multipart/form-data"  method="POST">
			<input type="hidden" name="MAX_FILE_SIZE" value="30000" >
			<b>Send this file: </b><input name="userfile" type="file" class=textbox onkeypress='return alert_msg()'>
			</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
	           	<td colspan=2 align="center"><input type="submit" class="button" value="Send File" onclick='javascript:checkupload()'></td>
            </tr>
			<tr>
			
		        <td colspan=2 class="greybluetext10">Please refer to the 
		          <a href="download/fm_upload.txt" target="_blank">Text Template </a> for FM Details Upload </td>
		      </tr>
		      <tr>
			
		        <td colspan=2 class="greybluetext10">Please refer to the 
		          <a href="download/validation_fmupload.doc" target="_blank"> Validation Document </a> for FM Details Upload </td>
		      </tr>
			<?
			// If upd flag is set then the appropriate msg set above should be displayed here
			if($upd!='')
			{
				//On successful upload, Display success msg
				if($upd=='true')
				{?>
				<tr>
					<td colspan=2 align="center" class='alertmsg'><?echo $emsg[$msg];?></td>
				</tr>			
				<?}
				//if the upload operation is not successful, Display error msg
				else
				{?>
				<tr>
					<td colspan=2 align="center" class='alertmsg'><?echo $emsg[$msg];?></td>
				</tr>		
				<?}
			}
			
			//If all the records are successfully inserted, display the inserted records fetching it from DB
			if ($upd=='true' && $msg=='1')
			{
			?>
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr>
	           	<td colspan=2 align="center"><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2> Uploaded Details</b></font></td>
            </tr> 
			<tr>
			<td>
			<table align=center border=1>
        	<tr>
        	<td class="greybluetext10"><b>S.No</b></td>
        	<td class="greybluetext10"><b>Centre Code</b></td>
        	<td class="greybluetext10"><b>City</b></td>
        	<td class="greybluetext10"><b>No.of Seats</b></td>
        	<td class="greybluetext10"><b>Fm Name</b></td>
        	<td class="greybluetext10"><b>Fm Contact No.</b></td>
        	</tr>
        	<?
        	$i=1;
        	$sql_fm_details="select a. centre_code ,  c. city ,c. no_of_seats , b. name , c. phone  from  iib_franchisee_iway a , iib_franchisee b, iib_franchisee_map c  where  a. centre_code = c. centre_code and a. fm_id = c. FranchiseeId and b. row_id = a. fm_id and b. row_id = c. FranchiseeId order by  a. centre_code";
        	$res_fm_details=mysql_query($sql_fm_details);
        	while(list($Centre_Code,$City,$Seats,$Name,$Phone)=mysql_fetch_row($res_fm_details))
        	{?>
        	<tr>
        		<td class="greybluetext10" ><?=$i?></td>
	        	<td class="greybluetext10" ><?=$Centre_Code?></td>
	        	<td class="greybluetext10" ><?=$City?></td>
	        	<td class="greybluetext10" ><?=$Seats?></td>
	        	<td class="greybluetext10" ><?=$Name?></td>
	        	<td class="greybluetext10" ><?=$Phone?></td>
	        </tr>
        	<?$i++;
        	}
        	?>
        	</table>
        	</td>
        	</tr>
        	<?
    		}
    		//If there are less no.of records in the file is less than the database entries then display the missing record (in the file)from DB 
    		else if($upd=='true' && $msg=='10')
    		{
	    	?>
		    	<tr>
				<td>&nbsp;</td>
				</tr>
				<tr>
		           	<td colspan=2 align="center"><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2> Fm details are missing for the Centre(s) </b></font></td>
	            </tr> 
				<tr>
				<td>
				<table align=center border=1>
	        	<tr>
	        	<td class="greybluetext10"><b>S.No</b></td>
	        	<td class="greybluetext10"><b>Centre Code</b></td>
	        	</tr>
	        	<?
				
	        	$handle = fopen($filename, "r");
				//Writing the comtents of the file to an ARRAY named $aManual
				while(!feof($handle))
				{
					$aManual[] = fgets($handle);
				}
		 		fclose($handle);
		 		$i=0;
				// Exploding the comma seperated values into an array named $row
				foreach ($aManual as $value)
				{
					if($value!='')
					{	
						$row[$i]=explode(',',$value);
					}
					$i++;
				}	
				$cnt= count($row);
				for($a=0;$a<$cnt;$a++)
				{
					$row[$a][0]=strtoupper($row[$a][0]);
					$fmArr[$a]=$row[$a][0];
				}
	        	$sql_iway_details="select centre_code from  iib_iway_details order by centre_code";
	        	$res_iway_details=mysql_query($sql_iway_details);
	        	$b=0;
	        	while(list($centre_Code)=mysql_fetch_row($res_iway_details))
	        	{
		        	$centre_Code=strtoupper($centre_Code);
		        	$iwayArr[$b]=$centre_Code;
		        	$b++;
	        	}
	        	//Finding Difference between the iway details and fm details inserted
	        	$resultArr=array_diff($iwayArr,$fmArr);
	        	$j=1;
	        	foreach($resultArr as $key => $values)
	        	{
	        	?>
	        	<tr>
	        		<td class="greybluetext10" ><?=$j?></td>
		        	<td class="greybluetext10" ><?=$values?></td>
		        </tr>
	        	<?
	        	$j++;
        		}
	        	?>
	        	</table>
	        	</td>
	        	</tr>
    		<?
    		}
    		
    		?>
		</form>
		</td>
		</tr>
		<TR>
		<?include("includes/footer.php");?>
    	<!--<TD bgColor=#7292c5 width=780>&nbsp;</TD>-->
    	</TR>
		</table>
		</td>
		</tr>
		</table>
		</center>
		</body>
		</html>