<?php
set_time_limit(0);
ob_start();
$admin_flag = 0;
require_once("sessionchk.php");
require_once("constants.inc");
$fontPath = $fpdfPath."/font/";
$fpdfFile = $fpdfPath."/fpdf.php";
define('FPDF_FONTPATH',$fontPath);
require_once("dbconfig.php");
require($fpdfFile);

class PDF_MC_Table extends FPDF
{
var $widths;
function PDF_MC_Table(){
	$this->FPDF();
}
function SetWidths($w)
{
    //Set the array of column widths
    $this->widths=$w;
}

function Row($data,$aln,$optFill)
{
    //Calculate the height of the row
    $nb=0;
    for($i=0;$i<count($data);$i++)
        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
    $h=5*$nb;
    //Issue a page break first if needed
    $this->CheckPageBreak($h);
    //Draw the cells of the row
    for($i=0;$i<count($data);$i++)
    {
        $w=$this->widths[$i];
        //Save the current position
        $x=$this->GetX();
        $y=$this->GetY();
        //Draw the border
		//$col = $pdf->GetFillColor();
		if ($optFill == ''){
			$this->SetFillColor(255,255,255);
	        $this->Rect($x,$y,$w,$h,'F');
		}
		else
        	$this->Rect($x,$y,$w,$h);
        //Print the text
        $this->MultiCell($w,5,$data[$i]." ".$col,0,$aln);
        //Put the position to the right of the cell
        $this->SetXY($x+$w,$y);
    }
    //Go to the next line
    $this->Ln($h);
}

function CheckPageBreak($h)
{
    //If the height h would cause an overflow, add a new page immediately
    if($this->GetY()+$h>$this->PageBreakTrigger)
        $this->AddPage($this->CurOrientation);
}
function PutLineBreak(){
	//put a line break 
    $this->Ln(1);
}
function NbLines($w,$txt)
{
    //Computes the number of lines a MultiCell of width w will take
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);
    if($nb>0 and $s[$nb-1]=="\n")
        $nb--;
    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $nl=1;
    while($i<$nb)
    {
        $c=$s[$i];
        if($c=="\n")
        {
            $i++;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
            continue;
        }
        if($c==' ')
            $sep=$i;
        $l+=$cw[$c];
        if($l>$wmax)
        {
            if($sep==-1)
            {
                if($i==$j)
                    $i++;
            }
            else
                $i=$sep+1;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
        }
        else
            $i++;
    }
    return $nl;
}
function Footer(){
	//Position at 1.5 cm from bottom
	$this->SetY(-15);
	$this->SetFont('Arial','I',8);
	//Page number
	//$this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
}
function Header(){
	
	$this->Image('images/logo.png',10,8,200,20);
	$this->SetFont('Arial','B',15);
	$this->Cell(80);
	$this->Ln(25);
}
}

$offset = $_REQUEST["hOffset"];
$pagecount = $_REQUEST["hPageCount"];
$page = $_REQUEST["hPage"];
$each_link = $_REQUEST["hEachLink"];
$link_per_page = $_REQUEST["hLinkPerPage"];
$index = $_REQUEST["hIndex"];
$diff = $_REQUEST["hDiff"];
$centre_code = $_REQUEST["sCentreCode"];	
$sExamCode=isset($_REQUEST["sExamCode"])?$_REQUEST["sExamCode"]:"";

$rec_per_page=$each_link*$link_per_page;
$displacement = $offset + ($index*$each_link);
//$sql_iway_can = "select distinct(membership_no) from iib_candidate_iway  a, iib_iway_details b where b.exam_centre_code = '$centre_code' and a.centre_code = b.centre_code order by membership_no limit $displacement,$diff";
if ($sExamCode== 0)
	$sql_iway_can = "select distinct(membership_no) from iib_candidate_iway  a, iib_iway_details b where b.exam_centre_code = '$centre_code' and a.centre_code = b.centre_code order by membership_no limit $displacement,$diff";
elseif ($sExamCode != "")
	$sql_iway_can = "select distinct(membership_no) from iib_candidate_iway a, iib_iway_details b where a.exam_code='$sExamCode' and b.exam_centre_code = '$centre_code' and a.centre_code = b.centre_code order by membership_no limit $displacement,$diff";

$res_iway_can=mysql_query($sql_iway_can);
$num_can = @mysql_num_rows($res_iway_can);
if ($num_can < 1)
	exit;
$j=0;
while ($j < $num_can){
list($memno[$j])= mysql_fetch_row($res_iway_can);
$j++;
}
$st_memno = $memno[0];
$end_memno = $memno[--$j];

$memlist = implode('\',\'',$memno);

$sql_can = "select name,password,address1,address2,address3,address4,address5,address6,pin_code from iib_candidate where membership_no in ('$memlist') order by membership_no";
$res_can = mysql_query($sql_can);
$pdf=new PDF_MC_Table();
$pdf->Open();
$pdf->SetFont('Arial','',10);
$strFileName = 'Admit_'.$st_memno.'_'.$end_memno.'.pdf'; 
$i=0;

while ($memno[$i]!=""){
	$c_no = $memno[$i];
	list($c_name,$c_passwd,$c_addr1,$c_addr2,$c_addr3,$c_addr4,$c_addr5,$c_addr6,$c_pin)=mysql_fetch_row($res_can);
	$caddress6 = "";
	if ($c_addr6 != "")
		$caddress6 .= $c_addr6." ".$c_pin;	
	else
		$caddress6 .= $c_pin;
	$pdf->AddPage();
	$pdf->PutLineBreak();
	/*
	$pdf->SetWidths(array(150));
	$pdf->Row(array("Admission Card For Online Examination"),'L','');
	$pdf->PutLineBreak();*/
	$pdf->SetWidths(array(40,10,100));
	$pdf->Row(array("Membership No",":",$c_no),'L','');
	$pdf->PutLineBreak();
	$pdf->SetWidths(array(190));
	//$pdf->Row(array("Name"," : ",$c_name),'L','');
	$pdf->Row(array($c_name),'L','');
	//$pdf->PutLineBreak();
	if ($c_addr1 != "")
	{
		$pdf->SetWidths(array(190));
	//$pdf->Row(array("Address", " : ",$caddress),'L','');
		$pdf->Row(array($c_addr1),'L','');
		//$pdf->PutLineBreak();
	}
	if ($c_addr2 != "")
	{
		$pdf->SetWidths(array(190));
	//$pdf->Row(array("Address", " : ",$caddress),'L','');
		$pdf->Row(array($c_addr2),'L','');
		//$pdf->PutLineBreak();
	}
	if ($c_addr3 != "")
	{
		$pdf->SetWidths(array(190));
	//$pdf->Row(array("Address", " : ",$caddress),'L','');
		$pdf->Row(array($c_addr3),'L','');
		//$pdf->PutLineBreak();
	}
	if ($c_addr4 != "")
	{
		$pdf->SetWidths(array(190));
	//$pdf->Row(array("Address", " : ",$caddress),'L','');
		$pdf->Row(array($c_addr4),'L','');
		//$pdf->PutLineBreak();
	}
	if ($c_addr5 != "")
	{
		$pdf->SetWidths(array(190));
	//$pdf->Row(array("Address", " : ",$caddress),'L','');
		$pdf->Row(array($c_addr5),'L','');
		//$pdf->PutLineBreak();
	}
	if ($caddress6 != "")
	{
		$pdf->SetWidths(array(190));
	//$pdf->Row(array("Address", " : ",$caddress),'L','');
		$pdf->Row(array($caddress6),'L','');
		//$pdf->PutLineBreak();
	}
	$pdf->PutLineBreak();
	$pdf->PutLineBreak();
	$pdf->PutLineBreak();
	$pdf->PutLineBreak();
	$pdf->PutLineBreak();
/*	$sql_sub = "select b.subject_name,d.medium_code,a.exam_date, a.exam_time, c.iway_name, c.iway_address1, c.iway_address2, c.iway_city, c.iway_state, c.iway_pin_code from iib_candidate_iway a, iib_exam_subjects b, iib_iway_details c, iib_exam_candidate d  where  a.membership_no ='$memno[$i]' and b.subject_code=a.subject_code and c.centre_code = a.centre_code and d.membership_no = a.membership_no and d.exam_code = a.exam_code and d.subject_code = a.subject_code";
	$sql_sub = "select c.subject_name,e.medium_code,a.exam_date, a.exam_time, d.iway_name, d.iway_address1, d.iway_address2, d.iway_city, d.iway_state, d.iway_pin_code from iib_candidate_iway a, iib_exam b, iib_exam_subjects c, iib_iway_details d, iib_exam_candidate e  where a.membership_no ='$memno[$i]'and b.exam_code=a.exam_code and c.subject_code=a.subject_code and d.centre_code = a.centre_code and e.membership_no = a.membership_no and e.exam_code = a.exam_code and e.subject_code = a.subject_code"; */
	$sql_sel_can="select membership_no, exam_code,subject_code,exam_date,exam_time,centre_code from iib_candidate_iway where membership_no='$memno[$i]' order by exam_date,exam_time";
	$res_sel_can=mysql_query($sql_sel_can);
	$num_sub=@mysql_num_rows($res_sel_can);
	if ($num_sub==0){
		$errmsg="No Subjects !";
	}
	$chk = 0;
	$pdf->SetWidths(array(40,20,25,25,80));
	$pdf->Row(array("Subject Name","Medium","Date of Exam","Time of Exam","Venue Address"),'C','F');
	while ($chk < $num_sub){
//		list($subj_name,$medium_code,$exam_date,$exam_time,$iway_name,$iway_address1, $iway_address2, $iway_city, $iway_pin_code)=mysql_fetch_row($res_sub);
		list($membershipNo,$exam_code,$subj_code,$exam_date,$exam_time,$centre_code)=mysql_fetch_row($res_sel_can);		
		$sql_sel_sub="select subject_name from iib_exam_subjects where exam_code='$exam_code' and subject_code='$subj_code'";
		$res_sel_sub=mysql_query($sql_sel_sub);
		list($subj_name)=mysql_fetch_row($res_sel_sub);
		$sql_iway_det = "select iway_name, iway_address1, iway_address2, iway_city, iway_state, iway_pin_code from iib_iway_details where centre_code='$centre_code'";
		$res_iway_det = mysql_query($sql_iway_det); 
		list($iway_name,$iway_address1, $iway_address2, $iway_city, $iway_state,$iway_pin_code)=mysql_fetch_row($res_iway_det);
		$sql_medium = "select medium_code from iib_exam_candidate where membership_no='$memno[$i]' and exam_code='$exam_code' and subject_code='$subj_code'";
//		echo $sql_medium."<br>";
		$res_medium = mysql_query($sql_medium);
		list($medium_code)= mysql_fetch_row($res_medium);
		$address = "";
		if ($iway_name != "")
        	$address = $iway_name;
		if (($address != "") && ($iway_address1 != ""))
			$address .= " ".$iway_address1;
		if (($address != "") && ($iway_address2 != ""))
			$address .= " ".$iway_address2;
		if (($address != "") && ($iway_city != ""))
			$address .= " ".$iway_city;
		if (($address != "") && ($iway_state != ""))
			$address .= " ".$iway_state;
		if (($address != "") && ($iway_pin_code != ""))
			$address .= " ".$iway_pin_code;
		$tmpDate = $exam_date;
		$tmpTime = $exam_time;
		$tmpDate = explode('-',$tmpDate);
		$tmpTime =  explode(':',$tmpTime);
		$exam_date =  $tmpDate[2]."/".$tmpDate[1]."/".$tmpDate[0];
		$exam_time = $tmpTime[0].":".$tmpTime[1];
		if ($medium_code != "")
  			$pdf->Row(array($subj_name,$aMedium[$medium_code],$exam_date,$exam_time,$address),'L','F');
		$chk++;
	}
	$i++;
	$pdf->PutLineBreak();
	$pdf->PutLineBreak();
	$pdf->PutLineBreak();
	$pdf->PutLineBreak();
	$pdf->PutLineBreak();
	$pdf->PutLineBreak();
	$pdf->PutLineBreak();
	$pdf->PutLineBreak();
	$pdf->PutLineBreak();
	$pdf->SetWidths(array(20,5,165));
	$pdf->Row(array("Password",":",$c_passwd),'L','');	
	/*$pdf->SetWidths(array(190));
	$pdf->Row(array($c_passwd),'L','');	*/
	$pdf->PutLineBreak();
	$pdf->PutLineBreak();
	$pdf->SetWidths(array(15,160));
	$pdf->Row(array(" "," "),'L','');		
	$pdf->PutLineBreak();
	$pdf->SetWidths(array(15,160));
	$pdf->Row(array(" "," "),'L','');		
	$pdf->SetWidths(array(160));	
	$pdf->PutLineBreak();
	$pdf->SetWidths(array(15,160));
	$pdf->Row(array(" "," "),'L','');		
	$pdf->PutLineBreak();
	$pdf->SetWidths(array(15,160));
	$pdf->Row(array(" "," "),'L','');		
	$pdf->SetWidths(array(160));	
	$pdf->PutLineBreak();
	$pdf->SetWidths(array(15,160));
	$pdf->Row(array(" "," "),'L','');		
	$pdf->PutLineBreak();
	$pdf->SetWidths(array(15,160));
	$pdf->Row(array(" "," "),'L','');		
	$pdf->SetWidths(array(160));	
	$pdf->PutLineBreak();
	$pdf->SetWidths(array(15,160));
	$pdf->Row(array("1.","Please reach the venue at the specified time. In case you are late by 15 minutes or more from the time mentioned above. Permission to appear at the examination will be denied and you will lose the change to appear for the examination"),'L','');		
	$pdf->PutLineBreak();
	$pdf->SetWidths(array(15,160));
	$pdf->Row(array("2.","Please carry the membership Identity Card alongwith this admit card to the venue failing which admission for the examination will be denied"),'L','');	
	$pdf->SetWidths(array(160));	
	$pdf->SetWidths(array(15,160));
	$pdf->Row(array("3.","You would be able to log in to the system only with your password given above which should not be disclosed to others."),'L','');	
	$pdf->SetWidths(array(160));	
	$pdf->SetWidths(array(15,160));
	$pdf->Row(array("4.","In case the i-way(examination hall) is full, yuou will be required to wait for the next session even if you report for the examination in time."),'L','');	
	$pdf->SetWidths(array(160));	
}		
$pdf->Output($strFileName,true);
ob_end_flush();
?>
