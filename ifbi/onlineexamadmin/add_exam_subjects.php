<?php
/****************************************************
* Application Name            :  IBPS
* Module Name                 :  Adding Subjects
* Revision Number             :  1.1
* Revision Date               :
* Table(s)                    :
* Tables used for only selects: iib_exam_subjects,iib_subject_section
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              : 
* Document/Reference Material :
* Created By                  : 
* Created ON                  : 
* Last Modified By            : Sathiyanarayanan S
* Last Modified Date          : 28-May-2008
* Description                 : IM to add the subject
*****************************************************/
session_start();
if ($_SESSION["admin_type"]== '0') {
	$admin_flag = 0;
} else if ($_SESSION["admin_type"]== '1'){
	$admin_flag = 1;
}

require_once "sessionchk.php";
require_once "dbconfig.php";

//Added for BOI
//fetching the optional ids
$already_exists='false';
$i=0;
$option_ids='';
$sel_option="select distinct(option_id) from iib_exam_subjects";
$res_option=mysql_query($sel_option);
while($row=mysql_fetch_row($res_option))
{
	if($option_ids=='')
	{
		$option_ids=$row[0];
	}
	else
	{	
		$option_ids=$option_ids.','.$row[0];
	}
	
}

function min_to_sec($time_in_min)
{
	return $time_in_min*60;
}


//Added for BOI
	if($_POST['h_subjecttype']=='true')
	{
		$examcode     =   isSet($_REQUEST["examcode"])    ?   $_REQUEST["examcode"]   : " ";  
		$selectqry="select exam_type from iib_exam where exam_code='$examcode' and online='Y' order by exam_name";
		$selectres=mysql_query($selectqry);
		list($exam_type) = mysql_fetch_row($selectres);
		$subjectname     =   isSet($_REQUEST["subjectname"])    ?   $_REQUEST["subjectname"]   : " ";  
		
		$online	  =   "Y";    
		$subjectname=trim($subjectname);
		$subjectname=addslashes($subjectname);
		$passmark = trim($_POST['passmark']);
		$totalmarks = trim($_POST['totalmarks']);
		$examcode=$_POST["examcode"];
		$examcode=addslashes(trim($examcode));
		$subjectCode = trim($_POST['subjectcode']);
		$subjectduration = trim($_POST['duration_sub']);
		
		
		$subjectduration_sec=min_to_sec($subjectduration);	//Calling function to convert minutes to seconds
		$pre_grace = trim($_POST['pre_grace']);
		$pre_grace_sec=min_to_sec($pre_grace);	//Calling function to convert minutes to seconds
		
		$post_grace = trim($_POST['post_grace']);
		$post_grace_sec=min_to_sec($post_grace);	//Calling function to convert minutes to seconds
		
		$PassTxt = trim($_POST['PassTxt']);
		
		$FailTxt = trim($_POST['FailTxt']);
		
		$disp_score=isSet($_POST["disp_score"]) ? "Y" : "N" ;
		$disp_resp=isSet($_POST["disp_resp"]) ? "Y" : "N" ;
		$roundoff_score=isSet($_POST["roundoff_score"]) ? "Y" : "N" ;
		$disp_result=isSet($_POST["disp_result"]) ? "Y" : "N" ;
		$display_mark=isSet($_POST["disp_mark"]) ? "Y" : "N" ;
		$answer_shuffling=isset($_POST["answer_shuffling"]) ? "Y" : "N" ;
		$display_section=isSet($_POST["disp_section"]) ? "Y" : "N" ;
		$question_display=trim($_POST["question_display"]);
		if($question_display == 'M'){
			$noofquestionsperpage = $_POST['noofquestionsperpage'];
		}else if($question_display == 'O'){
			$noofquestionsperpage = 1;
		}else if($question_display == 'A'){	
			$noofquestionsperpage = 0;
		}
		$answersavingtype = trim($_POST["answersavingtype"]);
		$answersavinginterval = trim($_POST["answersavinginterval"]);
		$casedisplay = trim($_POST["casedisplay"]);	
		
		$subjecttype=$_POST["subjecttype"];
		if($subjecttype == 'Y')
		{
			$sel_option_check="select count(*) from iib_exam_subjects where exam_code='$examcode' and option_flag='Y'";
			$res_option_check=mysql_query($sel_option_check);
			$count=mysql_fetch_row($res_option_check);
			if($count[0]>0)
			{
				$sel_option="select distinct(option_id) from iib_exam_subjects where exam_code='$examcode' and option_flag='Y'";
				$res_option=mysql_query($sel_option);
				$optional=mysql_fetch_row($res_option);
				$optionalid=$optional[0];
				$already_exists="true";
			}
			else
			{
				$optionalid=$_POST['optionalid'];
			}
		}
		$max_no_questions=$_POST['maxno_ofquestions'];
		$subjectduration = trim($_POST['duration_sub']);
		$breakduration = trim($_POST['duration_brk']);
		//Added for BOI Ends here
	}

//Add a Exam
if ($_POST["submitted"]=='true')
{
    /*echo '<pre>';
	print_r($_POST);
	echo '</pre>';*/
	$subjectname     =   isSet($_REQUEST["subjectname"])    ?   $_REQUEST["subjectname"]   : " ";  
	$examcode     =   isSet($_REQUEST["examcode"])    ?   $_REQUEST["examcode"]   : " ";  
	$online	  =   "Y";    
	$subjectname=trim($subjectname);
	$subjectname=addslashes($subjectname);
	$passmark = trim($_POST['passmark']);
	$totalmarks = trim($_POST['totalmarks']);
	$examcode=addslashes(trim($examcode));
	$subjectCode = trim($_POST['subjectcode']);
	
	$subjectduration = trim($_POST['duration_sub']);
	$preventduration = trim($_POST['duration_prevent']);
	$preventduration_sec = min_to_sec($preventduration);
	
	$subjectduration_sec=min_to_sec($subjectduration);	//Calling function to convert minutes to seconds
	
	$pre_grace = trim($_POST['pre_grace']);
	$pre_grace_sec=min_to_sec($pre_grace);	//Calling function to convert minutes to seconds
	
	$post_grace = trim($_POST['post_grace']);
	$post_grace_sec=min_to_sec($post_grace);	//Calling function to convert minutes to seconds
	
	$PassTxt = trim($_POST['PassTxt']);
	
	$FailTxt = trim($_POST['FailTxt']);
	
	$disp_score=isSet($_POST["disp_score"]) ? "Y" : "N" ;
	$disp_resp=isSet($_POST["disp_resp"]) ? "Y" : "N" ;
	$roundoff_score=isSet($_POST["roundoff_score"]) ? "Y" : "N" ;
	$disp_result=isSet($_POST["disp_result"]) ? "Y" : "N" ;
	$disp_print=isSet($_POST["disp_print"]) ? "Y" : "N" ;
	$display_mark=isSet($_POST["disp_mark"]) ? "Y" : "N" ;
	$strLanguages = implode(",",$_POST["languages"]);
	
	$answer_shuffling=isset($_POST["answer_shuffling"]) ? "Y" : "N" ;
	$display_section=isSet($_POST["disp_section"]) ? "Y" : "N" ;
	$question_display=trim($_POST["question_display"]);
	if($question_display == 'M'){
		$noofquestionsperpage = $_POST['noofquestionsperpage'];
	}else if($question_display == 'O'){
		$noofquestionsperpage = 1;
	}else if($question_display == 'A'){	
		$noofquestionsperpage = 0;
	}
	$answersavingtype = trim($_POST["answersavingtype"]);
	$answersavinginterval = trim($_POST["answersavinginterval"]);
	$casedisplay = trim($_POST["casedisplay"]);
 
	
	//Added for BOI
	$breakduration = trim($_POST['duration_brk']);
	$breakduration_sec=min_to_sec($breakduration,$var_to_sec);	//Calling function to convert minutes to seconds
	$subjecttype=$_POST["subjecttype"];
    if($subjecttype=='Y')
	{
		$optionalid=$_POST['optionalid'];
	}
	$max_no_questions=$_POST['maxno_ofquestions'];
	//Added for BOI Ends here
	//print_r($_POST);
	//exit;
	if($err!=3)
	{
		$sql = "select count(1) from iib_exam_subjects where online='Y' and subject_code=$subjectCode ";	
		$res = mysql_query($sql);
		list($nCode) = mysql_fetch_row($res);
		if ($nCode > 0)
		{
			$err=4;
			$emsg='This code already exists!';
		}
		
		$sql_check = "select count(1) from iib_exam_subjects where online='N' and subject_code=$subjectCode ";	
		$res_check = mysql_query($sql_check);
		list($nCode_check) = mysql_fetch_row($res_check);
		if ($nCode_check > 0)
		{
			$err=5;
			$emsg='Cannot use this code !';
		}
		
		if(($err!=4)&&($err!=5))
		{
			$chksql="select count(1) from iib_exam_subjects where online='Y' and exam_code='$examcode' and subject_name='$subjectname'";	
			$chkres=mysql_query($chksql);
			list($num)=mysql_fetch_row($chkres);
			if($num==0)
			{	
				$sql = "INSERT INTO iib_exam_subjects (subject_code,subject_name,exam_code,total_marks,pass_mark,"
					    ."subject_duration,display_score,display_result,display_response,roundoff_score,pass_msg,fail_msg,grace_pre,grace_post,answer_shuffling,"
						."question_display,updatedtime,online,option_id,option_flag,max_no_of_questions,break_duration,"
						."duration_prevent,display_print,languages,display_mark,display_sectionname,display_casequestion,"
					    ."display_noofquestions,answer_saving_type,answer_saving_interval)"	
						."VALUES"
						."('$subjectCode','$subjectname','$examcode','$totalmarks','$passmark','$subjectduration_sec','$disp_score',"
						."'$disp_result','$disp_resp','$roundoff_score','$PassTxt','$FailTxt','$pre_grace_sec','$post_grace_sec','$answer_shuffling',"
						."'$question_display',now(),'$online','$optionalid','$subjecttype','$max_no_questions','$breakduration_sec',"
						."'$preventduration_sec','$disp_print','$strLanguages','$display_mark','$display_section','$casedisplay',"
						."'$noofquestionsperpage','$answersavingtype','$answersavinginterval')";
				
				//echo "Insert Qry :-->".$sql;				
				$res = mysql_query($sql) or die ("Connection cannot be done3 error: ".mysql_error()."--->");
				if(!$res)
				{
					$err=1;
					$emsg="There was an error in inserting the record.";
					$displaybug='true';
				}
				else
				{
					$done=1;
					$emsg="New record created.";
				}
			}
			else{
				$err=2;
				$emsg='This name already exists!';
			}
		}
	}
}
$selectqry="select exam_code,exam_name from iib_exam where online='Y' order by exam_name";
$selectres=mysql_query($selectqry);

?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
	FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language="JavaScript" src="./includes/validations.js" ></script>
<script language='javascript'>

function validate()
{
	var f=document.addSection;
	if(f.examcode.value==-1)
	{
		alert('Please Select the Exam Code!');
		f.examcode.focus();
		return;
	}
/* Added for BOI */

 if(f.subjecttype)
 {
  
  	if(f.subjecttype.value==-1)
	{
		alert('Please Select the Subject Type!');
		f.subjecttype.focus();
		return;
	}

  if(f.subjecttype.value=='Y')
	{ 
		if(f.subjectcode.value==f.optionalid.value)
		{
			alert("Optional ID and subject code cannot be the same");
			f.optionalid.focus();
			return;
		}
	}
	if(f.subjecttype.value=='Y')
	{
		if(trim(f.optionalid.value)=='')
		{
			alert('Please enter the Optional ID!');
			f.optionalid.focus();
			return;	
		}
		myRegExp = new RegExp("[^0-9]");
		optionalid_val=f.optionalid.value;
		result_optionalid=optionalid_val.match(myRegExp);
		if (result_optionalid)
		{
			alert('Only numbers allowed for optional ID!');
			f.optionalid.focus();
			return;
		}
	}
	if(trim(f.duration_brk.value)=='')
	{
		alert('Please Enter the Break Duration!');
		f.duration_brk.focus();
		return;
	}
	if(parseInt(trim(f.duration_brk.value))<'0')
	{
		alert('Please Enter valid Duration!');
		f.duration_brk.focus();
		return;
	}
	 myRegExp = new RegExp("[^0-9]");
    bdur=f.duration_brk.value;
    result_brk=bdur.match(myRegExp);
    if (result_brk)
    {
        alert('Only numbers allowed!');
        f.duration_brk.focus();
        return;
   }
   	if(trim(f.maxno_ofquestions.value)=='')
	{
		alert('Please Enter the maximum number of Questions!');
		f.maxno_ofquestions.focus();
		return;
	}
	myRegExp = new RegExp("[^0-9]");
	maxno_ofquestions_val=f.maxno_ofquestions.value;
    result_maxno_ofquestions=maxno_ofquestions_val.match(myRegExp);
    if (result_maxno_ofquestions)
    {
        alert('Only numbers allowed!');
        f.maxno_ofquestions.focus();
        return;
    }
	if(trim(f.maxno_ofquestions.value)<='0')
		{
			alert('Please Enter valid value for maximum number of questions!');
			f.maxno_ofquestions.focus();
			return;
		}
}

/* BOI ends here */
	if(f.subjectcode.value=='')
	{
		alert('Please Enter the Subject Code!');
		f.subjectcode.focus();
		return;
	}
	
	myRegExp = new RegExp("[^0-9]");
    sCode=f.subjectcode.value;
    result=sCode.match(myRegExp);
    if (result)
    {
        alert('Only numbers allowed!');
        f.subjectcode.focus();
        return;
   }
   
	if(trim(f.subjectname.value)=='')
	{
		alert('Please Enter the Subject Name!');
		f.subjectname.value='';
		f.subjectname.focus();
		return;
	}
	
    myRegExp = new RegExp("[^a-zA-Z0-9  _/&,:#\\\)(-.]");
    eNAME=f.subjectname.value;
    result=eNAME.match(myRegExp);
    if(result)
    {
       // alert('Allowed Character Alphabet,Numbers,/,\\,#,&,(,),-,:,.');
	    alert('Allowed Character Alphabet,Numbers,/,#,&,(,),-,:,.');
        f.subjectname.focus();
        return;
   }

    myRegExp = new RegExp("^/.*$");
    eNAME=f.subjectname.value;
    result=eNAME.match(myRegExp);
    if(result)
    {
        alert('/ Cannot be the first character!');
        f.subjectname.focus();
        return;
    }
    
    if (f.totalmarks.value=='')
	{
		alert('Please Enter The Total Marks!');
		f.totalmarks.focus();
		return;
	}	
    myRegExp = new RegExp("[^0-9]");
    tMark=f.totalmarks.value;
    result=tMark.match(myRegExp);
    if(result)
    {
        alert('Enter only numbers');
        f.totalmarks.focus();
        return;
   }
	if (f.totalmarks.value <= 0)
   	{
	 	alert('Total marks must be greater than 0');
        f.totalmarks.focus();
        return;
   	}
    
	if (f.passmark.value=='')
	{
		alert('Please Enter The Pass Mark!');
		f.passmark.focus();
		return;
	}	
    myRegExp = new RegExp("[^0-9]");
    pMark=f.passmark.value;
    result=pMark.match(myRegExp);
    if(result)
    {
        alert('Enter only numbers');
        f.passmark.focus();
        return;
   }
	if ((f.passmark.value <= 0) || (f.passmark.value > parseInt(f.totalmarks.value)))		
   	{
	 	alert('Pass mark must be in the range 1-'+f.totalmarks.value);
        f.passmark.focus();
        return;
   	}
   	if(trim(f.duration_sub.value)=='')
	{
		alert('Please Enter the Subject Duration!');
		f.duration_sub.focus();
		return;
	}
	if(trim(f.duration_sub.value)=='0')
	{
		alert('Please Enter valid Duration!');
		f.duration_sub.focus();
		return;
	}
	myRegExp = new RegExp("[^0-9]");
    sdur=f.duration_sub.value;
    result1=sdur.match(myRegExp);
    if (result1)
    {
        alert('Only numbers allowed!');
        f.duration_sub.focus();
        return;
   }
   
   if(trim(f.duration_prevent.value)=='')
	{
		alert('Please Enter the Duration to prevent submit');
		f.duration_prevent.focus();
		return;
	}
    myRegExp = new RegExp("[^0-9]");
    sdur2=f.duration_prevent.value;
    result2=sdur2.match(myRegExp);
    if (result2)
    {
        alert('Only numbers allowed!');
        f.duration_prevent.focus();
        return;
   }  
    if(parseInt(f.duration_prevent.value) > parseInt(f.duration_sub.value))
	 {
		 alert('Duration to prevent submit should be less than or equal to subject duration');
		 f.duration_prevent.focus();
		 return;
	 }  	
 
   /*Validation for pre grace time*/
   
   if(trim(f.pre_grace.value)=='')
	{
		alert('Please Enter the Pre Grace Time!');
		f.pre_grace.focus();
		return;
	}
	if(trim(f.pre_grace.value)=='0')
	{
		alert('Please Enter valid Pre Grace Time!');
		f.pre_grace.focus();
		return;
	}
	
    pregracedur=f.pre_grace.value;
    result_pregrace=pregracedur.match(myRegExp);
    if (result_pregrace)
    {
        alert('Only numbers allowed!');
        f.pre_grace.focus();
        return;
   }
   
   /*Validation for Post grace time*/
   
   if(trim(f.post_grace.value)=='')
	{
		alert('Please Enter the Post Grace Time!');
		f.post_grace.focus();
		return;
	}
	if(trim(f.post_grace.value)=='0')
	{
		alert('Please Enter valid Post Grace Time!');
		f.post_grace.focus();
		return;
	}
	
    postgracedur=f.post_grace.value;
    result_postgrace=postgracedur.match(myRegExp);
    if (result_postgrace)
    {
        alert('Only numbers allowed!');
        f.post_grace.focus();
        return;
    }
    

     /*Validation for  Message*/
		
   	if (trim(f.PassTxt.value)=="")
    {
	    alert("Please enter the Pass Message");
	    f.PassTxt.focus();
	    return false;
    }
    else
    {
	    //myRegExp = new RegExp("[^a-zA-Z0-9-,&:/ .]+\n\r");    
	    myRegExp = new RegExp("[^a-zA-Z0-9-,&:/ ._]+\n\r");    
	    spass_msg=f.PassTxt.value;
	    result_passmsg=spass_msg.match(myRegExp);
	    
	    
	    if (result_passmsg)
	    {
	        alert('Allowed Character Alphabet,Numbers,/,,,&,-,_,:, ,.');
	        f.PassTxt.focus();
	        return;
	   }
    } 
    
    /*Validation for Fail Message*/
    
    if (f.FailTxt.value=="")
    {
	    alert("Please enter the Fail Message");
	    f.FailTxt.focus();
	    return false;
    }
    else
    {
	    myRegExp = new RegExp("[^a-zA-Z0-9-,&:/ ._]+\n\r");    
	    sfail_msg=f.FailTxt.value;
	    result_failmsg=sfail_msg.match(myRegExp);
	    
	    if (result_failmsg)
	    {
	        alert('Allowed Character Alphabet,Numbers,/,,,&,-,_,:, ,.');
	        f.failTxt.focus();
	        return;
	   }
    }    
    /*if(charchk(f.FailTxt.value))
    {
	    alert("Only Alphabets and space are allowed");
	    f.FailTxt.focus();
	    return false;
    }*/    	
	if( f.question_display[2].checked){
		if( f.noofquestionsperpage.value == ''){
			alert("Please enter no.of questions per page");	
			f.noofquestionsperpage.focus();
			return false;    
		}else{
			noofquestion=f.noofquestionsperpage.value;
    		myRegExp = new RegExp("[^0-9]");
    		result_noofquestion = noofquestion.match(myRegExp);
			if (result_noofquestion){
				alert('Only numbers allowed!');
				f.noofquestionsperpage.focus();
				return;
			}									
		}
	}
	if(document.getElementById('languages').checked==0){	
		alert("please select atleast one language to do question paper upload");
		f.languages[0].focus();
		return false;
	}		
	f.submitted.value='true';
	f.submit();
}

function submitForm()
{
    if (window.event.keyCode == 13)
    {
        validate();
    }
}

function check_optional()
{
	var f=document.addSection;
	f.h_subjecttype.value="true";
	f.submit();
}
function check_subjectcode()
{
	var f=document.addSection;
	var val=f.subjectcode.value;
	var arr=f.h_optionids.value;
	var col_array=arr.split(",");
	var counter=0;
	while (counter < col_array.length)
	{
		if(val==col_array[counter])
		{
			alert("Please enter the subject code which is not in the optional ids");
			f.subjectcode.focus();
			return;
		}
		counter++;
	}
	return;
}
function check_id()
{
	var f=document.addSection;
	if(f.subjectcode.value==f.optionalid.value)
	{
		alert("Optional ID and subject code cannot be the same");
		f.optionalid.focus();
		return;
	}
}

function calcprevsub(obj)
{
	var f=document.addSection;
	if(obj.value!='')
	{
		myRegExp = new RegExp("[^0-9]");
    	sdur=obj.value;
	    result1=sdur.match(myRegExp);
		if(!result1){
			f.duration_prevent.value=parseInt((parseInt(sdur)*3)/4);			
		}		
	}
}
function changeQuestionDisplay(){
	var f=document.addSection;
	for(var i=0;i<f.question_display.length;i++){
		if(f.question_display[i].checked == true){
			if(f.question_display[i].value == 'M'){
				document.getElementById('divnoofquestionsperpage').style.display="block";
				document.getElementById('divnoofquestionsperpage').style.visibility="visible";
			}else{
				document.getElementById('divnoofquestionsperpage').style.display="none";
				document.getElementById('divnoofquestionsperpage').style.visibility="hidden";
				f.noofquestionsperpage.value='';	
			}		
		}
	}
}
function changeAnswerSavinType(){
	var f=document.addSection;
	for(var i=0;i<f.answersavingtype.length;i++){
		if(f.answersavingtype[i].checked == true){
			if(f.answersavingtype[i].value == 'T'){
				document.getElementById('spananswersavinginterval').innerHTML=" Minutes";
			}else{
				document.getElementById('spananswersavinginterval').innerHTML= " Question(s)";
			}		
		}
	}
}
</script>
</HEAD>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0 onload='javascript:document.addSection.examcode.focus();' onKeyPress='javascript:submitForm()'>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=900>
	<!--<TR> Topnav Image 
    	<TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR>	-->
	<tr><td width="900"><?include("includes/header.php");?></td></tr>
<!-- 	<TR>
    	<TD bgColor=#7292c5 width=780>&nbsp;</TD>
	</TR> -->
	<tr>
		<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>	
	<TR>
	    <TD background=images/tile.jpg vAlign=top width=900 align=center>
     	<form name=addSection method="post">
		<table width="900" cellpadding=0 cellspacing=5 border=0>
			<tr>
				<td colspan=2 align="center"><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2>Add Subject</font></b></td>
			</tr>		
		
<!--
			<tr>
				<td align="right" class="greybluetext10" valign=top>
				Enter The Subject Name :&nbsp;
				</td>
				<td align="left" valign=top>
				<input class="textbox" type="text" name="subjectname" size=20 maxlength=50>
				</td>
			</tr>
-->
			<tr>	
				<td align="right" width=390 class="greybluetext10" valign=top>
				Exam Code :&nbsp;				</td>
				<td align="left" valign=top>
					<select name='examcode' class='textbox' style={width:250px} onChange="check_optional()">
					<option value=-1>-Select-</option>
					<?
					while(list($exam_code,$exam_nam)=mysql_fetch_row($selectres))
					{
						if($examcode == $exam_code)
							$selected = " SELECTED ";
						else
							$selected = " ";
						echo "<option value=$exam_code  $selected >$exam_code -> $exam_nam</option>";
					}
					?>
					</select>
<!--					<input type="text" class="textbox" name=examcode size=20>-->
					<input type="hidden" name="h_optionids" value=<?echo $option_ids?>>				</td> 
			</tr>
 <!-- Added for BOI starts here --> 
			 <?php
				if($exam_type == 3)
				{
			 ?>
			 <tr>
                    <td align="right" class="greybluetext10" valign=top>
                     Subject Type :&nbsp;                    </td>
                    <td><select name='subjecttype' class='textbox' style={width:150px}  onchange="check_optional()">
						<option value=-1>-Select-</option>

					<?if($subjecttype=='N')
					{?>
						<option value='N' selected>Mandatory</option>
						<option value='Y' >Optional</option>
					<?}
					else if($subjecttype=='Y')
					{?>
						<option value='N' >Mandatory</option>
						<option value='Y' selected>Optional</option>
					<?}
					else
					{?>
						<option value='N' >Mandatory</option>
						<option value='Y' >Optional</option>
					<?}?>

					</select>					</td>
             </tr>  
			 <?if($subjecttype=='Y')
			 {?>
				 <tr>
						<td align="right" class="greybluetext10" valign=top>
						 Optional ID :&nbsp;						</td>
						<td align="left" valign=top>
						<?if($already_exists=='true')
						{?>
							<input class="textbox" type="text" name="optionalid" size=27 maxlength=5 readonly value=<?=$optionalid?> >
						<?}
						else if($already_exists=='false')
						{?>
							<input class="textbox" type="text" name="optionalid" size=27 maxlength=5 onblur='javascript:check_id()'; value=<?=$optionalid?> >
						<?}?>						</td>
				 </tr>  
			<?}?>
			             <tr>
                    <td align="right" class="greybluetext10" valign=top>
                    Break Duration :&nbsp;                    </td>
                    <td align="left" valign=top>
                    <input class="textbox" type="text" name="duration_brk" size=27 maxlength=5 value=<?=$breakduration?>>
					<span class="greybluetext10">In Minutes</span>                    </td>
             </tr>
			 <tr>
                    <td align="right" class="greybluetext10" valign=top>
                     Maximum No: of Questions :&nbsp;                    </td>
                    <td align="left" valign=top>
                    <input class="textbox" type="text" name="maxno_ofquestions" size=27 maxlength=3 value=<?=$max_no_questions?>>                    </td>
             </tr> 
			<?php
				}
			?>
             <!-- Added for BOI ends here -->
			<tr>
                    <td align="right" class="greybluetext10" valign=top>
                    Enter The Subject Code :&nbsp;                    </td>
                    <td align="left" valign=top>
                    <input class="textbox" type="text" name="subjectcode" size=27 maxlength=50>                    </td>
            </tr>
            <tr>
                    <td align="right" class="greybluetext10" valign=top>
                    Enter The Subject Name :&nbsp;                    </td>
                    <td align="left" valign=top>
                    <input class="textbox" type="text" name="subjectname" size=27 maxlength=50>                    </td>
            </tr>
            <tr>
                    <td align="right" class="greybluetext10" valign=top>
                    Enter The Total Marks :&nbsp;                    </td>
                    <td align="left" valign=top>
                    <input class="textbox" type="text" name="totalmarks" size=27 maxlength=5>                    </td>
             </tr>
			<tr>
                    <td align="right" class="greybluetext10" valign=top>
                    Enter The Pass Mark :&nbsp;                    </td>
                    <td align="left" valign=top>
                    <input class="textbox" type="text" name="passmark" size=27 maxlength=5>                    </td>
             </tr>
             
              <tr>
                    <td align="right" valign=top class="greybluetext10">
                    Subject Duration :&nbsp;                    </td>
                    <td align="left" valign=top>
                    <input class="textbox" type="text" name="duration_sub" size=27 maxlength=5 onBlur="calcprevsub(this)">
					<span class="greybluetext10">In Minutes</span>                    </td>
             </tr>
              <tr>
                <td align="right" valign=top class="greybluetext10">Duration to prevent submit (Default 3/4 subject duratiion): </td>
                <td align="left" valign=top><input class="textbox" type="text" name="duration_prevent" size=27 maxlength=5>
                <span class="greybluetext10">In Minutes</span></td>
              </tr>
             <tr>
                    <td align="right" class="greybluetext10" valign=top>
                     Pre Grace Time :&nbsp;                    </td>
                    <td align="left" valign=top>
                    <input class="textbox" type="text" name="pre_grace" size=27 maxlength=5>
					<span class="greybluetext10">In Minutes</span>                    </td>
             </tr>
                  
             <tr>
                    <td align="right" class="greybluetext10" valign=top>
                     Post Grace Time :&nbsp;                    </td>
                    <td align="left" valign=top>
                    <input class="textbox" type="text" name="post_grace" size=27 maxlength=5>
					<span class="greybluetext10">In Minutes</span>                    </td>
             </tr>    
             <tr>
                <td align='right' class='greybluetext10'>Pass Message : </td>
                <td>
                	<textarea  name="PassTxt" cols=30 rows=5 maxlength=500 value=''>REP_MEMBERSHIPNAME has secured REP_SCORE/REP_TOTALMARK Marks in REP_SUBJECTNAME</textarea>                </td>
            </tr>
          <tr>
                <td align='right' class='greybluetext10'>Fail Message : </td>
                <td>
                	<textarea  name="FailTxt" cols=30 rows=5 maxlength=500 value=''>REP_MEMBERSHIPNAME has secured REP_SCORE/REP_TOTALMARK Marks in REP_SUBJECTNAME</textarea>                </td>
            </tr>
            
            <tr>
                    <td align="right" class="greybluetext10" valign=top>
                    Display Score :&nbsp;&nbsp;                    </td>
                    <td align="left" valign=top>
                    <INPUT TYPE=checkbox  name="disp_score"> </td>
             </tr>
            <tr>
                    <td align="right" class="greybluetext10" valign=top>
                    Display Response :&nbsp;&nbsp;                    </td>
                    <td align="left" valign=top>
                    <INPUT TYPE=checkbox  name="disp_resp"> </td>
             </tr>
             
             <tr>
                    <td align="right" class="greybluetext10" valign=top>
                    Display Result:&nbsp;&nbsp;                    </td>
                    <td align="left" valign=top>
                    <INPUT TYPE=checkbox   name="disp_result">                    </td>
             </tr>
            <tr>
                    <td align="right" class="greybluetext10" valign=top>
                    Round Off - Score Calculation :&nbsp;&nbsp;                    </td>
                    <td align="left" valign=top>
                    <INPUT TYPE=checkbox  name="roundoff_score"> </td>
             </tr>
             <tr>
               <td align="right" class="greybluetext10" valign=top>Print button Display in  score(response)  page: </td>
               <td align="left" valign=top><INPUT   name="disp_print" TYPE=checkbox checked/></td>
             </tr>
             <tr>
               <td align="right" class="greybluetext10" valign=top>Display Marks in Question paper page: </td>
               <td align="left" valign=top><INPUT   name="disp_mark" TYPE=checkbox checked/></td>
			 </tr>
			  <tr>
               <td align="right" class="greybluetext10" valign=top>Display Section Name in Question paper page:&nbsp;&nbsp;</td>
               <td align="left" valign=top><INPUT TYPE=checkbox value='Y'  name="display_section" checked="checked"/></td>
             </tr> 
    		  <tr>
               <td align="right" class="greybluetext10" valign=top>Answer Shuffling:&nbsp;&nbsp;</td>
               <td align="left" valign=top><INPUT TYPE=checkbox value='Y'  name="answer_shuffling"></td>
             </tr> 
             <tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Question Display :&nbsp;</td>
                <td align="left" valign=top class="greybluetext10">
				<input type="radio" value='O' name=question_display onclick="changeQuestionDisplay()" checked="checked" >One question per page <br/>
				<input type="radio" value='A' name=question_display onclick="changeQuestionDisplay()" >All questions in single page <br/>
				<input type="radio" value='M' name=question_display onclick="changeQuestionDisplay()" >Parameterize no.of question(s) per page</td>
			</tr>
	  <tr >
	  <td  colspan="2" align="left"  class="greybluetext10" valign=top>
		<div id="divnoofquestionsperpage"  style="visibility:hidden;display:none;" align=left>
 &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp;	 &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Enter No.Of Question's per Page: 
&nbsp; &nbsp; <input class="textbox" type="text" name="noofquestionsperpage" id="noofquestionsperpage"/> 
		</div>    		
		</td></tr>			 		
      <tr>
       <td align="right" width=390 class="greybluetext10" valign=top>Answer Saving Type :&nbsp;</td>
                <td align="left" valign=top class="greybluetext10">
				<input type="radio" value='Q' name="answersavingtype" checked="checked"  onclick="changeAnswerSavinType()" > Question Based &nbsp;&nbsp;
				<input type="radio" value='T' name="answersavingtype" onclick="changeAnswerSavinType()"> Time Based </td>
			</tr>
      <tr>
       <td align="right" width=390 class="greybluetext10" valign=top> Answer Saving Interval:</td>
				<td align="left" valign=top class="greybluetext10">&nbsp;
				<input class="textbox" type="text" name="answersavinginterval" id="answersavinginterval" Maxlength=5 value="1" />
				<span id="spananswersavinginterval"> Question(s)</span></td>
	  </tr>	
	<tr>
       <td align="right" width=390 class="greybluetext10" valign=top>Display Case Question :&nbsp;</td>
       <td align="left" valign=top class="greybluetext10">
	   <input type="radio" value='A' name="casedisplay" checked="checked">All case type questions single page &nbsp;&nbsp;
	   <input type="radio" value='S' name="casedisplay">Single case type question  per Page</td>
	</tr>
	<tr>
    <td align="right" width=390 class="greybluetext10" valign=top>Languages :&nbsp;</td>
    <td align="left" valign=top class="greybluetext10">
<?php
	$sqlLang="select lang_code,lang_name from iib_languages where is_active='Y'";
	$resLang=mysql_query($sqlLang);
	$strHTML = '';
	$strHTML .= "<table border='0' cellspacing='1' cellpadding='1' align='left' border='1'><tr>";
	$colCT=0;
	while (list($langCode, $langName) = mysql_fetch_row($resLang))
	{
			if(@in_array($langCode, $arrLanguages))
			$checked = 'checked';
		else
			$checked = '';

		$strHTML .= "<td valign=top class='greybluetext10'><input type='checkbox' id='languages' name='languages[]' value='".$langCode."' $checked >".$langName."</td>";

		$colCT++;
		if(($colCT % 3)== 0)
			$strHTML .= "</tr><tr><td colspan='3'>&nbsp;</td></tr><tr>";
	}
	$strHTML .= "</tr></table>";
	echo $strHTML;
?>				</td>
			</tr>
			 <tr>
               <td align="right" class="greybluetext10" valign=top>&nbsp;</td>
               <td align="left" valign=top>&nbsp;</td>
             </tr>

			<!--
			<tr>
				<td align="right" class="greybluetext10" valign=top>Status :&nbsp;</td>
				<td align="left" valign=top>
					<select name='cOnline' class="textbox">
					<option value='Y'>Online</option>
					<option value='N'>Offline</option>
					</select>
				</td> 
			</tr>
-->
			<INPUT type="hidden" name=tCode value=<? echo $_REQUEST["code"]?> />
			<tr>
				<td align="right"><input type="button" class=button name=bSubmit value="Save" onclick='javascript:validate();'></td>
				<td align="left"><input type="reset" class=button name=bReset value="Reset"></td> 
			</tr>
			<?if(isset($err)){?>	
			<tr>
				<td colspan=2 align="center" class='errormsg'><b><?echo $emsg;?></b></td>
			</tr>		
		<?}?>
		<?if(isset($done)){?>	
			<tr>
				<td colspan=2 align="center" class='alertmsg'><b><?echo $emsg;?></b></td>
			</tr>		
		<?}?>
		<br><br>
		<tr>
				<td colspan=2 align="left" class=greybluetext10><b>REP_MEMBERSHIPNAME - To display Member Name</b></td>
		</tr>
		<tr>
			<td colspan=2 align="left" class=greybluetext10><b>REP_SCORE - To display score</b></td>
		</tr>
		<tr>
			<td colspan=2 align="left" class=greybluetext10><b>REP_TOTALMARK - To display total marks</b></td>
		</tr>
		<tr>
			<td colspan=2 align="left" class=greybluetext10><b>REP_SUBJECTNAME - To display Subject Name</b></td>
		</tr>
					<input type="hidden" name="submitted">
					<input type="hidden" name="h_subjecttype">
		</table>
		</form>
		</TD>
	</TR>
	<!-- <TR>
		<TD bgColor=#7292c5 width=780>&nbsp;</TD>
	</TR> -->
	<?include("includes/footer.php");?>
</TABLE>
</FORM>
</center>
</BODY>
</HTML>
