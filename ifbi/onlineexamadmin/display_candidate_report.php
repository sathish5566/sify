<?php
$admin_flag=0;
require_once "sessionchk.php";
require_once "dbconfig.php";
require_once("constants.inc");

set_time_limit(0);
$examCode = $_POST['hid_exam_code'];
$membershipNo = $_POST['memno'];
$examName = $_POST['hid_exam_name'];
$subjectName = $_POST['hid_subject_name'];
$changed = $_POST['changed'];
$subjectCode = $_POST['hid_subject_code'];

$errormsg ="";
$msg = "";
$commonDebug = false;

	//question paper
	$sqlQuestions = "SELECT question_paper_no FROM iib_candidate_test WHERE exam_code='$examCode' AND subject_code='$subjectCode' ".
		" AND test_status='C' AND membership_no='$membershipNo'";

	if ($commonDebug)
	{
		print("\n".$sqlQuestions);
	}
	$resQuestions = @mysql_query($sqlQuestions);
	list($questionPaperNo) = @mysql_fetch_row($resQuestions);
	if ($commonDebug)
        {
                print("\n".$questionPaperNo);
        }

	if ($questionPaperNo == "")
	{
		$errormsg = "Candidate has not completed taking the exam";
	}
	else
	{
		//candidate
		$sqlMember = "SELECT name, address1, address2, address3, address4, address5, address6, pin_code FROM iib_candidate WHERE ".
	  		"membership_no='$membershipNo'";
		if ($commonDebug)
		{
			print("\n".$sqlMember);
		}
		
		$resMember = @mysql_query($sqlMember) or die("select from iib_candidate failed ".mysql_error());
		$row = mysql_fetch_array($resMember);
		$memberName = $row['name'];
		$c_addr1 = $row['address1'];
		$c_addr2 = $row['address2'];
		$c_addr3 = $row['address3'];
		$c_addr4 = $row['address4'];
		$c_addr5 = $row['address5'];
		$c_addr6 = $row['address6'];
		$c_pin = $row['pin_code'];
		
		$memberAddress = "";
		if ($c_addr1 != "")
			$memberAddress .= $c_addr1;
		if ($memberAddress != "") 
			$memberAddress .= " ";
		if ($c_addr2 != "")
			$memberAddress .= $c_addr2;
		if (($memberAddress != "") && ($c_addr2 != ""))
			$memberAddress .= " ";
		if ($c_addr3 != "")
			$memberAddress .= $c_addr3;
		if (($memberAddress != "") && ($c_addr3 != ""))
			$memberAddress .= " ";
		if ($c_addr4 != "")
			$memberAddress .= $c_addr4;
		if (($memberAddress != "") && ($c_addr4 != ""))
			$memberAddress .= " ";
		if ($c_addr5 != "")
			$memberAddress .= $c_addr5;
		if (($memberAddress != "") && ($c_addr5 != ""))
			$memberAddress .= " ";
		if ($c_addr6 != "")
			$memberAddress .= $c_addr6;
		if (($memberAddress != "") && ($c_addr6 != ""))
			$memberAddress .= " ";	
		if ($c_pin != "")
			$memberAddress .= $c_pin;	
			
		//iway
		$sqlIway = " SELECT centre_code, exam_date, exam_time FROM iib_candidate_iway WHERE  exam_code='$examCode' AND ".
						" subject_code='$subjectCode' AND membership_no='$membershipNo' ";
		if ($commonDebug)
		{					
			print $sqlIway;
		}
		$resIway = mysql_query($sqlIway);
		
		$nIway = mysql_num_rows($resIway);	
		if ($nIway > 0)
		{
			list($centreCode, $examDate, $examTime) = mysql_fetch_row($resIway);
			if ($examDate != "")
			{
				$aDate = explode("-",$examDate);										
			}
			$dispExamDate = date("d/m/Y", mktime (0,0,0,$aDate[1],$aDate[2],$aDate[0]));	
			$sqlIWay = "SELECT iway_address1, iway_address2, iway_city, iway_state, iway_pin_code FROM iib_iway_details ".
			" WHERE centre_code='$centreCode'";
			if ($commonDebug)
			{
				print("\n".$sqlIWay);
			}
			$resIWay = @mysql_query($sqlIWay) or die("select from iib_iway_details failed ".mysql_error());
			$row = mysql_fetch_array($resIWay);
			$iwayAddress = "";
			$row['iway_address1'] != "" ? $iwayAddress .= " ".$row['iway_address1'] : $iwayAddress .= "";
			$row['iway_address2'] != "" ? $iwayAddress .= " ".$row['iway_address2'] : $iwayAddress .= "";
			$row['iway_city'] != "" ? $iwayAddress .= " ".$row['iway_city'] : $iwayAddress .= "";
			$row['iway_pin_code'] != "" ? $iwayAddress .= " ".$row['iway_pin_code'] : $iwayAddress .= "";
			$row['iway_state'] != "" ? $iwayAddress .= " ".$row['iway_state'] : $iwayAddress .= "";
		}
		
		//medium
		$sqlMedium = "SELECT e.medium_code, institution_name  FROM iib_exam_candidate e, iib_candidate c WHERE c.membership_no='$membershipNo' ".
			" AND c.membership_no=e.membership_no AND exam_code='$examCode' AND subject_code='$subjectCode' ";
		if ($commonDebug)
		{
			print("\n".$sqlMedium);
		}
		$resMedium = @mysql_query($sqlMedium);
		list($mediumCode, $institutionName) = @mysql_fetch_row($resMedium);
		$strMedium = $aMedium[$mediumCode];

		//marks
		$sqlMarks = "SELECT total_marks, pass_mark FROM iib_exam_subjects WHERE exam_code='$examCode' ".
			" AND subject_code='$subjectCode' AND online='Y' ";
		if ($commonDebug)
		{
			print("\n".$sqlMarks);
		}
		$resMarks = @mysql_query($sqlMarks);
		list($totalMarks, $passMark) = @mysql_fetch_row($resMarks);			
		
		//scores
		$sqlScores = "SELECT score FROM iib_candidate_scores WHERE membership_no='$membershipNo' AND subject_code='$subjectCode' ";
		if ($commonDebug)
		{
			print("\n".$sqlScores);
		}
		$resScores = @mysql_query($sqlScores);
		list($score) = @mysql_fetch_row($resScores);
		$score = $score;
		$sqlQnsSum = "SELECT no_of_questions FROM iib_qp_weightage WHERE question_paper_no=$questionPaperNo AND ".
			"exam_code='$examCode' AND subject_code='$subjectCode' ";
		if ($commonDebug)
		{
			print("\n".$sqlQnsSum);
		}
		$resQnsSum = @mysql_query($sqlQnsSum);
		$qnsSum = 0;
		while (list($no_of_qns) = @mysql_fetch_row($resQnsSum))
		{
			$qnsSum += $no_of_qns;
		}
		$sqlQnsIds = "SELECT question_id FROM iib_question_paper_details WHERE question_paper_no=$questionPaperNo  ORDER BY display_order";
                $resQnsIds = @mysql_query($sqlQnsIds);
                while(list($quesIdsVar) = @mysql_fetch_row($resQnsIds)) {
                        $quesIdsArr[] = $quesIdsVar;
                }
		//$sqlQns = "SELECT question_id, answer FROM iib_question_paper_details WHERE question_paper_no=$questionPaperNo  ORDER BY display_order ";
		$sqlQns = "select question_id, answer from iib_response where question_paper_no=$questionPaperNo and id in ( select  max(id) from iib_response where question_paper_no ='$questionPaperNo' group by question_id) ORDER BY display_order";
		if ($commonDebug)
		{
			print("\n".$sqlQns);
		}
		$resQns = @mysql_query($sqlQns);
		$unAttQns = 0;
		$attQns = 0;
		$aQuestions = array();
		while (list($questionID, $answer) = @mysql_fetch_row($resQns)) {
                        $ansQuestionId[] = $questionID;
                        $ansQuesAnswer[$questionID] = $answer;
                }
                $arrDiffQID = array_diff($quesIdsArr, $ansQuestionId);
                foreach($arrDiffQID as $qUnAnsVal) {
                        if($qUnAnsVal != '') $ansQuesAnswer[$qUnAnsVal] = '';
                }

                foreach($quesIdsArr as $ansKey) {
                        if ($ansQuesAnswer[$ansKey] == "") {
                                $unAttQns += 1;
                                $aQuestions[$ansKey] = $ansQuesAnswer[$ansKey];
                        }
                        else {
                                $attQns += 1;
                                $aQuestions[$ansKey] = $ansQuesAnswer[$ansKey];
                        }
                }

		/*while (list($questionID, $answer) = @mysql_fetch_row($resQns))
		{
			if ($answer == "")
			{
				$unAttQns += 1;
				$aQuestions[$questionID] = $answer;
			}
			else
			{
				$attQns += 1;
				$aQuestions[$questionID] = $answer;
			}
		} *///end of while
	} // else question paper
	$subjectCode = "";
	$examCode = "";

?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<SCRIPT SRC="http://sifyitest.com/js/YogeshEN.js" TYPE="text/javascript"></SCRIPT>
<STYLE TYPE="text/css">
  @font-face {
    font-family: DVBW-TTYogeshEN;
    font-style:  normal;
    font-weight: normal;
    src: url(http://sifyitest.com/eot/DVBWTTY0.eot);
  }
  .qn
  {
	 font-family:'DVBW-TTYogeshEN'; font-weight:bold; font-size:16px
  }
  .ans
  {
	font-family:'DVBW-TTYogeshEN'; font-weight:normal; font-size:16px
  }
</STYLE>
</HEAD>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0 onLoad='this.focus()'>   		
<table width="640" border="0" cellspacing="0" cellpadding="0">
	<tr> 
	<!--	<td><img src="images/logo.png" width="640" height="70"></td> -->
	         <? include("includes/header.php"); ?>
	</tr>
	<tr>
		<!-- <td width="640" bgcolor="7292C5">&nbsp;</td>-->
	</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="640" background="images/tile.jpg">
	<tr>
		<td>
		<br>
		<table border="0" cellspacing="0" cellpadding="10" width="100%" align=center>
			<tr>
				<td colspan=6 class="greybluetext10" bgcolor="#D1E0EF"><b class="arial11a">Candidate Report</b></td>
			</tr>
			<tr>
				<td class=greybluetext10><b>Name </b></td><td class=greybluetext10><b> : </b></td>
				<td class=greybluetext10 colspan=4><?=$memberName ?></td>
			</tr>
			<tr>
				<td class=greybluetext10><b>Address </b></td><td class=greybluetext10><b> : </b></td>
				<td class=greybluetext10 colspan=4><?=$memberAddress ?></td>
			</tr>
			<tr>
				<td class=greybluetext10><b>Exam </b></td><td class=greybluetext10><b> : </b></td>
				<td class=greybluetext10 colspan=4><?=$examName ?></td>
			</tr>
			<tr>
				<td class=greybluetext10><b>Subject </b></td><td class=greybluetext10><b> : </b></td>
				<td class=greybluetext10 colspan=4><?=$subjectName ?></td>
			</tr>
			<tr>
				<td class=greybluetext10><b>Exam Date </b></td><td class=greybluetext10><b> : </b></td>
				<td class=greybluetext10 colspan=4><?=$dispExamDate ?></td>
			</tr>
			<tr>
				<td class=greybluetext10><b>Exam Time </b></td><td class=greybluetext10><b> : </b></td>
				<td class=greybluetext10 colspan=4><?=substr($examTime,0,5) ?> hrs</td>
			</tr>
			<tr>
				<td class=greybluetext10><b>Venue </b></td><td class=greybluetext10><b> : </b></td>
				<td class=greybluetext10 colspan=4><?=$iwayAddress ?></td>
			</tr>
			<tr>
				<td class=greybluetext10><b>Medium </b></td><td class=greybluetext10><b> : </b></td>
				<td class=greybluetext10 colspan=4><?=$strMedium ?></td>
			</tr>
			<tr>
				<td class=greybluetext10><b>Institution </b></td><td class=greybluetext10><b> : </b></td>
				<td class=greybluetext10 colspan=4><?=$institutionName ?></td>
			</tr>
			<tr>
				<td class=greybluetext10 width=20%><b>Total No. of Questions </b></td><td class=greybluetext10 width=5%><b> : </b></td>
				<td class=greybluetext10 width=25%><?=$qnsSum ?></td>
				<td class=greybluetext10 width=20%><b>Total Marks </b></td><td class=greybluetext10 width=5%><b> : </b></td>
				<td class=greybluetext10 width=25%><?=$totalMarks ?></td>
			</tr>
			<tr>
				<td class=greybluetext10><b>No. of Attempted Questions </b></td><td class=greybluetext10><b> : </b></td>
				<td class=greybluetext10><?=$attQns ?></td>
				<td class=greybluetext10><b>Pass Mark </b></td><td class=greybluetext10><b> : </b></td>
				<td class=greybluetext10><?=$passMark ?></td>
			</tr>
			<tr>
				<td class=greybluetext10><b>No. of Unattempted Questions </b></td><td class=greybluetext10><b> : </b></td>
				<td class=greybluetext10><?=$unAttQns ?></td>
				<td class=greybluetext10><b>Marks Obtained </b></td><td class=greybluetext10><b> : </b></td>
				<td class=greybluetext10><?=$score ?></td>
			</tr>
			<?php
			if (count($aQuestions) > 0)
			{
			?>
			<tr>
				<td colspan=6>
				<table align=center width=100% cellpadding=2 cellspacing=2>
			<?php
				$nRow = 0;
				if ($strMedium == "HINDI")
				{
					$tableName = "iib_section_questions_hindi";
				}
				else
				{
					$tableName = "iib_section_questions";
				}
				$alpha = array("a","b","c","d","e");
				foreach ($aQuestions as $questionID=>$markedAnswer)
				{				
					$nRow++;
					$sql = "SELECT question_text, correct_answer, marks, question_type FROM $tableName WHERE question_id=$questionID ";					
					$res = @mysql_query($sql);
					$row = @mysql_fetch_array($res);
					$question_type = $row['question_type'];
					$questionText = $row['question_text'];
					$correctAnswer = $row['correct_answer'];
					if ($correctAnswer != "")
					{
						$sel = '';
						if($question_type=='M'){
							$expcans = explode(',',$correctAnswer);
							$n=0;
							foreach($expcans as $k => $v){
								$n++;
								$sel .= "option_".$v;
								if($n<sizeof($expcans))
									$sel .=',';
							}

							$sqlA = "SELECT $sel FROM $tableName WHERE question_id=$questionID ";
						//	print $sqlA;					
							$resA = @mysql_query($sqlA);
							$rowA = @mysql_fetch_row($resA);
							$cAns = join(',',$rowA);
						}else{
							$sqlA = "SELECT option_".$correctAnswer." FROM $tableName WHERE question_id=$questionID ";
						//	print $sqlA;					
							$resA = @mysql_query($sqlA);
							$rowA = @mysql_fetch_row($resA);
							$cAns = $rowA[0];
						}
					}
					else
					{
						$cAns = "";
					}
					if ($markedAnswer != "")
					{
						$selm = '';
						if($question_type=='M'){
							$expcandans = explode(',',$markedAnswer);
							$n1=0;
							foreach($expcandans as $k1 => $v1){
								$n1++;
								$selm .= "option_".$v1;
								if($n1<sizeof($expcandans))
									$selm .=',';
							}

							$sqlA = "SELECT $selm FROM $tableName WHERE question_id=$questionID ";
						//	print $sqlA;					
							$resA = @mysql_query($sqlA);
							$rowA = @mysql_fetch_row($resA);
							$mAns = join(',',$rowA);
						}else{
							$sqlA = "SELECT option_".$markedAnswer." FROM $tableName WHERE question_id=$questionID ";					
						//	print $sqlA;					
							$resA = @mysql_query($sqlA);
							$rowA = @mysql_fetch_row($resA);
							$mAns = $rowA[0];
						}
					}
					else
					{
						$mAns = "";
					}
					
					$marks = $row['marks'];
			?>
			
					<tr>
						<td class=greybluetext10 colspan=2>
						<?php 
						if ($strMedium == "HINDI")
						{
							print("<font class=qn>");
							print($nRow.".  ".$questionText." [Mark/s - $marks]"); 
							print("</font>");
						}
						else
						{
							print("<b>");
							print($nRow.".  ".$questionText." [Mark/s - $marks]"); 
							print("</b>");
						}
						?>
						</td>
					</tr>
					<tr>
						<td class=greybluetext10>
						<?php 
							if ($markedAnswer != "")
							{
								print("Marked Answer : ".$alpha[$markedAnswer-1].") ");
								if ($strMedium == "HINDI")
								{
									print("<font class=ans>$mAns");							
									print("</font>");
								}
								else
								{
									print($mAns);
								}								 
							}
							else
							{
								print("Marked Answer : - "); 
							}
						?>
						</td>
					</tr>
					<tr>
						<td class=greybluetext10>
						<?php 
							if ($correctAnswer != "")
							{
								print("Correct Answer : ".$alpha[$correctAnswer-1].") ");
								if ($strMedium == "HINDI")
								{
									print("<font class=ans>$cAns");							
									print("</font>");
								}
								else
								{
									print($cAns);
								}		
							}
							else
							{
								print("Correct Answer : - "); 
							}
						?>
						</td>
					</tr>
				
			<?php
				} // end of foreach
			?>
				</table>
				</td>
			</tr>
			<?php
			} // end of count questions
			?>
		</table>		
		
		</td>
		</tr>
		<tr>
			<td><br></td>
		</tr>
		<Tr>
			<td>
			<table border=0 width=640>
				<tr>
					<td><img src="images/spacer.gif" width=273 height=17></td>
					<td background="images/print.gif"><a href="javascript:window.print()"><img src="images/spacer.gif" width=38 height=17 border=0></a></td>
					<td background="images/exit.gif"><a href="javascript:window.close()"><img src="images/spacer.gif" width=29 height=17 border=0></a></td>								
					<td><img src="images/spacer.gif" width=273 height=17></td>																
				</tr>
			</table>
			</td>
		</Tr>
		<tr>
			<td><br><br></td>
		</tr>
	</table>
	<table border="0" cellpadding="0" cellspacing="0" width="640">
		<tr>
     <!--			<td width="640" bgcolor="7292C5">&nbsp;</td> -->
	             <? include("includes/footer.php"); ?>
		</tr>
	</table>
	<br>
</body>
</html>
