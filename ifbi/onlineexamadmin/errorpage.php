<?php

/****************************************************
* Application Name            :  IIB IWAY BULKUPLOAD
* Module Name                 :  Error Page
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  settings.php
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  Bala
* Created ON                  :  Feb 9 2005
* Last Modified By            :  B.Devi
* Last Modified Date          :  29-03-2006
* Description                 :  Whenever any error then , it is redirected to this page.
*****************************************************/
require_once("sessionchk.php");
require_once("dbconfig.php");
include ('includes/settings.php');

$statusid = trim($_GET['status']);//variable to get the error id
?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
.textbox        
{ 
font-family: Verdana, Arial, Helvetica, sans-serif; 
font-size: 10px; font-style: normal; 
text-decoration: none; 
height: 17px; 
border: 1px #000000 solid
}
</STYLE>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/validations.js"></script>
</head>

<BODY leftMargin=0 topMargin=0 LeftMargin=0 marginheight=0 marginwidth=0>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
<!--	<TR><!-- Topnav Image -->
   <!--     <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR> -->
     <tr><td width="780"><?include("includes/header.php");?></td></tr>
    <TR>
        
    </TR>
	<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>
    <TR>
    	<TD background=images/tile.jpg vAlign=top width=780 align=center>
<!-- header_bof //-->
<?//php include('includes/header.php'); ?>
<table cellspacing="0" cellpadding="0" width="100%" border="0" align="center">
  <tr>
    <td width="5%"></td>
    <td width="90%" valign="top">

<?php
	if ($statusid != 1){
?>
		<table width="75%"  border="0" align="center" cellpadding="5" cellspacing="1">
      <tr align="center">
        <td colspan="2" class="pageTitle">
					<?php	echo $status1[$statusid];?>				
				</td>
      </tr>
    </table>
    </td>
    <td width="5%"  valign="top"></td>
  </tr>
</table>

<?php
	}else{
?>
<table width="95%" align="center" cellpadding="2"  >
  <tr>
    <td>
      <p>Bulk Upload Utility</p>
      </td>
  </tr>
</table>
<br><br>
<table width="75%"  border="0" align="center" cellpadding="5" cellspacing="1">
      <tr>
        <td><?php echo $status1[1]; ?> <br><br> <a href="index.php"><img src="images/login.gif" border="0" align="absmiddle"></a> Again!</td>
      </tr>
    </table>
    </td>
  </tr>
</table>

<?php
}
?>

        </TD>
	</TR>
    <TR>
    	<?include("includes/footer.php");?>
    </TR>
</TABLE>
</center>
</body>

</html>
