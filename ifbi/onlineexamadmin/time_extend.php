<?php ob_start();
$admin_flag=4;
require_once "sessionchk.php";
require_once("dbconfig.php");
require_once("constants.inc");
//require_once("$candpath/constants.inc");
//print_r($_POST);
$errmsg[0]="No records found";
$errmsg[1]="There is error in insertion into log table";
$errmsg[2]="There is error in deletion of scores tables";
$errmsg[3]="Record updated successfully";
$errmsg[4]="The Time to be extended should not exceed total time.";
$errmsg[5]="Problem in extending time and problem in clearing the logs!!!";
$errmsg[6]="Problem in extending time!!!";
$test_update = 0;
$msg=isset($_REQUEST["msg"]) ? $_REQUEST["msg"] : '';

if($_POST)
{
	$memno=isset($_REQUEST["tMemNo"]) ? $_REQUEST["tMemNo"] : '';
	$disp=isset($_REQUEST["disp"]) ? $_REQUEST["disp"] : '';
	$submitted=isset($_REQUEST["submitted"]) ? $_REQUEST["submitted"] : '';
	$subject=isset($_REQUEST["subject"]) ? $_REQUEST["subject"] : '';
	$update=isset($_REQUEST["update"]) ? $_REQUEST["update"] : 'false';
	$subjectCode=isset($_REQUEST["subject_code"]) ? $_REQUEST["subject_code"] : '';
	$examCode=isset($_REQUEST["exam_code"]) ? $_REQUEST["exam_code"] : '';
	$time_extend=isset($_REQUEST["t_extendtime"]) ? $_REQUEST["t_extendtime"] : '';
	$htotaltime=isset($_REQUEST["hTotaltime"]) ? $_REQUEST["hTotaltime"] : '';
	$time_extend=$time_extend * 60;
	$htotaltime = $htotaltime *60;
	$total_time_upd=$time_extend;
	
	$sqlselectexam = "select subject_duration from iib_exam_subjects where exam_code='$examCode' AND subject_code='$subjectCode'";		
	$resultexam = @mysql_query($sqlselectexam);
	if(mysql_num_rows($resultexam) > 0)
	{
		list($examtotalTime) = mysql_fetch_row($resultexam);
		$totalTime=$examtotalTime/60;				
	}				
	if($memno!='' && $disp == 1)
	{
		 $sql_exam="select distinct a.exam_code , b. exam_name from iib_candidate_iway a , iib_exam b where a. exam_code = b. exam_code and a. membership_no='$memno' and a.exam_date=current_date()";
		$res_exam=mysql_query($sql_exam);
		$nexam = mysql_num_rows($res_exam);
		if($nexam <= 0)
		{
			header("Location:time_extend.php?msg=0");
			exit;
		}	
	}
	if($submitted=='true' || $subject=='true')
	{
		$sql_sub="select distinct subject_code , subject_name from iib_exam_subjects where exam_code='$examCode'";
		$res_sub=mysql_query($sql_sub);
		$sub_cnt=mysql_num_rows($res_sub);
	}
	if($submitted=='true' && $memno!='' && $subjectCode!='')
	{
		
		/* compare candidate test Lastupdatetime and answer last updated time if answer updatetime greater than test updated time need to update test update time with answer update time */
		
		$todaydate = date("Y-m-d");
		$todayStart = $todaydate." 00:00:00";
		$todayEnd = $todaydate." 23:59:59";
		
		$sqlSelect1 = " SELECT TIME_TO_SEC(DATE_FORMAT(last_updated_time,'%T')), test_id, test_status,question_paper_no ".
		" FROM iib_candidate_test WHERE membership_no='$memno' AND exam_code='$examCode' AND subject_code='$subjectCode' AND ".
		" last_updated_time between '$todayStart' AND '$todayEnd'  ".
		" order by last_updated_time desc LIMIT 1 ";
						
		$result1 = mysql_query($sqlSelect1);
		if (mysql_num_rows($result1) > 0)
		{
			list($lastupdatedtime, $testID, $testStatus,$question_paper_no) = mysql_fetch_row($result1);			
			 /*$sqlSelect2 = " SELECT TIME_TO_SEC(DATE_FORMAT(updated_time,'%T')) , updated_time FROM iib_question_paper_details WHERE ".   
								"question_paper_no='$question_paper_no' order by updated_time desc LIMIT 1 "; */
			$sqlSelect2 = " SELECT TIME_TO_SEC(DATE_FORMAT(updatedtime,'%T')) , updatedtime FROM iib_response WHERE "."question_paper_no='$question_paper_no' order by updatedtime desc LIMIT 1 ";
			$result2 = mysql_query($sqlSelect2);
			list($answerupdatedtime,$answerupdatedtime1) = mysql_fetch_row($result2);
			if($answerupdatedtime > $lastupdatedtime){
			$sqlUpdate = " UPDATE iib_candidate_test set last_updated_time='$answerupdatedtime1' where test_id='$testID'";
				mysql_query($sqlUpdate);
            }
		 }
		/* End compare candidate test */				
               
		 $sql_cand_detail="select TIME_TO_SEC(DATE_FORMAT(last_updated_time,'%T')) ".
		" - TIME_TO_SEC(DATE_FORMAT(start_time,'%T')),test_id,question_paper_no,test_status,start_time,last_updated_time,end_time,total_time,time_extended from iib_candidate_test where membership_no='$memno' and subject_code='$subjectCode' and left(last_updated_time,10) = current_date() and current_session='Y' order by last_updated_time desc LIMIT 1";
		$res_cand_detail=mysql_query($sql_cand_detail);
		$cand_detail_cnt=mysql_num_rows($res_cand_detail);
		if($cand_detail_cnt > 0)
		{
			list($timetaken_calc,$testId,$qpNo,$testStatus,$sTime,$l_uTime,$eTime,$tTime,$time_extended)=mysql_fetch_row($res_cand_detail);
			$tTime = ($tTime+$time_extended) - $timetaken_calc;
			if($tTime < 0 )
			{
				$tTime=0;
			}
			if($eTime=='')
			{
				$eTime="---"; 	
			}
		} 
		else
		{
			header("Location:time_extend.php?msg=0");
			exit;
		}
	}
	if($update=='true')
	{
		$updatedTime=date("Y-m-d H:i:s");
		
		$sql_insert_test_log="insert into iib_candidate_test_log (test_id,membership_no,exam_code,subject_code,question_paper_no,ta_override,login_override,test_status,start_time,last_updated_time,end_time,total_time,time_taken,time_left,current_session,updatedtime,time_extended) select test_id,membership_no,exam_code,subject_code,question_paper_no,ta_override,login_override,test_status,start_time,last_updated_time,end_time,total_time,time_taken,time_left,current_session,'$updatedTime',$time_extend from iib_candidate_test where membership_no='$memno' and subject_code='$subjectCode' and test_id='$testId'";

		$res_insert_test_log=mysql_query($sql_insert_test_log);
		
		
		$sql_insert_question="insert into iib_question_paper_details_log (question_paper_no,subject_code,section_code,question_id,answer,answer_order,display_order,updatedtime) select question_paper_no,subject_code,section_code,question_id,answer,answer_order,display_order,'$updatedTime' from iib_question_paper_details where question_paper_no='$qpNo'";

		$res_insert_question=mysql_query($sql_insert_question);
				
			
		$sql_sel_candidate_scores="select score,exam_date,time_taken,result from iib_candidate_scores where membership_no='$memno' and exam_code='$examCode' and subject_code='$subjectCode'";
		$res_sel_candidate_scores=mysql_query($sql_sel_candidate_scores);
		$cnt_scores=mysql_num_rows($res_sel_candidate_scores);
			if($cnt_scores > 0)
			{
				list($score,$EDate,$TTaken,$result)=mysql_fetch_row($res_sel_candidate_scores);
				$sql_insert_scores_log="insert into iib_candidate_scores_log (membership_no,exam_code,subject_code,score,exam_date,time_taken,result,updatedtime) values ('$memno','$examCode','$subjectCode','$score','$EDate','$TTaken','$result','$updatedTime')";
				$res_insert_scores_log=mysql_query($sql_insert_scores_log);
				if(!$res_insert_scores_log || !$res_insert_test_log || !$res_insert_question)
				{
					header("Location:time_extend.php?msg=1");
					exit;
				}
				else
				{
					$sql_del_scores="delete from iib_candidate_scores where membership_no='$memno' and exam_code='$examCode' and subject_code='$subjectCode'";
					$res_del_scores=mysql_query($sql_del_scores);
					if(!$res_del_scores)
					{
						header("Location:time_extend.php?msg=2");
						exit;
					}
					else
					{
						$test_update = 1;
					}
				}
			}
			else
			{
				if(!$res_insert_test_log || !$res_insert_question)
				{
					header("Location:time_extend.php?msg=1");
					exit;
				}
				else
				{
					$test_update = 1;
				}
			}
			if($test_update == 1)
			{
          
                $qryTrack="UPDATE iib_candidate_tracking set user_status=0 where membership_no='$memno'";
                mysql_query($qryTrack);

                $sql_update_test="update iib_candidate_test set time_extended=time_extended+$total_time_upd,test_status='IC',time_left='',time_taken='',end_time='' where membership_no='$memno' and  test_id='$testId'";
				$res_update_test=mysql_query($sql_update_test);
				if($res_update_test)
				{					
					@header("Location:time_extend.php?msg=3");
					exit;
				}
				//on failure of resetting the time, delete the written log
				else
				{
					$sql_delete_test_log="delete from iib_candidate_test_log where membership_no='$memno' and subject_code='$subjectCode' and test_id='$testId' and updatedtime='$updatedTime'";
					$res_delete_test_log=mysql_query($sql_delete_test_log);
					$sql_delete_question_log="delete from iib_question_paper_details_log where question_paper_no='$qpNo' and updatedtime='$updatedTime'";
					$res_delete_question_log=mysql_query($sql_delete_question_log);
					$sql_delete_scores_log="delete from iib_candidate_scores_log where membership_no='$memno' and exam_code='$examCode' and subject_code='$subjectCode' and updatedtime='$updatedTime'";
					$res_delete_scores_log=mysql_query($sql_delete_scores_log);
					if(!$res_delete_test_log || !$res_delete_question_log || !$res_delete_scores_log)
					{
						header("Location:time_extend.php?msg=5");
						exit;
					}
					else
					{
						header("Location:time_extend.php?msg=6");
						exit;
					}
				}
			
			}
		}
	}
?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='JavaScript' src="./includes/validations.js"></script>
<script language="javascript">
function getvalues(e){
   	var keyCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode; 
    frm = document.frmextend;
    if (keyCode == 13 && frm.tMemNo.value != "" && frm.tMemNo.readOnly == false ){
        frm.action="time_extend.php?disp=1";
        frm.submit();
    }
}

function getsubjects()
{
	if (document.frmextend.exam_code.selectedIndex == 0)
	{
		alert("Please select the exam ");
		document.frmextend.exam_code.focus();
		return false;
	}

	document.frmextend.subject.value='true';
	document.frmextend.submitted.value='false';
	document.frmextend.submit();

}
function getcand()
{
	frm = document.frmextend;
	if (frm.exam_code.selectedIndex == 0)
	{
		alert("Please select the exam ");
		document.frmextend.exam_code.focus();
		return false;
	}
	if (frm.subject_code.selectedIndex == 0)
	{
		alert("Please select the Subject ");
		frm.exam_code.focus();
		return false;
	}
	frm.subject.value='true';
	frm.submitted.value='true';
	frm.submit();
}
function frmreset()
{
	frm=document.frmextend;
	frm.tMemNo.value='';
	frm.action="time_extend.php";
	frm.submit();
}
function validate()
{
	frm=document.frmextend;
	if(frm.t_extendtime.value=='')
	{
		alert("The time to be extended is empty");
		frm.t_extendtime.focus();
		return;
	}
	else
	{
		myRegExp = new RegExp("^-?[0-9]*$");
		val=frm.t_extendtime.value;
		if (!myRegExp.test(val))
		{
			alert("Time to be extended should contain only numbers.");
			frm.t_extendtime.focus();
			return false;
		}
		/*var myVal = parseInt(frm.t_extendtime.value);
		if (isNaN(myVal)) {
		    alert("Time to be extended should contain only numbers.");
		    frm.t_extendtime.value="";
		    frm.t_extendtime.focus();
		    return;
		}*/
		/*else 
		{
			alert("t_extendtime is not empty");
		    // OK, operate on the value numerically
		}*/
		if(frm.t_extendtime.value == 0)
		{
			alert("Time to be extended cannot be Zero.");
			frm.t_extendtime.focus();
			return false;
		}
		TotalTimeLeft= eval(frm.t_extendtime.value) + eval(frm.hTotaltime.value);
		if(eval(TotalTimeLeft) > eval(frm.ConstTotaltime.value))
		{
			alert("The Sum of Time Left and Time to be extended should not exceed total time for the Exam.");
			frm.t_extendtime.focus();
		    return;
		}
		else if( eval(TotalTimeLeft) < 0 )
		{
			alert("The Sum of Time Left and Time to be extended  should not be less than Zero.");
			frm.t_extendtime.focus();
		    return;
		}

	}
	if(!confirm("Are you sure to Submit ?"))
	{
		return;
	}
	else
	{
		frm.update.value='true';
		frm.submitted.value='true';
		frm.submit();
	}
}
</script>
</HEAD>
<BODY leftMargin=0 topMargin=0 LeftMargin=0 marginheight=0 marginwidth=0>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
	<tr><td width="780"><?include("includes/header.php");?></td></tr>
  	<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>
    <TR>
    	<TD background=images/tile.jpg vAlign=top width=780 align=center>
    	<form name='frmextend' method="post" onSubmit="return false;">  
    	<input type='hidden' name='subject' value=''>
    	<input type='hidden' name='update' value=''>
		<input type='hidden' name='submitted'>      
        <table width=780 cellpadding=0 cellspacing=5 border=0>
        	<tr>
				<td colspan=2 align="center"><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2>Candidate Time Extension</font></b></td>
            </tr>                
			<? //if ($errmsg <> NULL ){ ?>
        	<tr>
				<td colspan=2 align="center" class="alertmsg"><? echo $errmsg[$msg]; ?></td>
            </tr>                
			<?// }
			?>
			<tr>
            	<td align="right" width=390 class="greybluetext10" valign=top>Member Number : </td>
                <td align="left" valign=top><input type="text" class="textbox" size=30 name="tMemNo" value=<? echo "'".$memno."'"; if ($memno<> NULL) echo " readonly"; ?> onKeyPress="getvalues(event)"; maxlength=20><font size=1>Press enter after typing the Membership No</font></td>
			</tr>
			<?
			if($memno!='' && $disp == 1 && $nexam > 0)
			{?>
			<tr>
    	        	<td align="right" width=390 class="greybluetext10" valign=top>Exam : </td>
        	    	<td align="left" valign=top class="greybluetext10">
            			<select name='exam_code' class="greybluetext10" style={width:275px} onchange='javascript:getsubjects();'>
            				<option value=''>--Select--</option>
            				<?
            					while (list($code,$name) = mysql_fetch_row($res_exam))
            					{

	            					print("<option value='$code' ");
	            						if ($examCode == $code)
	            						{
		            						print(" selected ");
	            						}
	            					print(">$name</option>");
	            				}
	            		?>
            			</select>            		</td>
            	</tr>
            	<?
            	if($sub_cnt>0)
            	{
            	?>
            	<tr>
            		<td align="right" width=390 class="greybluetext10" valign=top> Subjects: </td>
            		<td align="left" valign=top class="greybluetext10">
            			<select name='subject_code' class="greybluetext10" style={width:350px} onchange='javascript:getcand();'>
            				<option value=''>--Select--</option>

            				<?
            					while (list($Scode,$Sname) = mysql_fetch_row($res_sub))
            					{
	            					print("<option value='$Scode' ");
	            					if ($subjectCode == $Scode)
	            					{
		            					print(" selected ");
	            					}
	            						print(">$Sname</option>");
	            				}
	            		?>
            			</select>            		</td>
            	</tr>
            	<?
            	if(($submitted=='true') && ($cand_detail_cnt > 0))
            	{
	            	//$testStatus,$sTime,$l_uTime,$eTime,$tTime
            	?>
            	<tr>
	            	<td align="right" width=390 class="greybluetext10" valign=top>Test Id: </td>
	                <td align="left" valign=top class=greybluetext10><?=$testId?></td>
				</tr>
            	<tr>
	            	<td align="right" width=390 class="greybluetext10" valign=top>Question Paper No: </td>
	                <td align="left" valign=top class=greybluetext10><?=$qpNo?></td>
				</tr>				
            	<tr>
	            	<td align="right" width=390 class="greybluetext10" valign=top>Test Status: </td>
	                <td align="left" valign=top class=greybluetext10><?=$testStatus?></td>
				</tr>
				<tr>
	            	<td align="right" width=390 class="greybluetext10" valign=top>Start Time: </td>
	                <td align="left" valign=top class=greybluetext10><?=$sTime?></td>
				</tr>
				<tr>
	            	<td align="right" width=390 class="greybluetext10" valign=top>Last Updated Time: </td>
	                <td align="left" valign=top class=greybluetext10><?=$l_uTime?></td>
				</tr>
				<tr>
	            	<td align="right" width=390 class="greybluetext10" valign=top>End Time: </td>
	                <td align="left" valign=top class=greybluetext10><?=$eTime?></td>
				</tr>			
               	<tr>
                    <td colspan=2 align="center"  class="greybluetext10" valign=top>
                <!-- Table start --> 
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                 <td height="24" align="center" valign="top" bgcolor="#000066">
                 <table width="100%" border="1" align="left" cellpadding="5" cellspacing="0" bordercolor="#000066" bgcolor="#FFFFFF" style="border-left-width:thin">
                    <tr>
                      <td width="6%" bgcolor="#0080FF" class="whitecolor"><div align="center"><strong>Q.No</strong></div></td>
                      <td width="22%" bgcolor="#0080FF" class="whitecolor"><div align="center"><strong>Answer</strong></div></td>
                      <td width="25%" bgcolor="#0080FF" class="whitecolor"><div align="center"><strong>Answered Time</strong></div></td>
                    </tr>                    
<?PHP
                        $qpcnt=0;
                        $ansqpcnt=0;    
                        /*Getting Answer Details */    
                        /*$qryAnswer = "select display_order,answer,updated_time from iib_question_paper_details where question_paper_no='$question_paper_no'"
                                . " order by display_order";
                            $resAnswer = mysql_query($qryAnswer);
                            while($rAnswer = mysql_fetch_object($resAnswer)){                                     
                               echo '<tr>';							 
                                echo "<td class='greybluetext10'>".$rAnswer->display_order."&nbsp;</td>
                                      <td class='greybluetext10'>".$rAnswer->answer."&nbsp;</td>
                                      <td class='greybluetext10'>".$rAnswer->updated_time."&nbsp;</td>
                                      </tr>";
                               $qpcnt++;
                               if($rAnswer->answer!='' && $rAnswer->answer!='NULL')
                                   $ansqpcnt++;

                            } */
				$sqlQnsIds = "SELECT question_id FROM iib_question_paper_details WHERE question_paper_no='$question_paper_no'  ORDER BY display_order";
                               $resQnsIds = @mysql_query($sqlQnsIds);
                               while(list($quesIdsVar) = @mysql_fetch_row($resQnsIds)) {
                               	$quesIdsArr[] = $quesIdsVar;
                               }
				$qryAnswer = "select question_id, answer, updatedtime,display_order from iib_response where id in ( select  max(id) from iib_response where question_paper_no ='$question_paper_no' group by question_id) ORDER BY display_order";
				//echo $qryAnswer;
                            $resAnswer = mysql_query($qryAnswer);
                                                        while (list($question_id, $answer, $updatedtime) = @mysql_fetch_row($resAnswer)) {
                                                                $ansQuestionId[] = $question_id;
                                                                $ansQuesAnswer[$question_id][0] = $answer;
                                                                $ansQuesAnswer[$question_id][1] = $updatedtime;
                                                        }
                                                        $arrDiffQID = array_diff($quesIdsArr, $ansQuestionId);
                                                        foreach($arrDiffQID as $qUnAnsVal) {
                                                                if($qUnAnsVal != '') {
                                                                        $ansQuesAnswer[$qUnAnsVal][0] = '';
                                                                        $ansQuesAnswer[$qUnAnsVal][1] = '';
                                                                }
                                                        }
                                                        foreach($quesIdsArr as $ansKey) {
                                                                if ($ansQuesAnswer[$ansKey][0] == "") {
                                                                        $unAttQns += 1;
                                                                        echo '<tr>';
                                                                        echo "<td class='greybluetext10'>".$ansKey."&nbsp;</td>
                                      <td class='greybluetext10'>".$ansQuesAnswer[$ansKey][0]."&nbsp;</td>
                                      <td class='greybluetext10'>".$ansQuesAnswer[$ansKey][1]."&nbsp;</td>
                                      </tr>";
                                                                }
                                                                else {
                                                                        $attQns += 1;
                                                                        echo '<tr>';
                                                                        echo "<td class='greybluetext10'>".$ansKey."&nbsp;</td>
                                      <td class='greybluetext10'>".$ansQuesAnswer[$ansKey][0]."&nbsp;</td>
                                      <td class='greybluetext10'>".$ansQuesAnswer[$ansKey][1]."&nbsp;</td>
                                      </tr>";
                                                                }
                                                        }


                            echo '<tr>';							 
                            echo "<td colspan=2 class='greybluetext10'>No.questions Answered:</td>
                             <td class='greybluetext10'>".$attQns."</td>
                               </tr>";
                            echo '<tr>';							 
                            echo "<td colspan=2 class='greybluetext10'>No.questions Unanswered:</td>
                                <td class='greybluetext10'>".$unAttQns."</td>
                               </tr>";
 
                      ?>
                  </table></td>
                </tr>
              </table>
                <!-- Table end --> 
                 </td>
				</tr>			
 
                 <tr>
	            	<td align="right" width=390 class="greybluetext10" valign=top>Total time for this Exam : </td>
	                <td align="left" valign=top class=greybluetext10><?=$totalTime?>&nbsp;Mins</td>
				</tr>					
				<tr>
	            	<td align="right" width=390 class="greybluetext10" valign=top>Already Extended Time</td>
	                <td align="left" valign=top class=greybluetext10><?PHP if($time_extended!=''){$time_extendedmin=$time_extended/60; echo $time_extendedmin." Mins";}?></td>
	                <input type=hidden name=hTotaltime value=<?=$tTimeMin?>>
	                <input type=hidden name=ConstTotaltime value=<?=$totalTime?>>
				</tr>
				<tr>
				  <td align="right" class="greybluetext10" valign=top>Total Time Left:: </td>
		          <td align="left" valign=top class=greybluetext10><?
	                $tTimeMin=$tTime/60;
	                $tTimeMin=ceil($tTimeMin);
	                //echo $tTimeMin." Mins (".$tTime." Secs)";
	                echo $tTimeMin." Mins";
	                //echo $tTime." (Secs)";
	                ?></td>
			</tr>
				<tr>
	            	<td align="right" width=390 class="greybluetext10" valign=top>Total Time to be extended<!--<font color=red>*</font>-->: </td>
	                <td align="left" valign=top><input type="text" class="textbox" size=30 name="t_extendtime" value="" maxlength=5><font size=1>In Minutes</font></td>
				</tr>
            	<tr>
					<td colspan=2 align=center>
						<input class="button" type="button" name="sub_save" value="Update" onClick="javascript:validate();">&nbsp;&nbsp;
						<input class="button" type="button" name="sub_reset" value="Reset" onClick="javascript:frmreset();">&nbsp;&nbsp;					</td>
				</tr>
				<!--<tr>
					<td class=greybluetext10 width=100%>
						<font color=red>* </font><b>Total Time to be extended</b> will be the Remaining time that the candidate will be allowed to write the exam 
					</td>
				</tr>-->
			<?	}
			  }
			}
			?>
			</table>
        </form>
        </TD>
	</TR>
	
     <TR>
    	<?include("includes/footer.php");?>
    </TR>
</TABLE>
</FORM>
</center>
</BODY>
</html>
