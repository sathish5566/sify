<?php
require_once("sessionchk.php"); 
require_once("dbconfig.php");

$commondebug=false;
$msg=0;
$time=date(jF_Gis);
	$filepath = "./ta_files/";
	// getting the extension of the file
	$ext=explode('.',$_FILES['userfile']['name']);
	//echo $_FILES['userfile']['name'];

	// only if the file is of .txt format proceed further
	if($ext[1]=='txt')
	{
		$uploadfile = $filepath . $time .$_FILES['userfile']['name'];
	       //echo $filepath."<br>";  
		if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
		} else {
			print "Possible file upload attack!  Here's some debugging info:\n";
			print_r($_FILES);
			exit;
		}
		// get contents of a file into a string
		$filename = $uploadfile;
		if($filename!='')
		{
			$sql_log_insert="insert into iib_ta_log (file,updatedtime) values('$filename',now())";
			//$res_log_insert=mysql_query($sql_log_insert);
		}
		$handle = fopen($filename, "r");
		//Writing the comtents of the file to an ARRAY named $aManual
		while(!feof($handle))
		{
			$aManual[] = fgets($handle);
		}
		 fclose($handle);
		
		if ($commondebug){
			echo "<pre>";
			print_r($aManual);
			echo "</pre>";
		}
		$i=0;
		// Exploding the comma seperated values into an array named $row
		foreach ($aManual as $value)
		{
			if($value!='')
			{	
				$row[$i]=explode(',',$value);
			}
			$i++;
		}	
	
		$cnt= count($row);
		
		/*echo "<pre>";
		print_r($row);
		echo "</pre>";*/
			
		$break='false';
		$actual_less='false';
		$dup_code='';
		$result_vali='false';
		$centre_notEQ='false';
		for($l=0;$l<$cnt;$l++)
		{
		$row[$l][1]=strtoupper(trim($row[$l][1]));
		$cafeStart = substr($row[$l][0], 0, 5);  // To get the first 5 letters i.e., iwfr_
		$cafeEnd = strtoupper(substr($row[$l][0], -7));  // To get the first 7 letters eg., 600001a
		
		$row[$l][0]=$cafeStart."".$cafeEnd;
		
		// Validating the centre code to contain 6 digits and an alphabet, taname to be a string
		//if ((ereg("^[0-9]{6}[a-zA-Z]+$", $row[$l][0])) && (is_numeric($row[$l][1])) && (is_string($row[$l][3])) && (is_string($row[$l][2])))
		/*echo "CAFE END --> ".$cafeEnd."<br>";
		echo "C C --> ".$row[$l][1]."<br>";*/
		
			if ($cafeStart=='iwfr_' && (ereg("^[0-9]{6}[a-zA-Z]+$", $cafeEnd) && (ereg("^[0-9]{6}[a-zA-Z]+$", $row[$l][1])) && (ereg("^[a-zA-Z][a-zA-Z .]*$",trim($row[$l][2])))))
			{
				if($cafeEnd == $row[$l][1])
				{
					//echo "IF --> IF --> ".$row[$l][1]."<br>";
					$result_vali='true';

						if($l=='0') 
						{
							$code[$l]=$row[$l][1];
						}
						else
						{
							//Checking for duplicates
							if((in_array($row[$l][1],$code)))
							{
								$msg=4;
								$dup_code=$row[$l][1];
								$break='true';	
							}
							else
							{
								$code[$l]=$row[$l][1];
							}
						}
						if($break=='true') break;
						$arrcnt[$l]=sizeof($row[$l]);
						
				}
				else
				{
					//echo "ELSE --> ".$row[$l][1]."<br>";
					$centre_notEQ='true';
					$result_CCode=$row[$l][1];
					$break='true';
				}
			}
			//If the validations are not satisfied
			else
			{
				//echo "IF --> ELSE --> ".$row[$l][1]."<br>";
				$result_vali='false';
				$lineNo=$l+1;
				$result_CCode=$row[$l][1];
				$break='true';	
				
			}
		
			if($break=='true') break;
		}//end for loop
		//exit;
		
	
		//$sql_sel="select count(1),centre_code from iib_ta_details_temp group by centre_code";
		$sql_sel="select count(1),centre_code from iib_ta_details group by centre_code";

		$res_sel=mysql_query($sql_sel);
		while(list($No,$Centre_Code)=mysql_fetch_row($res_sel))
		{
			$Centre_Code=strtoupper($Centre_Code);
			$countArr[$Centre_Code]=$No;
		}
		
		/*echo "<PRE>";
		print_r($countArr);
		echo "</PRE>";*/
		//exit;
		$sql_exam_date = "select distinct exam_date from iib_exam_schedule order by exam_date";

#echo $sql_exam_date ; exit ;

		$res_exam_date = mysql_query($sql_exam_date);
		$date_count=0;
		while(list($exam_date) = mysql_fetch_row($res_exam_date))
		{
				$dateArr[$date_count]=$exam_date;
				$date_count++;
		}
		
		$sql_exam_time = "select distinct exam_time from iib_exam_schedule order by exam_time";

#echo $sql_exam_date ; exit ;

		$res_exam_time = mysql_query($sql_exam_time);
		$time_count=0;
		while(list($exam_time) = mysql_fetch_row($res_exam_time))
		{
				$timeArr[$time_count]=$exam_time;
				$time_count++;
		}

		//$sql_ta_iway = "select centre_code, exam_date from iib_ta_iway_temp group by centre_code,exam_date";
$sql_ta_iway = "select centre_code, exam_date from iib_ta_iway group by centre_code,exam_date";

#echo $sql_ta_iway ; exit ;

		$res_ta_iway = mysql_query($sql_ta_iway);
		while(list($cen_Code,$cen_Date) = mysql_fetch_row($res_ta_iway))
		{
				$taIwayArr[$cen_Code][]=$cen_Date;
		}
		

		$fields='false';
		$no_flag='false';
		$dup_flag='false';
		$iway_flag='false';
		$break='false';
		for ($chk=0;$chk<$cnt;$chk++)
		{
			$row_code=strtoupper($row[$chk][1]);
			$cnt_data=$countArr[$row_code];
			$cnt_Iway = count($taIwayArr[$row_code]);
			$cnt_ExamDate = count($dateArr);
			$cnt_ExamDate1 = count($timeArr);
			if($cnt_data=='') $cnt_data=0;
			// to check the no of columns in the file
			if($arrcnt[$chk] > 3)
			{
				$fields='true'	;
				$break='true';
				break;
			}
		

			if ($cnt_data >= 1)
			{
				$dup_Ccode=$row_code;
				$dup_flag='true';
				if($cnt_Iway!=$cnt_ExamDate)
				{
					$iway_flag='true'; //no of entries in the iib_ta_iway table is less than the no of entries in the iib_exam_schedule , proceed with inserting in iib_ta_iway table skipping iib_ta_details
				}
				$break='true';
				break;
			}
			if($break=='true') break;
		}
		//if the no of rows in the file  is != 0 then proceed
		if($cnt!='0')
		{
		  // if actual seats are equal then proceed
			if($centre_notEQ!='true')		
			{
			  if($result_vali=='true')
			  {
				  
						if(($dup_flag!='true') || ($dup_flag=='true' && $iway_flag=='true'))
				  		{	  
							// checking for duplicates
							if($msg!='4')
							{
								if($fields!='true')
								{
																
								$val_flag=false;
								$temp_insert=false;
								for($k=0;$k<$cnt;$k++)
								{
									//assigning the values from the array to the variables 
									$ta_login=trim($row[$k][0]);
									$c_Code=trim($row[$k][1]);
									$name=trim($row[$k][2]);
									$no=$countArr[$c_Code];
									$cafeId=":>".$c_Code;
									
									//Forming queries for tables iib_ta_details , iib_ta_password , iib_ta_iway
									if($k==0)
									{
										if(!count($taIwayArr[$c_Code]) > 0)
										{
											$ta_details="('','$ta_login','$name','','$cafeId','$c_Code','1','0','0')";
											$ta_password="('$ta_login','')";
										}
										for($l=0;$l<$date_count;$l++)
										{
											if(!in_array($dateArr[$l],$taIwayArr[$c_Code]))
											{
												//echo "Inside In_array --> $row_code<br>";
												if($l==0)
												{
													$ta_iway="('$ta_login','$c_Code','$dateArr[$l]','$timeArr[$l]')";
												}
												else
												{
													if($ta_iway!='')
													{
														$ta_iway.=",('$ta_login','$c_Code','$dateArr[$l]','$timeArr[$l]')";
													}
													else
													{
														$ta_iway="('$ta_login','$c_Code','$dateArr[$l]','$timeArr[$l]')";
													}
												}
											}
										}
									}
									else
									{

/*
echo "<PRE>";
print_r($taIwayArr) ;
*/
										if(!count($taIwayArr[$c_Code]) > 0)
										{

											$ta_details.=",('','$ta_login','$name','','$cafeId','$c_Code','1','0','0')";
											$ta_password.=",('$ta_login','')";
										}
										for($l=0;$l<$date_count;$l++)
										{
											if(!in_array($dateArr[$l],$taIwayArr[$c_Code]))
											{
												if($l==0)
												{
													if($ta_iway!='')
													{
														$ta_iway.=",('$ta_login','$c_Code','$dateArr[$l]','$timeArr[$l]')";
													}
													else
													{
														$ta_iway="('$ta_login','$c_Code','$dateArr[$l]','$timeArr[$l]')";
													}
													
												}
												else
												{
													if($ta_iway!='')
													{
														$ta_iway.=",('$ta_login','$c_Code','$dateArr[$l]','$timeArr[$l]')";
													}
													else
													{
														$ta_iway="('$ta_login','$c_Code','$dateArr[$l]','$timeArr[$l]')";
													}
												}
											}
										}
									}
								}
								
								/*echo "TA DETAILS ==> ".$ta_details."<br>";
								echo "TA PASSWORD ==> ".$ta_password."<br>";
								echo "TA IWAY ==> ".$ta_iway."<br>";  exit;*/

								if($ta_iway!='')
								{
									if($ta_details!='' && $ta_password!='')
									{

#echo $ta_details ; exit ;
										// insert into iib_ta_details
										$sql_insert_ta_details="INSERT INTO iib_ta_details VALUES $ta_details";
	
										//echo "Insert TA details".$sql_insert_ta_details."<br>";
										$res_insert_ta_details=mysql_query($sql_insert_ta_details) or die('e1'.mysql_error());
																			
										// insert into iib_ta_password
										$sql_insert_ta_password="INSERT INTO iib_ta_password VALUES $ta_password";

										//echo "Insert TA Password".$sql_insert_ta_password."<br>";
										$res_insert_ta_password=mysql_query($sql_insert_ta_password) or die('e2'.mysql_error());
										
										// insert into ta password decrypt table
										
										$sql_insert_ta_pass_decrypt="INSERT INTO iib_ta_password_decrypt VALUES $ta_password";

										$res_insert_ta_pass_decrypt=mysql_query($sql_insert_ta_pass_decrypt) or die('e3'.mysql_error());
									}
									
											
									//insert into iib_ta_iway
									$sql_insert_ta_iway="INSERT INTO iib_ta_iway VALUES $ta_iway";

									//echo "Insert TA Iway".$sql_insert_ta_iway."<br>";
									$res_insert_ta_iway=mysql_query($sql_insert_ta_iway) or die('e4'.mysql_error()); 
								}
								
/*								echo "TA DETAILS ==> ".$sql_insert_ta_details."<br>";
								echo "TA PASSWORD ==> ".$sql_insert_ta_password."<br>";
								echo "TA decrypt ==> ".$sql_insert_ta_pass_decrypt."<br>";
								echo "TA IWAY ==> ".$sql_insert_ta_iway."<br>";

								echo "TA DETAILS ==> ".$res_insert_ta_details."<br>";
								echo "TA PASSWORD ==> ".$res_insert_ta_password."<br>";
								echo "TA decrypt ==> ".$res_insert_ta_pass_decrypt."<br>";

								echo "TA DETAILS ==> ".$res_insert_ta_iway."<br>";
								exit;
*/								
							
#echo $sql_insert_ta_iway."<BR><BR>".$res_insert_ta_iway ; exit ;
								//if($no_flag!='true' || $dup_flag!='true' || $result_vali=='true')
								if($res_insert_ta_iway) 
								{
									
									//form the header,if uploading operations are done successfully
									//header("Location:tafile_upload.php?upd=true&msg=1");

#echo $ta_details."<BR><BR>".$ta_password ; exit ;

									if($ta_details =='' && $ta_password =='')
									{
#echo '1' ;
										header("Location:tafile_upload.php?upd=true&msg=1");
									}
									else
									{
#echo '2' ;
										header("Location:tapass_generate.php");
									}
								}
							/*}
							// if actual seats are not equal then else
							else
							{
								//form the header,if actual seats are not equal
								header("Location:tafile_upload.php?upd=false&msg=9&actls=$actual_code");
							}*/
							}
							else
							{
								//form the header,if there are more than 4 fields in the file
								header("Location:tafile_upload.php?upd=false&msg=8");
							}
							
						}
							else
							{
								//form the header,if there are duplicate records in the file
								header("Location:tafile_upload.php?upd=false&msg=4&dup=$dup_code");
							}
					}
				else if ($dup_flag=='true' && $iway_flag='false')
				{
					//form the header,if there are duplicate records in the DB
					header("Location:tafile_upload.php?upd=false&msg=5&duptbl=$dup_Ccode");
				}	
				/*}
				else if ($no_flag=='true')
				{
					//form the header,if there are no records for the centre code in the DB
					header("Location:tafile_upload.php?upd=false&msg=6&norec=$no_Ccode");
				}
			}
			else
			{
				//form the header,if the data in the file is less than the records in iway table
				header("Location:tafile_upload.php?upd=true&msg=10&fn=$uploadfile");
			}*/
			}
			else
			{
				//form the header,if there are validation problems
				//header("Location:tafile_upload.php?upd=false&msg=7&vali_code=$result_CCode");
				header("Location:tafile_upload.php?upd=false&msg=7&vali_code=$lineNo");
				
			}
		}
		else
		{
			header("Location:tafile_upload.php?upd=false&msg=11&vali_code=$result_CCode");
		}
		
		//}
	
		
		}
		else
		{
			//form the header,if there are no contents in the file
			header("Location:tafile_upload.php?upd=false&msg=2");
		}
	}
	else
	{
		//form the header,if the uploaded file is other than text(.txt) file
		header("Location:tafile_upload.php?upd=false&msg=3");
	}
?>
