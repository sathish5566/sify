<?php
$admin_flag=1;
require("includes/dbconfig.php");
require("includes/constants.inc");
require("includes/settings.php");
?>
<html>
<head>
    <title><?=PAGE_TITLE ?></title>
	<link rel="stylesheet" href="includes/exl.css" type="text/css">
	<script language='JavaScript' src='./includes/validations.js'></script>
	<script language='JavaScript'>
		function validateForm()
		{					
			document.frmselectquestions.total_question_paper.value = trim(document.frmselectquestions.total_question_paper.value);
			var total_question_paper = document.frmselectquestions.total_question_paper.value;		
						
			if (!checknull(document.frmselectquestions.total_question_paper))
			{
				alert('No. of question papers cannot be empty');
				document.frmselectquestions.total_question_paper.focus();
				return;				
			}
			
			if (total_question_paper <= 0)
			{
				alert('No.of question papers must be greater than 0');
				document.frmselectquestions.total_question_paper.focus();
				return;
			}
			document.frmselectquestions.submit();
		}
	</script>
</head>
<?php
		
		$examCode = $_REQUEST['exam_code'];
		$subjectCodeType = $_REQUEST['subject_code'];
		$aSubject = explode("-",$subjectCodeType);
		$subjectCode = $aSubject[0];
		$sql = "SELECT exam_name FROM oe_exam WHERE exam_code=$examCode";
		if ($commonDebug)
		{
			print "query :".$sql;
		}
		$result = @mysql_query($sql) or die("Query failed");
		list($examName) = mysql_fetch_array($result);
	
		$sql = "SELECT subject_name, total_marks, pass_mark FROM oe_exam_subjects WHERE subject_code=$subjectCode";
		if ($commonDebug)
		{
			print "query :".$sql;
		}
		$result = @mysql_query($sql) or die("Query failed");
		list($subjectName, $totalMarks, $passMark) = mysql_fetch_array($result);
		
		// get existing QP count
		$sqlCount = "SELECT COUNT(question_paper_no) FROM oe_question_paper WHERE exam_code=$examCode ".
			" AND subject_code=$subjectCode AND online='Y' ";
		if ($commonDebug)
		{
			print "query :".$sqlCount;
		}
		$resCount = @mysql_query($sqlCount) or die("Query on oe_question_paper failed");
		list($existingCount) = mysql_fetch_row($resCount);
		if ($existingCount == "")
		{
			$existingCount = 0;
		}
		// get total questions			
		$sqlSum = "SELECT SUM(no_of_questions) FROM oe_qp_weightage_desc WHERE exam_code=$examCode ".
			" AND subject_code=$subjectCode group by question_paper_no LIMIT 1";
		if ($commonDebug)
		{
			print "query :".$sqlSum;
		}
		$resSum = @mysql_query($sqlSum) or die("Query on oe_qp_weightage failed");
		if (mysql_num_rows($resSum) > 0)
		{
			list($existingSum) = mysql_fetch_row($resSum);
		}
		else
		{
			$existingSum = "";
		}
		$sql = "SELECT section_code, section_name FROM oe_subject_sections_desc WHERE ".
			"subject_code=$subjectCode and online='Y' ORDER BY section_code ";
		if ($commonDebug)
		{
			print "query :".$sql;
		}
		$result = @mysql_query($sql) or die("Query failed");
		$nRows = mysql_num_rows($result);
		if ($nRows == 0)
		{
?>
<body leftmargin="0" topmargin="0" >
<center>
<table width="780" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="780"><? include("header.php"); ?></td>
  </tr>
  <tr> 
    <td width="780" >&nbsp;</td>
  </tr>
  <tr>
		<Td><? include("admin_menu.php") ?></Td>	
  </tr>
  <tr> 
    <td width="780"  valign="top"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td height="34"> 
            <div align="center"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Admin 
              module - Question Paper(s) Generation</font> </strong></div></td>
        </tr>
        <tr> 
          <td> 
            <div align="center"> <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
			<?php
					print("<br><br><center><div class='alertmsg'>The subject ($examName, $subjectName) has no sections".
							"<br></div></center><br>");
			?>
			</div>
		  </td>
	  </tr>
	</td>
  </tr>
  <tr> 
    <td width="780">&nbsp;</td>
  </tr>
</table>
</center>
</body>
</html>
<?					
		}
		else
		{
?>					
<body leftmargin="0" topmargin="0" onLoad="document.forms[0].total_question_paper.focus()" onKeyPress="return number(event)">
<center>
<table width="780" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="780"><? include("header.php"); ?></td>
  </tr>
  <tr> 
    <td width="780">&nbsp;</td>
  </tr>
  <tr>
		<Td class="DefaultFontClass" ><? include("admin_menu.php") ?></Td>	
  </tr>
  <tr> 
    <td width="780" valign="top"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td height="34"> 
            <div align="center"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Admin 
              module - Question Paper(s) Generation</font> </strong></div></td>
        </tr>
        <tr> 
          <td> 
            <div align="center"> <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
              <?php
					print("<form name='frmselectquestions' method=post action=generate_papers_typing.php>\n".
					"<input type=hidden name=exam_code value='$examCode'>\n".
					"<input type=hidden name=subject_code value='$subjectCode'>\n".
					"<input type=hidden name=total_marks value='$totalMarks'>\n");
					print("<table width=100% >\n");
					print("<tr><td class='DefaultFontClass'><b>Exam : $examName</b></td>".
					"<td colspan=2 class='DefaultFontClass'><b>Subject : $subjectName</b></td></tr>");
					print("<tr><td class='DefaultFontClass'><b>Total Marks : $totalMarks</b></td>".
					"<input type=hidden name=total_marks value='$totalMarks'>".
					"<td class='DefaultFontClass'><b>Pass Mark : $passMark</b></td>".
					"<input type=hidden name=pass_mark value='$passMark'>".		
					"<td class='DefaultFontClass'><b>Question Papers already Generated : $existingCount </b></td></tr>");
					print("<tr><td class='DefaultFontClass'>&nbsp;</td></tr></table>");
					print("<table width=100%><tr><td>&nbsp;</td></tr>".
					"<tr><td class='DefaultFontClass' ><b>No. of question papers : </b>".
					"<input type=text class=textboxclass name=total_question_paper value=1 maxlength=5 onKeyPress=\"return number(event)\" tabindex=1></td>".
					"<td>".
					"<input type=button class=buttonclass name='sub' value='Generate Question Paper(s)' onClick='validateForm()'  tabindex=2 ".
					"onBlur=\"document.forms[0].total_question_paper.focus();\"></td></tr>");
					print("</table>");
					print("</form>");
				} // end of else
			
				/* Free resultset */
				mysql_free_result($result);
			?>
              </font> </div></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
          <td width="780" ><? include("footer.php"); ?></td>
  </tr>
</table>
</center>
</body>
</html>