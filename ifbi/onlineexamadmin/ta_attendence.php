<?php
/****************************************************
* Application Name				            :  IBPS
* Module Name								:  Ta Attendence
* Revision Number							:  1
* Revision Date								:
* Tables used for only updates				: -
* Tables used for only selects				: iib_candidate_iway,iib_iway_details,iib_ta_tracking,iib_exam_slots,iib_exam_centres
* View(s)									:  -
* Stored Procedure(s)						:  -
* Dependant Module(s)						:  -
* Output File(s)							:   
* Document/Reference Material				:
* Created By								:  B. Devi
* Created ON								:  24-05-2006
* Last Modified By							:  
* Last Modified Date						:  
* Description								:  This UI helps to get the TA attendence details .
*****************************************************/

$admin_flag=2;
require_once "sessionchk.php";
require_once("dbconfig_slave_123.php");//since only selects dbconfig_slave_123.php is used

$commonDebug=false;//To debug the code

$const_iwfr="IWFR";
$var_upper="upper";


//Query to get the slots
$sql_time = "select trim(slot_time) from iib_exam_slots order by slot_time";

if($commonDebug)
{
	   echo $sql_time;
}

$res_time = @mysql_query($sql_time);

if (mysql_error()){
	echo mysql_error()."Problem in Selecting Slot Details";
	exit;	
}

$aslot_time=array();
while(list($slot_time) = mysql_fetch_row($res_time))
{
	$aslot_time[]=$slot_time;
}//end of while


$slot_count=count($aslot_time);//Count of number of slots

$currentdate = date("Y-m-d");//Today's Date

$exam_time=$_POST['slots'];//variable to store the selected slot


/*Login for the Slot Checks :

  -- If there is only one slot just check exam_time > selected slot
  -- If there is more than one slot 
   		a.if the selected slot is the last slot - then check exam_time > selected slot(which is last slot)
   		b.a.if the selected slot is not the last slot - then check exam_time > selected slot and exam_time < next slot to the selected slot
*/

if($slot_count==1){
	$chk_exam_time = "exam_time >="."'$exam_time'";
}//if $slot_count ==1
elseif($slot_count>1){		
		$key_value=array_search($exam_time,$aslot_time);		
		if($key_value==$slot_count-1)
		{			
			$chk_exam_time = "exam_time >="."'$exam_time'";
		}
		else{
			$next_exam_time=$aslot_time[$key_value+1];	
			$chk_exam_time = "exam_time >="."'$exam_time'"." and "."exam_time <"."'$next_exam_time'" ;		
		}
}//elseif ($slot_count>1)

/*This function is called only when in "Select City" drop down other than "ALL" is selected
Function used for getting the centre_code based on exam_centre_code selected 
*/
 function get_centrecode()
 {	 	 
	 global $city,$var_upper; 	
	 
	 $sql_centre_code_details="select $var_upper(trim(centre_code)) from iib_iway_details where exam_centre_code ='".$city."' ";	 								 
					
	 $res_centre_code_details=mysql_query($sql_centre_code_details);
	 if (mysql_error()){
	    echo mysql_error();
	    exit;
	  }	
	 $alistcentre_code=""; 
     while(list($centre_code) = mysql_fetch_row($res_centre_code_details))
	 {	 	
	 	$alistcentre_code .= "'".$centre_code."',";
	 }
	 
	 $alistcentre_code = substr($alistcentre_code , 0 ,-1);			 
	 	 
	 return $alistcentre_code;
	 
 }
 
/*
Here first we selected the centrecodes who are scheduled for today's date and selected time.
If in "Select City" drop down other than "ALL" is selected, then have to call get_centrecode(), to get the centrecodes for the selected city
else function need not be called.
*/
 function todays_centre_code()
 {	 
	 global $currentdate,$chk_exam_time,$city,$var_upper;	 
	 if($city=='all'){
		 $sql_centre_code="select  distinct($var_upper(trim(centre_code))) ".
		 							" from iib_candidate_iway ".
		 							" where exam_date='".$currentdate."' ".
		 							" and $chk_exam_time ";	 							
	}
	else{
		$centre_code_list=get_centrecode();		
		$sql_centre_code="select   distinct($var_upper(trim(centre_code))) ".
		 							" from iib_candidate_iway ".
		 							" where exam_date='".$currentdate."' ".
		 							" and $chk_exam_time and centre_code in ($centre_code_list) ";	 		 							
	}		 								
	$res_centre_code=mysql_query($sql_centre_code);
	 if (mysql_error()){
		 echo "$sql_centre_code";
	    echo mysql_error();
	    exit;
	  }
	 $acentre_code=array();
     while(list($centre_code) = mysql_fetch_row($res_centre_code))
	 {
	 	$acentre_code[]=$centre_code;
	 }	
	 
	 return $acentre_code;
	 
 }
 
 //Function used for getting the centre_code,exam_time for today's date in iib_ta_iway
 function ta_iway_time()
 {
	 global $currentdate,$var_upper;	
	 $sql_ta_iway="select $var_upper(trim(centre_code)),trim(exam_time) from iib_ta_iway where exam_date= CURDATE()";
	 $res_ta_iway=mysql_query($sql_ta_iway);
	 if (mysql_error()){
	    echo mysql_error();
	    exit;
	  }
	 $ata_iway=array();
     while(list($centre_code,$exam_time) = mysql_fetch_row($res_ta_iway))
	 {
	 	$ata_iway[$centre_code]=$exam_time;
	 }	
	 
	 return $ata_iway;
	 
 }
 
 //Function used for checking whether ta has logged in right side
 function ta_check()
 {
	 global $var_upper;
	 $currDate = date("Y-m-d");//Today's Date	 
	 $sql_ta_check="select distinct $var_upper(trim(ta_login)) from iib_ta_tracking where updated_time like '$currDate%'";	 	 	 	 	 
	 $res_ta_check=mysql_query($sql_ta_check);
	 if (mysql_error()){
	    echo mysql_error();
	    exit;
	  }
	  
	 $ata_tracked=array();
     while(list($ta_logged) = mysql_fetch_row($res_ta_check))
	 {
	 	$ata_tracked[]=$ta_logged;
	 }	
	 	 
	 return $ata_tracked;	
 }
 
 
 
extract($_POST);
$val1 = "";
$val2 = "";
if ($show[0]==1) $val1='Checked';
if ($show[0]==2) $val2='Checked';
if ($val1==$val2)
    $val2="Checked";

if(isset($city) && !empty($city) && !empty($show[0]))
{
        
    $todays_centre_code=todays_centre_code();//function called to get the centre codes scheduled for today's date and selected time      
    if(count($todays_centre_code)>0)
    {		    
    	$ta_iway_time=ta_iway_time();	
	}	
	
	if((is_array($ta_iway_time)) && (count($ta_iway_time)>0)){
	    if($show[0]=='2')//To show absentees : If Yet to Login in Ta's Checkbox is selected
	    {          
		     /*This loop is to fetch the centre code who have not been logged till now, by selecting in iib_ta_iway,
		       the exam_time is null . And those centrecode have to seperated in an array*/ 
		     foreach($todays_centre_code as $centre_code_value)
		     {	     
			     if (array_key_exists($centre_code_value, $ta_iway_time))
			     {	
				  	if(($ta_iway_time[$centre_code_value]) == '00:00:00')
				  	{				  	
				  		$centre_code_details[]=$centre_code_value;
			 		}
			 		
			     }//end of if 
		     } //foreach($todays_centre_code)
		    
	    }//if$show==2
	    elseif($show[0]=='1'){//If  Logged in Ta's Checkbox is selected
		 
	    /*This loop is to fetch the centre code who have  been logged in now, by selecting in iib_ta_iway,
		       the exam_time is not null . And those centrecode have to seperated in an array*/ 
		 foreach($todays_centre_code as $centre_code_value)
	     {	    	     
		     if (array_key_exists($centre_code_value, $ta_iway_time))
		     {		    
			  	if(($ta_iway_time[$centre_code_value])!= '00:00:00')		  	
			  		$centre_code_details[]=$centre_code_value;
		     }//end of if	     
	     }//foreach($todays_centre_code)
	     	  
	    }//if$show==1
    }//if(is_array)
    
}//if(isset)


//Fetching Exam Centre Details
$sql = "select trim(exam_centre_code),trim(exam_centre_name) from iib_exam_centres where exam_centre_code not in ('9999') order by exam_centre_name";
if($commonDebug)
{
	   echo $sql;
}
$res = mysql_query($sql);
if (mysql_error()){
	echo mysql_error();
	exit;	
}

?>
<html>
<head>
<title><?=PAGE_TITLE?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
	FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='JavaScript' src='./includes/validations.js'></script>
<script>
function validate()
{
    var f=document.taregister;
    if (f.city.selectedIndex == 0)
    {
    	alert('Please select the city');
    	f.city.focus();
    	return false;
    }
    
    if (f.slots.selectedIndex == 0)
    {
    	alert('Please select the slot');
    	f.slots.focus();
    	return false;
    }
    f.submit();
}
function newwin(a,b)
{
    window.open('tadetails.php?id='+a+'&cid='+b,'win1','left=10,top=10,width=300,height=250,toolbar=no');
}

</script>
</head>
<body align=center>
<form name='taregister' method='post'>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780 background=images/tile.jpg>
<!-- Topnav Image -->	
<!--
	<TR>
    	<TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR>	
	<TR>
    	<TD bgColor=#7292c5 width=780>&nbsp;</TD>
	</TR>-->
	<tr><td width="780"><?include("includes/header.php");?></td></tr>
	<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>
	<TR>
	    <TD vAlign=top width=780 align=center>
	</tr>
	<tr>
	   <td align="center"  background=images/tile.jpg><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2>MIS- Test Admin Attendance</font><br><br></td>
    </tr>
	<tr>
		<td>
	    <table align=center>
        <tr>
            <td class="greybluetext10">Select City : </td>
            <td><select name='city' class="textbox">
                <option value=''>--Select--</option>
				<option value='all'>All</option>
                <?
                    while(list($citycode,$cityname)=mysql_fetch_row($res))
                    {
                        if($_POST['city']==trim($citycode))
                             echo "<option value='$citycode' selected>$cityname</option>";
                        else
                             echo "<option value='$citycode'>$cityname</option>";                        
                    }//while
                ?>
                </select>
            </td>
        </tr>
        <tr>
        	<td class="greybluetext10">Select Slots : </td>
        	<td>
        		<select name='slots' class="textbox">
        		<option value=''>--Select--</option>
        		<?
        		foreach($aslot_time as  $slot_time)
          		{
	              	if (trim($exam_time) == trim($slot_time))
	                echo "<option value='$slot_time' selected>$slot_time</option>";
	                else
	                echo "<option value='$slot_time'>$slot_time</option>";
          		}//foreach($aslot_time)
        			
        		?>
        		</select>
        	</td>
        </tr>
        <?
        
        ?>
        <tr>
            <td class="greybluetext10">Logged In Ta's</td>
            <td><input type='radio' name='show' value='1' <?=$val1?>></td>
        </tr>
        <tr>
            <td class="greybluetext10">Yet To Login In Ta's</td>
            <td><input type='radio' name='show' value='2' <?=$val2?>></td>
        </tr>
        <tr>
            <td colspan=2 align=center><input type='button' name='ok' value='Display' onclick='javascript:validate();' class="button"></td>
        </tr>
</table>
<?
    if(isset($city) && !empty($city))
    {
	    
		    
    if(!is_array($centre_code_details)) 
    $num="0";
    else
    $num=count($centre_code_details);    
    
        echo "<div align=center class=alertmsg><br>No of Records : $num<br><br></div>";
        if($num > 0)
        {
		natcasesort($centre_code_details);
?>
        <table align=center border=1 width=100%>
        <tr bgcolor="#E8EFF7">
            <td class="greybluetext10"><b>Alert Franchisee</b></td>
            <td class="greybluetext10"><b>Login Id</b></td>         	
            <td align=center class="greybluetext10"><b>Centre Code</b></td>
            <?php
            if ($val1 == "Checked")
            	print("<td align=center class=\"greybluetext10\"><b>Time of Login</b></td>");           
            if ($val2 == "Checked")
            	print("<td align=center class=\"greybluetext10\"><b>Right side Logged In </b></td>");
            ?>
        </tr>
<?
        if ($val2 == "Checked")        
        {	 
	       
	        $ta_logged_once=ta_check();//function called to check whether ta has logged in or not	            		       		       
	        foreach($centre_code_details as $disp_ta_details)
	        {	
		        if(is_array($ta_logged_once)){	       		     
			        if(in_array($const_iwfr."_".$disp_ta_details,$ta_logged_once))
			        $logged_once='Yes';
			        else
			        $logged_once='No';
		        }
		        else{
			        $logged_once='--';
		        }
			       
		        ?>	
		        <tr>
	                <td><a href="javascript:newwin('<?=$const_iwfr."_".$disp_ta_details?>','<?=$disp_ta_details?>')">Franchisee</a></td>
	                <td class="greybluetext10"><?=$const_iwfr."_".$disp_ta_details?></a></td>	               
	                <td class="greybluetext10"><?=$disp_ta_details?></td>	                
	                <td align=center class="greybluetext10"><?=$logged_once?></td>
	            </tr>
	        <?}	   	       
			
        }//if($val2)
        else
        {	        
	        foreach($centre_code_details as $disp_ta_details)
	        {?>		        
		        <tr>
	                <td><a href="javascript:newwin('<?=$const_iwfr."_".$disp_ta_details?>','<?=$disp_ta_details?>')">Franchisee</a></td>
	                <td class="greybluetext10"><?=$const_iwfr."_".$disp_ta_details?></a></td>	               
	                <td class="greybluetext10"><?=$disp_ta_details?></td>	                
	                <td align=center class="greybluetext10"><?=$ta_iway_time[$disp_ta_details];?></td>
	            </tr>
	        <?}	        
        }//else
        echo "</table>";  
     }//$num 
    }//isset
?>

</form>
		</td>
	</tr>
	<!--<TR>
		<TD bgColor=#7292c5 width=780>&nbsp;</TD>
	</TR>-->
	
		<?php include("includes/footer.php")?>
</TABLE>
</body>
</html>
