<?php
/****************************************************
* Application Name            : IIB 
* Module Name                 : Subjectwise Result report
* Revision Number             : 1
* Revision Date               :
* Table(s)                    : 
* Tables used for only selects: iib_exam,iib_exam_subjects,iib_exam_candidate,iib_exam_subjects,iib_candidate_test,iib_candidate_scores,
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              :
* Document/Reference Material :
* Created By                  :  
* Created ON                  :  
* Last Modified By            :  K.KARUNA MOORTHY RAJU
* Last Modified Date          :  23-03-2006
* Description                 :  Report which displaying the subjectwise report with the related information and it can be downloaded as a csv file.
*****************************************************/

    ob_start();
	require_once "sessionchk.php";
	require_once("dbconfig_slave_123.php");
    require_once "constants.inc";
	
/*** Include file for excel download ***/
		
	include_once("excelconfig.php");
	$sqlexam = "SELECT exam_code,exam_name FROM iib_exam where online='Y' ";
	$resexam = mysql_query($sqlexam);
	$subject = $_POST['subject'];
	$subjectCode = $_POST['subject_code'];
	$medium = $_POST['medium'];
	$examCode = $_POST['exam_code'];
    $submitted= $_POST['submitted'];
	$exam_count = mysql_num_rows($resexam);
	$color_count = count($aColor);

	if($exam_count >  $color_count)
	{
		echo "Please ask the developers to include more colours";
		exit;
	}
	$colour = 0;
	while(list($exam_code,$exam_name) = mysql_fetch_row($resexam))
	{
		 
		$exam_array[$exam_code] = $exam_name;
		$bgcolor_array[$exam_code] = $aColor[$colour];
		$colour++;
	}
    
/*** Variables - $ex_flag, $filename, $exce_header,$exce_content.
     $ex_flag is for getting the values when clicking download button and $filename is used for 
downloading the file with its value
  ***/
        
    $ex_flag=$_POST['exl'];
    $filename="subjectresult_report_Exam_(".$examCode.")_sub(".$subjectCode.")_medium_(".$medium.")";
    $exce_header=array("S No.","Subject Name","Subject Code","No of candidates allocated","No. of Candidates appeared","No of candidates Passed","No of candidates Failed","No of candidates absent","No of Incomplete candidates");
    $header_count = count($exce_header);
    
    	if ((($subject == 'true') || ($submitted=='true')) && ($examCode!='all'))
    	{
	    	$sql_subject_sel="select exam_code,subject_code,subject_name from iib_exam_subjects where exam_code='".$examCode."' order by subject_name";
	    }
    	else if((($subject == 'true') || ($submitted=='true'))  && ($examCode=='all'))
    	{
	    	$sql_subject_sel="select exam_code,subject_code,subject_name from iib_exam_subjects order by subject_code ";
    	}
	$res_subject_sel=mysql_query($sql_subject_sel);

/*** Select the centre code,actualseats,exam date and other details
 depends in the examCode,subjectcode and medium                            ***/	

  if (($examCode != "") && ($subjectCode !="")  && ($medium!='') && ($submitted=='true')) {
		if($medium == 'EN')  $medium_query = "E' or medium_code='EN";
		else if($medium == 'HI') $medium_query = "HI";
       		switch($examCode)
        	{
            	case "all":
	    		if ($subjectCode=='all') {
                    	$sqlFormat="select count(1),subject_code, exam_code from iib_exam_candidate where (medium_code='".$medium_query."') and alloted='Y' group by subject_code ";
                	}
                	else {
                    	$sqlFormat="select count(1),subject_code, exam_code from iib_exam_candidate where (medium_code='".$medium_query."')  and subject_code='".$subjectCode."' and alloted='Y' group by subject_code ";
                	}
                	break;
            		default :
             		if ($subjectCode=='all') {
                        $sqlFormat="select count(1),subject_code, exam_code from iib_exam_candidate where (medium_code='".$medium_query."')  and exam_code='".$examCode."' and alloted='Y' group by subject_code";
               		 }
                	else {
                    	$sqlFormat="select count(1),subject_code, exam_code from iib_exam_candidate where (medium_code='".$medium_query."')  and exam_code='".$examCode."' and subject_code='".$subjectCode."' and alloted='Y' group by subject_code";
                	}
                	break;
        	}
	//echo $sqlFormat; exit;
        $resFormat = mysql_query($sqlFormat);
        $ncount=mysql_num_rows($resFormat);
        $sql_subject_code="select subject_code,subject_name from iib_exam_subjects";
        $res_subject_code=mysql_query($sql_subject_code);
        
        	while(list($subject_Code,$subjectName)=mysql_fetch_row($res_subject_code))
        	{
	      	 	$subjectArr[$subject_Code]=$subjectName;
	    	}

	    		    	
	    //$sql_appeared="select count(1),b.exam_code,b.subject_code from iib_candidate_test a,iib_exam_candidate b where a.membership_no=b.membership_no and b.medium_code='".$medium."' and a.exam_code=b.exam_code group by b.exam_code,b.subject_code,b.medium_code";
	        if ($examCode=='all')
	    	{
	 	   		if ($subjectCode=='all')
	    		{
	    			$sql_appeared="select count(distinct a.membership_no,a.subject_code),b.exam_code,b.subject_code from iib_candidate_test a,iib_exam_candidate b where a.membership_no=b.membership_no and (b.medium_code='".$medium_query."') and a.exam_code=b.exam_code and a.subject_code=b.subject_code and alloted='Y'  group by b.exam_code,b.subject_code";
	    			
	    			$sql_result_pass="select count(distinct a.membership_no,a.subject_code),b.exam_code,b.subject_code from iib_candidate_scores a,iib_exam_candidate b where a.membership_no=b.membership_no and a.exam_code=b.exam_code and a.subject_code=b.subject_code and (b.medium_code='".$medium_query."') and a.result='P' and alloted='Y' group by b.exam_code,b.subject_code ";
	    			
	    			$sql_result_fail="select count(distinct a.membership_no,a.subject_code),b.exam_code,b.subject_code from iib_candidate_scores a,iib_exam_candidate b where a.membership_no=b.membership_no and a.exam_code=b.exam_code and a.subject_code=b.subject_code and (b.medium_code='".$medium_query."') and a.result='F' and alloted='Y' group by b.exam_code,b.subject_code ";
		            $res_result_fail=mysql_query($sql_result_fail);
		            
		    			while(list($failed,$exam_code,$subject_Code)=mysql_fetch_row($res_result_fail))
		    			{
							$resultArr_fail[$exam_code][$subject_Code]=$failed;
		    			}
	        	        			
		     	    $res_appeared=mysql_query($sql_appeared);
		    			while(list($appeared,$exam_code,$SubjectCode)=mysql_fetch_row($res_appeared))
		   			 	{
						$appearedArr[$exam_code][$SubjectCode]=$appeared;
		    		    }
		    		$res_result_pass=mysql_query($sql_result_pass);   
		    		while(list($passed,$exam_code,$subject_Code)=mysql_fetch_row($res_result_pass))
	    					{
								$resultArr_pass[$exam_code][$subject_Code]=$passed;
	    					}    
	    		}
	    		else
	    		{
		    		//$sql_appeared="select count(distinct a.membership_no,a.subject_code),b.exam_code,b.subject_code from iib_candidate_test a,iib_exam_candidate b where a.membership_no=b.membership_no and b.medium_code='".$medium."' and a.exam_code=b.exam_code and a.subject_code=b.subject_code group by b.exam_code,b.subject_code";
		    		//$sql_appeared="select count(distinct a.membership_no,a.subject_code),a.exam_code,a.subject_code from iib_candidate_test a,iib_exam_candidate b  where a.membership_no=b.membership_no and a.subject_code=b.subject_code and  a.subject_code ='".$subjectCode."' and b.medium_code='e' group by a.subject_code";
		    		$sql_appeared="select count(distinct a.membership_no,a.subject_code) from iib_candidate_test a,iib_exam_candidate b  where a.membership_no=b.membership_no and a.exam_code = b.exam_code and a.subject_code=b.subject_code and  a.subject_code ='".$subjectCode."' and (b.medium_code='".$medium_query."') and alloted='Y'";
		    		
		    		$sql_result_pass="select count(distinct a.membership_no,a.subject_code) from iib_candidate_scores a,iib_exam_candidate b where a.membership_no=b.membership_no and a.subject_code=b.subject_code and (b.medium_code='".$medium_query."') and a.subject_code ='".$subjectCode."' and a.result='P' and alloted='Y'";
		    		
		    		$sql_result_fail="select count(distinct a.membership_no,a.subject_code) from iib_candidate_scores a,iib_exam_candidate b where a.membership_no=b.membership_no and a.subject_code=b.subject_code and (b.medium_code='".$medium_query."') and a.subject_code ='".$subjectCode."' and a.result='F' and alloted='Y' ";
				//echo $sql_result_fail;
		    	    $res_result_fail=mysql_query($sql_result_fail);
		    		list($failed)=mysql_fetch_row($res_result_fail);
					$failcan=$failed;
					
	    		    $res_result_pass=mysql_query($sql_result_pass);
	    			list($passed,$exam_code,$subject_Code)=mysql_fetch_row($res_result_pass);
					$passcan=$passed;
		    		
		    		$res_appeared=mysql_query($sql_appeared);
		    		list($appeared,$exam_code,$SubjectCode)=mysql_fetch_row($res_appeared);
					$appearcan=$appeared;

		    	}
    		}
   		else
    		{
	    		if($subjectCode=='all')
	    		{
		    		$sql_appeared="select count(distinct a.membership_no,a.subject_code),b.exam_code,b.subject_code from iib_candidate_test a,iib_exam_candidate b where a.membership_no=b.membership_no and a.exam_code='".$examCode."' and (b.medium_code='".$medium_query."') and a.exam_code=b.exam_code and a.subject_code=b.subject_code and alloted='Y'  group by b.exam_code,b.subject_code";
		    		$sql_result_pass="select count(distinct a.membership_no,a.subject_code),b.exam_code,b.subject_code from iib_candidate_scores a,iib_exam_candidate b where a.membership_no=b.membership_no and a.exam_code=b.exam_code and a.subject_code=b.subject_code and a.exam_code='".$examCode."' and (b.medium_code='".$medium_query."') and a.result='P' and alloted='Y' group by b.exam_code,b.subject_code ";
		    		
		    		$sql_result_fail="select count(distinct a.membership_no,a.subject_code),b.exam_code,b.subject_code from iib_candidate_scores a,iib_exam_candidate b where a.membership_no=b.membership_no and a.exam_code=b.exam_code and a.subject_code=b.subject_code and a.exam_code='".$examCode."' and (b.medium_code='".$medium_query."') and a.result='F' and alloted='Y' group by b.exam_code,b.subject_code ";
		    		
		    	    $res_result_fail=mysql_query($sql_result_fail);
		    	    
		    			while(list($failed,$exam_code,$subject_Code)=mysql_fetch_row($res_result_fail))
		    			{
							$resultArr_fail[$exam_code][$subject_Code]=$failed;
		    			}
	    		    $res_result_pass=mysql_query($sql_result_pass);
	    		    
	    				while(list($passed,$exam_code,$subject_Code)=mysql_fetch_row($res_result_pass))
	    				{
							$resultArr_pass[$exam_code][$subject_Code]=$passed;
	    				}

		    		$res_appeared=mysql_query($sql_appeared);
		    			while(list($appeared,$exam_code,$SubjectCode)=mysql_fetch_row($res_appeared))
		    			{
							$appearedArr[$exam_code][$SubjectCode]=$appeared;
		    			}
	    		}
	    		else
	    		{
		    	$sql_appeared="select count(distinct a.membership_no,a.subject_code) from iib_candidate_test a,iib_exam_candidate b  where a.membership_no=b.membership_no and a.exam_code = b.exam_code and a.subject_code=b.subject_code and a.exam_code='".$examCode."' and  a.subject_code ='".$subjectCode."' and (b.medium_code='".$medium_query."') and alloted='Y'";
		    	$sql_result_pass="select count(distinct a.membership_no,a.subject_code) from iib_candidate_scores a,iib_exam_candidate b where a.membership_no=b.membership_no and a.subject_code=b.subject_code and (b.medium_code='".$medium_query."') and a.exam_code='".$examCode."' and a.subject_code ='".$subjectCode."' and a.result='P' and alloted='Y'";
		    	
		    	$sql_result_fail="select count(distinct a.membership_no,a.subject_code) from iib_candidate_scores a,iib_exam_candidate b where a.membership_no=b.membership_no and a.subject_code=b.subject_code and (b.medium_code='".$medium_query."') and a.exam_code='".$examCode."' and a.subject_code ='".$subjectCode."' and a.result='F' and alloted='Y' ";
		 //echo $sql_result_pass;
		    	    $res_result_fail=mysql_query($sql_result_fail);
		    		list($failed)=mysql_fetch_row($res_result_fail);
					$failcan=$failed;
					
	    		    $res_result_pass=mysql_query($sql_result_pass);
	    			list($passed,$exam_code,$subject_Code)=mysql_fetch_row($res_result_pass);
					$passcan=$passed;
		    	$res_appeared=mysql_query($sql_appeared);
		    	list($appeared,$exam_code,$SubjectCode)=mysql_fetch_row($res_appeared);
				$appearcan=$appeared;
		  		}
		    }

	       //$sql_result_pass="select count(*),b.exam_code,b.subject_code from iib_candidate_scores a,iib_exam_candidate b where a.membership_no=b.membership_no and a.exam_code=b.exam_code and b.medium_code='".$medium."'  and a.result='P'  group by b.exam_code,b.subject_code,b.medium_code";

/*** Store the subjectname,subjectcode,No of candidates allocated,No candidates appeared,No. of candidates passed,No. of candidates failed,No. of candidates absent and No. of Incomplete candidates into an array	 ***/	      

	   $i=0;
	        while(list($eligible,$subject_code,$exam_code)=mysql_fetch_row($resFormat))
	        {
	          
	        	$subject_name=$subjectArr[$subject_code];    
	        	$exce_content[$i][0] =$i+1;  
	            $exce_content[$i][1] = $subject_name;
                $exce_content[$i][2] = $subject_code;	                                 
                $exce_content[$i][3] = $eligible;
              
	          		if ($subjectCode=='all')
          	  		{
          	  			$appear=$appearedArr[$exam_code][$subject_code];
          	  	    }
      	  	  		else
      	  	  		{
	      	  	  		$appear=$appearcan;
      	  	  		}
      	  	  		if ($appear=='')$appear=0;
          	  		if ($subjectCode=='all')
          	  		{
          	  			$pass=$resultArr_pass[$exam_code][$subject_code];
      	  	  		}
      	  	  		else
      	  	  		{
	      	  			$pass=$passcan;
      	  	  		}
      	  	  		if($pass=='')$pass=0;
          	  		if ($subjectCode=='all')
          	  		{
          	  			$fail=$resultArr_fail[$exam_code][$subject_code];
      	  	  		}
      	  	  		else
      	  	  		{	
	      	  			$fail=$failcan;
	      	  		}
	      	  	      	  
	      	  		if($fail=='')$fail=0;
	      	  		
          	  $absent=$eligible-$appear;
          	  $ic=($appear-($pass+$fail));
          	  
          	  $exce_content[$i][4]=$appear;
	      	  $exce_content[$i][5] =$pass;
	      	  $exce_content[$i][6] = $fail;
	      	  $exce_content[$i][7] = $absent;
	      	  $exce_content[$i][8] = $ic;
	      	  $exce_content[$i][9] = $exam_code;
        $i++;  	  
        }
         


/* Execution of excel download function */

     if($ex_flag=='1') 
      {
        excelconfig($filename,$exce_header,$exce_content);
        exit;	 
       }
             
}
		


?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='JavaScript' src="./includes/validations.js"></script>
<script language='JavaScript'>
function getsubjects()
{
	if (document.frmalloc.exam_code.selectedIndex == 0)
	{
		alert("Please select the exam ");
		document.frmalloc.exam_code.focus();
		return false;
	}

	document.frmalloc.subject.value='true';
	document.frmalloc.exl.value='2';
	document.frmalloc.submit();

}
function submitForm()
{
	if (document.frmalloc.exam_code.selectedIndex == 0)
	{
		alert("Please select the exam ");
		document.frmalloc.exam_code.focus();
		return false;
	}
	if (document.frmalloc.subject_code.selectedIndex == 0)
	{
		alert("Please select the Subject ");
		document.frmalloc.subject_code.focus();
		return false;
	}
	if (document.frmalloc.medium.selectedIndex == 0)
	{
		alert("Please select the Medium ");
		document.frmalloc.medium.focus();
		return false;
	}

    document.frmalloc.submitted.value='true';
    document.frmalloc.exl.value='0';
	document.frmalloc.submit();
}
function excelsubmit()
{
	var frmname=document.frmalloc;
	frmname.submitted.value='true';
	frmname.exl.value='1';
	frmname.submit();
}
</script>
</HEAD>
<BODY leftMargin=0 topMargin=0 LeftMargin=0 marginheight=0 marginwidth=0 >
<center>
<form name='frmalloc' method='post'>
<input type='hidden' name='subject' value=''>
<input type='hidden' name='submitted'>
<input type=hidden name=exl value='0'>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
<!--<TABLE border=0 cellPadding=0 cellSpacing=0 width=780 height=500>-->
<!--	<TR><!-- Topnav Image -->
      <!--  <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
	</TR> -->
	<tr><td width="780"><?include("includes/header.php");?></td></tr>
    <TR>

    </TR>
	<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>
	</tr>
    <TR>
    	<TD background=images/tile.jpg vAlign=top width=780 align=center>

        	<table width=780 cellpadding=0 cellspacing=5 border=0 >
        		<tr>
					<td colspan=2 align="center"><b><font face="Verdana, Arial, Helvetica, sans-serif" size=2>Subject Wise Result</b></font><br><br></td>
            	</tr>
            <!--<tr>
            	<td colspan=2 align="center" class="greybluetext10"><b><?=$msg ?></b></td>
            </tr>-->
			   <tr>
    	        	<td align="right" width=390 class="greybluetext10" valign=top>Exam : </td>
					<td align="left" valign=top class="greybluetext10">
            			<select name='exam_code' class="greybluetext10" style={width:275px} onchange='javascript:getsubjects();'>
            				<option value=''>--Select--</option>
            				<?
            					if ($examCode == "all")
	            				{
		            				$selectedAll = "selected";
	            				}
            				?>
            				<option value='all' <?=$selectedAll?>>--All--</option>
            				<?
//							    $sqlexam = "SELECT exam_code,exam_name FROM iib_exam where online='Y' ";
                            	$resexamm = mysql_query($sqlexam);
                                while (list($code,$name) = mysql_fetch_row($resexamm))
            					{
                                   	print("<option value='$code' ");
	            						if ($examCode == $code)
	            						{
		            						print(" selected ");
	            						}
	            					print(">$name</option>");
                                }
	            		?>
            			</select>
            		</td>
            	</tr>
            	<tr>
					<td align="right" width=390 class="greybluetext10" valign=top>Exam Subjects: </td>
            		<td align="left" valign=top class="greybluetext10">
            			<select name='subject_code' class="greybluetext10" style={width:350px} >
            				<option value=''>--Select--</option>

            				<?
            						if ($subjectCode == "all")
	            					{
		            					$selctdAll = "selected";
	            					}
            				?>
            				<option value='all' <?=$selctdAll?>>--All--</option>
            				<?

            					while (list($code,$Scode,$Sname) = mysql_fetch_row($res_subject_sel))
            					{
	            		    		if ($examCode=='all')
            						{
                   				/*		if ($code=='1')$exam_Name='JAIIB-Old';
	            						if ($code=='17')$exam_Name='DBF';
	            						if ($code=='21')$exam_Name='JAIIB-New';   
										*/
                                       $exam_Name = $exam_array[$code];
                                          
							

            						}
	            					print("<option value='$Scode' ");
	            					if ($subjectCode == $Scode)
	            					{
		            					print(" selected ");
	            					}

	            					if($examCode=='all')
	            					{
	            						print(">$Sname($exam_Name)</option>");
            						}
            						else
            						{
	            						print(">$Sname</option>");
            						}

	            				}
	            		?>
            			</select>
            		</td>
            	</tr>
            	<tr>
            		<td align="right" width=390 class="greybluetext10" valign=top>Medium: </td>
            		<td align="left" valign=top class="greybluetext10">
            			<select name='medium' class="greybluetext10" style={width:150px} <!--onchange='javascript:getsubjects();'-->>
            				<option value=''>--Select--</option>
            				<option value='EN' <?if($medium=='EN') {print("selected");} ?>  >English</option>
            				<option value='HI' <?if($medium=='HI'){ print("selected");} ?>>Hindi</option>
            			</select>
            		</td>
            	</tr>
             </table>
    		 <table width=780 >
        	 <tr>
				<td colspan=2 align="center"><input type=button name='sub_assign' value='Submit' class='button' onClick='javascript:submitForm()'>
				</td>
    	    </tr>
      <? if (($submitted=='true') && ($ncount < 1))
         {       ?>
       		<tr>
       			<td width=780 align=center class=alertmsg>No Records to display</td>
       		</tr>
       		
     <?  }      ?>
     
    		</table>
    		
 <?      if(($submitted=='true') && ($ncount >=1 ))
         {       
         
    	print("<table border=1 cellspacing=0 cellpadding=2><tr>");
		for($a=0;$a<$header_count;$a++)
		{
			print("<td class=greybluetext10><b>$exce_header[$a]</b></td>");
		}
		print("</tr>");
		print("<tr>");
/*** It checks with the flag value and it retrieving the data from the array and displaying the report in the 
     html page  **/
           
           
                $header_count=$header_count+1;
/* Displaying the Cafe capacity report in the html part. Retrieving the data from the array
 	          */
          		 for($j=0;$j<$ncount;$j++)
	            	 {
		            	for($k=0;$k<$header_count-1;$k++)
		            	{
			          		$chek = $header_count-1;

/* Background color of the each column is changed depends on conditions */	
			          					          
			       		$bgcolor = $bgcolor_array[$exce_content[$j][$chek]];  
			    		$fontc   ="black";
			          
			         ?><td class="greybluetext10" bgcolor="<?=$bgcolor?>"><font color="<?=$fontc?>"><?=$exce_content[$j][$k]?></font></td>
			         <?
		         		}        
	            print("</tr>");
            }
        
        ?>    
   </table>
    <table width=300  cellspacing=5 border=0 align=center > <?
       foreach($exam_array as $disp_exam_code => $disp_exam_name)
    	{	
			?>
				<tr>
					<td colspan=2 bgcolor= <?=$bgcolor_array[$disp_exam_code]?>></td>
					<td class="greybluetext10" colspan=4><?=$disp_exam_name?></td>
				</tr>
			<?	
			} 
           ?>
	</table>		
	<table>
	  	    <tr><td class=greybluetext10>&nbsp;</td></tr>     
	    	<tr><td align="center" class=greybluetext10><input class='button' type='button' name='download' value='Download' onClick='javascript:excelsubmit()'></td></tr>
		    <tr><td class=greybluetext10>&nbsp;</td></tr>     
	  
	     </table>
	   <?  } ?>   
       </TD>
     </TR>   
     <TR>
    	<?include("includes/footer.php");?>
    </TR>
</TABLE>
</form>
</center>
</BODY>
</HTML>
