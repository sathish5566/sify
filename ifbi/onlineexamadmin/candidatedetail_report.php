<?php
/****************************************************
* Application Name				            :  IIB
* Module Name			                    :  Mapping Report
* Revision Number							 :  1
* Revision Date								 :
* Table(s)											 :
* Tables used for only selects		 : iib_candidate_iway,iib_candidate,iib_iway_details,iib_exam,iib_exam_subjects
* View(s)											 :  -
* Stored Procedure(s)						 :  -
* Dependant Module(s)					 :  -
* Output File(s)								     : CSV file
* Document/Reference Material	 :
* Created By										 : 
* Created ON									 :
* Last Modified By				             :  K KARUNA MOORTHY RAJU
* Last Modified Date						 :  20-04-2006
* Description                 : This report displaying the mapping report for the candidates mapped to the respective exam centres.
*****************************************************/

ob_start();
require("dbconfig_slave_123.php");
require("sessionchk.php");
require("constants.inc");
/* Include file for excel download */
include_once("excelconfig.php");
set_time_limit(0);

/**  To select the subjects for the respective examinations  **/

$choice        =   isSet($_REQUEST["choice"])    ?   $_REQUEST["choice"]   : "";
/** It forms the array for the header information for the excel export   **/

$exce_header=array("Membership No","Candidate Name","Candidate Password","Candidate Address","Exam Time","Centre Code","Cafe Address","City","Contact No.");

//Post variables
$exam_code     =   isSet($_POST["sExamList"])    ?   $_POST["sExamList"]   : "";
$subject_code  =   isSet($_POST["sSubjectList"])    ?   $_POST["sSubjectList"]   : "";

/* $ex_flag is for getting the values when clicking download button and $filename is used for
downloading the file with its value **/

$ex_flag             =   $_POST['exl'];
$filename="candidatedetail_sub(".$subject_code.")";
if ($exam_code != "" && $subject_code !="" )
{

	$sql_memname="select upper(membership_no),upper(centre_code),exam_time from iib_candidate_iway where exam_code='$exam_code' and subject_code='$subject_code'";
	$res_memname=mysql_query($sql_memname);
	$sql_mempass="select upper(membership_no),name,password,address1,address2,address3,address4,address5,address6 from iib_candidate ";
	$res_mempass=mysql_query($sql_mempass);
	while(list($memno,$Name,$pwd,$add1,$add3,$add4,$add5,$add6)=mysql_fetch_row($res_mempass))
	{
		$passArr[$memno][0]=$Name;
		$passArr[$memno][1]=$pwd;
		$passArr[$memno][2]=$add1.$add2.$add3.$add4.$add5.$add6;
	}
	$sql_qp = "Select upper(centre_code),iway_address1,iway_address2,iway_city from iib_iway_details";
	$res_qp = mysql_query($sql_qp);
	while (list($CCode,$addr1,$addr2,$city)=mysql_fetch_row($res_qp))
	{
		$cafeArr[$CCode][0]=$addr1;
		$cafeArr[$CCode][1]=$addr2;
		$cafeArr[$CCode][2]=$city;
	}
	if(mysql_num_rows($res_memname)>0)
	{
		$i=0;
/**** Getting the candidate and iway details into an array named exce_content[][].      ****/
		while(list($membership_no,$centre_code,$Etime) = mysql_fetch_row($res_memname))
		{ 
			$cand_address = str_replace('"',"'",$passArr[$membership_no][2]);
			$cafe_address  = str_replace('"',"'",$cafeArr[$centre_code][0]);
			$cand_name     = str_replace('"',"'",$passArr[$membership_no][0]);
			$cand_pass      = $passArr[$membership_no][1];
			$ta_city               = str_replace('"',"'",$cafeArr[$centre_code][2]);
			$ta_contact        = str_replace('"',"'",$cafeArr[$centre_code][1]);
			$ta_contact        = str_replace(",","/",$ta_contact);
			$exce_content[$i][]=$membership_no;
			//$exce_content[$i][]="\"$cand_name\"";
			$exce_content[$i][]=substr("\"$cand_name\"",1,strlen("\"$cand_name\"")-2);
			$exce_content[$i][]=$cand_pass;
			//$exce_content[$i][]="\"$cand_address\"";
			$exce_content[$i][]=substr("\"$cand_address\"",1,strlen("\"$cand_address\"")-2);
			$exce_content[$i][]=$Etime;
			$exce_content[$i][]=$centre_code;
			//$exce_content[$i][]="\"$cafe_address\"";
			//$exce_content[$i][]="\"$ta_city\"";
			//$exce_content[$i][]="\"$ta_contact\"";
			$exce_content[$i][]=substr("\"$cafe_address\"",1,strlen("\"$cafe_address\"")-2);
			$exce_content[$i][]=substr("\"$ta_city\"",1,strlen("\"$ta_city\"")-2);
			$exce_content[$i][]=substr("\"$ta_contact\"",1,strlen("\"$ta_contact\"")-2); 

			$i++;
		}//while $res_memname
		/* Execution of excel download function */
		if($ex_flag=='1')
		{
			excelconfig($filename,$exce_header,$exce_content);
			exit;	 
		}//end if for $ex_flag 
	}//if number of rows of $res_memname
	
}//if exam code and subject code
?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>
BODY
{
	FONT-FAMILY: Arial, Verdana
}
.textblk11
{ font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; text-decoration: none}
</STYLE>

<script language='JavaScript'>
function submitForm()
{
	var frm = document.frmcandidate;
	if (frm.sExamList.selectedIndex == 0)
	{
		alert("Please select the Exam ");
		frm.sExamList.focus();
		return false;
	}
	if (frm.sSubjectList.selectedIndex == 0)
	{
		alert("Please select the Subject");
		frm.sSubjectList.focus();
		return false;
	}
	frm.exl.value='1';
	frm.submit();
}
function redrawform(a){
	var frm = document.frmcandidate;
	if (frm.sExamList.options.selectedIndex == 0){
		alert ("Please select a Exam");
		frm.sExamList.focus();
		return;
	}
	if (a > 1){
		if (frm.sSubjectList.options.selectedIndex == 0){
			alert ("Please select a Subject");
			frm.sSubjectList.focus();
			return;
		}
	}
	frm.action="candidatedetail_report.php?choice="+a;
	frm.submit();
}

</script>



</head>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
		<tr><td><?include("includes/header.php");?> </td></tr>
      <tr> 
	    
	</tr>
	<tr>
		<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>
	</tr>
	<TR>
	    <TD background=images/tile.jpg vAlign=top width=780 align=center>
	<form name='frmcandidate' method='post'>
	<input type='hidden' name='exl' value='0'>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		  <td height="24">
			<div align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Mapping Report</strong></font></div>
		  </td>
		</tr>
	</table>
	<table border="0" cellpadding="0" cellspacing="10" width=780 align=center>
		<tr>
			<td align="right" class=greybluetext10 width=30%>&nbsp;</td>
			<td align="left" class=greybluetext10 width=10%><b>Exam :</b></td>
			<td align=left width=60%>
			<Select name=sExamList onchange="redrawform(1)" class=textbox style={width:150px}>
			<option>Select</option>
			<?
					$sql = "select exam_code,exam_name from iib_exam where online='Y' order by exam_code";
					$res = mysql_query($sql) or die ("Connection cannot be done3 error:'".mysql_error ($db)."'");
					if (mysql_num_rows($res) < 1)
					echo "<font class=\"alertmsg\">No Exams under the list</font>";
					else
						{
							while($num = mysql_fetch_row($res)){
							if ($exam_code == $num[0] and $_REQUEST["hSubmitted"] !='true')
								echo ("<option value=".$num[0]." selected>".$num[1]."</option>");
							else
								echo ("<option value=".$num[0].">".$num[1]."</option>");
							}
					}
			?>
		</select>
		</td>
	</tr>
	<tr>
			<td align="right" class=greybluetext10 width=30%>&nbsp;</td>
			<td align="left" class=greybluetext10 width=10%><b>Subject :</b></td>
			<td align=left width=60%>
			<?
						$sql = "select subject_code,subject_name from iib_exam_subjects where exam_code='$exam_code'  and online='Y' order by subject_code";
						$res = mysql_query($sql) or die ("Connection cannot be done3 error:'".mysql_error ($db)."'");
						if (mysql_num_rows($res) < 1) {
							echo "<font class=\"alertmsg\">No subjects under the list</font>";
						}
						else
						{
							echo "<Select name=sSubjectList class=textbox style={width:150px}><option>Select a Subject</option>";
								if (trim($exam_code) != NULL) {
									if ($choice >= 1 ){
										while($num = mysql_fetch_row($res)){
											if ($subject_code == $num[0])
												echo ("<option value=".$num[0]." selected>".$num[1]."</option>");
											else
												echo ("<option value=".$num[0].">".$num[1]."</option>");
										}
									}
							}
					}
			?>
			</select>
			</td>
	</tr>
	<tr>
		<td align=center colspan=3><input class='button' type='button' name='sub' value='Submit' onClick='javascript:submitForm()'></td>
	</tr>
</select>
</table>
</form>
</TD>
</TR>
	 <tr>
    <? include("includes/footer.php");?>
  </tr>
</TABLE>
</center>
</BODY>
</HTML>
