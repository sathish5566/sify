<?php
	require_once("dbconfig.php");
	set_time_limit(0);
	
	//usage : offline_generate.php exam_code subject_code no_of_papers total_marks section_count marks_string is_sample
	
	$commonDebug = false;
	
	/*$examCode = $aArgs[1];
	$subjectCode = $aArgs[2];
	$nPapers = $aArgs[3];
	$totalMarks = $aArgs[4];
	$sectionCount = $aArgs[5];
	$strMarks = $aArgs[6];
	$isSample = $aArgs[7];*/

	$aMarks = array();
	if ($strMarks != "")
	{
		$aMarks = explode("-", $strMarks);
	}
	
	$nMarks = count($aMarks);
	$aMarksCount = array();
	for ($i = 0; $i < $nMarks; $i++)
	{
		$aCount = array();
		$aCount = explode(":", $aMarks[$i]);
		$aMarksCount[$aCount[0]] = $aCount[1];
	}
	
	$sql = "SELECT section_code FROM iib_subject_sections WHERE subject_code='$subjectCode' and online='Y' ORDER BY section_code";
	if ($commonDebug)
	{
		print "query :".$sql;
	}
	$result = @mysql_query($sql) or die("Query failed");
	$nRows = mysql_num_rows($result);
	if ($nRows == 0)
	{
		print("<br>This subject has no sections<br>");
		return;
	}
	$aSectionCode = array();
	while (list($sectionCode) = mysql_fetch_row($result))
	{
		$aSectionCode[] = $sectionCode;
	}
		
	for ($nCount = 0; $nCount < $nPapers; $nCount++)
	{
		$sqlQuestions = "SELECT question_paper_no FROM  iib_question_paper WHERE complete='N' ".
		" AND exam_code='$examCode' AND subject_code='$subjectCode' ";
		if ($commonDebug)
		{
			print "query :".$sqlQuestions;
		}
		$resQuestions = @mysql_query($sqlQuestions) or die("select on iib_question_paper failed");
		$nIncomplete = mysql_num_rows($resQuestions);
		if ($nIncomplete > 0)
		{
			$strIncomplete = "";
			while (list($incompleteQns) = mysql_fetch_row($resQuestions))
			{
				($strIncomplete == "") ? $strIncomplete = $incompleteQns : $strIncomplete .= ", ".$incompleteQns;
			}
			$sqlDelete = "DELETE FROM iib_question_paper WHERE question_paper_no IN ($strIncomplete)";
			if ($commonDebug)
			{
				print "query :".$sqlDelete;
			}
			@mysql_query($sqlDelete) or die("Delete on iib_question_paper failed");
			$sqlDelete = "DELETE FROM iib_qp_weightage WHERE question_paper_no IN ($strIncomplete)";
			if ($commonDebug)
			{
				print "query :".$sqlDelete;
			}
			@mysql_query($sqlDelete) or die("Delete on iib_qp_weightage failed");			
			$sqlDelete = "DELETE FROM iib_question_paper_details WHERE question_paper_no IN ($strIncomplete)";
			if ($commonDebug)
			{
				print "query :".$sqlDelete;
			}
			@mysql_query($sqlDelete) or die("Delete on iib_question_paper_details failed");			
		}
		$sqlInsert = "INSERT INTO iib_question_paper ". "(exam_code,subject_code,total_marks,sample,enabled,online,assigned,medium_code) ".
		"VALUES ('$examCode','$subjectCode','$totalMarks','$isSample','Y','N','N','$mediumCode')";
		if ($commonDebug)
		{
			print "query :".$sqlInsert;
		}

		@mysql_query($sqlInsert) or die("Insert on iib_question_paper failed");
		$questionPaperNo = mysql_insert_id();
		
		//question_paper_no| exam_code| subject_code| section_code| marks| no_of_questions		
		$aSectionWiseCount = array();
		for ($j = 0; $j < $nStoredMarks; $j++)
		{
			$strMarks = $aMarksCount[$j];
			$aSectionMarks = array();
			$aSectionMarks = explode(",", $strMarks);
			for ($i = 0; $i < $sectionCount; $i++)
			{
				$aSectionWiseCount[$i][$j] = $aSectionMarks[$i];
			}
		}
		$aQuestions = array();
		$cnt = 0;
		for ($j = 0; $j < $nStoredMarks; $j++)
		{
			for ($i = 0; $i < $sectionCount; $i++)
			{
				$sectionCode = $aSectionCode[$i];
				$sectionMarks = $aStoredMarks[$j];
				$questionCount = $aSectionWiseCount[$i][$j];
				
				$sqlInsert1 = "INSERT INTO iib_qp_weightage ".
					"(question_paper_no, exam_code, subject_code, section_code, marks, ".
					" no_of_questions) VALUES ".
					"('$questionPaperNo', '$examCode', '$subjectCode', '$sectionCode', ".
					"'$sectionMarks', '$questionCount')";
				if ($commonDebug)
				{
					print("\n".$sqlInsert1);
				}
				@mysql_query($sqlInsert1) or die("Insert on iib_qp_weightage failed");
				if ($questionCount > 0)
				{
					$sqlQuestions = "SELECT question_id, question_code FROM iib_section_questions WHERE ".
					" exam_code='$examCode' AND subject_code='$subjectCode' AND section_code='$sectionCode' ".
					" AND marks='$sectionMarks' ORDER BY rand() LIMIT $questionCount";
					if ($commonDebug)
					{
						print("\n".$sqlQuestions);
					}
				
					$result = @mysql_query($sqlQuestions) or die("select from iib_section_questions failed");
					$nRows = mysql_num_rows($result);
					
					if ($nRows > 0)
					{
						while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
						{
							$questionID = $row['question_id'];
							$questionCode = $row['question_code'];
							$aQuestions[$cnt][0] = $questionID;
							$aQuestions[$cnt][1] = $sectionCode;
							$aQuestions[$cnt][2] = $sectionMarks;
							$cnt++;
						}
					}
				} // end of if question count > 0
			} // end of for section count
		} // end of for marks
		if ($commonDebug)
		{
			print ("<br>questions :");
			print_r($aQuestions);
		}
		
		$nQuestions = count($aQuestions);
		srand ((float) microtime() * 10000000);
		$aRandQuestions = array_rand($aQuestions, $nQuestions);
		
		if ($commonDebug)
		{
			print ("<br>questions 2 :");
			print_r($aRandQuestions);
		}
		
		for ($qCount = 0; $qCount < $nQuestions; $qCount++)
		{
			$iCount = $qCount+1;
			$index = $aRandQuestions[$qCount];
			$questionID = $aQuestions[$index][0];
			$sectionCode = $aQuestions[$index][1];
			$sectionMarks = $aQuestions[$index][2];
			$sqlOptions = "SELECT option_1, option_2, option_3, option_4, option_5 FROM iib_section_questions WHERE ".
								" exam_code='$examCode' AND subject_code='$subjectCode' AND section_code='$sectionCode' ".
								" AND question_id='$questionID'";
			$resOptions = @mysql_query($sqlOptions) or die("cannot execute query");
			$options = mysql_fetch_row($resOptions);
							
			$input = array();
			$rand_keys = array();
			$strOptOrder = "";
			
			for ($optCnt = 1; $optCnt <= 5; $optCnt++)
			{	
				if ($options[$optCnt-1] != "")
				{							
					$input[$optCnt] = $optCnt;								
				}
			}

			srand ((float) microtime() * 10000000);
			$rand_keys = array_rand ($input, count($input));
			if ($commonDebug)
			{
				print("<br>rand keys: ");
				print_r($input);
				print_r($rand_keys);
				print("<br>");
			}
			$strOptOrder = implode($rand_keys,",");
							
			// question_paper_no,| section_code  | question_id | answer    
			$sqlInsert2 = "INSERT INTO iib_question_paper_details ".
							"(question_paper_no, subject_code, section_code, question_id, answer_order, display_order) ".
							"VALUES ('$questionPaperNo', '$subjectCode', '$sectionCode', ".
							"'$questionID', '$strOptOrder', $iCount)";
			if ($commonDebug)
			{
				print("\n".$sqlInsert2);
			}
			
			@mysql_query($sqlInsert2) or die("Insert on iib_question_paper_details failed");
			
		} // end of for qCount
		
		$sqlUpdate = "UPDATE iib_question_paper SET complete='Y' WHERE question_paper_no='$questionPaperNo' ";
		if ($commonDebug)
		{
			print "query :".$sqlUpdate;
		}
		
		@mysql_query($sqlUpdate) or die("Update on iib_question_paper failed");
		
	}//end of for-papercount
	$strAttributesList = "generate_offline.php $examCode $subjectCode $nPapers $totalMarks $sectionCount $strMarks $isSample $mediumCode";
		   
	$sqlInsert = "INSERT INTO iib_question_paper_generation (attributes_list, transaction_time) VALUES ('$strAttributesList', now())";
	if ($commonDebug)
	{
		print("\n".$sqlInsert);
	}
	
	@mysql_query($sqlInsert) or die("Insert on iib_question_paper_generation  failed");

?>