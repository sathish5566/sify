<?php 
ob_start();
set_time_limit(0);
ini_set("memory_limit",'300M');

function isExists($path)
{ 
	return file_exists($path);
}
function getNumFiles($path) 
{  
 // return 1;
 return count(glob($path.'*'));
} 
function arrayToJSObject($array, $varname, $sub = false ) {
    $jsarray = $sub ? $varname . "{" : $varname . " = {\n";
    $varname = "\t$varname";
    if(is_array($array)){
		reset($array);
		// Loop through each element of the array
		while (list($key, $value) = each($array)) {
			$jskey = "'" . $key . "' : ";
		   
			if (is_array($value)) {
				// Multi Dimensional Array
				$temp[] = arrayToJSObject($value, $jskey, true);
			} else {
				if (is_numeric($value)) {
					$jskey .= "$value";
				} elseif (is_bool($value)) {
					$jskey .= ($value ? 'true' : 'false') . "";
				} elseif ($value === NULL) {
					$jskey .= "null";
				} else {
					static $pattern = array("\\", "'", "\r", "\n");
					static $replace = array('\\', '\\\'', '\r', '\n');
					$jskey .= "'" . str_replace($pattern, $replace, $value) . "'";
				}
				$temp[] = $jskey;
			}
		}		
		$jsarray .= @implode(', ', $temp);
		
	}    
    $jsarray .= "}\n";
    return $jsarray;
} 

require("dbconfig.php"); 
include("constants.inc");
$examcode=$_REQUEST['examcode'];
$subjectcode=$_REQUEST["subjectcode"];
$mediumcode=$_REQUEST["medium"];

if ($mediumcode == "H")
{
	$tableName = "iib_section_questions_hindi";
	$tableName_case = "iib_case_master_hindi";		
}
else
{
	$tableName = "iib_section_questions";
	$tableName_case = "iib_case_master";
}
$sqlSubject = "SELECT answer_shuffling FROM iib_exam_subjects WHERE subject_code='$subjectcode' AND exam_code='$examcode'";
if ($commonDebug)
{
	print("\n".$sqlSubject);
}
$resSubject = @mysql_query($sqlSubject) or die("select from iib_exam_subjects failed ".mysql_error());
list($answer_shuffling) = mysql_fetch_array($resSubject);

?>
<html>
<script language='javascript'>
function html_generate(status)
{
	var exam_code=document.frm_html.h_exam.value;
	var sub_code=document.frm_html.h_subject.value;
	var qp_count=document.frm_html.h_qpcount.value;
	var min_qp=document.frm_html.h_min.value;
	var max_qp=document.frm_html.h_max.value;
	var medium=document.frm_html.h_medium.value;
	
	document.frm_html.action="html_generate.php?ec="+exam_code+"&sc="+sub_code+"&status="+status+"&count="+qp_count+"&min_qp="+min_qp+"&max_qp="+max_qp+"&medium="+medium;
	document.frm_html.submit();
}
</script>
<body>
<form name="frm_html" method="post">
<input type="hidden" name="h_exam" value='<? echo $examcode?>'>
<input type="hidden" name="h_subject" value='<? echo $subjectcode?>'>
<input type="hidden" name="h_medium" value='<? echo $mediumcode?>'>
<?php 
$str='';
//$str.="<html>\n";
//$str.="<head>\n";
//$str.="<title></title>\n";
//$str.="</head>\n";
//$str.="<body>\n";

$str.='<?PHP if($_SERVER["PHP_SELF"]!="/online_exam.php"){';
$str.='ob_end_clean();';
$str.='header("WWW-Authenticate: Basic Realm=BALA");';
$str.='header("HTTP/1.0 401 Unauthorized");';
$str.='print("<h1>You are Not Authorized to view this page!!!</h1>");';
$str.='exit;}?>';

//$str.="<table bgcolor='#E8EFF7' border='0' cellspacing='1' cellpadding='3' valign=top width=100%>";

$tmp=$str;
$questionpaper_array=array();
$alpha = array('a', 'b', 'c', 'd', 'e');
$sel="select question_paper_no from iib_question_paper where exam_code='$examcode' and subject_code='$subjectcode' and  html_qp_generated='N' order by question_paper_no limit 1000";
$res=mysql_query($sel);
$count_qp=mysql_num_rows($res);
if($count_qp == 0)
{ ?>
	<input type="hidden" name="h_qpcount" value='<? echo $count_qp?>'>
	<input type="hidden" name="h_min" value=''>
	<input type="hidden" name="h_max" value=''>
	<script language="javascript">
	 html_generate(0);	
	</script>
<?PHP 
}
	while(list($qpno)=mysql_fetch_row($res))
	{
		$qp_array[$qpno]=$qpno;
		array_push($questionpaper_array,"'".$qpno."'");
	}
	$questionpaperno_text=implode(",",$questionpaper_array);
	$min_qp = min($qp_array);	
	$first="true";  
	
	/* Case Questions */
	$sel_caseques="select case_id,case_text,case_marks,section_code from $tableName_case where exam_code='$examcode' and subject_code='$subjectcode' and is_active='1' order by case_id";
	$sql_case = mysql_query($sel_caseques) or die("Selection in mysql failure");
	$num_caseques = mysql_num_rows($sql_case);
	if($num_caseques > 0)
	{
		 while(list($case_id,$case_text,$case_marks,$case_section) = mysql_fetch_row($sql_case))
		 {
				$case_array[$case_id]['case_id'] = $case_id;
				$case_array[$case_id]['case_text'] = $case_text;
				$case_array[$case_id] ['case_marks'] = $case_marks;
				$case_array[$case_id] ['case_section'] = $case_section;				
		 }
	}				
		
	/* Section Questions */
	$sel_section_ques="select question_id,question_text,option_1,option_2,option_3,option_4,option_5,marks,case_id,case_type from $tableName where exam_code='$examcode' and subject_code='$subjectcode' order by question_id";
	$sql_section = mysql_query($sel_section_ques) or die("Selection in mysql failure");
	$num_ques = mysql_num_rows($sql_section);
	if($num_ques > 0)
	{
		 while(list($q_id,$q_text,$q_opt1,$q_opt2,$q_opt3,$q_opt4,$q_opt5,$q_marks,$case_id,$case_type) = mysql_fetch_row($sql_section))
		 {
				$section_text_array[$q_id]=$q_text;
				$option_1_array[$q_id] =$q_opt1;
				$option_2_array[$q_id] =$q_opt2;
				$option_3_array[$q_id] =$q_opt3;
				$option_4_array[$q_id] =$q_opt4;
				$option_5_array[$q_id] =$q_opt5;						
				$marks_array[$q_id] =$q_marks;
				if($case_type == 'Y')
					$casesection_array[$q_id] = $case_id;
				else
					$casesection_array[$q_id]=0;
		 }
	}		
$sel_qn="select question_paper_no,question_id,display_order,answer_order from iib_question_paper_details where question_paper_no IN ($questionpaperno_text) and subject_code='$subjectcode' order by question_paper_no";

	
	$res_qn=mysql_query($sel_qn) or die(mysql_error());
	$count_n = mysql_num_rows($res_qn);
	$qp_weight= "select sum(no_of_questions) from iib_qp_weightage where exam_code='$examcode' and subject_code='$subjectcode' group by question_paper_no limit 1";
	
	$sql_weight=mysql_query($qp_weight) or die(mysql_error());
	list($qp_count) = mysql_fetch_row($sql_weight);
    $i=0;
	while($details_arr=mysql_fetch_assoc($res_qn))
	{		
		$qp_id =$details_arr['question_id'];
		$qp_no = $details_arr['question_paper_no'];		
		$disp_order =$details_arr['display_order']; 
		$ans_order =$details_arr['answer_order'];		
		$qp_text = $section_text_array[$qp_id];
		$opt1 = $option_1_array[$qp_id];
		$opt2 = $option_2_array[$qp_id];
		$opt3 = $option_3_array[$qp_id];
		$opt4 = $option_4_array[$qp_id];
		$opt5 = $option_5_array[$qp_id];
		$marks = $marks_array[$qp_id];
		$case_id = $casesection_array[$qp_id];
		
		$qustn_arr[$qp_no][$qp_id][0]= $qp_no;
		$qustn_arr[$qp_no][$qp_id][1]= $qp_text;
		$qustn_arr[$qp_no][$qp_id][2]= $opt1;
		$qustn_arr[$qp_no][$qp_id][3]= $opt2;
		$qustn_arr[$qp_no][$qp_id][4]= $opt3;
		$qustn_arr[$qp_no][$qp_id][5]= $opt4;
		$qustn_arr[$qp_no][$qp_id][6]= $opt5;
		$qustn_arr[$qp_no][$qp_id][7]= $qp_id;
		$qustn_arr[$qp_no][$qp_id][8]= $marks;
		$qustn_arr[$qp_no][$qp_id][9]=$ans_order;
		$qustn_arr[$qp_no][$qp_id][10]=$case_id;				
		$qid_array[$qp_no][$disp_order]=$qp_id;						
		$i++;
	}		
	$dbqp_count = count($qp_array);
	$total_count =$min_qp+$dbqp_count;	
	?>
	<input type="hidden" name="h_qpcount" value='<? echo $count_qp?>'>
	<input type="hidden" name="h_min" value='<? echo $min_qp?>'>
	<input type="hidden" name="h_max" value='<? echo $total_count-1?>'>
	<?		
	for($j=$min_qp;$j<$total_count;$j++)
	{
		$question_paper_no=$qp_array[$j];
		$qn_no=0;
		$arrQuestionid=array();
		if($first=="true")
		{
			$lwrlimit=$qp_array[$j];
			$first="false";
			$uprlimit=$lwrlimit + ($MAXIMUM_FILE_COUNT-1);
		}		
		for($k=1;$k<=$qp_count;$k++) 
		{
			$qid=$qid_array[$j][$k];

			if ($mediumcode == 'H')
				$questionText="<font class='qn'>".$qustn_arr[$j][$qid][1]."</font>";
			else
			   $questionText=$qustn_arr[$j][$qid][1];
			
			$questionID=$qustn_arr[$j][$qid][7];
			$questionMarks=$qustn_arr[$j][$qid][8];
			 $case_id=$qustn_arr[$qp_no][$qid][10];
			
			/*if($qn_no==0)
				$str=$tmp."<tr><td>\n<div id='q$qn_no'>\n";
			else
				$str=$str."<tr><td>\n<div id='q$qn_no'>\n";
			*/
			if($qn_no==0)
				$str=$tmp."<div id='q$qn_no'>\n";
			else
				$str=$str."<div id='q$qn_no'>\n";
			
			
			$str=$str."<table align='left' bgcolor='#E8EFF7' border='0' cellspacing='3' cellpadding='3' valign=top width=100%>\n";
			$str=$str."<input type=hidden name=question_id[".$qn_no."] value='$questionID' />\n"; 
			$str=$str."<input type=hidden name=answer_order[".$qn_no."] value='$answer_order' />\n";
			$qno=$qn_no+1;			
			if(isset($case_id) && $case_id!=0){
				if ($mediumcode == 'H'){
					$str=$str."<tr>\n<td class='greybluetext10' bgcolor='#E8EFF7' align=left valign=top colspan=2 ><font class='qn'><b>Please answer the question  basis of the following paragraph/caselet.</b><br/><br/>".$case_array[$case_id]['case_text']."</font></td>\n</tr>\n";
				}else{
					$str=$str."<tr>\n<td class='greybluetext10' bgcolor='#E8EFF7' align=left valign=top colspan=2 ><b>Please answer the question  basis of the following paragraph/caselet.</b><br/><br/>".$case_array[$case_id]['case_text']."</td>\n</tr>\n";
				}
				
			}	
			$str=$str."<tr>\n<td class='greybluetext10' bgcolor='#E8EFF7' align=right valign=top nowrap><b>(Mark/s - $questionMarks)</b><br/><a href='javascript:eraseAnswer($questionID,$qn_no)'>Erase Answer</a></td>\n</tr>\n";													
			$str=$str."<tr>\n<td class='greybluetext10' bgcolor='#E8EFF7' valign=top align='left'><b>Q".$qno.". ".$questionText."</b></td>\n</tr>\n";
			
			//Code added to display answer shuffled					
			$dis_answer_order=explode(",",$qustn_arr[$j][$qid][9]);						
			if(trim($answer_shuffling)=="N")
			{
						sort($dis_answer_order);
			}						
			$rand_keys = array();							
			foreach($dis_answer_order as $key=> $i)
			{
					$str.="<tr>\n";
					$var = "option".$i;							
					$$var = $qustn_arr[$j][$qid][$i+1];
					
					if (trim($$var) != "")
					{										
						$str.="<td class='greybluetextans' bgcolor='#E8EFF7' align='left' valign=middle colspan=2><input type='radio' id='answer".$questionID."' name='answer".$questionID."' value='".$i."'onClick='javascript:changeChecked($questionID,$qn_no,0)'/>". $alpha[$key].") <label onClick='javascript:changeChecked($questionID,$qn_no,$i)' dir=ltr>";	
							if ($mediumcode == 'H')
									$str.="<font class='ans'>".$$var."</font>";
							else
								$str.=$$var;
							$str.="</label></td>";
					}
					$str.="\n</tr>";
			}																	
			$arrQuestionid[$qn_no]=$questionID;			
			$qn_no++;
			$str=$str."</table>\n";			
			//$str=$str."</div>\n</td></tr>";
			$str=$str."</div>\n";						
		}				
		
		$str.="<input type=hidden name=question_count id=question_count value='$qp_count'>";
		$str.='<script type="text/javascript">'."\n";
		$str.=arrayToJSObject($arrQuestionid,'arrQuestionid'); 
		$str.="</script>\n";		
		//$str=$str."</table>\n";
		//$str=$str."</body>\n";
		//$str=$str."</html>\n";		
		$qn = $str;					
		$filename =  $examcode."_".$subjectcode."_".$question_paper_no.".php";
		if($filename!=""){
			$fld="/home/sites/ibps/questionpaper/";
			$handle=$qn;
			if(!is_dir($fld.$mediumcode))
			  mkdir($fld.$mediumcode,0777);
			if(is_dir($fld.$mediumcode)){
				$file=$fld.$mediumcode.'/'.$filename;
				$fp = fopen($file,"w");
				fwrite($fp,$str);
				fclose($fp);
			}else{
			  echo "Directory Not exist";
			  exit;
			}
			$upd_path="update iib_question_paper set html_qp_generated='Y' where question_paper_no='$question_paper_no'";
			$res_path=mysql_query($upd_path);
		}
}		$str='';
?>
<script language="javascript">
html_generate(1);
</script>
</form>
</body>
</html>
