<?php
require_once "sessionchk.php";
require_once "dbconfig.php";
//Centre Array
$arrCentre=array("1"=>"Chennai","2"=>"Mumbai");

//Get All Mapped TAs
$arrTAs = array();
$sqlTASel = "SELECT ta_login FROM iib_ta_iway ORDER BY ta_login";
$resTASel = mysql_query($sqlTASel);
if(mysql_num_rows($resTASel) > 0)
{
	while(list($valTAs) = mysql_fetch_array($resTASel))
	{
		$arrTAs[$valTAs] = $valTAs;
	}
}

if ($_POST["submitted"]=="true")
{
	$intLocationId = $_POST['selLocation'];
	$strLocation = $arrCentre[$intLocationId];
	$strMappedTAs = $_POST['mappedseries'];
	$filename = '/usr/local/apache2.0.52/htdocs/projects/ibps_load/update_logs/'.$intLocationId.'_talist.txt';
	$filename1 = $intLocationId.'_talist.txt';
	$content = $strMappedTAs;

	//if (is_writable($filename)) {

		if (!$handle = fopen($filename, 'w+')) {
			 $emsg = "Cannot open file ($filename1)";
			 exit;
		}

		if (fwrite($handle, $content) === FALSE) {
			$emsg = "Cannot write to file ($filename1)";
			exit;
		}

		$msg = "Successfully mapped.";

		fclose($handle);

	//} else {
		//$emsg =  "The file $filename1 is not writable";
	//}
}

	//Remove already mapped TAs
	$filePath = "/usr/local/apache2.0.52/htdocs/projects/ibps_load/update_logs/";
	foreach($arrCentre as $key=>$value)
	{
		$fileName = $filePath.$key."_talist.txt";
		if(file_exists($fileName))
		{
			$handle = fopen($fileName, "r");
			$contents = fread($handle, filesize($fileName));
			$arrMappedTAs = explode(",",trim($contents,","));
			foreach($arrMappedTAs as $k1 => $v1)
			{
				if(in_array($arrTAs[$v1],$arrMappedTAs))
				{
					unset($arrTAs[$v1]);
				}
			}
			fclose($handle);
		}

	}
//Remove already mapped TAs ends here

?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
	FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='JavaScript' src='./includes/validations.js'></script>
<script language='javascript'>
window.onerror = myfalse;
function submit_page(){
	var oMapped = document.getElementById("mapped");
	var oMappedSeries = document.getElementById("mappedseries");
	var hSubmitted = document.getElementById("submitted");
	oMappedLength = oMapped.length;
	
	if(oMapped.length <= 0){
			var alertmsg = "Kindly Map the TAs."
			alert(alertmsg);
			return false;
	}

	for(i=0;i<oMappedLength; i++){
		oMappedSeries.value += oMapped.options[i].value+",";
	}			
	var frm = document.map;
	frm.action="ta_url_redirect.php";
	hSubmitted.value="true";
	frm.submit();	
}

function reset_click(){
	var frm = document.map;
	var oMapped = document.getElementById("mapped");
	var oDatabase = document.getElementById("talist");
		
	oMappedLength = parseInt(oMapped.length);
	oDatabaseLength = oDatabase.length;
	frm.reset();	
	var k =0;
	while (k<oMappedLength)
	{
			j=0;
			if(oMapped.options[j].text !="") {
				oDatabase.appendChild(oMapped.options.item(j));
			}			
			k++;
	}
	return false;
}

function myfalse(){
	return false;
}

// automatic Mapping
function automap(){
	
	var oMapped = document.getElementById("mapped");
	var oDatabase = document.getElementById("talist");

	oMappedLength = parseInt(oMapped.length);
	var k =0;
	while (k<oMappedLength)
	{
			j=0;
			if(oMapped.options[j].text !="") {
				oDatabase.appendChild(oMapped.options.item(j));
			}			
			k++;
	}
	
	oMappedLength = parseInt(oMapped.length);
	oDatabaseLength = oDatabase.length;
	
	return false;
	
}



/**
*		Javascript for mapping the attributes
*/

function menuAdd(){
//select one list item from the Inactive menu
var oInactiveMenu = document.getElementById("talist");
var oActiveMenu = document.getElementById("mapped");

  if(IsFocus(oInactiveMenu)){
    var k = oInactiveMenu.length;
    var index = 0;
    //for adding the Ist list item to the active menu
    for(j=0; j<k; j++){
      if(oInactiveMenu.options[index].selected){
        //alert("selected: "+oInactiveMenu.selectedIndex);
        oActiveMenu.appendChild(oInactiveMenu.options.item(oInactiveMenu.selectedIndex));
      }else{
        //alert("Not selected: "+oInactiveMenu.options[index].text);
        index++;
      }
    }
  }// focussing the current list box
  return true;
}

function menuRemove(){
var oInactiveMenu = document.getElementById("talist");
var oActiveMenu = document.getElementById("mapped");

  if(IsFocus(oActiveMenu)){
    var k = oActiveMenu.length;
    var index = 0;
    for(j=0; j<k; j++){
      if(oActiveMenu.options[index].selected){
        //alert("selected: "+oActiveMenu.options[index].value);
        //oInactiveMenu.selectedIndex = oActiveMenu.selectedIndex;
        oInactiveMenu.appendChild(oActiveMenu.options.item(oActiveMenu.selectedIndex));
      }else{
        //alert("Not selected: "+oActiveMenu.options[index].text);
        index++;
      }
    }
  }// focussing the active menu box
  return true;
}


function swapMenu(source, destination) {
  var oActiveMenu = document.getElementById("mapped");
  var tempValue = oActiveMenu[source].value;
  var tempText = oActiveMenu[source].text;
  oActiveMenu[source].value = oActiveMenu[destination].value;
  oActiveMenu[source].text = oActiveMenu[destination].text;
  oActiveMenu[destination].value = tempValue;
  oActiveMenu[destination].text = tempText;
  oActiveMenu.selectedIndex = destination;
}

function IsFocus(object){
  if (object.value == ""){
    alert("Please select the "+ object.name +" To Proceed");
    return false;
  }
  return true;
}
</script>
</HEAD>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0 >
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
	<tr><td width="780"><?include("includes/header.php");?></td></tr>
	<tr>
		<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>
	<TR>
	    <TD background=images/tile.jpg vAlign=top width=780 align=center>
     	<form name=map method="post">
		<table width=600 cellpadding=0 cellspacing=3 border=0>
			<tr>
				<td colspan=2 align="center" ><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2>Config TA Login URL Redirection</font></b></td>
			</tr>
			<tr>
				<td class="greybluetext10" colspan="2">
				<table width="70%"  border="0" cellspacing="1" cellpadding="5" align="center">
				<tr align="center">
						<td  class="greybluetext10">Location to be redirected :</td>
						
						<td class="greybluetext10">
						<select name="selLocation" id="selLocation" class="textbox">
						  <?PHP foreach($arrCentre as $k=>$v)
						  {
								echo "<option value=\"$k\">$v</option>";
						   }
						   ?>						
						  </select>
						</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr align="center">
						<td  class="greybluetext10">TA List</td>
						<td>&nbsp;</td>
						<td class="greybluetext10">Mapped TAs</td>
					  </tr>
					  <tr align="center">
						<td class="greybluetext10" >
								<select name="talist[]" id="talist" size="13" style="width:120px">
								<?php					
										foreach($arrTAs as $key=>$value){
												echo '<option value="'.$key.'">'.$value.'</option>';
										}
								?>
						</select>
								</td>
								<td  align="center">
									<input type="image"  src="images/go-forward.png" name="remove" onClick="javascript:menuAdd();return false;">&nbsp;<br>
						  <input type="image" src="images/go-back.png" name="add" onClick="javascript:menuRemove();return false;">&nbsp;<br>
									</td>
							<td  align="center" class="greybluetext10">
								<select name="mapped[]" id="mapped" size="13" style="width:120px">
								</select>
								</td>
						
					  </tr>
					</table>				
				</td>
			</tr>
				<tr>
							<td align="right" width="30%"><input type="button" class=button name=bSubmit value="Save" onclick='javascript:submit_page();'></td>
							<td align="left" width="30%"><input type="button" class=button name=bReset value="Reset"  onClick="javascript:reset_click();return false;"></td>
						</tr>
			<tr>
				<td align="right"><input type="hidden" name="mappedseries" value="" id="mappedseries"></td>
				<td align="left"><input type="hidden" name="submitted"  id="submitted"></td> 
			</tr>
			<?if(isset($emsg)){?>	
			<tr>
				<td colspan=2 align="center" class=errormsg><?echo $emsg;?></td>
			</tr>		
		<?}?>
		<?if(isset($msg)){?>	
			<tr>
				<td colspan=2 align="center" class='alertmsg'><?echo $msg;?></td>
			</tr>		
		<?}?>
					
		</table>
		</form>
		</TD>
	</TR>
	<TR>
		<?include("includes/footer.php");?>
	</TR>
</TABLE>
</FORM>
</center>
</BODY>
</HTML>