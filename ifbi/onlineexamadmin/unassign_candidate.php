<?php
require_once "sessionchk.php";
require_once("dbconfig.php");
require_once("constants.inc");
$commonDebug = false;
$errormsg = "";
$membershipNo = trim($_POST['memno']);
if ($membershipNo == "")
{
	$membershipNo = trim($_POST['membership_no']);
	$numCentres = $_POST['num_centres'];
	if ($numCentres > 0)
	{
		//print_r($_POST);
		$aChecked = $_POST['chk'];
		$nChecked = count($aChecked);
		for ($nCount = 0; $nCount < $nChecked; $nCount++)
		{
			$checkedVal = $aChecked[$nCount];
			if ($checkedVal != "")
			{
				$aCols = explode("|", $checkedVal);
				//print_r($aCols);
				$examCode = $aCols[0];
				$subjectCode = $aCols[1];
				$examDate = $aCols[2];
				$examTime = $aCols[3];
				$centreCode = $aCols[4];
				$sqlDelete = "DELETE FROM iib_candidate_iway WHERE exam_code='$examCode' AND subject_code='$subjectCode' AND exam_date='$examDate' ".
				" AND exam_time='$examTime' AND centre_code='$centreCode' AND membership_no='$membershipNo' ";
				if ($commonDebug)
				{
					print($sqlDelete);
				}
				@mysql_query($sqlDelete);
				$nRows = @mysql_affected_rows();
				//print("rows = ".$nRows);
				if ($nRows > 0)
				{
					$sqlUpdate = "UPDATE iib_iway_vacancy SET filled = filled-1, remaining = remaining+1 WHERE centre_code='$centreCode' ".
					" AND exam_date='$examDate' AND exam_time='$examTime' ";
					if ($commonDebug)
					{
						print($sqlUpdate);
					}
					@mysql_query($sqlUpdate);
					$sqlUpdate = "UPDATE iib_exam_candidate SET alloted='N' WHERE membership_no='$membershipNo' AND ".
					 " exam_code='$examCode' AND subject_code='$subjectCode' ";
					if ($commonDebug)
					{
						print($sqlUpdate);
					}
					@mysql_query($sqlUpdate);
				}
			}
		}
	}
}
if ($membershipNo != "")
{
	$sqlSelect = " SELECT centre_code, exam_code, subject_code, exam_date, exam_time FROM iib_candidate_iway ".
		" WHERE membership_no='$membershipNo' ORDER BY exam_date ";
		
	if ($commonDebug)
	{
		print("\n".$sqlSelect);
	}		
	$result = @mysql_query($sqlSelect) or die("select iib_candidate_test failed ".mysql_error());			
	$nCentres = mysql_num_rows($result);
	if ($nCentres == 0)
	{				
		$errormsg = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This candidate has not been assigned to any I Way";
	}
}
else
{
	if ($_POST['submitted'] == 'Y')
	{
		$errormsg = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Membership No. found empty";
	}
}
?>
<html>
<head>
<title><?=PAGE_TITLE ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<script language='JavaScript'>
	history.go(1);
	</script >
<script language='JavaScript' src="./includes/validations.js"></script>
<script language="javascript">
function validate()
{
	Memno=document.unassignfrm.memno.value;
	Memno=trim(Memno);

	if (Memno == '')
	{
		alert('Please enter Membership No!');
		document.unassignfrm.memno.focus();
		return false;
	}
	else
	if (Memno.indexOf(' ') != -1)
	{
		alert('Membership No. cannot contain space');
		document.unassignfrm.memno.focus();
		return false;
	}
	else
	{
		document.unassignfrm.action="<?=$_SERVER['PHP_SELF'] ?>";
		document.unassignfrm.target = '_self';
		document.unassignfrm.memno.value=Memno;	
		document.unassignfrm.submit();	
	}
}

function SetChecked(chkName) 
{
	var val = document.frmunassign2.chkall.checked;
	dml=document.frmunassign2;
	len = dml.elements.length;
	var i=0;
	for( i=0 ; i<len ; i++) 
	{
		if (dml.elements[i].name==chkName) 
		{
			dml.elements[i].checked=val;
		}
	}
}

function ValidateForm(chkName)
{	
	dml = document.frmunassign2;
	len = document.frmunassign2.elements.length;
	var i=0;
	for( i=0 ; i<len ; i++)
	{
		if ((dml.elements[i].name==chkName) && (dml.elements[i].checked==1)) 
		{
			dml.submit();
			return true;
		}
	}
	alert("Please select at least one candidate-iway map to be unassigned")
	return false;
}


function captureEnter()
{
	if (window.event.keyCode == 13)
	{
		validate();
	}
}

function setFocus()
{
	document.forms[0].memno.focus();		
}
</script>

</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" <?php if ($_POST['submitted'] != 'Y') print("onLoad='javascript:setFocus()'"); ?> onKeyPress='javascript:captureEnter()'>
<center>
<table width="780" border="0" cellspacing="0" cellpadding="0">
<!--  <tr>
    <td width="780"><img src="images/logo1.jpg" width="136" height="59"><img src="images/logo2.gif" width="284" height="59"><img src="images/logo3.gif" width="360" height="59"></td>
  </tr>  -->
   <tr><td width="780"><?include("includes/header.php");?></td></tr>
  <tr>
    
  </tr>
  <tr>
	<td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></td>	
  </tr>
  <tr>
    <td width="780" background="images/tile.jpg" height="315" valign="top">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="100">&nbsp;</td>
          <td width=500>
            <table width="70%" border="0" cellspacing="0" cellpadding="0" align=center>
              <tr> 
              	<td class="greybluetext10" height="35"  ><br></td>                
              </tr>
              <tr> 
                <td> 
                <form name="unassignfrm" method="post" >				
					<input type=hidden name='submitted' value='Y'>
                    <table cellspacing="0" cellpadding="5" border=0 align=center>
                      <tr>                         
                        <td class="greybluetext10" colspan="2" height="35" bgcolor="#D1E0EF"><b>Unassign Candidate</b></td>
                      </tr>                  
                      <tr>                         
                        <td class="greybluetext10" bgcolor="#E8EFF7" height="40" width="125"><b>Membership No </b></td>
                        <td bgcolor="#E8EFF7" height="35" width="225" class="greybluetext10"> 
                          <input class="textbox" name="memno" type="text" id="memno" value="<?=$membershipNo ?>" onKeyPress="return letternumber(event)" maxlength="20">
                        </td>
                      </tr>                      
                      <tr>                         
                        <td colspan="2" align="center" bgcolor="#D1E0EF" height="35">                
                          <input class='button' id='reset3' type='button' value='View' name="sub_view" onclick='javascript:validate();'> 
                        </td>
                      </tr>
                    </table>                    
                </form>
                </td>
              </tr>
              <tr> 
              	<td class="greybluetext10" height="35"  ><b><?=$errormsg ?></b></td>                
              </tr>
            </table>
          </td>
        </tr>
        <tr><td colspan=2><br><br></td></tr>
        <tr>         
        <td colspan=2>
        	<form name='frmunassign2' method=post>
        	<input type=hidden name=membership_no value='<?=$membershipNo ?>'>
        	<table width="90%" border="1" bordercolor=#7292c5 cellspacing="0" cellpadding="3" align=center>
        	<?php
        	
        	if ($nCentres > 0)
        	{
	        	print("<input type=hidden name='num_centres' value='$nCentres'>");
	        	print("<tr><td width=5% class=greybluetext10 height=25><input type=checkbox name=chkall align=center onClick=\"javascript:SetChecked('chk[]')\"></td>
	        	<td width=15% class=greybluetext10 height=25><b>Exam</b></td>
	        	<td width=15% class=greybluetext10><b>Subject</b></td>
	        	<td width=15% class=greybluetext10><b>Exam Date</b></td>
	        	<td width=10% class=greybluetext10><b>Exam Time</b></td>
	        	<td width=15% class=greybluetext10><b>Centre Code</b></td>
	        	<td width=25% class=greybluetext10><b>Venue</b></td></tr>");
	        	while (list($centreCode, $examCode, $subjectCode, $examDate, $examTime) = mysql_fetch_row($result))
	        	{
		        	$sqlSubject = "SELECT subject_name FROM iib_exam_subjects WHERE online='Y' AND subject_code='$subjectCode' ".
		        						"AND exam_code='$examCode'";
		        	$resSubject = @mysql_query($sqlSubject);
		        	list($subjectName) = @mysql_fetch_row($resSubject);
		        	$sqlExam = "SELECT exam_name FROM iib_exam WHERE online='Y' AND exam_code='$examCode' ";
		        	$resExam = @mysql_query($sqlExam);
		        	list($examName) = @mysql_fetch_row($resExam);
		        	$sqlDetails = "SELECT iway_name, iway_address1, iway_address2, iway_city, iway_state, iway_pin_code ".
										" FROM iib_iway_details WHERE centre_code='$centreCode' ";
					$resDetails = @mysql_query($sqlDetails);
					list($iway_name, $iway_address1, $iway_address2, $iway_city, $iway_state, $iway_pin_code) = mysql_fetch_row($resDetails);	
					$address = "";
                  	if ($iway_name != "")
                  		$address .= $iway_name;
					if ($address != "") 
						$address .= " ";
					if ($iway_address1 != "")
						$address .= $iway_address1;
					if (($address != "") && ($iway_address1 != ""))
						$address .= " ";
					if ($iway_address2 != "")
						$address .= $iway_address2;
					if (($address != "") && ($iway_address2 != ""))
						$address .= " ";
					if ($iway_city != "")
						$address .= $iway_city;
					if (($address != "") && ($iway_city != ""))
						$address .= " ";
					if ($iway_pin_code != "")
						$address .= $iway_pin_code;
					if ($examDate != "")
					{
						$dispDate = explode('-',$examDate);
					}
					if ($examTime != "")
					{
						$dispTime =  explode(':',$examTime);
					}
					$dispDate =  $dispDate[2]."/".$dispDate[1]."/".$dispDate[0];
					$dispTime = $dispTime[0].":".$dispTime[1];
					$chkVal = $examCode."|".$subjectCode."|".$examDate."|".$examTime."|".$centreCode;
					print("<tr>
					<td width=5% class=greybluetext10 height=25><input type=checkbox name=chk[] align=center value='$chkVal'></td>
					<td valign=top class=greybluetext10>");
					
					if ($examName == "")
						print("<br>");	
					else	
						print($examName);	
					print("</td>");
					print("<td valign=top class=greybluetext10>");				
					if ($subjectName == "")
						 print("<br>");
					else
						print($subjectName);
					print("</td>");
					print("<td valign=top class=greybluetext10>");
					if ($dispDate == "")
						print("<br>");
					else
						print($dispDate);
					print("</td>");
					print("<td valign=top class=greybluetext10>");
					if ($dispTime == "")
						print("<br>");
					else
						print($dispTime);
					print("</td>");
					print("<td valign=top class=greybluetext10>");
					if ($centreCode == "")
						print("<br>");
					else
						print($centreCode);
					print("</td>");
					print("<td valign=top class=greybluetext10>");
					if ($address == "")
						print("<br>");															
					else
						print($address);
					print("</td></tr>");		        	
	        	}
    		}
        	?>        	
        	</table>
        </td>
        </tr>
        <tr><td colspan=2><br></td></tr>
        <?php
        	if ($nCentres > 0)
        	{
	    ?>
        <tr><td colspan=2 align=center><input type=button class=button name='sub_un' value='UnAssign' onClick="javascript:ValidateForm('chk[]')"></td></tr>
        <tr><td colspan=2><br></td></tr>
        <?php
    		}
    	?>
        </form>
      </table>
    </td>
  </tr>
  <tr>
    <? include("includes/footer.php");?>
  </tr>
</table>
</center>
</body>
</html>