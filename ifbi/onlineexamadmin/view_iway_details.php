<?php
require_once "sessionchk.php";
require_once "dbconfig.php";

$examCentreCode = $_POST['exam_centre_code'];
$centreCode = $_POST['centre_code'];

//print_r($_POST);

$nDetails = 0;
$nIWay = 0;

if ($examCentreCode != "")
{
	$sqlIWay = "SELECT centre_code, iway_name FROM iib_iway_details WHERE exam_centre_code='$examCentreCode' ";
//	print $sqlIWay;
	$resIWay = @mysql_query($sqlIWay);
	if (mysql_error())
	{
		$err=1;
		$emsg="Error in selecting I Ways.";
		$displaybug='true';	
	}
	$nIWay = mysql_num_rows($resIWay);
}
if (($examCentreCode != "") && ($centreCode != ""))
{
	$sqlDetails = "SELECT no_of_seats, status, iway_address1, iway_address2, iway_city, iway_state, iway_pin_code ".
	" FROM iib_iway_details WHERE exam_centre_code='$examCentreCode' AND centre_code='$centreCode' ";
	$resDetails = @mysql_query($sqlDetails);
	if (mysql_error())
	{
		$err=1;
		$emsg="Error in selecting IWay Details.";
		$displaybug='true';	
	}
	$nDetails = mysql_num_rows($resDetails);
}
$sql="select exam_centre_code, exam_centre_name from iib_exam_centres where online='Y' order by exam_centre_name";
$res=mysql_query($sql);
if (!$res)
{
	$err=1;
	$emsg="Error in selecting exam centres.";
	$displaybug='true';	
}
?>
<HTML>
<HEAD>
<script language='JavaScript' src="./includes/validations.js"></script>
<script language='JavaScript'>
function getCentres()
{
	document.viewCentres.submit();
}
function getIwayDetails()
{
	document.viewCentres.submit();
}
</script>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
</STYLE>
</HEAD>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0 onload='javascript:document.viewCentres.exam_centre_code.focus();' onKeyPress='javascript:submitForm()'>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
<!--        <TR><!-- Topnav Image -->
      <!--  <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
        </TR> -->
		<tr><td width="780"><?include("includes/header.php");?></td></tr>
        <TR>
        
        </TR>
		<tr>
			<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
		</tr>
        <TR>
            <TD background=images/tile.jpg vAlign=top width=780 align=center>
        <form name=viewCentres method="post" onsubmit="return false;">        
                <table width=780 cellpadding=0 cellspacing=5 border=0>
                        <tr>
                                <td colspan=2 align="center"><b><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2>View IWay Details</b></font></td>
                        </tr>                
		                <tr>
                                <td align="right" width=390 class="greybluetext10" valign=top>Select Exam Centre :&nbsp;</td>
                                <td align="left" valign=top>
                                        <select class="textbox" name="exam_centre_code" style={width:250px} onChange='javascript:getCentres()'>
										<option value=''>-Select-</option>
						<?
								while(list($exam_centre_code,$exam_centre_name) = mysql_fetch_row($res))
								{
										$exam_centre_name=stripslashes($exam_centre_name);
										if ($exam_centre_code == $examCentreCode) 
										{
											echo "<option value=\"$exam_centre_code\" selected>$exam_centre_code - $exam_centre_name</option>";											
										}
										else
										{
											echo "<option value=\"$exam_centre_code\">$exam_centre_code - $exam_centre_name</option>";
										}
								}
						?>
										</select>
                                </td>
                        </tr>
						
						<tr>
                        	<td align="right" width=40% class="greybluetext10" valign=top>Select I Way :&nbsp;</td>
                            <td align="left" valign=top>
                            	<select class="textbox" name="centre_code" style={width:250px} onChange='javascript:getIwayDetails()'>
										<option value=''>-Select-</option>
                            <?php
                            if ($nIWay > 0)
                            {
	                            while (list($centre_code, $iway_name) = mysql_fetch_row($resIWay))
	                            {
		                            if ($centre_code == $centreCode) 
									{
										echo "<option value=\"$centre_code\" selected>$centre_code - $iway_name</option>";											
									}
									else
									{
										echo "<option value=\"$centre_code\">$centre_code - $iway_name</option>";
									}
	                            }
                            }
                            ?>
                            </td>
                        </tr>
						<?php
						if ($nDetails > 0)
						{
							print("<tr><td align=center colspan=2 class=greybluetext10>");
							
							print("<table align=center width=60%  cellspacing=0 bordercolor=#7292c5 border=1 cellpadding=2>");
							print("<tr><td class=greybluetext10><b>No. of seats</b></td><td class=greybluetext10><b>Status</b></td><td class=greybluetext10><b>Venue</b></td></tr>");
							while (list($no_of_seats, $status, $iway_address1, $iway_address2, $iway_city, $iway_state, $iway_pin_code) = mysql_fetch_row($resDetails))
							{								
								print("<tr><td class=greybluetext10 width=20%>$no_of_seats</td>");
								print("<td class=greybluetext10 width=20%>");
								switch ($status)
								{
									case 'N' : print("New");
													break;
									case 'A' : print("Activated");
													break;
									case 'D' : print("De-activated");
													break;									
								}
								print("</td>");
								$address = "";
                              	if ($iway_name != "")
                              		$address .= $iway_name;
								if ($address != "") 
									$address .= " ";
								if ($iway_address1 != "")
									$address .= $iway_address1;
								if (($address != "") && ($iway_address1 != ""))
									$address .= " ";
								if ($iway_address2 != "")
									$address .= $iway_address2;
								if (($address != "") && ($iway_address2 != ""))
									$address .= " ";
								if ($iway_city != "")
									$address .= $iway_city;
								if (($address != "") && ($iway_city != ""))
									$address .= " ";
								if ($iway_pin_code != "")
									$address .= $iway_pin_code;
								
								if ($address == "")
									$address = "<br>";		
								print("<td class=greybluetext10>$address</td>");
								print("</tr>");
								
							}
							print("</table>");
							print("</td></tr>");
							
						}
						?>
						<!--<tr>
							<td colspan=2 align=center>
								<input class="button" type="button" name="edit_record" value="Update" onclick="javascript:e_validate();">
								<input class="button" type="button" name="del" value="Delete" onclick="javascript:d_validate();">
								<input class="button" type="button" name="reset" value="Reset" onclick="javascript:reset_val();">
							</td>
						</tr>						-->
						<?php 
							if (isset($err) && $err!='') 
							{
						?>
                        <tr>
                                <td colspan=2 align="center" class='errormsg'><b><?echo $emsg;?></b></td>
                        </tr>
                		<?php
                			}
                		?>
                		<?if (isset($done) && $done!=''){?>
                        <tr>
                                <td colspan=2 align="center" class='alertmsg'><b><?echo $msg;?></b></td>
                        </tr>
                		<?}?>
                                        <input type="hidden" name="submitted">
                                        <input type="hidden" name="edited">
                                        <input type="hidden" name="deleted">
                                        <input type="hidden" name="hid_name" value="<?=$S_NAME;?>">
                </table>
                </form>
                </TD>
        </TR>
        <TR>
                <?include("includes/footer.php");?>
        </TR>
</TABLE>
</FORM>
</center>
</BODY>
</HTML>
<script language='javascript'>
function reset_val(){
document.addExam.tExamName.options[0].selected=true;
document.addExam.E_code.value="";
document.addExam.E_name.value="";
}


function validate()
{
var f=document.addExam;
        if(f.tExamName.value==-1)
        {
                alert('Please Select The Exam Name!');
                f.tExamName.focus();
                return;
        }
        f.submitted.value='true';
        f.submit();
}
function e_validate()
{
var f=document.addExam;

	if(f.tExamName.value==-1)
	{
		alert('Please Select The Exam Code!');	
		f.E_code.focus();
		return;	
	}

	if(f.E_code.value=='')
	{
        alert('Please Enter The Code!');
        f.E_code.focus();
		return;
	}
	
	if(f.E_name.value=='')
	{
        alert('Please Enter The Name!');
        f.E_name.focus();
		return;
	}
	myRegExp = new RegExp("[^a-zA-Z0-9 _/&,:#\)(-]");
	if(f.tExamName.value!=-1)
	{		
		eCode=f.E_code.value;
		myRegExp = new RegExp("[^a-zA-Z0-9 _/&,:#\)(-]");
		result=eCode.match(myRegExp);		
		if(result)
		{
			alert('Allowed Character Alphabet,Numbers,/,\\,#,&,(,),-,:');
			f.E_code.focus();
			return;
		}
		myRegExp = new RegExp("^/.*$");
    	eCode=f.E_code.value;
    	result=eCode.match(myRegExp);
    	if(result)
    	{
    		alert('/ Cannot be the first character!');
    		f.E_code.focus();
        	return;
    	}
    	myRegExp = new RegExp("[^a-zA-Z0-9 _/&,:#\)(-]");
    	eName=f.E_name.value;
		result=eName.match(myRegExp);		
		if(result)
		{
			alert('Allowed Character Alphabet,Numbers,/,\\,#,&,(,),-,:');
			f.E_name.focus();
			return;
		}
		myRegExp = new RegExp("^/.*$");
    	eNAME=f.E_name.value;
    	result=eNAME.match(myRegExp);
    	if(result)
    	{
    		alert('/ Cannot be the first character!');
    		f.E_name.focus();
        	return;
    	}
	}
	else 
		return;

/*
	if(f.hid_name.value==trim(f.E_name.value))
	{
		alert('You Have Not Changed The Exam Name!');
        f.E_name.focus();
		return;
	}
*/
	f.submitted.value='false';
	f.edited.value='true';
	f.submit();

}
function d_validate()
{
var f=document.addExam;
	if(f.tExamName.value!=-1){
	if (confirm("Are you sure - Delete ?")){
		f.submitted.value='false';
		f.deleted.value='true';
//		f.E_code.blur();
		f.E_name.blur();
		f.submit();
		}
	}
	else
	{
		alert('Please Choose A Exam To Delete!');
		f.tExamName.focus();
		return;
	}
}

function submitForm()
{
    if (window.event.keyCode == 13)
    {	    	   
        e_validate();
    }
}
</script>

