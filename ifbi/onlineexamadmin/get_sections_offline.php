<?php
	$admin_flag = 1;
	require("sessionchk.php");
	require("constants.inc");
	require("dbconfig.php");
	$commonDebug = false;	
	set_time_limit(0);
	
?>
<html>
<head>
	<title><?=PAGE_TITLE ?></title>
	<link href="images/iibf.css" rel="stylesheet" type="text/css">
	<script language='JavaScript' src='./includes/validations.js'></script>
	<script language='JavaScript'>
		var astoredmarks = new Array(<?=$nStoredMarks ?>);
		<?php
		for ($i = 0; $i < $nStoredMarks; $i++)
		{			
		?>
			astoredmarks[<?=$i ?>] = <?=$aStoredMarks[$i] ?>;			
		<?php
		}
		?>
		
	    function checknumber(cnt)
	    {
		    v = eval('document.frmselectquestions.question_count'+cnt+'.value');
		    if(!numOnly(v))
		    {
			  	eval('document.frmselectquestions.question_count'+cnt+'.focus()');
			   	alert('Please Enter Numbers Only')
			   	return;
		    }
	    }
	    	    
	    function isValidTotalQP()
	    {
		    v = document.frmselectquestions.total_question_paper.value;
		    if(!numOnly(v))
		    {
			   	document.frmselectquestions.total_question_paper.focus();
			   	alert('Please Enter Numbers Only')
			   	return;
		    }		    
	    }
	    
	    function numOnly(strValue)
	    {
        	var valid = "0123456789 "; 
     		var input = strValue;
    		var length = input.length;
     		for(i=0; i<length; i++)
    	 	{
          		var sub = input.substring(i,i+1);
          		if(valid.indexOf(sub)==-1)
             	{                   
             		return false;          
          		}     
      		}
      		return true;
		}		
		
 		function updateText(section_no, marks)
 		{
	 		var varname;
	 		var j;			
	 		//shift+tab	 	
	 		varname = eval('document.frmselectquestions.question_count'+section_no+marks);
	 		
	 		if (trim(varname.value) == "")
	 		{
		 		varname.value = '0';
	 		}
	 		
	 		var rowqnsvar = eval('document.frmselectquestions.row_questions'+section_no);
	 		var rowmarksvar = eval('document.frmselectquestions.row_marks'+section_no);
	 		var tempqns = 0;
	 		var tempmarks = 0;
	 		var actualmarks = 0;
	 		for (j=0; j < <?=$nStoredMarks ?>; j++)
	 		{		 		
		 		varname = eval('document.frmselectquestions.question_count'+section_no+j);	 		
		 		if (varname.value != "")
		 		{			 		
			 		tempqns = tempqns+parseInt(varname.value, 10);
			 		tempmarks = tempmarks+(parseInt(varname.value, 10))*astoredmarks[j];
		 		}
		 		else
		 		{			 		
			 		tempqns = tempqns+0;
			 		tempmarks = tempmarks+0;
		 		}		 		
	 		}
	 		
	 		if (!isNaN(tempqns))
	 		{
	 			rowqnsvar.value = tempqns;
 			}
 			else
 			{
	 			rowqnsvar.value = 0;
 			}
	 		var rowmarksvar = eval('document.frmselectquestions.row_marks'+section_no);
	 		
	 		if (!isNaN(tempmarks))
	 		{
	 			rowmarksvar.value = tempmarks;
 			}
 			else
 			{
	 			rowmarksvar.value = 0;
 			}
	 		calculateTotal();
 		}
 		
 		function validateText(section_no, marks)
		{				 		
			var varname;	 						 				 		
			var j = 0;
			var sumcountqns = 0;
			var sumcountmarks = 0;
			var i = 0;
			var sec_cnt = document.frmselectquestions.section_count.value;
			i =section_no;
			for (j = 0; j < <?=$nStoredMarks ?>; j++)
			{
				maxcountvar = eval('document.frmselectquestions.max_count'+i+j);
				maxcount = parseInt(maxcountvar.value, 10);				
				givencountvar = eval('document.frmselectquestions.question_count'+i+j);
				givencountvar.value = trim(givencountvar.value);
				givencount = parseInt(givencountvar.value, 10);
				sumcountmarks = parseFloat(sumcountmarks, 10)+parseFloat(givencount*astoredmarks[j], 10);
				sumcountqns = parseInt(sumcountqns, 10)+parseInt(givencount, 10);	
				if (givencount > maxcount)
				{
					if ((maxcount == 0) || (maxcount == ""))
					{
						alert("There are no '"+astoredmarks[j]+" mark' questions available in  this section");
						givencountvar.value = '';
						givencountvar.focus();
						givencountvar.select();
						return false;
					}
					else
					{
						alert("There are only "+maxcount+" '"+astoredmarks[j]+" mark' questions available in  this section");
						givencountvar.value = '';
						givencountvar.focus();
						givencountvar.select();					
						return false;
					}
					break;
				}			
			}
			
			var total_marks = parseFloat(document.frmselectquestions.total_marks.value, 10);

			if (sumcountmarks > total_marks)
			{
				alert('Marks in section '+(parseInt(i)+1)+' exceeds the total marks');
				givencountvar = eval('document.frmselectquestions.question_count'+section_no+marks);
				givencountvar.value = '';
				givencountvar.focus();
				givencountvar.select();
				return false;
			}
			var rowmarksvar = eval('document.frmselectquestions.row_marks'+i);
			if (!isNaN(sumcountmarks))
			{
				rowmarksvar.value = sumcountmarks;			
			}
			else
			{
				rowmarksvar.value = '';		
			}
			var rowqnsvar = eval('document.frmselectquestions.row_questions'+i);
			if (!isNaN(sumcountqns))
			{
				rowqnsvar.value = sumcountqns;			
			}
			else
			{
				rowqnsvar.value = '';		
			}		
			calculateTotal();
			calc_total_marks = parseFloat(document.frmselectquestions.sum_marks.value, 10);
			
			if (calc_total_marks > total_marks)
			{
				alert('Total marks calculated exceeds total marks given ');
				givencountvar = eval('document.frmselectquestions.question_count'+section_no+marks);
				
				givencountvar.value = '';
				givencountvar.focus();
				givencountvar.select();
				return false;
			}
			return true;
		}	
			
		function calculateTotal()
		{
			var sumcountqns = 0;
			var sumcountmarks = 0;
			var sumqns = 0;
			var summarks = 0;
			var sec_cnt = document.frmselectquestions.section_count.value;
			
			for (var i=0; i <sec_cnt; i++)
			{
				sumqnsvar = eval('document.frmselectquestions.row_questions'+i);
				sumcountqns = parseInt(sumcountqns, 10)+parseInt(sumqnsvar.value, 10);				
				summarksvar = eval('document.frmselectquestions.row_marks'+i);
				sumcountmarks = parseFloat(sumcountmarks, 10)+parseFloat(summarksvar.value, 10);
			}
			
			if (!isNaN(sumcountqns))
			{
				document.frmselectquestions.sum_qns.value=sumcountqns;
			}
			else
			{
				document.frmselectquestions.sum_qns.value="";
			}
			if (!isNaN(sumcountmarks))
			{
				document.frmselectquestions.sum_marks.value=sumcountmarks;
			}
			else
			{
				document.frmselectquestions.sum_marks.value="";
			}
		}
		
		function validateForm()
		{			
			
			document.frmselectquestions.total_question_paper.value = trim(document.frmselectquestions.total_question_paper.value);
			document.frmselectquestions.total_marks.value = trim(document.frmselectquestions.total_marks.value);
			
			var sec_cnt = document.frmselectquestions.section_count.value;
			var total_marks = document.frmselectquestions.total_marks.value;
			var total_question_paper = document.frmselectquestions.total_question_paper.value;		
						
			if (!checknull(document.frmselectquestions.total_question_paper))
			{
				alert('No. of question papers cannot be empty');
				document.frmselectquestions.total_question_paper.focus();
				return;				
			}
			if (!isNumber(document.frmselectquestions.total_question_paper))
			{
				alert('No. of question papers must be a number');
				document.frmselectquestions.total_question_paper.focus();
				return;
			}
			if(!numOnly(document.frmselectquestions.total_question_paper.value))
		    {			    
			    alert('No. of question papers must be a number');
			    document.frmselectquestions.total_question_paper.focus();
			    return;
		    }		
			if (total_question_paper <= 0)
			{
				alert('No.of question papers must be greater than 0');
				document.frmselectquestions.total_question_paper.focus();
				return;
			}
			var existing_sum = trim(document.frmselectquestions.existing_total_questions.value);			
			var maxcount = '';
			var givencount = '';
			var maxcountvar = '';
			var givencountvar = '';
			var sumcount = 0;
			var totalqns = 0;
			var sectionmarks = 0;
			for (var i = 0; i < sec_cnt; i++)
			{
				sectionmarks = 0;
				for (var j = 0; j < <?=$nStoredMarks ?>; j++)
				{
					maxcountvar = eval('document.frmselectquestions.max_count'+i+j);
					maxcount = parseInt(maxcountvar.value, 10);
					givencountvar = eval('document.frmselectquestions.question_count'+i+j);
					givencountvar.value = trim(givencountvar.value);
					givencount = parseInt(givencountvar.value, 10);
					sumcount = parseFloat(sumcount, 10)+parseFloat(givencount*astoredmarks[j], 10);
					totalqns = parseInt(totalqns, 10)+parseInt(givencount, 10);
					if (!checknull(givencountvar))
					{						
						givencountvar.value = 0;
					}
					if (!isNumber(givencountvar))
					{
						alert('No. of questions must be a number');
						givencountvar.focus();
						return;
					}
					if(!numOnly(givencount))
		    		{			    
			    		alert('No. of questions must be a number');
			    		givencountvar.focus();
			    		return;
		    		}		
					if (givencount < 0)
					{
						alert("No. of questions cannot be less than 0");
						givencountvar.focus();
						return;
					}
					if ((givencount*astoredmarks[j]) > total_marks)
					{
						alert('Marks in section '+(parseInt(i)+1)+' exceeds the total marks');
						givencountvar.focus();
						return;
					}
					sectionmarks = parseFloat(sectionmarks, 10)+(givencount*astoredmarks[j]);
					
					if (givencount > maxcount)
					{
						if ((maxcount == 0) || (maxcount == ""))
						{
							alert("There are no '"+astoredmarks[j]+" mark' questions available in  this section");
						}
						else
						{
							alert("There are only "+maxcount+" '"+astoredmarks[j]+" mark' questions available in  this section");
						}
						givencountvar.focus();
						return;
					}
				}
				
				if (sectionmarks > total_marks)
				{
					alert('Marks in section '+(parseInt(i)+1)+' exceeds the total marks');
					givencountvar.focus();
					return;
				}
			}
			
			if (isNaN(sumcount))
			{
				sumcount = 0;
			}
			if (sumcount != total_marks)
			{
				alert('Total marks ['+sumcount+'] calculated must be equal to Total marks given ['+total_marks+']');				
				return;
			}
			if ((existing_sum != "") && (existing_sum != totalqns))
			{
				alert('Total questions ['+totalqns+'] calculated does not tally with the total questions \n\t in the existing question papers ['+existing_sum+'] for the subject.\n Either delete the existing question papers or change \n\t the selected weightage scheme.\n');				
				return;
			}
			document.frmselectquestions.submit();
		}
		
		function setFocus(ctrlname)
		{		
			ctrl = eval('document.frmselectquestions.'+ctrlname);			
			ctrl.select();			
		}
		
		function setFirstFocus()
		{			
			document.frmselectquestions.question_count00.focus();			
		}
	</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<?php
				
		$examCode = $_REQUEST['exam_code'];
		$subjectCode = $_REQUEST['subject_code'];
		$mediumCode = $_POST['medium_code'];
		$sql = "SELECT exam_name FROM iib_exam WHERE exam_code='$examCode'";
		if ($commonDebug)
		{
			print "query :".$sql;
		}
		$result = @mysql_query($sql) or die("Query failed");
		list($examName) = mysql_fetch_array($result);
	
		$sql = "SELECT subject_name, total_marks, pass_mark FROM iib_exam_subjects WHERE subject_code='$subjectCode'";
		if ($commonDebug)
		{
			print "query :".$sql;
		}
		$result = @mysql_query($sql) or die("Query failed");
		list($subjectName, $totalMarks, $passMark) = mysql_fetch_array($result);
		
		// get existing QP count
		$sqlCount = "SELECT COUNT(question_paper_no) FROM iib_question_paper WHERE exam_code='$examCode' ".
			" AND subject_code='$subjectCode' AND online='N' ";
		if ($commonDebug)
		{
			print "query :".$sqlCount;
		}
		$resCount = @mysql_query($sqlCount) or die("Query on iib_question_paper failed");
		list($existingCount) = mysql_fetch_row($resCount);
		if ($existingCount == "")
		{
			$existingCount = 0;
		}
		// get total questions			
		$sqlSum = "SELECT SUM(no_of_questions) FROM iib_qp_weightage WHERE exam_code='$examCode' ".
			" AND subject_code='$subjectCode' group by question_paper_no LIMIT 1";
		if ($commonDebug)
		{
			print "query :".$sqlSum;
		}
		$resSum = @mysql_query($sqlSum) or die("Query on iib_qp_weightage failed");
		if (mysql_num_rows($resSum) > 0)
		{
			list($existingSum) = mysql_fetch_row($resSum);
		}
		else
		{
			$existingSum = "";
		}
		$sql = "SELECT section_code, section_name FROM iib_subject_sections WHERE ".
			"subject_code='$subjectCode' and online='Y' ORDER BY section_code ";
		if ($commonDebug)
		{
			print "query :".$sql;
		}
		$result = @mysql_query($sql) or die("Query failed");
		$nRows = mysql_num_rows($result);
		if ($nRows == 0)
		{					
?>
<body leftmargin="0" topmargin="0" >
<center>
<table width="780" border="0" cellspacing="0" cellpadding="0">
<!--  <tr> 
    <td width="780"><img src="images/logo1.jpg" width="136" height="59"><img src="images/logo2.gif" width="284" height="59"><img src="images/logo3.gif" width="360" height="59"></td>
  </tr> -->
  <tr> 
        <? include("includes/header.php"); ?>
  </tr>
  <tr>
		<Td background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
  </tr>
  <tr> 
    <td width="780" background="images/tile.jpg" valign="top"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td height="34"> 
            <div align="center"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Admin 
              module - Offline Question Paper(s) Generation</font> </strong></div></td>
        </tr>
        <tr> 
          <td> 
            <div align="center"> <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
			<?php					
					print("<br><br><center><div class='alertmsg'>The subject ($examName, $subjectName) has no sections".
							"<br></div></center><br>");										
			?>
			</div>
		  </td>
	  </tr>
	</td>
  </tr>
  <tr> 
    <?// include("includes/footer.php");?>
  </tr>
</table>
</center>
</body>
</html>
<?					
		}
		else
		{
?>					
<body leftmargin="0" topmargin="0" onLoad='document.frmselectquestions.question_count00.focus()' onKeyPress="return number(event)">
<center>
<table width="780" border="0" cellspacing="0" cellpadding="0">
<!--  <tr> 
    <td width="780"><img src="images/logo1.jpg" width="136" height="59"><img src="images/logo2.gif" width="284" height="59"><img src="images/logo3.gif" width="360" height="59"></td>
  </tr> -->
  <tr> 
    <? include("includes/header.php");?>
  </tr>
  <tr>
		<Td background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
  </tr>
  <tr> 
    <td width="780" background="images/tile.jpg" valign="top"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td height="34"> 
            <div align="center"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Admin 
              module - Offline Question Paper(s) Generation</font> </strong></div></td>
        </tr>
        <tr> 
          <td> 
            <div align="center"> <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
              <?php
				print_r($POST);
					$aSectionCode = array();
					$aSectionName = array();
					while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
					{
						$aSectionCode[] = $row['section_code'];
						$aSectionName[] = $row['section_name'];
					}
					
					$nSections = count($aSectionCode);
					$qa = array();
					
					for ($j = 0; $j < $nStoredMarks; $j++)
					{
						$nSetMark = $aStoredMarks[$j];
						$sqlCount =  " SELECT count(1) as cnt FROM iib_section_questions isq, iib_subject_sections iss ".
											" WHERE isq.exam_code='$examCode' AND isq.subject_code='$subjectCode'  ".
											" AND isq.marks='". $nSetMark. "' and isq.online='Y' AND isq.section_code=iss.section_code ".
											" AND isq.subject_code=iss.subject_code ";
						if ($commonDebug)
						{
							print "query : ".$sqlCount;
						}
						$resCount = @mysql_query($sqlCount) or die("Query failed");
						list($cnt) = mysql_fetch_row($resCount);
						$qa[$j] = $cnt;
					}	
					
					print("<form name='frmselectquestions' method=post action=generate_papers_offline.php>\n".
					"<input type=hidden name=exam_code value='$examCode'>\n".
					"<input type=hidden name=subject_code value='$subjectCode'>\n".
					"<input type=hidden name=medium_code value='$mediumCode'>\n".
					"<input type=hidden name=section_count value='$nSections'>\n");
					print("<table width=100%>\n");
					print("<tr><td class='greybluetext10'><b>Exam : $examName</b></td>".
					"<td colspan=2 class='greybluetext10'><b>Subject : $subjectName</b></td></tr>");
					print("<tr><td class='greybluetext10'><b>Total Marks : $totalMarks</b></td>".
					"<input type=hidden name=total_marks value='$totalMarks'>".
					"<td class='greybluetext10'><b>Pass Mark : $passMark</b></td>".
					"<input type=hidden name=pass_mark value='$passMark'>".		
					"<td class='greybluetext10'><b>Question Papers already Generated : $existingCount </b></td></tr>");
					if ($existingSum != "")
					{
						print("<tr><td colspan=3 class='greybluetext10'><b>No. of Questions required for this Question Paper are - $existingSum</b></td></tr>");
					}
					print("<tr><td class='greybluetext10'>&nbsp;</td></tr></table>");
					print("<table width=100% cellspacing=2 cellpadding=2>");
					print("<tr><td class='greybluetext10' width=20%><b>Section</b></td>");
					$wid = round(60/$nStoredMarks);
					for ($a = 0; $a < $nStoredMarks; $a++)
					{
						print("<td class='greybluetext10' width=".$wid."% ><b>{$aStoredMarks[$a]} Mark(s)</b></td>");
					}					
					print("<td class='greybluetext10' width=10%><b>Total Questions</b></td>".
							"<td class='greybluetext10' width=10%><b>Total Marks</b></td>".
							"</tr>\n");
					print("<tr><td class='greybluetext10'><br></td>");
					for ($a = 0; $a < $nStoredMarks; $a++)
					{
						print("<td class='greybluetext10'><b>[Questions Available-".$qa[$a]."]</b></td>");
					}
					print("</tr>\n");
					$sumTotalQns = 0;
					$sumTotalMarks = 0;
					$tabIndex = 1;
					for ($i = 0; $i < $nSections; $i++)
					{	
						$sel = " SELECT marks, count(1) as cnt FROM iib_section_questions ".
									" WHERE exam_code='$examCode' AND subject_code='$subjectCode' ".
									" AND section_code='". $aSectionCode[$i]. "' and online='Y' ".
									" GROUP BY marks";
						if ($commonDebug)
						{
							print "query :".$sel;
						}
						$result = @mysql_query($sel) or die("Query failed");
						$totalQuestions = array();
						while (list($marks, $cnt) = mysql_fetch_array($result)) 
						{
							if ($commonDebug)
							{
								print("<br>".$marks.":".$cnt);
							}
							$key = array_search($marks, $aStoredMarks);
							$totalQuestions[$key] = $cnt;
						}
						$sumQns = 0;
						$sumMarks = 0;
						print("<tr><td class='greybluetext10'>{$aSectionName[$i]}</td>\n");
						for ($j = 0; $j < $nStoredMarks; $j++)
						{							
							$nSetMark = $aStoredMarks[$j];
							($totalQuestions[$j] == "") ? $totalQuestions[$j] = 0 : $totalQuestions[$j] = $totalQuestions[$j];
							print("<td class='greybluetext10'>".	
								"<input type=text class=textbox maxlength=5 size=7  ".						
								"onChange=\"javascript:return validateText($i, $j)\"  ".
								"onBlur=\"javascript:return updateText($i, $j)\"  ".								
								"name='question_count".$i.$j."' value='' tabindex=\"$tabIndex\"> [".$totalQuestions[$j]."]</td>\n");
							print("<input type=hidden name='max_count".$i.$j."' value='".$totalQuestions[$j]."'>");
							$sumQns += $totalQuestions[$j];	
							$sumMarks += ($totalQuestions[$j]*$nSetMark);		
							$tabIndex++;					
						}
						$sumTotalQns += $sumQns;
						$sumTotalMarks += $sumMarks;
						print("\n<td><input type=text readonly tabindex=2000 onChange='javascript:calculateTotal()' class=textbox name='row_questions".$i."' value='' size=15></td>");
						print("\n<td><input type=text readonly tabindex=2000 onChange='javascript:calculateTotal()' class=textbox name='row_marks".$i."' value='' size=15></td>");
						print("</tr>\n");						  
					
					} // end of for
					print("<tr><td colspan=".($nStoredMarks+1)." align='center'><br></td>".									
					"<td class='greybluetext10' >".
					"<input type=text readonly class=textbox name='sum_qns' value='' size=15 tabindex=2000></td>".
					"<td class='greybluetext10' >".
					"<input type=text readonly class=textbox name='sum_marks' value='' size=15 tabindex=2000></td>".
					"</tr>");
					$nextIndex = $tabIndex+1;
					print("<tr><td>&nbsp;</td></tr><tr>".
					"<td class='greybluetext10' colspan=3><b>No. of question papers : </b>".
					"<input type=text class='textbox' name=total_question_paper value=1 maxlength=5 onKeyPress=\"return number(event)\" tabindex=\"$tabIndex\"></td>".
					"<td colspan=2 align='center'>".
					"<input type=button class=button name='sub' value='Generate Question Paper(s)' onClick='validateForm()'  tabindex=\"$nextIndex\" ".
					"onBlur=\"javascript:setFirstFocus()\"></td>".
					"</tr>");
					print("</table>");
					print("\n<input type=hidden name='existing_total_questions' value='$existingSum'>\n");
					print("</form>");
				} // end of else
			
				/* Free resultset */
				mysql_free_result($result);
			
			?>
              </font> </div></td>
        </tr>
      </table></td>
  </tr>
 <!-- <tr> 
    <? include("includes/footer.php");?>
  </tr> -->
</table>
 <table width="100%"  border=0 bgcolor="004a80">
  <tr height="10" width="100%">
       <td width="100%">  <? include("includes/footer.php");?> </td>
 	</tr>
</table>
</center>
</body>
</html>