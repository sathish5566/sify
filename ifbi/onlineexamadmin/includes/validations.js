function trim(value) {
   var temp = value;
   var obj = /^(\s*)([\W\w]*)(\b\s*$)/;
   if (obj.test(temp)) { temp = temp.replace(obj, '$2'); }
   var obj = / +/g;
   temp = temp.replace(obj, " ");
   if (temp == " ") { temp = ""; }
   return temp;
}

function checknull(textstr){	
  if(trim(textstr.value) == ""){
	  textstr.focus();
  	  return false
	}
	return true;
}
function checkurl(url){
	
	var re = /http:\/\/\w+(\.\w{2,})/gi;	
	if ( url.match(re) ) {
		return true;
	}
	return false;
}

function letternumber(e)
{
	var key;
	var keychar;
	if (window.event)
 		key = window.event.keyCode;
	else if (e)
 		key = e.which;
	else
 		return true;
	keychar = String.fromCharCode(key);
	keychar = keychar.toLowerCase();
	// control keys
	if ((key==null) || (key==0) || (key==8) || 
 	(key==9) || (key==13) || (key==27) )
 		return true;
	// alphas and numbers
	else if ((("abcdefghijklmnopqrstuvwxyz0123456789").indexOf(keychar) > -1))
 		return true;
	else
 		return false;
}

function number(e)
{
	var key;
	var keychar;
	if (window.event)
 		key = window.event.keyCode;
	else if (e)
 		key = e.which;
	else
 		return true;
	keychar = String.fromCharCode(key);
	keychar = keychar.toLowerCase();
	// control keys
	if ((key==null) || (key==0) || (key==8) || 
 	(key==9) || (key==13) || (key==27) )
 		return true;
	// alphas and numbers
	else if ((("0123456789").indexOf(keychar) > -1))
 		return true;
	else
 		return false;
}

function checkstring(textstr){
	var chksp = 0;
	
	var checkOK = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz. ";
		var checkStr = textstr.value;
		for (i = 0;  i < checkStr.length;  i++){
			ch = checkStr.charAt(i);
			if (ch == " ")
				chksp++;
			for (j = 0;  j < checkOK.length;  j++)
				if (ch == checkOK.charAt(j))
					break;
			if ((j == checkOK.length) || (chksp==checkStr.length)){
				textstr.focus();
				return false;
			}
		}
		return true;
}
function alphabetcheck(textstr){
	var chksp = 0;
	
	var checkOK = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz. ";
		var checkStr = textstr.value;
		for (i = 0;  i < checkStr.length;  i++){
			ch = checkStr.charAt(i);
			if (ch == " ")
				chksp++;
			for (j = 0;  j < checkOK.length;  j++)
				if (ch == checkOK.charAt(j))
					break;
			if ((j == checkOK.length) || (chksp==checkStr.length)){
				textstr.focus();
				return false;
			}
		}
		return true;
}



	function FormatTextArea(sTarget,iMax)
	{

		//var sTarget = document.getElementById(sTarget);
		var iLen = sTarget.value.length;
		if(iLen > parseInt(iMax)) 
		{
			return false;
		}
		return true;
		//sTarget.value=sTarget.value.substring(0,parseInt(iMax));
	}

function onlyalphabetcheck(textstr){
	var chksp = 0;
	
	var checkOK = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz& ";
		var checkStr = textstr.value;
		for (i = 0;  i < checkStr.length;  i++){
			ch = checkStr.charAt(i);
			if (ch == " ")
				chksp++;
			for (j = 0;  j < checkOK.length;  j++)
				if (ch == checkOK.charAt(j))
					break;
			if ((j == checkOK.length) || (chksp==checkStr.length)){
				textstr.focus();
				return false;
			}
		}
		return true;
}

function onlyalphabetchecknospace(textstr){
	var chksp = 0;
	
	var checkOK = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		var checkStr = textstr.value;
		for (i = 0;  i < checkStr.length;  i++){
			ch = checkStr.charAt(i);
			if (ch == " ")
				chksp++;
			for (j = 0;  j < checkOK.length;  j++)
				if (ch == checkOK.charAt(j))
					break;
			if ((j == checkOK.length) || (chksp==checkStr.length)){
				textstr.focus();
				return false;
			}
		}
		return true;
}
		
function isValidMailID(textmail){
	var i;
	var str;
	str = textmail.value;
	var filter=/^[a-zA-Z][a-zA-Z0-9_.]+[@]{1}[a-zA-Z0-9]+[.][a-zA-Z.]+$/
	if (filter.test(str))
		return true;
	else{
		textmail.focus();
		return false;
	}
	return true;
}

function isValidEmail(str){
	var i;
	
	var filter=/^.+@.+\..{2,3}$/
	if (filter.test(str))
		return true;
	else{	
		return false;
	}
	return true;
}

function isNumber(textnum){
	
	if (isNaN(textnum.value)){
  		textnum.focus();
  		return false;
  	}
	return true;
}

function isAlphaNumSpecial(frminput,frmlbl)
{
	val = trim(frminput.value);
	if (val != "")
	{
		myRegExp = new RegExp("[^a-zA-Z0-9  _/&,:#\)(-]");
    
    	result=val.match(myRegExp);
    	if (result)
    	{
       		alert('Allowed Characters for '+frmlbl+' are alphanumeric,/,\\,#,&,(,),-,:');
       		frminput.focus();			
       		return false;
   		}
	}
	return true;
}

function checkForCurrentDate(dd,mm,yyyy)
{
	
	var months=new Array(13);
	months[1]="January";
	months[2]="February";
	months[3]="March";
	months[4]="April";
	months[5]="May";
	months[6]="June";
	months[7]="July";
	months[8]="August";
	months[9]="September";
	months[10]="October";
	months[11]="November";
	months[12]="December";
	
	var this_date=new Date();
	mon = months[mm];
	var my_date=new Date(mon+" "+dd+", "+yyyy);
	
	if (this_date > my_date)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function rangeDateCheck(sdd,smm,syyyy,ddd,dmm,dyyyy)
{
	
	
	var months=new Array(13);
	months[1]="January";
	months[2]="February";
	months[3]="March";
	months[4]="April";
	months[5]="May";
	months[6]="June";
	months[7]="July";
	months[8]="August";
	months[9]="September";
	months[10]="October";
	months[11]="November";
	months[12]="December";
	
	//var this_date=new Date();
	//alert(smm);
	if (smm<10)
	{
		//alert("inside");
		smm=10+parseInt(smm)-10;
	}
	//alert(smm);
	smon = months[smm];
	//alert(smon);
	var my_date=new Date(smon+" "+sdd+", "+syyyy);
	dmon = months[dmm];
	
	//alert(dmon);
	var this_date=new Date(dmon+" "+ddd+", "+dyyyy);
	if (this_date > my_date)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkFormValidation (objForm) {
	
	var result;
	var attrValid;
	for (var i=0; i<objForm.elements.length; i++) {
		attrValid = objForm.elements[i].getAttribute("valid");
		if (attrValid) {
			var regExp = new RegExp("^(m|n)_(d|i|a|s|o|f|e|u)","g");
			result = regExp.exec(attrValid);
			if (result[1] == "m" && !checkFormMantatory(objForm.elements[i]) )  return false;
			if (trim(objForm.elements[i].value) != "") {
				
				switch (result[2]) {
					case "d":
						if ( !checkValidDate(objForm.elements[i],true) ) return false;
						break;
					case "i":
						if ( !checkFormInteger(objForm.elements[i],true) ) return false;
						break;
					case "a":
						if ( !checkFormAlphaNumeric(objForm.elements[i],true )) return false;
						break;
					case "s":
						if ( !checkFormSpecialChar(objForm.elements[i],true) ) return false;
						break;
					case "o":
						if ( !checkOptionForm(objForm.elements[i],true) ) return false;
						break;
					case "f":
						if ( !checkFormImageFile(objForm.elements[i],true) ) return false;
						break;					
					case "e":
						if ( !checkFormEmail(objForm.elements[i],true) ) return false;
						break;						
					case "u":
						break;						

				} //  Switch end
				
			}// Non mantatory but characters are there !!! checking
			
		} // valid attribute check
	} // for llop
	return true;
}

function checkFormEmail (objElement) {

		var argv = arguments;
	var argc = arguments.length;
	var isAlert = (argc > 1 ) ? argv[1] : false;
	var re=/^.+@.+\..{2,3}$/;
	var strInputDate = objElement.value;
	if ( strInputDate.match(re) ) {
		return true;
	}else {
		if ( isAlert ) {
			alert( objElement.getAttribute("errname")+" is not valid a Valid EMail");
			objElement.focus();
		}
		return false;
	}
	return true;
}

function checkFormMantatory (objElement) {
	
	if ( trim(objElement.value) == "" ) {
		alert ("Please enter the "+objElement.getAttribute("errname"));
		objElement.focus();
		return false;
	}
	return true;
}

function checkOptionForm(objElement) {
	
	if ( trim(objElement.value) == "" ) {
		alert ("Please select the "+objElement.getAttribute("errname"));
		objElement.focus();
		return false;
	}
	return true;
}

function checkRadioForm(objForm,errname) {

	var formLength = objForm.elements.length;
	var checked = 0;
	var checkboxLength = 0;
	for (var i=0;i<formLength;i++) {
	  if ( objForm.elements[i].type.match(/^(radio)/gi) ) {
		  if(objForm.elements[i].checked == true){
			 		checked ++;
			}
	  }
  }
	if (checked == 0 ){
		alert("Please select the "+errname+" Option!");
		return false;
	}	
	return true;
}


function checkValidDate (objElement) {
	var argv = arguments;
	var argc = arguments.length;
	var isAlert = (argc > 1 ) ? argv[1] : false;
	var re=/^((29-0?2-\d{2}([13579][26]|[02468][048]))|((0?[1-9]|[1-2]\d|3[0-1])-(0?[13578]|10|12)-\d{4})|((0?[1-9]|[1-2]\d|30)-(0?[469]|11)-\d{4})|((0?[1-9]|1\d|2[0-8])-0?2-\d{4}))/g;
	var strInputDate = objElement.value.replace(/\//g,"-");
	if ( strInputDate.match(re) ) {
		return true;
	}else {
		if ( isAlert ) {
			alert( objElement.getAttribute("errname")+" is not valid");
			objElement.focus();
		}
		return false;
	}
	return true;
}


/*This function change date to long*/
function getLongDate(dd,mm,yy){
	dd=dd.toString()
	mm=mm.toString()
	yy=yy.toString()
	return parseInt(yy+mm+dd)
}

/*	Extract date from given form date */
function getFormDate (date) {
	var re = /^(0?\d{1,2})[\/-]0?\d{1,2}[\/-]\d{4}/g
	var arr= re.exec(date);
	var ret = false;
	for (i in arr) {
		if ( i==1) ret = true;
	}
	if (ret) return checkDateLength( arr[1] );
	return 0;
}

/*	Extract month from given form date */
function getFormMonth (date) {
	var re = /^0?\d{1,2}[\/-](0?\d{1,2})[\/-]\d{4}/g
	var arr= re.exec(date);
	var ret = false;
	for (i in arr) {
		if ( i==1) ret = true;
	}
	if (ret) return checkDateLength( arr[1] );
	return 0;
}

/*	Extract year from given form date */

function getFormYear (date) {
	var re = /^0?\d{1,2}[\/-]0?\d{1,2}[\/-](\d{4})/g
	var arr= re.exec(date);
	var ret = false;
	for (i in arr) {
		if ( i==1) ret = true;
	}
	if (ret) return arr[1];
	return 0;
}
 
function compareDate (date1,date2) {
	var date1 = getLongDate ( getFormDate(date1),getFormMonth(date1),getFormYear(date1));
	var date2 = getLongDate ( getFormDate(date2),getFormMonth(date2),getFormYear(date2));
	if (date1 == date2) return 0;
	if (date1 > date2) return 1;
	if (date1 < date2) return -1;
}

function checkDateLength (date) {
	var strDate = date.toString();
	if ( strDate.length == 1) strDate = "0"+strDate;
	return strDate;
}

/*	Check special characters */

function checkFormInteger (objElement) {
		var argv = arguments;
		var argc = arguments.length;
		var isAlert = ( argc > 1) ? argv[1] : false;
		var regExp =/^[1-9]\d*/gi
		var result =  new String(objElement.value.match(regExp));
		if ( result == objElement.value) return true;
		if ( isAlert ) {
			alert (objElement.getAttribute("errname")+" accepts only integer values");
		}
		objElement.focus();
		return false;
	}

function checkFormAlphaNumeric (objElement) {
	var argv = arguments;
	var argc = arguments.length;
	var isAlert = (argc > 1 ) ? argv[1] : false;
	var regExp = /^[a-zA-Z0-9_\s]*/gi;
	var result = new String(objElement.value.match(regExp));
	if ( result == objElement.value) return true;
	if ( isAlert ) {
		alert (objElement.getAttribute("errname") + " accepts only alpha numeric values");
	}
	objElement.focus();
	return false;
}

function checkFormSpecialChar (objElement) {
	var argv = arguments;
	var argc = arguments.length;
	var isAlert = (argc > 1 ) ? argv[1] : false;
	var regExp = /^[\w\W]*/gi;
	var result = new String(objElement.value.match(regExp));
	if ( result == objElement.value) return true;
	if ( isAlert ) {
		alert (objElement.getAttribute("errname")+"  accepts only alphanumeric, &, - and spaces");
	}
	objElement.focus();
	return false;
}

function checkFormImageFile (objElement) {
	var argv = arguments;
	var argc = arguments.length;
	var isAlert = (argc > 1 ) ? argv[1] : false;

	//var regExp = /\.(jpg|jpeg|gif)/gi
	var regExp = /\.(xls)/gi
	if (!objElement.value.match(regExp)){
		alert (objElement.getAttribute("errname")+"  accepts only .xls File Formats.");
		objElement.focus();
		return false;
	}
	return true;

}

function checkImageFile (strInput) {
	var regExp = /\.(jpg|jpeg|gif)/gi
	if ( strInput.match(regExp) ) {
		return true;
	}
	return false;
}

function removeOptions (objElement, removeIndex) {
	var totLength = objElement.options.length;
	for ( var i=0;i<totLength;i++) {
		objElement.remove(removeIndex);
	}
}

//	Reset form values for the given form

function resetFormValues (objForm) {
	for (var i=0;i<objForm.elements.length;i++) {
       if ( objForm.elements[i].type.match(/^(text)/gi) ) {
	       objForm.elements[i].value = "";
	   }
	   if ( objForm.elements[i].type.match(/^(select)/gi) ) {
	       objForm.elements[i].selectedIndex = 0;
	   }
	   if ( objForm.elements[i].type.match(/^(checkbox|radio)/gi) ) {
		   objForm.elements[i].checked = false;
	   }
	   
   }
}

function blockImages () {
	var objImages = document.images;
	for ( var i=0;i<objImages.length;i++) {
		if ( !checkImageFile(objImages[i].src) ) {
			objImages[i].style.display = "none";
		}
	}
}

function checkall_click(formid){
	var objForm = document.forms[formid];
	for (var i=0;i<objForm.elements.length;i++) {
	   if ( objForm.elements[i].type.match(/^(checkbox)/gi) ) {
		   objForm.elements[i].checked = true;
	   }	   
   }
}

function clearall_click(formid){
	var objForm = document.forms[formid];
	for (var i=0;i<objForm.elements.length;i++) {
	   if ( objForm.elements[i].type.match(/^(checkbox)/gi) ) {
		   objForm.elements[i].checked = false;
	   }	   
   }
}

function delete_click(formid){
	var objForm = document.forms[formid];
	var formLength = objForm.elements.length;
	var checked = 0;
	var checkboxLength = 0;
	for (var i=0;i<formLength;i++) {
	  if ( objForm.elements[i].type.match(/^(checkbox)/gi) ) {
			checkboxLength++;
		  if(objForm.elements[i].checked == true){
			 		checked ++;
			}
	  }	   
  }
	
	if (checkboxLength == 0) {	
		return false;	
	}
	
	if (checked > 0 ){
		if(confirm("Are you sure want to delete the selected file(s)?")){
			objForm.submit();
		}
		return false;
	}else{
		alert("Atleast one File need to be Selected!");
		return false;
	}
	
}


// Function added to limit the type of Files to be uploaded.
extArray = new Array(".xls");
 		function LimitAttach(form, file) {
 			allowSubmit = false;
 			if (!file) return;
 			while (file.indexOf("\\") != -1)
 			file = file.slice(file.indexOf("\\") + 1);
 			ext = file.slice(file.indexOf(".")).toLowerCase();
 			for (var i = 0; i < extArray.length; i++)
 			{
 				if (extArray[i] == ext) { allowSubmit = true; break; }
 			}
 			if (allowSubmit)
 			{
 				return true;
 			}
 			else
 			{
 			alert("Upload files that end in type :  "
 			+ (extArray.join("  ")) + "\nPlease select new"
 			+ " file to upload.");
 			return false;
 			}
 		}