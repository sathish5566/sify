<?
	require_once('includes/gen_html/conf/settings.conf');
	require_once("includes/DBConfig.php");
	require_once('includes/user_contents.inc');
	require_once('includes/cms_list_events.inc');
	require_once('includes/cms_list_language.inc');
	require_once('includes/cms_list_editorial.inc');
	require_once('includes/gen_html/show_combo.inc');
	require_once('includes/gen_html/show_form_control.inc');
	require_once('includes/gen_html/show_multicombo.inc');
	require_once('includes/gen_html/conf/cms_srs.conf');
	require_once('includes/content_type_functions.inc');
	require_once('date/date.inc');
	require_once('includes/topnav.inc');
	require_once('includes/UserAccess.inc');
	require_once('includes/NavigateWorkFlowState.inc');
	require_once('includes/cmslogger.php');
	require_once('includes/GroupFieldAccess.inc');

$submitted = $_POST['submitted'];
$name='purpose';
//	$name = $_GET['name'];
	if($submitted)
	{
		$img=$_FILES['img_upload']['name'];
        if ($img== "")
        {
			echo "Select a Filename";
	 }
	 else
	 {
        $uploaddir='/projects/www/cms/srs_images/';
		$unique = new UniqNumber();
		$id = (int)$unique->getNumber("IMG");
		
		if (($_FILES['img_upload']['type'] == 'image/pjpeg')||($_FILES['img_upload']['type'] == 'image/gif')||($_FILES['img_upload']['type'] == 'image/bmp'))
		{
			$imagefile = $id.'_'.$_FILES['img_upload']['name'];
       		$uploadfile=$uploaddir . $imagefile;
	      		if ((copy($_FILES['img_upload']['tmp_name'],$uploadfile))&&($_FILES['img_upload']['type'] == 'image/gif')||($_FILES['img_upload']['type'] == 'image/bmp')||($_FILES['img_upload']['type'] == 'image/pjpeg'))
    	      	echo "The File has been Uploaded Successfully";
       	}
       	else
       	{
	    		echo "The File has not been uploaded.";
	//    		if (($_FILES['img_upload']['type'] != 'image/gif')&&($_FILES['img_upload']['type'] != 'image/bmp')&&($_FILES['img_upload']['type'] != 'image/jpeg'))
			echo "Please select a Image File";
       	}
	}
	}
			/* End of Code -- Done By Seetha.N */
?>

<html>

<head>
  <title>Insert Image</title>

<style type="text/css">
html, body {
  background: ButtonFace;
  color: ButtonText;
  font: 11px Tahoma,Verdana,sans-serif;
  margin: 0px;
  padding: 0px;
}
body { padding: 5px; }
table {
  font: 11px Tahoma,Verdana,sans-serif;
}
form p {
  margin-top: 5px;
  margin-bottom: 5px;
}
.fl { width: 9em; float: left; padding: 2px 5px; text-align: right; }
.fr { width: 6em; float: left; padding: 2px 5px; text-align: right; }
fieldset { padding: 0px 10px 5px 5px; }
select, input, button { font: 11px Tahoma,Verdana,sans-serif; }
button { width: 70px; }
.space { padding: 2px; }

.title { background: #ddf; color: #000; font-weight: bold; font-size: 120%; padding: 3px 10px; margin-bottom: 10px;
border-bottom: 1px solid black; letter-spacing: 2px;
}
form { padding: 0px; margin: 0px; }
</style>

</head>
<script type="text/javascript" src="popup.js"></script>

<script type="text/javascript">

window.resizeTo(400, 100);

function Init() {
  __dlg_init();
  var param = window.dialogArguments;
  if (param) {
      document.getElementById("f_url").value = param["f_url"];
      document.getElementById("f_alt").value = param["f_alt"];
      document.getElementById("f_border").value = param["f_border"];
      document.getElementById("f_align").value = param["f_align"];
      document.getElementById("f_vert").value = param["f_vert"];
      document.getElementById("f_horiz").value = param["f_horiz"];
      window.ipreview.location.replace(param.f_url);
  }
  document.getElementById("f_url").focus();
};

function onOK() {
  var required = {
    "f_url": "You must enter the URL"
  };
  for (var i in required) {
    var el = document.getElementById(i);
    if (!el.value) {
     // alert(required[i]);
      el.focus();
      return false;
    }
  }
//  pass data back to the calling window
  var fields = ["f_url", "f_alt", "f_align", "f_border",
                "f_horiz", "f_vert"];
  var param = new Object();
  for (var i in fields) {
    var id = fields[i];
    var el = document.getElementById(id);
	param[id] = el.value;	
  }
  __dlg_close(param);

  return false;
//  window.opener.document.edit.ta.value="<img src='/cms/srs_images/" + document.frminsert.url.value + "'">;
//  window.close();
};

function onCancel() {
  __dlg_close(null);
  return false;
};

function onPreview() {
  var f_url = document.getElementById("f_url");
  var url =  f_url.value;
  if (!url) {
    alert("You have to enter an URL first");
    f_url.focus();
    return false;
  }
  window.ipreview.location.replace(url);
  return false;
};

function doSubmit()
{
document.frminsert.submitted.value="true";
document.frminsert.submit();
};

function doInsert()
{
	if(document.frminsert.imgName.value == '')
    {
	    alert('You have to upload the image first');
    }
	if(document.frminsert.imgName.value != '')
    {
       var n = '<?=$name?>';
	//document.frminsert.url.value=document.frminsert.imgName.value;
     	//var imgname = document.frminsert.imgName.value;
	//imgname = "/cms/srs_images/" + imgname;
    	//var hidden_field="hidden_" + n;
	alert('The image has been inserted. Click Preview to view the image in the pane');
    }
}
</script>

<body onload="Init()">
<form name = "frminsert" method="POST" enctype="multipart/form-data" action="">
<div class="title">Insert Image</div>
<!--- new stuff --->
<table border="0" width="100%" style="padding: 0px; margin: 0px">
<tbody>
<tr>
<td colspan="2">    
 <!--    <button name="Upload" onclick="return ImageUpload();">Upload</button> -->
<? if ($submitted != true) 
{
	echo "<input name='img_upload' type='file'>";
	echo "<button name='butsubmit' value='Upload' onclick='javascript:doSubmit();'>Upload</button>";
}
else
{
	echo "<input name='img_upload' type='file' disabled>";
	echo "<input type='button' name='butsubmit' value='Upload' onclick='javascript:doSubmit()' disabled >";
	
}
?>
<button name="insert" value="Insert Image" onclick="javascript:doInsert();">Insert Image</button>
</td >
</tr>
<tr>
    <td  colspan=2 style="text-align: left">Image Name:&nbsp;
    	<input type="text" name="url" id="f_url" size="30" title="Enter the image URL here" />
      	<button name="preview" onclick="return onPreview();" title="Preview the image in a new window">Preview</button>
    </td>
</tr>
<tr>
    <td colspan="2" style="text-align: left">Alternate text:&nbsp;
    	<input type="text" name="alt" id="f_alt" size="40" title="For browsers that don't support images" /></td>
</tr>

  </tbody>
</table>

<p />

<fieldset style="float: left; margin-left: 5px;">
<legend>Layout</legend>

<div class="space"></div>

<div class="fl">Alignment:</div>
<select size="1" name="align" id="f_align"
  title="Positioning of this image">
  <option value=""                             >Not set</option>
  <option value="left"                         >Left</option>
  <option value="right"                        >Right</option>
  <option value="texttop"                      >Texttop</option>
  <option value="absmiddle"                    >Absmiddle</option>
  <option value="baseline"        		      >Baseline</option>
  <option value="absbottom"                    >Absbottom</option>
  <option value="bottom"                       >Bottom</option>
  <option value="middle"  selected="1"         >Middle</option>
  <option value="top"                          >Top</option>
</select>

<p />

<div class="fl">Border thickness:</div>
<input type="text" name="border" id="f_border" size="5" value="0"
title="Leave empty for no border" />

<div class="space"></div>

</fieldset>

<fieldset style="float:right; margin-right: 5px;">
<legend>Spacing</legend>

<div class="space"></div>

<div class="fr">Horizontal:</div>
<input type="text" name="horiz" id="f_horiz" size="5" value="0"
title="Horizontal padding" />

<p />

<div class="fr">Vertical:</div>
<input type="text" name="vert" id="f_vert" size="5" value="0"
title="Vertical padding" />

<div class="space"></div>

</fieldset>
<br clear="all" />
<table width="100%" style="margin-bottom: 0.2em">
 <tr>
  <td valign="bottom">
    Image Preview:<br />
    <iframe name="ipreview" id="ipreview" frameborder="0" style="border : 1px solid gray;" height="200" width="300" src=""></iframe>
  </td>
  <td valign="bottom" style="text-align: right">
    <button type="button" name="ok" onclick="return onOK();">Done</button>
    <!-- button type="button" name="cancel" onclick="return onCancel();">Cancel</button -->
  </td>
 </tr>
</table>
<input type=hidden name=submitted value="false">
<input type=hidden  name=imgName value="<?=$imagefile?>">

</form>
</body>
</html>
