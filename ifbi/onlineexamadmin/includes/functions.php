<?php
/****************************************************
* Application Name            :  IIB IWAY BULKUPLOAD
* Module Name                 :  Functions.php
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  Balachandar Muruganantham
* Created ON                  :  Feb 9 2005
* Last Modified By            :  B.Devi
* Last Modified Date          :  24-03-2006
* Description                 :  Contains a generic functions which is used in iway upload
*****************************************************/

//Header array to be checked with the excel 
$header = array("centre_code","iway_name","actual_seats","no_of_seats","status","iway_address1","iway_address2","iway_city","iway_state","iway_pin_code","exam_centre_code");

$centre_validate = '/^[1-9][0-9]{5}[A-Z]{1}$/i';
$num_validate = '/^[1-9][0-9]{0,2}$/i';
$exam_centre_validate = '/^[0-9]{1,11}$/i';
$add2_validate = '/^[0-9 \/,-]*$/i';
$status_validate = '/^(N|A|D)$/';
$pin_code_validate='/^[1-9][0-9]{5}$/i';
$validation_alpha= '/^[a-zA-Z ]*$/i'; //alphabet validation for state and city

$quesid_validate = '/^[0-9]+$/i';
$correctans = '/^[1-5]$/i';//correct_answer

//Array to store all slots
$select_time="select slot_time from iib_exam_slots order by slot_time";
$res_time=mysql_query($select_time);
if ($commondebug){
	print $select_time;
	echo mysql_error();
}
if (mysql_error()){
	echo mysql_error();
	exit;	
}
$aslot_time=array();
while(list($slot_time) = mysql_fetch_row($res_time))
{
	$aslot_time[]=$slot_time;
}//end of while


/**
*		fetching the uploaded file from the table
*		Table used: excelfiles
*/

$sql_file = "select file_name, language,exam_date from ".TABLE_EXCEL_FILES." where file_id=$fileid";

$result_file = @mysql_query($sql_file);
list($file_name,$language,$excel_exam_date) = mysql_fetch_row($result_file);

	if ($language == "E"){
			$languageText = "English";
	}elseif ($language== "H"){
			$languageText = '<img src="images/hindi.gif" align="absmiddle">';
	}else{
			$languageText = "-";	
	}

/*Array Formed to the check whether the ExamCentreCode matches in the iib_exam_centres*/
$sql_exam_centres="select exam_centre_code "
					   ." from iib_exam_centres "
					   ." where online='Y'";
if($commonDebug){
	echo $sql_exam_centres;
}
else{
	$res_exam_centres=mysql_query($sql_exam_centres);
}

if(!$res_exam_centres){
	die("Error in Selecting Records From ExamCentres".mysql_error());
}
$aexam_centres=array();
while(list($eCentreCode)=mysql_fetch_row($res_exam_centres)){
	$aexam_centres[]=$eCentreCode;
}// end of while

/*variables used in $cntseats array*/

$var_iwayname="iwayname";		
$var_no_of_seats="no_of_seats";	
$var_actualseats="actualseats";	
$var_status="status";	
$var_iway_address1="iway_address1";	
$var_iway_address2="iway_address2";	
$var_iway_city="iway_city";	
$var_iway_state="iway_state";	
$var_iway_pin_code="iway_pin_code";	
$var_exam_centre_code="exam_centre_code";	
$varupper="upper";
/*Array to store the details of all centre code*/
$select_seats="select  $varupper(trim(centre_code)) , "
					." $varupper(trim(iway_name)) , "
					." trim(no_of_seats) , "
					." trim(actual_seats) , "
					." trim(status) , "
					." $varupper(trim(iway_address1)) , "
					." trim(iway_address2) , "
					." $varupper(trim(iway_city)) , "
					." $varupper(trim(iway_state)) , "
					." trim(iway_pin_code) , "
					." trim(exam_centre_code)  "					
					." from iib_iway_details";			
$res_seats=@mysql_query($select_seats);
if ($commondebug){
	print $select_seats;
	echo mysql_error();
}
if (mysql_error()){
	echo mysql_error();
	exit;	
}

$cntseats=array();
while(list($dbcentre_code,$dbiway_name,$dbno_of_seats,$dbactual_seats,$dbstatus,$dbiway_address1,$dbiway_address2,$dbiway_city,$dbiway_state,$dbiway_pin_code,$dbexam_centre_code) = mysql_fetch_row($res_seats))
{
	$cntseats[$dbcentre_code][$var_iwayname] = "$dbiway_name";
	$cntseats[$dbcentre_code][$var_no_of_seats] = "$dbno_of_seats";
	$cntseats[$dbcentre_code][$var_actualseats] = "$dbactual_seats";
	$cntseats[$dbcentre_code][$var_status] = "$dbstatus";
	$cntseats[$dbcentre_code][$var_iway_address1] = "$dbiway_address1";
	$cntseats[$dbcentre_code][$var_iway_address2] = "$dbiway_address2";
	$cntseats[$dbcentre_code][$var_iway_city] = "$dbiway_city";
	$cntseats[$dbcentre_code][$var_iway_state] = "$dbiway_state";
	$cntseats[$dbcentre_code][$var_iway_pin_code] = "$dbiway_pin_code";
	$cntseats[$dbcentre_code][$var_exam_centre_code] = "$dbexam_centre_code";	
	
}// end of while


/**
*		$all_slots_filled - Array with  the centre code while has entry for all slots 
*		$missed_slot - Array with  the centre code while has entry missing with some slots 
*		
*/

$excel_exam_date		= explode(" ",$excel_exam_date);//Exam Date selected in Combo


$select_code="select $varupper(trim(centre_code)),exam_time from iib_iway_vacancy where exam_date='$excel_exam_date[0]' ";

$res_code=@mysql_query($select_code);
if ($commondebug){
	print $select_code;
	echo mysql_error();
}
if (mysql_error()){
	echo mysql_error();
	exit;	
}

//Vacancy Array will have all the centre code and the existing slots for a particular date from db.
$vaccancy=array();
while(list($centre_code,$exam_time) = mysql_fetch_row($res_code))
{
	$vaccancy[$centre_code][]=$exam_time;
}

//Missed slot Array is formed using the centre code those are present in the database for particular date
//Missed slot Array will have centre code(for which even one slot is missing) as key and the missed slot as the value.
//Missed slot Array will not have centre code which doesn't have any entry in the db ie., new centre


//Remaining  centres Array will heve all the centre codes which has atleast one entry in the database.
//Some centres will present in both missed slot and Reamining centres(Teh centres which have entry for some slots)

//all slots filled array will have the centres for which all the slots are filled.
//all slots is got by differentiating the missed slot from the remaining centres array.

$missed_slot=array();
$all_slots_filled=array();
$remaining_centres=array();
foreach($aslot_time as $key){	
	foreach($vaccancy as $key1 =>$value){
		if(!in_array($key,$value))
		{			
			$missed_slot[$key1][]=$key;	
					
		}
		else{			
			$remaining_centres[]=$key1;					
		}
	}//$foreach($vaccancy)	
}// end of foreach($aslottime)

$missed_checkslot=array_keys($missed_slot);
$remaining_centres=array_unique($remaining_centres);
$missed_checkslot=array_unique($missed_checkslot);
$all_slots_filled=array_diff($remaining_centres,$missed_checkslot);
/**
*			@function		:			headervalidate
*			@param			:			header array index
*			@param			:			cell data
*			@return			:			1 - no error | 0 - error
*/
function headervalidate($headerindex,$celldata){
global $header;
	//decremented to 1 since we have the header starting the index from 0
	$headerindex = $headerindex - 1;
	//checking for the header row data
	if (trim($celldata) == $header[$headerindex])
	{
		return 1;
	}
	else
	{
		return 0;
	}
}// end of function(headervalidate)

/**
*			@function		:			countCheck
*			@param			:			cell data
*			@return			:			1 - no error | 0 - error
*			Check done to find whether any of the centre code is duplicated
*			And General Validations
*/		
/*Function specific to centrecode: (esp to check the uniqueness of centre code in excel)*/
function countCheck($celldata,$UnqCentre_array){
	global $centre_validate,$all_slots_filled;
	$celldata=strtoupper(trim($celldata));
	if(trim($celldata)!=""){
		if(preg_match($centre_validate,trim($celldata))){			
				$i=0;
				foreach($UnqCentre_array as $key)
				{					
					if(trim($key)==$celldata)
					$i++;
				}//end of foreach		
				
				if($i<=1){							
					if(in_array(trim($celldata),$all_slots_filled))															
						return 0;
					else									
						return 1;
				}//end of $i
				else{				
					return 0;
				}
		}else{
			return 0;
		}	
	}// end of if($celldate!="")
	else{
		return 0;
	}
	
}//end of function countcheck




function rowvalidate($index,$celldata){
	global $centre_validate, $validation_alpha,$add2_validate,$exam_centre_validate, $num_validate, $status_validate, $pin_code_validate, $aseats, $nseats,$aexam_centres;
	
	
	if ($index == 2){
		if(trim($celldata)!=""){
			 if(preg_match($validation_alpha,trim($celldata))){
					return 1;
			 }else{
					return 0;
			 }
		 }
		 else{
			return 0;
		}
	}// end of $index=2
	if ($index >= 3 &&  $index <= 4){
		if ($index == 3){
			if(trim($celldata)!=""){
				if(preg_match($num_validate,trim($celldata))){
					return 1;
				}
				else{
					return 0;
				}
			}
			else{
				return 0;
			}				
		}// end of $index=3
		if ($index == 4){
			if(trim($celldata)!=""){
				if(preg_match($num_validate,trim($celldata))){
					return 1;
				}
				else{
					return 0;
				}
			}
			else{
				return 0;
			}				
		}// end of $index=4

	}// end of $index=2 and 3	
	
	
	
	if ($index == 5){
		if(trim($celldata)!=""){
			 if(preg_match($status_validate,trim($celldata))){
					return 1;
			 }else{
					return 0;
			 }
		}
		 else{
			return 0;
		}
	}// end of $index=5
	
	if ($index == 6){
		if(trim($celldata)!=""){
			return 1;
		}
		else{
				return 0;
		 }
	}// end of $index=6
	
	if ($index == 7){
		if(trim($celldata)!=""){			
			if(preg_match($add2_validate,trim($celldata))){
				if(trim($celldata)!="0"){
					return 1;
				}
				else{
					return 0;
				}
			 }else{
					return 0;
			 }
		}		
	}// end of $index=7
	
	if ($index >= 8 &&  $index <= 9){
		if(trim($celldata)!=""){	
			 if(preg_match($validation_alpha,trim($celldata))){
					return 1;
			 }else{
					return 0;
			 }
		 }
	}// end of $index=8 and 9
	
	if ($index == 10){
		if(trim($celldata)!=""){
			 if(preg_match($pin_code_validate,trim($celldata))){
					return 1;
			 }else{
					return 0;
			 }
		 }else{
				return 0;
		 }
	}// end of $index=10
	
	if ($index == 11){
		if(trim($celldata)!=""){
		
			 if((preg_match($exam_centre_validate,trim($celldata)))&&(in_array(trim($celldata),$aexam_centres))){
					return 1;
			 }else{
					return 0;
			 }
		}else{
				return 0;
		}
	}// end of $index=11
	
return 1;// if no error return 1
}//end of function





/** function convert the bytes to KB or MB */
function filesizeinwords($sizeinbytes)
{
	$sizelength = strlen($sizeinbytes);

	switch($sizelength){
	case ($sizelength >= 6):
		$sizelength = ($sizeinbytes/1048576) . "mb";
		break;
	case ($sizelength >=3):
		$sizelength = ($sizeinbytes/1024)."kb";
		break;
	default:
		$sizelength = ($sizeinbytes) . "b";
		break;
	}// end of switch
	return $sizelength;
}// end of function


function isNull($mystring){
	global $language;

	if ($language == "E"){
		if (trim($mystring)!=""){
			//$mystring = decryptText($mystring);			
			return $mystring;	
		}
	}else{
		if (trim($mystring)!=""){
			//$mystring = "<font face='DVBW-TTYogeshEN' size=4>".decryptText($mystring)."</font>";
			$mystring = "<font face='DVBW-TTYogeshEN' size=4>".$mystring."</font>";
			return $mystring;
		}
	}
	return false;
}// end of function(isNull)

/*Function used for uploading Correct Answer Excel*/

function file_upload($file,$path)
{
	
	$excelFile 		= $file['excelfile'];
	$filename 		= $excelFile['name'];
	$afilename		= explode(".",$filename);	
	$renamefile		= date('dmYHis').".".$afilename[1];
	$filenamepath 	= $path.$renamefile;
    global $message;	
	if($filename <> "")
	{
		if($afilename[0]!=""){
			if($afilename[1] == "xls")
			{
				$mimetype 		= $excelFile['type'];
				$filesize 		= $excelFile['size'];
				$fileerror 		= $excelFile['error'];
				if (!$fileerror){					
						if(move_uploaded_file($excelFile['tmp_name'],$filenamepath)){
							$xlsfilename	= $renamefile;							
						}//end of if(move_uploaded_file)
						else{
							$message = "File upload Error!.";
					}
				}//end of if(!$fileerror)
				else{
					$message = "File is too Big to upload!";
				}
			}//end of $afilename[1]
			else
			{
				$message = "File Upload Error. Please choose Excel file.";
			}
				
		}//if($afilename[0]!="")
		else
		{
			$message = "Invalid File Name. Please choose an excel file.";	
		}
		
	}//if($filename<>"")
	else
	{
		$message = "File Upload Error. Please choose an excel file.";	
	}

	return $xlsfilename.",".$message;
	
	
	
}//function(fileupload)

//Function To display the Counts from iib_section_questions
function dispdata_question(){
	//Array to store datas from iib_section_questions
	$select_disp_data="select exam_code,subject_code,count(question_id) as ques_count from iib_section_questions group by exam_code,subject_code";	
	$res_disp_data=mysql_query($select_disp_data);
	if ($commondebug){
		print $select_disp_data;
		echo mysql_error();
	}
	if (mysql_error()){
		echo mysql_error();
		exit;	
	}
	$asecques_data=array();	

	while ($secquesDetails = mysql_fetch_assoc($res_disp_data)) 
	{
		$asecques_data[]=$secquesDetails;	
	}//end of while
	
	return $asecques_data;

	
}


//Function To store Question IDS
function select_ques(){
	//Array to store question_ids
	$select_question_id="select trim(question_id),trim(correct_answer) from iib_section_questions";
	$res_question_id=mysql_query($select_question_id);
	if ($commondebug){
		print $select_question_id;
		echo mysql_error();
	}
	if (mysql_error()){
		echo mysql_error();
		exit;	
	}
	$aquestion_id=array();	
	while(list($list_ques_id,$list_correct_ans) = mysql_fetch_row($res_question_id))
	{
		$aquestion_id[$list_ques_id]=$list_correct_ans;		
		
	}//end of while
	
	return $aquestion_id;

}//function(select_ques)

//Function To store Question IDS
function select_quesid_details($ques_log_id){
	
	$select_question_details="select question_id,exam_code,subject_code,section_code,correct_answer,option_1,option_2,option_3,option_4,option_5 from iib_section_questions where question_id='".trim($ques_log_id)."' ";	
	$res_question_details=mysql_query($select_question_details);
	if ($commondebug){
		print $select_question_details;
		echo mysql_error();
	}
	if (mysql_error()){
		echo mysql_error();
		exit;	
	}
	$aQDetails=array();
	while ($QDetails = mysql_fetch_assoc($res_question_details)) 
	{
		$aQDetails=$QDetails;	
	}//end of while
	
	return $aQDetails;

}//function(select_ques)

/**
*			@function		:			headervalidate
*			@param			:			header array index
*			@param			:			cell data
*			@return			:			1 - no error | 0 - error
*/



//Header array to be checked with the excel 
$header_update = array("question_id","correct_answer");
function header_update_validate($headerindex,$celldata){
global $header_update;
	//decremented to 1 since we have the header starting the index from 0
	$headerindex = $headerindex - 1;
	//checking for the header row data
	if (trim($celldata) == $header_update[$headerindex])
	{
		return 1;
	}
	else
	{
		return 0;
	}
}// end of function(headervalidate)

$aquestion_id=select_ques();
/*Function specific to Question ID: (esp to check the uniqueness of Question ID in excel)*/
function UniqueCheck($celldata,$Unq_ques_id_array){	
	global $quesid_validate,$aquestion_id;
	$celldata=trim($celldata);
	if(trim($celldata)!=""){		
		if((preg_match($quesid_validate,trim($celldata))) && (array_key_exists(trim($celldata),$aquestion_id))){
											
				$i=0;
				foreach($Unq_ques_id_array as $key)
				{					
					if(trim($key)==$celldata)
					$i++;
				}//end of foreach		
				
				if($i<=1){							
					return 1;
				}//end of $i
				else{				
					return 0;
				}
		}else{			
			return 0;
		}	
	}// end of if($celldate!="")
	else{
		return 0;
	}
	
}//end of function countcheck

function rowval($index,$celldata){
	global $correctans;

	if ($index == 2){
		if(trim($celldata)!=""){
			 if(preg_match($correctans,trim($celldata))){
					return 1;
			 }else{
					return 0;
			 }
		 }
		 else{
			return 0;
		}
	}// end of $index=2
}

/*Files to be redirected in View Uploaded Files Page*/


//This is for Iway Upload
$IWAY_KEY="IWU";//key set for iway upload
$viewfile_redirect[$IWAY_KEY]="viewfile.php"; //Array set for viewing the file	
$viewdb_redirect[$IWAY_KEY]="viewdb.php";//Array set for viewing the db
?>
