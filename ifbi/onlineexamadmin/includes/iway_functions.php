<?
/****************************************************
* Application Name            :  IIB IWAY BULKUPLOAD
* Module Name                 :  Functions.php
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  iib_exam_schedule
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  Koshy.K.I.
* Created ON                  :  18/03/2006.
* Last Modified By            :  B.Devi
* Last Modified Date          :  24-03-2006
* Description                 :  Functions used in iway upload.
*****************************************************/

function getdistinctDates()
{
$sql="select distinct exam_date from iib_exam_schedule order by exam_date";
$res=mysql_query($sql);
return $res;
}


//wherever this function is used $language and $source_key should be declared
function fileupload($file,$path,$edate)
{
	global $excelerrormsg,$language,$source_key;	
	$excelFile 		= $file['iwayexcelfile'];
	$filename 		= $excelFile['name'];
	$afilename		= explode(".",$filename);
	$renamefile		= $afilename[0].".".$afilename[1];
	$filenamepath 	= $path.$renamefile;
	
if($filename <> "")
{
	if($afilename[0]!=""){
	if(!file_exists($filenamepath))
	{
		if($afilename[1] == "xls")
		{
			//$renamefile		= chmod($renamefile,0777);
			//$filenamepath 	= $path.$renamefile;
			$mimetype 		= $excelFile['type'];
			$filesize 		= $excelFile['size'];
			$fileerror 		= $excelFile['error'];
			
			$upload_user	= $_SESSION["admin_id"];
			if (!$fileerror){
					if(move_uploaded_file($excelFile['tmp_name'],$filenamepath)){
					//if(move_uploaded_file(("$excelFile[tmp_name]", 0777),$filenamepath))
						$xlsfilename	= $renamefile;
						
						$sql  = "insert into ".TABLE_EXCEL_FILES."(file_name,file_type,file_size,language,is_imported,is_active,uploaded_on,uploaded_by,exam_date,source_key) ";
						$sql .= "values('$renamefile','$mimetype','$filesize','$language','".INACTIVE_FLAG."','".ACTIVE_FLAG."',NOW(),'$upload_user','$edate','$source_key')";
						
						$res = mysql_query($sql);
						if(mysql_error()){
							$msg = "<FONT face='Verdana, Arial, Helvetica, sans-serif' size=2 color='red'>Error Inserting the information about the uploaded files.</font>";
						}
						else
						{
							$retfilename = $xlsfilename;
							$msg = "<FONT face='Verdana, Arial, Helvetica, sans-serif' size=2>".$retfilename. " File uploaded successfully.</font>";
						}
					}//end of if(move_uploaded_file)
					else{
						$msg = "<FONT face='Verdana, Arial, Helvetica, sans-serif' size=2 color='red'>File upload Error!.</font>";
				}
			}//end of if(!$fileerror)
			else{
				$msg = "<FONT face='Verdana, Arial, Helvetica, sans-serif' size=2 color='red'>File is too Big to upload!</font>";
			}
		}//end of $afilename[1]
		else
		{
			$msg = "<FONT face='Verdana, Arial, Helvetica, sans-serif' size=2 color='red'>File Upload Error. Please choose Excel file.</font>";
		}
		
	}//end of !file_exists
	else
	{
		$msg = "<FONT face='Verdana, Arial, Helvetica, sans-serif' size=2 color='red'>File Already Exists!</font>";
	}
	}//end of $afilename[0]!=""
else
{
	$msg = "<FONT face='Verdana, Arial, Helvetica, sans-serif' size=2 color='red'>Invalid File Name. Please choose an excel file.</font>";	
}
}//end of $function<>""
else
{
	$msg = "<FONT face='Verdana, Arial, Helvetica, sans-serif' size=2 color='red'>File Upload Error. Please choose an excel file.</font>";	
}

	return $msg;
}//end of function(fileupload)
?>