<?php
/**
*   @author   :   Balachandar Muruganantham
*   @created  :   Feb 25 2005
*   @purpose  :   Logger which logs all the user activities
*
*/

$file_accessed = $_SERVER['PHP_SELF'];
$remote_host = $_SERVER['REMOTE_ADDR'];
$user_agent = $_SERVER['HTTP_USER_AGENT'];
$http_referrer = $_SERVER['HTTP_REFERER'];
$argument = $_SERVER['argv'][0];

$sql_logging="insert into ".TABLE_LOG."(fileaccessed,browser,referrer,access_time,argument)"
						 ." Values('$file_accessed','$user_agent','$http_referrer',NOW(),'$argument')";

$result_log = @mysql_query($sql_logging);

if (mysql_error()){
	header("Location: errorpage.php?status=4");
}

?>
