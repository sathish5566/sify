var secs
var timerID = null
var timerRunning = false
var delay = 1000

function InitializeTimer(len_of_timer, formname,formaction)
	{
	    // Set the length of the timer, in seconds
	    secs = len_of_timer
	    StopTheClock()
	    StartTheTimer(formname,formaction)
	}
	
	function StopTheClock()
	{
	    if(timerRunning)
	        clearTimeout(timerID)
	    timerRunning = false
	}
	
	function StartTheTimer(formname,formaction)
	{
		if (secs == 300)
		{
			window.open('timeleft.html','wintime','top=300,left=300,width=150,height=100');					
		}
	    if (secs==0)
	    {
	        StopTheClock();
	        eval('document.' + formname +'.time_left.value = 0');
	        eval("document." + formname + ".action='"+formaction+"'");
	        eval('document.'+ formname + '.submit()');
	    }
	    else
	    {
		    
			var min = Math.floor(secs/60);
			var remaining_secs = secs%60;
			var texthr = Math.floor(min/60);		
			var textmin = new String(min%60);
			
			if (textmin.length == 1)
			{
				textmin = "0"+textmin;
			}
			var textsec = new String(remaining_secs);
			
			if (textsec.length == 1)
			{
				textsec = "0"+textsec;
			}
			self.status = 'TIME LEFT : ' + texthr + ' Hr(s) '+ textmin + ' Min(s) ' + remaining_secs + ' Secs '
	        eval("self.document." + formname + ".timeleft.value ='" + texthr + ':' + textmin +':'+textsec+" hrs'");
	        eval('self.document.' + formname + '.time_left.value = ' + secs);
	        var temp = secs;
	        secs = secs - 1;
	        timerRunning = true
	        timerID = self.setTimeout("StartTheTimer('" + formname + "','" + formaction + "')", delay)	       
	    }
}