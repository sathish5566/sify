<?php

/****************************************************
* Application Name            :  IIB IWAY BULKUPLOAD
* Module Name                 :  Settings Page
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects: 
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  Balachandar Muruganantham
* Created ON                  :  Feb 9 2005
* Last Modified By            :  B.Devi
* Last Modified Date          :  29-03-2006
* Description                 :  Settings page for the application
*****************************************************/

/** Application title  */
define('APP_TITLE','Bulk Upload Application');
define('APP_INDEX_HEADING','Welcome to Bulk Upload Utility!');
define('APP_INDEX_TEXT','Bulk Upload Utility facilitates the upload for Question paper, from the Microsoft Excel Sheet to the Mysql Database using PHP which will be accessed by the end user through an web based interface provide here.');
define('APP_INDEX_FORM_HEADING','Bulk Upload Login');

/** Status message */
$status1[1] = "Invalid User Login!";
$status1[2] = "Database connection Failed! Please contact system Administrator";
$status1[3] = "Database Selection Problem! Please contact SysAdmin.";
$status1[4] = "Database Query Execution Failed. Please contact Sysadmin!";
$status1[5] = "Successfully Imported the Data. Records Uploaded: ";
$status1[6] = "Successfully Logged Out!";
$status1[7] = "File not Found!";
$status1[8] = "Error in Excel file(Should have only one sheet)";
$status1[9] = "Number of Columns is less than 11";
$status1[10] = "No Entry In Slots. Please make entry in Slots";
$status1[11] = "Error in Excel file. No Records Found";
$status1[12] = "Number of Columns should be 2";
$status1[13] = "Keyword problem.Please contact system Administrator";

/** Database Tables */
define('TABLE_CONFIG','config');
define('TABLE_AUDIT_TRAIL','bulk_audit_trail');
define('TABLE_EXCEL_FILES','excelfiles');
define('TABLE_BULK_KEYS','bulk_keys');
define('TABLE_SESSION','bulk_session');
define('TABLE_LOG','bulk_log');
define('TABLE_IWAY_DETAILS','iib_iway_details');
define('TABLE_IWAY_VACANCY','iib_iway_vacancy');
define('TABLE_SECTION_QUESTIONS','iib_section_questions');
define('TABLE_QP_CORRECTANS_UPD_LOG','iib_qp_correctans_update_log');

/** file name path where the excel file were uploaded (iway upload) */
$filenamepath = "bulkuploadfiles/";

//Path to store the excel files used for correctanswer update
$correctans_excelpath="correctansfiles/";

/*Number of columns for correctanswerupdate excel*/
$constant_cols="2";

/** definition */
define('ACTIVE_FLAG','1');
define('INACTIVE_FLAG','0');

/** definition for File Upload */
define('FILE_ALREADY_EXISTS','File Already Exists!');
define('FILE_UPLOAD_ERROR','File Upload Error!');
define('FILE_UPDATE_DB','Error Inserting the information about the uploaded files.');
define('FILE_SIZE_WARNING','File is too Big to upload!');
define('FILE_UPLOAD_SUCCESS','File $filename uploaded successfully.<br>File Type: $mimetype.<br>File Size: $filesize');

/** definition for the authentication */
define('AUTH_ERROR','Bulk Upload Application - Authentication Error');

/** key used for encryption and decryption */
$keyval = "iibf security";
?>
