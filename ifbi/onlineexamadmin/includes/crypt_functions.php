<?php

	function openHandler()
	{
		global $td, $iv, $key, $keyval;
			
		/* Open the cipher */
		$td = mcrypt_module_open('rijndael-128', '', 'ecb', '');

		/* Create the IV and determine the keysize length, used MCRYPT_RAND
		* on Windows instead */
		$iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_DEV_RANDOM);
		$ks = mcrypt_enc_get_key_size($td);
		
		/* Create key */
		$key = substr(md5("iibf"), 0, $ks);
	}
	
	function encryptText ($strData)
    {
		global $td, $iv, $key;

		/* Intialize encryption */
		mcrypt_generic_init($td, $key, $iv);

		/* Encrypt data */
		$encrypted = mcrypt_generic($td, $strData);
		mcrypt_generic_deinit($td);

		return $encrypted;
    }
 
    function decryptText ($enText)
    {
		global $td, $iv, $key;

    	/* Initialize encryption module for decryption */
		mcrypt_generic_init($td, $key, $iv);
		$decrypted = mdecrypt_generic($td, $enText);
		
		/* Terminate decryption handle and close module */
		mcrypt_generic_deinit($td);
		return $decrypted;
    }

	function closeHandler()
	{
		global $td;
		mcrypt_module_close($td);
	}
	
?>
