<?

/*Function To Display Error Message*/
function disp_error($errormsg){
	if($errormsg!="")
	{
?>
	<!--<table bgcolor="#E8D897" border="0" cellspacing="1" cellpadding="5" align="center">
		<tr align="left" class="tablecontent2" bgcolor="#FFFFFF" valign="middle">
			<td height="30" valign="middle" class="text1">
				<font color="#FF0000"><strong><?=$errormsg?></strong><font>
			</td>
		</tr>
	</table>-->
	<table  border="0" cellspacing="1" cellpadding="5" bgcolor="FFFFFF" align="center">
		<tr align="left" class="greybluetext10" bgcolor="#D1E0EF" valign="middle">
			<td height="30" valign="middle" class="text1">
				<font color="#FF0000"><strong><?=$errormsg?></strong><font>
			</td>
		</tr>
	</table>
<?
	}
}


/*Headers in QP Upload*/
$header = array("case_id","exam_code","subject_code","section_code","question_text","option_1","option_2","option_3","option_4","option_5","correct_answer","marks","case_study","case_mcq");
function headervalidate($headerindex,$celldata){
global $header;
	//decremented to 1 since we have the header starting the index from 0
	$headerindex = $headerindex - 1;

	//checking for the header row data
	if (trim($celldata) == $header[$headerindex]){
		return 1;
	}else{
		return 0;
	}

}

/*Headers in Candidate Upload*/
//$header_candidate = array("membership_no","name","email_id","address1","address2","address3","address4","city","state","pin_code");

function header_candivalidate($headerindex,$celldata){
     global $header_candidate;
	//decremented to 1 since we have the header starting the index from 0
	$headerindex = $headerindex - 1;
	//checking for the header row data
	if (trim($celldata) == $header_candidate[$headerindex]){
		return 1;
	}else{
		return 0;
	}
}
function header_examcentrevalidate($headerindex,$celldata){
     global $header_candidate;
		//decremented to 1 since we have the header starting the index from 0
	$headerindex = $headerindex - 1;
	//checking for the header row data
	if (trim($celldata) == $header_candidate[$headerindex]){
		return 1;
	}else{
		return 0;
	}
}

/*Headers in Activation List Upload*/
$header_activate= array("membership_no","exam_code","subject_code","medium_code","expiry_date");
function header_activate($headerindex,$celldata){
global $header_activate;
	//decremented to 1 since we have the header starting the index from 0
	$headerindex = $headerindex - 1;

	//checking for the header row data
	if (trim($celldata) == $header_activate[$headerindex]){
		return 1;
	}else{
		return 0;
	}

}

/*Headers in Case Details Upload*/
$header_case= array("case_id","exam_code","subject_code","section_code","case_text","marks");
function header_case($headerindex,$celldata){
global $header_case;
	//decremented to 1 since we have the header starting the index from 0
	$headerindex = $headerindex - 1;

	//checking for the header row data
	if (trim($celldata) == $header_case[$headerindex]){
		return 1;
	}else{
		return 0;
	}

}



/*mail date format -- included by mahalakshmi*/
define("MAIL_DATE_FORMAT","dd/mm/yyyy");

/*Function To Display Dates in Specified Format*/
function dateFormat($date,$to)
{
		if($date!='' && $to !='')
		{
		//$CurrentTime = strftime("%T");
		$DateStr     = strtotime("$date");
		switch($to)
		{
			case "yyyy-mm-dd":
			{
				$ReturnDate = strftime("%Y-%m-%d",$DateStr);
				break;
			}
			case "mm/dd/yyyy":
			{
				$ReturnDate = strftime("%m/%d/%Y",$DateStr);
				break;
			}
			case "dd/mm/yyyy":
			{
				$ReturnDate = strftime("%d/%m/%Y",$DateStr);
				break;
			}
			case "dd-Mon-yy":
			{
				$ReturnDate = strftime("%d-%b-%y",$DateStr);
				break;
			}
			case "dd-mm-yyyy":
			{
				$ReturnDate = strftime("%d-%m-%Y",$DateStr);
				break;
			}
		}

	}
return $ReturnDate;
}

function dispName($xat_id)
{
	global $connSel;
	$sql_sel =" SELECT first_name, middle_name, last_name FROM ".TABLE_XAT_CAND_REGS." WHERE xat_id=$xat_id";
	//echo $sql_sel;
	$res_sel = @mysql_query($sql_sel,$connSel);
	if($result = @mysql_fetch_row($res_sel))
	{
		$name = $result[0]." ".$result[1]." ".$result[2];
	}

	echo "
		<table width=60% align=center cellpadding=5 cellspacing=0>
			<tr class=tablecontent2>
				<td width=25% height=30>".Message_xat_id."</td>
				<td width=75% height=30>$xat_id </td>
			</tr>
			<tr class=tablecontent2>
				<td width=25% height=31>".Message_name."</td>
				<td width=75% height=31>$name</td>
			</tr>
		</table>";
}
function getAdminUsers($id)
{
	global $connSel;
	$sql_sel	= "
					SELECT name, organization, email_address, login, type_of_user
					FROM ".TABLE_XAT_ADMIN."
					WHERE id = $id AND is_active = 'Y'";
	//echo $sql_sel;
	$res_sel	= @mysql_query($sql_sel,$connSel);
	if($result = @mysql_fetch_row($res_sel))
	{
		return $result;
	}
}

function getReports($sql_sel)
{
	//global $connSel;

	$row = array();

	$res_sel = @mysql_query($sql_sel);

	if(!$res_sel)
	{
		disp_error("Error in your SQL query");
		return false;
	}

	$num_rows = @mysql_num_rows($res_sel);

	if($num_rows == 0)
	{
		$result = array();
		array_push($row,$result);
		//$row = 0;
		//disp_error("Sorry No record found");
		//return false;
	}
	else
	{
		while($result = @mysql_fetch_array($res_sel,1))
		{
			//array_push($row,$result);
			$row[] = $result;
		}
	}


	return $row;

}

function dispReports($result, $result2,$aheaders)
{
	echo "<center>
	<table width=100% border=0 cellpadding=3 cellspacing=1 bgcolor=#E8D897>
	  <tr align=center valign=middle class=tablecontent3>";



	if(!Isset($aheaders) || ($aheaders == ""))
	{
		disp_error("Sorry No header found");
		return false;
	}

	$length = count($aheaders);
	$width = (int)(100/$length);
	if($result2 == "sc/st")
	{
		foreach($aheaders as $key => $value)
		{
			if($key < 2)
			{
				echo "
				<td width='14%' rowspan=2>$value</td>";
			}
			else
			{
				if($key >= 2)
				{
					echo "<td colspan=2>$value</td>";
				}
			}
		}
		echo "</tr>
			 <tr align=center valign=middle class=tablecontent3>
                <td width='12%'>Yes</td>
                <td width='12%'>No</td>
                <td width='12%'>Yes</td>
                <td width='12%'>No </td>
                <td width='12%'>Yes</td>
                <td width='12%'>No</td>
              ";
	}
	else
	{
		foreach($aheaders as $key => $value)
		{
			echo "
			<td width='$width%'>$value</td>";
		}
	}

	echo "
		</tr>";


	if(!Isset($result) || ($result == "") || (count($result[0])==0))
	{
		echo "
		<tr align='center' valign='middle' class='tablecontent1'><td colspan='8'>";
		disp_error("Sorry No record found");
		echo "</td></tr></table>";
		return false;
	}

	$count = 1;
	/*echo "<pre>";
	print_r($result);
	echo "</pre>";*/
	$tdcount = 0;
	$trcount = 1;
	$trString = "";
	foreach($result as $key => $result1)
	{
		if($count % 2 != 0)
			$table = "tablecontent1";
		else
			$table = "tablecontent2";
		$trString .= "<tr align='center' valign='middle' class='$table'>";
		foreach($result1 as $key1 => $value)
		{
	/*		switch($key1){
				case "no_of_applicants":
					$no_of_applicants += $value;
					break;
				case "percentage":
					$percentage += $value;
					break;
				case "paid":
					$paid += $value;
					break;
			}*/
			$totalArray[1][1]="Total";
			$tdcount++;
			if ($tdcount > 0 ){
				$totalArray[$trcount][$tdcount] += $value;
			}
			$trString .= "<td width='16%' height='30'>$value</td>";
		}

		$trString .="</tr>";
		$count++;
		$tdcount = 0;
	}
	/*echo "<pre>";
	print_r($totalArray);
	echo "</pre>";*/

	/*if($result2 != "sc/st"){

	echo "<tr align='center' valign='middle' class='tablecontent3'>";
	//echo "No of Applications: ".$no_of_applicants." Paid ".$paid." Percentage ".$percentage;
	echo "<td width='16%' height='30'>&nbsp;<b>Total</b></td><td width='16%' height='30'>".$no_of_applicants."</td> <td width='16%' height='30'>".$paid."</td><td width='16%' height='30'>".round($percentage)."</td>";
	echo "</tr>";*/

	echo "<tr align='center' valign='middle' class='tablecontent3'>";
	foreach($totalArray[1] as $key=>$value){
		echo "<td width='16%' height='30'>$value</td>";
	}
	echo "</tr>";
	echo $trString;

	echo "</table></center>";

}

function getCityNames()
{
	global $connSel;
	$city = array();
	$sql_sel = "SELECT id, city_name FROM ".TABLE_XAT_TEST_CITY." WHERE is_active='Y' order by city_name;";
	//echo $sql_sel;
	$res_sel = mysql_query($sql_sel, $connSel);
	while($result = mysql_fetch_array($res_sel,1))
	{
		$key = $result['id'];
		$value = $result['city_name'];
		$city[$key]=$value;
	}
	return $city;

}
function getCityName($citycode)
{
	global $connSel;
	$sql_sel = "SELECT city_name FROM ".TABLE_XAT_TEST_CITY." WHERE id=$citycode AND is_active='Y';";
	//echo $sql_sel;
	$res_sel = mysql_query($sql_sel, $connSel);
	if($result = mysql_fetch_array($res_sel,1))
	{
		$city_name = $result['city_name'];
	}
	return $city_name;

}

function getCenterNames($citycode)
{
	global $connSel;
	$center = array();
	$sql_sel = "SELECT centre_code, centre_name FROM ".TABLE_XAT_TESTCENTRE_DETAILS." WHERE centre_test_city='$citycode' order by centre_name;";
	//echo $sql_sel;
	$res_sel = mysql_query($sql_sel, $connSel);
	while($result = mysql_fetch_array($res_sel,1))
	{
		$key = $result['centre_code'];
		$value = $result['centre_name'];
		$center[$key]=$value;
	}
	return $center;

}
function getCenterName($citycode,$centercode)
{
	global $connSel;
	$sql_sel = "SELECT centre_name FROM ".TABLE_XAT_TESTCENTRE_DETAILS." WHERE centre_test_city='$citycode' AND centre_code='$centercode';";
	//echo $sql_sel;
	$res_sel = mysql_query($sql_sel, $connSel);
	if($result = mysql_fetch_array($res_sel,1))
	{
		$center_name = $result['centre_name'];
	}
	return $center_name;

}


function getExamDate($centercode)
{
	global $connSel;
	$center = array();
	$sql_sel = "SELECT exam_date FROM ".TABLE_XAT_CANDIDATE_CENTRE." WHERE centre_code='$centercode';";
	//echo $sql_sel;
	$res_sel = mysql_query($sql_sel, $connSel);
	if($result = mysql_fetch_array($res_sel,1))
	{
		$exam_date = $result['exam_date'];

	}
	return $exam_date;

}

function fileupload($file,$path)
{
	global $excelerrormsg;
	$excelFile 		= $file['excelfile'];
	$filename 		= $excelFile['name'];
	$afilename		= explode(".",$filename);
	$renamefile		= date('dmYHis').".".$afilename[1];
	if($afilename[1] == "xls")
	{
		//$renamefile		= chmod($renamefile,0777);
		$filenamepath 	= $path.$renamefile;
		$mimetype 		= $excelFile['type'];
		$filesize 		= $excelFile['size'];
		$fileerror 		= $excelFile['error'];
		if (!$fileerror){

				if(move_uploaded_file($excelFile['tmp_name'],$filenamepath)){
				//if(move_uploaded_file(("$excelFile[tmp_name]", 0777),$filenamepath)){
					$xlsfilename	= $renamefile;

				}else{
					$excelerrormsg = "File upload Error!.";
			}
		}else{
			$excelerrormsg = "File Upload Error. Please try after Some Time.";
		}
	}
	else
	{
		$excelerrormsg = "File Upload Error. Please choose Excel file";
	}

	/*if($errormsg)
	{
		disp_error($errormsg);
		return false;
	}
	else*/
		return $xlsfilename;

}

function getICICIfield()
{
	global $connSel;
	$sql_sel = " SELECT * FROM iib_candidate limit 1";
	//echo $sql_sel;
	$res_sel = mysql_query($sql_sel);
	$length = mysql_num_fields($res_sel);
	for($i=0;$i<$length;$i++)
	{
		//if($i != 0)
			$result = mysql_field_name($res_sel,$i);
			//echo $result;

		//$name = $result[0]." ".$result[1];
		$ICICI_details[] = $result;
	}
		/*echo("<pre>");
		print_r($xat_details);
		echo("</pre>");*/

	return $ICICI_details;
}

function getICICIdetails($mem_no)
{
	global $connSel;
	/*$sql_sel = " SELECT xat_id,DECODE(xat_pass,'$xat_id') as xat_pass,salutation,first_name,middle_name,last_name,date_of_birth,gender,registration_date,married_status,xat_payment_mode,xat_bank_name,
xat_branch_name,xat_other_bank,bank_code,branch_code,date_of_payment,
xat_bulletin_no,amount,dd_issue_date,dd_no,primary_email,alternate_email,
test_city,graduation_status,payment_status,batch_no,admit_card,stream_graduation,ostream_graduation,hq_edu,full_time_exp,total_exp,current_responsibility,institute_applied,step_no,quick_link,check_photo,check_sign,is_checked,checked_date,is_active FROM ".TABLE_XAT_CAND_REGS." WHERE xat_id=$xat_id";*/
	//$sql_sel = " SELECT * FROM ".TABLE_XAT_CAND_REGS." WHERE xat_id=$xat_id";
	$sql_sel = " SELECT * FROM iib_candidate WHERE membership_no='$mem_no'";
	//echo $sql_sel;
	$res_sel = mysql_query($sql_sel);
	//print($res_sel);
	if($result = mysql_fetch_array($res_sel,1))
	{
		//$name = $result[0]." ".$result[1];
		$icicipru_details = $result;
	}
	return $icicipru_details;
}

function getSubject($template_id)
{
	global $connSel;
	$sql_sel = " SELECT mail_subject FROM ".TABLE_XAT_MAIL_TEMPLATE." WHERE id=$template_id";
	$res_sel = mysql_query($sql_sel,$connSel);
	if($result = mysql_fetch_row($res_sel))
	{
		$subject = $result[0];
	}
	return $subject;
}

function getBody($template_id,$xat_details,$atemplate)
{
	global $connSel;
	$sql_sel = " SELECT mail_body FROM ".TABLE_XAT_MAIL_TEMPLATE." WHERE id=$template_id";
	$res_sel = mysql_query($sql_sel,$connSel);
	if($result = mysql_fetch_row($res_sel))
	{
		$body = $result[0];
	}

	if(count($atemplate) > 0)
	{
		foreach($atemplate as $key => $value)
		{
			$body=str_replace("$value",$xat_details[$value],$body);
		}
	}

	return $body;
}

function getSubject_Body($template_id,$xat_details,$atemplate)
{
	global $connSel;
	$sql_sel = " SELECT mail_subject , mail_body FROM iib_mail_template WHERE id=$template_id";
	//echo $sql_sel;
	$res_sel = @mysql_query($sql_sel);
	if(list($mail_subject , $mail_body) = mysql_fetch_row($res_sel))
	{
		$body[0] = $mail_subject;
		$body[1] = $mail_body;
	}
	//print_r($atemplate);
	if(count($atemplate) > 0)
	{
		foreach($atemplate as $key => $value)
		{
			$body[1]=str_replace("$value",$xat_details[$value],$body[1]);
		}
	}
	return $body;
}

function getTotalPaid()
{
	global $connSel;
	$sql_sel_totalPaid = " SELECT COUNT(1) FROM ".TABLE_XAT_CAND_REGS."  WHERE payment_status='Y' AND is_active='Y' ";
	$res_sel_totalPaid = mysql_query($sql_sel_totalPaid,$connSel);
	list($total_paid) = mysql_fetch_row($res_sel_totalPaid);

	return $total_paid;
}

function getXlriTotalPaid()
{
	global $connSel;
	$sql_sel_totalPaid = " SELECT COUNT(1) FROM ".TABLE_XLRI_CAND_REGS."  WHERE payment_status='Y' AND is_active='Y' ";
	$res_sel_totalPaid = @mysql_query($sql_sel_totalPaid,$connSel);
	list($total_paid) = mysql_fetch_row($res_sel_totalPaid);

	return $total_paid;
}

function getXlriTotalNotPaid()
{
	global $connSel;
	$sql_sel_totalPaid = " SELECT COUNT(1) FROM ".TABLE_XLRI_CAND_REGS."  WHERE payment_status='N' AND is_active='Y' ";
	$res_sel_totalPaid = @mysql_query($sql_sel_totalPaid,$connSel);
	list($total_paid) = mysql_fetch_row($res_sel_totalPaid);
	return $total_paid;
}

function getWorkTotalPaid()
{
	global $connSel;
	$sql_sel_totalPaid = " SELECT COUNT(1) FROM ".TABLE_XAT_CAND_REGS."  WHERE payment_status='Y' AND full_time_exp='Y' AND is_active='Y' ";
	$res_sel_totalPaid = mysql_query($sql_sel_totalPaid,$connSel);
	list($total_paid) = mysql_fetch_row($res_sel_totalPaid);

	return $total_paid;
}

$validate1 = '/^[0-9]{1,11}$/i'; //examcode,subjectcode,sectioncode,question_id
$marks = '/^((0\.5)|1|2|3)$/i';//marks
$correctans = '/^[1-5]+/i';
$mcq_case='/^(Y|N)/i';
$type_case='/^(Y)/i';
$type_case_null='/^(N)/i';
$cmarks = '/^(5|10|15|20)$/i';//casemarks
$validation_alpha= '/^[a-zA-Z ]*$/i'; //alphabets
$member_nums='/^[a-z0-9]*$/i';//membernumber
$address='/^[a-z0-9//\]*$/i';




$code_medium='/^(E|H)/i';
$pincode_val='/^[1-9][0-9]{5}$/i';
function rowvalidate($index,$celldata){
	global $validate1,$correctans,$marks,$type_case,$test;
	if ($index == 1){
		 if(trim($celldata) != ""){
			if(preg_match($validate1,$celldata)){
				return true;
			}else{
				return false;
			}
		 }
	}

	if ($index >= 2 && $index <= 5){
			if(preg_match($validate1,$celldata)){
				return true;
			}else{
				return false;
			}
	}

	if ($index >= 6 && $index <= 8){
		 if(trim($celldata) != " "){
				return true;
		 }else{
				return false;
		 }
	}
	if ($index == 12){
		 if(preg_match($correctans,$celldata)){
				return true;
		 }else{
				return false;
		 }
	}
	if($index == 13){
		if(preg_match($marks,$celldata)){
				return true;
		 }else{
				return false;
		 }
	}
	if($index == 14){
		if(preg_match($type_case,$celldata)){
				return true;
		 }else{
				return false;
		 }
	}


	return true;
}//rowvalidate
?>
