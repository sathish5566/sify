<?php
ob_start();
set_time_limit(0);
/****************************************************
* Application Name            :  IIB
* Module Name                 :  Candidate Duration Report
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :
* Tables used for only selects: iib_exam,iib_exam_subjects,iib_candidate_iway,iib_candidate_test,iib_candidate_scores 
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              : CSV file & HTML Page
* Document/Reference Material :
* Created By                  : Bala Murugan.S
* Created ON                  : 17-dec-2009
* Last Modified By            : Bala Murugan.S
* Last Modified Date          : 18-dec-2009
* Description                 : This report displaying the Duration report for the candidates with their related information and also downloaded as csv file.
*****************************************************/
//ini_set("display_errors",'on');
//error_reporting('E_ALL');
 
require_once "sessionchk.php";
require("dbconfig_slave_123.php");
$commondebug=false;
$incompleted =array();
$test_values =array();
$absenties =array();
/*Array to store the result value*/
$flag_result['F']="Fail";
$flag_result['P']="Pass";
$flag_result['--']="--";
$var_upper="upper";
  
/******** General file for excel download      *********/
include_once("excelconfig.php");

$exce_header=array("S.No.","Cafe Id","Membership Number","Start Time","End Time","Total Duration(HRS:MINS)","Score","Result","Extended Time(Minutes)","Attempted Questions Count","Timeloss");
$header_count = count($exce_header);//Header count

//Fetch ExamDates 
$sqlDate = "SELECT distinct trim(exam_date) FROM iib_exam_schedule order by exam_date";
$resDate = @mysql_query($sqlDate);
if($commondebug)
{
	echo $sqlDate;
}

if(!$resDate)
{
	echo "Problem in Fetching Dates".mysql_error ();
	exit;
}
$nDates = @mysql_num_rows($resDate);
if ($nDates > 0)
{
	while (list($eDate) = @mysql_fetch_row($resDate))
	{
		$aDate = explode("-",$eDate);
		$aDispDate[$eDate] = $aDate[2]."-".$aDate[1]."-".$aDate[0];
	}
}

/*Post Variables*/
$examDate = $_POST['exam_date'];
$Exam_Code = $_POST['Exam_code'];
$Subject_code = $_POST['Subject_code'];
$submitted= $_POST['submitted'];

/******** Get values from the Download button  **********/
$ex_flag=$_POST['exl'];

/********* $filename stores the filename of the csv file with unique values ********/
$filename="candidate_duration_(".$examDate.")_examcode_(".$Exam_Code.")_subjcode_(".$Subject_code.")";

/*Fetch Exam Details*/
$select_exam="select trim(b.exam_code),trim(b.exam_name) from iib_exam_subjects a, iib_exam b where a.exam_code=b.exam_code group by a.exam_code order by b.exam_code";
$res_exam=@mysql_query($select_exam);
if($commondebug)
{
	echo $select_exam;
}

if(!$res_exam)
{
	echo "Problem in Fetching Exam Details".mysql_error ();
	exit;
}

/*Fetch Subject Details*/

if($Exam_Code !='')
{
	$select_subj="select trim(subject_code), trim(subject_name) from iib_exam_subjects where exam_code= '$Exam_Code' order by subject_code";	
	$res_subj=@mysql_query($select_subj);
	if($commondebug)
	{
		echo $select_subj;
	}
	
	if(!$res_subj)
	{
		echo "Problem in Fetching Subject Details".mysql_error ();
		exit;
	}
	
}//if($Exam_Code)

if($submitted == "true"){
	if($examDate !='' && $Exam_Code !='' && $Subject_code !='')
	{
		$arrTimeextend=array();
		$arrTimeloss=array();
		$arrQPCount=array();
		$sql1 = "select membership_no,sum(time_extended)as time_extended, sum(timeloss) as timeloss from iib_candidate_test where exam_code='$Exam_Code' and subject_code='$Subject_code' group by membership_no ";
		$res1 = mysql_query($sql1) or die(mysql_error());		
		while($r=mysql_fetch_array($res1))
		{
			$arrTimeextend[$r['membership_no']]=convertHrs($r['time_extended']);
			$arrTimeloss[$r['membership_no']]=$r['timeloss'];
		}
		
		/*$sql2 = "select question_paper_no,count(1) as attemptedcount from iib_question_paper_details where subject_code='$Subject_code' and answer!='' group by question_paper_no ";
		$res2 = mysql_query($sql2) or die(mysql_error());		
		while($r2=mysql_fetch_array($res2))
		{
			$arrQPCount[$r2['question_paper_no']]=$r2['attemptedcount'];
		} */
		$sql_question = "select question_paper_no from iib_question_paper_details where subject_code='$Subject_code' group by question_paper_no ";
                $res_question = mysql_query($sql_question) or die(mysql_error());
                while(list($q_p_no_new) = mysql_fetch_array($res_question)) {
                        $sql2 = "select question_paper_no,count(id) as attemptedcount from iib_response where question_paper_no ='$q_p_no_new' and id in (select  max(id) from iib_response where question_paper_no ='$q_p_no_new' group by question_id) and answer != '' group by question_paper_no";
                        $res2 = mysql_query($sql2) or die(mysql_error());
                        while($r2=mysql_fetch_array($res2)) {
                                $arrQPCount[$r2['question_paper_no']]=$r2['attemptedcount'];
                        }
                }

		
		//Query to Fetch Membership Number according to the input
		$sql_membership_no="select trim(membership_no),$var_upper(trim(centre_code)) from iib_candidate_iway where exam_date='$examDate' and exam_code='$Exam_Code' and subject_code ='$Subject_code'";				
		$res_membership_no=@mysql_query($sql_membership_no);
		if($commondebug)
		{
			echo $sql_membership_no;
		}
		
		if(!$res_membership_no)
		{
			echo "Problem in Fetching Membership List according to selected input".mysql_error ();
			exit;
		}
		$array_values=array();//Array to store the Complete and Incomplete Candidates Record
		$array_values_absenties=array();//Array to store the Absenties Candidates Record		
		$array_values_incomp=array();//Array to store the InComplete Candidates Record		
		$array_values_comp=array();//Array to store the Complete Candidates Record		
		
		/*Initializing of variables*/
		$i=0;
		$comp=0;
		$incomp=0;
		$main_array=array();
		$QPNO='';
		while(list($memno,$centre_code)= @mysql_fetch_row($res_membership_no)){			
						
			/*Selecting time details for the fetched candidates						
			   Here the duration of each candidate is taken for all fetched candidates
			*/									
			$sql_time="select trim(start_time), trim(last_updated_time),TIME_TO_SEC(DATE_FORMAT(trim(last_updated_time),'%T')) - TIME_TO_SEC(DATE_FORMAT(trim(start_time),'%T'))  duration,trim(test_status),question_paper_no from iib_candidate_test where membership_no='$memno' and exam_code='$Exam_Code' and subject_code='$Subject_code' order by last_updated_time asc ";			
			/*if($memno =='500035936'){
					echo $sql_time;
					exit;
			}*/
			$res_time=@mysql_query($sql_time);
			$nCount_test=@mysql_num_rows($res_time);
			if($commondebug)
			{
				echo $sql_time;
			}
			
			if(!$res_time)
			{
				echo "Problem in Fetching Start Time and End Time".mysql_error ();
				exit;
			}
			$main_array[$i][$memno]['centre_code']=$centre_code;
			if(ctype_digit($memno))
				$main_array[$i][$memno]['mem_no']=$memno;
			else
			  $main_array[$i][$memno]['mem_no']="'".$memno;
			$count=0;
			if($nCount_test>0)
			{
				$dur=0;
				$j=0;
				while(list($start_time,$end_time,$duration,$test_status,$question_paper_no)= @mysql_fetch_row($res_time))
				{ 											
						$main_array[$i][$memno]['Time'][$j]['start_time']=$start_time;
						$main_array[$i][$memno]['Time'][$j]['end_time']=$end_time;												
						$dur+=$duration;
						$st=$test_status;
						$j++;	
						$QPNO=$question_paper_no;																						
				}				
				$main_array[$i][$memno]['duration']=convertHrs($dur);			  
				if($st == 'C')
				{															
					
					 $sql_score = "select trim(score),trim(result) from iib_candidate_scores where membership_no='$memno' and exam_code='$Exam_Code' and  subject_code='$Subject_code'";													
				   
				   
					if($commondebug)
					{
						echo $sql_score;
					}
					$res_score=mysql_query($sql_score);
					if(!$res_score)
					{
						echo "Problem in Fetching Result and Score".mysql_error ();
						exit;
					}
					list($score,$result)= mysql_fetch_row($res_score);
					if($score =='')
						$score ='--';
					if($result =='')
						$result ='--';
					else
					  $result=$flag_result[$result];					
					$main_array[$i][$memno]['score']=$score;
					$main_array[$i][$memno]['result']= $result;
					if(array_key_exists($memno,$arrTimeextend))
						$main_array[$i][$memno]['timeextended']= $arrTimeextend[$memno];
					else
						$main_array[$i][$memno]['timeextended']= '--';

										 
					  if(array_key_exists($QPNO,$arrQPCount))
				   		$main_array[$i][$memno]['attemptqpcount']=$arrQPCount[$QPNO];
				      else 
						$main_array[$i][$memno]['attemptqpcount']=0;
					
					if(array_key_exists($memno,$arrTimeloss))
						$main_array[$i][$memno]['timeloss']= $arrTimeloss[$memno];
					else
						$main_array[$i][$memno]['timeloss']= '--';
																	
					array_push($array_values_comp,$main_array[$i][$memno]);															
				}else
				{
				   $main_array[$i][$memno]['score']='--';
				   $main_array[$i][$memno]['result']='--';
				  if(array_key_exists($memno,$arrTimeextend))
						$main_array[$i][$memno]['timeextended']= $arrTimeextend[$memno];
					else
						$main_array[$i][$memno]['timeextended']= '--';	

					
					if(array_key_exists($QPNO,$arrQPCount))
			   			$main_array[$i][$memno]['attemptqpcount']=$arrQPCount[$QPNO];
				    else 
						$main_array[$i][$memno]['attemptqpcount']=0;
					
					if(array_key_exists($memno,$arrTimeloss))
						$main_array[$i][$memno]['timeloss']= $arrTimeloss[$memno];
					else
						$main_array[$i][$memno]['timeloss']= '--';
												
				   array_push($array_values_incomp,$main_array[$i][$memno]);					
				}
				 														
			}else
			{
				$array_values_absenties[$i]['centre_code']=$centre_code;
				if(ctype_digit($memno))
					$array_values_absenties[$i]['mem_no']=$memno;
				else
				  $array_values_absenties[$i]['mem_no']="'".$memno;
				 
				$array_values_absenties[$i]['Time'][0]['start_time']='--';
				$array_values_absenties[$i]['Time'][0]['end_time']='--';
				$array_values_absenties[$i]['duration']='--';
				$array_values_absenties[$i]['score']='--';
				$array_values_absenties[$i]['result']='--';	
				$array_values_absenties[$i]['timeextended']= '--';	
				$array_values_absenties[$i]['attemptqpcount']= '--';	
				$array_values_absenties[$i]['timeloss']= '--';								
				//Absentiess
			}										
			$i++;
		}
				
	$count_scheduledcandidate=count($array_values_absenties)+count($array_values_incomp)+count($array_values_comp);
	$count_compcandidate=count($array_values_comp);
	$count_incompcandidate=count($array_values_incomp);
	$count_abcandidate=count($array_values_absenties);
			
/******* Download the excel file when clicking the download button *************/ 
		if($ex_flag=='1')
		{		 	
		 	 header("Cache-Control: no cache,must-revalidate");
			 header("Content-Type:application/dummy"); 
			 header("Content-Disposition: attachment; filename=$filename.xls");			 		  	 			
			
			echo '<table  width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			<td colspan=2  nowrap background="images/tile.jpg" align="left"><span class="pageTitle">Completed Candidate Details</span></div></td>
		  </tr>
		  <tr>
			<td colspan=2 background="images/tile.jpg" align=center><table width="100%" border="1" cellspacing="0" cellpadding="2">
			  <tr>
				<td width="7%" class="pageTitle">S.No.</td>
				<td width="13%" class="pageTitle">Centre Code </td>
				<td width="16%" class="pageTitle">Membership No </td>
				<td width="35%"><span class="pageTitle">Start Time</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="pageTitle"> End Time</span></td>
				<td width="8%" class="pageTitle">Duration(H:M)</td>
				<td width="9%" class="pageTitle">Score</td>
				<td width="3%" class="pageTitle">Result</td>
				<td width="3%" class="pageTitle">Extended Time(Minutes)</td>
				<td width="3%" class="pageTitle">Attempted Questions Count</td>				
				<td width="3%" class="pageTitle">Time Loss (in sec)</td>				
			  </tr>';          
			  $x=1;
				foreach ($array_values_comp as $k=>$v)
				{			
					echo "<tr align=left>";						 
					echo "<td valign=top class=\"smallText\">$x</td>";
					if(is_array($v))
					{
							foreach($v as $key=>$value)
							{
								if($key=='centre_code') echo "<td valign=top class=\"smallText\">$value</td>"; 
								if($key=='mem_no')  echo "<td valign=top class=\"smallText\">$value</td>";
								if( ($key=='Time') && (is_array($value)))
								{
									echo '<td valign=top class=\"smallText\"><table width="100%" border="0" cellspacing="0" cellpadding="0">';
									foreach($value as  $kt=>$kv)
									{										
										foreach($kv as  $kt1=>$kv1)
										{																		
											if($kt1=='start_time')
											  echo "<tr align=left><td valign=top class=\"smallText\">$kv1</td>";
											if($kt1=='end_time')
											  echo "<td valign=top class=\"smallText\">$kv1</td></tr>";								  																								
										}										
									}							
									echo '</table></td>';														
								}
								if($key=='duration')  echo "<td  valign=top class=\"smallText\">$value</td>";
								if($key=='score')  echo "<td valign=top class=\"smallText\">$value</td>";
								if($key=='result')  echo "<td valign=top class=\"smallText\">$value</td>";
								if($key=='timeextended')  echo "<td valign=top class=\"smallText\">$value</td>";
								if($key=='attemptqpcount')  echo "<td valign=top class=\"smallText\">$value</td>";								
								if($key=='timeloss')  echo "<td valign=top class=\"smallText\">$value</td>";								
																
							}											
					 }			 						  										
					echo "</tr>";
					$x++;		
				}				         
        echo '</table></td>
			  </tr>
			  <tr>
				<td colspan=2 align=left nowrap background="images/tile.jpg"><span class="pageTitle">Incompleted Candidate Details</span></td>
			  </tr>
			  <tr>
				<td colspan=2 background="images/tile.jpg" align=center>
				<table width="100%" border="1" cellspacing="0" cellpadding="2">
				  <tr>
					<td width="7%" class="pageTitle">S.No.</td>
					<td width="13%" class="pageTitle">Centre Code </td>
					<td width="16%" class="pageTitle">Membership No </td>
					<td width="35%"><span class="pageTitle">Start Time</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="pageTitle"> End Time</span></td>
					<td width="8%" class="pageTitle">Duration(H:M)</td>
					<td width="9%" class="pageTitle">Score</td>
					<td width="6%" class="pageTitle">Result</td>
					<td width="3%" class="pageTitle">Extended Time(Minutes)</td>
					<td width="3%" class="pageTitle">Attempted Questions Count</td>	
					<td width="3%" class="pageTitle">Time Loss (in sec)</td>	
				  </tr>';          
					$x=1;
				foreach ($array_values_incomp as $k=>$v)
				{			
					echo "<tr align=left>";						 
					echo "<td valign=top class=\"smallText\">$x</td>";
					if(is_array($v))
					{
							foreach($v as $key=>$value)
							{
								if($key=='centre_code') echo "<td valign=top class=\"smallText\">$value</td>"; 
								if($key=='mem_no')  echo "<td valign=top class=\"smallText\">$value</td>";
								if( ($key=='Time') && (is_array($value)))
								{
									echo '<td  valign=top class=\"smallText\"><table width="100%" border="0" cellspacing="1" cellpadding="1">';
									foreach($value as  $kt=>$kv)
									{
										//echo "<tr>";
										foreach($kv as  $kt1=>$kv1)
										{																		
											if($kt1=='start_time')
											  echo "<tr align=left><td valign=top class=\"smallText\">$kv1</td>";
											if($kt1=='end_time')
											  echo "<td valign=top class=\"smallText\">$kv1</td></tr>";								  																								
										}
										//echo "</tr>"; 	
									}							
									echo '</table></td>';														
								}
								if($key=='duration')  echo "<td valign=top class=\"smallText\">$value</td>";
								if($key=='score')  echo "<td valign=top class=\"smallText\">$value</td>";
								if($key=='result')  echo "<td valign=top class=\"smallText\">$value</td>";
								if($key=='timeextended')  echo "<td valign=top class=\"smallText\">$value</td>";
								if($key=='attemptqpcount')  echo "<td valign=top class=\"smallText\">$value</td>";							
								if($key=='timeloss')  echo "<td valign=top class=\"smallText\">$value</td>";								
							}											
					 }			 						  										
					echo "</tr>";
					$x++;		
				}		
        echo '</table></td>
			  </tr>
			  <tr>
				<td colspan=2 align=left nowrap background="images/tile.jpg"><span class="pageTitle">Absent Candidate Details</span></td>
			  </tr>
			  <tr>
				<td colspan=2 background="images/tile.jpg" align=center>
				<table width="100%" border="1" cellspacing="0" cellpadding="2">
				  <tr>
					<td width="7%" class="pageTitle">S.No.</td>
					<td width="13%" class="pageTitle">Centre Code </td>
					<td width="16%" class="pageTitle">Membership No </td>
					<td width="35%"><span class="pageTitle">Start Time</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="pageTitle"> End Time</span></td>
					<td width="8%" class="pageTitle">Duration(H:M)</td>
					<td width="9%" class="pageTitle">Score</td>
					<td width="6%" class="pageTitle">Result</td>
					<td width="3%" class="pageTitle">Extended Time(Minutes)</td>
					<td width="3%" class="pageTitle">Attempted Questions Count</td>	
					<td width="3%" class="pageTitle">Time Loss (in sec)</td>	
				  </tr>';         
			$x=1;
				foreach ($array_values_absenties as $k=>$v)
				{			
					echo "<tr align=left>";						 
					echo "<td valign=top class=\"smallText\">$x</td>";
					if(is_array($v))
					{
							foreach($v as $key=>$value)
							{
								if($key=='centre_code') echo "<td  valign=top class=\"smallText\">$value</td>"; 
								if($key=='mem_no')  echo "<td valign=top class=\"smallText\">$value</td>";
								if( ($key=='Time') && (is_array($value)))
								{
									echo '<td valign=top class=\"smallText\"><table width="100%" border="0" cellspacing="1" cellpadding="1">';
									foreach($value as  $kt=>$kv)
									{
										//echo "<tr>";
										foreach($kv as  $kt1=>$kv1)
										{																		
											if($kt1=='start_time')
											  echo "<tr align=left><td  valign=top class=\"smallText\">$kv1</td>";
											if($kt1=='end_time')
											  echo "<td valign=top class=\"smallText\">$kv1</td></tr>";								  																								
										}
										//echo "</tr>"; 	
									}							
									echo '</table></td>';														
								}
								if($key=='duration')  echo "<td valign=top class=\"smallText\">$value</td>";
								if($key=='score')  echo "<td valign=top class=\"smallText\">$value</td>";
								if($key=='result')  echo "<td valign=top class=\"smallText\">$value</td>";
								if($key=='timeextended')  echo "<td valign=top class=\"smallText\">$value</td>";
								if($key=='attemptqpcount')  echo "<td valign=top class=\"smallText\">$value</td>";								
								if($key=='timeloss')  echo "<td valign=top class=\"smallText\">$value</td>";							
							}											
					 }			 						  										
					echo "</tr>";
					$x++;		
				}		
        echo '</table>
		</td>
		</tr>
		</table>';									
		exit;	 
	 	}
	}//if($examDate)
}//if($submitted)

function convertHrs($duration_val)
{	
	$dispTimeHrs = floor($duration_val / 3600);			
	if (($duration_val % 3600) == 0)
	{
				$dispTimeHrs = $duration_val / 3600;
				$dispTimeMin = "00";
	}
	else
	{
		$dispTimeMin = floor(($duration_val % 3600) / 60);									
		if ($dispTimeMin == 60)
		{
				$dispTimeHrs = $dispTimeHrs+1;
				$dispTimeMin = "00";
			}
	}
	$dispTime = $dispTimeHrs.":".sprintf('%02d',$dispTimeMin);
	return $dispTime;
}
?>
<HTML>
<HEAD>
<title>Indian Institute Of Banking &amp; Finance</title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
        FONT-FAMILY: Arial, Verdana
}
</STYLE>
<script language='JavaScript' src="./includes/validations.js"></script>

<script language='JavaScript'>
function validate()
{        
		var frm=document.frmdurationreport;        
        frm.exl.value=0;
		frm.submit();
}
function submitForm()
{	
	
        var frm=document.frmdurationreport;        
        if (frm.exam_date.selectedIndex == 0)
		{
			alert("Please select the Exam Date");
			frm.exam_date.focus();
			return false;
		}
		if (frm.Exam_code.selectedIndex == 0)
		{
			alert("Please select the Exam Name");
			frm.Exam_code.focus();
			return false;
		}
		if (frm.Subject_code.selectedIndex == 0)
		{
			alert("Please select the Subject Name");
			frm.Subject_code.focus();
			return false;
		}
		frm.submitted.value='true';
		frm.exl.value='0';
        frm.submit();
}

/* Function for excel download */

function excelsubmit()
{
	var frmname=document.frmdurationreport;
	frmname.submitted.value='true';
	frmname.exl.value='1';
	frmname.submit();
}
</script>


<link href="./includes/stylesheet.css" rel="stylesheet" type="text/css">
</HEAD>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0 >
<center>
<form name='frmdurationreport' method='post'>
<input type='hidden' name='submitted'>
<input type=hidden name='exl' value='0'>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780 background=images/tile.jpg>
<TR><td width="780"><?include("includes/header.php");?></td>
</TR>
<tr>
	<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>
</tr>
<TR>
	<TD background=images/tile.jpg vAlign=top width=780 align=center><table width=100% cellpadding=0 cellspacing=5 border=0 >
      <tr>
        <td colspan=2 align="center"><b><font face="Verdana, Arial, Helvetica, sans-serif" size=2>Candidate Duration Report</font></b><br>
            <br></td>
      </tr>
      <tr>
        <td align="right" width=319 class="greybluetext10" valign=top>Exam Date : </td>
        <td width="446" align="left" valign=top class="greybluetext10"><select name='exam_date' class="greybluetext10" style={width:150px} >
            <option value=''>--Select--</option>
            <?php
            				foreach ($aDispDate as $key=>$val)
            				{
	            				print("<option value='$key' ");
	            				if ($examDate == $key)
	            				{
		            				print(" selected ");
	            				}
	            				print(">$val</option>");
            				}
            			?>
          </select>        </td>
      </tr>
      <tr>
        <td align="right" width=319 class="greybluetext10" valign=top>Exam Name : </td>
        <td align="left" valign=top class="greybluetext10"><select name='Exam_code' class="greybluetext10" onChange='javascript:validate();' style={width:250px} >
            <option value=''>--Select--</option>
            <?php
            				while(list($exam_code,$exam_name)=mysql_fetch_row($res_exam))
			                {
			                 if($exam_code==$_POST['Exam_code'])
			                       echo "<option value=$exam_code selected>$exam_code-->$exam_name</option>";
			                 else
			                       echo "<option value=$exam_code>$exam_code-->$exam_name</option>";
			                }
            			?>
          </select>        </td>
      </tr>
      <tr>
        <td align="right" width=319 class="greybluetext10" valign=top>Subject Name : </td>
        <td align="left" valign=top class="greybluetext10"><select name='Subject_code' class="greybluetext10"  style={width:250px} >
            <option value=''>--Select--</option>
            <?php
            				while(list($subject_code,$subject_name)=mysql_fetch_row($res_subj))
			                {
			                 if($subject_code==$Subject_code)
			                       echo "<option value=$subject_code selected>$subject_code-->$subject_name</option>";
			                 else
			                       echo "<option value=$subject_code>$subject_code-->$subject_name</option>";
			                }
            			?>
          </select>        </td>
      </tr>
      <tr>
        <td colspan=2 align="center"><input type='button' name='sub_assign' value='Submit' class='button' onClick='javascript:submitForm()'>        </td>
      </tr>
    <?PHP  if ($submitted=='true')
	{
	?>
	  <tr>
        <td colspan=2><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="37%" class="mainText">Total No.Of Candidates Scheduled : </td>
            <td width="63%"><?PHP echo $count_scheduledcandidate?></td>
          </tr>
          <tr>
            <td class="mainText">Total No.Of Candidates Completed: </td>
            <td><?PHP echo $count_compcandidate?></td>
          </tr>
          <tr>
            <td class="mainText">Total No.Of Candidates In Completed:</td>
            <td><?PHP echo $count_incompcandidate?></td>
          </tr>
          <tr>
            <td class="mainText">Total No.Of Candidates Absent : </td>
            <td><?PHP echo $count_abcandidate?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan=2 align=center nowrap background="images/tile.jpg"><div align="left"><span class="pageTitle">Completed Candidate Details</span></div></td>
      </tr>
      <tr>
        <td colspan=2 background="images/tile.jpg" align=center><table width="100%" border="1" cellspacing="0" cellpadding="2">
          <tr >
            <td width="7%" class="pageTitle">S.No.</td>
            <td width="13%" class="pageTitle">Centre Code </td>
            <td width="16%" class="pageTitle">Membership No </td>
            <td width="35%"><span class="pageTitle">Start Time</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="pageTitle"> End Time</span></td>
            <td width="8%" class="pageTitle">Duration(H:M)</td>
            <td width="9%" class="pageTitle">Score</td>
            <td width="3%" class="pageTitle">Result</td>
			<td width="3%" class="pageTitle">Extended Time(Minutes)</td>
			 <td width="3%" class="pageTitle">Attempted Questions Count</td>
			 <td width="3%" class="pageTitle">Time Loss (in sec)</td>
          </tr>
          
          <?PHP  
		$x=1;
		foreach ($array_values_comp as $k=>$v)
		{			
			echo "<tr>";						 
			echo "<td  valign=top class=\"smallText\">$x</td>";
			if(is_array($v))
			{
					foreach($v as $key=>$value)
					{
						if($key=='centre_code') echo "<td valign=top class=\"smallText\">$value</td>"; 
						if($key=='mem_no')  echo "<td valign=top class=\"smallText\">$value</td>";
						if( ($key=='Time') && (is_array($value)))
						{
							echo '<td  valign=top class=\"smallText\"><table width="100%" border="0" cellspacing="1" cellpadding="1">';
     						foreach($value as  $kt=>$kv)
							{
								//echo "<tr>";
								foreach($kv as  $kt1=>$kv1)
								{																		
									if($kt1=='start_time')
									  echo "<tr><td valign=top class=\"smallText\">$kv1</td>";
									if($kt1=='end_time')
									  echo "<td valign=top class=\"smallText\">$kv1</td></tr>";								  																								
								}
								//echo "</tr>"; 	
							}							
							echo '</table></td>';														
						}
						if($key=='duration')  echo "<td valign=top class=\"smallText\">$value</td>";
						if($key=='score')  echo "<td valign=top class=\"smallText\">$value</td>";
						if($key=='result')  echo "<td valign=top class=\"smallText\">$value</td>";
						if($key=='timeextended')  echo "<td valign=top class=\"smallText\">$value</td>";
						if($key=='attemptqpcount')  echo "<td valign=top class=\"smallText\">$value</td>";						
						if($key=='timeloss')  echo "<td valign=top class=\"smallText\">$value</td>";						
					}											
			 }			 						  										
			echo "</tr>";
			$x++;		
		}
		?>
         
        </table></td>
      </tr>
      <tr>
        <td colspan=2 align=center nowrap background="images/tile.jpg"><div align="left"><span class="pageTitle">Incompleted Candidate Details</span></div></td>
      </tr>
      <tr>
        <td colspan=2 background="images/tile.jpg" align=center>
		<table width="100%" border="1" cellspacing="0" cellpadding="2">
          <tr>
            <td width="7%" class="pageTitle">S.No.</td>
            <td width="13%" class="pageTitle">Centre Code </td>
            <td width="16%" class="pageTitle">Membership No </td>
            <td width="35%"><span class="pageTitle">Start Time</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="pageTitle"> End Time</span></td>
            <td width="8%" class="pageTitle">Duration(H:M)</td>
            <td width="9%" class="pageTitle">Score</td>
			<td width="3%" class="pageTitle">Result</td>
			<td width="3%" class="pageTitle">Extended Time(Minutes)</td>
			 <td width="3%" class="pageTitle">Attempted Questions Count</td>
			 <td width="3%" class="pageTitle">Time Loss (in sec)</td>
          </tr>          
          <?PHP  
		$x=1;
		foreach ($array_values_incomp as $k=>$v)
		{			
			echo "<tr>";						 
			echo "<td valign=top class=\"smallText\">$x</td>";
			if(is_array($v))
			{
					foreach($v as $key=>$value)
					{
						if($key=='centre_code') echo "<td valign=top class=\"smallText\">$value</td>"; 
						if($key=='mem_no')  echo "<td valign=top class=\"smallText\">$value</td>";
						if( ($key=='Time') && (is_array($value)))
						{
							echo '<td valign=top class=\"smallText\"><table width="100%" border="0" cellspacing="1" cellpadding="1">';
     						foreach($value as  $kt=>$kv)
							{
								//echo "<tr>";
								foreach($kv as  $kt1=>$kv1)
								{																		
									if($kt1=='start_time')
									  echo "<tr><td valign=top class=\"smallText\">$kv1</td>";
									if($kt1=='end_time')
									  echo "<td valign=top class=\"smallText\">$kv1</td></tr>";								  																								
								}
								//echo "</tr>"; 	
							}							
							echo '</table></td>';														
						}
						if($key=='duration')  echo "<td valign=top class=\"smallText\">$value</td>";
						if($key=='score')  echo "<td valign=top class=\"smallText\">$value</td>";
						if($key=='result')  echo "<td valign=top class=\"smallText\">$value</td>";
						if($key=='timeextended')  echo "<td valign=top class=\"smallText\">$value</td>";
						if($key=='attemptqpcount')  echo "<td valign=top class=\"smallText\">$value</td>";						
						if($key=='timeloss')  echo "<td valign=top class=\"smallText\">$value</td>";							
					}											
			 }			 						  										
			echo "</tr>";
			$x++;		
		}
		?>
        </table>		</td>
      </tr>
      <tr>
        <td colspan=2 align=center nowrap background="images/tile.jpg"><div align="left"><span class="pageTitle">Absent Candidate Details</span></div></td>
      </tr>
      <tr>
        <td colspan=2 background="images/tile.jpg" align=center>
		<table width="100%" border="1" cellspacing="0" cellpadding="2">
          <tr>
            <td width="7%" class="pageTitle">S.No.</td>
            <td width="13%" class="pageTitle">Centre Code </td>
            <td width="16%" class="pageTitle">Membership No </td>
            <td width="35%"><span class="pageTitle">Start Time</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="pageTitle"> End Time</span></td>
            <td width="8%" class="pageTitle">Duration(H:M)</td>
            <td width="9%" class="pageTitle">Score</td>
           <td width="3%" class="pageTitle">Result</td>
		   <td width="3%" class="pageTitle">Extended Time(Minutes)</td>
		   <td width="3%" class="pageTitle">Attempted Questions Count</td>
			 <td width="3%" class="pageTitle">Time Loss (in sec)</td>
		   
          </tr>
          
          <?PHP  
		$x=1;
		foreach ($array_values_absenties as $k=>$v)
		{			
			echo "<tr>";						 
			echo "<td class=\"smallText\">$x</td>";
			if(is_array($v))
			{
					foreach($v as $key=>$value)
					{
						if($key=='centre_code') echo "<td valign=top class=\"smallText\">$value</td>"; 
						if($key=='mem_no')  echo "<td valign=top class=\"smallText\">$value</td>";
						if( ($key=='Time') && (is_array($value)))
						{
							echo '<td class=\"smallText\"><table width="100%" border="0" cellspacing="1" cellpadding="1">';
     						foreach($value as  $kt=>$kv)
							{
								//echo "<tr>";
								foreach($kv as  $kt1=>$kv1)
								{																		
									if($kt1=='start_time')
									  echo "<tr><td valign=top class=\"smallText\">$kv1</td>";
									if($kt1=='end_time')
									  echo "<td valign=top class=\"smallText\">$kv1</td></tr>";								  																								
								}
								//echo "</tr>"; 	
							}							
							echo '</table></td>';														
						}
						if($key=='duration')  echo "<td valign=top class=\"smallText\">$value</td>";
						if($key=='score')  echo "<td  valign=top class=\"smallText\">$value</td>";
						if($key=='result')  echo "<td valign=top class=\"smallText\">$value</td>";	
						if($key=='timeextended')  echo "<td valign=top class=\"smallText\">$value</td>";
						if($key=='attemptqpcount')  echo "<td valign=top class=\"smallText\">$value</td>";							
						if($key=='timeloss')  echo "<td valign=top class=\"smallText\">$value</td>";					
					}											
			 }			 						  										
			echo "</tr>";
			$x++;		
		}
		?>
        </table>		</td>
      </tr>
      <tr>
        <td colspan=2 background="images/tile.jpg" align=center>&nbsp;</td>
      </tr>
      <tr>
        <td class="greybluetext10" colspan=2><b>Note :</b></td>
      </tr>
      <tr>
        <td class="greybluetext10" colspan=2>&nbsp;&nbsp;<b>List of candidates mapped to the selected date will be displayed </b></td>
      </tr>
      <tr>
        <td class="greybluetext10" colspan=2>&nbsp;&nbsp;<b>Absentees will be displayed without Time,Score and Result </b></td>
      </tr>
      <tr>
        <td class="greybluetext10" colspan=2>&nbsp;&nbsp;<b>Incomplete Candidates will not get Score and Result </b></td>
      </tr>
      <tr>
        <td class="greybluetext10" colspan=2>&nbsp;&nbsp;<b>Time extension given candidates duration is not accurate. </b></td>
      </tr>      
    </table></TD>
</TR>  
<tr><td class=greybluetext10 background=images/tile.jpg>&nbsp;</td></tr> 
<tr><td class=greybluetext10 background=images/tile.jpg align="center" ><input type='button' name='download'  class="button" value='Download' onClick='javascript:excelsubmit()'></td></tr>
<tr><td class=greybluetext10 background=images/tile.jpg>&nbsp;</td></tr>

<?
}?>

</table>
</form>
</center>
</body>
</head>
</html>
<?
//ob_end_flush(); 
?>
