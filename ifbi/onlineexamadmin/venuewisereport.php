<?php
require("dbconfig.php");
$examCode = 2;
$examName = "DBF";
$examCentreCode = "610";
$examCentreName = "NEWDELHI";
$currentDate = date("d/m/Y");
$fp = fopen("/tmp/venuewisereports/dvenuewise_".$examCentreCode."_".$examCentreName.".txt","wb");
fwrite($fp,"\n\n");
fwrite($fp,"\t\tThe Indian Institute of Banking and Finance\n\n");
fwrite($fp,"\t\tCentre,Venue,Batch wise Allocation - online exam (".$examName.") \n\n");
$str = "";
$str = "Date : ".$currentDate."\t\t	Page : 1\n\n";
$str .= "Centre Code : ".$examCentreCode."\n";
$str .= "Centre Name : ".$examCentreName."\n\n";
fwrite($fp, $str);

//$aCount = array("71"=>0,"72"=>0,"73"=>0,"74"=>0,"75"=>0,"76"=>0);
$aCount = array("1"=>0,"2"=>0,"3"=>0,"4"=>0);
$aCentre = array();

$sqli = "SELECT DISTINCT a.centre_code FROM  iib_candidate_iway a, iib_candidate b  ".
		" WHERE a.membership_no=b.membership_no AND b.exam_centre_code='$examCentreCode' ".
		" AND a.exam_code='$examCode' ORDER BY a.centre_code ";
print $sqli;		
$resi = mysql_query($sqli);
while (list($centreCode) = mysql_fetch_row($resi))
{
	$sqlIWay = "SELECT iway_name,iway_address1, iway_address2, iway_city, iway_state, iway_pin_code FROM iib_iway_details ".
						" WHERE centre_code='$centreCode'";				
	$resIWay = @mysql_query($sqlIWay) or die("select from iib_iway_details failed ".mysql_error());
	$row = mysql_fetch_array($resIWay);
	$iwayAddress = "";

	$row['iway_name'] != "" ? $iwayAddress .= " ".str_replace("\n","",$row['iway_name']) : $iwayAddress .= "";
	$row['iway_address1'] != "" ? $iwayAddress .= " ".str_replace("\n","",$row['iway_address1']) : $iwayAddress .= "";
	$row['iway_address2'] != "" ? $iwayAddress .= " ".str_replace("\n","",$row['iway_address2']) : $iwayAddress .= "";
	$row['iway_city'] != "" ? $iwayAddress .= " ".str_replace("\n","",$row['iway_city']) : $iwayAddress .= "";
	$row['iway_pin_code'] != "" ? $iwayAddress .= " ".str_replace("\n","",$row['iway_pin_code']) : $iwayAddress .= "";
	$row['iway_state'] != "" ? $iwayAddress .= " ".str_replace("\n","",$row['iway_state']) : $iwayAddress .= "";	
	$aCentre[$centreCode] = $iwayAddress;
}

foreach ($aCentre as $centreCode=>$iwayAddress)
{
	fwrite($fp, "\nVenue Name/Address : ".$iwayAddress."\n\n");
	$str = "Sub cd\tDate/Time\tMember No.\n\n";
	fwrite($fp, $str);
	$sql = " SELECT DISTINCT a.membership_no,a.subject_code,a.exam_date,a.exam_time ".
	" FROM iib_candidate_iway a, iib_candidate b ".
	" WHERE a.membership_no=b.membership_no AND a.centre_code='$centreCode' ".
	" AND a.exam_code='$examCode'  AND b.exam_centre_code='$examCentreCode' ".
	" ORDER BY a.subject_code,a.exam_date,a.exam_time,a.membership_no ";
	print $sql;
	$res = mysql_query($sql);

	$seqNo = 0;
	
	$prevSubjectCode =  "";
	$prevExamDate = "";
	$prevExamTime = "";
	while (list($membershipNo, $subjectCode, $examDate, $examTime) = mysql_fetch_row($res))
	{	
		$aCount[$subjectCode] = $aCount[$subjectCode]+1;
		$dispTime = substr($examTime,0,5);
		$aDate = explode("-",$examDate);
		$dispDate = $aDate[2]."/".$aDate[1]."/".substr($aDate[0],2,2);
		if ($prevExamTime != $examTime)
		{
			$prevExamTime = $examTime;								
			$strTime = $dispTime;			
		}
		else
		{
			if ($prevExamDate != $examDate)
			{
				$strTime = $dispTime;
			}	
			else
			{
				$strTime = fillSpace(strlen($dispTime));		
			}
		}		
		if ($prevExamDate != $examDate)
		{
			$prevExamDate = $examDate;
			$strDate = $dispDate;			
		}
		else
		{
			if ($prevSubjectCode != $subjectCode)
			{
				$strDate = $dispDate;			
			}
			else
			{
				$strDate = fillSpace(strlen($dispDate));
			}
		}
		if ($prevSubjectCode != $subjectCode)
		{					
			$prevSubjectCode = $subjectCode;
			$strSubject = $subjectCode;
		}
		else
		{
			$strSubject = fillSpace(strlen($subjectCode));
		}
					
		// 71     18/01/03 08:30		xxxxxxxxx
		$str = $strSubject."\t".$strDate." ".$strTime."\t".$membershipNo."\n";
		fwrite($fp, $str);
	}
}
fwrite($fp,"\nSummary :\n\n");
$sqlTotal = "SELECT count(distinct a.membership_no) FROM iib_candidate_iway a, iib_candidate b ".
	"	WHERE b.exam_centre_code='$examCentreCode' AND a.exam_code='$examCode' ".
	" and a.membership_no=b.membership_no";
print $sqlTotal;	
$resTotal = mysql_query($sqlTotal);
list($total) = mysql_fetch_row($resTotal);
$str = "Total Members : ".$total."\n";
foreach ($aCount as $subjectCode =>$nCount)
{	
	$str .= "Total for Subject ".$subjectCode." :  ".$nCount."\n";
}
fwrite($fp, $str);
fclose($fp);

function fillSpace($len)
{
	$str = "";
	for ($i=0; $i < $len; $i++)
		$str .=" ";
	return $str;
}
?>