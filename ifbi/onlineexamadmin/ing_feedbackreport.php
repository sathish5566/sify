<?php
$admin_flag=2;
require("dbconfig.php");
require("sessionchk.php");
set_time_limit(0);

$examDate = $_POST['exam_date'];
$sqlDate = "SELECT distinct exam_date FROM iib_candidate_iway order by exam_date ";
$resDate = mysql_query($sqlDate);

$arrYesNo = array('Y'=>'Yes','N'=>'No');
$arrAbtQns = array('A'=>'Easy','B'=>'Difficult','C'=>'Not relevant','D'=>'Can\'t Say');

?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>BODY {
	FONT-FAMILY: Arial, Verdana	
}
.textblk11     { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; text-decoration: none}
</STYLE>
</head>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0>
<form name='frm' method='post'>
<center>

<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
	<tr><td width="780"><?include("includes/header.php");?></td></tr>
	<tr>
		<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>	
	<TR>
	    <TD background=images/tile.jpg vAlign=top width=780 align=center>

<table width=100% border=0 cellspacing=0 cellpadding=5>
	<tr><td align=center><div align=center><b>Feedback Report</b></div></td></tr>
	<?php
if (mysql_num_rows($resDate) > 0)
{

	?>
	<tr>
		<td align=center class=textblk11>Exam Date : 
			<select name='exam_date' style={width:150px} class=textblk11>
				<option value=''>--Select--</option>
<?php
	while (list($eDate) = mysql_fetch_row($resDate))
	{
		$aDate = explode("-",$eDate);
		$dispDate = $aDate[2]."-".$aDate[1]."-".$aDate[0];
		print("<option value='$eDate' ");
		if ($eDate == $examDate)
			print(" selected ");
		print(">$dispDate</option>");
	}

?>
		</td>
	</tr>
	<tr>
		<td align=center><input class='button' type='button' name='sub' value='Submit' onClick='javascript:submitForm()'></td>
	</tr>
</select>
<?php
}else{
	print("<tr><td ><div align=center class=textblk11>No feedbacks found</div></td></tr>");
}
?>
</table>
</TD>
	</TR>
<script language='JavaScript'>
function submitForm()
{
	if (document.frm.exam_date.selectedIndex == 0)
	{
		alert("Please select the exam date");
		document.frm.exam_date.focus();
		return false;
	}
	var url="ing_feedbackreport.php";
	document.frm.action=url;	
	document.frm.submit();
}
function downloadReport()
{
	var url="download_ing_feedback.php";
	document.frm.action=url;
	document.frm.submit();
}
</script>
<?php
if ($examDate != "")
{
	print "<tr><td>";
	$aDates = explode("-", $examDate);
	$dispDate = $aDates[2]."-".$aDates[1]."-".$aDates[0];
	$sql = "select a. membership_no, c. name, b. question1, b. question2, b. question3, b. question4, b. question5, b. question6, b. question7, b. question8 from iib_candidate_iway a, iib_ing_feedback b, iib_candidate c where a.exam_date='$examDate' and a. membership_no=b. membership_no and a. membership_no=c. membership_no and a. exam_code=b. exam_code and a. subject_code=b. subject_code and a.exam_date='$examDate' order by a.membership_no";
	$res = mysql_query($sql);
	//print $sql;
	$nRows = mysql_num_rows($res);
	if ($nRows > 0)
	{
		print("<table width=100% border=1 cellspacing=1 cellpadding=1>");
		print("<tr><td class=textblk11>&nbsp;</td><td class=textblk11>&nbsp;</td><td class=textblk11>&nbsp;</td><td class=textblk11>&nbsp;</td>");
		print("<td class=textblk11 colspan='3' align='center' valign='top'><b>Did you face any issue in</b></td>");
		print("<td class=textblk11>&nbsp;</td><td class=textblk11>&nbsp;</td><td class=textblk11>&nbsp;</td><td class=textblk11>&nbsp;</td><td class=textblk11>&nbsp;</td></tr>");
		print("<tr><td class=textblk11 valign='top'><b>Membership No</b></td>");
		print("<td  class=textblk11 valign='top'><b>Name</b></td>");
		print("<td  class=textblk11 valign='top'><b>Did you have enough adequate time to complete all questions?</b></td>");
		print("<td  class=textblk11 valign='top'><b>Is the venue facilities where you are sitting satisfactory?</b></td>");
		print("<td  class=textblk11 valign='top'><b>Login / Password and registration no</b></td>");
		print("<td  class=textblk11 valign='top'><b>Navigating the screen to next questions</b></td>");
		print("<td  class=textblk11 valign='top'><b>Any other technical issues</b></td>");
	//	print("<td  class=textblk11 valign='top'><b>Were you able to see your marks at the end of the online test</b></td>");
		print("<td  class=textblk11 valign='top'><b>How were the questions in Part A (General Banking) </b></td>");
		print("<td  class=textblk11 valign='top'><b>How were the questions in Part B (Aptitude) </b></td>");
		print("<td  class=textblk11 valign='top'><b>Overall how would you rate the online test methodology (1-Poor; 5-Excellant)</b></td>");
		print("<td  class=textblk11 valign='top'><b>Any other comments</b></td></tr>");
		while (list($membershipNo, $candName, $qn1, $qn2, $qn3, $qn4, $qn5, $qn6, $qn7, $qn8) = mysql_fetch_row($res))
		{		
			print("<tr>");	
			print("<td class=textblk11 valign='top'>$membershipNo</td>");
			print("<td class=textblk11 valign='top'>$candName</td>");
			print("<td class=textblk11 valign='top'>$arrYesNo[$qn1]</td>");	
			print("<td class=textblk11 valign='top'>$arrYesNo[$qn2]</td>");
			$arrQn3 = explode("|",$qn3);
			if(is_array($arrQn3) && count($arrQn3) > 0)
			{
				foreach($arrQn3 as $val)
				{
					print("<td class=textblk11 valign='top'>$arrYesNo[$val]</td>");
				}
			}else{
				print("<td class=textblk11 valign='top'>No</td>");
				print("<td class=textblk11 valign='top'>No</td>");
				print("<td class=textblk11 valign='top'>No</td>");
			}
			//print("<td class=textblk11 valign='top'>$arrYesNo[$qn4]</td>");
			print("<td class=textblk11 valign='top'>$arrAbtQns[$qn5]</td>");
			print("<td class=textblk11 valign='top'>$arrAbtQns[$qn6]</td>");
			print("<td class=textblk11 valign='top'>$qn7</td>");
			$feedback = stripslashes($qn8);
			if ($feedback == "")
				print("<td class=textblk11>&nbsp;</td>");
			else
				print("<td class=textblk11 valign='top'>".substr($feedback,0,20)."...</td>");
			print("</tr>");
		}
		print("</table><br><br>");
		print("</TD>");
		print("</TR>");
		print("<tr><td align='center'><input class='button' type='button' name='download' value='Download' onClick='javascript:downloadReport()'></td></tr>");
	}		
	else
	{
		print("<br><div align=center class=textblk11>No records found</div><br><br>");
		print("</TD>");
		print("</TR>");
	}
?>
		
<?php
}
	?>
	<tr><td>&nbsp;</td></tr>
	<TR>
		<?include("includes/footer.php");?>
	</TR>
</TABLE>
</center>
</form>
</BODY>
</HTML>