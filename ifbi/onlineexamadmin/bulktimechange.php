<?php
/****************************************************
* Application Name            :  IIB& IBPS
* Module Name                 :  Tiem change option
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :
* Tables used for only selects:  -
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              :  -
* Document/Reference Material :
* Created By                  : BalaMurugan.S
* Created ON                  : 25-06-2010
* Last Modified By            :  
* Last Modified Date          :  
* Description                 : Bulk time change 
*****************************************************/
@session_start();
$admin_flag=0;
require("dbconfig_slave_123.php");
require("sessionchk.php");
set_time_limit(0);
if( isset($_GET['edate']) && $_GET['edate']!='' && isset($_GET['loc']) && $_GET['loc']!='' )
{	$arr=array();
	$edate = mysql_escape_string($_GET['edate']);
	$loc = mysql_escape_string($_GET['loc']);			
	if($loc==0)
		$sqlcentre = " select centre_code from iib_iway_details order by centre_code";
	else
	  $sqlcentre = " select centre_code from iib_iway_details where exam_centre_code='$loc' order by centre_code";
	$rescentre = mysql_query($sqlcentre);
	$rescentrerows = mysql_num_rows($rescentre);
	if($rescentrerows > 0){
		array_push($arr,0);		
		while($rec = mysql_fetch_array($rescentre)){            		         
		   array_push($arr,$rec[0]);		   		
		}
	}	
	echo implode(",",$arr);	
	exit;
}

if(isset($_POST['sub']) && $_POST['submitType']==1)
{
	require("dbconfig.php");
	$rows=0;
	$hNewTime=mysql_escape_string($_POST['hNewTime']);
	$hReason=mysql_escape_string($_POST['hReason']);	
	if(isset($_POST['chkarrmem']) &&  $_POST['chkarrmem']!=''){		
		$arrMEM=$_POST['chkarrmem'];		
		foreach ($arrMEM as $mem){
			$arrdet=explode("_",$mem);								
			 $sql_insert = "insert into iib_time_change_logs (membership_no,exam_code,subject_code,exam_date,centre_code,old_exam_time,new_exam_time,reason) 
			select membership_no,exam_code,subject_code,exam_date,centre_code,exam_time,'$hNewTime','$hReason' from iib_candidate_iway where membership_no ='".$arrdet[0]."' and exam_code = '".$arrdet[1]."' and subject_code = '".$arrdet[2]."'";
			$afrows=mysql_query($sql_insert,$connect);			
			if(mysql_affected_rows($connect)){			
			 $sql_upd = "update iib_candidate_iway set exam_time = '$hNewTime' where membership_no ='".$arrdet[0]."' and exam_code = '".$arrdet[1]."' and subject_code = '".$arrdet[2]."'";						
				if(mysql_query($sql_upd,$connect))
					$rows++;							
			}					
				
		}	
		
	}
}
if(isset($_POST['sub']))
{
	$edate = mysql_escape_string($_POST['selexam_date']);
	$loc = mysql_escape_string($_POST['sellocation']);
	$centre = mysql_escape_string($_POST['selcentre']);
	if($centre==0)
		$qry_track="select membership_no,updated_time from iib_candidate_tracking group by membership_no,updated_time desc";
	else
		$qry_track="select membership_no,updated_time from iib_candidate_tracking  where cafe_id='$centre' group by membership_no,updated_time desc";
	$arrTrack=array();
	$rec='';
	$restrack = mysql_query($qry_track);
	$restrackrows = mysql_num_rows($restrack);
	if($restrackrows > 0){			
		while($rec = mysql_fetch_array($restrack)){            		         
		   if(!in_array($rec[0],$arrTrack))
				array_push($arrTrack,$rec[0]);						   	
		}
	}
	if($centre != 0)
	{
		$iway_qry="select A.membership_no as memno,A.centre_code,A.exam_code,A.subject_code,A.exam_time,A.exam_date,B.membership_no from iib_candidate_iway A LEFT JOIN iib_candidate_test B ON A.membership_no=B.membership_no and A.exam_code=B.exam_code and A.subject_code=B.subject_code and B.current_session='Y' where B.membership_no is NULL and A.exam_date='$edate' and A.centre_code='$centre';";		
	}else if($loc != 0)
	{
		$iway_qry="select A.membership_no as memno,A.centre_code,A.exam_code,A.subject_code,A.exam_time,A.exam_date,B.membership_no from iib_candidate_iway A LEFT JOIN iib_candidate_test B ON A.membership_no=B.membership_no and A.exam_code=B.exam_code and A.subject_code=B.subject_code and B.current_session='Y' where B.membership_no is NULL and A.exam_date='$edate' and A.centre_code IN (select centre_code from iib_iway_details where exam_centre_code='$loc') order by A.centre_code";		
	}else
	{
		$iway_qry="select A.membership_no as memno,A.centre_code,A.exam_code,A.subject_code,A.exam_time,A.exam_date,B.membership_no from iib_candidate_iway A LEFT JOIN iib_candidate_test B ON A.membership_no=B.membership_no and A.exam_code=B.exam_code and A.subject_code=B.subject_code and B.current_session='Y' where  B.membership_no is NULL and  A.exam_date='$edate'";		
	}		
	$resiway = mysql_query($iway_qry);
	$resiwayrows = mysql_num_rows($resiway);
}

$sqlDate = "SELECT distinct exam_date FROM iib_exam_schedule order by exam_date ";
$resDate = mysql_query($sqlDate);

$sqlloc = "SELECT exam_centre_code,exam_centre_name from  iib_exam_centres order by exam_centre_name";
$resloc = mysql_query($sqlloc);

if($loc != 0)
	$sqlcentre = "select centre_code from iib_iway_details where exam_centre_code='$loc' order by centre_code"; 
else
  $sqlcentre = "select centre_code from iib_iway_details order by centre_code"; 
$rescentre = mysql_query($sqlcentre);



?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<STYLE>
BODY {
	FONT-FAMILY: Arial, Verdana	
}
.textblk11  { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; text-decoration: none}
</STYLE>
<script language="javascript" type="text/javascript">
function trim(b){var a=b;var c=/^(\s*)([\W\w]*)(\b\s*$)/;if(c.test(a)){a=a.replace(c,"$2")}var c=/ +/g;a=a.replace(c," ");if(a==" "){a=""}return a}
function isObject( what )
{
		return (typeof what == 'object');
}

function explode (delimiter, string, limit) {
    var emptyArray = { 0: '' };
    
    // third argument is not required
    if ( arguments.length < 2 ||
        typeof arguments[0] == 'undefined' ||
        typeof arguments[1] == 'undefined' ) {
        return null;
    }
 
    if ( delimiter === '' ||
        delimiter === false ||
        delimiter === null ) {
        return false;
    }
 
    if ( typeof delimiter == 'function' ||
        typeof delimiter == 'object' ||
        typeof string == 'function' ||
        typeof string == 'object' ) {
        return emptyArray;
    }
 
    if ( delimiter === true ) {
        delimiter = '1';
    }
    
    if (!limit) {
        return string.toString().split(delimiter.toString());
    } else {
        // support for limit argument
        var splitted = string.toString().split(delimiter.toString());
        var partA = splitted.splice(0, limit - 1);
        var partB = splitted.join(delimiter.toString());
        partA.push(partB);
        return partA;
    }
}
var xmlHttp = false;
function createAjaxRequestObject()
{
	try
	{
		xmlHttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
	}
	catch(e)
	{
		alert('Sorry, but your browser doesn\'t support XMLHttpRequest.');
	}
	return xmlHttp;
}
function submitForm()
{ 	
	var submitType=document.getElementById('submitType').value;
	if(submitType == 0 ){
		if (document.getElementById('selexam_date').selectedIndex == 0){
			alert("Please select the exam date");
			document.getElementById('selexam_date').focus();
			return false;
		}else
		 return true;
	}else if(submitType == 1 ){ 	 		
		
		if (document.getElementById('selexam_date').selectedIndex == 0){
			alert("Please select the exam date");
			document.getElementById('selexam_date').focus();
			return false;
		}		
		var frm	=document.frmtimchange;
		if (frm.sHours.selectedIndex == 0){
			alert ("Please select Hour");
			frm.sHours.focus();
			return false;
		}
		if (frm.sMinutes.selectedIndex == 0){
			alert ("Please select Minutes");
			frm.sMinutes.focus();
			return false;
		}								
		if (frm.sReason.selectedIndex == 0){
				alert ("Reason cannot be empty");
				frm.sReason.focus();
				return false;
		}
		if ((frm.sReason[frm.sReason.selectedIndex].value == "Others") && (trim(frm.tOthers.value) == "")){
			alert ("Please mention the reason in others");
			frm.tOthers.disabled=false;
			frm.tOthers.focus();
			return false;
		}				
		if (trim(frm.tOthers.value) == "")
			{
				frm.hReason.value=frm["sReason"][frm["sReason"].selectedIndex].value;	
			}
			else		
			{
				if ((frm["sReason"].selectedIndex > 0) && (frm["sReason"].selectedIndex < 4))
				{
					frm.hReason.value = 	frm["sReason"][frm["sReason"].selectedIndex].value;
				}
				if (frm.hReason.value == "")
				{
					frm.hReason.value=frm.tOthers.value;
				}
				else
				{
					frm.hReason.value=frm.hReason.value+", "+frm.tOthers.value;
				}
			}
			var tmp = Math.round(((1 - 0.5) +( ( (5+0.49999) - (1-0.5) ) * Math.random() ) )*100000);
			frm.hTmp.value = tmp;
			tmp=frm.sHours[frm.sHours.selectedIndex].value + ":" + frm.sMinutes[frm.sMinutes.selectedIndex].value+":00";
			frm.hNewTime.value=tmp;	
			if(clickCheck())
				return true;
			else
				return false;		
	}
}
function callCentre()
{
	if (document.getElementById('selexam_date').selectedIndex == 0){
		alert("Please select the exam date");
		document.getElementById('sellocation').value=0;
		document.getElementById('selexam_date').focus();
		return false;
	}
	else{	
		var edate=document.getElementById('selexam_date').value;
		var loc=document.getElementById('sellocation').value;
		createAjaxRequestObject();
		var url="bulktimechange.php?edate="+edate+'&loc='+loc;				
		xmlHttp.open("GET",url, true);
		xmlHttp.send(null);
		xmlHttp.onreadystatechange = function(){
			if ( (xmlHttp.readyState == 4) && (xmlHttp.status == 200) ) {				  
				 var Data =trim(xmlHttp.responseText);
				 var arrData=explode(",",Data);
				document.getElementById('selcentre').length=0;				
				 for(val in arrData){
				 	if(val == 0)
						document.getElementById('selcentre').options[0]=new Option("--SELECT ALL--",0);					
					else
						document.getElementById('selcentre').options[val]=new Option(arrData[val],arrData[val]);
												
				 }
				 document.getElementById('statusmsg').innerHtml='';;
			}else{
				document.getElementById('statusmsg').innerHtml="Centre Loading...";			
			}		
		}; 	
   }	
}

var checked=false;
function checkedAll() {
	var chk= document.getElementById('frmtimchange');
	if (checked == false)
         checked = true;    
    else      
        checked = false;     
	for (var i =0; i < chk.elements.length; i++){			
			if(chk.elements[i].id=='chkarrmem')
				chk.elements[i].checked = checked;
	}
}

function clickCheck()
{
	var checkcond=0;
	var chk= document.getElementById('frmtimchange');
	for (var i =0; i < chk.elements.length; i++){			
			if(chk.elements[i].id=='chkarrmem'  && chk.elements[i].checked){
				checkcond++;
			}	
	}
	if(checkcond==0){
		alert("Please check atleast one candidate to change time slot");
		return false;		
	}else
		return true;	
}
function callReset(){
	try{
		document.getElementById('selexam_date').value='';
		document.getElementById('sellocation').value=0;
		document.getElementById('selcentre').value=0;	
		document.getElementById('submitType').value=0;	
		document.getElementById('hNewTime').value='';
		document.getElementById('hReason').value='';	
		
		if(isObject(document.getElementById('content')) == true){		
			document.getElementById('content').style.visibility='hidden';
			document.getElementById('content').style.display='none';
		}	
		if(isObject(document.getElementById('divnewtime')) == true){
			document.getElementById('divnewtime').style.visibility='hidden';
			document.getElementById('divnewtime').style.display='none';	
		}			
		if(isObject(document.getElementById('idafrows')) == true){
			document.getElementById('idafrows').style.visibility='hidden';
			document.getElementById('idafrows').style.display='none';		
		}	
		if(isObject(document.getElementById('alertmsg')) == true){
			document.getElementById('alertmsg').style.visibility='hidden';
			document.getElementById('alertmsg').style.display='none';
		}
	}catch(e)
	{}	
}
function win_open(a,b)
{
	window.open('tadetails.php?id='+a+'&cid='+b,'win1','left=10,top=10,width=600,height=700,toolbar=no,menubar=no,addressbar=no,resizable=1');		
}
</script>
</head>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780>
	<tr><td width="780"><?php include("includes/header.php");?></td></tr>
	<TR>    	
	</TR>
	<tr>
	<Td class="greybluetext10"  background=images/tile.jpg><? include("admin_menu.php") ?></Td>	
	</tr>	
	<TR>
	    <TD background=images/tile.jpg vAlign=top width=780 align=center>
	    <form name='frmtimchange' id="frmtimchange" method='post' onSubmit="return submitForm()">
<table width=100% border=0 cellspacing=0 cellpadding=5>
	<tr><td colspan="2" align=center><div align=center><b>Bulk Time Change</b></div></td></tr>
	<tr>
		<td width="43%" align=right valign="middle" class=textblk11>Exam Date :</td>
	    <td width="57%" align=left valign="middle" class=textblk11><select  name='selexam_date' id='selexam_date' class='textblk11'>
          <option value=''>--Select--</option>
          <?php
			if (mysql_num_rows($resDate) > 0)
			{
				while (list($eDate) = mysql_fetch_row($resDate))
				{
					$aDate = explode("-",$eDate);
					$dispDate = $aDate[2]."-".$aDate[1]."-".$aDate[0];
					print("<option value='$eDate' ");
					if ($eDate == $edate)
						print(" selected ");
					print(">$dispDate</option>");
				}
			}
		?>
        </select></td>
	</tr>
	<tr>
	  <td align=right valign="middle" class=textblk11>Centre Location: </td>
	  <td align=left valign="middle" class=textblk11><select onChange="callCentre()" name='sellocation' id='sellocation'  class='textblk11'>
        <option value='0'>--SELECT ALL--</option> 
        <?php
			if (mysql_num_rows($resloc) > 0)
			{
				while (list($ecode,$ename) = mysql_fetch_row($resloc))
				{								
					print("<option value='$ecode' ");
					if ($ecode == $loc)
						print(" selected ");
					print(">$ename</option>");
				}
			}
		?>     
      </select></td>
	</tr>
	<tr>
	  <td align=right valign="middle" class=textblk11>Centre Code: </td>
	  <td align=left valign="middle" class=textblk11><select name='selcentre' id='selcentre'  class='textblk11'>
        <option value='0'>--SELECT ALL--</option>
        <?php
			if (mysql_num_rows($rescentre) > 0)
			{
				while (list($centrecode1) = mysql_fetch_row($rescentre))
				{								
					print("<option value='$centrecode1' ");
					if ($centrecode1 == $centre)
						print(" selected ");
					print(">$centrecode1</option>");
				}
			}
		?>     
      </select>
	  <div id="statusmsg" style="color:red"></div>	  </td>
	</tr>
	<tr>
	  <td colspan="2" align=right valign="middle" class=textblk11>	  
	<?PHP
	if(isset($_POST['sub']) && $resiwayrows > 0 )
	{?>	 
	  <div id="divnewtime" style="visibility:visible;display:block">
	  <table width="100%" cellpadding="5" cellspacing="0">
		<tr>
	  <td width="43%" align=right valign="middle" class=textblk11>New Time:</td>
	  <td width="57%" align=left valign="middle" class=textblk11>
	    <select name="sHours" class="textbox" style="width:40;">
          <option value=select selected>--</option>
          <?				
					for($i=0;$i<24;$i++){
					if ( $i < 10)
						echo "<option value='0$i'>0$i</option>";
					else
						echo "<option value=$i>$i</option>";
					}			
				?>
        </select>
	    &nbsp;Hrs&nbsp;
	    <select name="sMinutes" class="textbox" style="width:40;">
          <option value=select selected>--</option>
          <?				
					for($i=0;$i<60;$i++){
					if ( $i < 10)
						echo "<option value='0$i'>0$i</option>";
					else
						echo "<option value=$i>$i</option>";
					}		
				?>
        </select>
	    &nbsp;Mins &nbsp; </td>
	</tr>
	<tr>
	  <td align=right valign="middle" class=textblk11>Reason :</td>
	  <td align=left valign="middle" class=textblk11>
	    <select name='sReason' class='textbox' style='width:150;' onChange=" if(this.value=='Others') document.getElementById('tOthers').disabled=false; else {document.getElementById('tOthers').disabled=true;document.getElementById('tOthers').value=''}">
          <option value=0>--Select--</option>
          <?
				 $sql_reason = "select reason from iib_time_change_reason";
				 $res_reason = mysql_query($sql_reason);
				 while(list($reason)=mysql_fetch_row($res_reason)){
					$sel="";
					if ($reason==$sReason)
					 	$sel = "selected";
					 echo "<option value='$reason' $sel>$reason</option>";
				}
				 ?>
        </select>
        <?
					echo "<tr><td align=\"right\" width=390 class=\"textblk11\" valign=top>Others&nbsp;&nbsp;:</td><td align=\"left\" width=390 class=\"greybluetext10\" valign=top> <input disabled type=text value='' name='tOthers' id='tOthers' style='width:75px' class='textbox' maxlength=40></td></tr>";
				
	?>     </td>
	</tr>	
	</table>
	</div>	
	<?PHP }?>	  </td>
	  </tr>	
	<tr>
		<td colspan="2" align=center><input class='button' type="submit" name='sub' value='Submit'>&nbsp;&nbsp; 
		  <input class='button' type="button" name='sub2' value='Clear' onClick="callReset()">		  </td>
	</tr>
	<tr>
	  <td colspan="2" align=center valign="top"> <div align="left" class="arial11a">Color codes:<br>
	    Yellow color: 
	  Logged but not started his exam(viewing instructions, sample test ...) </div></td>
	  </tr>
	
	<?PHP	
	if( isset($_POST['sub']) && $_POST['submitType']==1 )
		echo "<tr><td colspan=7 align=center class=alertmsg><div id='alertmsg'>$rows candidates exam time changed sucessfully.</div></td></tr>";		
	?>	
	<?PHP 
	if(isset($_POST['sub']) && $resiwayrows > 0)
	{		
		echo "<tr><td colspan=7 align=center class=alertmsg><div id='idafrows'>No.Of Records:$resiwayrows</div></td></tr>";				 			
	?>
	
	<tr>
	  <td colspan="2" align=center>
	  <div id="content" style="visibility:visible;display:block">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="24" align="left" valign="top" bgcolor="#000066">
		  <table width="100%" border="1" align="left" cellpadding="5" cellspacing="0" bordercolor="#000066" bgcolor="#FFFFFF" style="border-left-width:thin">
            <tr>
              <td width="3%" bgcolor="#0080FF"><input type='checkbox' name='chkmem' id='chkmem' onClick="checkedAll()"></td>
              <td width="22%" bgcolor="#0080FF" class="whitecolor"><div align="center"><strong>Membership No</strong></div></td>
              <td width="16%" bgcolor="#0080FF" class="whitecolor"><div align="center"><strong>Centre Code </strong></div></td>
              <td width="15%" bgcolor="#0080FF" class="whitecolor"><div align="center"><strong>Exam Code</strong></div></td>
              <td width="14%" bgcolor="#0080FF" class="whitecolor"><div align="center"><strong>Subject Code </strong></div></td>
              <td width="13%" bgcolor="#0080FF" class="whitecolor"><div align="center"><strong>Exam Time</strong></div></td>
              <td width="17%" bgcolor="#0080FF" class="whitecolor"><div align="center"><strong>Exam Date</strong></div></td>
            </tr>
            <?PHP															
					while($rs = mysql_fetch_array($resiway)){            		         
					   $memno = $rs['memno'];
						$ccode = $rs['centre_code'];
						$excode = $rs['exam_code'];
						$subcode = $rs['subject_code'];
						$exam_time = $rs['exam_time'];
						$exam_date = $rs['exam_date'];																																				
						$prim_code=$memno.'_'.$excode.'_'.$subcode;
						$iw_iway="iwfr_".$ccode;				
						if(in_array($memno,$arrTrack))
							echo '<tr bgcolor="#FFFF99">';
						else
						     echo '<tr>';							 
							 echo "<td class='greybluetext10'><input type='checkbox' name='chkarrmem[]' id='chkarrmem' value='$prim_code'></td>
							  <td class='greybluetext10'><a href='candidateentries.php?cname=$memno' target='_blank'>$memno</a></td>
							  <td class='greybluetext10'><a href=javascript:win_open('$iw_iway','$ccode')>$ccode</td>
							  <td class='greybluetext10'>$excode</td>
							  <td class='greybluetext10'>$subcode</td>
							  <td class='greybluetext10'>$exam_time</td>
							  <td class='greybluetext10'>$exam_date</td>
							</tr>";							          
		  			}
			 
			  ?>
		  </table></td>
        </tr>
      </table>
	  </div>	  </td>
	  </tr>
	<?PHP }else if(isset($_POST['sub']))				
			echo '<tr><td colspan="7" align=center class=alertmsg>The candidate\'s are not mapped / All candidates are started his exam.</td></tr>';
	  ?>
</table>
<input type="hidden" name="submitType" id="submitType" value="<?php echo ( isset($_POST['sub']) && $resiwayrows > 0 ) ? 1 : 0;?>"/>
<input type=hidden name=hTmp>
<input type=hidden name=hNewTime>
<input type=hidden name=hReason>
<input type=hidden name=hAdminFlag value='<? echo $_SESSION["admin_type"];	?>'>
</form>
</TD>
</TR>
<TR>
<?php include("includes/footer.php");?>
</TR>
</TABLE>
</center>
</BODY>
</HTML>
