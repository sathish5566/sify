<?
ob_start();//header will work inspite of the echo's used
/****************************************************
* Application Name            :  IIB BULKUPLOAD
* Module Name                 :  Bulkupload for iway details
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  B.Devi
* Created ON                  :  21-03-2006
* Last Modified By            :  B.Devi
* Last Modified Date          :  21-03-2006
* Description                 :  
*****************************************************/
require_once("sessionchk.php");
require_once("includes/iway_functions.php");	
require("includes/session_handle.php");
require("includes/reader.php");
require_once("dbconfig.php");	

$fileid=trim($_POST['fileid']);

require_once("includes/settings.php");
require("includes/functions.php");
include("includes/logger.php");

$imported_user = $_SESSION["admin_id"];

/** for debugging purpose */
$commondebug = false;

//If no entry in slots redirected to error page
if(count($aslot_time)==0){
	header("Location: errorpage.php?status=10");
	exit;
}

/*All Validations made in viewfile.php should be repeated in db_import.php*/

/*Error flag Variable*/
$error_flag="2";

/*Updating the id in excel files: Used to display the datas from table*/
$question_sid = 0;

	if($commondebug){
		echo "<pre>";
		print_r($_POST);
		echo "</pre>";
	}
	
		
	$no_of_rows = $_POST['no_of_rows'];
	$no_of_cols = $_POST['no_of_cols'];
	
	
	
/**
*			Excel reader class usage starts here
*			Instantiating the Excel reader Class
*/

$data = new Spreadsheet_Excel_Reader();

/** setting the encoding format */
$data->setOutputEncoding('CP1252');

/** This will read from the excel file  */
$data->read($filenamepath.$file_name);

$no_of_sheets = count($data->sheets);

if($no_of_sheets >1){
	$flag[]=2;					
	header("Location: errorpage.php?status=8");	
	exit;
}

$cellerror = 0;

for($i=0; $i<1; $i++)
{
	$no_of_rows = $data->sheets[$i]['numRows'];
	$no_of_cols = $data->sheets[$i]['numCols'];
	
	
	/** printing the no. of rows and columns  */
	if ($commonDebug) {
		echo "ROWS: ".$no_of_rows . " COLS: ".$no_of_cols."<br>";
	}
	
	echo '<input type="hidden" name="rows_sheet'.$i.'" id="rows_sheet'.$i.'" value="'.$no_of_rows.'">';
	echo '<input type="hidden" name="cols_sheet'.$i.'" id="cols_sheet'.$i.'" value="'.$no_of_cols.'">';
	
	/** rights now hard coded here for no of cols check limit to 11*/
	if ($no_of_cols == 11){
		
		/*Array to check the uniqueness of centre code*/
		$actualDataArray = array();
		$UnqCentre_array = array();
		for($j=1;$j<=$no_of_rows;$j++){
			for($k=1;$k<=$no_of_cols;$k++){
				$actualDataArray[$j][$k] = $data->sheets[$i]['cells'][$j][$k];
				if($j!=1){
					if($k==1){
						$Ucentrecode =$actualDataArray[$j][$k];										
						$Ucentrecode=strtoupper(trim($Ucentrecode));
					}
														
				}//end of($j!=1)									
			}//end of($k)
			if($j!=1)
			$UnqCentre_array[]=$Ucentrecode;						
		}//end of for($j)
		
		/** Looping through each rows */
		for($j=1;$j<=$no_of_rows;$j++){
			
			for($k=1;$k<=$no_of_cols;$k++){
				$celldata = $data->sheets[$i]['cells'][$j][$k];
				
				//Header Column Check
				if ($j == 1){
					$errormsg = headervalidate($k,$celldata);
					if ($errormsg == 0){
						//if error redirected to viewfile.php
						$errormsg = "";
						$flag[]=2;
						header("Location: viewfile.php?fileid=$fileid");	
						exit;						
					}
				}//end of if($j==1)
				
				if ($j > 1 && $k <=9 || $j > 1 && $k>=10) {
						if($k == 1){	
														
							$centre_code_store=strtoupper(trim($celldata));//var to store the centre code from excel																																						
							$centrecheck = countCheck($celldata,$UnqCentre_array);//different function called for centre code 																															
							if ($centrecheck == 0) {									
								//if error redirected to viewfile.php
								$centrecheck = "";
								$flag[]=2;
								header("Location: viewfile.php?fileid=$fileid");															
								exit;
							}
																			
						}//end of $k==1
						
						if($k!=1){
							$errormsg = rowvalidate($k,$celldata);							
							/*Code added by Devi :
							if the Centre Code already added in Iway Details then the related details should match with 
							Excel Sheet.
							array_key_exists($cntseats) check done to find whether the centrecode is already 
							present in database
							If the center code is already present in database then the details of excel is compared
							with the excel data by using $cntseats.
							If there is any difference in details then to the display the error in green the $errormsg is assigned as 2
							*/
							if($k== 2){
								if($errormsg==1){																																																						
									if (array_key_exists($centre_code_store, $cntseats)) 												
									{
										if(($cntseats[$centre_code_store][$var_iwayname])==strtoupper(trim($celldata)))
											$errormsg=1;
										else
											$errormsg=0;
									}//end of if(array_key_exists)
								}//end of $errormsg
							}//end of $k=2
							
							if($k== 3){
								$actual_seats_excel=$celldata;
								if($errormsg==1){																																								
									if (array_key_exists($centre_code_store, $cntseats)) 												
									{
										if(($cntseats[$centre_code_store][$var_actualseats])==trim($celldata))
											$errormsg=1;
										else
											$errormsg=0;
									}//end of if(array_key_exists)
								}//end of $errormsg
							}//end of $k=3
							
							if($k== 4){
								if($errormsg==1){																																								
									if (array_key_exists($centre_code_store, $cntseats))
									{														
										if(($cntseats[$centre_code_store][$var_no_of_seats])==trim($celldata))
											$errormsg=1;
										else
											$errormsg=0;
									}//end of if(array_key_exists)
									else{
										if($actual_seats_excel<=$celldata)//no_of_seats should be greater than or equal to actual seats
											$errormsg=1;
										else
											$errormsg=0;
									}
								}//end of $errormsg
							}//end of $k=4
							
							if($k== 5){
								if($errormsg==1){																																								
									if (array_key_exists($centre_code_store, $cntseats))
									{														
										if(($cntseats[$centre_code_store][$var_status])==trim($celldata))
											$errormsg=1;
										else
											$errormsg=0;
									}//end of if(array_key_exists)
								}//end of $errormsg
							}//end of $k=5
							
							if($k== 8){
								if($errormsg==1){																																								
									if (array_key_exists($centre_code_store, $cntseats))
									{														
										if(($cntseats[$centre_code_store][$var_iway_city])==strtoupper(trim($celldata)))
											$errormsg=1;
										else
											$errormsg=0;
									}//end of if(array_key_exists)
								}//end of $errormsg
							}//end of $k=8
							
							if($k== 9){
								if($errormsg==1){																																								
									if (array_key_exists($centre_code_store, $cntseats))
									{														
										if(($cntseats[$centre_code_store][$var_iway_state])==strtoupper(trim($celldata)))
											$errormsg=1;
										else
											$errormsg=0;
									}//end of if(array_key_exists)
								}//end of $errormsg
							}//end of $k=9
							
							if($k== 10){
								if($errormsg==1){								
									//Check made for comparing the centrecode's five digit and pincode. Both must be same																																														
									$pincode_check=substr($centre_code_store,0,6);
									if($pincode_check==trim($celldata))
										$errormsg=1;
									else
										$errormsg=0;
								}//end of $errormsg
							}//end of $k=10
							
							if($k== 11){
								if($errormsg==1){																																								
									if (array_key_exists($centre_code_store, $cntseats))
									{														
										if(($cntseats[$centre_code_store][$var_exam_centre_code])==trim($celldata))
											$errormsg=1;
										else
											$errormsg=0;
									}//end of if(array_key_exists)
								}//end of $errormsg
							}//end of $k=10
							//echo $errormsg;
							
							if ($errormsg == 0) {
								//If Error redirected to viewfile.php
								$errormsg = "";
								$flag[]=2;
								header("Location: viewfile.php?fileid=$fileid");															
								exit;
							}
						}
					}//end of if ($j > 1 ,$k <=9, $k>=10)
					
					
					if (($errormsg == 1)||($centrecheck == 1)){
						$flag[]=1;
						$errormsg = "";
						$centrecheck="";
					}
					else{
							$flag[]=2;
					}
					
										
			}//end of for($K)
			
		}//end of for($j)		
	}// end of ($no_of_cols)
	else{
		$flag[]=2;
		header("Location: errorpage.php?status=9");	
		exit;
	}	
	
}//end of for($i)

?>

<form name="db_import" method="post" >
<?
if(!in_array($error_flag,$flag)){
	
for($j=2;$j<=$no_of_rows;$j++){
	for($k=1;$k<=$no_of_cols;$k++){
		$insertDataArray[$j][$k] = trim($data->sheets[0]['cells'][$j][$k]);
	}//end of  for($k)
}//end of for($j)

/*Dbcolumns of iib_iway_details*/
$dbcolumns	='centre_id,centre_code,iway_name,actual_seats,no_of_seats,status,iway_address1,iway_address2,iway_city,iway_state,iway_pin_code,exam_centre_code';

/*Dbcolumns of iib_iway_vacancy*/
$dbcolumns_vacancy='centre_code,exam_date,exam_time,no_of_seats,filled,remaining,id';

$filled='0';

foreach($insertDataArray as $key=>$value)
{
	
	$excel_centre_code=$value[1];//centre code from excel
	$excel_centre_code=strtoupper(trim($excel_centre_code));//converting to upper case
	$excel_no_seats=$value[4];	//no_of_seats from the excel
	
	//check if the centre is already exists in the iway details table
	if (!array_key_exists($excel_centre_code, $cntseats)) 
	
	{				
		$filecolumnname="";
		$filecolumnname.="'',";
		foreach($value as $key1 => $value1)
		{					
			$filecolumnname .= "'".addslashes(trim(strtoupper($value1)))."',";		
		}
		
		$filecolumnname = substr($filecolumnname , 0 ,-1);		
											
		$sql_insert_iway = "insert into ".TABLE_IWAY_DETAILS."($dbcolumns) values($filecolumnname)";
		$res_insert_iway = @mysql_query($sql_insert_iway);
		if ($commondebug){
			print $sql_insert_iway;
			echo mysql_error();
		}
		if (mysql_error()){
			echo mysql_error();
			exit;	
		}
		$filecolumnname = "";
	}//end of if(!array_key_exists)
			
	foreach($aslot_time as $key)
	{
		//This two needs to be cleared for each loop
		$sql_insert_vacancy="";
		$res_insert_vacancy="";
		
		$filecolumnname_vacancy="'".trim($excel_centre_code)."',";				
		$filecolumnname_vacancy.="'".trim($excel_exam_date[0])."',";
		$filecolumnname_vacancy.="'".trim($key)."',";
		$filecolumnname_vacancy.="'".trim($excel_no_seats)."',";
		$filecolumnname_vacancy.="$filled,";
		$filecolumnname_vacancy.="'".trim($excel_no_seats)."'";
		$filecolumnname_vacancy.=",''";
		
		if(in_array($excel_centre_code,$missed_checkslot))
		{
			if(in_array($key,$missed_slot[$excel_centre_code]))
			{																
				$sql_insert_vacancy = "insert into ".TABLE_IWAY_VACANCY."($dbcolumns_vacancy) values($filecolumnname_vacancy)";																								
				$res_insert_vacancy = @mysql_query($sql_insert_vacancy);
			}
		}//end of if(in_array)
		
		if(!in_array($excel_centre_code,$missed_checkslot))
		{																		
			$sql_insert_vacancy = "insert into ".TABLE_IWAY_VACANCY."($dbcolumns_vacancy) values($filecolumnname_vacancy)";														
			$res_insert_vacancy = @mysql_query($sql_insert_vacancy);
		}
					
		if($res_insert_vacancy)
		{
			if($question_sid  == 0){
				$question_sid = mysql_insert_id($connect);
			}
			
			$vac_rows = $vac_rows + mysql_affected_rows($connect);								
		}//end of if res_insert_vacancy
		if ($commondebug){
			print $sql_insert_vacancy;
			echo mysql_error();
		}
		if (mysql_error()){
			echo mysql_error();
			exit;			
		}
	}//end of if(aslot_time)			
			
}

$question_eid=$question_sid+$vac_rows-1;

if ( $vac_rows <= 0 ){
	header("Location: errorpage.php?status=4");
	exit;
}
}//$error_flag
else{
	header("Location: viewfile.php?fileid=$fileid");	
	exit;
}

/** Updating the status in the TABLE_EXCEL_FILES */
$sql_file_imported = "update ".TABLE_EXCEL_FILES." set question_sid='$question_sid',question_eid='$question_eid',imported_by='$imported_user',imported_on=NOW(),is_imported='".ACTIVE_FLAG."' where file_id=$fileid";
if($commondebug){
	echo "<br>".$sql_file_imported;
}

$result_imported= mysql_query($sql_file_imported);
if(mysql_error()){
	header("Location: errorpage.php?status=4");
	exit;
}


/** Redirecting a success page */
if($result_imported){
	header("Location: viewdb.php?fileid=$fileid&sid=$question_sid&eid=$question_eid");
}
?>
</form>

