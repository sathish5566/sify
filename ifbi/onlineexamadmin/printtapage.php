<?
require_once "sessionchk.php";
require_once "dbconfig.php";

$centrecode=$_REQUEST["centrecode"];
$talogin=$_REQUEST["talogin"];
//$talogin='dk';

//echo "Centere : $centrecode,Ta Login :$talogin";

$sql="select iway_name,iway_address1,iway_address2,iway_city,iway_state,iway_pin_code,ta_name from ".
	"iib_iway_details a,iib_ta_details b,iib_ta_iway c where a.centre_code=c.centre_code and b.ta_login=c.ta_login and ".
	"a.centre_code='$centrecode' and b.ta_login='$talogin'";

//echo "$sql";

$res=@mysql_query($sql);
$num_of_rows=mysql_num_rows($res);
$emsg="";

if($res && $num_of_rows>0)
{
	list($iway_name,$iway_address1,$iway_address2,$iway_city,$iway_state,$iway_pin_code,$ta_name)=mysql_fetch_row($res);
	$Address = "";
                              	
	if ($iway_address1 != "")
		$Address .= $iway_address1;
	if (($Address != "") && ($iway_address1 != ""))
		$Address .= " ";
	if ($iway_address2 != "")
		$Address .= $iway_address2;
	if (($Address != "") && ($iway_address2 != ""))
		$Address .= " ";
	if ($iway_city != "")
		$Address .= $iway_city;
	if (($Address != "") && ($iway_city != ""))
		$Address .= " ";
	if ($iway_pin_code != "")
		$Address .= $iway_pin_code;
	//$Address=$iway_address1.','.$iway_address2.','.$iway_city.','.$iway_state.','.$iway_pin_code;
	$sqlPass = "SELECT login_password FROM iib_ta_password WHERE ta_login='$talogin' ";
	$resPass = @mysql_query($sqlPass);
	list($ta_password) = @mysql_fetch_row($resPass);
}
else
{
	$emsg="Sorry Details Cannot Be Retrieved.Please Enter the Correct Data";
	$iway_name='NA';
	$Address='NA';
	$ta_password='NA';
	$ta_name='NA';
}
?>

<html>
<head>
<title><?=PAGE_TITLE ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<script language='JavaScript'>
function callOnLoad()
{
	window.opener.close();
	window.print();
}
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="640" border="0" cellspacing="0" cellpadding="0">
<tr> 
	<td><img src="images/logo.png" width="640" height="70"></td>
</tr>
<tr>
	<td width="640" bgcolor="7292C5">&nbsp;</td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="640" background="images/tile.jpg">
<tr>
<td>
<br>

<table border="0" cellspacing="0" cellpadding="10" width="75%" align=center>
<!-- This is to print the error message-->
<?if($emsg!="") {?>
<tr>
<td class="greybluetext10" colspan="3" bgcolor="#D1E0EF"><b class="arial11a"><?echo $emsg;?></b></td>
</tr>
<?}?>
<tr> 
<td class="greybluetext10" colspan="3" bgcolor="#D1E0EF"><b class="arial11a">TA details for iWay</b></td>
</tr>
<tr> 
<td width="35%" class="greybluetext10" bgcolor="#E8EFF7"><b>TA Name</b></td>
<td width="2%" class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
<td width="53%" bgcolor="#E8EFF7" class="greybluetext10"><?echo $ta_name;?></td>
</tr>
<tr>
<td class="greybluetext10" bgcolor="#E8EFF7"><b>iWay</b></td>
<td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
<td bgcolor="#E8EFF7" class="greybluetext10"><?echo $iway_name;?></td>
</tr>
<tr> 
<td class="greybluetext10" bgcolor="#E8EFF7"><b>Address</b></td>
<td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
<td bgcolor="#E8EFF7" class="greybluetext10"><?echo $Address;?></td>
</tr>
<tr> 
<td class="greybluetext10" bgcolor="#E8EFF7" colspan="3"> 
<p><b>Instructions for TA</b></p>
<ul>
<li>Please note that this is "ON-LINE" Examination (i.e. Electronic Mode) and the candidates have to reply the answers by mouse-clicking the correct choice
<li> Dummy Instructions for TA from IIB
<li> Dummy Instructions for TA from IIB
<li> Dummy Instructions for TA from IIB
</ul>
</td>
</tr>
<tr> 
<td class="greybluetext10" bgcolor="#E8EFF7" colspan="3"> 
<p><b>TA to give following Instructions to the Candidates</b></p>
<ul>
<li>Please note that this is "ON-LINE" Examination (i.e. Electronic Mode) and the candidates have to reply the answers by mouse-clicking the correct choice.
<li> The candidates are requested to be present in the examination hall at least half an hour before the commencement of the examination.  They are expected to be present at least by 09.00 a.m. on <b>28th November 2003 </b>at the examination venue.
<li> The Test Administrator will issue instructions regarding the conduct of the examination at about 9.00 a.m. when all the candidates should be present in the examination hall.
<li> The candidates would be allowed to undergo "Sample Test" before they start the actual examination.  However, the "Sample Test" could be undertaken only between 9.00 a.m. to 9.30 a.m.  This is to familiarize the candidates for the On-line Examination.
<li> The result will be generated immediately after the closure of the Examination.
<li> <b>The candidates are requested to bring their Membership Identity Card and also Admit Card in the examination hall, without which they would not be allowed to take the Examination. Test Administrators are required to verify the identity of each Candidate based on Membership Identity Card and also Admit Card. Test Administrators will obtain signatures of the Candidates on the Attendance sheet, which should be forwarded to Sify.</b>
</ul>
</td>
</tr>
<tr> 
	<td class="greybluetext10" bgcolor="#E8EFF7"><b>Site URL</b></td>
	<td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
	<td bgcolor="#E8EFF7" class="greybluetext10">www.sifyitest.com</td>
</tr>
<tr> 
	<td class="greybluetext10" bgcolor="#E8EFF7"><b>TA Login</b></td>
	<td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
	<td bgcolor="#E8EFF7" class="greybluetext10"><?echo $talogin;?></td>
</tr>
<tr> 
	<td class="greybluetext10" bgcolor="#E8EFF7"><b>TA Password</b></td>
	<td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
	<td bgcolor="#E8EFF7" class="greybluetext10"><?echo $ta_password;?></td>
</tr>

</table>
<br>
</td>
</tr>
<tr>
			<td><br></td>
		</tr>
		<Tr>
			<td>
			<table border=0 width=640>
				<tr>
					<td><img src="images/spacer.gif" width=273 height=17></td>
					<td background="images/print.gif"><a href="javascript:window.print()"><img src="images/spacer.gif" width=38 height=17 border=0></a></td>
					<td background="images/exit.gif"><a href="javascript:window.close()"><img src="images/spacer.gif" width=29 height=17 border=0></a></td>								
					<td><img src="images/spacer.gif" width=273 height=17></td>																
				</tr>
			</table>
			</td>
		</Tr>
		<tr>
			<td><br><br></td>
		</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="640">
<tr>
<td width="640" bgcolor="7292C5">&nbsp;</td>
</tr>
</table>
<br>
</body>
</html>