<?php
header("Content-Disposition: attachment; filename=feedback_report_".date("YmdHis").".xls");
header("Content-Type: application/msexcel;filename=feedback_report_".date("YmdHis").".xls");
$admin_flag=2;
require("dbconfig.php");
require("sessionchk.php");
set_time_limit(0);
$examDate = $_POST['exam_date'];

$arrYesNo = array('Y'=>'Yes','N'=>'No');
$arrAbtQns = array('A'=>'Easy','B'=>'Difficult','C'=>'Not relevant','D'=>'Can\'t Say');

if ($examDate != "")
{
	print "<tr><td>";
	$aDates = explode("-", $examDate);
	$dispDate = $aDates[2]."-".$aDates[1]."-".$aDates[0];
	$sql = "select a. membership_no, c. name, b. question1, b. question2, b. question3, b. question4, b. question5, b. question6, b. question7, b. question8 from iib_candidate_iway a, iib_ing_feedback b, iib_candidate c where a.exam_date='$examDate' and a. membership_no=b. membership_no and a. membership_no=c. membership_no and a. exam_code=b. exam_code and a. subject_code=b. subject_code and a.exam_date='$examDate' order by a.membership_no";
	$res = mysql_query($sql);
	//print $sql;
	$nRows = mysql_num_rows($res);
	if ($nRows > 0)
	{
		print("<table width=100% border=1 cellspacing=1 cellpadding=1>");
		print("<tr><td class=textblk11>&nbsp;</td><td class=textblk11>&nbsp;</td><td class=textblk11>&nbsp;</td><td class=textblk11>&nbsp;</td>");
		print("<td class=textblk11 colspan='3' align='center' valign='top'><b>Did you face any issue in</b></td>");
		print("<td class=textblk11>&nbsp;</td><td class=textblk11>&nbsp;</td><td class=textblk11>&nbsp;</td><td class=textblk11>&nbsp;</td><td class=textblk11>&nbsp;</td></tr>");
		print("<tr><td class=textblk11 valign='top'><b>Membership No</b></td>");
		print("<td  class=textblk11 valign='top'><b>Name</b></td>");
		print("<td  class=textblk11 valign='top'><b>Did you have enough adequate time to complete all questions?</b></td>");
		print("<td  class=textblk11 valign='top'><b>Is the venue facilities where you are sitting satisfactory?</b></td>");
		print("<td  class=textblk11 valign='top'><b>Login / Password and registration no</b></td>");
		print("<td  class=textblk11 valign='top'><b>Navigating the screen to next questions</b></td>");
		print("<td  class=textblk11 valign='top'><b>Any other technical issues</b></td>");
//		print("<td  class=textblk11 valign='top'><b>Were you able to see your marks at the end of the online test</b></td>");
		print("<td  class=textblk11 valign='top'><b>How were the questions in Part A (General banking) </b></td>");
		print("<td  class=textblk11 valign='top'><b>How were the questions in Part B (Aptitude) </b></td>");
		print("<td  class=textblk11 valign='top'><b>Overall how would you rate the online test methodology (1-Poor; 5-Excellant)</b></td>");
		print("<td  class=textblk11 valign='top'><b>Any other comments</b></td></tr>");
		while (list($membershipNo, $candName, $qn1, $qn2, $qn3, $qn4, $qn5, $qn6, $qn7, $qn8) = mysql_fetch_row($res))
		{		
			print("<tr>");	
			print("<td class=textblk11 valign='top' align='left'>$membershipNo</td>");
			print("<td class=textblk11 valign='top'>$candName</td>");
			print("<td class=textblk11 valign='top'>$arrYesNo[$qn1]</td>");	
			print("<td class=textblk11 valign='top'>$arrYesNo[$qn2]</td>");
			$arrQn3 = explode("|",$qn3);
			if(is_array($arrQn3) && count($arrQn3) > 0)
			{
				foreach($arrQn3 as $val)
				{
					print("<td class=textblk11 valign='top'>$arrYesNo[$val]</td>");
				}
			}else{
				print("<td class=textblk11 valign='top'>No</td>");
				print("<td class=textblk11 valign='top'>No</td>");
				print("<td class=textblk11 valign='top'>No</td>");
			}
			//print("<td class=textblk11 valign='top'>$arrYesNo[$qn4]</td>");
			print("<td class=textblk11 valign='top'>$arrAbtQns[$qn5]</td>");
			print("<td class=textblk11 valign='top'>$arrAbtQns[$qn6]</td>");
			print("<td class=textblk11 valign='top'>$qn7</td>");
			$feedback = stripslashes($qn8);
			if ($feedback == "")
				print("<td class=textblk11>&nbsp;</td>");
			else
				print("<td class=textblk11 valign='top'>$feedback</td>");
			print("</tr>");
		}
		print("</table><br><br>");
	}		
	else
	{
		print("<br><div align=center class=textblk11>No records found</div><br><br>");
	}

}
	?>