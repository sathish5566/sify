<?php
@session_start();
if ($_SESSION["admin_type"]== '5') {
	$admin_flag = 5;
} else {
	$admin_flag = 2;
}
require("dbconfig_slave_123.php");
require("sessionchk.php");
set_time_limit(0);

$examDate = $_POST['exam_date'];
$sqlDate = "SELECT distinct exam_date FROM iib_exam_schedule order by exam_date ";
$resDate = mysql_query($sqlDate);

function getBrowser(){
	
	$useragent = $_SERVER['HTTP_USER_AGENT'];
   	if (preg_match('|MSIE ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
		$browser_version=$matched[1];
		$browser = 'IE';
	}else if(preg_match( '|Opera ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
		$browser_version=$matched[1];
		$browser = 'Opera';
	} elseif(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched)) {
			$browser_version=$matched[1];
			$browser = 'Firefox';
	} elseif(preg_match('|Safari/([0-9\.]+)|',$useragent,$matched)) {
			$browser_version=$matched[1];
			$browser = 'Safari';
	} else {
			// browser not recognized!
		$browser_version = 0;
		$browser= 'other';
	}		
	return $browser;
}


function sec2hms ($sec, $padHours = false) {

    $hms = "";    
    // there are 3600 seconds in an hour, so if we
    // divide total seconds by 3600 and throw away
    // the remainder, we've got the number of hours
    $hours = intval(intval($sec) / 3600); 

    // add to $hms, with a leading 0 if asked for
    $hms .= ($padHours) 
          ? str_pad($hours, 2, "0", STR_PAD_LEFT). ':'
          : $hours. ':';
     
    // dividing the total seconds by 60 will give us
    // the number of minutes, but we're interested in 
    // minutes past the hour: to get that, we need to 
    // divide by 60 again and keep the remainder
    $minutes = intval(($sec / 60) % 60); 

    // then add to $hms (with a leading 0 if needed)
    $hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT). ':';

    // seconds are simple - just divide the total
    // seconds by 60 and keep the remainder
    $seconds = intval($sec % 60); 

    // add to $hms, again with a leading 0 if needed
    $hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT);

    return $hms;
}

if ($examDate != "")
{
    $arrSub=array();
    $sql_sub = "select exam_code,subject_code,subject_duration from iib_exam_subjects";
	$res_sub = @mysql_query($sql_sub);
    if($res_sub = mysql_query($sql_sub)){
        while($rsub=mysql_fetch_object($res_sub)){        
            $arrSub[$rsub->exam_code][$rsub->subject_code]=$rsub->subject_duration;
        }
    }
    $arriway=array();
    $sql_iway = "select iway_name,centre_code from iib_iway_details";
	$res_iway = @mysql_query($sql_iway);
    if($res_iway = mysql_query($sql_iway)){
        while($riway=mysql_fetch_object($res_iway)){        
            $arriway[$riway->centre_code]=$riway->iway_name."(<a href='#' title=Click to view Centredetails onclick=win_open('tadetails.php?cid=".$riway->centre_code."','400','600')>".$riway->centre_code."</a>)";
        }
    }

    $arrQP=array();    
    //$sql_question1 = " select count(1) as cnt,question_paper_no from iib_question_paper_details where answer!='' AND answer IS NOT NULL  group by question_paper_no";
	$sql_question1 = "select count(distinct question_id) as cnt,question_paper_no from iib_response group by question_paper_no";
    if($res_question1 = mysql_query($sql_question1)){
        while($rquestion=mysql_fetch_object($res_question1)){        
            $arrQP[$rquestion->question_paper_no]['anscount']=$rquestion->cnt;
        }
    }
    reset($arrQP);
    //$sql_question = "select question_paper_no,MAX(DATE_FORMAT(updated_time,'%T')) as uptime from iib_question_paper_details group by question_paper_no";
	$sql_question = "select question_paper_no,MAX(DATE_FORMAT(updatedtime,'%T')) as uptime from iib_response group by question_paper_no";
	$res_question = @mysql_query($sql_question);
    if($res_question = mysql_query($sql_question)){
        while($rquestion=mysql_fetch_object($res_question)){
            $arrQP[$rquestion->question_paper_no]['uptime']=$rquestion->uptime;
        }
    }
   
    $arrMem=array();
    $str='';
    $str5='';
    $str15='';
	$aDates = explode("-", $examDate);
	$dispDate = $aDates[2]."-".$aDates[1]."-".$aDates[0];
	$examDate1 = $examDate." 00:00:00";
	$examDate2 = $examDate." 23:59:59";
    $sql = "SELECT a.membership_no,a.exam_code,a.subject_code, DATE_FORMAT(a.start_time,'%T'), DATE_FORMAT(a.last_updated_time,'%T'),"
          ." question_paper_no,a.browser_status FROM iib_candidate_test a LEFT JOIN	iib_candidate_scores b ON a.membership_no=b.membership_no AND "
          ." a.exam_code=b.exam_code AND a.subject_code=b.subject_code WHERE b.subject_code IS NULL AND a.start_time between "
          ." '$examDate1'  AND '$examDate2' order by test_id desc ";	
	$res = mysql_query($sql);	
	$nRows = mysql_num_rows($res);		
    $j=0;
    $s=0;$s5=0;$s15=0;$STR='';
    if ($nRows > 0)
	{												
    	while (list($membershipNo, $examCode, $subjectCode, $startTime,$updateTime,$QPno,$browserstatus) = mysql_fetch_row($res))
		{											
			$val=trim($membershipNo.'_'.$examCode.'_'.$subjectCode);			
			if(!in_array($val,$arrMem))
			{							
				array_push($arrMem,$val);
	
				$sql_iway = "select centre_code from iib_candidate_iway where membership_no='".$membershipNo."' and  exam_code='".$examCode."' and subject_code='".$subjectCode."'";
				$res_iway = @mysql_query($sql_iway);
				list($iway)=mysql_fetch_row($res_iway);
                $iw_iway="iwfr_".$iway;
                $centrename=$arriway[$iway];

                if($arrQP[$QPno]['anscount'] == '')
                    $anscount=0;
                else
                    $anscount=$arrQP[$QPno]['anscount'];
                $qpupdatedtime=$arrQP[$QPno]['uptime'];
                
                if($qpupdatedtime!='' && $qpupdatedtime!='00:00:00')
					$latime=strtotime($qpupdatedtime);
				else
				   $latime=strtotime($startTime);	

				   										
				$curtime=strtotime("now");
				$diff=$curtime-$latime;				
				$idletime=sec2hms($diff);

               $qry2="SELECT SUM(TIME_TO_SEC(DATE_FORMAT(last_updated_time,'%T')) - TIME_TO_SEC(DATE_FORMAT(start_time,'%T'))) as "
                    ." totaltimetaken,SUM(time_extended) as extendedtime from iib_candidate_test where membership_no='$membershipNo' and exam_code='$examCode'"
                    ." and subject_code='$subjectCode' group by exam_code,subject_code";
                $res2= mysql_query($qry2);
                while($rs2=mysql_fetch_object($res2)){
                    $timeTaken = $rs2->totaltimetaken;
                    $exttime=$rs2->time_extended;
                }
               
                $subduration=$arrSub[$examCode][$subjectCode];
                $Totaltime=$subduration + $exttime;
                $timeleft=$Totaltime - $timeTaken;
                    
                $membershipNo1="<a title='Click to view candidate details' onclick=win_open('grid_candidate.php?id=$membershipNo','800','800') href='#'>$membershipNo</a>";
                
                if( ($diff > 300) && ($diff <= 900 ) ) { 
                    $str5.='{ membershipno:"'.$membershipNo1.'",';
                    $str5.='centrecode:"'.$centrename.'",';
                    $str5.='examcode:"'.$examCode.'",';                    
                    $str5.='subjectcode:"'.$subjectCode.'",';
                    $str5.='subjectduration:"'.sec2hms($subduration).'",';
                    $str5.='anscount:"'.$anscount.'",';                        
                    $str5.='qpupdatedtime:"'.$qpupdatedtime.'",';                        
                    $str5.='starttime:"'.$startTime.'",';
                    $str5.='updatetime:"'.$updateTime.'",';
                    $str5.='exttime:"'.sec2hms($exttime).'",';
                    $str5.='timetaken:"'.sec2hms($timeTaken).'",';
                    $str5.='timeleft:"'.sec2hms($timeleft).'",';              
                    $str5.='browser:"'.$browserstatus.'",';                                        
                    $str5.='idletime:"'.$idletime.'"},'; 
                    $s5++;
                }else if($diff > 900) { 
                    $str15.='{ membershipno:"'.$membershipNo1.'",';
                    $str15.='centrecode:"'.$centrename.'",';
                    $str15.='examcode:"'.$examCode.'",';
                    $str15.='subjectcode:"'.$subjectCode.'",';
                    $str15.='subjectduration:"'.sec2hms($subduration).'",';
                    $str15.='anscount:"'.$anscount.'",';                        
                    $str15.='qpupdatedtime:"'.$qpupdatedtime.'",';                        
                    $str15.='starttime:"'.$startTime.'",';
                    $str15.='updatetime:"'.$updateTime.'",';
                    $str15.='exttime:"'.sec2hms($exttime).'",';
                    $str15.='timetaken:"'.sec2hms($timeTaken).'",';
                    $str15.='timeleft:"'.sec2hms($timeleft).'",';              
                    $str15.='browser:"'.$browserstatus.'",';                                        
                    $str15.='idletime:"'.$idletime.'"},';
                    $s15++;
                }else{
                    $str.='{ membershipno:"'.$membershipNo1.'",';
                    $str.='centrecode:"'.$centrename.'",';
                    $str.='examcode:"'.$examCode.'",';
                    $str.='subjectcode:"'.$subjectCode.'",';
                    $str.='subjectduration:"'.sec2hms($subduration).'",';
                    $str.='anscount:"'.$anscount.'",';                        
                    $str.='qpupdatedtime:"'.$qpupdatedtime.'",';                        
                    $str.='starttime:"'.$startTime.'",';
                    $str.='updatetime:"'.$updateTime.'",';
                    $str.='exttime:"'.sec2hms($exttime).'",';
                    $str.='timetaken:"'.sec2hms($timeTaken).'",';
                    $str.='timeleft:"'.sec2hms($timeleft).'",';              
                    $str.='browser:"'.$browserstatus.'",';                                        
                    $str.='idletime:"'.$idletime.'"},';
                    $s++;
                }

            }
        }
        if($s > 0)
            $STR.='var mydata = ['.substr($str,0,-1).'];';
        else
            $STR.='var mydata =[];';

        if($s5 > 0 )
            $STR.='var mydata5 = ['.substr($str5,0,-1).'];';
        else
            $STR.='var mydata5 =[];';
            
        if($s15 > 0 )
            $STR.='var mydata15 = ['.substr($str15,0,-1).'];';
        else
            $STR.='var mydata15 =[];';
            
	}		
	else
	{
        $STR.='var mydata =[];';
        $STR.='var mydata5 =[];';
        $STR.='var mydata15 =[];';
        
	}
}
echo '<script type="text/javascript">';
echo 'var BROWSER="'.getBrowser().'";';
echo $STR;
echo '</script>';
?>
<HTML>
<HEAD>
<title><?=PAGE_TITLE ?></title>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" media="screen" href="./jquery/themes/redmond/jquery-ui-1.8.2.custom.css" />
<link rel="stylesheet" type="text/css" media="screen" href="./jquery/themes/ui.jqgrid.css" />
<link rel="stylesheet" type="text/css" media="screen" href="./jquery/themes/ui.multiselect.css" />
<script src="./jquery/js/jquery.min.js" type="text/javascript"></script>
<script src="./jquery/js/jquery-ui-1.8.2.custom.min.js" type="text/javascript"></script>
<script src="./jquery/js/jquery.layout.js" type="text/javascript"></script>
<script src="./jquery/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script type="text/javascript">
$.jgrid.no_legacy_api = true;
$.jgrid.useJSON = true;
</script>
<!-- <script src="./jquery/js/ui.multiselect.js" type="text/javascript"></script> -->
<script src="./jquery/js/jquery.jqGrid.min.js" type="text/javascript"></script>
<script src="./jquery/js/jquery.tablednd.js" type="text/javascript"></script>
<script src="./jquery/js/jquery.contextmenu.js" type="text/javascript"></script>
<style>

.rowColorYELLOW {
    background-color:#FFFFC1  !important;
}
.rowColorRED {
    background-color:#FF8533  !important;
}
</style>
<script language='JavaScript'>
function win_open(url,w,h)
{
    window.open(url,'w','left=10,top=10,width='+w+',height='+h+',toolbar=no,menubar=no,addressbar=no,resizable=1,scrollbars=1');
    return false;
}
function submitForm()
{
	if (document.frm.exam_date.selectedIndex == 0)
	{
		alert("Please select the exam date");
		document.frm.exam_date.focus();
		return false;
	}
	document.frm.submit();
}
</script>
<script type="text/javascript">
<?PHP if ($examDate != ""){  ?>

jQuery(document).ready(function(){        

      jQuery("#grid15").jqGrid({
        autowidth:false,
        data: mydata15,
        datatype: "local",
        height: 'auto',
        rowNum: 500,
        rowList: [100,500,1000,5000],
        colNames:['Membership No','Exam Code','Subject Code','Subject Duration',
                     'Answered Count','Answer Saved Time','Idle Time','Start Time','Updated Time',
                      'Extended Time','Time Taken','Time Left','Browser','Centre'],
       
        colModel:[
                {name:'membershipno',index:'membershipno',width:120,align:'center'},
                {name:'examcode',index:'examcode', width:90,align:'center'},
                {name:'subjectcode',index:'subjectcode', width:95,align:'center'},        
                {name:'subjectduration',index:'subjectduration', width:120,align:'center'},
                {name:'anscount',index:'anscount', width:125,align:'center'},		
                {name:'qpupdatedtime',index:'qpupdatedtime',width:125,align:'center'},
                {name:'idletime',index:'idletime',width:80,align:'center'},
                {name:'starttime',index:'starttime', width:90,align:'center'},		
                {name:'updatetime',index:'updatetime', width:105,align:'center'},
                {name:'exttime',index:'exttime', width:105,align:'center'},		
                {name:'timetaken',index:'timetaken', width:95,align:'center'},		
                {name:'timeleft',index:'timeleft', width:85,align:'center'},		 
                {name:'browser',index:'browser', width:75,align:'center'},		 
                {name:'centrecode',index:'centrecode',width:400,align:'center'},
                ],
        pager: "#pgrid15",
        emptyrecords: "<b>No records found</b>",   
        viewrecords: true,
        sortname: 'centrecode',
        grouping:true,
        groupingView : {
            groupField : ['centrecode'],
            groupColumnShow : [false],
            groupText : ['<b>{0} - {1} Candidate(s)</b>'],
            groupCollapse : true
        },
        caption:"List of Candidates having more than 15 minutes idletime(HH:MM:SS)"
      });

    jQuery("#grid5").jqGrid({
        autowidth:false,
        data: mydata5,
        datatype: "local",
        height: 'auto',
        rowNum: 500,
        rowList: [100,500,1000,5000],
        colNames:['Membership No','Exam Code','Subject Code','Subject Duration',
                 'Answered Count','Answer Saved Time','Idle Time','Start Time','Updated Time',
                 'Extended Time','Time Taken','Time Left','Browser','Centre'],

        colModel:[
                {name:'membershipno',index:'membershipno',width:120,align:'center'},
                {name:'examcode',index:'examcode', width:90,align:'center'},
                {name:'subjectcode',index:'subjectcode', width:95,align:'center'},        
                {name:'subjectduration',index:'subjectduration', width:120,align:'center'},
                {name:'anscount',index:'anscount', width:125,align:'center'},		
                {name:'qpupdatedtime',index:'qpupdatedtime',width:125,align:'center'},
                {name:'idletime',index:'idletime',width:80,align:'center'},
                {name:'starttime',index:'starttime', width:90,align:'center'},		
                {name:'updatetime',index:'updatetime', width:105,align:'center'},
                {name:'exttime',index:'exttime', width:105,align:'center'},		
                {name:'timetaken',index:'timetaken', width:95,align:'center'},		
                {name:'timeleft',index:'timeleft', width:85,align:'center'},		 
                {name:'browser',index:'browser', width:75,align:'center'},		 
                {name:'centrecode',index:'centrecode',width:400,align:'center'},
                ],
        pager: "#pgrid5",
        emptyrecords: "<b>No records found</b>",   
        viewrecords: true,
        sortname: 'centrecode',
        grouping:true,
        groupingView : {
            groupField : ['centrecode'],
            groupColumnShow : [false],
            groupText : ['<b>{0} - {1} Candidate(s)</b>'],
            groupCollapse : true
        },
        caption:"List of Candidates having more than 5 minutes idle time(HH:MM:SS)"
      });

    jQuery("#grid").jqGrid({
        autowidth:false,
        data: mydata,
        datatype: "local",
        height: 'auto',
        rowNum: 500,
        rowList: [100,500,1000,5000],
        colNames:['Membership No','Exam Code','Subject Code','Subject Duration',
                 'Answered Count','Answer Saved Time','Idle Time','Start Time','Updated Time',
                 'Extended Time','Time Taken','Time Left','Browser','Centre'],
 
        colModel:[
                {name:'membershipno',index:'membershipno',width:120,align:'center'},
                {name:'examcode',index:'examcode', width:90,align:'center'},
                {name:'subjectcode',index:'subjectcode', width:95,align:'center'},        
                {name:'subjectduration',index:'subjectduration', width:120,align:'center'},
                {name:'anscount',index:'anscount', width:125,align:'center'},		
                {name:'qpupdatedtime',index:'qpupdatedtime',width:125,align:'center'},
                {name:'idletime',index:'idletime',width:80,align:'center'},
                {name:'starttime',index:'starttime', width:90,align:'center'},		
                {name:'updatetime',index:'updatetime', width:105,align:'center'},
                {name:'exttime',index:'exttime', width:105,align:'center'},		
                {name:'timetaken',index:'timetaken', width:95,align:'center'},		
                {name:'timeleft',index:'timeleft', width:85,align:'center'},		 
                {name:'browser',index:'browser', width:75,align:'center'},		 
                {name:'centrecode',index:'centrecode',width:400,align:'center'},
                ],
        pager: "#pgrid",
        emptyrecords: "<b>No records found</b>",   
        viewrecords: true,
        sortname: 'centrecode',
        grouping:true,
        groupingView : {
            groupField : ['centrecode'],
            groupColumnShow : [false],
            groupText : ['<b>{0} - {1} Candidate(s)</b>'],
            groupCollapse : true
        },
       caption:"List of Candidates having Less than 5 minutes idle time"
      });
          
});

<?PHP } ?>
</script>


</head>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0>
<center>
<TABLE border=0 cellPadding=0 cellSpacing=0 width="98%">
	<tr><td><?php include("includes/header.php");?></td></tr>
	<TR>    	
	</TR>
	<tr>
	<Td class="greybluetext10"  background=images/tile.jpg><?php  include("admin_menu.php") ?></Td>	
	</tr>	
	<TR>
	    <TD background=images/tile.jpg vAlign=top  align=center>
	    <form name='frm' method='post'>
<table width=100% border=0 cellspacing=0 cellpadding=5>
	<tr><td align=center><div align=center><b>Incomplete Candidate(S) Status Report</b></div></td></tr>
	<tr>
		<td align=center class=textblk11>Exam Date : 
			<select name='exam_date' style='{width:150px}' class='textblk11'>
				<option value=''>--Select--</option>
<?php
if (mysql_num_rows($resDate) > 0)
{
	while (list($eDate) = mysql_fetch_row($resDate))
	{
		$aDate = explode("-",$eDate);
		$dispDate = $aDate[2]."-".$aDate[1]."-".$aDate[0];
		print("<option value='$eDate' ");
		if ($eDate == $examDate)
			print(" selected ");
		print(">$dispDate</option>");
	}
}

?>
		</select>
		</td>
	</tr>
	<tr>
<td align=center><input class='button' type='button' name='sub' value='Submit' onClick='javascript:submitForm()'></td>
</tr>
</table>
</form>
</td>
</tr>
<TR>
<TD background=images/tile.jpg vAlign=top  align=center>
<table id="grid15"></table>
<div id="pgrid15"></div>
<br/><br/>
<table id="grid5"></table>
<div id="pgrid5"></div>
<br/><br/>
<table id="grid"></table>
<div id="pgrid"></div>


<div style="background:#004a80" height=10>&nbsp;</div>
</td>
</tr>
</TABLE>
</center>
</BODY>
</HTML>
