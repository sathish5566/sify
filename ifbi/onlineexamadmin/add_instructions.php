<?php
require_once "sessionchk.php";
require_once "dbconfig.php";
//error_reporting(E_ALL);
function utf8tohtml($utf8, $encodeTags) {
    $result = '';
    for ($i = 0; $i < strlen($utf8); $i++) {
        $char = $utf8[$i];
        $ascii = ord($char);
        if ($ascii < 128) {
            // one-byte character
            $result .= ($encodeTags) ? htmlentities($char) : $char;
        } else if ($ascii < 192) {
            // non-utf8 character or not a start byte
        } else if ($ascii < 224) {
            // two-byte character
            $result .= htmlentities(substr($utf8, $i, 2), ENT_QUOTES, 'UTF-8');
            $i++;
        } else if ($ascii < 240) {
            // three-byte character
            $ascii1 = ord($utf8[$i+1]);
            $ascii2 = ord($utf8[$i+2]);
            $unicode = (15 & $ascii) * 4096 +
                       (63 & $ascii1) * 64 +
                       (63 & $ascii2);
            $result .= "&#$unicode;";
            $i += 2;
        } else if ($ascii < 248) {
            // four-byte character
            $ascii1 = ord($utf8[$i+1]);
            $ascii2 = ord($utf8[$i+2]);
            $ascii3 = ord($utf8[$i+3]);
            $unicode = (15 & $ascii) * 262144 +
                       (63 & $ascii1) * 4096 +
                       (63 & $ascii2) * 64 +
                       (63 & $ascii3);
            $result .= "&#$unicode;";
            $i += 3;
        }
    }
    return $result;
}

$submitted		= $_POST['submit_flag'];
$sExamCode		= $_POST['sExamCode'];
$sSubjectCode	= $_POST['sSubjectCode'];
$sMedium		= $_POST['sMedium'];
$commonDebug	= 0;

$sql_res ="SELECT instruction_text FROM iib_instructions_template WHERE exam_code='".$sExamCode."' AND subject_code='".$sSubjectCode."' AND medium_code='".$sMedium."' AND is_active='Y'";
if ($commonDebug)
{
	echo	$sql_res."<br>";
}
$query_res = mysql_query($sql_res);
$num = mysql_num_rows($query_res);
if($num > 0)
{
	list($instruction) = mysql_fetch_row($query_res);
	$instruction = htmlentities($instruction);
}
else 
{
	$instruction = "";
}

if($submitted=='1')
{
	
   
	$instruction = $_POST['hid_ta'];
	
	$instruction = str_replace("'","`",$instruction);
	if($sMedium == 'EN') $ins_medium = $instruction;
	elseif($sMedium == 'HI') $ins_medium = utf8tohtml($instruction);
	
	if($num > 0)
	{
		$sql_up = "UPDATE iib_instructions_template SET instruction_text='$ins_medium' WHERE exam_code='".$sExamCode."' AND subject_code='".$sSubjectCode."' AND medium_code='".$sMedium."'";
		$query_up =mysql_query($sql_up) or die("Update Failed");
		if ($commonDebug)
		{
			echo	$sql_up."<br>";
		}
	}
	else
	{

		 $sql_ins	=	" INSERT INTO iib_instructions_template(exam_code,subject_code,medium_code,instruction_text) VALUES('$sExamCode','$sSubjectCode','$sMedium','$ins_medium')";
		 $sql_query = mysql_query($sql_ins) or die("insert falied");
		 if ($commonDebug)
		 {
			echo	$sql_ins."<br>";
		 }
	}
	$view_instruction = $instruction;
	$instruction = htmlentities($instruction);
	


}


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE><?=PAGE_TITLE ?></TITLE>
</HEAD>
<STYLE type="text/css">
BODY {
	FONT-FAMILY: Arial, Verdana 
}
.textblk11 { 
	font-family: Verdana, Arial, Helvetica, sans-serif; 
	font-size: 11px; 
	color: #000000; 
	text-decoration: none;
}
</STYLE>
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<script language="javascript" src="./includes/validations.js"></script>
<script language="javascript">

function Add()
{
	var inner=editor.getHTML();
	var frm = document.email_add;
	frm.hid_ta.value=inner;
	if (frm.sExamCode.value=="-1") 
	{
		alert("Please select any Exam Code");
		frm.sExamCode.focus();
		return false;
	}
	if (frm.sSubjectCode.value=="-1") 
	{
		alert("Please select any Subject Code");
		frm.sSubjectCode.focus();
		return false;
	}
	if (frm.sMedium.value=="-1") 
	{
		alert("Please select any Medium");
		frm.sMedium.focus();
		return false;
	}
	
	if(frm.hid_ta.value.length<1)
	{
		alert("Please enter instructions");
		return false;
	}
	/*if(trim(frm.hid_ta.value)=='')
	{
		alert("Please enter instructions");
		return false;
	}*/

    frm.submit_flag.value='1'; 
	frm.action="add_instructions.php";
    frm.submit();
	// }
	
} 
function onChangeExam()
{
	var frm = document.email_add;
	if (frm.sExamCode.value != "-1") 
	{
		frm.sSubjectCode.value	= -1;
		frm.sMedium.value	= -1;
		frm.action="add_instructions.php";
		frm.submit();
	}
}
function onChangeSubject()
{
	var frm = document.email_add;
	frm.sMedium.value	= -1;
}
function onChangeMedium()
{
	var frm = document.email_add;
	if (frm.sMedium.value != "-1") 
	{
		frm.action="add_instructions.php";
		frm.submit();
	}
}
function cancel()
{
	var frm = document.email_add;
	frm.action="add_instructions.php";
	frm.submit();
} 
</script>
</head>
<BODY leftMargin=0 topMargin=0 marginheight=0 marginwidth=0>
<center>
<form name='email_add' method=post action="">
<input type="hidden" name="submit_flag" value="0">
<input type="hidden" name="hid_ta" value="<?$instruction?>">
<TABLE border=0 cellPadding=0 cellSpacing=0 width=780 background=images/tile.jpg >
	<!--<TR> Topnav Image 
        <TD width=780><IMG height=59 src="images/logo1.jpg" width=136><IMG height=59 src="images/logo2.gif" width=284><IMG height=59 src="images/logo3.gif" width=360></TD>
    </TR>-->
    <!-- <TR>
    	<TD bgColor=#7292c5 width=780>&nbsp;</TD>
    </TR> -->
	<tr><td width="780"><?include("includes/header.php");?></td></tr>
	<tr>
		<Td class="greybluetext10"><? include("admin_menu.php") ?></Td>	
	</tr>	
    <TR>
    	<TD vAlign=top width=780 align=left>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
 	    	   <tr>
    	       	<td align="center">
	    	      	<table width="80%" border="0" cellspacing="0" cellpadding="0">
 	    	       		<tr>
 	    	       	    	<td colspan=2>&nbsp;</td>
 	    	       	   	</tr>
						<tr>
                             <td align="Left" class="greybluetext10" valign=top>Exam Code  </td>
                             <td align="left" valign=top>:&nbsp;
						<?
								$sql="select exam_code,exam_name from iib_exam where online='Y' order by exam_name";
								$res=mysql_query($sql);
								if ($commonDebug)
								{
									echo	$sql."<br>";
								}
								if(!$res)
								{
									echo "Some problem in selecting records.";
									echo "<input type=\"hidden\" name=\"sExamCode\" value=\"-1\">";
								} 
								else 
								{
								?>
							   <select class="textbox" name=sExamCode onchange='javascript:onChangeExam();' style={width:150px}>
									<option value=-1>-Select-</option>
								<?	while(list($exam_code,$exam_name)=mysql_fetch_row($res))
									{
											$exam_name=stripslashes($exam_name);
											if($_POST['sExamCode']==$exam_code){
											echo "<option value=\"$exam_code\" selected>".$exam_code."->".$exam_name."</option>";
											$S_NAME=$exam_name;
											}
											else
											echo "<option value=\"$exam_code\">".$exam_code."->".$exam_name."</option>";
									}
									echo "</select>";
								}
						?>
									
                            </td>
                        </tr>
						<tr>
							<td align="left" class="greybluetext10" valign=top>
							Subject Code
							</td>
							<td align="left" valign=top>:&nbsp;
								  <?
									$sql_sub="SELECT subject_code,subject_name FROM iib_exam_subjects WHERE exam_code='".$_POST['sExamCode']."' and online='Y'";
									$res_sub = mysql_query($sql_sub);
									if ($commonDebug)
									{
										echo	$sql_sub."<br>";
									}
									if(!$res_sub)
									{
										echo "Some problem in selecting records.";
										echo "<input type=\"hidden\" name=\"sSubjectCode\" value=\"-1\">";
									} 
									else 
									{
								 ?>
									<select class="textbox" name="sSubjectCode" style={width:150px} onchange='javascript:onChangeSubject()'>
									   <option value=-1>-Select-</option>
									 <?
										while (list($subject_code,$subject_name)=mysql_fetch_row($res_sub))
										{							
											$subject_name=stripslashes($subject_name);
											if($_POST['sSubjectCode']==$subject_code) 
											{
												echo "<option value=\"$subject_code\" selected>".$subject_code."->".$subject_name."</option>";
											}
											else
											{
												echo "<option value=\"$subject_code\">".$subject_code."->".$subject_name."</option>";
											}
										}
										?>
									</select>
								<?}?>
								
							</td>
						</tr>
						<tr>
							<td align="left" class="greybluetext10" valign=top>
							Medium
							</td>
							<td align="left" valign=top> :&nbsp;
								<select class="textbox" name="sMedium" style={width:150px} onchange="javascript:onChangeMedium();">
									<option value=-1>-Select-</option>
									<option value="EN" <? if($_POST['sMedium']=="EN") echo "selected"; ?>>English</option>
									<option value="HI" <? if($_POST['sMedium']=="HI") echo "selected"; 
									?>>Hindi</option>
								</select>
							</td>
						</tr>
 	    	       	 	<tr>
							<td  class="greybluetext10" colspan=2><b>Instructions</b></td>
						</tr>
							<tr bgcolor="#D1E0EF" class="greybluetext10">
		                        <td colspan="2">
					    <?
									include("includes/editor/examples/editor.php");

					    ?>
		                        </td>
		                      </tr>

		                      <tr align="center" class="tablecontent1">
		                        <td height="50" colspan="4">
		                        <input name="Submit1" type="button" class="button" value="Save" onClick="javascript:Add();return false;">
		                          &nbsp;&nbsp;&nbsp;&nbsp;
		                          <input name="Submit2" type="button" class="button" value="Reset" onClick="javascript:cancel();">
		                          </td>
		                     </tr>
					   </table>
					  </td>
				</tr>
				<tr>
					<td valign="top">&nbsp;</td>
				</tr>
			</table>
 		</TD>
 	</TR>
	<? if($submitted=='1')
	{
	?>
	 <TR>
	     <TD>
			<table border="0" cellspacing="0" cellpadding="10" width="80%" align=center>
			<tr>
				<td colspan="3" class="greybluetext10" bgcolor="#D1E0EF" style={text-align:justify}><?=$view_instruction ?></td>
            </tr> 
				  
            </table> 
		</TD>
	  </TR>
	<?
	}
	?>
   	<TR>
    	<TD width=780><br><br></TD>
	</TR>
   	<!-- <TR>
    	<TD bgColor=#7292c5 width=780>&nbsp;</TD>
	</TR> -->
	<?include("includes/footer.php");?>
</TABLE>
</FORM>
</center>
</BODY>
</HTML>
</HTML>
