<?PHP /****************************************************
* Application Name            :  IIB 
* Module Name                 :  Exam Module
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  iib_candidate,iib_exam_subjects,iib_candidate_iway,iib_candidate_test
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              : 
* Created ON                  :  
* Last Modified By            :  Suresh.A
* Last Modified Date          :  26-10-2013
* Description                 :  Interface for the TA to restart candidate exam
*****************************************************/
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkTALogin();
require("constants.inc");

//Added for BOI
$sqlSelectTest = "SELECT membership_no , exam_code , subject_code , test_status from iib_candidate_test where current_session='Y'";
$resSelectTest = @mysql_query($sqlSelectTest);
$cntSelectTest = mysql_num_rows($resSelectTest);
if($cntSelectTest > 0)
{
	while(list($M_No,$E_Code,$SC,$TS)=mysql_fetch_row($resSelectTest))
	{
		$StatusArr[$M_No][$E_Code][$SC]=$TS;
	}
}

/*
 ** Function to fetch the status(IC/C/0) of the candidate whose exam code , subject code is being passed as an input along with membership no. The result will be returned to the called page.
 ** IC - Incomplete Status
 ** C - Complete Status
 ** 0 - Not taken the test
 */
function checkExamStatus($MemNo,$ExamCode,$SubCode)
{
	
	$return_value=0;
	global $StatusArr;
	
	if(is_array($StatusArr))
	{
				
		if(array_key_exists($SubCode,$StatusArr[$MemNo][$ExamCode]))
		{
			
			$return_value = $StatusArr[$MemNo][$ExamCode][$SubCode];			
			return $return_value;
		}
		else
		{
			//echo "hi1";
			return $return_value;
		}
	}
	else
	{
		//echo "bye";
		return $return_value;
	}
}

//Added for BOI ends here


$errormsg = "";
$membershipNo = trim($_POST['memno']);
$todaydate = date("Y-m-d");
$aExamCode = array();
$aSubCode = array();
$aTime = array();

if ($membershipNo != "")
{
$currTimeStamp = mktime(date("H"),date("i"), date("s"),date("m"),date("d"),date("Y"));
$sqlTime = "SELECT exam_time,subject_code,exam_code from iib_candidate_iway where  membership_no='$membershipNo' and exam_date='$todaydate' order by exam_time";
$resTime = @mysql_query($sqlTime,$SLAVECONN);
$nTime = @mysql_num_rows($resTime);
	
	if ($nTime > 0)
	{
				$nCntTime = 0;				
			    while (list($eTime,$suCode,$exCode) = mysql_fetch_row($resTime))
	        	{
		        	$aTime[$nCntTime] = $eTime;
					$aExamCode[$nCntTime] = $exCode;
					$aSubCode[$nCntTime] = $suCode;
					$aTmp = array();
		        	$aTmp = explode(":", $eTime);
					$aTimeStamp[$nCntTime] = mktime($aTmp[0],$aTmp[1],$aTmp[2],date("m"),date("d"),date("Y"));
		        	 $nCntTime++;
	        	} //end of while
	        	$check_array=array_unique($aTime);
	        	
				if ($nCntTime > 1)
	        	{
		        	for($l=0;$l<$nCntTime;$l++)
	        		{
											
										$SubjectStatus = checkExamStatus($membershipNo,$aExamCode[$l],$aSubCode[$l]);
										
												if($SubjectStatus == '0' || $SubjectStatus == 'IC')
												{
													//echo "hi";
													$selExamTime = $aTime[$l];
													$subject_code = $aSubCode[$l];
													$exam_code = $aExamCode[$l];
													$centre_code = $aCentreCode[$l];
													$break=true;
												}
												else
												{
													//echo "bye";
													$selExamTime = $aTime[$l];
													$subject_code = $aSubCode[$l];
													$exam_code = $aExamCode[$l];
													$centre_code = $aCentreCode[$l];
												}
												if($break) break;
												//Added for BOI ends here
						
					}

	        	} //end of time check
				else
				{
					$selExamTime = $aTime[0];
					$subject_code = $aSubCode[0];
					$exam_code = $aExamCode[0];
					
				}
				$_SESSION['sc'] = $subject_code;
				$_SESSION['ex'] = $exam_code;
				$_SESSION['et'] = $selExamTime;
		
		$todaydate = date("Y-m-d");
		$todayStart = $todaydate." 00:00:00";
		$todayEnd = $todaydate." 23:59:59";
		
		/* compare candidate test Lastupdatetime and answer last updated time if answer updatetime greater than test updated time need to update test update time with answer update time */
		
		$sqlSelect1 = " SELECT TIME_TO_SEC(DATE_FORMAT(last_updated_time,'%T')), test_id, test_status,question_paper_no,timeloss ".
		" FROM iib_candidate_test WHERE membership_no='$membershipNo' AND exam_code='$exam_code' AND subject_code='$subject_code' AND	 "." last_updated_time between '$todayStart' AND '$todayEnd'  "." order by last_updated_time desc LIMIT 1 ";
						
		$result1 = mysql_query($sqlSelect1,$SLAVECONN) or errorlog('err05'," Query:$sqlSelect1  ".mysql_error($SLAVECONN));
		if (mysql_num_rows($result1) > 0)
		{
			list($lastupdatedtime, $testID, $testStatus,$question_paper_no,$timeloss) = mysql_fetch_row($result1);
			$_SESSION['timeloss'] = $timeloss;
			if ($testStatus == 'IC')
			{						
				 $sqlSelect2 = " SELECT TIME_TO_SEC(DATE_FORMAT(updatedtime,'%T')) , updatedtime FROM iib_response WHERE "."question_paper_no='$question_paper_no' order by updatedtime desc LIMIT 1 ";
				 $result2 = mysql_query($sqlSelect2,$SLAVECONN) or errorlog('err05'," Query:$sqlSelect2  ".mysql_error($SLAVECONN));
				 list($answerupdatedtime,$answerupdatedtime1) = mysql_fetch_row($result2);
				 if($answerupdatedtime > $lastupdatedtime)
				 {
				 	$sqlUpdate = " UPDATE iib_candidate_test set last_updated_time='$answerupdatedtime1' where test_id='$testID'";
					mysql_query($sqlUpdate,$MASTERCONN)  or errorlog('err06'," QUERY:$sqlUpdate  ".mysql_error($MASTERCONN));
					/*if(mysql_affected_rows($MASTERCONN) == 0)
					{
						errorlog('err06'," QUERY:affeted rows 0:$sqlUpdate ".mysql_error($MASTERCONN));
					}*/
				 }				 				
			 }
		}					
		$sqlSelect = " SELECT TIME_TO_SEC(DATE_FORMAT(last_updated_time,'%T')) ".
		" - TIME_TO_SEC(DATE_FORMAT(start_time,'%T')), test_id, total_time, test_status,time_extended,timeloss ".
		" FROM iib_candidate_test WHERE membership_no='$membershipNo' AND exam_code='$exam_code' AND subject_code='$subject_code' AND
		 "." last_updated_time between '$todayStart' AND '$todayEnd' "." order by last_updated_time desc LIMIT 1 ";
											
		$result = @mysql_query($sqlSelect,$MASTERCONN) or errorlog('err05'," Query:$sqlSelect  ".mysql_error($MASTERCONN));
		
		if (mysql_num_rows($result) > 0)
		{
			list($timeTaken, $testID, $totalTime, $testStatus,$time_extended,$timeloss) = mysql_fetch_row($result);
			if ($testStatus == 'IC')
			{						
				 $timeLeft = ($totalTime+$time_extended+$timeloss) - $timeTaken;
				 if($timeLeft < 0)
				 	$timeLeft = 0;
				  $dispTime=sec2hms($timeLeft);
			}
			else
			{				
				redirect_taoption(7);				
			}		
		}
		else
		{					
			 redirect_taoption(6);
		}
	} // end of nTime
	else
	{
		redirect_taoption(5);
	}
} // end of memno

$includeJS = '<script language="javascript" type="text/javascript" src="./includes/browser.js"></script>';
$includeJS .= '<script language="javascript" src="./includes/validations.js"></script>';
$includeJS .= <<<JSSC
<script language="javascript">
function validate()
{
	Memno=document.loginfrm.memno.value;
	Memno=trim(Memno);
	
	if(Memno.indexOf(' ')!=-1)
	{
		alert('Member Id Cannot Contain Space');
		document.loginfrm.memno.focus();
		return false;
	}
	document.loginfrm.memno.value=Memno;
	LeftTime=document.loginfrm.hid_lefttime.value	
	if(Memno=='' || LeftTime=='')
	{
		alert('Member Id Or Time Empty!');
		document.loginfrm.memno.focus();
		return false;
	}
	document.getElementById("sub_form").disabled=true;
	document.getElementById("submitstatus").innerHTML='please wait submitting form...';

	document.loginfrm.action='logincandidate.php';	
	document.loginfrm.submit();
}
function captureEnter(h)
{
	var g = (navigator.appName == "Netscape") ? 1 : 0;
   	h = (g) ? h : window.event;
	if (h.keyCode == 13 || h.keyCode == 9)
	{
		document.forms[0].submit();
	}
}
function setFocus()
{
	document.forms[0].memno.focus();	
}
</script>
JSSC;

	$bodyOnload =' onLoad="javascript:setFocus()" onKeyDown="return doKeyDown(event);"  ondragstart ="return Stop(event)" onselectstart="return Stop(event)"onmousedown="return Stop(event)" onmouseup="return Stop(event)"';
	include("themes/default/header.php");

?>
<div id="body_container_inner">
      <div class="innerbox_container_calogin_wrapper"><div class="outerbox">
	<div class="innerbox">
    	<div class="innerbox_container_calogin">			
        	<div class="pager_header_common">Restart Exam Time Details</div>
            <div class="content_container_calogin">
            	<!--<div class="content_container_box_calogin" >-->
				<form name='loginfrm' method="post">
					<input type="hidden" name="hid_lefttime" value="<?PHP echo $timeLeft?>">
					<input type='hidden' name='ta_override' value='Y'>
            	 <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" id="content_container_box_calogin_inner_tb" >
				 <tr>
					  <td colspan="2"><b>Fill the Membership No. and press Tab</b></td>
					</tr>
				  <tr>
					<td width="37%"><label>Membership Number</label></td>
					<td width="63%"><input class="textbox" name="memno" type="text" id="memno" value="<?=$membershipNo ?>" onkeydown='javascript:captureEnter(event)' onkeypress="return letternumber(event)" onchange='document.forms[0].submit()'  maxlength="20"  <?php if($dispTime != "") echo "readonly"; ?> /></td>
				  </tr>
				  <tr>
					<td width="37%"><label>Exam Time Left:</label></td>
					<td width="63%"><input name="timeleft" type="hidden" id="timeleft" value='<?=$dispTime ?>' />
                     <b><?=$dispTime ?>
                                  <?php ($dispTime != "") ? print(" hrs") : print(""); ?>
                                </b></td>
				  </tr>
				 </table> 
			 </form>
            	<!--</div>-->
            </div>
            <div class="button_container_common">																		
			<input name="reset3" id='sub_form' class="green_button" type="button" value="Submit"  style="margin-right:6px;" onclick="javascript:validate();" />
			<input name="sub_view2" id='sub_view' class="green_button" type="button" value="Back" onclick="javascript:window.location='ta_options.php'" />
			<br/><span id="submitstatus" class="errormsg"></span>
            </div>
        </div>
    </div>
</div></div>
    </div>
	<?php include("themes/default/footer.php");
//mysql_close($MASTERCONN);
//mysql_close($SLAVECONN);
?>
