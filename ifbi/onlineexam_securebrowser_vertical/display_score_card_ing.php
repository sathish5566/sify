<?php
/****************************************************
* Application Name            :  IIB 
* Module Name                 :  Exam Module
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  iib_candidate,iib_exam_subjects,iib_candidate_iway,iib_candidate_test
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              : 
* Created ON                  :  
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  22-01-2010
* Description                 :  Interface for the TA to candidate  view score card
*****************************************************/
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");  // always modified
header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");                          // HTTP/1.0
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkTALogin();
require("constants.inc");

	
	//print_r($_POST);
	$todaydate = date("Y-m-d");
	$todate_start = $todaydate." 00:00:00 ";
	$todate_end = $todaydate. " 23:59:59 ";
	$membershipNo = $_POST['memno'];	
	
    $sqlSelect = " SELECT exam_code, subject_code, score, time_taken, result FROM iib_candidate_scores WHERE membership_no='$membershipNo' ".
					   " AND exam_date between '$todate_start' and '$todate_end' ORDER BY exam_date desc limit 1";	
	
	if ($commonDebug)
	{
		print("\n".$sqlSelect);
	}
	
  	$resSelect = @mysql_query($sqlSelect,$SLAVECONN) or errorlog('err05'," QUERY:$sqlSelect ".mysql_error($SLAVECONN));
  	list($examCode, $subjectCode, $score, $timeTaken, $result) = mysql_fetch_row($resSelect);
  	
  	$sqlSubject = "SELECT subject_name, pass_mark, grace_mark, total_marks , display_score, display_result, pass_msg, fail_msg,result_type FROM iib_exam_subjects WHERE ".
		" subject_code='$subjectCode' AND exam_code='$examCode'";
	if ($commonDebug)
	{
		print("\n".$sqlSubject);
	}
	
	$resSubject = @mysql_query($sqlSubject,$SLAVECONN) or errorlog('err05'," QUERY:$sqlSubject  ".mysql_error($SLAVECONN));
	$row = mysql_fetch_array($resSubject);
	
	$subjectName = $row['subject_name'];
	$passMark = $row['pass_mark'];
	$graceMark = $row['grace_mark'];
  	$totalMarks = $row['total_marks'];
  	$display_scr = $row['display_score'];
  	$display_res = $row['display_result'];
  	$passmsg = $row['pass_msg'];
  	$failmsg = $row['fail_msg'];
	$resultType = $row['result_type'];
  	  	  	
  	$score = round($score);
  	
  	if ($score <= 0)
  	{
	  	$percentage = 0;
  	}
  	else
  	{
	  	$percentage = round((($score+0.0) / $totalMarks) * 100);
  	}
  	if ($commonDebug)
  	{
  		print("<br>score = ".$score);
  		print("<br>total marks = ".$totalMarks);
  		print("<br>percentage = ".$percentage);
  		print("<br>pass mark = ".$passMark);
  		print("<br>grace mark = ".$graceMark);
	}
	if ($result == 'P')
	{	
		if($resultType == 'PA')
			$displayResult = 'Pass';
		elseif($resultType == 'PR')
			$displayResult = 'Provisional';
		$msg=$passmsg;
	}
	else
	{		
		if($resultType == 'PA')
			$displayResult = 'Fail';
		elseif($resultType == 'PR')
			$displayResult = 'Provisional';
		$msg=$failmsg;
	}

  	$sqlMember = "SELECT name, address1, address2, address3, address4, address5, address6, pin_code FROM iib_candidate WHERE ".
  		"membership_no='$membershipNo'";
	if ($commonDebug)
	{
		print("\n".$sqlMember);
	}
	
	$resMember = @mysql_query($sqlMember,$SLAVECONN) or errorlog('err05'," iib_candidate  ".mysql_error($SLAVECONN));
	$row = mysql_fetch_array($resMember);
	
	$memberName = $row['name'];
	$c_addr1 = $row['address1'];
	$c_addr2 = $row['address2'];
	$c_addr3 = $row['address3'];
	$c_addr4 = $row['address4'];

	$c_addr5 = $row['address5'];
	$c_addr6 = $row['address6'];
	$c_pin = $row['pin_code'];
	
	$memberAddress = "";
	if ($c_addr1 != "")
		$memberAddress .= $c_addr1;
	if ($memberAddress != "") 
		$memberAddress .= " ";
	if ($c_addr2 != "")
		$memberAddress .= $c_addr2;
	if (($memberAddress != "") && ($c_addr2 != ""))
		$memberAddress .= " ";
	if ($c_addr3 != "")
		$memberAddress .= $c_addr3;
	if (($memberAddress != "") && ($c_addr3 != ""))
		$memberAddress .= " ";
	if ($c_addr4 != "")
		$memberAddress .= $c_addr4;
	if (($memberAddress != "") && ($c_addr4 != ""))
		$memberAddress .= " ";
	if ($c_addr5 != "")
		$memberAddress .= $c_addr5;
	if (($memberAddress != "") && ($c_addr5 != ""))
		$memberAddress .= " ";
	if ($c_addr6 != "")
		$memberAddress .= $c_addr6;
	if (($memberAddress != "") && ($c_addr6 != ""))
		$memberAddress .= " ";	
	if ($c_pin != "")
		$memberAddress .= $c_pin;		
	
	$sqlExam = "SELECT exam_name FROM iib_exam WHERE exam_code='$examCode'";
	if ($commonDebug)
	{
		print("\n".$sqlExam);
	}
	
	$resExam = @mysql_query($sqlExam,$SLAVECONN) or errorlog('err05'," iib_exam  ".mysql_error($SLAVECONN));
	$row = mysql_fetch_array($resExam);
	
	$examName = $row['exam_name'];
	
	$sqlCentre = "SELECT centre_code, DATE_FORMAT(exam_date, '%d/%m/%Y') examdate, ".
		" TIME_FORMAT(exam_time, '%H:%i') examtime FROM iib_candidate_iway WHERE ".
		" subject_code='$subjectCode' AND exam_code='$examCode' AND membership_no='$membershipNo' ";
	
	if ($commonDebug)
	{
		print("\n".$sqlCentre);
	}
	
	$resCentre = @mysql_query($sqlCentre,$SLAVECONN) or errorlog('err05'," iib_candidate_iway  ".mysql_error($SLAVECONN));
	$row = mysql_fetch_array($resCentre);
	
	$centreCode = $row['centre_code'];
	$examDate = $row['examdate'];
	$examTime = $row['examtime'];
	
	$sqlIWay = "SELECT iway_address1, iway_address2, iway_city, iway_state, iway_pin_code FROM iib_iway_details ".
		" WHERE centre_code='$centreCode'";
	if ($commonDebug)
	{
		print("\n".$sqlIWay);
	}
	$resIWay = @mysql_query($sqlIWay,$SLAVECONN) or errorlog('err05'," iib_iway_details  ".mysql_error($SLAVECONN));
	$row = mysql_fetch_array($resIWay);
	$iwayAddress = "";
	$row['iway_address1'] != "" ? $iwayAddress .= " ".$row['iway_address1'] : $iwayAddress .= "";
	//$row['iway_address2'] != "" ? $iwayAddress .= " ".$row['iway_address2'] : $iwayAddress .= "";
	$row['iway_city'] != "" ? $iwayAddress .= " ".$row['iway_city'] : $iwayAddress .= "";
	$row['iway_pin_code'] != "" ? $iwayAddress .= " ".$row['iway_pin_code'] : $iwayAddress .= "";
	$row['iway_state'] != "" ? $iwayAddress .= " ".$row['iway_state'] : $iwayAddress .= "";
	$row['iway_address2'] != "" ? $iwayAddress .= ",Contact No. :  ".$row['iway_address2'] : $iwayAddress .= "";
	
	if($msg != '')
	{					
		// Constant for displaying the time in the score card message
	     if($resultType == 'PR')
		{
		   $Constants_msg=array(
		       "/\bREP_MEMBERSHIPNAME\b/",
		       "/\bREP_SCORE\b/",
		       "/\bREP_TOTALMARK\b/",
		       "/\bREP_SUBJECTNAME\b/"
		     );
			 $replace_msg=array(
		       $memberName,
		       $score,
		       $totalMarks,
		       $subjectName
		     );
		}
		elseif($resultType == 'PA')
		{
			 $Constants_msg=array(
		       "/\bREP_MEMBERSHIPNAME\b/",
		       "/\bREP_SCORE\b/",
			   "/\bREP_TOTALMARK\b/",
		       "/\bREP_SUBJECTNAME\b/"
		     );
			 $replace_msg=array(
		       $memberName,
		       $score,
				$totalMarks,
		       $subjectName
		     );
		}

		   
		     $display_msg=preg_replace($Constants_msg,$replace_msg,$msg); 
		
	}
	else
	{
		$not_rep_msg=true;
	}

	if($not_rep_msg)
	$display_msg=$msg;
	
	
	
	$sql_select2 = "SELECT question_paper_no FROM iib_question_paper where membership_no='$membershipNo' AND exam_Code='$examCode' AND subject_code='$subjectCode'";
	$resSql2 = @mysql_query($sql_select2,$SLAVECONN)  or errorlog('err05'," SQL:$sql_select2 ".mysql_error($SLAVECONN));
	list($questionPaperNo) = mysql_fetch_row($resSql2);
	  		
	$sql_select3 = "SELECT display_order,answer FROM iib_question_paper_details where question_paper_no='$questionPaperNo' order by display_order";
	$resSql = @mysql_query($sql_select3,$SLAVECONN)  or errorlog('err05'," SQL:$sql_select3 ".mysql_error($SLAVECONN));
	 $sqlct=mysql_num_rows($resSql);	
?>
<html>
<head>
	<title><?PHP echo TITLE?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="images/iibf.css" rel="stylesheet" type="text/css">
<script language="javascript">
	function showhide(id, id1)
	 {
		 obj=document.getElementById(id);
		 obj1=document.getElementById(id1);
		 
		 obj.style.display="none"
		 obj1.style.display="";
	 }	 	
	 function printdoc(){
	 	window.print();
	 }	 	 
	 function screenclose(){
	 window.close();
	 }
 </script>
</head>


<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad='this.focus()'>
	<table width="640" border="0" cellspacing="0" cellpadding="0">
		<tr> 
			<td  bgcolor="#014A7F"><img src="./images/logo1.jpg" alt="IBPS"><img src="./images/logo2.jpg" alt="IBPS" ></td>
		</tr>
		<tr>
			<td width="640" bgcolor="7292C5">&nbsp;</td>
		</tr>
	</table>
<div id="responsesheet" style="display:block">
	<table border="0" cellspacing="0" cellpadding="0" width="640" background="images/tile.jpg">
		<tr>
			<td>
			<br>
			<table border="0" cellspacing="0" cellpadding="10" width="75%" align=center>
				<tr>
					<td colspan="3" class="greybluetext10" bgcolor="#D1E0EF"><b class="arial11a">Candidate Exam Result</b></td>
				</tr>
				<tr> 
					<td width="35%" class="greybluetext10" bgcolor="#E8EFF7"><b>Membership No</b></td>
					<td width="2%" class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
					<td width="53%" bgcolor="#E8EFF7" class="greybluetext10"><?=$membershipNo ?></td>
				</tr>
				<tr> 
					<td class="greybluetext10" bgcolor="#E8EFF7"><b>Name</b></td>
					<td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
					<td bgcolor="#E8EFF7" class="greybluetext10"><?=$memberName ?></td>
				</tr>
				<tr> 
					<td class="greybluetext10" bgcolor="#E8EFF7"><b>Address</b></td>
					<td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
					<td bgcolor="#E8EFF7" class="greybluetext10"><?=$memberAddress ?></td>
				</tr>
				<tr> 
					<td class="greybluetext10" bgcolor="#E8EFF7"><b>Examination</b></td>
					<td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
					<td bgcolor="#E8EFF7" class="greybluetext10"><?=$examName ?></td>
				</tr>
				<tr> 
					<td class="greybluetext10" bgcolor="#E8EFF7"><b>Subject Name</b></td>
					<td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
					<td bgcolor="#E8EFF7" class="greybluetext10"><?=$subjectName ?></td>
				</tr>
				<tr> 
					<td class="greybluetext10" bgcolor="#E8EFF7"><b>Examination Centre</b></td>
					<td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
					<td bgcolor="#E8EFF7" class="greybluetext10"><?=$iwayAddress ?></td>
				</tr>
				<tr> 
					<td class="greybluetext10" bgcolor="#E8EFF7"><b>Examination Date</b></td>
					<td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
					<td bgcolor="#E8EFF7" class="greybluetext10"><?=$examDate ?></td>
				</tr>
				<tr> 
					<td class="greybluetext10" bgcolor="#E8EFF7"><b>Examination Time</b></td>
					<td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
					<td bgcolor="#E8EFF7" class="greybluetext10"><?php if ($examTime != "") print($examTime." Hrs."); ?></td>
				</tr>
				<?php if($display_scr == 'Y')
				{
				?>
					<tr> 
						<td class="greybluetext10" bgcolor="#E8EFF7"><b>Score</b></td>
						<td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
						<td bgcolor="#E8EFF7" class="greybluetext10"><?=$score ?></td>
					</tr>
				<?
				}
				if($display_res == 'Y')
				{
				?>	
				<tr> 
					<td class="greybluetext10" bgcolor="#E8EFF7"><b>Result</b></td>
					<td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
					<td bgcolor="#E8EFF7" class="greybluetext10"><?=$displayResult ?></td>
				</tr>
				<? }?>				
				<tr>
					<td class="greybluetext10" bgcolor="#E8EFF7" colspan="3">
				
					<!-- Final results will be displayed once your result is reviewed by IBPS.<br>-->
				 <b> <br>Thank you for taking the Online Examination.</b></span></td>
				</tr>
			</table>			</td>
		</tr>
		<tr>
			<td><br></td>
		</tr>
		<tr>
		  <td>
		  
		  </td>
	  </tr>
		<tr>
		  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="CellTextLight"><table width="100%" border="1" cellspacing="0" cellpadding="2">
                <tr>
                  <td height="32" colspan="10" bgcolor="#D1E0EF" class="arial11a"><strong>Response Report</strong></td>
                </tr>
                <tr>
                  <td width="5%" class="CellTextLight"><strong>Q.No</strong></td>
                  <td width="15%" class="CellTextLight"><strong>Answer</strong></td>
                  <td width="5%" class="CellTextLight"><strong>Q.No</strong></td>
                  <td width="15%" class="CellTextLight"><strong>Answer</strong></td>
                  <td width="5%" class="CellTextLight"><strong>Q.No</strong></td>
                  <td width="15%" class="CellTextLight"><strong>Answer</strong></td>
                  <td width="5%" class="CellTextLight"><strong>Q.No</strong></td>
                  <td width="15%" class="CellTextLight"><strong>Answer</strong></td>
                  <td width="5%" class="CellTextLight"><strong>Q.No</strong></td>
                  <td width="15%" class="CellTextLight"><strong>Answer</strong></td>
                </tr>
                <?PHP 
					$attempt_cnt=0;
					$unattempt_cnt=0;
					$v=$sqlct % 5;					
					$i=1;
					$j=1;
					while($repData = mysql_fetch_array($resSql))
			  		{			   
						 $dis_order=$repData['display_order'];
						 if($repData['answer']==''){ 
						  		$dis_answer='--';
							 	$unattempt_cnt++;
						 }else{
						  	 	$dis_answer=$repData['answer'];
						    	$attempt_cnt++;
						  } 
						 if($i==1){
						  	echo '<tr><td width="5%" class="CellTextLight">'.$dis_order.'</td><td width="15%" class="CellTextLight">'. $dis_answer.'</td>';
							$i++;
							if($j==$sqlct && $v==1)
							{
								echo '<td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  </tr>';
							}
								
						  }else if($i==2){
						  	echo '<td width="5%" class="CellTextLight">'.$dis_order.'</td><td width="15%" class="CellTextLight">'. $dis_answer.'</td>';
							$i++;
							if($j==$sqlct && $v==2)
							{
								echo '<td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
								      <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  </tr>';
							}
								
						  }else if ($i==3){ 						  
						  	echo '<td width="5%" class="CellTextLight">'.$dis_order.'</td><td width="15%" class="CellTextLight">'. $dis_answer.'</td>';
						    $i++;
							if($j==$sqlct && $v==3)
							{
								echo '<td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
								      <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  </tr>';
							}
						  }else if ($i==4){ 						  
								echo '<td width="5%" class="CellTextLight">'.$dis_order.'</td><td width="15%" class="CellTextLight">'.$dis_answer.'</td>';
								$i++;
								if($j==$sqlct && $v==4)
								{
									echo '<td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									      <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
										  </tr>';
								}
						  }
						  else if ($i==5){ 						  	
							   echo '<td width="5%" class="CellTextLight">'.$dis_order.'</td><td width="15%" class="CellTextLight">'.$dis_answer.'</td>';
							   echo '</tr>';
							   $i=1;							   
	   	                  }	
						 $j++; 					  						 					  										
                 	}
					
										
					?>
              </table></td>
            </tr>
            <tr>
              <td class="CellTextLight">&nbsp;</td>
            </tr>
          </table></td>
	  </tr>
		<tr>
			<td class="CellTextLight">
			  <b>Total number of questions attempted:
              <?=$attempt_cnt?>
		    </b></td>
		</tr>
		<tr>
		  <td class="CellTextLight"><b>Total number of questions unattempted:
              <?=$unattempt_cnt?>
		  </b></td>
	  </tr>
		<Tr>
		  <td><div align="center">
		    <input type=button class=button name='sub_exit' value='Print' onClick="printdoc();return(false);">
		    <input type=button class=button name='sub_exit2' value='Close' onClick="javascript:window.close()">
		    <input type=button class=button name='sub_exit3' value='Score Card' onClick="showhide('responsesheet', 'scorecarddiv')">
	      </div></td>
		</Tr>
		<tr>
			<td><br><br></td>
		</tr>
	</table>
	<table border="0" cellpadding="0" cellspacing="0" width="640">
		<tr>
			<?php include("includes/footer.php");?>
		</tr>
	</table>
	</div>
	<div id="scorecarddiv" style="display:none">
		<table border="0" cellspacing="0" cellpadding="0" width="600" background="images/tile.jpg" align="left">
		<tr>
			<td>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
				<tr>
					<td colspan="3" class="greybluetext10" bgcolor="#D1E0EF" align=center><b class="arial11a">Candidate Exam Result </b></td>
				</tr>
				<tr>
					<td width="35%" class="greybluetext10" bgcolor="#E8EFF7"><b>Membership No</td>
					<td width="2%" class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
					<td width="53%" bgcolor="#E8EFF7" class="greybluetext10"><?=$membershipNo ?></td>
				</tr>
				<tr>
					<td class="greybluetext10" bgcolor="#E8EFF7"><b>Name</b></td>
					<td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
					<td bgcolor="#E8EFF7" class="greybluetext10"><?=$memberName ?></td>
				</tr>
				<tr>
                  <td class="greybluetext10" bgcolor="#E8EFF7"><b>Address</b></td>
				  <td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
				  <td bgcolor="#E8EFF7" class="greybluetext10"><?=$memberAddress ?></td>
			  </tr>
				<tr>
                  <td class="greybluetext10" bgcolor="#E8EFF7"><b>Examination </b></td>
				  <td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
				  <td bgcolor="#E8EFF7" class="greybluetext10"><?=$examName?></td>
			  </tr>
				<tr>
                  <td class="greybluetext10" bgcolor="#E8EFF7"><b>Subject Name </b></td>
				  <td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
				  <td bgcolor="#E8EFF7" class="greybluetext10"><?=$subjectName ?></td>
			  </tr>
				<tr>
					<td class="greybluetext10" bgcolor="#E8EFF7"><b>Examination Centre</b></td>
					<td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
					<td bgcolor="#E8EFF7" class="greybluetext10"><?=$iwayAddress ?></td>
				</tr>
				<tr>
                  <td class="greybluetext10" bgcolor="#E8EFF7"><b>Examination Date </b></td>
				  <td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
				  <td bgcolor="#E8EFF7" class="greybluetext10"><?=$examDate ?></td>
			  </tr>
				<tr>
                  <td class="greybluetext10" bgcolor="#E8EFF7"><b>Examination Time </b></td>
				  <td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
				  <td bgcolor="#E8EFF7" class="greybluetext10"><?=$examTime ?></td>
			  </tr>
				<tr>
                  <td class="greybluetext10" bgcolor="#E8EFF7"><b>Score</b></td>
				  <td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
				  <td bgcolor="#E8EFF7" class="greybluetext10"><?=$score ?></td>
			  </tr>
				
				<tr>
				  <td colspan="3" bgcolor="#E8EFF7" class="greybluetext10">Final results will be declared once the result is reviewed by the Bank.</td>
			  </tr>
			</table>
			</td>
		</tr>		
		<Tr>
			<td>
			<table border=0 width=640>
				<tr>
					<td><img src="images/spacer.gif" width=273 height=17></td>
					<td background="images/print.gif"><a href="" onClick="printdoc();return(false);"><img src="images/spacer.gif" width=38 height=17 border=0></a></td>
					<td background="images/exit.gif"><a href="javascript:screenclose()"><img src="images/spacer.gif" width=29 height=17 border=0></a></td>
					<td><img src="images/spacer.gif" width=273 height=17></td>
				</tr>
			</table>
			</td>
		</Tr>
		<tr>
			<td><br><br></td>
		</tr>
	</table>
	
</div>	
	<br>
</body>
</html>
<?php
//mysql_close($SLAVECONN);

?>
