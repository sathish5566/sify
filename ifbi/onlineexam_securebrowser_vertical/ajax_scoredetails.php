<?php
require_once("dbconfig.php");
require("constants.inc");
require_once("login_tracking_functions.php");
masterConnect();
slaveConnect();


$questionPaperNo = $_GET['qpno'];
$subjectCode = $_GET['scode'];
$examCode = $_GET['ecode'];
$membershipNo = $_GET['memno'];
$passMark = $_GET['pmark'];


/*************************************** SCORE CALCULATION START *****************************************************/ 

  	$score = 0;	
  	$posscore = 0;	
  	$negscore = 0;

	$sqlMaxIds = "select  max(id) from iib_response where question_paper_no ='$questionPaperNo' group by question_id";
        $resMaxIds = mysql_query($sqlMaxIds);
        $arrMaxId = array();
        while(list($maxId) = @mysql_fetch_row($resMaxIds)) {
                $arrMaxId[] = $maxId;
        }
        $maxIds = implode(',', $arrMaxId);
	if($maxIds == "" )
		$maxIds = "''";
			
	$sql_select = "SELECT SUM(A.marks) FROM iib_section_questions A  LEFT JOIN iib_response B ON A.question_id=B.question_id where B.question_paper_no ='$questionPaperNo' AND B.id IN ($maxIds)  AND A.correct_answer=B.answer";	
			
	$sql_select1 = "SELECT SUM(A.negative_marks) FROM iib_section_questions A  LEFT JOIN iib_response B ON A.question_id=B.question_id where B.question_paper_no='$questionPaperNo' AND B.id IN ($maxIds) AND A.correct_answer!=B.answer AND B.answer!=''";

		$res_select = mysql_query($sql_select,$MASTERCONN)  or errorlog('err05'," QUERY:$sql_select  ".mysql_error($MASTERCONN)); 
		list($posscore)=mysql_fetch_row($res_select);
		
		$res_select1 = mysql_query($sql_select1,$MASTERCONN) or errorlog('err05'," QUERY:$sql_select1  ".mysql_error($MASTERCONN)); 
		list($negscore)=mysql_fetch_row($res_select1);		
			
	
	if(isset($negscore) && $negscore > 0)	
	{
		$score = $posscore - $negscore;		
	}else
	  $score = $posscore ;	  					
  	$score = round($score);
  	if ($score == -0)
  		$score = 0;
	//code added to convert negative marks to 0
	if ($score < 0)
		$score = 0;

	if ($score >= $passMark)
	{
		$examResult = 'P';
	}
	else
	{		
		$examResult = 'F';
	}
		  	
    $sql_score_check = "SELECT score,result,count(1) from iib_candidate_scores where membership_no='$membershipNo' AND exam_code='$examCode' AND subject_code='$subjectCode'";
	$res_score_check = mysql_query($sql_score_check,$SLAVECONN) or errorlog('err05'," QUERY:$sql_score_check  ".mysql_error($MASTERCONN)); 
	list($extscore,$extresult,$cnt_score_chk) = mysql_fetch_row($res_score_check);
	if($cnt_score_chk==1 && is_null($extscore) && is_null($extresult))
	{
		$timestamp = date("Y-m-d H:i:s");
		$final_qry = "insert into iib_response_final select '',question_paper_no, question_id, answer, display_order, host_ip, updatedtime from iib_response where question_paper_no='".$questionPaperNo."' AND id IN ($maxIds) order by display_order";
		mysql_query($final_qry,$MASTERCONN); 	

		$sqlUpdate = "UPDATE iib_candidate_scores set score='$score',result='$examResult',updated_on='$timestamp' where membership_no='$membershipNo' and exam_code='$examCode' AND subject_code='$subjectCode'";
		mysql_query($sqlUpdate,$MASTERCONN) or errorlog('err06'," QUERY:$sqlUpdate   ".mysql_error($MASTERCONN)); 	
		echo $score."|".$examResult;
	}elseif($cnt_score_chk==1 && !is_null($extscore) && !is_null($extresult))
	{
		echo $extscore."|".$extresult;
	}
  /*************************************** SCORE CALCULATION END *****************************************************/
//mysql_close($MASTERCONN);
//mysql_close($SLAVECONN);

?>
