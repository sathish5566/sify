<?php require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkTALogin();
require("member_functions.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?PHP echo TITLE?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<script language='JavaScript' src="./includes/validations.js"></script>
<script language='JavaScript'>
function validate()
{
	Memno=document.loginfrm.memno.value;
	Memno=trim(Memno);

	if (Memno == '')
	{
		alert('Please enter Member ID!');
		document.loginfrm.memno.focus();
		return;
	}
	
	if (Memno.indexOf(' ') != -1)
	{
		alert('Member ID Cannot Contain Space');
		document.loginfrm.memno.focus();
		return;
	}
	if (document.loginfrm.exam_time.selectedIndex == 0)
	{
		alert('Please select the exam time');
		document.loginfrm.exam_time.focus();
		return false;
	}
	
	document.loginfrm.action="<?=$_SERVER['PHP_SELF'];?>";	
	document.loginfrm.memno.value=Memno;	
	document.loginfrm.submit();	
}

</script>
<script language='javascript'>
	function validateFeedback(cnt)
	{
		frm = document.formf;
		isChecked = false;

		for(i=1;i<cnt;i++)
			if(eval('frm.reason'+i+'.checked'))
				isChecked = true;
				
		if (!isChecked && (frm.otherReason.value==''))
		{
			alert('Please select a reason')
			frm.reason1.focus();
			return false;
		}
		val = trim(frm.otherReason.value);
		strlen = val.length;
		if (strlen > 255)
		{
			alert('Other Reasons text can contain maximim of only 255 characters');
			frm.otherReason.focus();
			return false;
		}
		frm.submit();
	}
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad='document.loginfrm.memno.focus()'>

<center>
<?php
	$taLogin = $_SESSION['login'];
	$centreCode =  getCentre($taLogin);
	$membershipNo = $_POST['memno'];
	$examTime = $_POST['exam_time'];
	$examDate = date("Y-m-d");
	$dispDate = date("d-m-Y");
	
?>
<?php
require("topnav.php");
require("formmenu.php");
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" > 
  <tr> 
    <td width="100%" background="images/tile.jpg" >	  
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          
          <td>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr> 
              	<td class="greybluetext10" colspan="2" height="35"  ><b><?=$errormsg ?></b></td>                
              </tr>
              <tr> 
                <td> 
                <form name="loginfrm" method="post" >
					<input type="hidden" name="ta_login" value='<?=$taLogin ?>'>
					<input type="hidden" name="submitted" value='<?=$submitted ?>'>
					<table width="25%" border="0" align="center" cellpadding="1" cellspacing="0">
                      <tr>
                        <td bgcolor="#014A7F"><table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">
                            <tr>
                              <td height="35" colspan="2" align="left" valign="middle" bgcolor="#D1E0EF" class="ColumnHeader1"><b>Format F</b></td>
                            </tr>
                            <tr>
                              <td class="greybluetext10" bgcolor="#E8EFF7" height="40" width="125"><b>Membership No: </b></td>
                              <td bgcolor="#E8EFF7" height="40" width="225" class="greybluetext10"><input class="textbox" name="memno" type="text" id="memno" value="<?=$membershipNo ?>" onkeypress="return letternumber(event)" maxlength="20" />
                              </td>
                            </tr>
                            <tr>
                              <td class="greybluetext10" bgcolor="#E8EFF7" height="40" width="125"><b>Exam Time: </b></td>
                              <td bgcolor="#E8EFF7" height="40" width="225" class="greybluetext10"><select name='exam_time' class='greybluetext10' id="exam_time">
                                  <option value=''>--Select--</option>
                                  <?php
                          $aSlots = getSlots();
                          if (count($aSlots) > 0)
                          {
	                          foreach ($aSlots as $slotTime)
	                          {
		                          $sTime=substr($slotTime,0,5);
		                          print("<option value='$slotTime' ");
		                          if ($examTime == $slotTime)
		                          {
			                          print(" selected ");
		                          }
		                          print(">$sTime</option>");
	                          }
                          }
                          ?>
                                </select>
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" align="center" bgcolor="#D1E0EF" height="35"><input class='button' id='reset3' type='button' value='Submit' name="sub_view" onclick='javascript:validate();' />
                              </td>
                            </tr>
                        </table></td>
                      </tr>
                    </table>
                </form></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
  	<td background="images/tile.jpg"><br><br></td>
  </tr>
  <?php
 if (($membershipNo != "") && ($examTime != ""))
 {
	 $subjectName = getSubject($membershipNo,$examDate,$examTime,$centreCode);
	 $msg = "";
 	$formatType='F';
 	$memberDetails = getMemberDetails($membershipNo);
 	$memberName = $memberDetails[0];
 	$memberAddress = $memberDetails[1];
 	$memDetails = $memberName."<br>".$memberAddress;

	$iwayAddress = getIwayDetails($centreCode);
	$submitted = $_POST['submitted'];
	if ($submitted == 'Y')
	{
		
		$reason = "";
		$reason1 = $_POST['reason1'];
		$reason2 = $_POST['reason2'];
		$reason3 = $_POST['reason3'];
		$reason4 = $_POST['reason4'];
		$reason5 = $_POST['reason5'];
		$others = $_POST['otherReason'];
		if ($reason1 != "")
			$reason .= $reason1;
		
		if ($reason2 != "")
		{
			if ($reason != "")
				$reason .= ",";
			$reason .= $reason2;
		}
		
		if ($reason3 != "")
		{
			if ($reason != "")
				$reason .= ",";
			$reason .= $reason3;
		}		
		if ($reason4 != "")
		{
			if ($reason != "")
				$reason .= ",";
			$reason .= $reason4;
		}		
		
		$others = addslashes($others);
		$setID = $_POST['set_id'];
		if ($setID != "")
		{
			$sqlFormat = "UPDATE iib_format SET others='$others', reasons='$reason' WHERE id='$setID' ";
		}
		else
		{
			$sqlFormat = "INSERT INTO iib_format (membership_no,ta_login,centre_code,exam_date,exam_time,reasons,others,format_type) ".
				" VALUES ('$membershipNo','$taLogin','$centreCode','$examDate','$examTime','$reason','$others','$formatType')";
		}
		mysql_query($sqlFormat,$MASTERCONN);
		$msg = "Format F details saved";
		print("<script language='JavaScript'>document.loginfrm.memno.value='';document.loginfrm.exam_time.selectedIndex = 0;</script>");
	}
	
	$sqlSelect = "SELECT reasons,others,id FROM iib_format where format_type='$formatType' AND membership_no='$membershipNo' AND ta_login='$taLogin' ".
		" AND exam_time='$examTime' AND exam_date='$examDate' AND centre_code='$centreCode' ";
	//print $sqlSelect;
	$resSelect =mysql_query($sqlSelect,$SLAVECONN);
	if (mysql_num_rows($resSelect) > 0)
	{
		list($reasons,$others,$id) = mysql_fetch_row($resSelect);
		$others = stripslashes($others);
		if ($reasons != "")
		{
			
			$aReasons = explode(",",$reasons);
			if (in_array("HELP FROM OUTSIDE SOURCE",$aReasons))
			{
				$chk1 = " checked ";
			}
			else
			{
				$chk1 = " ";
			}
			if (in_array("SOLVED PAPERS SUPPLIED AND CIRCULATED INSIDE THE   HALL",$aReasons))
			{
				$chk2 = " checked ";
			}
			else
			{
				$chk2 = " ";
			}
			if (in_array("USE OF BOOKS / NOTES OR ANY OTHER WRITTEN MATERIAL",$aReasons))
			{
				$chk3 = " checked ";
			}
			else
			{
				$chk3 = " ";
			}
			if (in_array("DISOBEDIENCE INSIDE THE EXAMINATION HALL",$aReasons))
			{
				$chk4 = " checked ";
			}
			else
			{
				$chk4 = " ";
			}
			
		}
	}
 ?> 
<tr>
  	<td background="images/tile.jpg">
  	<form name='formf' method='post'>
  	<input type='hidden' name='memno' value='<?=$membershipNo ?>'>
  	<input type='hidden' name='exam_time' value='<?=$examTime ?>'>
  	<input type='hidden' name='ta_login' value='<?=$taLogin ?>'>
  	<input type='hidden' name='submitted' value='Y'>
  	<input type='hidden' name='set_id' value='<?=$id ?>'>
  	<table width="65%" border="0" align="center" cellpadding="1" cellspacing="0">
      <tr>
        <td bgcolor="#014A7F"><table width="100%" border="0" cellspacing="0" cellpadding="5">
            <tr>
              <td colspan="2" align="center" bgcolor="#E8EFF7" class="errormsg"><b>
                <?=$msg ?>
              </b></td>
            </tr>
            <?php
  	if ($submitted != 'Y')
	{
	?>
            <tr>
              <td   width="40%" align="left" bgcolor="#E8EFF7" class="greybluetext10"><b>DATE OF THE EXAMINATION:</b></td>
              <td align="left" bgcolor="#E8EFF7" class="greybluetext10" ><?=$dispDate ?></td>
            </tr>
            <tr>
              <td   width="40%" align="left" bgcolor="#E8EFF7" class="greybluetext10"><b>NAME OF THE CENTRE:</b></td>
              <td align="left" bgcolor="#E8EFF7" class="greybluetext10"  ><?=$centreCode ?></td>
            </tr>
            <tr>
              <td  width="40%"  align="left" bgcolor="#E8EFF7" class="greybluetext10"><b> IWAY ADDRESS:</b></td>
              <td align="left" bgcolor="#E8EFF7" class="greybluetext10"  ><?=$iwayAddress ?></td>
            </tr>
            <tr>
              <td  width="40%" align="left" bgcolor="#E8EFF7" class="greybluetext10"><b>MEMBERSHIP NUMBER OF THE CANDIDATE:</b></td>
              <td align="left" bgcolor="#E8EFF7" class="greybluetext10"  ><?=$membershipNo ?></td>
            </tr>
            <tr>
              <td   width="40%" align="left" bgcolor="#E8EFF7" class="greybluetext10"><b>NAME AND ADDRESS OF THE CANDIDATE:</b></td>
              <td align="left" bgcolor="#E8EFF7" class="greybluetext10"  ><?=$memDetails ?></td>
            </tr>
            <tr>
              <td   width="40%" align="left" bgcolor="#E8EFF7" class="greybluetext10"><b>NAME OF THE SUBJECT:</b></td>
              <td align="left" bgcolor="#E8EFF7" class="greybluetext10"  ><?=$subjectName ?></td>
            </tr>
            <tr>
              <td   width="40%" align="left" bgcolor="#E8EFF7" class="greybluetext10"><b>DETAILS OF THE TIME SLOT:</b></td>
              <td align="left" bgcolor="#E8EFF7" class="greybluetext10"  ><?=$examTime ?></td>
            </tr>
            <tr>
              <td   width="40%" align="left" valign="top" bgcolor="#E8EFF7" class="greybluetext10"><b>REASON FOR NOT ALLOWING THE CANDIDATE:</b></td>
              <td width="60%" bgcolor="#E8EFF7" class="greybluetext10" ><br /></td>
            </tr>
            <tr>
              <td colspan="2" bgcolor="#E8EFF7"><table cellpadding="2" cellspacing="0" align="center" width="100%">
                  <tr>
                    <td class="greybluetext10" align="left" ><input type="checkbox" name="reason1" value='HELP FROM OUTSIDE SOURCE' <?=$chk1 ?> /></td>
                    <td class="greybluetext10" align="left" >HELP FROM OUTSIDE SOURCE</td>
                  </tr>
                  <tr>
                    <td class="greybluetext10" align="left" ><input type="checkbox" name="reason2" value='SOLVED PAPERS SUPPLIED AND CIRCULATED INSIDE THE HALL'  <?=$chk2 ?> /></td>
                    <td class="greybluetext10" align="left" >SOLVED PAPERS SUPPLIED AND CIRCULATED INSIDE THE HALL</td>
                  </tr>
                  <tr>
                    <td class="greybluetext10" align="left" ><input type="checkbox" name="reason3" value='USE OF BOOKS / NOTES OR ANY OTHER WRITTEN MATERIAL' <?=$chk3 ?> /></td>
                    <td class="greybluetext10" align="left" >USE OF BOOKS / NOTES OR ANY OTHER WRITTEN MATERIAL</td>
                  </tr>
                  <tr>
                    <td class="greybluetext10" align="left" ><input type="checkbox" name="reason4" value='DISOBEDIENCE INSIDE THE EXAMINATION HALL' <?=$chk4 ?> /></td>
                    <td class="greybluetext10" align="left" >DISOBEDIENCE INSIDE THE EXAMINATION HALL</td>
                  </tr>
                  <tr >
                    <td colspan="2" class="greybluetext10" align="left" >OTHER REASONS</td>
                  </tr>
                  <tr>
                    <td colspan="2" class="greybluetext10" align="left" ><textarea class="greybluetext10" rows="5" cols="100" name='otherReason'><?=$others ?>
    </textarea></td>
                  </tr>
              </table></td>
            </tr>
            <tr>
              <td colspan="2" align="center" bgcolor="#E8EFF7"><input name="button" type="button" class='button' onclick='javascript:validateFeedback(5);' value='Save' />
              </td>
            </tr>
            <?php
	}
	?>
        </table></td>
      </tr>
    </table>
  	</form>
  	</td>
  </tr>
 <?php
}
?>
</table>
<?php
require("bottomnav.php");
//mysql_close($MASTERCONN);
//mysql_close($SLAVECONN);
?>
</center>
</body>
</html>
