<?php /****************************************************
* Application Name            :  IIB 
* Module Name                 :  TA Options 
* Revision Number             :  1
* Revision Date               :  -
* Table(s)                    :  -
* Tables used for only selects:  -
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :  -
* Document/Reference Material :  -
* Created By	              :  -
* Created ON                  :  -
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  22-01-2010
* Description                 :  Interface for APACHE CUSTOME ERROR
*****************************************************/
require_once("dbconfig.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?PHP echo TITLE?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<script language='JavaScript'>
history.go(1);
</script>
<style type="text/css">
<!--
.alertmsg{font-family:Verdana, Arial, Helvetica, sans-serif;font-style:normal;font-weight:bold;color:blue;text-decoration:none;}
.style1 {
	font-size: 16px;
	color: #FF0000;
}
.style2 {font-size: 14px}
.style5 {color: #FF0000}
.style6 {font-size: 12px}
-->
</style>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<center>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <? include("includes/header.php");?>
    </tr>
    <tr> </tr>
    <tr>
      <td width="100%" height="315" align="left" valign="top" background="images/tile.jpg" ><table width="79%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td height="160" align="left" valign="middle" bgcolor="#E8EFF7" class="alertmsg style1"><table width="100%" border="0" align="center" cellpadding="5" cellspacing="5">
                <tr>
                  <td><div align="center" class="style2">
                    <p align="left">File Not Found Error.  <a href="http://<?PHP echo $_SERVER['HTTP_HOST']?>">Click here</a> to go Home Page </p>
                  </div></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          </table>
      </td>
    </tr>
    <tr>
      <?php include("includes/footer.php");?>
    </tr>
  </table>
</center>
</body>
</html>