function showdiv(a) {
    document.getElementById(a).style.visibility = "visible";
    document.getElementById(a).style.display = "block"
}

function hidediv(a) {
    document.getElementById(a).style.visibility = "hidden";
    document.getElementById(a).style.display = "none"
}

function ajax_status(f, b, a) {
    if (a == 0) {
        document.getElementById("statusMessageContainer").style.background = "black";
        document.getElementById("statusMessageContainer").style.color = "red"
    } else {
        if (a == 1) {
            document.getElementById("statusMessageContainer").style.background = "blue";
            document.getElementById("statusMessageContainer").style.color = "white"
        }
    } if (b == 1) {
        document.getElementById("statusMessageContainer").innerHTML = f;
        document.getElementById("statusMessageContainer").style.visibility = "visible";
        document.getElementById("statusContainer").style.visibility = "visible"
    } else {
        if (b == 0) {
            document.getElementById("statusMessageContainer").innerHTML = "";
            document.getElementById("statusMessageContainer").style.visibility = "hidden";
            document.getElementById("statusContainer").style.visibility = "hidden"
        }
    }
}

function utildisp(a) {
    switch (a) {
    case 1:
        return "01";
        break;
    case 2:
        return "02";
        break;
    case 3:
        return "03";
        break;
    case 4:
        return "04";
        break;
    case 5:
        return "05";
        break;
    case 6:
        return "06";
        break;
    case 7:
        return "07";
        break;
    case 8:
        return "08";
        break;
    case 9:
        return "09";
        break;
    default:
        return a;
        break
    }
}

function chkParent(b) {
    var c = (navigator.appName == "Netscape") ? 1 : 0;
    b = (c) ? b : window.event;
    if (!c) {
        if (b.clientY < 0) {
            StopTheClock();
            var a = document.frmonlineexam.last_clicked_question.value;
            var d = document.frmonlineexam.ques_ID.value;
            document.frmonlineexam.action = "destroy_login.php";
            document.frmonlineexam.submit()
        }
    } else {
        if (document.frmonlineexam.action == "") {
            StopTheClock();
            var a = document.frmonlineexam.last_clicked_question.value;
            var d = document.frmonlineexam.ques_ID.value;
            document.frmonlineexam.action = "destroy_login.php";
            document.frmonlineexam.submit()
        }
    }
}
var alertbox_callback = function (d) {
    if (d.id == "alertboxok") {
        document.getElementById("auto_submit").value = "N";
        var b = document.getElementById("ques_ID");
        var a = document.getElementById("answer_ques");
        var c = document.getElementById("question_paper_no");
        if ((b.value != "") && (a.value != "")) {
            ajax_save(1, 0)
        }
        TINY.box.hide();
        document.frmonlineexam.action = "wait.php";
        document.frmonlineexam.submit();
        return true
    } else {
        TINY.box.hide();
        return false
    }
};

function alertbox(b, d, a) {
    if (b == 1) {
		var c='<div class="content_container_confi_wrapper"><div class="outerbox">'+
		'<div class="innerbox">'+
		'<div class="innerbox_container_confi_login">'+
		'<div class="pager_header_common">Confirmation</div>'+
		'<div class="content_container_confi_login">'+
		'<div id="confo_div"><h1>'+d+
		'</h1></div>'+
		'</div>'+
		'<div class="button_container_common"><input name="button" type="button" class="green_button" onclick="TINY.box.hide()" value="OK"/>'+
		'</div>'+
		'</div>'+
		'</div>'+
		'</div></div>';
    } else {
        if (b == 2) {
		var c='<div class="content_container_confi_wrapper"><div class="outerbox">'+
                '<div class="innerbox">'+
                '<div class="innerbox_container_confi_login">'+
                '<div class="pager_header_common">Confirmation</div>'+
                '<div class="content_container_confi_login">'+
                '<div id="confo_div"><h1>'+d+
                '</h1></div>'+
                '</div>'+
                '<div class="button_container_common"><input name="button" type="button" id="alertboxok"  class="green_button" onclick="alertbox_callback(this)" value="YES"/><input name="button" type="button" class="green_button" onclick="alertbox_callback(this)" value="NO"/>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div></div>';
        }
    }
    TINY.box.show(c, 0, 0, 0, 0, 0)
}

function getRowCount(a) {
    var c = document.getElementById("question_count").value;
    var b = 45;
    if (parseInt(c) <= 100) {
        if (screen.width <= 800) {
            rowcount = 10;
            b = 30
        } else {
            if (screen.width <= 1024 && screen.width > 800) {
                rowcount = 13;
                b = 40
            } else {
                if (screen.width <= 1280 && screen.width > 1024) {
                    rowcount = 15;
                    b = 50
                } else {
                    if (screen.width <= 1440 && screen.width > 1280) {
                        rowcount = 17;
                        b = 60
                    } else {
                        if (screen.width <= 1680 && screen.width > 1440) {
                            rowcount = 29;
                            b = 70
                        } else {
                            if (screen.width <= 1920 && screen.width > 1680) {
                                rowcount = 21;
                                b = 80
                            } else {
                                if (screen.width > 1920) {
                                    rowcount = 23;
                                    b = 90
                                }
                            }
                        }
                    }
                }
            }
        }
    } else {
        if (parseInt(c) <= 120) {
            if (screen.width <= 800) {
                rowcount = 11;
                b = 30
            } else {
                if (screen.width <= 1024 && screen.width > 800) {
                    rowcount = 12;
                    b = 40
                } else {
                    if (screen.width <= 1280 && screen.width > 1024) {
                        rowcount = 15;
                        b = 50
                    } else {
                        if (screen.width <= 1440 && screen.width > 1280) {
                            rowcount = 17;
                            b = 60
                        } else {
                            if (screen.width <= 1680 && screen.width > 1440) {
                                rowcount = 19;
                                b = 70
                            } else {
                                if (screen.width <= 1920 && screen.width > 1680) {
                                    rowcount = 21;
                                    b = 80
                                } else {
                                    if (screen.width > 1920) {
                                        rowcount = 23;
                                        b = 90
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (parseInt(c) > 120) {
                if (screen.width <= 800) {
                    rowcount = 11;
                    b = 30
                } else {
                    if (screen.width <= 1024 && screen.width > 800) {
                        rowcount = 12;
                        b = 40
                    } else {
                        if (screen.width <= 1280 && screen.width > 1024) {
                            rowcount = 15;
                            b = 60
                        } else {
                            if (screen.width <= 1440 && screen.width > 1280) {
                                rowcount = 17;
                                b = 60
                            } else {
                                if (screen.width <= 1680 && screen.width > 1440) {
                                    rowcount = 18;
                                    b = 70
                                } else {
                                    if (screen.width <= 1920 && screen.width > 1680) {
                                        rowcount = 20;
                                        b = 80
                                    } else {
                                        if (screen.width > 1920) {
                                            rowcount = 22;
                                            b = 90
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    } if (a == 1) {
        return rowcount
    } else {
        document.getElementById("subname").size = b;
    }
}

function sessionLogout() {
    ajax_status("Your membership no already logged in some other system / Your lost session not removed, will logout soon.", 1, 0);
    document.getElementById("screenoverlay").style.visibility = "visible";
    StopTheClock();
    setTimeout("window.location='destroy_login.php'", 10000)
}

function Preload() {
    var a = Preload.arguments;
    document.imageArray = new Array(a.length);
    for (var b = 0; b < a.length; b++) {
        document.imageArray[b] = new Image;
        document.imageArray[b].src = a[b]
    }
}
Array.prototype.max = function () {
    var b = this[0];
    var a = this.length;
    for (var c = 1; c < a; c++) {
        if (this[c] > b) {
            b = this[c]
        }
    }
    return b
};
Array.prototype.min = function () {
    var c = this[0];
    var a = this.length;
    for (var b = 1; b < a; b++) {
        if (this[b] < c) {
            c = this[b]
        }
    }
    return c
};
Object.size = function (c) {
    var b = 0,
        a;
    for (a in c) {
        if (c.hasOwnProperty(a)) {
            b++
        }
    }
    return b
};

function ued_encode(a, g) {
    var e = "";
    if (typeof g == "undefined") {
        g = ""
    }
    if (typeof (a) == "object") {
        var f = new Array();
        for (key in a) {
            var d = a[key];
            var b = key;
            if (g) {
                b = g + "[" + key + "]"
            }
            if (typeof (d) == "object") {
                if (d.length) {
                    for (var c = 0; c < d.length; c++) {
                        f.push(b + "[]=" + ued_encode(d[c], b))
                    }
                } else {
                    f.push(ued_encode(d, b))
                }
            } else {
                f.push(b + "=" + encodeURIComponent(d))
            }
        }
        e = f.join("&")
    } else {
        e = encodeURIComponent(a)
    }
    return e
};