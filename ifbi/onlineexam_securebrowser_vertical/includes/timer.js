var secs;
var timerID = null;
var timerRunning = false;
var delay = 1000;
var timecheck = 60;
var formname;
var formaction;
var timeLoss = 0;
var timeLoss_start = 0;

function clearClipboard() {
    var a = (navigator.appName == "Netscape") ? 1 : 0;
    if (!a) {
        try {
            if (window.clipboardData.clearData()) {
                window.clipboardData.clearData()
            }
        } catch (d) {}
    }
}
function InitializeTimer(a, c, b) {
    secs = a;
    StopTheClock();
    formname = c;
    formaction = b;
    timerID = setInterval("displayTimer()", 1000)
}
function StartTheClock() {
    InitializeTimer(document.getElementById("time_left").value, "frmonlineexam", "wait.php")
}
function StopTheClock() {
    if (timerRunning) {
        clearInterval(timerID)
    }
    timerRunning = false
}
function displayTimer() {
    if (secs == 300) {
        fireMyPopup()
    }
    if (secs <= 0) {
        StopTheClock();
        eval("document." + formname + ".time_left.value = 0");
        if (formname == "frmonlineexam") {
            preview_submit(1)
        } else {
            ajax_save(0);
            eval("document." + formname + ".action='" + formaction + "'");
            eval("document." + formname + ".submit()")
        }
    } else {
        var min = Math.floor(secs / 60);
        var remaining_secs = secs % 60;
        var texthr = Math.floor(min / 60);
        var textmin = new String(min % 60);
        if (textmin.length == 1) {
            textmin = "0" + textmin
        }
        var textsec = new String(remaining_secs);
        if (textsec.length == 1) {
            textsec = "0" + textsec
        }
        self.status = "TIME LEFT : " + texthr + " Hr(s) " + textmin + " Min(s) " + remaining_secs + " Secs ";
       // eval("self.document." + formname + ".timeleft.innerHTML ='" + texthr + ":" + textmin + ":" + textsec + " hrs'");
	     eval("self.document.getElementById('timeleft').innerHTML ='" + texthr + ":" + textmin + ":" + textsec + " hrs'");
        eval("self.document." + formname + ".time_left.value = " + secs);
        var temp = secs;
        secs = secs - 1;
        timerRunning = true;
        if ((formname == "frmonlineexam") && ((secs % timecheck) == 0) && secs != 0) {
            update_cand_time()
        }
    }
}
var bottom = 0;
var padding = 5;
var X = 5;
var Y = 0;

function dropMyPopup() {
    Y++;
    if (Y > bottom) {
        return
    }
    document.getElementById("styled_popup").style.top = Y + "px";
    setTimeout("dropMyPopup()", 25)
}
function fireMyPopup() {
    var c;
    if (self.pageYOffset) {
        c = self.pageYOffset
    } else {
        if (document.documentElement && document.documentElement.scrollTop) {
            c = document.documentElement.scrollTop
        } else {
            if (document.body) {
                c = document.body.scrollTop
            }
        }
    }
    var d;
    if (self.innerHeight) {
        d = self.innerHeight
    } else {
        if (document.documentElement && document.documentElement.clientHeight) {
            d = document.documentElement.clientHeight
        } else {
            if (document.body) {
                d = document.body.clientHeight
            }
        }
    }
    bottom = c + d - padding - 85;
    Y = c;
    document.getElementById("styled_popup").style.right = X + "px";
    document.getElementById("styled_popup").style.display = "block";
    dropMyPopup();
    setTimeout("styledPopupClose()", 60000)
}
function styledPopupClose() {
    document.getElementById("styled_popup").style.display = "none";
    Y = bottom
}

function setCookie(cname,cvalue,exhrs)
{
var d = new Date();
d.setTime(d.getTime()+(exhrs*60*60*1000));
var expires = "expires="+d.toGMTString();
document.cookie = cname + "=" + cvalue + "; " + expires;
}

function connectivityTimer(b) {
	var d = new Date();
	var n = d.getTime(); 
    if (b == 1) {
        //I = setInterval("innerConnectivityTimer()", 1000)
		timeLoss_start = n
		setCookie("tstart",timeLoss_start,3);
    } else {
        //clearInterval(I)
		timeLoss = n - timeLoss_start ;
		document.getElementById("connfailureduration").value =parseInt(document.getElementById("connfailureduration").value) + timeLoss
			setCookie("tloss",document.getElementById("connfailureduration").value,3);
			document.cookie = "tstart=; expires=Thu, 01 Jan 1970 00:00:00 GMT"; 
			timeLoss_start=0
			timeLoss=0
    }
}
function innerConnectivityTimer() {
    document.getElementById("connfailureduration").value = parseInt(document.getElementById("connfailureduration").value) + 1
}
function update_cand_time() {
    var e = document.getElementById("test_id");
    var q = document.getElementById("timelog").value;
    var n = e.value;
    if (n != "") {
        var p = "update_cand_time.php";
        if (q == "Y") {
            qpno = document.getElementById("question_paper_no").value;
            memno = document.getElementById("memno").value;
            var d = new Date();
            var h = d.getFullYear() + "-" + parseInt(d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            var o = "test=" + n + "&qpno=" + qpno + "&memno=" + memno + "&date=" + h + "&rVal=" + new Date().getTime() + "&connfailureduration=" + document.getElementById("connfailureduration").value
        } else {
            var o = "test=" + n + "&rVal=" + new Date().getTime() + "&connfailureduration=" + document.getElementById("connfailureduration").value
        }
        var r = false;
        try {
            r = new ActiveXObject("Msxml2.XMLHTTP")
        } catch (g) {
            try {
                r = new ActiveXObject("Microsoft.XMLHTTP")
            } catch (j) {
                r = false
            }
        }
        if (!r && typeof XMLHttpRequest != "undefined") {
            r = new XMLHttpRequest()
        }
        /*r.open("POST", p, true);
        r.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        r.setRequestHeader("Content-length", o.length);
        r.setRequestHeader("Connection", "close");*/
        p=p+"?"+o;         
        r.open("GET", p, true);
        r.onreadystatechange = function () {
            if ((r.readyState == 4) && (r.status == 200)) {
                var b = trim(r.responseText);
                var a = b.split("`");
                if (a[1] == 0) {
                    sessionLogout()
                } else {
                    if (a[0] != "0") {
                        //timeupdate(a[0]);
                        ajax_status("", 0, 1);
						document.getElementById("connfailureduration").value=0;
						document.getElementById("timeLoss_start").value=0;
                    } else {
                        ajax_status("There is a connectivity issue. Synchronizing Server Time... Please exit your session", 1, 0)
                    }
                }
            } else {
                ajax_status("Synchronizing Server Time...", 1, 1);
                if ((r.readyState == 4) && (r.status != 200)) {
                    ajax_status("There is a connectivity issue. Synchronizing Server Time... ... Please exit your session", 1, 0)
                }
            }
        };
        try {
          //  r.send(o)
             r.send(null);
        } catch (s) {}
    }
}
function timeupdate(j) {
    var l = document.getElementById("total_time_candi");
    var g = document.getElementById("h_startTime");
    l = l.value;
    g = g.value;
    var k = 0;
    k = j;
    starttimeText = g.split(":");
    starttimeText[2] = starttimeText[2].split(" ");
    starttime1 = parseInt(starttimeText[0], 10) * 3600 + parseInt(starttimeText[1], 10) * 60 + parseInt(starttimeText[2][0], 10);
    serverTimerText = k.split(":");
    serverTimerText[2] = serverTimerText[2].split(" ");
    currentservertime1 = parseInt(serverTimerText[0], 10) * 3600 + parseInt(serverTimerText[1], 10) * 60 + parseInt(serverTimerText[2][0], 10);
    var h = currentservertime1 - starttime1;
    var i = l - h;
    i = i + 2;
    InitializeTimer(i, "frmonlineexam", "wait.php")
};
