var UNANSWERCOUNT = 0;
var ANSWERCHECKED = 0;
var TAGGEDCOUNT = 0;
var ajaxloaderblack = '<img src="./images/black.gif" alt="." height="20"/>';
var ajaxloaderblue = '<img src="./images/blue.gif" alt="." height="20"/>';
var DEFAULTLANGUAGE = document.getElementById("medium_code").value;
var QUESTIONDISPLAY = document.getElementById("question_display").value;
var CASEDISPLAY = document.getElementById("case_display").value;
if (parseInt(document.getElementById("question_count").value) < parseInt(document.getElementById("display_noofquestions").value)) {
    var NOOFQUESTIONSPERPAGE = parseInt(document.getElementById("questions_count").value)
} else {
    var NOOFQUESTIONSPERPAGE = parseInt(document.getElementById("display_noofquestions").value)
}
var TOTALNOOFQUESTIONS = parseInt(document.getElementById("question_count").value);
var PAGINGCOUNT = parseInt(TOTALNOOFQUESTIONS / NOOFQUESTIONSPERPAGE) + parseInt(TOTALNOOFQUESTIONS % NOOFQUESTIONSPERPAGE);
var CURRENTPAGE = 1;
if (QUESTIONDISPLAY == "A") {
    NOOFQUESTIONSPERPAGE = TOTALNOOFQUESTIONS
} else {
    if (QUESTIONDISPLAY == "O") {
        NOOFQUESTIONSPERPAGE = 1
    }
}
var ANSWERSAVINGTYPE = document.getElementById("answer_saving_type").value;
var ANSWERSAVINGINTERVAL = document.getElementById("answer_saving_interval").value;
var ANSWERSAVINGPROGRESS = 0;
if (ANSWERSAVINGTYPE == "T") {
    var answersavinginterval_millisecond = parseInt((ANSWERSAVINGINTERVAL * 60) * 1000);
    var ANSWERTIMER = setInterval("ajax_save(1)", answersavinginterval_millisecond)
}
var PAGEEND = parseInt(CURRENTPAGE * NOOFQUESTIONSPERPAGE) - 1;
var PAGESTART = parseInt((CURRENTPAGE * NOOFQUESTIONSPERPAGE) - NOOFQUESTIONSPERPAGE);
var LASTPAGECOUNT = parseInt(TOTALNOOFQUESTIONS % NOOFQUESTIONSPERPAGE);
var arrCurrentCaseQuestion = new Array();




function swap_sheet(m,clkfrm) { 
//	alert("in-->"+CURRENTPAGE)
//		var qid = arrQuestionDetails[qdno]["questionid"];
	

	var l = 1;
	if(clkfrm==1)
		document.getElementById("qd_no").value=m;
	else
		document.getElementById("qd_no").value=m-1;
    if (m > TOTALNOOFQUESTIONS) {
        alertbox(1, "<h1>You are in last question,please use previous question button or preview panel to view other questions</h1>");
        return
    }
    if (m == 0) {
        alertbox(1, "<h1>You are in first question,please use next question button or preview panel to view other questions</h1>");
        return
    }
    if (ANSWERSAVINGTYPE == "Q") {
		connectivityTimer(1)
        ajax_save(1)
    }
	
	if (m == TOTALNOOFQUESTIONS) {
//		document.getElementById("Next").disabled=true;
		document.getElementById("Next").style.color="#7B7B7B";
	}else{
//		document.getElementById("Next").disabled=false;
		document.getElementById("Next").style.color="#FFFFFF";
	}
	if (m == 1) {
//		document.getElementById("Prev").disabled=true;
		document.getElementById("Prev").style.color="#7B7B7B";
	}else{
//		document.getElementById("Prev").disabled=false;
		document.getElementById("Prev").style.color="#FFFFFF";
	}

	//document.getElementById("TAG_BTN").innerHTML = "tagBtn"+m;
	
    arrCurrentCaseQuestion.length = 0;
    if (m < NOOFQUESTIONSPERPAGE) {
        CURRENTPAGE = 1
    } else {
        var o = parseInt(m % NOOFQUESTIONSPERPAGE);
        var r = parseInt(m / NOOFQUESTIONSPERPAGE);
        if (o == 0) {
            CURRENTPAGE = r
        } else {
            CURRENTPAGE = r + 1
        }
    }

	//alert("out-->"+CURRENTPAGE+"@@@VAL-->"+arrQuestionDetails[CURRENTPAGE-1]["questionid"])

	
    var marked_string = document.getElementById("marked_string").value;
    var mstring = marked_string.split(",") ;
    /**/

	var questionID = arrQuestionDetails[CURRENTPAGE-1]["questionid"];
	var btnname = 'mark_btn_'+questionID;

	if(inArray(mstring,arrQuestionid[CURRENTPAGE-1]) == 1) {
	document.getElementById("tagDiv").innerHTML = '<input title="Click to De-Tag the question" type="button" name="'+btnname+'" id="'+btnname+'" class="yellow_button" value=" De-Tag " onClick="javascript: tagQ(this, '+questionID+');">';
	}else {
	document.getElementById("tagDiv").innerHTML = '<input title="Click to Tag the question" type="button" name="'+btnname+'" id="'+btnname+'" class="yellow_button" value=" Tag " onClick="javascript: tagQ(this, '+questionID+');">';
    }

	document.getElementById("eraseDiv").innerHTML = '<input title="Click to erase the answer" onclick="eraseAnswer('+parseInt(CURRENTPAGE-1)+')" class="red_button" type="button" value="Erase Answer">';

	/*alert(btnname)
	alert(document.getElementById(btnname).value)
	document.getElementsByClassName().title = "";
	document.getElementsByClassName().name = "mark_btn_"+arrQuestionDetails[CURRENTPAGE-1]["questionid"];
	document.getElementsByClassName().id = "mark_btn_"+arrQuestionDetails[CURRENTPAGE-1]["questionid"];
	document.getElementsByClassName().value = "";
	document.getElementsByClassName().value = "";*/


    for (p = 0; p < TOTALNOOFQUESTIONS; p++) {
        var d = document.getElementById("q" + p);
        if (d != "undefined") {
            d.style.display = "none"
        }
    }
    PAGESTART = parseInt((CURRENTPAGE * NOOFQUESTIONSPERPAGE) - NOOFQUESTIONSPERPAGE);
    PAGEEND = parseInt(CURRENTPAGE * NOOFQUESTIONSPERPAGE) - 1;
    if (PAGESTART >= TOTALNOOFQUESTIONS) {
        PAGESTART = TOTALNOOFQUESTIONS - NOOFQUESTIONSPERPAGE
    }
    if (PAGEEND > TOTALNOOFQUESTIONS) {
        PAGEEND = TOTALNOOFQUESTIONS - 1
    }

	//document.getElementById("sam_quest_text_inner_container").style.height='470px'
    if (typeof arrCaseid != "undefined" && arrCaseid instanceof Object && QUESTIONDISPLAY != "A") {
        for (var p = 0; p < arrCaseid1.length; p++) {
            var d = document.getElementById("caseid" + arrCaseid[p]);
            if (d != "undefined") {
                d.style.display = "none"
				document.getElementById("q" + (CURRENTPAGE-1)).style.height='559px'
				//document.getElementById("sam_quest_text_inner_container").style.height='470px'
            }
        }
        if (document.getElementById("casetitletext")) {
            document.getElementById("casetitletext").style.display = "none"
        }
    } else {
        if (document.getElementById("casetitletext")) {
            document.getElementById("casetitletext").style.display = "none"
        }
    }
    for (p = PAGESTART; p <= PAGEEND; p++) {
        var d = document.getElementById("q" + p);
        d.style.display = "block";
        if (arrQuestionDetails != undefined && arrQuestionDetails[p]["caseid"] != 0) {
            var n = document.getElementById("caseid" + arrQuestionDetails[p]["caseid"]);
            if (n) {
                n.style.display = "block"				
				document.getElementById("q" + (CURRENTPAGE-1)).style.height='275px'
				document.getElementById("q" + (CURRENTPAGE-1)).style.overflowY='scroll'
				document.getElementById("q" + (CURRENTPAGE-1)).style.overflowX='hidden'
            }
            if (document.getElementById("casetitletext")) {
                document.getElementById("casetitletext").style.display = "block"
            }
        }
    }
    if (CASEDISPLAY == "A" && QUESTIONDISPLAY == "O" && typeof arrCaseid != "undefined" && arrCaseid instanceof Object) {
        if (m == 0) {
            var q = 0
        } else {
            var q = parseInt(m) - 1
        } if (arrQuestionDetails[q]["caseid"] != 0) {
            var s = arrQuestionDetails[q]["caseid"];
            for (p = 0, j = 0; p < TOTALNOOFQUESTIONS; p++) {
                if (arrQuestionDetails[p]["caseid"] == s) {
                    arrCurrentCaseQuestion[j] = p;
                    j++
                }
            }
        }
        for (p = 0; p < arrCurrentCaseQuestion.length; p++) {
            var d = document.getElementById("q" + arrCurrentCaseQuestion[p]);
            d.style.display = "block"
        }
    }
    preview();
    showdiv("preview_div");
    showdiv("questionpaper_div")
}

function prev() {
    if (arrCurrentCaseQuestion.length > 0) {
        CURRENTPAGE = arrCurrentCaseQuestion.min();
        prevquestion = parseInt((CURRENTPAGE * NOOFQUESTIONSPERPAGE) - NOOFQUESTIONSPERPAGE) + 1
    } else {
        prevquestion = parseInt((CURRENTPAGE * NOOFQUESTIONSPERPAGE) - NOOFQUESTIONSPERPAGE)
    }
    swap_sheet(prevquestion,0)
}

function next() {
    if (arrCurrentCaseQuestion.length > 0) {
        CURRENTPAGE = arrCurrentCaseQuestion.max();
        nextquestion = parseInt((CURRENTPAGE * NOOFQUESTIONSPERPAGE) + NOOFQUESTIONSPERPAGE) + 1
    } else {
        nextquestion = parseInt((CURRENTPAGE * NOOFQUESTIONSPERPAGE) + NOOFQUESTIONSPERPAGE)
    }
    swap_sheet(nextquestion,0)
}

function inArray(haystack, needle) {

// alert(needle+', '+haystack) ; return false ;

    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return 1;
    }
    return 0;
}

function preview() {
    var z = "";
    var A = "";
    rowcount = getRowCount(1);
    var s = 0;
    var D = "</span></a>";
    var t = "";
    var x = "";
    var r = 1;
    U = 0;
    var q = 0;
    UNcount = 0;
    tcount = 0;
    for (var C = 0; C < TOTALNOOFQUESTIONS; C++) {
        var y = parseInt(C + 1);
        var w = "#link" + y;
        var u = "Q" + utildisp(parseInt(C) + 1);
        //var v = '<a  title="Click to view ' + u + '"  href="' + w + '"  class="attempt"  onClick=javascript:swap_sheet(' + y + ",1)>";
        //var B = '<a  title="Click to view ' + u + '"  href="' + w + '"  class="unattempt" onClick=javascript:swap_sheet(' + y + ",1)>";
        var v = '<a id="q_disp'+y+'" title="Click to view ' + u + '"  href="' + w + '"  class="attempt"  onClick=javascript:swap_sheet(' + y + ',1)><span>';
        var B = '<a id="q_disp'+y+'" title="Click to view ' + u + '"  href="' + w + '"  class="unattempt" onClick=javascript:swap_sheet(' + y + ',1)><span>';
        var yt = '<a id="q_disp'+y+'" title="Click to view ' + u + '"  href="' + w + '"  class="qnodisp_tag" onClick=javascript:swap_sheet(' + y + ',1)><span>';
        var e = arrQuestionid[C];

        var marked_string = document.getElementById("marked_string").value;

        var mstring = marked_string.split(",") ;

        if(inArray(mstring,e) == 1)
        {
            if (C > 99) {
                x += yt + u + D + z + z
            } else {
                x += yt + u + D + z + z + z
            }
            tcount++;
            U++;
           
        } else if (arrQuestionDetails[C]["answerinserver"] == "NULL" || arrQuestionDetails[C]["answerinserver"] == "") {
            if (C > 99) {
                x += B + u + D + z + z
            } else {
                x += B + u + D + z + z + z
            }
            UNcount++;
            U++
        } else {
            if (C > 99) {
                x += v + u + D + z + z
            } else {
                x += v + u + D + z + z + z
            }
            q++;
            U++
        } if (U == rowcount) {
            //x += "<br/>";
            U = 0
        }
    }
    document.getElementById("qnodetails").innerHTML = x;
    document.getElementById("no_attempt").innerHTML = q;
    document.getElementById("no_unattempt").innerHTML = UNcount;
    document.getElementById("no_tagged").innerHTML = tcount;
    UNANSWERCOUNT = UNcount
    TAGGEDCOUNT = tcount;
}

function ajax_resetAnswerElement(q_rowno) {
    var answered = false;
    if (arrQuestionDetails[q_rowno]["displayorder"] != "undefined") {
        if (arrQuestionDetails[q_rowno]["questiontype"] == "O") {
            alen = eval("document.frmonlineexam.answer" + arrQuestionDetails[q_rowno]["questionid"] + ".length");
            for (pos = 0; pos < alen; pos++) {
                answervar = eval("document.frmonlineexam.answer" + arrQuestionDetails[q_rowno]["questionid"] + "[" + pos + "]");
                if (answervar.checked == true) {
                    answered = true
                }
                answervar.checked = false
            }
        } else {
            if (arrQuestionDetails[q_rowno]["questiontype"] == "M") {
                alen = eval("document.frmonlineexam.answer" + arrQuestionDetails[q_rowno]["questionid"] + ".length");
                for (pos = 0; pos < alen; pos++) {
                    answervar = eval("document.frmonlineexam.answer" + arrQuestionDetails[q_rowno]["questionid"] + "[" + pos + "]");
                    if (answervar.checked == true) {
                        answered = true
                    }
                    answervar.checked = false
                }
            }
        } if (answered == true) {
            arrQuestionDetails[q_rowno]["answerinlocal"] = "NULL";
            if (arrQuestionDetails[q_rowno]["answerinserver"] != "NULL") {
                arrQuestionDetails[q_rowno]["savedinserver"] = 0
            }
        }
    }
}

function preview_submit(t) {
	connectivityTimer(1);
    ajax_save(1);
    preview();
   // var e = document.getElementById("timeleft").value;
   var e = document.getElementById("timeleft").innerHTML; 
    if (document.getElementById("exam_type").value == 3) {
        var u = parseInt(document.getElementById("question_count").value) - parseInt(document.getElementById("max_no_of_questions").value);
        if (UNANSWERCOUNT > u || TAGGEDCOUNT > u) {
            var p = "You have <span>" + e + "</span> to answer <span>" + UNANSWERCOUNT + "</span>  un-answered question(s), <span>"+ TAGGEDCOUNT  +"</span> tagged question(s). still do you want to proceed with your submit?"
        } else {
            var p = "Do you want to proceed with your submit?"
        }
    } else {
        if (UNANSWERCOUNT > 0 || TAGGEDCOUNT > 0) {
            var p = "You have <span>" + e + "</span> to answer <span>" + UNANSWERCOUNT + "</span>  un-answered question(s), <span>"+ TAGGEDCOUNT  +"</span> tagged question(s). still do you want to proceed with your submit?"
        } else {
            var p = "Do you want to proceed with your submit?"
        }
    }
    var k = document.getElementById("time_left").value;
    var v = document.getElementById("h_totalTime").value;
    var b = document.getElementById("duration_prevent").value;
    var d = v - b;
    var o = Math.floor((k - d) / 60);
    var s = parseInt((k - d) % 60);
    var q = o + " minute(s) " + s + " second(s)";
    if (k > d) {
        alertbox(1, "<h1>Sorry !!! You are not allowed to submit now. please wait <span>" + q + "</span> to submit your exam</h1>");
        return false
    }
    if (t == 0) {
        alertbox(2, p, alertbox_callback)
    } else {
        if (t == 1) {
            document.getElementById("auto_submit").value = "Y"
        }
        document.frmonlineexam.action = "wait.php";
        document.frmonlineexam.submit()
    }
}

function changeChecked(q_rowno) {
    var ans = "";
    if (arrQuestionDetails[q_rowno]["displayorder"] != "undefined") {
        if (arrQuestionDetails[q_rowno]["questiontype"] == "O") {
            alen = eval("document.frmonlineexam.answer" + arrQuestionDetails[q_rowno]["questionid"] + ".length");
            for (pos = 0; pos < alen; pos++) {
                answervar = eval("document.frmonlineexam.answer" + arrQuestionDetails[q_rowno]["questionid"] + "[" + pos + "]");
                if (answervar.checked == true) {
                    ans = answervar.value
                }
            }
        } else {
            if (arrQuestionDetails[q_rowno]["questiontype"] == "M") {
                alen = eval("document.frmonlineexam.answer" + arrQuestionDetails[q_rowno]["questionid"] + ".length");
                for (pos = 0; pos < alen; pos++) {
                    answervar = eval("document.frmonlineexam.answer" + arrQuestionDetails[q_rowno]["questionid"] + "[" + pos + "]");
                    if (answervar.checked == true) {
                        if (ans != "") {
                            ans = ans + "," + answervar.value
                        } else {
                            ans = answervar.value
                        }
                    }
                }
            }
        } if (ans != "") {
            if (arrQuestionDetails[q_rowno]["answerinserver"] != trim(ans)) {
                if (arrQuestionDetails[q_rowno]["answerinlocal"] != trim(ans)) {
                    arrQuestionDetails[q_rowno]["answerinlocal"] = ans;
                    arrQuestionDetails[q_rowno]["savedinserver"] = 0
                } else {
                    arrQuestionDetails[q_rowno]["answerinlocal"] = arrQuestionDetails[q_rowno]["answerinserver"];
                    arrQuestionDetails[q_rowno]["savedinserver"] = 1
                }
            }
        }
    }
}

function eraseAnswer(q_rowno) {
    var ans = "";
    var answered = false;
    if (arrQuestionDetails[q_rowno]["displayorder"] != "undefined") {
        if (arrQuestionDetails[q_rowno]["questiontype"] == "O") {
            alen = eval("document.frmonlineexam.answer" + arrQuestionDetails[q_rowno]["questionid"] + ".length");
            for (pos = 0; pos < alen; pos++) {
                answervar = eval("document.frmonlineexam.answer" + arrQuestionDetails[q_rowno]["questionid"] + "[" + pos + "]");
                if (answervar.checked == true) {
                    answered = true
                }
                answervar.checked = false
            }
        } else {
            if (arrQuestionDetails[q_rowno]["questiontype"] == "M") {
                alen = eval("document.frmonlineexam.answer" + arrQuestionDetails[q_rowno]["questionid"] + ".length");
                for (pos = 0; pos < alen; pos++) {
                    answervar = eval("document.frmonlineexam.answer" + arrQuestionDetails[q_rowno]["questionid"] + "[" + pos + "]");
                    if (answervar.checked == true) {
                        answered = true
                    }
                    answervar.checked = false
                }
            }
        } if (answered == true) {
            arrQuestionDetails[q_rowno]["answerinlocal"] = "NULL";
            if (arrQuestionDetails[q_rowno]["answerinserver"] != "NULL") {
                arrQuestionDetails[q_rowno]["savedinserver"] = 0
            }
        }
		
		connectivityTimer(1)
        ajax_save(1);
        preview()
    }
}

function ajax_save(g) {
    var k = new Array();
    k.length = 0;
    for (i = 0, j = 0; i < TOTALNOOFQUESTIONS; i++) {
        if (arrQuestionDetails[i]["savedinserver"] == "1" && arrQuestionDetails[i]["answerinlocal"] == "" && arrQuestionDetails[i]["answerinserver"] == "") {
            ajax_resetAnswerElement(i)
        }
        if (arrQuestionDetails[i]["savedinserver"] == "0" && arrQuestionDetails[i]["answerinlocal"] != "") {
            k[j] = arrQuestionDetails[i]["displayorder"];
            j++
        }
    }
    var m = {};
    if (ANSWERSAVINGTYPE == "Q") {
        //if (k.length >= ANSWERSAVINGINTERVAL) {
			if(k.length==0 && document.getElementById("qd_no").value>0){
				var h = "update_answers.php";
				var n = document.getElementById("question_paper_no").value;
				var qdno = CURRENTPAGE-1;
				var qid = arrQuestionDetails[qdno]["questionid"];
				m[0] = {}; 
				m[0]["qid"] = qid;
				if(arrQuestionDetails[qdno]["answerinlocal"]=='')
					m[0]["answer"] = "";
				else
					m[0]["answer"] = arrQuestionDetails[qdno]["answerinlocal"];
				m[0]["rowno"] = qdno;
				var l = "qpNo=" + n + "&rVal=" + new Date().getTime() + "&" + ued_encode(m, "");
			}else{
				for (i = 0; i < k.length; i++) {
					m[i] = {};
					m[i]["qid"] = arrQuestionDetails[k[i]]["questionid"];
					m[i]["answer"] = arrQuestionDetails[k[i]]["answerinlocal"];
					m[i]["rowno"] = arrQuestionDetails[k[i]]["displayorder"];
				}
				var h = "update_answers.php";
				var n = document.getElementById("question_paper_no").value;
				var l = "qpNo=" + n + "&rVal=" + new Date().getTime() + "&" + ued_encode(m, "");
			}
				ajaxRequest(g, h, l)
        //}
    } else {
        /*if (ANSWERSAVINGTYPE == "T") {
            if (ANSWERSAVINGPROGRESS == 0) {
                for (i = 0; i < k.length; i++) {
                    m[i] = {};
                    m[i]["qid"] = arrQuestionDetails[k[i]]["questionid"];
                    m[i]["answer"] = arrQuestionDetails[k[i]]["answerinlocal"];
                    m[i]["rowno"] = arrQuestionDetails[k[i]]["displayorder"]
                }
                var h = "update_answers.php";
                var n = document.getElementById("question_paper_no").value;
                var l = "qpNo=" + n + "&rVal=" + new Date().getTime() + "&" + ued_encode(m, "");
                ajaxRequest(g, h, l)
            }
        }*/
    }
}
var connectCount = 0;

function ajaxRequest(o, p, l) {
    var e = false;
    try {
        e = new ActiveXObject("Msxml2.XMLHTTP")
    } catch (m) {
        try {
            e = new ActiveXObject("Microsoft.XMLHTTP")
        } catch (n) {
            e = false
        }
    }
    if (!e && typeof XMLHttpRequest != "undefined") {
        e = new XMLHttpRequest()
    }
		
    //connectivityTimer(1)
    if (o == 0) {
        e.open("POST", p, true)
    } else {
        e.open("POST", p, false)
    }
    e.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=iso-8859-1");
    e.setRequestHeader("Content-length", l.length);
    e.setRequestHeader("Connection", "close");
    e.onreadystatechange = function () {
        ANSWERSAVINGPROGRESS = 1;
        document.getElementById("screenoverlay").style.visibility = "visible";
        if ((e.readyState == 4) && (e.status == 200)) {
            JSONData = trim(e.responseText);
            if(JSONData == null || JSONData == ""){
                sessionLogout()
                return;
            }
            JSONobj = JSONData.parseJSON();
			
            ANSWERSAVINGPROGRESS = 0;
            if (JSONobj.ses != 1) {
                sessionLogout()
            } else {
                if (JSONobj.con != 1) {
                    if (connectCount == 0) {
                        StopTheClock();
                        //connectivityTimer(1)
                    }
                    connectCount = parseInt(connectCount) + 1;
                    if (connectCount < 3) {
                        if (connectCount == 1) {
                            ajax_status(" Server connection failure.Retrying " + ajaxloaderblack, 1, 0)
                        } else {
                            if (connectCount == 2) {
                                ajax_status(" Server connection failure.Last try... " + ajaxloaderblack, 1, 0)
                            }
                        }
                        setTimeout('ajaxRequest("' + o + '","' + p + '","' + l + '")', 2000)
                    } else {
                        ajax_status(" Server connection failure.answer(s) are not saved.please exit your browser.", 1, 0);
                        //connectivityTimer(0);
						
 							/*if (confirm("Connection Failure.. Kindly restart your exam")) {
									this.window.close();
							  }*/
                        preview()
                    }
                } else {
					if (connectCount == 1 || connectCount == 2) {
						StartTheClock();
					}
                    for (resobj in JSONobj) {
                        var a = JSONobj[resobj]["qid"];
                        var b = JSONobj[resobj]["answer"];
                        var c = JSONobj[resobj]["saved"];
                        var d = JSONobj[resobj]["rowno"];
                        if (c == 1) {
                            arrQuestionDetails[d]["savedinserver"] = 1;
                            arrQuestionDetails[d]["answerinserver"] = b
                        }
                    }
                    connectCount = 0;
                    ajax_status("", 0, 1);
                    document.getElementById("screenoverlay").style.visibility = "hidden";
					connectivityTimer(0);
                    preview()
                }
            }
        } else {
            if (connectCount == 0) {
                ajax_status("answer(s) saving to the server " + ajaxloaderblue, 1, 1)
            } else {
                if (connectCount == 1) {
                    ajax_status(" Server connection failure.Retrying " + ajaxloaderblack, 1, 0)
                } else {
                    if (connectCount == 2) {
                        ajax_status(" Server connection failure.Last try... " + ajaxloaderblack, 1, 0)
                    }
                }
            } if ((e.readyState == 4) && (e.status != 200)) {
                ANSWERSAVINGPROGRESS = 0;
                if (connectCount == 0) {
                    StopTheClock();
                    //connectivityTimer(1)
                }
                connectCount = parseInt(connectCount) + 1;
                if (connectCount < 3) {
                    if (connectCount == 1) {
                        ajax_status(" Server connection failure.Retrying " + ajaxloaderblack, 1, 0)
                    } else {
                        if (connectCount == 2) {
                            ajax_status(" Server connection failure.Last try... " + ajaxloaderblack, 1, 0)
                        }
                    }
                    setTimeout('ajaxRequest("' + o + '","' + p + '","' + l + '")', 2000)
                } else {
                    ajax_status(" Server connection failure.answer(s) are not saved.please exit your browser.", 1, 0);
                    //connectivityTimer(0);
							/*if (confirm("Connection Failure.. Kindly restart your exam")) {
									this.window.close();
							  }*/
                    preview()
                }
            }
        }
    };
    try {
        e.send(l)
    } catch (k) {}
}

function intialAnswerMark() {
    for (var i = 0; i < TOTALNOOFQUESTIONS; i++) {
        if (arrQuestionDetails[i]["answer"] != "") {
            var ele = "answer" + arrQuestionDetails[i]["questionid"];
            if (eval("document.frmonlineexam." + ele + "!=undefined")) {
                for (var j = 0; j < eval("document.frmonlineexam." + ele + ".length"); j++) {
                    if (arrQuestionDetails[i]["questiontype"] == "M") {
                        var arrAns = explode(",", arrQuestionDetails[i]["answerinserver"]);
                        for (var k = 0; k < arrAns.length; k++) {
                            if (eval("document.frmonlineexam." + ele + "[" + j + "].value") == arrAns[k]) {
                                eval("document.frmonlineexam." + ele + "[" + j + "].checked=true")
                            }
                        }
                    } else {
                        if (arrQuestionDetails[i]["questiontype"] == "O") {
                            if (eval("document.frmonlineexam." + ele + "[" + j + "].value") == arrQuestionDetails[i]["answerinserver"]) {
                                eval("document.frmonlineexam." + ele + "[" + j + "].checked=true")
                            }
                        } else {
                            if (arrQuestionDetails[i]["questiontype"] == "Z") {
                                if (eval("document.frmonlineexam." + ele + "[" + j + "].value") == arrQuestionDetails[i]["answerinserver"]) {
                                    eval("document.frmonlineexam." + ele + "[" + j + "].checked=true")
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

function callOnLoad() {
    InitializeTimer(document.getElementById("time_left").value, "frmonlineexam", "wait.php");
    swap_sheet(1,0);
    intialAnswerMark();
    //getRowCount(0);
    setElementsCountByClassName();
    Preload("./images/black.gif", "./images/blue.gif", "./images/white.png","./themes/default/images/header_bg.jpg","./themes/default/images/sify_logo.png","./themes/default/images/sample_question_header_bg.jpg","./themes/default/images/A-.png","./themes/default/images/A+.png","./themes/default/images/sample_question_left_sec_body_bg.jpg","./themes/default/images/photo_no.jpg","./themes/default/images/sign.jpg","./themes/default/images/bt-link-bg.jpg","./images/prev_arrow.png","./themes/default/images/prev_btn_bg.jpg","./images/next_arrow.png","./images/final_submit_arow.png","./themes/default/images/final_submit_bg.jpg","./themes/default/images/btn_bg.jpg","./themes/default/images/footer_bg.jpg");
    dispQPLanguage(DEFAULTLANGUAGE, "ALL")
}

function zoomimg(f, e, d) {
    PopEx(f, null, null, e, d, 75, "PopBoxImageLarge")
}

function checkImageZoom() {
    var d = document.getElementsByTagName("img");
    for (var c = 0; c < d.length; c++) {
        if (d[c].style.zIndex > 500) {
            Revert(d[c], 75, "PopBoxImageLarge")
        }
    }
}

function changeLangList() {
    dispQPLanguage(document.getElementById("langlist").value, "ALL")
}

function dispQPLanguage(lang, qid) {
    if (qid == "ALL") {
        resetLanguage("ALL");
        for (var j = 0; j < TOTALNOOFQUESTIONS; j++) {
            var qtext = document.getElementById("qtext" + lang + arrQuestionDetails[j]["questionid"]);
            if (qtext != undefined) {
                eval("qtext.style.display='inline';")
            }
            var qopt1 = document.getElementById("qopt1" + lang + arrQuestionDetails[j]["questionid"]);
            if (qopt1 != undefined) {
                eval("qopt1.style.display='inline';")
            }
            var qopt2 = document.getElementById("qopt2" + lang + arrQuestionDetails[j]["questionid"]);
            if (qopt2 != undefined) {
                eval("qopt2.style.display='inline';")
            }
            var qopt3 = document.getElementById("qopt3" + lang + arrQuestionDetails[j]["questionid"]);
            if (qopt3 != undefined) {
                eval("qopt3.style.display='inline';")
            }
            var qopt4 = document.getElementById("qopt4" + lang + arrQuestionDetails[j]["questionid"]);
            if (qopt4 != undefined) {
                eval("qopt4.style.display='inline';")
            }
            var qopt5 = document.getElementById("qopt5" + lang + arrQuestionDetails[j]["questionid"]);
            if (qopt5 != undefined) {
                eval("qopt5.style.display='inline';")
            }
            var qcase = document.getElementsByName("qcase" + lang + arrQuestionDetails[j]["caseid"]);
            for (var k = 0; k < qcase.length; k++) {
                if (qcase[k] != undefined) {
                    eval("qcase[k].style.display='inline';")
                }
            }
        }
    } else {
        resetLanguage(qid);
        var qtext = document.getElementById("qtext" + lang + qid);
        if (qtext != undefined) {
            eval("qtext.style.display='inline';")
        }
        var qopt1 = document.getElementById("qopt1" + lang + qid);
        if (qopt1 != undefined) {
            eval("qopt1.style.display='inline';")
        }
        var qopt2 = document.getElementById("qopt2" + lang + qid);
        if (qopt2 != undefined) {
            eval("qopt2.style.display='inline';")
        }
        var qopt3 = document.getElementById("qopt3" + lang + qid);
        if (qopt3 != undefined) {
            eval("qopt3.style.display='inline';")
        }
        var qopt4 = document.getElementById("qopt4" + lang + qid);
        if (qopt4 != undefined) {
            eval("qopt4.style.display='inline';")
        }
        var qopt5 = document.getElementById("qopt5" + lang + qid);
        if (qopt5 != undefined) {
            eval("qopt5.style.display='inline';")
        }
        var qcase = document.getElementsByName("qcase" + lang + qid);
        for (var k = 0; k < qcase.length; k++) {
            if (qcase[k] != undefined) {
                eval("qcase[k].style.display='inline';")
            }
        }
    }
}

function resetLanguage(qid) {
    if (qid == "ALL") {
        var langCount = Object.size(arrLanguage);
        for (var i = 0; i < langCount; i++) {
            var lang = arrLanguage[i];
            for (var j = 0; j < TOTALNOOFQUESTIONS; j++) {
                var qtext = document.getElementById("qtext" + lang + arrQuestionDetails[j]["questionid"]);
                if (qtext != undefined) {
                    eval("qtext.style.display='none';")
                }
                var qopt1 = document.getElementById("qopt1" + lang + arrQuestionDetails[j]["questionid"]);
                if (qopt1 != undefined) {
                    eval("qopt1.style.display='none';")
                }
                var qopt2 = document.getElementById("qopt2" + lang + arrQuestionDetails[j]["questionid"]);
                if (qopt2 != undefined) {
                    eval("qopt2.style.display='none';")
                }
                var qopt3 = document.getElementById("qopt3" + lang + arrQuestionDetails[j]["questionid"]);
                if (qopt3 != undefined) {
                    eval("qopt3.style.display='none';")
                }
                var qopt4 = document.getElementById("qopt4" + lang + arrQuestionDetails[j]["questionid"]);
                if (qopt4 != undefined) {
                    eval("qopt4.style.display='none';")
                }
                var qopt5 = document.getElementById("qopt5" + lang + arrQuestionDetails[j]["questionid"]);
                if (qopt5 != undefined) {
                    eval("qopt5.style.display='none';")
                }
                var qcase = document.getElementsByName("qcase" + lang + arrQuestionDetails[j]["caseid"]);
                for (var k = 0; k < qcase.length; k++) {
                    if (qcase[k] != undefined) {
                        eval("qcase[k].style.display='none';")
                    }
                }
            }
        }
    } else {
        var langCount = Object.size(arrLanguage);
        for (var i = 0; i < langCount; i++) {
            var lang = arrLanguage[i];
            var qtext = document.getElementById("qtext" + lang + qid);
            if (qtext != undefined) {
                eval("qtext.style.display='none';")
            }
            var qopt1 = document.getElementById("qopt1" + lang + qid);
            if (qopt1 != undefined) {
                eval("qopt1.style.display='none';")
            }
            var qopt2 = document.getElementById("qopt2" + lang + qid);
            if (qopt2 != undefined) {
                eval("qopt2.style.display='none';")
            }
            var qopt3 = document.getElementById("qopt3" + lang + qid);
            if (qopt3 != undefined) {
                eval("qopt3.style.display='none';")
            }
            var qopt4 = document.getElementById("qopt4" + lang + qid);
            if (qopt4 != undefined) {
                eval("qopt4.style.display='none';")
            }
            var qopt5 = document.getElementById("qopt5" + lang + qid);
            if (qopt5 != undefined) {
                eval("qopt5.style.display='none';")
            }
            var qcase = document.getElementsByName("qcase" + lang + qid);
            for (var k = 0; k < qcase.length; k++) {
                if (qcase[k] != undefined) {
                    eval("qcase[k].style.display='none';")
                }
            }
        }
    }
}

function donotanswer(q_rowno, qid, obj) {
    if (obj.checked) {
        eraseAnswer(q_rowno);
        if ((arrQuestionDetails[q_rowno]["questiontype"] == "M") || (arrQuestionDetails[q_rowno]["questiontype"] == "O")) {
            alen = eval("document.frmonlineexam.answer" + arrQuestionDetails[q_rowno]["questionid"] + ".length");
            for (pos = 0; pos < alen; pos++) {
                eval("document.frmonlineexam.answer" + arrQuestionDetails[q_rowno]["questionid"] + "[" + pos + "].disabled=true")
            }
        }
    } else {
        if ((arrQuestionDetails[q_rowno]["questiontype"] == "M") || (arrQuestionDetails[q_rowno]["questiontype"] == "O")) {
            alen = eval("document.frmonlineexam.answer" + arrQuestionDetails[q_rowno]["questionid"] + ".length");
            for (pos = 0; pos < alen; pos++) {
                eval("document.frmonlineexam.answer" + arrQuestionDetails[q_rowno]["questionid"] + "[" + pos + "].disabled=false")
            }
        }
    }
};
