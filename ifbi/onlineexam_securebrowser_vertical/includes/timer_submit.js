var secs;
var timerID = null;
var timerRunning = false;
var delay = 1000;

function InitializeTimer(b, a, c) {
    secs = b;
    StopTheClock();
    StartTheTimer(a, c)
}

function StopTheClock() {
    if (timerRunning) {
        clearTimeout(timerID)
    }
    timerRunning = false
}

function StartTheTimer(formname, formaction) {
    if (secs == 300) {
        fireMyPopup()
    }
    if (secs <= 0) {
        StopTheClock();
        eval("document." + formname + ".time_to_submit.value = 0");
        eval("document." + formname + ".action='" + formaction + "'");
        eval("document." + formname + ".target='_self'");
        eval("document." + formname + ".submit()")
    } else {
        var min = Math.floor(secs / 60);
        var remaining_secs = secs % 60;
        var texthr = Math.floor(min / 60);
        var textmin = new String(min % 60);
        if (textmin.length == 1) {
            textmin = "0" + textmin
        }
        var textsec = new String(remaining_secs);
        if (textsec.length == 1) {
            textsec = "0" + textsec
        }
        eval("self.document." + formname + ".time_to_submit.value = " + secs);
        var temp = secs;
        secs = secs - 1;
        timerRunning = true;
        timerID = self.setTimeout("StartTheTimer('" + formname + "','" + formaction + "')", delay)
    }
}
var bottom = 0;
var padding = 5;
var X = 5;
var Y = 0;

function dropMyPopup() {
    Y++;
    if (Y > bottom) {
        return
    }
    document.getElementById("styled_popup").style.top = Y + "px";
    setTimeout("dropMyPopup()", 25)
}

function fireMyPopup() {
    var c;
    if (self.pageYOffset) {
        c = self.pageYOffset
    } else {
        if (document.documentElement && document.documentElement.scrollTop) {
            c = document.documentElement.scrollTop
        } else {
            if (document.body) {
                c = document.body.scrollTop
            }
        }
    }
    var d;
    if (self.innerHeight) {
        d = self.innerHeight
    } else {
        if (document.documentElement && document.documentElement.clientHeight) {
            d = document.documentElement.clientHeight
        } else {
            if (document.body) {
                d = document.body.clientHeight
            }
        }
    }
    bottom = c + d - padding - 85;
    Y = c;
    document.getElementById("styled_popup").style.right = X + "px";
    document.getElementById("styled_popup").style.display = "block";
    dropMyPopup();
    setTimeout("styledPopupClose()", 60000)
}

function styledPopupClose() {
    document.getElementById("styled_popup").style.display = "none";
    Y = bottom
};