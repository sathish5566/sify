<?PHP require_once("../dbconfig.php");
if( ($_SERVER['REMOTE_ADDR']!="210.210.122.203") &&  ($_SERVER['REMOTE_ADDR']!= "127.0.0.1") &&  ($_SERVER['REMOTE_ADDR']!= "124.7.228.177") ){
	ob_end_clean();
	header("WWW-Authenticate: Basic Realm=BALA");
	header("HTTP/1.0 401 Unauthorized");
	print("<h1>You are Not Authorized to view this page!!!</h1>");	
	exit;	
}
function getMicroTime() {
        list($usec, $sec) = explode(" ",microtime());
        return ((float)$usec + (float)$sec);
}
	
$Dbobj=array();
$dbarray=array("onlineexam_generic"=>"onlineexam_generic");
$outputformatarray=array("html"=>"html","xls"=>"excel"); 

if(isset($_POST['Submit']))
{
	if($_POST['server'] == 'master')
		masterConnect();
	else
		slaveConnect();
	if($_POST['txtarqry']!='')
		$txtarqry=stripslashes($_POST['txtarqry']);
		$outformat=mysql_escape_string($_POST['outformat']);	
		$startTime=getMicroTime();
		if($_POST['server'] == 'master')
			$rs=mysql_query($txtarqry,$MASTERCONN);			
		else
			$rs=mysql_query($txtarqry,$SLAVECONN);						
		$totalTime=getMicroTime()-$startTime;
		$totalTime=round($totalTime,4);		
}

if(isset($outformat) && $outformat=="xls")
	{
				header("Cache-Control: no-cache,must-revalidate");
				header("Content-type: application/msexcel");
				header("Content-Disposition: attachment; filename=QueryReport_".date("YmdHis").".xls");
				//header("Content-Type: application/msexcel;filename=QueryReport_".date("YmdHis").".xls");
				if(is_resource($rs))
				{						
						$col=mysql_num_fields($rs);				
						echo "<table width=\"100%\" border=\"0\" align=\"left\" cellpadding=\"5\" cellspacing=\"1\"><tr>";
						for($c=0;$c<$col;$c++){				
							echo "<td height=\"25\" align=\"left\" valign=\"top\" class=\"colorhead\">".mysql_field_name($rs,$c)."                        </td>";								    					
						}								
						echo "</tr>";
						while($r=mysql_fetch_array($rs))	
						{
							echo "<tr>";
							for($c=0;$c<$col;$c++)			
								echo "<td height=\"25\" align=\"left\" valign=\"top\" class=\"txt\">".$r[$c]."</td>";						
							echo "</tr>";
						}																		
						echo "</table>";
						exit;																												
				}															
	}
?>
					 					 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Mysql Query Browser</title>
<style>
.red {

	color: #CC0000;

	font-family: Verdana, Arial, Helvetica, sans-serif;

	font-size: 10px;

}
.colorbuttons_small {

background-color: #66BFE1;

font-family: Arial, Helvetica, sans-serif;

font-size: 11px;

font-style: normal;

border: 0px ridge #66BFE1;

color: #FFFFFF;

background-position: center center;

font-weight: bolder;

font-variant: normal;

height: 20px;

width: 50px;

}

.colorhead {

	background-color: #004A83;

	font-family: Arial, Helvetica, sans-serif;

	font-size: 11px;

	font-style: normal;

	border: 0px ridge #004A83;

	color: #FFFFFF;

	background-position:center;

	font-weight: bolder;

	font-variant: normal;

}
.colorhead1 {

	background-color: #FFFFFF;

	font-family: Arial, Helvetica, sans-serif;

	font-size: 11px;

	font-style: normal;

	border: 0px ridge #004A83;

	color: #000000;

	background-position:center;

	font-weight: bolder;

	font-variant: normal;

}

.txt{
	background-color: #66BFE1;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-style:normal;
	border: 0px ridge #004A83;
	color: #FFFFFF;
	background-position:center;
	font-variant: normal;
}



.txtborder
{
	  BORDER-BOTTOM: #004A83 1px solid;

	  BORDER-LEFT: #004A83 1px solid;

	  BORDER-RIGHT: #004A83 1px solid; 

	  BORDER-TOP: #004A83 1px solid;
}


</style>
<script>
function valid()
{
	form=document.form1;		
	if(form.seldb.value ==''){
		alert("Please select DB");
		form.seldb.focus;
		return false;		
	}
	else if(form.txtarqry.value=='')
	{
		alert("Please enter query");
		form.txtarqry.focus;
		return false;		
	}else
		return true;		
}
</script>
</head>
<body>
<table width="100%" border="0" align="left" cellpadding="1" cellspacing="0">
  <tr>
    <td class="colorhead"><form id="form1" name="form1" method="post" action="" onsubmit="return valid()">
      <table width="100%" border="0" cellspacing="1" cellpadding="5">
        <tr>
          <td colspan="2" align="left" valign="top" class="colorhead">MYSQL Query Interface </td>
        </tr>
        <tr>
          <td width="11%" align="left" valign="top" class="colorhead1" >Database:</td>
          <td width="89%" align="left" valign="top" bgcolor="#FFFFFF"><select name="seldb" class="txtborder" id="seldb">
              <option value="" >Select</option>
              <?PHP  foreach ($dbarray as $k=>$v)
						{
							echo "<option value=".$k; 
							if( isset($seldb) && $seldb!='' && trim($seldb)==trim($k) )
								echo " selected=selected ";								
							echo ">".$v."</option>";          
						}
			?>
            </select>          </td>
        </tr>
        <tr>
          <td align="left" valign="top" class="colorhead1" >Query : </td>
          <td align="left" valign="top" bgcolor="#FFFFFF">
		  <textarea name="txtarqry" cols="75" rows="5" class="txtborder" id="txtarqry"><?PHP if(isset($txtarqry) && $txtarqry!=' ') echo $txtarqry;?></textarea></td>
        </tr>
        <tr>
          <td align="left" valign="top" bgcolor="#FFFFFF"><span class="colorhead1">OUTPUT FORMAT: </span></td>
          <td align="left" valign="top" bgcolor="#FFFFFF">
          <select name="outformat" class="txtborder" id="outformat">
             
                  <?PHP  foreach ($outputformatarray as $k=>$v)
						{
							echo "<option value=".$k; 
							if( isset($outformat) && $outformat!='' && trim($outformat)==trim($k) )
								echo " selected=selected ";								
							echo ">".strtoupper($v)."</option>";          
						}
			?>                
           </select>          </td>
        </tr>
        <tr>
          <td align="left" valign="top" bgcolor="#FFFFFF"><span class="colorhead1">RUN IN SERVER : </span></td>
          <td align="left" valign="top" bgcolor="#FFFFFF">
		  <select name="server" class="txtborder" id="server">
		  <option value="slave">SLAVE</option>
		  <option value="master">MASTER</option>		 		              			
          </select></td>
        </tr>
        <tr>
          <td align="left" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
          <td align="left" valign="top" bgcolor="#FFFFFF"><input name="Submit" type="submit" class="colorbuttons_small" value="Submit" /></td>
        </tr>
      </table>
    </form>
    </td>
  </tr>
</table>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>
</p>
  
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top" class="colorhead"><table width="100%" border="0" align="left" cellpadding="5" cellspacing="1">
      <tr >
        <td  height="26" align="left" valign="top"> &nbsp;&nbsp;Query Result:</td>
      </tr>
      <tr>
        <td height="33" align="left" valign="top"  bgcolor="#FFFFFF">		 
		 <?PHP
if(isset($_POST['Submit']) && $outformat=="html")
{										
		if(is_bool($rs) && $rs==false)
			echo "<span class=\"red\">".mysql_error($con)."</span>";
		else if($rs==true)		
		 echo "<span class=\"red\">"."The query Excuted Successfully. The no.of affected rows:".mysql_affected_rows($SLAVECONN)."\n
		 Query Execution time:".$totalTime." Sec</span><br/>";			
		if(is_resource($rs))
		{						
				$col=mysql_num_fields($rs);				
				echo "<table width=\"100%\" border=\"0\" align=\"left\" cellpadding=\"5\" cellspacing=\"1\"><tr>";
			    for($c=0;$c<$col;$c++){				
					echo "<td height=\"25\" align=\"left\" valign=\"top\" class=\"colorhead\">".mysql_field_name($rs,$c)."</td>";								    					
				}								
				echo "</tr>";
				while($r=mysql_fetch_array($rs))	
				{
					echo "<tr>";
					for($c=0;$c<$col;$c++)			
						echo "<td height=\"25\" align=\"left\" valign=\"top\" class=\"txt\">".$r[$c]."</td>";						
					echo "</tr>";
				}																		
				echo "</table>";																
	 }								
	 		
}
@mysql_free_result($rs);	
@mysql_close($con);					 					  	
?>		<span>
		</td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
