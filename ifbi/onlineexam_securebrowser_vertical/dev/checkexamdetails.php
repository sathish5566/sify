<?php /****************************************************
* Application Name            :  IIB 
* Module Name                 :  Exam Module
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  iib_candidate_tracking,iib_candidate_test,iib_question_paper
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              : 
* Created ON                  :  
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  22-01-2010
* Description                 :  Interface for the exam check list
***************************************************/
ob_start();
require_once("../dbconfig.php");
//masterConnect();
if( isset($_POST['Submit']) && $_POST['Submit'] == 'Truncate Load test Data')
{
	//$CONN=mysql_connect('221.135.170.48','loaduser','loaduser') or die("Connection failed truncate user".mysql_error($CONN));
	//$db=@mysql_select_db('iib',$CONN)  or die("db selection failed truncate user".mysql_error($CONN));
	
	$CONN=mysql_connect('localhost','root','') or die("Connection failed truncate user".mysql_error($CONN));
	$db=@mysql_select_db('unified_online_qp_chk',$CONN)  or die("db selection failed truncate user".mysql_error($CONN));
	
	mysql_query("truncate iib_session",$CONN);
	mysql_query('truncate iib_candidate_tracking',$CONN);
	mysql_query('truncate iib_candidate_test',$CONN);
	mysql_query('truncate iib_ta_tracking',$CONN);
	mysql_query('truncate iib_candidate_scores',$CONN);
	mysql_query('truncate iib_feedback',$CONN);
	mysql_query('update iib_candidate_iway set exam_date=now(),exam_time=now() ',$CONN);
	mysql_query('update iib_exam_schedule set exam_date=now()',$CONN);
	mysql_query('update iib_exam_slots set slot_time=now()',$CONN);
	mysql_query('update iib_ta_iway set exam_date=now()',$CONN);
	mysql_query('update iib_iway_vacancy set exam_date=now(),exam_time=now()',$CONN);
	mysql_query("update iib_question_paper_details set answer='',updated_time='' where answer in(1,2,3,4,5)",$CONN);			
	mysql_query("truncate iib_response",$CONN);		
	mysql_query("truncate iib_response_final",$CONN);		
	mysql_close($CONN);
	header("Location:checkexamdetails.php?msg=truncated successfully");
	exit;
}

slaveConnect();
$qry="select count(1) from iib_ta_tracking";
$result=@mysql_query($qry,$SLAVECONN);
list($loggedta_count) = mysql_fetch_row($result);

$qry1="select distinct membership_no from iib_candidate_tracking";
$result1=@mysql_query($qry1,$SLAVECONN);
$loggedcandidate_count=mysql_num_rows($result1);


$qry2="select distinct membership_no from iib_candidate_test where current_session='Y'";
$result2=@mysql_query($qry2,$SLAVECONN);
$attendcandidate_count = mysql_num_rows($result2);

$qry3="select membership_no from iib_candidate_scores";
$result3=@mysql_query($qry3,$SLAVECONN);
$compcandidate_count=mysql_num_rows($result3);
$arrcompcandidate=array();
while(list($compcandidate) = mysql_fetch_row($result3)){
	array_push($arrcompcandidate,$compcandidate);
}
if( count($arrcompcandidate) > 0 ){
	$X=implode(",",$arrcompcandidate);
	 $qry4="select distinct membership_no from iib_candidate_test where test_status='IC' and current_session='Y' and membership_no NOT IN('".$X."')";
}else
	$qry4="select distinct membership_no from iib_candidate_test where test_status='IC' and current_session='Y' ";

$result4=@mysql_query($qry4,$SLAVECONN);
$incompcandidate_count=mysql_num_rows($result4);


$qry5="select question_paper_no,count(1) from iib_question_paper_details where answer in (1,2,3,4,5) group by question_paper_no having count(1) = 100";
$result5=@mysql_query($qry5,$SLAVECONN);
$equal100candidate_count = mysql_num_rows($result5);
//list($equal100candidate_count,$eq100) = mysql_fetch_row($result5);

$qry6="select question_paper_no,count(1) from iib_question_paper_details where answer in (1,2,3,4,5) group by question_paper_no having count(1) < 100";
$result6=@mysql_query($qry6,$SLAVECONN);
$less100candidate_count=0;
$less100candidate_count=mysql_num_rows($result6);
//list($less100candidate_count,$neq100) = mysql_fetch_row($result6);
if($less100candidate_count > 0)
{
	$arrqp=array();
	$qp_id=array();
		
	$qry7="select question_paper_no from iib_question_paper_details where answer in (1,2,3,4,5) group by question_paper_no having count(1) < 100";
	$result7=@mysql_query($qry7,$SLAVECONN);
	while($r=mysql_fetch_array($result7))
	{
		array_push($arrqp,$r['question_paper_no']);		
	}			
	
	$qp=implode(',',$arrqp);						 					
	$arrMEM=array();
	$arrQP=array();
	$arrStatus=array();
	
	
			$i=0;
					$qry8="select membership_no,question_paper_no from iib_question_paper where question_paper_no in ($qp) ";
					$result8=@mysql_query($qry8,$SLAVECONN);
					while($r1=mysql_fetch_array($result8))
					{
						$arrMEM[$i]=$r1['membership_no'];
						$arrQP[$i]=$r1['question_paper_no'];
						$i++;					
					}					
							
					
					$qry9="select question_id,question_paper_no from iib_question_paper_details where question_paper_no in ($qp) and answer=''";
					$result9=@mysql_query($qry9,$SLAVECONN);
					while($r2=mysql_fetch_array($result9))
					{												
						if($qp_id[$r2['question_paper_no']] != '')						
						$x = $qp_id[$r2['question_paper_no']]. " , " . $r2['question_id'];						
						else
						  $x = $r2['question_id'];
						  
						$qp_id[$r2['question_paper_no']]=$x;
						$x='';
					}			
					$mem=implode(",",$arrMEM);
					$qry10="select test_status,membership_no from iib_candidate_test where membership_no in ($mem) and current_session='Y' ";
					$result10=@mysql_query($qry10,$SLAVECONN);
					while($r3=mysql_fetch_array($result10))
					{
						$arrStatus[$r3['membership_no']]=$r3['test_status'];
					}				
}													
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>The Indian Institute of Banking &amp; Finance</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../images/iibf.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<center>
<table width="780" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <?php include("../includes/header.php");?>
  </tr>
  <tr>  </tr>
  <tr> 
    <td width="780"  align="left" valign="top" background="../images/tile.jpg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td class="alertmsg">Please Refer this page after exam completion , The details are taken from DB slave server. </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="158" align="left" valign="top" background="../images/tile.jpg"><table width="100%" border="1" align="left" cellpadding="5" cellspacing="3">
      <tr>
        <td width="62%" class="ColumnHeader">Label</td>
        <td width="38%" class="ColumnHeader">Count</td>
      </tr>
      <tr>
        <td class="greybluetext12">Total no.of Logged TA: </td>
        <td class="greybluetext12"><?PHP echo $loggedta_count?></td>
      </tr>
      <tr>
        <td class="greybluetext12">Total no.of Logged Candidates: </td>
        <td class="greybluetext12"><?PHP echo $loggedcandidate_count?></td>
      </tr>
      <tr>
        <td class="greybluetext12">Total no.of candidates viewed Question Paper(started exam) : </td>
        <td class="greybluetext12"><?PHP echo $attendcandidate_count?></td>
      </tr>
      <tr>
        <td class="greybluetext12">Total no.of candidates completed exam: (having score) </td>
        <td class="greybluetext12"><?PHP echo $compcandidate_count?></td>
      </tr>
      <tr>
        <td class="greybluetext12">Total no.of candidates Not completed exam: (not having score) </td>
        <td class="greybluetext12"><?PHP echo $incompcandidate_count?></td>
      </tr>
	   <tr>
        <td class="greybluetext12">100 Questions attempted candidate count : (having score) </td>
        <td class="greybluetext12"><?PHP echo $equal100candidate_count?></td>
      </tr>
	   <tr>
        <td class="greybluetext12">Less than 100 questions attempted candidate count : (having score with atleaset 1 question answered) </td>
        <td class="greybluetext12"><?PHP echo $less100candidate_count?></td>
      </tr>
	   <tr>
	     <td height="53" colspan="2" align="left" valign="top" class="greybluetext12"><table width="100%" border="1" cellspacing="3" cellpadding="3">
           <tr>
             <td colspan="4" class="ColumnHeader">LESS THAN 100 questions attempted candidate details </td>
             </tr>
           <tr>
             <td width="15%" align="left" valign="top" class="ColumnHeader">Candidate ID </td>
             <td width="19%" align="left" valign="top" class="ColumnHeader">Question Paper No </td>
             <td width="33%" align="left" valign="top" class="ColumnHeader">Un anwered Question Id's </td>
             <td width="33%" align="left" valign="top" class="ColumnHeader">Test Status </td>
           </tr>
		  <?PHP 		  	
		  for( $j =0; $j < count($arrQP); $j++) { ?> 
			   <tr>
				 <td align="left" valign="top"><?PHP echo $arrMEM[$j]?></td>
				 <td align="left" valign="top"><?PHP echo $arrQP[$j]?></td>
				 <td align="left" valign="top"><?PHP echo $qp_id[$arrQP[$j]]?></td>
			     <td align="left" valign="top"><?PHP echo $arrStatus[$arrMEM[$j]]?></td>
			   </tr>
  		<?PHP   } ?>
         </table></td>
	     </tr>
	   <tr>
	     <td height="54" colspan="2" align="left" valign="top" class="greybluetext12"><form id="trfrm" name="trfrm" method="post" action="" onsubmit="if(confirm('Are You Sure you want to truncate data?')) document.trfrm.submit();">
	       <label>
	       <div align="center">
	         <input name="Submit" type="submit" class="button" value="Truncate Load test Data"/>
	         </div>
	       </label>
           </form> </td>
	     </tr>
    </table></td>
  </tr>
  <tr>
 <?php include("../includes/footer.php");?>
  </tr>
</table>
</center>
</body>
</html>
