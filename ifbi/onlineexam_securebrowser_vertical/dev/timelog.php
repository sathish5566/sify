<?php /****************************************************
* Application Name            :  IIB 
* Module Name                 :  Exam Module
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  iib_candidate_tracking,iib_candidate_test,iib_question_paper
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              : 
* Created ON                  :  
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  22-01-2010
* Description                 :  Interface for the exam check list
***************************************************/
ob_start();
require_once("../dbconfig.php");
if( isset($_POST['Submit']) && ($_POST['Submit'] == 'Get Time Log') )
{
    $memno=mysql_escape_string($_POST['memno']);
    slaveConnect();
    $qry="select * from timelog where membership_no='$memno' order by test_id,servertime";
    $result=@mysql_query($qry,$SLAVECONN);
    $cnt=mysql_num_rows($result);
}													
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>IBPS</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../images/iibf.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
function validate(){
    if(document.getElementById('memno').value == ''){
        alert('Please enter membershipno');
        return false;        
    }else
        return true;
}
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="frmtimelog" method=post onsubmit="return validate()">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
    <td height="158" align="left" valign="top" background="../images/tile.jpg">
    <table width="100%" border="1" align="left" cellpadding="5" cellspacing="3">
      <tr>
        <td width="12%" class="ColumnHeader">Membership No:</td>
        <td width="78%" class="ColumnHeader"><input type="text" name="memno" id="memno" maxlength=20/>&nbsp;&nbsp;
        <input name="Submit" type="submit" class="button" value="Get Time Log"/>            
        </td>
      </tr>
      <tr>
        <td class="greybluetext12">Total no.of Logged TA: </td>
        <td class="greybluetext12"><?PHP echo $loggedta_count?></td>
      </tr>
	   <tr>
         <td height="54" colspan="2" align="left" valign="top" class="greybluetext12">
        <?PHP if( isset($_POST['Submit']) && ($_POST['Submit'] == 'Get Time Log') ) { ?>
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
        <tr><td  class="ColumnHeader1">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="1">
        <tr>
        <td width="5%" class="ColumnHeader1">S.no</td>
        <td width="15%" class="ColumnHeader1">Membership No</td>
        <td width="10%" class="ColumnHeader1">QuestionPaperNo</td>
        <td width="10%" class="ColumnHeader1">Test ID</td>
        <td width="15%" class="ColumnHeader1">Server Time</td>
        <td width="15%" class="ColumnHeader1">Client Time</td>
        <td width="15%" class="ColumnHeader1">Intial Time diff b/w Server / Client Time</td>
        <td width="30%" class="ColumnHeader1">Time Diff ((Server Time - ClientTime)+ IntialTimeDiff)in Seconds</td>
        </tr>				
        <?PHP 
            $i=1; 
            $totdiff=0; $oldtestid=0;
            while($r=mysql_fetch_object($result)){
                if( ($i == 1) || ( $oldtestid != $r->test_id) ){
                    $inidiff=strtotime($r->servertime) - strtotime($r->clienttime);
                    $oldtestid = $r->test_id;
                }
                if(strtotime($r->servertime)== strtotime($r->servertime)){
                    $diff=0;                 
                }else
                    $diff=(strtotime($r->servertime) - strtotime($r->clienttime))- $inidiff;
                $totdiff+=$diff;
            ?>						
         <tr>
         <td bgcolor="#E8EFF7" class="greybluetext12"><?PHP echo $i++;?></td> 
         <td bgcolor="#E8EFF7" class="greybluetext12"><?PHP echo $r->membership_no;?></td> 
         <td bgcolor="#E8EFF7" class="greybluetext12"><?PHP echo $r->questionpaperno;?></td> 
         <td bgcolor="#E8EFF7" class="greybluetext12"><?PHP echo $r->test_id; ?></td> 
         <td bgcolor="#E8EFF7" class="greybluetext12"><?PHP echo $r->servertime; ?></td> 
         <td bgcolor="#E8EFF7" class="greybluetext12"><?PHP echo $r->clienttime; ?></td> 
         <td bgcolor="#E8EFF7" class="greybluetext12"><?PHP echo $inidiff; ?></td> 
         <td bgcolor="#E8EFF7" class="greybluetext12"><?PHP echo $diff; ?></td> 
         </tr>
        <?PHP  }    ?>
         <tr>
         <td bgcolor="#E8EFF7" class="greybluetext12" colspan=7>Total Time diffrence between Server/Client:(If negative, client time faster than server) </td> 
         <td bgcolor="#E8EFF7" class="greybluetext12"><?PHP echo $totdiff;?></td> 
        </tr>
    </table>
</td></tr></table>
      <?PHP } ?>
</td>
	     </tr>
    </table></td>
  </tr>
  <tr>
 <?php include("../includes/footer.php");?>
  </tr>
</table>
</center>
</form>
</body>
</html>
