<?php /****************************************************
* Application Name            :  IIB 
* Module Name                 :  TA Options 
* Revision Number             :  1
* Revision Date               :  -
* Table(s)                    :  -
* Tables used for only selects:  -
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :  -
* Document/Reference Material :  -
* Created By	              :  -
* Created ON                  :  -
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  22-01-2010
* Description                 :  Interface for the TA to login
*****************************************************/
require_once("dbconfig.php");
masterConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkTALogin();
$includeJS = '<script language="javascript" type="text/javascript" src="./includes/browser.js"></script>';
$includeJS .= '<script language="JavaScript">history.go(1);</script>';
$bodyOnload = ' onload="document.getElementById(\'stexam\').focus()"';

include("themes/default/header.php");

?>


      <form method="post" name="loginfrm" id="loginfrm" >
  <div id="body_container_inner">
    <div id="start_exam">
<?PHP
if( $emsg != "" ) echo "<div class=\"errMsg\">$emsg</div>";
if( $_GET['reason']==1 ) echo "<div class=\"errMsg\">You Need To Login/Re-Login</div>";
if( $_GET['reason']==2 ) echo "<div class=\"errMsg\">You have already taken the exam</div>";
if( $_GET['reason']==3 ) echo "<div class=\"errMsg\">You have already taken the exam.Please click on restart instead of start the exam.</div>";
if( $_GET['reason']==4 ) echo "<div class=\"errMsg\">You have already completed the exam.</div>";
if( $_GET['reason']==5 ) echo "<div class=\"errMsg\">There is no exam scheduled for you today.</div>";
if( $_GET['reason']==6 ) echo "<div class=\"errMsg\">You have not started the exam. Please click on start the exam instead of restart the exam.</div>";
if( $_GET['reason']==7 ) echo "<div class=\"errMsg\">This candidate has completed the exam.</div>";
if( $_GET['reason']==8 ) echo "<div class=\"errMsg\">QP not assigned for this candidate / Invalid access to online exam page</div>";
if( $_GET['reason']==9 ) echo "<div class=\"errMsg\">You have not not completed your exam for viewing score card</div>";
?>
      <div class="outerbox" style="margin-bottom:40px;">
        <div class="innerbox">
          <h1>
            <a title="Click to login and start your exam" href="logincandidate.php" class="attemptbig" id="stexam">Start the Exam</a>
          </h1>
        </div>
      </div>
      <div class="outerbox" style="margin-bottom:40px;" >
        <div class="innerbox">
          <h1>
            <a title="Click to relogin and restart your exam" href="restartingexam.php" class="attemptbig">Restart the Exam</a>
          </h1>
        </div>
      </div>
      <div class="outerbox">
        <div class="innerbox">
          <h1>
            <a title="Click to view/print your score card" href="view_score_card.php" class="attemptbig">View Score Card</a>
          </h1>
        </div>
      </div>
    </div>
  </div>
      </form>
<?php include("themes/default/footer.php");?>
