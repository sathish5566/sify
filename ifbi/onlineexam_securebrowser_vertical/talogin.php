<?php /****************************************************
* Application Name            :  IIB 
* Module Name                 :  Exam Module
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  iib_candidate,iib_exam_subjects,iib_candidate_iway,iib_candidate_test
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              : 
* Created ON                  :  
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  22-01-2010
* Description                 :  Interface for the candidate Ta override 
*****************************************************/
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkTALogin();
checkCandidateLogin();

//To pass the lefttime 
$emsg="";

$login = getVal($_POST['login']);
$password=getVal($_POST['password']);
if( ($login!='') && ($password!=''))
{	
		$sql="select ta_password,password('$password') from iib_ta_details where ta_login='$login'";
		$result=@mysql_query($sql,$SLAVECONN);
		if($result)
		{
			if (mysql_num_rows($result) > 0)
			{
	        	list($pass,$userpass) = @mysql_fetch_row($result);
        		if ($pass == $userpass)
        		{
	        		$hostIP = $_SERVER['REMOTE_ADDR'];
	        		$cafeID = $_SESSION['cafeID'];
					$sessionID = session_id();
					trackTALogin($login, $cafeID, $hostIP, $sessionID);
					$_SESSION['login_override']='Y';
					@header("Location:logincandidate1.php");
					exit;					
				}
        		else
                   $emsg="Invalid Login. Please check whether the Caps Lock on your keyboard is turned on.";
           }
           else
           	  $emsg="Invalid Login. Please check whether the Caps Lock on your keyboard is turned on.";
		}	
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>The Indian Institute of Banking &amp; Finance</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<script language='JavaScript' src="./includes/validations.js"></script>
<script language='javascript'>
function submitForm(e)
{	
	var keyCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
	if (keyCode == 13)
	{
		validate();
	}
}
function validate()
{
	loginname=document.loginfrm.login.value;
	loginname=trim(loginname);
	document.loginfrm.login.value=loginname;
	pass=document.loginfrm.password.value;

	if (loginname=='')
	{
		alert('TA Login Empty!');
		document.loginfrm.login.focus(); 
		return;
	}
	
	 if(pass=='')
	 {
    	alert('Password Empty!');
	  	document.loginfrm.password.focus();
	  	return;
	 }														 	
	document.loginfrm.submit();
}
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="javascipt:document.loginfrm.login.focus();" onKeyPress='javascript:submitForm(event)'>
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <?php include("includes/header.php");?>
  </tr>
  <tr> 
    
  </tr>
  <tr> 
    <td width="100%" background="images/tile.jpg" height="315">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td>&nbsp;</td>
          <td class="textgrey11"><b>&nbsp;<?php if($emsg!="") echo $emsg;?></b></td>
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
          <td background="images/logintile.jpg" width="279" height="160" valign="top">             
    <form name="loginfrm" action="talogin.php" method="Post">
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="125" height="30">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td height="30" valign="top"> 
                  <input type="text"  onKeyPress="return letternumber(event)" name="login" class="textbox" maxlength="20" size="15">
                </td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td> 
                  <input type="password" name="password" class="textbox" size="15" maxlength="50" onKeyPress="return letternumber(event)">
                </td>
              </tr>
              <tr align="center" valign="bottom"> 
                <td colspan="2" height="35">                         
                        <input class='button' id='ClearFrm' type='button' value='Submit' name="taoverwride" onClick="javascript:validate();">

                </td>
              </tr>
            </table>
         </form> 
     </td>
          <td width="20" height="160" valign="top">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <?php 
    include("includes/footer.php");
    //mysql_close($SLAVECONN);

?>
  </tr>
</table>
</center>
</body>
</html>
