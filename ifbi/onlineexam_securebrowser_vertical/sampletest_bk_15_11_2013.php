<?php
/****************************************************
* Application Name            :  IIB 
* Module Name                 :  Exam Module
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  -
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              : 
* Created ON                  :  
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  22-01-2010
* Description                 :  Interface for the Sample Exam page
*****************************************************/
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkTALogin();
checkCandidateLogin();
require_once("constants.inc");
 

$time_to_submit = getVal($_REQUEST['time_to_submit']);
$qid = isset($_GET["qid"])?$_GET["qid"]:0;
$qindex = $_POST['qindex'];

$membershipNo = $_SESSION['memno'];
$subjectcode = $_SESSION['sc'];
$examcode = $_SESSION['ex'];		
$mediumcode = $_SESSION['mc'];	
$starttime = date('Y-m-d H:i:s');
$starttimehrs =  substr($starttime,10,strlen($starttime));

$alpha = array('1', '2', '3', '4', '5');
$arrprevAnswers = array('1'=>0, '2'=>0, '3'=>0, '4'=>0, '5'=>0);
/*need to comment if hindi uploaded in unicode */
$mediumcode='EN';
//if($mediumcode !='EN')
//	$mediumcode='HIDV';

$marked_string = $_REQUEST['marked_string'];
	
$includeStyleHD = '<style>
.qnodisp_tag { Background-color:#FF8000;font-family="Verdana,Arial,Helvetica,sans-serif";font-weight:bold;font-size:10px;}
</style>';

if( ($mediumcode == "HISU") || ($mediumcode == "HISH") || ($mediumcode == "HIDV") ) {
	$metaTagHD = '<meta http-equiv="content-type" content="text/html; charset=x-user-defined">';
}else {
	$metaTagHD = '<meta http-equiv="content-type" content="text/html; charset=utf-8">';
}

if ($mediumcode == "HISU"){
	$includeStyleHD .= <<<CSSHD
<STYLE TYPE="text/css">
  @font-face {
    font-family: Suchi-Dev-0714W;
    font-style:  normal;
    font-weight: normal;
    src: url(./eot/SUCHIDE0.eot);
  }
.qn{font-family:'Suchi-Dev-0714W';font-weight:bold;font-size:18px}
.ans{font-family:'Suchi-Dev-0714W';font-weight:normal;font-size:18px}
  
</STYLE>
CSSHD;
}else if ($mediumcode == "HISH"){
	$includeStyleHD .= <<<CSSHD
<STYLE TYPE="text/css">
  @font-face {
    font-family: Shree-Dev-0714EW;
    font-style:  normal;
    font-weight: normal;
    src: url(./eot/SHREEDE0.eot);
  }
.qn{font-family:'Shree-Dev-0714EW';font-weight:bold;font-size:18px}
.ans{font-family:'Shree-Dev-0714EW';font-weight:normal;font-size:18px}  
</STYLE>
CSSHD;
}else if ($mediumcode == "HIDV"){
	$includeStyleHD .= <<<CSSHD
<STYLE TYPE="text/css">
 @font-face {
	font-family: DVBW-TTYogeshEN;
	font-style:  normal;
	font-weight: normal;
	src: url(./eot/DVBWTTY0.eot);
 }
 .qn{font-family:'DVBW-TTYogeshEN';font-weight:bold;font-size:18px}
 .ans{font-family:'DVBW-TTYogeshEN';font-weight:normal;font-size:18px}
</STYLE>
CSSHD;
}
$includeStyleHD .= <<<CSSHD
<style type="text/css">
<!--
.style2 {
	color: #FFFFFF;
	font-weight: bold;
}
.style3 {color: #FFFFFF}
.style4 {
	font-size: 14px;
	font-weight: bold;
}
.greybluetextquestion{font-size: 16px}
.greybluetextans{font-size: 14px}
-->
</style>
CSSHD;
$includeJS = <<<JSSC
<script language="JavaScript" type="text/javascript" src="./includes/tinybox.js"></script>
<script language="javascript" type="text/javascript" src="./includes/browser_test.js"></script>
<script>
var secs;
var timerID = null;
var timerRunning = false;
var delay = 1000;
var timecheck = 600;
var formname;
var formaction;

function InitializeTimer(c, b, a) {
    secs = c;
    StopTheClock();
    formname = b;
    formaction = a;
    timerID = setInterval("displayTimer()", 1000);
}
function StopTheClock() {
    if (timerRunning) {
        clearInterval(timerID);
    }
    timerRunning = false;
}

function displayTimer() {	
    if (secs == 300) {
		fireMyPopup();
    }
    if (secs <= 0) {
        StopTheClock();
        eval("document." + formname + ".time_left.value = 0");
        if (formname == "sample_form") {
            preview_submit(1);
        } else {
            eval("document." + formname + ".action='" + formaction + "'");
            eval("document." + formname + ".submit()");
        }
    } else {
        var min = Math.floor(secs / 60);
        var remaining_secs = secs % 60;
        var texthr = Math.floor(min / 60);
        var textmin = new String(min % 60);
        if (textmin.length == 1) {
            textmin = "0" + textmin;
        }
        var textsec = new String(remaining_secs);
        if (textsec.length == 1) {
            textsec = "0" + textsec;
        }
        self.status = "TIME LEFT : " + texthr + " Hr(s) " + textmin + " Min(s) " + remaining_secs + " Secs ";
        eval("self.document." + formname + ".timeleft.value ='" + texthr + ":" + textmin + ":" + textsec + " hrs'");
        eval("self.document." + formname + ".time_left.value = " + secs);
        var temp = secs;
        secs = secs - 1;
        timerRunning = true;      
    }
};

var bottom = 0;
var padding = 5;
var X = 5;
var Y = 0;

function dropMyPopup() {
  Y++;
  if( Y > bottom ) return;
  document.getElementById("styled_popup").style.top = Y + "px";
  setTimeout("dropMyPopup()", 25);
}

function fireMyPopup() {
  var scrolledY;
  if( self.pageYOffset ) {
    scrolledY = self.pageYOffset;
  } else if( document.documentElement && document.documentElement.scrollTop ) {
    scrolledY = document.documentElement.scrollTop;
  } else if( document.body ) {
    scrolledY = document.body.scrollTop;
  }

  var centerY;
  if( self.innerHeight ) {
    centerY = self.innerHeight;
  } else if( document.documentElement && document.documentElement.clientHeight ) {
    centerY = document.documentElement.clientHeight;
  } else if( document.body ) {
    centerY = document.body.clientHeight;
  }

  // Don't forget to subtract popup's height! ( 300 in our case )
  bottom = scrolledY + centerY - padding - 110;
  Y = scrolledY;

  document.getElementById("styled_popup").style.right = X + "px";
  document.getElementById("styled_popup").style.display = "block";
  dropMyPopup();
  setTimeout("styledPopupClose()",60000)
}

function styledPopupClose() {
  document.getElementById("styled_popup").style.display = "none";
  Y = bottom; // if it was closed, make sure extra computations are done in dropMyPopup()
}

var UNANSWERCOUNT=0;
var ANSWERCHECKED=0;
var TAGGEDCOUNT = 0;
/* QUESTION / ANSWER AMNIPULATION FUNCTIONS */
/* ANSWER  OPTION SELECTION ONLCLICK FUNCTION */
function markAnswer() {    	
		for (var i = 0; i < parseInt(document.getElementById("question_count").value); i++) {
	        var qid = arrQuestionid[i];        
			if (arrprevanswers[qid] != 0 && arrprevanswers[qid]!= null) {
            	var ele = "answer" + qid;
            	for (var j = 0; j < eval("document.sample_form." + ele + ".length"); j++) {
                	if (eval("document.sample_form." + ele + "[" + j + "].value") == arrprevanswers[qid]) {
                   	 eval("document.sample_form." + ele + "[" + j + "].checked=true");
                	}
            	}
        	}
    	}
}

function changeChecked(q_id, q_row_no, q_val) {
    var ans = 0;
    alen = eval("document.sample_form.answer" + q_id + ".length");
    for (pos = 0; pos < alen; pos++) {
        answervar = eval("document.sample_form.answer" + q_id + "[" + pos + "]");
        if (answervar.checked == true) {
            ans = answervar.value;
        }
    }	
    if (ans != "") {		
        document.sample_form.answer_ques.value = ans;
		ANSWERCHECKED=1;
    }
	document.sample_form.last_clicked_question.value = q_row_no;	
}

function eraseAnswer(q_id) {
 //   var q_id = arrQuestionid[document.getElementById("qindex").value];	
	var pos;
	var answervar;
    var len = eval("document.sample_form.answer" + q_id + ".length");
	var chk = 0;
	var chke = 0;
	for (pos = 0; pos < len; pos++) {
        answervar = eval("document.sample_form.answer" + q_id + "[" + pos + "]");
       	if (answervar.checked) {
           	chke = 1;
       	}
       	answervar.checked = false;
    }
    if (chke == 0) {
       	return;
    }					
	setAnswer(q_id,0);
	document.sample_form.answer_ques.value = 0;
	document.sample_form.last_clicked_question.value = document.getElementById("qindex").value;	
	ANSWERCHECKED=1;
}
function prev() {	   
	var h = document.getElementById("qindex");
    if(h.value <= 0) {
      v = parseInt(h.value) - 1
      swap_sheet(v);
    } else if (h.value == 1) {
        h.value = parseInt(h.value) - 1;
        swap_sheet(h.value);
    } else {
        h.value = parseInt(h.value) - 1;
        swap_sheet(h.value);
        return false;
    }
}
function next() {	
	var l = parseInt(document.getElementById("question_count").value) - 2;    
    var j = document.getElementById("qindex");
    if(j.value  > l) {
      v = parseInt(j.value) + 1;
      swap_sheet(v);
    }else if (j.value == l) {
        j.value = parseInt(j.value) + 1;
        swap_sheet(j.value);
    } else {
        j.value = parseInt(j.value) + 1;
        swap_sheet(j.value);
        return false;
    }
}
function preview() {  		
	var space1='';			
	var space2='';
		
	rowcount =getRowCount(1);
	var t = document.getElementById("ques_ID");
    var B = document.getElementById("answer_ques");
    var w = document.getElementById("question_paper_no");    
    var y = document.getElementById("question_count").value
	
	var v = 0;
    var D = "</span></a>";
    var H = "";
    var z = "";
    var G = 1;
    U = 0;
    var E = 0;
    UNcount = 0;
    tcount = 0;
    
	for (var C = 0; C < parseInt(document.getElementById("question_count").value); C++) {      
        var m = parseInt(C + 1);
        var qid = arrQuestionid[C];
        var I = "Q" + utildisp(parseInt(C) + 1);
        var J = '<a id="q_disp'+m+'" title="Click to view '+ I+'" class="attempt" href=javascript:swap_sheet(' + C + ") onClick=javascript:swap_sheet(" + C + ")><span>";
        var A = '<a id="q_disp'+m+'" title="Click to view '+ I+'" class="unattempt" href=javascript:swap_sheet(' + C + ") onClick=javascript:swap_sheet(" + C + ")><span>";
        var yt = '<a id="q_disp'+m+'" title="Click to view '+ I+'" class="qnodisp_tag" href=javascript:swap_sheet(' + C + ") onClick=javascript:swap_sheet(" + C + ")><span>";

        var marked_string = document.getElementById("marked_string").value;

        var mstring = marked_string.split(",") ;

        if(inArray(mstring,qid) == 1)
        {
            if (C > 99) {
                z += yt + I + D + space1 + space1 ;
            } else {
                z += yt + I + D + space1 + space1 + space1;
            }
            tcount++;
            U++;

        } else if (arrprevanswers[qid] == 0) {
            if(C > 99)
				z += A + I + D + space1 + space1 ;
			else
				z += A + I + D + space1 + space1 + space1;
    		
			 UNcount++;
			 U++;			
        } else {
             if(C > 99)
			 	z += J + I + D + space1+ space1;
			else
				z += J + I + D + space1 + space1 + space1;
		     
			 E++;
			 U++;
        }
		if(U == rowcount){
			z +="<br/>";
			U=0;
		}
	}
    document.getElementById("qnodetails").innerHTML = z;
    document.getElementById("no_attempt").innerHTML = E;
    document.getElementById("no_unattempt").innerHTML = UNcount;
    document.getElementById("no_tagged").innerHTML = tcount;
    UNANSWERCOUNT= UNcount;
    TAGGEDCOUNT = tcount;
}

function preview_submit(g) {       
  checkprevanswers();
  preview();
  var timeleft = document.getElementById("timeleft").value; 
   
  if(UNANSWERCOUNT > 0 || TAGGEDCOUNT > 0) {
	var unmsg="<h1>You have <span>"+timeleft+"</span> to answer <span>"+UNANSWERCOUNT+"</span>  un-answered question(s), <span>"+ TAGGEDCOUNT  +"</span> tagged question(s).</h1><h2>still do you want to proceed with your submit?</h2>";
  }
  else
	var unmsg='Do you want to proceed with your submit?';
			
   var m = document.getElementById("time_left").value;
   var c = document.getElementById("h_totalTime").value;
 
   if (g == 0) {    		
 		alertbox(2,unmsg,alertbox_callback);		
    } else {
        if (g == 1) {
            document.getElementById("auto_submit").value = "Y";
        }
	    document.sample_form.action = "online_exam.php";
	    document.sample_form.submit();
    }  
}
var preview_submit_callback=function(bool){		
	TINY.box.hide();
	if(bool){
		document.getElementById("auto_submit").value = "N";
		return true;
	}else{
		return false;	
	}
};

/* UTILITY FUNCTIONS */
function setAnswer(qid,answer)
{
	if(qid!='')
	{
		if(answer=='')
			answer=0;
		arrprevanswers[qid]=answer;
	}
}
function getAnswer(qid)
{
	if(qid!='')
	{
		return arrprevanswers[qid];
	}	
}

function checkprevanswers()
{			
	var q_id=document.getElementById("ques_ID").value;
	var ans= document.sample_form.answer_ques.value;	
    
	if(ans == "")
   	   ans=0;	
    if(ANSWERCHECKED==1)
	{		  
		if(arrprevanswers[q_id] == ans)  
		{
			//old and current choosed option same no need to raise ajax request
			document.sample_form.answer_ques.value='';
		}else
		{			
			arrprevanswers[q_id] = ans;
		}			
		ANSWERCHECKED=0;
	}	
}

function showdiv(b) {
    document.getElementById(b).style.visibility = "visible";
    document.getElementById(b).style.display = "block";
}
function hidediv(b) {
    document.getElementById(b).style.visibility = "hidden";
    document.getElementById(b).style.display = "none";
}
function preview_close() {
    hidediv("preview_div");
    showdiv("questionpaper_div");
}
function utildisp(b) {
    switch (b) {
    case 1:
        return "01";
        break;
    case 2:
        return "02";
        break;
    case 3:
        return "03";
        break;
    case 4:
        return "04";
        break;
    case 5:
        return "05";
        break;
    case 6:
        return "06";
        break;
    case 7:
        return "07";
        break;
    case 8:
        return "08";
        break;
    case 9:
        return "09";
        break;
    default:
        return b;
        break;
    }
}

function swap_sheet(g) { 	
	checkprevanswers();
	var f = document.getElementById("question_count").value;
    if(g == f){
       alertbox(1, "<h1>You are in last question,please use previous question button or preview panel to view other questions</h1>");
       return
    }
    if(g<0){
      alertbox(1, "<h1>You are in first question,please use next question button or preview panel to view other questions</h1>");
      return
    }
    document.getElementById("qindex").value = g;
    for (i = 0; i < f; i++) {
        var d = document.getElementById("q" + i);
        d.style.display = "none";
    }
    var d = document.getElementById("q" + g);
    d.style.display = "block";
    document.getElementById("ques_ID").value = arrQuestionid[g];
    document.getElementById("answer_ques").value = "";
    if (g == (f - 1)) {
        document.getElementById("Next").style.color="#7B7B7B";
        document.getElementById("Prev").style.color="#FFFFFF";
    } else {
        if (g == 0) {
            document.getElementById("Prev").style.color="#7B7B7B";
            document.getElementById("Next").style.color="#FFFFFF";
        } else {
            document.getElementById("Prev").style.color="#FFFFFF";
            document.getElementById("Next").style.color="#FFFFFF";
        }
    }
	/*if(getAnswer(arrQuestionid[g])!=0)
	  document.getElementById("EraseAnswer").disabled = false;
	else
	  document.getElementById("EraseAnswer").disabled = true;
	 */ 
	preview();
 	showdiv("preview_div");	
	showdiv("questionpaper_div");
}

/* Window load functions */
function callOnLoad() {
	InitializeTimer(document.getElementById("time_left").value, "sample_form", "sample_scores.php");
	swap_sheet(0);	
	window.onhelp = function () {
	   return false;
	};   
	getRowCount(0);
	setElementsCountByClassName();
}

/*Alert Content alert(type =1=1),confirm (type=2) */
var alertbox_callback=function(obj){				
	if(obj.id == 'alertboxok'){
		 document.getElementById("auto_submit").value = "N";							
		 TINY.box.hide();
		 document.sample_form.action = "sample_scores.php";
		 document.sample_form.submit();
		 return true;
	}else{		
		TINY.box.hide();
		return false;
	}	
}
/*Alert Content alert(type =1=1),confirm (type=2) */
function alertbox(type,msg,alertbox_callback)
{			
	if(type == 1)
	{
		var content='<div class="content_container_confi_wrapper"><div class="outerbox">'+
		'<div class="innerbox">'+
		'<div class="innerbox_container_confi_login">'+
		'<div class="pager_header_common">Confirmation</div>'+
		'<div class="content_container_confi_login">'+
		'<div id="confo_div">'+msg+
		'</div>'+
		'</div>'+
		'<div class="button_container_common"><input name="button" type="button" class="green_button" onclick="TINY.box.hide()" value="OK"/>'+
		'</div>'+
		'</div>'+
		'</div>'+
		'</div></div>';
	}else if(type == 2){
		var content='<div class="content_container_confi_wrapper"><div class="outerbox">'+
                '<div class="innerbox">'+
                '<div class="innerbox_container_confi_login">'+
                '<div class="pager_header_common">Confirmation</div>'+
                '<div class="content_container_confi_login">'+
                '<div id="confo_div">'+msg+
                '</div>'+
                '</div>'+
                '<div class="button_container_common"><input name="button" type="button" id="alertboxok"  class="green_button" onclick="alertbox_callback(this)" value="YES"/><input name="button" type="button" class="green_button" onclick="alertbox_callback(this)" value="NO"/>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div></div>';
	}
	TINY.box.show(content,0,0,0,0,0);
}

function getRowCount(type)
{
	var qpcount=document.getElementById("question_count").value;
	var subjsize=25;		
	var rowcount=5;	
	if(type==1)
		return rowcount;
	else
	  document.getElementById('subname').size=subjsize;	  	
}
function inArray(haystack, needle) {

// alert(needle+', '+haystack) ; return false ;

    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return 1;
    }
    return 0;
}
function tagQ(button_ctl, c)
{
        marked_string = document.getElementById("marked_string").value;
        if(button_ctl.value == " Tag ")
        {
                if(marked_string != '')
                        var marked_string = marked_string+','+c;
                else
                        var marked_string = c;

                        document.getElementById("marked_string").value = marked_string;
                        document.getElementById("mark_btn_"+c).value = " De-Tag " ;
                        document.getElementById("mark_btn_"+c).title = " Click to De-Tag the question " ;
                        document.getElementById("q_disp"+c).className= "qnodisp_tag";
        }
        else if(button_ctl.value == " De-Tag ")
        {
                var mstring = marked_string.split(",");

                var splittedValue = new Array();

                var j = 0 ;
                for(var i=0;i<mstring.length;i++)
                {
                        if(mstring[i] != c)
                        {
                                splittedValue[j] = mstring[i];
                                j++;
                        }
                }

                var reqValue = splittedValue.toString();
                document.getElementById("marked_string").value = reqValue;
                document.getElementById("mark_btn_"+c).value = " Tag " ;
                document.getElementById("mark_btn_"+c).title = " Click to Tag the question " ;
                if(arrprevanswers[c] == '')
                        document.getElementById("q_disp"+c).className= "unattempt";
                else
                        document.getElementById("q_disp"+c).className= "attempt";
        }
}
</script>
JSSC;

$bodyOnload = ' onLoad="javascript:callOnLoad();" onKeyDown="return doKeyDown(event);"  ondragstart="return Stop(event)" onselectstart="return Stop(event)" onmousedown="return Stop(event)" onmouseup="return Stop(event)"';
include("themes/default/header.php");
?>
<form name='sample_form' id='sample_form' action="" method="post">
<input type="hidden" name='time_to_submit' value='<?=$time_to_submit?>'>
<input type="hidden" id="memno" name="memno" value='<?=$membershipNo ?>'/>
<input type="hidden" id="time_left" name='time_left' value='<?=$time_to_submit ?>'/>
<input type="hidden" id="total_time_candi" name='total_time_candi' value='<?=$time_to_submit?>'/>
<input type="hidden" id="starttime" name='starttime' value='<?=$starttime ?>'/>	
<input type="hidden" id="h_totalTime" name="h_totalTime" value='<?=$time_to_submit?>'/>
<input type="hidden" id="h_startTime" name="h_startTime" value='<?=$starttimehrs?>'/>	
<input type="hidden" id="ques_ID" name='ques_ID' />
<input type="hidden" id="answer_ques"  name='answer_ques'/>
<input type="hidden" id="last_clicked_question" name="last_clicked_question" value="0"/>
<input type="hidden" id="auto_submit" name="auto_submit" />	
<input type='hidden' name='marked_string' id='marked_string' value='<?php echo htmlentities($marked_string);?>'>
    <!-- Top Container -->
  <div id="top-container-wrapper">
    <div id="top-container" class="container">
    	<h1>Sample Question Paper</h1>
        <!-- Candidate Details -->
        <div id="can-dtl">
          <span class="can-lable">Subject Name</span>
          <span>:</span>
          <span class="details"><input title="SAMPLE SUBJECT" size="30" type="text" name="subname" id="subname" class="bigtextbox" readonly="readonly" value="SAMPLE SUBJECT" /></span>
          <div class="clear"></div>
          <span class="can-lable">Membership No.</span>
          <span>:</span>
          <span class="details"><?PHP echo $membershipNo?></span>
          <div class="clear"></div>
          <span class="can-lable">Candidate Name</span>
          <span>:</span>
          <span class="details"><?PHP echo ucwords($_SESSION['memname']);?></span>
          <div class="clear"></div>
          <span class="can-lable">Time Left</span>
          <span>:</span>
          <span class="details"><input type="text" readonly="readonly" name="timeleft" id="timeleft" class="bigtextbox" value="" size="12" /></span>
          <div class="clear"></div>
          <span class="can-lable">Text Size</span>
          <span>:</span>
          <span class="details">
            <img style="cursor:hand" onclick="changeQPFontSize('1');" src="themes/default/images/Ap.jpg" title="Increase Font" width="23" height="22" />
            <img style="cursor:hand" onclick="changeQPFontSize('0');" src="themes/default/images/Am.jpg" title="Decrease Font" width="23" height="22" />
          </span>
          <div class="clear"></div>
        </div>
        <!-- Question Details -->
        <div class="no-que" id="preview_div">
          <!-- Photo & Signature -->
          <div id="phot-sign">
            <div id="photo">
              <img src="themes/default/images/photo_no.jpg" title="Photo" width="125" height="94" border="0" />
            </div>
            <div id="sign">
              <img src="themes/default/images/sign.jpg" title="Signature" width="125" height="40" border="0" />
            </div>
          </div>
          <!-- Questions -->
          <div id="que-att">
            <h1 style="padding-top:12px;">Number of Questions</h1>
            <div id="que-scroll">
          <div id="qnodetails"></div>
          </div>
          <span id="attempt"><b>Attempted</b><span class="N-attempt" id="no_attempt"></span></span>
          <span id="un-attempt"><b>Unattempted</b><span class="N-unattempt" id="no_unattempt"></span></span>
          <span id="no-tagged"><b>Tagged</b><span class="N-tagged" id="no_tagged"></span></span>
        </div>


    </div>  
  </div>
</div>
<div class="clear"></div>
   <div class="bottom-container-wrapper" id="questionpaper_div" style="position:relative;visibility:hidden;display:none;">
<?PHP 

$qn_no=0;		
for($k=0;$k<=4;$k++) {
	$questionID=$qid=$QUESTION_ARRAY[$k][$mediumcode]['question_id'];

	if ($mediumcode == 'EN')
		$questionText=$QUESTION_ARRAY[$k][$mediumcode]['question'];
	else
		$questionText="<font class='qn'>".$QUESTION_ARRAY[$k][$mediumcode]['question']."</font>";

	$opt1=$QUESTION_ARRAY[$k][$mediumcode]['option1'];
	$opt2=$QUESTION_ARRAY[$k][$mediumcode]['option2'];
	$opt3=$QUESTION_ARRAY[$k][$mediumcode]['option3'];
	$opt4=$QUESTION_ARRAY[$k][$mediumcode]['option4'];
	$opt5=$QUESTION_ARRAY[$k][$mediumcode]['option5'];
	$mark = $QUESTION_ARRAY[$k][$mediumcode]['mark'];

	if($qn_no==0)
		$str=$tmp.'<div class="bottom-container container" id="q'.$qn_no.'">'."\n";
	else
		$str.='<div class="bottom-container container" id="q'.$qn_no.'">'."\n";

        $str.='<h1>Section : Sample Question Title</h1>';

	$str.="<input type=hidden name=question_id[".$qn_no."] value='$questionID' />\n";
	$qno=$qn_no+1;

	if($_SESSION['display_mark']=='Y')
		$str.="<span id=\"mark\">Mark : <span>$mark</span></span>";

	$str.='<div id="samp_quest_div_container"><span id="samp_quest_div_quest_no">Q'.$qno.'. </span>'."\n";
	$str.='<div class="samp_quest_div_quest_text"><p class="greybluetextquestion">'.$questionText.'</p>'."\n";
	$dis_answer_order=array(1,2,3,4,5);

		$str.="<ul>\n";
	foreach($dis_answer_order as $key=> $i) {
		$var = "option".$i;
		$$var = $QUESTION_ARRAY[$k][$mediumcode][$var];

		if (trim($$var) != "") {
			$str.="<li class=\"greybluetextans\"><input type='radio' id='answer".$questionID."' name='answer".$questionID."' value='".$i."'onClick='javascript:changeChecked($questionID,$qn_no,0)'/>". $alpha[$key].") <label onClick='javascript:changeChecked($questionID,$qn_no,$i)' dir=ltr>";

			if ($mediumcode == 'EN')
				$str.=$$var;
			else
				$str.="<font class='ans'>".$$var."</font>";

			$str.="</label></li>\n";
		}
	}
		$str.="</ul>\n";

	if(@in_array($question_id,$marked_array))
		$btName = ' De-Tag ';
	else
		$btName = ' Tag ';

	$str.='<div class="clear"></div>'."\n";
	$str.="<div id=\"samp_quest_div_quest_text_btn_div\"><input title='Click to $btName the question' type='button' name='mark_btn_$questionID' id='mark_btn_$questionID' class='yellow_button' value='$btName' onClick='javascript: tagQ(this, $questionID);'><input title='Click to erase the answer' onclick=\"eraseAnswer($qno)\" class=\"red_button\" type=\"button\" value=\"Erase Answer\"></div>";
	$arrQuestionid[$qn_no]=$questionID;
	$qn_no++;
	$str.="</div>\n";
	$str.="</div>\n";
	$str.="</div>\n";
}
$str.="<input type=hidden name=question_count id=question_count value='5'>";
$str.='<script type="text/javascript">'."\n";
$str.=arrayToJSObject($arrQuestionid,'arrQuestionid'); 
$str.="</script>\n";				
echo $str;					 
echo '<script type="text/javascript">'."\n";
echo arrayToJSObject($arrprevAnswers,'arrprevanswers'); 
echo "</script>\n";
?>
       
   </div>
  <!-- Link Container-->
  <div class="clear"></div>
   <div id="link-con">
  	<div class="container"><div id="pre_btn_div"><div id="prev_arrow_div"><a href="#" onclick='javascript:prev()'><img src="images/prev_arrow.png" width="12" height="16" /></a></div><div id="prev_arrow_text"><a href="#" onclick='javascript:prev()' title="Click to view previous question" id="Prev">Previous Question</a></div></div>
    
    <div id="next_btn_div"><div id="next_arrow_text"><a title="Click to view next question" href="#" onclick="javascript:next()" id="Next">Next Question</a></div><div id="next_arrow_div"><a href="#" onclick='javascript:next()'><img src="images/next_arrow.png" width="12" height="16" /></a></div></div>
    
    <div id="final_submit_btn_div"><div id="final_submit_arrow_div"><a href="#" onclick="preview_submit(0)"><img src="images/final_submit_arow.png" width="16" height="13" /></a></div>
    <div id="final_submit_arrow_text"><a title="Click to submit Question Paper" href="#" onclick="preview_submit(0)">Preview Submit</a></div></div>
    <input type=hidden name="qindex" id="qindex" value='0' />
   </div>
  </div>

<!-- Timer Alert -->
<div id='styled_popup' style='cursor:hand;width: 350px; height: 78px; display:none; position: absolute; z-index: 100; top: -4px; left: -3px;' name='styled_popup'>
  <table width="100%" height="75" border="0" cellpadding="0" cellspacing="0" onclick='javascript:styledPopupClose()' >
    <tr>
      <td><table width='100%' height="78" border='0' cellpadding='5' cellspacing='1' class='Report_tb'>
    <tr><th colspan="10">Time Alert</th>  </tr>
      <tr class='tb_rw3'>
          <td colspan='2' align="center">5 Minutes left to auto submit your exam.<br/>
            <br/>
        <input name="button" type="button" class="green_button" onclick='javascript:styledPopupClose()' value=' Close ' />          </td>
        </tr>
      </table></td>
    </tr>
  </table>
</div>

<!-- Timer Alert -->
<?php
include "themes/default/footer.php";
?>
