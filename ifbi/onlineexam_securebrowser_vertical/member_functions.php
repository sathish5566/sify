<?php

function getIwayDetails($centreCode)
{
	global $SLAVECONN;
	global $dbsel;	
	$sqlIWay = "SELECT iway_address1, iway_address2, iway_city, iway_state, iway_pin_code FROM iib_iway_details ".
		" WHERE centre_code='$centreCode'";
	if ($commonDebug)
	{
		print("\n".$sqlIWay);
	}
	$resIWay = @mysql_query($sqlIWay,$SLAVECONN) or errorlog('err05'," iib_iway_details  ".mysql_error($SLAVECONN));
	$row = mysql_fetch_array($resIWay);
	$iwayAddress = "";
	$row['iway_address1'] != "" ? $iwayAddress .= " ".$row['iway_address1'] : $iwayAddress .= "";
	//$row['iway_address2'] != "" ? $iwayAddress .= " ".$row['iway_address2'] : $iwayAddress .= "";
	$row['iway_city'] != "" ? $iwayAddress .= " ".$row['iway_city'] : $iwayAddress .= "";
	$row['iway_pin_code'] != "" ? $iwayAddress .= " ".$row['iway_pin_code'] : $iwayAddress .= "";
	$row['iway_state'] != "" ? $iwayAddress .= " ".$row['iway_state'] : $iwayAddress .= "";
	$row['iway_address2'] != "" ? $iwayAddress .= ",Contact No. :  ".$row['iway_address2'] : $iwayAddress .= "";
	return $iwayAddress;
}

function getSubject($membershipNo,$examDate,$examTime,$centreCode)
{
	global $SLAVECONN;
	global $dbsel;
	
	$sql = "SELECT subject_code,exam_code FROM iib_candidate_iway WHERE membership_no='$membershipNo' AND exam_date='$examDate' AND exam_time='$examTime' AND centre_code='$centreCode' ";
	
	$res = mysql_query($sql,$SLAVECONN);
	$nRows = mysql_num_rows($res);
	if ($nRows == 0)
	{
		$subjectName = "";
	}
	else
	{
		list($subjectCode,$examCode) = mysql_fetch_row($res);
		$sql1 = "SELECT subject_name FROM iib_exam_subjects WHERE exam_code='$examCode' AND subject_code='$subjectCode' ";
		$res1 = mysql_query($sql1,$SLAVECONN);
		list($subjectName) = mysql_fetch_row($res1);
	}
	return $subjectName;
}

function getMemberDetails($membershipNo)
{
	global $SLAVECONN;
	global $dbsel;
	
	$sqlMember = "SELECT name, address1, address2, address3, address4, address5, address6, pin_code FROM iib_candidate WHERE ".
  		"membership_no='$membershipNo'";
	if ($commonDebug)
	{
		print("\n".$sqlMember);
	}
	
	$resMember = @mysql_query($sqlMember,$SLAVECONN) or errorlog('err05'," iib_candidate  ".mysql_error($SLAVECONN));
	$row = mysql_fetch_array($resMember);
	
	$memberName = $row['name'];
	$c_addr1 = $row['address1'];
	$c_addr2 = $row['address2'];
	$c_addr3 = $row['address3'];
	$c_addr4 = $row['address4'];
	$c_addr5 = $row['address5'];
	$c_addr6 = $row['address6'];
	$c_pin = $row['pin_code'];
	
	$memberAddress = "";
	if ($c_addr1 != "")
		$memberAddress .= $c_addr1;
	if ($memberAddress != "") 
		$memberAddress .= " ";
	if ($c_addr2 != "")
		$memberAddress .= $c_addr2;
	if (($memberAddress != "") && ($c_addr2 != ""))
		$memberAddress .= " ";
	if ($c_addr3 != "")
		$memberAddress .= $c_addr3;
	if (($memberAddress != "") && ($c_addr3 != ""))
		$memberAddress .= " ";
	if ($c_addr4 != "")
		$memberAddress .= $c_addr4;
	if (($memberAddress != "") && ($c_addr4 != ""))
		$memberAddress .= " ";
	if ($c_addr5 != "")
		$memberAddress .= $c_addr5;
	if (($memberAddress != "") && ($c_addr5 != ""))
		$memberAddress .= " ";
	if ($c_addr6 != "")
		$memberAddress .= $c_addr6;
	if (($memberAddress != "") && ($c_addr6 != ""))
		$memberAddress .= " ";	
	if ($c_pin != "")
		$memberAddress .= $c_pin;	
	$memberDetails[0] = $memberName;
	$memberDetails[1] = $memberAddress;	
	return $memberDetails;
}

function checkCandidateCentre($membershipNo,$examDate,$examTime,$centreCode)
{
	global $SLAVECONN;
	global $dbsel;
		
	$sql = "SELECT centre_code FROM iib_candidate_iway WHERE membership_no='$membershipNo' AND exam_date='$examDate' AND exam_time='$examTime' AND centre_code='$centreCode' ";
	$res = @mysql_query($sql,$SLAVECONN);
	$nRows = mysql_num_rows($res);
	if ($nRows > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function getCentre($taLogin)
{
	global $SLAVECONN;
	global $dbsel;
	
	$centreCode = "";
	$sql = "SELECT centre_code FROM iib_ta_iway WHERE ta_login='$taLogin' ";
	$res = mysql_query($sql,$SLAVECONN);
	$nRows = mysql_num_rows($res);
	if ($nRows > 0)
	{
		list($centreCode) = mysql_fetch_row($res);
	}
	return $centreCode;
}

function getExamCentre($centreCode)
{
	global $SLAVECONN;
	global $dbsel;
	
	$examCentreCode = "";
	$sql = "SELECT a.exam_centre_name FROM iib_exam_centres a, iib_iway_details b WHERE a.exam_centre_code=b.exam_centre_code AND b.centre_code='$centreCode' ";
	$res = mysql_query($sql,$SLAVECONN);
	$nRows = mysql_num_rows($res);
	if ($nRows > 0)
	{
		list($examCentreName) = mysql_fetch_row($res);
	}
	return $examCentreName;
}

function getSlots()
{
	global $SLAVECONN;
	global $dbsel;
	
	$aSlots = array();
	$sql = "SELECT slot_time FROM iib_exam_slots order by slot_time";
	$res = mysql_query($sql,$SLAVECONN);
	$nRows = mysql_num_rows($res);
	if ($nRows > 0)
	{
		$nCount = 0;
		while (list($slotTime) = mysql_fetch_row($res))
		{
			$aSlots[$nCount] = $slotTime;
			$nCount++;
		}
	}
	return $aSlots;
}
?>