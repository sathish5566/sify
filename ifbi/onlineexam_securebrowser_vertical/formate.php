<?php require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkTALogin();
require("member_functions.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?PHP echo TITLE?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/iibf.css" rel="stylesheet" type="text/css">
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<script language='JavaScript'>
function submitForm()
{
	document.formatfrm.submit();
}
</script>
<center>
<?php
	$taLogin = $_SESSION['login'];	
	$centreCode =  getCentre($taLogin);
	$examDate = date("Y-m-d");
	$dispDate = date("d-m-Y");
	$formatType='E';
	$iwayAddress = getIwayDetails($centreCode);
	$examCentreName =getExamCentre($centreCode);
	$hidExamDate = $_POST['exam_date'];
	if ($hidExamDate != "")
	{
		$sqlSelect = "SELECT id FROM iib_format WHERE format_type='$formatType' AND ta_login='$taLogin' AND centre_code='$centreCode' ";
		$resSelect = mysql_query($sqlSelect,$SLAVECONN);
		$nRows = mysql_num_rows($resSelect);
		if ($nRows > 0)
		{
			list($id) = mysql_fetch_row($resSelect);
			$msg = "Format E details already confirmed";
		}
		else
		{
			$sqlFormat = "INSERT INTO iib_format (ta_login,centre_code,exam_date,format_type) ".
				" VALUES ('$taLogin','$centreCode','$examDate','$formatType')";
			mysql_query($sqlFormat,$MASTERCONN);
			$msg = "Format E details confirmed";
		}		
	}

require("topnav.php");
require("formmenu.php");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0" > 
  <tr> 
    <td width="100%" background="images/tile.jpg" >
    <form name='formatfrm' method='post'>
      <p>
  <input type='hidden' name='ta_login' value='<?=$taLogin ?>'>
  <input type='hidden' name='exam_date' value='<?=$examDate ?>'>
</p>
      <p>&nbsp;      </p>
      <table width="65%" border="0" align="center" cellpadding="1" cellspacing="0">
  <tr>
    <td bgcolor="#014A7F"><table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
      <tr>
        <td colspan="2" align="center" bgcolor="#E8EFF7" class="greybluetext10"><b><font color='#000000'>Format E</font></b></td>
      </tr>
      <tr>
        <td colspan="2" bgcolor="#E8EFF7" class="greybluetext10"><b>CERTIFICATE</b></td>
      </tr>
      <tr>
        <td width="40%" align="left" valign="middle" bgcolor="#E8EFF7" class="greybluetext10"><b>Name of the centre</b></td>
        <td align="left" valign="middle" bgcolor="#E8EFF7" class="greybluetext10"><?=$centreCode ?></td>
      </tr>
      <tr>
        <td width="40%" align="left" valign="middle" bgcolor="#E8EFF7" class="greybluetext10"><b>Iway Address</b></td>
        <td align="left" valign="middle" bgcolor="#E8EFF7" class="greybluetext10"><?=$iwayAddress?></td>
      </tr>
      <tr>
        <td colspan="2" bgcolor="#E8EFF7"><br /></td>
      </tr>
    </table>
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" bgcolor="#E8EFF7">
          <tr>
            <td colspan="2" class="errormsg" align="center"><?=$msg ?></td>
          </tr>
          <tr>
            <td colspan="2" align="left" valign="middle" class="greybluetext10"><b>Dear Sir,</b></td>
          </tr>
          <tr>
            <td class="greybluetext10" width="20%"><br /></td>
            <td align="left" valign="middle" class="greybluetext10"><b>RE: JAIIB Examination Nov / Dec 2004.</b></td>
          </tr>
          <tr>
            <td colspan="2" align="left" valign="middle" class="greybluetext10"><b>We confirm that the above examination was conducted strictly in accordance with your instructions</b></td>
          </tr>
          <tr>
            <td colspan="2"><br /></td>
          </tr>
          <tr>
            <td width="20%" align="left" valign="middle" class="greybluetext10"><b>Date :</b></td>
            <td align="left" valign="middle" class="greybluetext10"><?=$dispDate ?></td>
          </tr>
          <tr>
            <td width="20%" align="left" valign="middle" class="greybluetext10"><b>Place :</b></td>
            <td align="left" valign="middle" class="greybluetext10"><?=$examCentreName ?></td>
          </tr>
          <tr>
            <td colspan="2" align="center"><input type="button" name='sub' value='Confirm' class="button" onclick='javascript:submitForm()' /></td>
          </tr>
      </table></td>
  </tr>
</table>
    </form>	  
      </td>
  </tr>
  <tr>
  	<td background="images/tile.jpg"><br><br></td>
  </tr>
</table>
<?php
require("bottomnav.php");
//mysql_close($MASTERCONN);
//mysql_close($SLAVECONN);
?>
</center>
</body>
</html>
