<?php
/****************************************************
* Application Name            :  IIB 
* Module Name                 :  Exam Module
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  -
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              : 
* Created ON                  :  
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  22-01-2010
* Description                 :  Interface for the Sample Exam page
*****************************************************/
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkTALogin();
checkCandidateLogin();
require_once("constants.inc");
 

$time_to_submit = getVal($_REQUEST['time_to_submit']);
$qid = isset($_GET["qid"])?$_GET["qid"]:0;
$qindex = $_POST['qindex'];

$membershipNo = $_SESSION['memno'];
$subjectcode = $_SESSION['sc'];
$examcode = $_SESSION['ex'];		
$mediumcode = $_SESSION['mc'];	
$exam_view = $_SESSION['exam_view'];	
$starttime = date('Y-m-d H:i:s');
$starttimehrs =  substr($starttime,10,strlen($starttime));

$alpha = array('1', '2', '3', '4', '5');
$arrprevAnswers = array('1'=>0, '2'=>0, '3'=>0, '4'=>0, '5'=>0);
/*need to comment if hindi uploaded in unicode */
$mediumcode='EN';
//if($mediumcode !='EN')
//	$mediumcode='HIDV';

$marked_string = $_REQUEST['marked_string'];
	
$includeStyleHD = '<style>
.qnodisp_tag { Background-color:#FF8000;font-family="Verdana,Arial,Helvetica,sans-serif";font-weight:bold;font-size:10px;}
</style>';

if( ($mediumcode == "HISU") || ($mediumcode == "HISH") || ($mediumcode == "HIDV") ) {
	$metaTagHD = '<meta http-equiv="content-type" content="text/html; charset=x-user-defined">';
}else {
	$metaTagHD = '<meta http-equiv="content-type" content="text/html; charset=utf-8">';
}

if ($mediumcode == "HISU"){
	$includeStyleHD .= <<<CSSHD
<STYLE TYPE="text/css">
  @font-face {
    font-family: Suchi-Dev-0714W;
    font-style:  normal;
    font-weight: normal;
    src: url(./eot/SUCHIDE0.eot);
  }
.qn{font-family:'Suchi-Dev-0714W';font-weight:bold;font-size:18px}
.ans{font-family:'Suchi-Dev-0714W';font-weight:normal;font-size:18px}
  
</STYLE>
CSSHD;
}else if ($mediumcode == "HISH"){
	$includeStyleHD .= <<<CSSHD
<STYLE TYPE="text/css">
  @font-face {
    font-family: Shree-Dev-0714EW;
    font-style:  normal;
    font-weight: normal;
    src: url(./eot/SHREEDE0.eot);
  }
.qn{font-family:'Shree-Dev-0714EW';font-weight:bold;font-size:18px}
.ans{font-family:'Shree-Dev-0714EW';font-weight:normal;font-size:18px}  
</STYLE>
CSSHD;
}else if ($mediumcode == "HIDV"){
	$includeStyleHD .= <<<CSSHD
<STYLE TYPE="text/css">
 @font-face {
	font-family: DVBW-TTYogeshEN;
	font-style:  normal;
	font-weight: normal;
	src: url(./eot/DVBWTTY0.eot);
 }
 .qn{font-family:'DVBW-TTYogeshEN';font-weight:bold;font-size:18px}
 .ans{font-family:'DVBW-TTYogeshEN';font-weight:normal;font-size:18px}
</STYLE>
CSSHD;
}
$includeStyleHD .= <<<CSSHD
<style type="text/css">
<!--
.style2 {
	color: #FFFFFF;
	font-weight: bold;
}
.style3 {color: #FFFFFF}
.style4 {
	font-size: 14px;
	font-weight: bold;
}
.greybluetextquestion{font-size: 16px;line-height:100%;}
.greybluetextans{font-size: 14px;line-height:100%;}
-->
</style>
CSSHD;
$includeJS = <<<JSSC
<script language="JavaScript" type="text/javascript" src="./includes/tinybox.js"></script>
<script language="javascript" type="text/javascript" src="./includes/browser.js"></script>
<script>
var secs;
var timerID = null;
var timerRunning = false;
var delay = 1000;
var timecheck = 600;
var formname;
var formaction;

function InitializeTimer(c, b, a) {
    secs = c;
    StopTheClock();
    formname = b;
    formaction = a;
    timerID = setInterval("displayTimer()", 1000);
}
function StopTheClock() {
    if (timerRunning) {
        clearInterval(timerID);
    }
    timerRunning = false;
}

function displayTimer() {	
    if (secs == 300) {
		fireMyPopup();
    }
    if (secs <= 0) {
        StopTheClock();
        eval("document." + formname + ".time_left.value = 0");
        if (formname == "sample_form") {
            preview_submit(1);
        } else {
            eval("document." + formname + ".action='" + formaction + "'");
            eval("document." + formname + ".submit()");
        }
    } else {
        var min = Math.floor(secs / 60);
        var remaining_secs = secs % 60;
        var texthr = Math.floor(min / 60);
        var textmin = new String(min % 60);
        if (textmin.length == 1) {
            textmin = "0" + textmin;
        }
        var textsec = new String(remaining_secs);
        if (textsec.length == 1) {
            textsec = "0" + textsec;
        }
        self.status = "TIME LEFT : " + texthr + " Hr(s) " + textmin + " Min(s) " + remaining_secs + " Secs ";
        eval("self.document.getElementById('timeleft').innerHTML ='" + texthr + ":" + textmin + ":" + textsec + " hrs'");
        eval("self.document." + formname + ".time_left.value = " + secs);
        var temp = secs;
        secs = secs - 1;
        timerRunning = true;      
    }
};

var bottom = 0;
var padding = 5;
var X = 5;
var Y = 0;

function dropMyPopup() {
  Y++;
  if( Y > bottom ) return;
  document.getElementById("styled_popup").style.top = Y + "px";
  setTimeout("dropMyPopup()", 25);
}

function fireMyPopup() {
  var scrolledY;
  if( self.pageYOffset ) {
    scrolledY = self.pageYOffset;
  } else if( document.documentElement && document.documentElement.scrollTop ) {
    scrolledY = document.documentElement.scrollTop;
  } else if( document.body ) {
    scrolledY = document.body.scrollTop;
  }

  var centerY;
  if( self.innerHeight ) {
    centerY = self.innerHeight;
  } else if( document.documentElement && document.documentElement.clientHeight ) {
    centerY = document.documentElement.clientHeight;
  } else if( document.body ) {
    centerY = document.body.clientHeight;
  }

  // Don't forget to subtract popup's height! ( 300 in our case )
  bottom = scrolledY + centerY - padding - 110;
  Y = scrolledY;

  document.getElementById("styled_popup").style.right = X + "px";
  document.getElementById("styled_popup").style.display = "block";
  dropMyPopup();
  setTimeout("styledPopupClose()",60000)
}

function styledPopupClose() {
  document.getElementById("styled_popup").style.display = "none";
  Y = bottom; // if it was closed, make sure extra computations are done in dropMyPopup()
}

var UNANSWERCOUNT=0;
var ANSWERCHECKED=0;
var TAGGEDCOUNT = 0;
/* QUESTION / ANSWER AMNIPULATION FUNCTIONS */
/* ANSWER  OPTION SELECTION ONLCLICK FUNCTION */
function markAnswer() {    	
		for (var i = 0; i < parseInt(document.getElementById("question_count").value); i++) {
	        var qid = arrQuestionid[i];        
			if (arrprevanswers[qid] != 0 && arrprevanswers[qid]!= null) {
            	var ele = "answer" + qid;
            	for (var j = 0; j < eval("document.sample_form." + ele + ".length"); j++) {
                	if (eval("document.sample_form." + ele + "[" + j + "].value") == arrprevanswers[qid]) {
                   	 eval("document.sample_form." + ele + "[" + j + "].checked=true");
                	}
            	}
        	}
    	}
}

function changeChecked(q_id, q_row_no, q_val) {
    var ans = 0;
    alen = eval("document.sample_form.answer" + q_id + ".length");
    for (pos = 0; pos < alen; pos++) {
        answervar = eval("document.sample_form.answer" + q_id + "[" + pos + "]");
        if (answervar.checked == true) {
            ans = answervar.value;
        }
    }	
    if (ans != "") {		
        document.sample_form.answer_ques.value = ans;
		ANSWERCHECKED=1;
    }
	document.sample_form.last_clicked_question.value = q_row_no;	
}

function eraseAnswer() {
	var q_id = arrQuestionid[document.getElementById("qindex").value];	
	var pos;
	var answervar;
	var len = eval("document.sample_form.answer" + q_id + ".length");
	var chk = 0;
	var chke = 0;
	for (pos = 0; pos < len; pos++) {
        answervar = eval("document.sample_form.answer" + q_id + "[" + pos + "]");
       	if (answervar.checked) {
           	chke = 1;
       	}
       	answervar.checked = false;
    }
    if (chke == 0) {
       	return;
    }					
	setAnswer(q_id,0);
	document.sample_form.answer_ques.value = 0;
	document.sample_form.last_clicked_question.value = document.getElementById("qindex").value;	
	ANSWERCHECKED=1;
}
function prev() {	   
	var h = document.getElementById("qindex");
    if(h.value <= 0) {
      v = parseInt(h.value) - 1
      swap_sheet(v);
    } else if (h.value == 1) {
        h.value = parseInt(h.value) - 1;
        swap_sheet(h.value);
    } else {
        h.value = parseInt(h.value) - 1;
        swap_sheet(h.value);
        return false;
    }
}
function next() {	
	var l = parseInt(document.getElementById("question_count").value) - 2;    
    var j = document.getElementById("qindex");
    if(j.value  > l) {
      v = parseInt(j.value) + 1;
      swap_sheet(v);
    }else if (j.value == l) {
        j.value = parseInt(j.value) + 1;
        swap_sheet(j.value);
    } else {
        j.value = parseInt(j.value) + 1;
        swap_sheet(j.value);
        return false;
    }
}
function preview() {  		
	var space1='';			
	var space2='';
		
	rowcount =getRowCount(1);
	var t = document.getElementById("ques_ID");
    var B = document.getElementById("answer_ques");
    var w = document.getElementById("question_paper_no");    
    var y = document.getElementById("question_count").value
	
	var v = 0;
    var D = "</span></a>";
    var H = "";
    var z = "";
    var G = 1;
    U = 0;
    var E = 0;
    UNcount = 0;
    tcount = 0;
    
	for (var C = 0; C < parseInt(document.getElementById("question_count").value); C++) {      
        var m = parseInt(C + 1);
        var qid = arrQuestionid[C];
        var I = "Q" + utildisp(parseInt(C) + 1);
        var J = '<a id="q_disp'+m+'" title="Click to view '+ I+'" class="attempt" href=javascript:swap_sheet(' + C + ") onClick=javascript:swap_sheet(" + C + ")><span>";
        var A = '<a id="q_disp'+m+'" title="Click to view '+ I+'" class="unattempt" href=javascript:swap_sheet(' + C + ") onClick=javascript:swap_sheet(" + C + ")><span>";
        var yt = '<a id="q_disp'+m+'" title="Click to view '+ I+'" class="qnodisp_tag" href=javascript:swap_sheet(' + C + ") onClick=javascript:swap_sheet(" + C + ")><span>";

        var marked_string = document.getElementById("marked_string").value;

        var mstring = marked_string.split(",") ;

        if(inArray(mstring,qid) == 1)
        {
            z += yt + I + D ;
            tcount++;
            U++;

        } else if (arrprevanswers[qid] == 0) {
	    z += A + I + D;
	    UNcount++;
	    U++;
        } else {
	    z += J + I + D;
	    E++;
	    U++;
        }
		if(U == rowcount){
			U=0;
		}
	}
    document.getElementById("qnodetails").innerHTML = z;
    document.getElementById("no_attempt").innerHTML = E;
    document.getElementById("no_unattempt").innerHTML = UNcount;
    document.getElementById("no_tagged").innerHTML = tcount;
    UNANSWERCOUNT= UNcount;
    TAGGEDCOUNT = tcount;
}

function preview_submit(g) {       
  checkprevanswers();
  preview();
//  var timeleft = document.getElementById("timeleft").value; 
  var timeleft = document.getElementById("timeleft").innerHTML; 
   
  if(UNANSWERCOUNT > 0 || TAGGEDCOUNT > 0) {
	var unmsg="<h1>You have <span>"+timeleft+"</span> to answer <span>"+UNANSWERCOUNT+"</span>  un-answered question(s), <span>"+ TAGGEDCOUNT  +"</span> tagged question(s).</h1><h2>still do you want to proceed with your submit?</h2>";
  }
  else
	var unmsg='Do you want to proceed with your submit?';
			
   var m = document.getElementById("time_left").value;
   var c = document.getElementById("h_totalTime").value;
 
   if (g == 0) {    		
 		alertbox(2,unmsg,alertbox_callback);		
    } else {
        if (g == 1) {
            document.getElementById("auto_submit").value = "Y";
        }

	if(document.getElementById("exam_view").value == "V"){ 
	    document.sample_form.action = "online_exam_ver.php";
	}else{ 
	    document.sample_form.action = "online_exam.php";
	} 

	    document.sample_form.submit();
    }  
}
var preview_submit_callback=function(bool){		
	TINY.box.hide();
	if(bool){
		document.getElementById("auto_submit").value = "N";
		return true;
	}else{
		return false;	
	}
};

/* UTILITY FUNCTIONS */
function setAnswer(qid,answer)
{
	if(qid!='')
	{
		if(answer=='')
			answer=0;
		arrprevanswers[qid]=answer;
	}
}
function getAnswer(qid)
{
	if(qid!='')
	{
		return arrprevanswers[qid];
	}	
}

function checkprevanswers()
{			
	var q_id=document.getElementById("ques_ID").value;
	var ans= document.sample_form.answer_ques.value;	
    
	if(ans == "")
   	   ans=0;	
    if(ANSWERCHECKED==1)
	{		  
		if(arrprevanswers[q_id] == ans)  
		{
			//old and current choosed option same no need to raise ajax request
			document.sample_form.answer_ques.value='';
		}else
		{			
			arrprevanswers[q_id] = ans;
		}			
		ANSWERCHECKED=0;
	}	
}

function showdiv(b) {
    document.getElementById(b).style.visibility = "visible";
    document.getElementById(b).style.display = "block";
}
function hidediv(b) {
    document.getElementById(b).style.visibility = "hidden";
    document.getElementById(b).style.display = "none";
}
function preview_close() {
    hidediv("preview_div");
    showdiv("questionpaper_div");
}
function utildisp(b) {
    switch (b) {
    case 1:
        return "01";
        break;
    case 2:
        return "02";
        break;
    case 3:
        return "03";
        break;
    case 4:
        return "04";
        break;
    case 5:
        return "05";
        break;
    case 6:
        return "06";
        break;
    case 7:
        return "07";
        break;
    case 8:
        return "08";
        break;
    case 9:
        return "09";
        break;
    default:
        return b;
        break;
    }
}

function swap_sheet(g) { 	
	checkprevanswers();
	var f = document.getElementById("question_count").value;
    if(g == f){
       alertbox(1, "<h1>You are in last question,please use previous question button or preview panel to view other questions</h1>");
       return
    }
    if(g<0){
      alertbox(1, "<h1>You are in first question,please use next question button or preview panel to view other questions</h1>");
      return
    }
    document.getElementById("qindex").value = g;
    for (i = 0; i < f; i++) {
        var d = document.getElementById("q" + i);
        d.style.display = "none";
    }
    var d = document.getElementById("q" + g);
    d.style.display = "block";
    document.getElementById("ques_ID").value = arrQuestionid[g];
    document.getElementById("answer_ques").value = "";

    var marked_string = document.getElementById("marked_string").value;
    var mstring = marked_string.split(",") ;
    var button_ctl = document.getElementById("mark_btn");
    if(inArray(mstring,arrQuestionid[g]) == 1) {
        button_ctl.value = " De-Tag " ;
        button_ctl.title = " Click to De-Tag the question " ;
    }else {
        button_ctl.value = " Tag " ;
        button_ctl.title = " Click to Tag the question " ;
    }

    if (g == (f - 1)) {
        document.getElementById("Next").style.color="#7B7B7B";
        document.getElementById("Prev").style.color="#FFFFFF";
    } else {
        if (g == 0) {
            document.getElementById("Prev").style.color="#7B7B7B";
            document.getElementById("Next").style.color="#FFFFFF";
        } else {
            document.getElementById("Prev").style.color="#FFFFFF";
            document.getElementById("Next").style.color="#FFFFFF";
        }
    }
	/*if(getAnswer(arrQuestionid[g])!=0)
	  document.getElementById("EraseAnswer").disabled = false;
	else
	  document.getElementById("EraseAnswer").disabled = true;
	 */ 
	preview();
 	showdiv("preview_div");	
	showdiv("questionpaper_div");
}

function Preload() {
    var a = Preload.arguments;
    document.imageArray = new Array(a.length);
    for (var b = 0; b < a.length; b++) {
        document.imageArray[b] = new Image;
        document.imageArray[b].src = a[b]
    }
}
/* Window load functions */
function callOnLoad() {
	InitializeTimer(document.getElementById("time_left").value, "sample_form", "sample_scores.php");
	swap_sheet(0);
	window.onhelp = function () {
	   return false;
	};   
	    Preload("./images/black.gif", "./images/blue.gif", "./images/white.png","./themes/default/images/header_bg.jpg","./themes/default/images/sify_logo.png","./themes/default/images/sample_question_header_bg.jpg","./themes/default/images/A-.png","./themes/default/images/A+.png","./themes/default/images/sample_question_left_sec_body_bg.jpg","./themes/default/images/photo_no.jpg","./themes/default/images/sign.jpg","./themes/default/images/bt-link-bg.jpg","./images/prev_arrow.png","./themes/default/images/prev_btn_bg.jpg","./images/next_arrow.png","./images/final_submit_arow.png","./themes/default/images/final_submit_bg.jpg","./themes/default/images/btn_bg.jpg","./themes/default/images/footer_bg.jpg");
	getRowCount(0);
	setElementsCountByClassName();
}

/*Alert Content alert(type =1=1),confirm (type=2) */
var alertbox_callback=function(obj){				
	if(obj.id == 'alertboxok'){
		 document.getElementById("auto_submit").value = "N";							
		 TINY.box.hide();
		 document.sample_form.action = "sample_scores.php";
		 document.sample_form.submit();
		 return true;
	}else{		
		TINY.box.hide();
		return false;
	}	
}
/*Alert Content alert(type =1=1),confirm (type=2) */
function alertbox(type,msg,alertbox_callback)
{			
	if(type == 1)
	{
		var content='<div class="content_container_confi_wrapper"><div class="outerbox">'+
		'<div class="innerbox">'+
		'<div class="innerbox_container_confi_login">'+
		'<div class="pager_header_common">Confirmation</div>'+
		'<div class="content_container_confi_login">'+
		'<div id="confo_div">'+msg+
		'</div>'+
		'</div>'+
		'<div class="button_container_common"><input name="button" type="button" class="green_button" onclick="TINY.box.hide()" value="OK"/>'+
		'</div>'+
		'</div>'+
		'</div>'+
		'</div></div>';
	}else if(type == 2){
		var content='<div class="content_container_confi_wrapper"><div class="outerbox">'+
                '<div class="innerbox">'+
                '<div class="innerbox_container_confi_login">'+
                '<div class="pager_header_common">Confirmation</div>'+
                '<div class="content_container_confi_login">'+
                '<div id="confo_div">'+msg+
                '</div>'+
                '</div>'+
                '<div class="button_container_common"><input name="button" type="button" id="alertboxok"  class="green_button" onclick="alertbox_callback(this)" value="YES"/><input name="button" type="button" class="green_button" onclick="alertbox_callback(this)" value="NO"/>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div></div>';
	}
	TINY.box.show(content,0,0,0,0,0);
}

function getRowCount(type)
{
	var qpcount=document.getElementById("question_count").value;
	var subjsize=25;		
	var rowcount=5;	
	if(type==1)
		return rowcount;
//	else
//	  document.getElementById('subname').size=subjsize;	  	
}
function inArray(haystack, needle) {

// alert(needle+', '+haystack) ; return false ;

    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return 1;
    }
    return 0;
}
function tagQ(button_ctl)
{
	var c=arrQuestionid[document.getElementById("qindex").value];
        c = parseInt(c);
        marked_string = document.getElementById("marked_string").value;
        if(button_ctl.value == " Tag ")
        {
                if(marked_string != '')
                        var marked_string = marked_string+','+c;
                else
                        var marked_string = c;

                        document.getElementById("marked_string").value = marked_string;
			tagByQuesId[c] = " De-Tag ";
                        button_ctl.value = " De-Tag " ;
                        button_ctl.title = " Click to De-Tag the question " ;
                        document.getElementById("q_disp"+c).className= "qnodisp_tag";
        }
        else if(button_ctl.value == " De-Tag ")
        {
                var mstring = marked_string.split(",");

                var splittedValue = new Array();

                var j = 0 ;
                for(var i=0;i<mstring.length;i++)
                {
                        if(mstring[i] != c)
                        {
                                splittedValue[j] = mstring[i];
                                j++;
                        }
                }

                var reqValue = splittedValue.toString();
                document.getElementById("marked_string").value = reqValue;
		tagByQuesId[c] = " Tag ";
                button_ctl.value = " Tag " ;
                button_ctl.title = " Click to Tag the question " ;
                if(arrprevanswers[c] == '')
                        document.getElementById("q_disp"+c).className= "unattempt";
                else
                        document.getElementById("q_disp"+c).className= "attempt";
        }
}
</script>
JSSC;

$bodyOnload = ' onLoad="javascript:callOnLoad();" onKeyDown="return doKeyDown(event);"  ondragstart="return Stop(event)" onselectstart="return Stop(event)" onmousedown="return Stop(event)" onmouseup="return Stop(event)"';
include("themes/default/header1.php");
?>
<form name='sample_form' id='sample_form' action="" method="post">
<input type="hidden" name='time_to_submit' value='<?=$time_to_submit?>'>
<input type="hidden" id="memno" name="memno" value='<?=$membershipNo ?>'/>
<input type="hidden" id="time_left" name='time_left' value='<?=$time_to_submit ?>'/>
<input type="hidden" id="total_time_candi" name='total_time_candi' value='<?=$time_to_submit?>'/>
<input type="hidden" id="starttime" name='starttime' value='<?=$starttime ?>'/>	
<input type="hidden" id="h_totalTime" name="h_totalTime" value='<?=$time_to_submit?>'/>
<input type="hidden" id="h_startTime" name="h_startTime" value='<?=$starttimehrs?>'/>	
<input type="hidden" id="ques_ID" name='ques_ID' />
<input type="hidden" id="answer_ques"  name='answer_ques'/>
<input type="hidden" id="last_clicked_question" name="last_clicked_question" value="0"/>
<input type="hidden" id="auto_submit" name="auto_submit" />	
<input type='hidden' name='marked_string' id='marked_string' value='<?php echo htmlentities($marked_string);?>'>
<input type='hidden' name='exam_view' id='exam_view' value='<?php echo $exam_view;?>'>


    <!-- Top Container -->
    <div id="top-container-wrapper">
      <div id="top-container" class="container">
        <div class="text_bold">SAMPLE SUBJECT</div>
        <div class="sam_quest_ti_lt_container">
<?php /*        <input type="hidden" readonly="readonly" name="timeleft" id="timeleft" class="bigtextbox" value="" size="12" />*/ ?>
        <div class="sam_quest_ti_lt_hrs" id="timeleft"></div>
          <div class="sam_quest_ti_lt_txt">Time Left</div>
        </div>
        <div class="sam_quest_pap_container">
          <div class="sam_quest_left_container">
            <div class="sam_quest_heading_bg">
              <h1>Text Size</h1>
              <div class="sam_quest_heading_ft_div">
                <a href="#"><img style="cursor:hand" onclick="changeQPFontSize('0');" src="themes/default/images/A-.png" title="Decrease Font" width="26" height="26" /></a>&nbsp;&nbsp;
                <a href="#"><img style="cursor:hand" onclick="changeQPFontSize('1');" src="themes/default/images/A+.png" title="Increase Font" width="26" height="26" /></a>
              </div>
<?php /*              <div class="sam_quest_mark_div">Marks&nbsp;:<span>&nbsp;60</span></div>*/ ?>
            </div>
            <div class="sam_quest_text_container" id="questionpaper_div" style="position:relative;visibility:hidden;display:none;">
<?PHP 

$qn_no=0;		
for($k=0;$k<=4;$k++) {
	$questionID=$qid=$QUESTION_ARRAY[$k][$mediumcode]['question_id'];

	if ($mediumcode == 'EN')
		$questionText=$QUESTION_ARRAY[$k][$mediumcode]['question'];
	else
		$questionText="<font class='qn'>".$QUESTION_ARRAY[$k][$mediumcode]['question']."</font>";

	$opt1=$QUESTION_ARRAY[$k][$mediumcode]['option1'];
	$opt2=$QUESTION_ARRAY[$k][$mediumcode]['option2'];
	$opt3=$QUESTION_ARRAY[$k][$mediumcode]['option3'];
	$opt4=$QUESTION_ARRAY[$k][$mediumcode]['option4'];
	$opt5=$QUESTION_ARRAY[$k][$mediumcode]['option5'];
	$mark = $QUESTION_ARRAY[$k][$mediumcode]['mark'];

	if($qn_no==0)
		$str=$tmp.'<div class="bottom-container" id="q'.$qn_no.'">'."\n";
	else
		$str.='<div class="bottom-container" id="q'.$qn_no.'">'."\n";

        $str.='<h1><b>Section : Sample Question Title</b></h1>';

	$str.="<input type=hidden name=question_id[".$qn_no."] value='$questionID' />\n";
	$qno=$qn_no+1;

	if($_SESSION['display_mark']=='Y')
		$str.="<span id=\"mark\">Mark : <span>$mark</span></span>";

	$str.='<div class="sam_quest_text_inner_container"><div id="samp_quest_div_quest_no">Q'.$qno.'. </div>'."\n";
	$str.='<div id="samp_quest_div_quest_text" class="sam_quest_wid" ><p class="greybluetextquestion">'.$questionText.'</p></div>'."\n";
	$dis_answer_order=array(1,2,3,4,5);

		$str.="<div class=\"sam_ans_text_container\"><ul>\n";
	foreach($dis_answer_order as $key=> $i) {
		$var = "option".$i;
		$$var = $QUESTION_ARRAY[$k][$mediumcode][$var];

		if (trim($$var) != "") {
			$str.="<li class=\"greybluetextans\"><input type='radio' id='answer".$questionID."' name='answer".$questionID."' value='".$i."' onClick='javascript:changeChecked($questionID,$qn_no,0)' />". $alpha[$key].") <label onClick='javascript:changeChecked($questionID,$qn_no,$i)' dir=ltr>";

			if ($mediumcode == 'EN')
				$str.=$$var;
			else
				$str.="<font class='ans'>".$$var."</font>";

			$str.="</label></li>\n";
		}
	}
		$str.="</ul>\n</div>\n";

	if(@in_array($question_id,$marked_array))
		$tagByQuesId[$qn_no] = ' De-Tag ';
	else
		$tagByQuesId[$qn_no] = ' Tag ';

	$arrQuestionid[$qn_no]=$questionID;
	$qn_no++;
	$str.="</div>\n";
	$str.="</div>\n";
}
$str.="<input type=hidden name=question_count id=question_count value='5'>";
$str.='<script type="text/javascript">'."\n";
$str.=arrayToJSObject($arrQuestionid,'arrQuestionid'); 
$str.="</script>\n";
echo $str;					 
echo '<script type="text/javascript">'."\n";
echo arrayToJSObject($arrprevAnswers,'arrprevanswers'); 
echo "</script>\n";
echo '<script type="text/javascript">'."\n";
echo arrayToJSObject($tagByQuesId,'tagByQuesId');
echo "</script>\n";
?>
            </div>
          </div>
          <div class="sam_quest_right_top_container">
            <div class="sam_quest_photo"><img src="themes/default/images/photo_no.jpg" width="150" height="150" /></div>
            <div class="sam_quest_sig"><img src="themes/default/images/sign.jpg" width="150" height="44" /></div>
            <div class="sam_quest_ca_detail">
              <div id="can-dt2">
                <span class="can-lable2">Membership No. </span>
                <span class="details2"><?PHP echo $membershipNo?></span>
                <span class="can-lable2">Candidate Name</span>
                <span class="details2"><?PHP echo ucwords($_SESSION['memname']);?></span>
              </div>
            </div>
          </div>
          <div class="sam_quest_right_bottom_container">
            <div id="sam-que-att">
              <h1 style="padding-top:12px;"><b>Number of Questions</b></h1>
              <div id="preview_div">
                <div id="que-scroll">
                  <div id="qnodetails"></div>
                </div>
              </div>
              <div class="clear"></div>
              <div class="attempt_container">
              <div class="sam_attempt" id="no_attempt"></div>
              <div class="sam_attempt_txt">Attempted</div>
              <div class="sam_tag " id="no_tagged"></div>
              <div class="sam_tag_text">Tagged</div>
              <div class="sam_unattempt" id="no_unattempt"></div> 
              <div class="sam_unattempt_text">Unattempted</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Link Container-->
    <div class="clear"></div>
    <div id="link-con">
      <div class="container">
        <div id="pre_btn_div">
          <div id="prev_arrow_div"><a href="javascript:void(0);" onclick='javascript:prev()'><img src="images/prev_arrow.png" width="12" height="16" /></a></div>
          <div id="prev_arrow_text"><a href="javascript:void(0);" onclick='javascript:prev()' title="Click to view previous question" id="Prev">Previous Question</a></div>
        </div>
        <div id="next_btn_div">
          <div id="next_arrow_text"><a title="Click to view next question" href="javascript:void(0);" onclick="javascript:next()" id="Next">Next Question</a></div>
          <div id="next_arrow_div"><a href="javascript:void(0);" onclick='javascript:next()'><img src="images/next_arrow.png" width="12" height="16" /></a></div>
        </div>
        
        <div class="sam_quest_tag_btn_div" id="tagDiv"><input class="yellow_button" type="button" value=" Tag " onClick="javascript: tagQ(this);" id="mark_btn" ></div>
        <div class="sam_quest_er_ans_btn_div" id="eraseDiv"><input class="red_button" type="button" value="Erase Answer" title="Click to erase the answer" onclick="eraseAnswer()" /></div>
        <div id="final_submit_btn_div">
          <div id="final_submit_arrow_div"><a href="#" onclick="preview_submit(0)"><img src="images/final_submit_arow.png" width="16" height="13" /></a></div>
          <div id="final_submit_arrow_text"><a title="Click to submit Question Paper" href="#" onclick="preview_submit(0)">Preview Submit</a></div>
    <input type=hidden name="qindex" id="qindex" value='0' />
        </div>
      </div>
    </div>

<!-- Timer Alert -->
<div id='styled_popup' style='cursor:hand;width: 350px; height: 78px; display:none; position: absolute; z-index: 100; top: -4px; left: -3px;' name='styled_popup'>
  <table width="100%" height="75" border="0" cellpadding="0" cellspacing="0" onclick='javascript:styledPopupClose()' >
    <tr>
      <td><table width='100%' height="78" border='0' cellpadding='5' cellspacing='1' class='Report_tb'>
    <tr><th colspan="10">Time Alert</th>  </tr>
      <tr class='tb_rw3'>
          <td colspan='2' align="center">5 Minutes left to auto submit your exam.<br/>
            <br/>
        <input name="button" type="button" class="green_button" onclick='javascript:styledPopupClose()' value=' Close ' />          </td>
        </tr>
      </table></td>
    </tr>
  </table>
</div>
<!-- Timer Alert -->
<?php
include "themes/default/footer.php";
?>
