<?php
/****************************************************
* Application Name            :  IIB 
* Module Name                 :  Online Exam Page
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  iib_candidate_test,iib_section_questions,iib_section_questions_hindi,iib_case_master,iib_case_master_hindi
* Tables used for only selects:  iib_exam_candidate,iib_exam_subjects
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  
* Created ON                  :  
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  14-Dec-09
* Description                 :  Interface for taking the exam wait 
*****************************************************/
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkTALogin();
checkCandidateLogin();
require_once("constants.inc");
$timeLeft = getVal($_POST['time_left']);
$auto_submit= getVal($_POST['auto_submit']);
$total_time_candi = getVal($_POST['total_time_candi']);
$connfailureduration=(int) getVal($_REQUEST['connfailureduration']);
$testid=getVal($_POST['test_id']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?PHP echo TITLE?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<script language=JavaScript>
history.go(1);
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br><br><br>
<form name='wait' method='post' action=''>
<div align=center class=greybluetext10><img src="images/wait.gif" name="waitImage" completed="false" onload="this.setAttribute('completed', true)"></div>
<br>
<div align=center class=greybluetext10>Please wait.. Your score is being calculated...</div>
<input type=hidden name=time_left value='<?=$timeLeft ?>'>
<input type="hidden" name="auto_submit" id="auto_submit" value='<?=$auto_submit?>'>
<input type="hidden" id="total_time_candi" name='total_time_candi' value='<?=$total_time_candi?>'/>
<input type="hidden" id="connfailureduration" name='connfailureduration' value='<?=$connfailureduration?>'/> 
<input type="hidden" id="testid" name='testid' value='<?=$testid?>'/>
</form>
<script language='JavaScript'>
var imgs = document.images;
 var allLoaded = true;
 do {
   for (i=0; i<imgs.length; i++) {
     if (!isImgLoaded)
     {
	     allLoaded = false;
     }
       
   }
 } while (!allLoaded);
 
 if(allLoaded)
 {
  	document.wait.action='online_exam_scores.php';
	document.wait.submit();
 }
 
 function isImgLoaded()
 {
     var blnLoad = document.getElementById("waitImage").complete;
     if(blnLoad)
     {
	     return true;
     }
     else
     {
	     return false;
     }
  }
</script>

<object width="550" height="400">
<param name="movie" value="somefilename.swf">
<embed src="somefilename.swf" width="550" height="400">
</embed>
</object>
</body>
</html>

