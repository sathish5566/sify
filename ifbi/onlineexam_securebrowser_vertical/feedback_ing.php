<?php
/****************************************************
* Application Name            :  IIB 
* Module Name                 :  Online Exam Page
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  iib_feedback
* Tables used for only selects:  iib_feedback
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  
* Created ON                  :  
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  27-Jan-10
* Description                 :  Interface for feed back ing vysya bank
*****************************************************/
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
require_once("constants.inc");
checkOnlineExamOther();
checkScore();

$examCode = $_SESSION['ex'];
$subjectCode = $_SESSION['sc'];
$membershipNo = $_SESSION['memno'];

if( isset($_POST['sub']) && ($_POST['sub']=='Submit') ) 
{	
	$question1 = getVal($_POST['q1']);
	$question2 = getVal($_POST['q2']);
	$question3 = getVal($_POST['q31']."|".$_POST['q32']."|".$_POST['q33']);
	$question4 = getVal($_POST['q4']);
	$question5 = getVal($_POST['q5']);
	$question6 = getVal($_POST['q6']);
	$question7 = getVal($_POST['q7']);
	$question8 = getVal($_POST['txtfeedback']);
	$msg = "";	
	
		$sqlSelect = "SELECT count(1) from iib_ing_feedback WHERE  membership_no='$membershipNo' AND exam_code='$examCode' AND subject_code='$subjectCode' ";
		$resSelect = mysql_query($sqlSelect,$SLAVECONN) or errorlog('err05'," QUERY:$sqlSelect  ".mysql_error($MASTERCONN));   	
		list($nRows) = mysql_fetch_row($resSelect);
		if ($nRows == 0)
		{
			$sqlInsert = "INSERT INTO iib_ing_feedback (membership_no, exam_code, subject_code, question1, question2, question3, question4, question5, question6, question7, question8, updated) values ('$membershipNo', '$examCode', '$subjectCode', '$question1','$question2' ,'$question3', '$question4', '$question5', '$question6', '$question7', '$question8', now()) ";
			mysql_query($sqlInsert,$MASTERCONN) or errorlog('err08'," QUERY:$sqlInsert  ".mysql_error($MASTERCONN)); 				
			$msg = "Thank you for your feedback!";
		}
		else
		{
			$sqlUpdate = "UPDATE iib_ing_feedback SET question1='$question1', question2='$question2', question3='$question3', question4='$question4', question5='$question5', question6='$question6', question7='$question7', question8='$question8', updated=now() WHERE membership_no='$membershipNo' AND exam_code='$examCode' AND subject_code='$subjectCode' ";
			mysql_query($sqlUpdate,$MASTERCONN) or errorlog('err06'," QUERY:$sqlUpdate  ".mysql_error($MASTERCONN)); 
			$msg = "Thank you for your feedback!";
		}
}			
?>	
<html>
<head>
	<title><?PHP echo TITLE?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="./images/iibf.css" rel="stylesheet" type="text/css">
	<style type="text/css">
	.tdRight{ font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-style: normal; font-weight: normal; color: #395157; text-decoration: none; color:#000; padding:5px; border-bottom:1px solid #E4E1E1;}
	</style>
		<script language="javascript" type="text/javascript" src="./includes/browser.js"></script>
	<script language='JavaScript' src="./includes/validations.js"></script>
	<script language='JavaScript'>

function get_radio_value(obj)
{	
	var rad_val='';
	for (var i=0; i < obj.length; i++)
  	{
	   if (obj[i].checked)
	   {
		  var rad_val = obj[i].value;
	   }
   	}
	return  rad_val;
}	
	
	function submitForm()
	{			
		if(get_radio_value(document.frmfeedback.q1)==""){
			alert("Please select Question No.1");
			document.frmfeedback.q1[0].focus();
			return false;
		}		
		if(get_radio_value(document.frmfeedback.q2)==""){
			alert("Please select Question No.2");
			document.frmfeedback.q2[0].focus();
			return false;
		}				
		if(get_radio_value(document.frmfeedback.q31)==""){
			alert("Please select Question No.3");
			document.frmfeedback.q31[0].focus();
			return false;
		}		
		if(get_radio_value(document.frmfeedback.q32)==""){
			alert("Please select Question No.3");
			document.frmfeedback.q32[0].focus();
			return false;
		}		
		if(get_radio_value(document.frmfeedback.q33)==""){
			alert("Please select Question No.3");
			document.frmfeedback.q33[0].focus();
			return false;
		}				
		if(get_radio_value(document.frmfeedback.q4)==""){
			alert("Please select Question No.4");
			document.frmfeedback.q4[0].focus();
			return false;
		}		
		if(get_radio_value(document.frmfeedback.q5)==""){
			alert("Please select Question No.5");
			document.frmfeedback.q5[0].focus();
			return false;
		}		
		if(get_radio_value(document.frmfeedback.q6)==""){
			alert("Please select Question No.6");
			document.frmfeedback.q6[0].focus();
			return false;
		}		
		if(get_radio_value(document.frmfeedback.q7)==""){
			alert("Please select Question No.7");
			document.frmfeedback.q7[0].focus();
			return false;
		}				
		val = trim(document.frmfeedback.txtfeedback.value);
		if (val != "")
		{			
			strlen = val.length;		
			if (strlen > 120)
			{
				alert('Feedback text can contain maximum of only 120 characters');
				document.frmfeedback.txtfeedback.focus();
				return false;
			}
		}		
		return true;
	}
	function chkParent(h)
	{		
		var g = (navigator.appName == "Netscape") ? 1 : 0;
    	h = (g) ? h : window.event;
   	    if (!g) {
			if (h.clientY < 0) {
				window.opener.location.href="logout.php";
				self.close();			
			}
		}else
		{
			if(document.frmfeedback.action=='')
			{
				window.opener.location.href="logout.php";
				self.close();			
			}
		}	
	}
	
	function screenclose()
	{
		window.opener.location.href="login.php";
		self.close();
	}
		
	function viewscrecard()
	{
		document.frmfeedback.action="online_exam_scores_ing.php";
		document.frmfeedback.submit();
//		self.close();
	}	
	</script>
    <style type="text/css">
	<!--
	.style1 {
		color: #FFFFFF;
		font-weight: bold;
	}
	.tdRight1 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-style: normal; font-weight: normal; color: #395157; text-decoration: none; color:#000; padding:5px; border-bottom:1px solid #E4E1E1;}
-->
    </style>
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" onKeyDown="return doKeyDown(event);" ondragstart ="return Stop(event)" onselectstart="return Stop(event)" onmousedown="return Stop(event)" onmouseup="return Stop(event)">
<center>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" >
	<?php include("includes/header.php")?>
	</table>
	<?php
	if ($msg == "")
	{
	?>
	<table width="100%" border="0" align="center" cellpadding="5" cellspacing="1"  bgcolor="#E8EFF7" >
		<form name='frmfeedback' method='post' onSubmit="return submitForm()">
		<input type=hidden name='membership_no' value='<?=$membershipNo ?>'>
		<input type=hidden name='exam_code' value='<?=$examCode ?>'>
		<input type=hidden name='subject_code' value='<?=$subjectCode ?>'>
		<input type=hidden name='calcscore' value='<?=$calcscore?>'>
		<tr>
		  <td colspan=7 align="center" class=greybluetext10><strong>Feedback on the Online-Test </strong></td>
	  </tr>
		<tr>
          <td width="29" align="center" valign=top bgcolor="#004a80" class=greybluetext10 ><span class="style1">S.No</span></td>
		  <td width="444" align="center" valign=top bgcolor="#004a80" class=greybluetext10 ><span class="style1">Question</span></td>
		  <td colspan="5" align="left" bgcolor="#004a80" class=greybluetext10 ><span class="style1">Response</span></td>
	  </tr>
		<tr>
          <td height="48" align="left" valign=top class="greybluetext10" ><strong>1.</strong></td>
		  <td align="left" valign=top class="greybluetext10" ><strong>Did you have enough adequate time to complete all questions?</strong></td>
		  <td width="28" align="left" valign="middle" class="greybluetext10" ><input type=radio value='Y' name='q1'></td>
	      <td width="50" align="left" valign="middle" class="greybluetext10" >YES</td>
	      <td width="20" align="left" valign="middle" class="greybluetext10" ><input type=radio value='N' name='q1'></td>
	      <td width="112" align="left" valign="middle" class="greybluetext10" >NO</td>
	      <td width="19" align="left" class="greybluetext10" >&nbsp;</td>
	  </tr>		
		<tr>
		  <td align="left" valign=top class="greybluetext10" ><strong>2.</strong></td>
		  <td align="left" valign=top class="greybluetext10" ><strong>Is the venue facilities where you are sitting satisfactory?</strong></td>
		  <td align="left" valign="middle" class="greybluetext10" ><input type=radio value='Y' name='q2'></td>
		  <td align="left" valign="middle" class="greybluetext10" >YES</td>
		  <td align="left" valign="middle" class="greybluetext10" ><input type=radio value='N' name='q2'></td>
		  <td align="left" valign="middle" class="greybluetext10" >NO</td>
		  <td align="left" class="greybluetext10" >&nbsp;</td>
	  </tr>
		<tr>
		  <td align="left" valign=top class="greybluetext10" ><strong>3.</strong></td>
		  <td align="left" valign=top class="greybluetext10" ><strong>Did you face any issue in:</strong></td>
		  <td colspan="5" align="left" valign="middle" class="greybluetext10" >&nbsp;</td>
	  </tr>
		<tr>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" valign=top class="greybluetext10" ><strong>Login / Password and registration no</strong></td>
		  <td align="left" valign="middle" class="greybluetext10" ><input type=radio value='Y' name='q31'></td>
		  <td align="left" valign="middle" class="greybluetext10" >YES</td>
		  <td align="left" valign="middle" class="greybluetext10" ><input type=radio value='N' name='q31'></td>
		  <td align="left" valign="middle" class="greybluetext10" >NO</td>
		  <td align="left" class="greybluetext10" >&nbsp;</td>
	  </tr>
		<tr>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" valign=top class="greybluetext10" ><strong>Navigating the screen to next questions</strong></td>
		  <td align="left" valign="middle" class="greybluetext10" ><input type=radio value='Y' name='q32'></td>
		  <td align="left" valign="middle" class="greybluetext10" >YES</td>
		  <td align="left" valign="middle" class="greybluetext10" ><input type=radio value='N' name='q32'></td>
		  <td align="left" valign="middle" class="greybluetext10" >NO</td>
		  <td align="left" class="greybluetext10" >&nbsp;</td>
	  </tr>
		<tr>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" valign=top class="greybluetext10" ><strong>Any other technical issues</strong></td>
		  <td align="left" valign="middle" class="greybluetext10" ><input type=radio value='Y' name='q33'></td>
		  <td align="left" valign="middle" class="greybluetext10" >YES</td>
		  <td align="left" valign="middle" class="greybluetext10" ><input type=radio value='N' name='q33'></td>
		  <td align="left" valign="middle" class="greybluetext10" >NO</td>
		  <td align="left" class="greybluetext10" >&nbsp;</td>
	  </tr>
		<tr>
		  <td align="left" valign=top class="greybluetext10" ><strong>4.</strong></td>
		  <td align="left" valign=top class="greybluetext10" ><strong>Were you able to see your marks at the end of the online test?</strong></td>
		  <td align="left" valign="middle" class="greybluetext10" ><input type=radio value='Y' name='q4'></td>
		  <td align="left" valign="middle" class="greybluetext10" >YES</td>
		  <td align="left" valign="middle" class="greybluetext10" ><input type=radio value='N' name='q4'></td>
		  <td align="left" valign="middle" class="greybluetext10" >NO</td>
		  <td align="left" class="greybluetext10" >&nbsp;</td>
	  </tr>
	  <tr>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" class="greybluetext10" >&nbsp;</td>
		  <td colspan="4" align="left" class="greybluetext10" >&nbsp;</td>
		</tr>
		<tr>
		  <td align="left" valign=top class="greybluetext10" ><strong>5.</strong></td>
		  <td align="left" valign=top class="greybluetext10" ><strong>How were the questions in Part A (Aptitude)?</strong></td>
		  <td align="left" class="greybluetext10" ><input type=radio value='A' name='q5'></td>
          <td colspan="4" align="left" class="greybluetext10" >Easy</td>
      </tr>
		<tr>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" class="greybluetext10" ><input type=radio value='B' name='q5'></td>
		  <td colspan="4" align="left" class="greybluetext10" >Difficult</td>
	  </tr>
		<tr>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" class="greybluetext10" ><input type=radio value='C' name='q5'></td>
		  <td colspan="4" align="left" class="greybluetext10" >Not relevant </td>
	  </tr>
		<tr>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" class="greybluetext10" ><input type=radio value='D' name='q5'></td>
		  <td colspan="4" align="left" class="greybluetext10" >Can't Say </td>
	  </tr>
		<tr>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" class="greybluetext10" >&nbsp;</td>
		  <td colspan="4" align="left" class="greybluetext10" >&nbsp;</td>
		</tr>
		<tr>
		  <td align="left" valign=top class="greybluetext10" ><strong>6.</strong></td>
		  <td align="left" valign=top class="greybluetext10" ><strong>How were the questions in Part B (General Banking)?</strong></td>
		  <td align="left" class="greybluetext10" ><input type=radio value='A' name='q6'></td>
		  <td colspan="4" align="left" class="greybluetext10" >Easy</td>
	  </tr>
		<tr>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" class="greybluetext10" ><input type=radio value='B' name='q6'></td>
		  <td colspan="4" align="left" class="greybluetext10" >Difficult</td>
	  </tr>
		<tr>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" class="greybluetext10" ><input type=radio value='C' name='q6'></td>
		  <td colspan="4" align="left" class="greybluetext10" >Not relevant </td>
	  </tr>
		<tr>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" class="greybluetext10" ><input type=radio value='D' name='q6'></td>
		  <td colspan="4" align="left" class="greybluetext10" >Can't Say </td>
	  </tr>
	  <tr>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" class="greybluetext10" >&nbsp;</td>
		  <td colspan="4" align="left" class="greybluetext10" >&nbsp;</td>
		</tr>
		<tr>
		  <td align="left" valign=top class="greybluetext10" ><strong>7.</strong></td>
		  <td align="left" valign=top class="greybluetext10" ><strong>Overall how would you rate the online test methodology (1-Poor; 5-Excellant)</strong></td>
		  <td align="left" class="greybluetext10" ><input type=radio value='1' name='q7'></td>
		  <td colspan="4" align="left" class="greybluetext10" >1</td>
	  </tr>
		<tr>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" class="greybluetext10" ><input type=radio value='2' name='q7'></td>
		  <td colspan="4" align="left" class="greybluetext10" >2</td>
	  </tr>
		<tr>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" class="greybluetext10" ><input type=radio value='3' name='q7'></td>
		  <td colspan="4" align="left" class="greybluetext10" >3</td>
	  </tr>
		<tr>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" class="greybluetext10" >
	      <input type=radio value='4' name='q7'>		  </td>
		  <td colspan="4" align="left" class="greybluetext10" >4</td>
	  </tr>
		<tr>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" class="greybluetext10" ><input type=radio value='5' name='q7'></td>
		  <td colspan="4" align="left" class="greybluetext10" >5</td>
	  </tr>
	  <tr>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" valign=top class="greybluetext10" >&nbsp;</td>
		  <td align="left" class="greybluetext10" >&nbsp;</td>
		  <td colspan="4" align="left" class="greybluetext10" >&nbsp;</td>
		</tr>
		<tr>
		  <td valign=top class="greybluetext10" ><strong>8.</strong></td>
		  <td valign=top class="greybluetext10"  ><strong>Any other Comments?</strong></td>
		  <td colspan="5" class="greybluetext10" >
		      <textarea name="txtfeedback" cols="40" class="greybluetext10"></textarea> 
	     <br/>(Maximum 120 chars)</td>
		</tr>
		<tr><td colspan=7 align="center"><br>
		  <span class="greybluetext10">
		  <input type=submit class=button name='sub' value='Submit' >
	    </span></td></tr>
	</form>
		<tr><td colspan="2"></td>
	<tr><td colspan="2"></tr>
  </table>
	<?php
	}
	else
	{
	?>
		<form name='frmfeedback' method='post'>
		<input type=hidden name='membership_no' value='<?=$membershipNo ?>'>
		<input type=hidden name='exam_code' value='<?=$examCode ?>'>
		<input type=hidden name='subject_code' value='<?=$subjectCode ?>'>
		<input type=hidden name='calcscore' value='<?=$calcscore?>'>
		<input type=hidden name=frmfeedback value='ingfeedback'>	

	<table border="0" cellspacing="0" cellpadding="0" width="100%" background="images/tile.jpg" align=center >
		<tr><td align=center><br></td></tr>
		<tr><td  align=center class=greybluetext10><?php print($msg); ?></td></tr>
		<tr><td align=center><br></td></tr>
		<tr><td align=center><input type=button class=button name='sub_exit' value='View Score Card' onClick='javascript:viewscrecard();'></td></tr>
		<tr><td align=center><br></td></tr>
	</table>
  </form>
	<?php
	}
	?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<?php include("includes/footer.php")?>

</table>
<br>

</center>
	<br>
</body>
</html>	
<?php
//mysql_close($MASTERCONN);
//mysql_close($SLAVECONN);

?>
