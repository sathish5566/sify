<?php
/****************************************************
* Application Name			: IBPS
* Module Name				: Intermediate Login Page
* Revision Number			: 1
* Revision Date				: 29-04-2010
* Table(s) used for Selects	: - 
* View(s)				:  -
* Stored Procedure(s)		:  -
* Dependant Module(s)		:  -
* Output File(s)			: 
* Document/Reference Material: This file is used to check for optional subjects 
								and redirectthe candidate to optional subject selection page.
* Created By				: Sathiyanarayanan S
* Created On				: 29-04-2010
* Last Modified By			: Sathiyanarayanan S
* Last Modified Date		: 
*****************************************************/
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
$strSourceFile = $_SERVER['HTTP_REFERER'];
checkCandidateLogin();
//checkOnlineExamOther();

$sqlSelectTest = "SELECT membership_no , exam_code , subject_code , test_status from iib_candidate_test where current_session='Y'";
$resSelectTest = @mysql_query($sqlSelectTest,$SLAVECONN) or errorlog('err05',"SQL:$sqlSelectTest ".mysql_error($SLAVECONN));
$cntSelectTest = mysql_num_rows($resSelectTest);
if($cntSelectTest > 0)
{
	while(list($M_No,$E_Code,$SC,$TS)=mysql_fetch_row($resSelectTest))
	{
		$StatusArr[$M_No][$E_Code][$SC]=$TS;
	}
}
/*
 ** Function to fetch the status(IC/C/0) of the candidate whose exam code , subject code is being passed as an input along with membership no. The result will be returned to the called page.
 ** IC - Incomplete Status
 ** C - Complete Status
 ** 0 - Not taken the test
 */

function checkExamStatus($MemNo,$ExamCode,$SubCode)
{
	$return_value=0;
	global $StatusArr;	
	if(is_array($StatusArr))
	{				
		if(array_key_exists($SubCode,$StatusArr[$MemNo][$ExamCode]))
		{			
			$return_value = $StatusArr[$MemNo][$ExamCode][$SubCode];			
			return $return_value;
		}
		else
		{			
			return $return_value;
		}
	}
	else
	{		
		return $return_value;
	}
}

 $taLogin = "iwfr_".$_SESSION['centrecode'];
 $cafeID = $_SESSION['centrecode'];
 $memno = $_SESSION['memno'];
 $optional_sub=$_SESSION['sub_opt'];



$emsg = "";
if(isset($memno) && isset($taLogin) && isset($optional_sub))
{

    if (trim($memno) != "" && trim($taLogin) !== "" && $optional_sub=="Y")
    {
			//$_SESSION['optional_sub']=$optional_sub;
	
			$hostIP = $_SERVER['REMOTE_ADDR'];	        
			$sessionID = session_id();
			trackCandidateLogin($memno, $taLogin, $cafeID, $hostIP, $sessionID);

			$aExamCode = array();
			$aSubCode = array();
			$aCentreCode = array();

        	$todaydate = date("Y-m-d");
        	$currTimeStamp = mktime(date("H"),date("i"), date("s"),date("m"),date("d"),date("Y"));
			$exam_cand_sel = "select subject_code from iib_exam_candidate where membership_no='$memno' ";
			$query_cand = mysql_query($exam_cand_sel,$SLAVECONN) or errorlog('err05',"SQL:$exam_cand_sel ".mysql_error($SLAVECONN));
			$num_cand = mysql_num_rows($query_cand);
			if($num_cand > 0)
			{  
			   $f=0;
			   while(list($sub_ecode) = mysql_fetch_row($query_cand))
				{
				   $sub_codearr[$f] =$sub_ecode; 
				   $f++;
				}
			}
			$selGrace="select break_duration , grace_pre , grace_post , exam_code , subject_code , option_id, option_flag from iib_exam_subjects order by exam_code,subject_code";
			$resGrace=mysql_query($selGrace,$SLAVECONN)or errorlog('err05',"SQL:$selGrace ".mysql_error($SLAVECONN));
			while(list($dur_brk,$pre_grace,$post_grace,$ECode,$SCode,$opt_id,$opt_flag)=mysql_fetch_row($resGrace))
			{
				if(($opt_flag=='Y') && in_array($opt_id,$sub_codearr))
					$SCode = $opt_id;
				$durArr[$ECode][$SCode] = $dur_brk;
				$pre_graceArr[$ECode][$SCode] = $pre_grace /60;
				$post_graceArr[$ECode][$SCode] = $post_grace /60;
			} 
			
			
		 
			$sqlTime1 = "SELECT TIME_TO_SEC(exam_time),exam_time,subject_code,exam_code,centre_code from iib_candidate_iway where  membership_no='$memno' and exam_date='$todaydate' order by subject_code";
        	$resTime = mysql_query($sqlTime1,$SLAVECONN) or errorlog('err05',"SQL:$sqlTime1 ".mysql_error($SLAVECONN));
        	$nTime = @mysql_num_rows($resTime);
        	if ($nTime > 0)
        	{
	        	$nCntTime = 0;
	        	while (list($eTimeSec,$eTime,$suCode,$exCode,$ceCode) = mysql_fetch_row($resTime))
	        	{
		        	$aTime[$nCntTime] = $eTime;
					$aExamCode[$nCntTime] = $exCode;
					$aSubCode[$nCntTime] = $suCode;
					$aCentreCode[$nCntTime] = $ceCode;
					$aTimeSec[$nCntTime] = $eTimeSec;
					if($nCntTime != 0)
					{
						$grace_Etime = $aTimeSec[$nCntTime] - $durArr[$exCode][$suCode];
						$grace_time[$nCntTime] = sec2hms($grace_Etime);
					}
			        	$nCntTime++;
					 
	        	} //end of while
	        	//print_r($aTime);
	        	$check_array=array_unique($aTime);

	        	if ($nCntTime > 1)
	        	{
	        		$break = false;
	        		for($l=0;$l<$nCntTime;$l++)
	        		{
						$SubjectStatus = checkExamStatus($memno,$aExamCode[$l],$aSubCode[$l]);
			        	if($SubjectStatus == '0' || $SubjectStatus == 'IC')
			        	{
				        	$selExamTime = $aTime[$l];
							$subject_code = $aSubCode[$l];
							$exam_code = $aExamCode[$l];
							$centre_code = $aCentreCode[$l];
							$break=true;
			        	}
			        	else
			        	{
				        	$selExamTime = $aTime[$l];
							$subject_code = $aSubCode[$l];
							$exam_code = $aExamCode[$l];
							$centre_code = $aCentreCode[$l];
			        	}
			        	if($break) break;
	        		}
	        	} //end of time check
				else
				{
					$selExamTime = $aTime[0];
					$subject_code = $aSubCode[0];
					$exam_code = $aExamCode[0];
					$centre_code = $aCentreCode[0];
				}
				$_SESSION['sc'] = $subject_code;
				$_SESSION['ex'] = $exam_code;
				$_SESSION['et'] = $selExamTime;
	  			if (strtoupper($centre_code) == strtoupper($cafeID))
		        {	
			        if($SubjectStatus != 'IC' || $SubjectStatus == 0)
			        {
				        $todate_start = date("Y-m-d")." 00:00:00";
			    	    $todate_end = date("Y-m-d")." 23:59:59";
		        		$sqlTestTaken = "SELECT COUNT(1) FROM iib_candidate_test WHERE ".
		        			" membership_no='$memno' AND exam_code='$exam_code' AND subject_code='$subject_code' ".
			      			" AND start_time between '$todate_start' and  '$todate_end' ";
	
						if ($taOverride == 'Y')
		    	    	{
			    	    	 $sqlTestTaken .= "AND test_status='C' ";
		        		}
	
			        	$resTestTaken = @mysql_query($sqlTestTaken,$SLAVECONN)  or errorlog('err05',"SQL:$sqlTestTaken ".mysql_error($SLAVECONN)); 
			        	list($nTestTaken) = @mysql_fetch_row($resTestTaken);
	
						//check for test table entry
		    	    	if ($nTestTaken > 0)
		        		{
			        		$time_key=array_search($subject_code,$aSubCode);
						
							if ($time_key != ($nCntTime-1))
							{
						
						        if($SubjectStatus != 'IC')
						        {
						    		$emsg = "You have already logged in for your previous test. Login after ". $grace_time[$time_key+1] . " to take the next test ";    	
					    		}
					    		else
					    		{
						    		$emsg = "You have not completed your previous test.Please contact the Test Administrator.";    	
					    		}
					        }
					        else
					        {
						        if($SubjectStatus != 'IC')
						        {
						        	$emsg = "You have already taken the test.";
					        	}
					        	else
					        	{
						        	$emsg = "You have not completed the test. Please contact Test Administrator";
					        	}
					        }
		    	    	}
		        		else
		        		{
						
							//check for start exam
			        		if ($taOverride != 'Y')
				        	{
					        		if(count($check_array)!=count($aTime)){
									$emsg="Problem in Exam Timing.Please Re-schedule the Timings";
								}
								else{
					        		$chkTime = "select (TIME_TO_SEC(current_time)-TIME_TO_SEC(exam_time))/60  time_diff ".
			            				" from iib_candidate_iway where membership_no='$memno' and exam_date='$todaydate' AND ".
			            				" exam_code='$exam_code' AND subject_code='$subject_code' ";

									$resTime = @mysql_query($chkTime,$SLAVECONN) or errorlog('err05',"SQL:$chkTime ".mysql_error($SLAVECONN));
			        	   			list ($timeDiff) = @mysql_fetch_row($resTime);

			        	   			if ($timeDiff < -($pre_graceArr[$exam_code][$subject_code]))
				            		{
					            		$emsg = "You are allowed to login only ".$pre_graceArr[$exam_code][$subject_code]." minutes prior to your exam time. Please contact the Test Administrator.";
			    	    	    	} //end of 15 mins prior
			        	    		else
			            			{	$post_graceArr[$exam_code][$subject_code];
				            			if ($timeDiff > $post_graceArr[$exam_code][$subject_code])
				            			{
					            			$emsg = "Exam timed out for Optional Subject. Please contact the Test Administrator for Re-scheduling the Timings.";
				        	    		}
				            			else
			    	        			{
				    	        			
											$subskip_sql="select option_flag from iib_exam_subjects where exam_code='$exam_code' AND subject_code='$subject_code'";
											$exec_subskip = mysql_query($subskip_sql,$SLAVECONN);
											list($opt_flag) = mysql_fetch_row($exec_subskip);																							
											@header("Location: sub_choice.php?sc=$subject_code&ex=$exam_code&et=$selExamTime");
											exit;
											
/** Added to skip the language choice and subject choice pages with the conditions ***/
				        	    		}
				            		}
		            			}//end of same time check
			        		} //end of start exam check
			        		elseif($taOverride== 'Y')
		    	    		{
//								
								/** Added for Language selection and subject selection **/
								@header("Location: logincandidate1.php");
								exit;
							//	@header("Location: logincandidate1.php?memno=$memno&ltime=$hid_lefttime&ta_override=$taOverride&hidd_lan=$medium_code");
		        			} //end of restart exam check
						}
					}// prev test status check ends here
					else
					{
						$emsg = "Candidate has not completed the previous test. Please contact the Test Administrator.";	
					}
    	    	}
        		else
        		{
	        		$emsg = "Candidate is not allowed to take the exam from this centre. Please contact the Test Administrator.";
	        	} //end of check cafe
        	} //end of time count
        	else
        	{
         		$emsg = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;There is no exam scheduled for you today.";
        	}
        /*} //end of password check
        else
        {
             $emsg .= "Invalid Login Set for this Session.";
        }*/
    }
    else
    {
    	$emsg = "Login name or Ta Login Invalid for this Session." ;
    }   
}else{
    	$emsg = "Session Failed " ;
}


?>

<?php 
if(isset($emsg)){
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?PHP echo TITLE?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<link href="images/iibf.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="./includes/browser.js"></script>
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onKeyDown="return doKeyDown(event);" ondragstart ="return Stop(event)" onselectstart="return Stop(event)" onmousedown="return Stop(event)" onmouseup="return Stop(event)" >
<Form name="examFrm" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" background="images/tile.jpg">
 <tr> 
    <? include("includes/header.php");?>
  </tr>
<tr valign="top" HEIGHT="10" ><td>&nbsp;</td></tr>
<tr valign="top" HEIGHT="10" ><td><?php include("includes/inner_header.php")?></td></tr>
<tr valign="top" HEIGHT="10" ><td>&nbsp;</td></tr>
<tr><td>
<TABLE Border="0" Bordercolor="#004a80"  align="center" cellspacing="0">
<TR><TD>
	<table width="262" border="0"  cellpadding="5" cellspacing="0">
	<tr>
		<td width="242" bgcolor="#D1E0EF" class="greybluetext10"><?php echo $emsg; ?></td>
	</tr>
	<tr>
		<td class="greybluetext10"  bgcolor="#D1E0EF" align="center"><a href="login.php">TA authorization for Re-Login</a></td>		
	</tr>
	<tr>
		<td class="greybluetext10"  bgcolor="#D1E0EF" align="center"><a href="#" onClick="javascript:window.close();">Exit</a></td>		
	</tr>
	</table>
</TD></TR>
</TABLE>
</td><tr> 
<tr>
    <?php include("includes/footer.php");?>
  </tr>
</table>
</Form>
</body>
</html>

<?php 
    } 
    //mysql_close($SLAVECONN);
?>
