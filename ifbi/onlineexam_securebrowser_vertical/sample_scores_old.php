<?php
/****************************************************
* Application Name			: IIB
* Module Name				: Sample scores page
* Revision Number			: 1
* Revision Date			:
* Table(s) used for Selects	:  iib_candidate , iib_exam , iib_exam_subjects , iib_iway_details , iib_candidate_iway 
* Tables used for Updates,Inserts : 
* View(s)				:  -
* Stored Procedure(s)		:  -
* Dependant Module(s)		:  -
* Output File(s)			: -
* Document/Reference Material	:
* Created By				: 
* Created On				: 
* Last Modified By			: Bala Murugan.S
* Last Modified Date		: 31-08-2010
* Change reason				: 
* Description               : Page where the score will be calculated for the the sample test. 
*****************************************************/
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkTALogin();
checkCandidateLogin();
require_once("constants.inc");
	
	$time_to_submit = getVal($_REQUEST['time_to_submit']);
	$centreCode = $_SESSION['cafeID'];
	$todaydate=date('Y-m-d');
	
	$exam_code = $_SESSION['ex'];
	$subject_code = $_SESSION['sc'];
	$selExamTime = $_SESSION['et'];
	$display_scr = $_SESSION['display_score'];
  	$display_res = $_SESSION['display_result'];
	$result_type=$_SESSION['result_type'];		
  	for ($i =0 ; $i < 5; $i++)
  	{		
		$sample_answer = trim($_POST["answer".($i+1)]);					
  		if ($sample_answer == $QUESTION_ARRAY[$i]['EN']['correct_answer'])
  		{
	  		$sample_score += $QUESTION_ARRAY[$i]['EN']['mark'];
  		}
  		if (($sample_answer != $QUESTION_ARRAY[$i]['EN']['correct_answer']) && ($sample_answer != ""))
  		{
	  		$sample_score -=  $QUESTION_ARRAY[$i]['EN']['negative_mark'];
  		}
  	} // end of for

  	$sample_score = round($sample_score);

  	//code added to convert negative marks to 0
  	if ($sample_score == -0)
  		$sample_score = 0;
	if ($sample_score < 0)
		$sample_score = 0;

  	if ($commonDebug)
  	{
  		print("<br>score = ".$sample_score);
  		print("<br>total marks = ".$sample_totalMarks);
  		print("<br>percentage = ".$sample_percentage);
  		print("<br>pass mark = ".$sample_passMark);
	}
	if ($sample_score >= $sample_passMark)
	{
		if($result_type == 'PA')
			$sample_displayResult = 'Pass';
		elseif($result_type == 'PR')
			$sample_displayResult = 'Provisional';
	
	}
	else
	{
		if($result_type == 'PA')
			$sample_displayResult = 'Fail';
		elseif($result_type == 'PR')
			$sample_displayResult = 'Provisional';
	}

	
	

	$tmpDate = $todaydate;
	$tmpTime = $selExamTime;
	$tmpDate = explode('-',$tmpDate);
	$tmpTime =  explode(':',$tmpTime);
	$examDate = $tmpDate[2]."/".$tmpDate[1]."/".$tmpDate[0];
	$examTime = $tmpTime[0].":".$tmpTime[1];
	$sqlIWay = "SELECT iway_address1, iway_address2, iway_city, iway_state, iway_pin_code FROM iib_iway_details ".
		" WHERE centre_code='$centreCode'";
	if ($commonDebug)
	{
		print("\n".$sqlIWay);
	}
	$resIWay = @mysql_query($sqlIWay,$SLAVECONN) or errorlog('err05'," iib_iway_details  ".mysql_error($SLAVECONN));
	$row = mysql_fetch_array($resIWay);
	$iwayAddress = "";
	$row['iway_address1'] != "" ? $iwayAddress .= " ".$row['iway_address1'] : $iwayAddress .= "";
	$row['iway_city'] != "" ? $iwayAddress .= " ".$row['iway_city'] : $iwayAddress .= "";
	$row['iway_pin_code'] != "" ? $iwayAddress .= " ".$row['iway_pin_code'] : $iwayAddress .= "";
	$row['iway_state'] != "" ? $iwayAddress .= " ".$row['iway_state'] : $iwayAddress .= "";
	$row['iway_address2'] != "" ? $iwayAddress .= ",Contact No. :  ".$row['iway_address2'] : $iwayAddress .= "";
?>
<html>
<head>
<title><?PHP echo TITLE?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<script language='JavaScript' src="./includes/timer_submit.js"></script>
<script language="javascript" type="text/javascript" src="./includes/browser.js"></script>
<script language='JavaScript'>
history.go(1);
function valid(){
	document.getElementById("sub_form").disabled=true;
    document.getElementById("submitstatus").innerHTML='please wait submitting form...';
    document.frmscores.submit();
}
function callOnLoad()
{
	InitializeTimer(<?=$time_to_submit?>,"frmscores","online_exam.php");
}
window.onload =  callOnLoad;
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad='javascript:callOnLoad();' onKeyDown="return doKeyDown(event);"  ondragstart ="return Stop(event)" onselectstart="return Stop(event)"onmousedown="return Stop(event)" onmouseup="return Stop(event)">
<center>
<form name='frmscores' method='post' action='online_exam.php'>
	<input type=hidden name='time_to_submit' value='<?=$time_to_submit?>'>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<?php include("includes/header.php")?>
		</tr>
		<tr>			
		</tr>
	</table>
	<table border="0" cellspacing="0" cellpadding="0" width="100%" background="images/tile.jpg">
		<tr>
			<td>
			<br>
			<table width="933" border="0" align="center" cellpadding="0" cellspacing="0" >
              <tr>
                <td><br>
                  <table width="75%" border="0" align="center" cellpadding="1" cellspacing="0">
                    <tr>
                      <td bgcolor="#014A7F"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                          <tr>
                            <td align="center"><table border="0" cellspacing="0" cellpadding="10" width="100%" align="center">
                                <tr>
                                  <td colspan="3" class="ColumnHeader1" bgcolor="#D1E0EF">Candidate Exam Result</td>
                                </tr>
                                <tr>
                                  <td width="35%" class="greybluetext10" bgcolor="#E8EFF7"><b>Membership No</b></td>
                                  <td width="2%" class="greybluetext10" bgcolor="#E8EFF7" align="center"><b>:</b></td>
                                  <td width="53%" bgcolor="#E8EFF7" class="greybluetext10"><?=$_SESSION['memno'];?></td>
                                </tr>
                                <tr>
                                  <td class="greybluetext10" bgcolor="#E8EFF7"><b>Name</b></td>
                                  <td class="greybluetext10" bgcolor="#E8EFF7" align="center"><b>:</b></td>
                                  <td bgcolor="#E8EFF7" class="greybluetext10"><?=$_SESSION['memname']?></td>
                                </tr>
                                <tr>
                                  <td class="greybluetext10" bgcolor="#E8EFF7"><b>Address</b></td>
                                  <td class="greybluetext10" bgcolor="#E8EFF7" align="center"><b>:</b></td>
                                  <td bgcolor="#E8EFF7" class="greybluetext10"><?=$_SESSION['address']; ?></td>
                                </tr>
                                <tr>
                                  <td class="greybluetext10" bgcolor="#E8EFF7"><b>Examination</b></td>
                                  <td class="greybluetext10" bgcolor="#E8EFF7" align="center"><b>:</b></td>
                                  <td bgcolor="#E8EFF7" class="greybluetext10"><?=$_SESSION['exam_name']?></td>
                                </tr>
                                <tr>
                                  <td class="greybluetext10" bgcolor="#E8EFF7"><b>Subject Name</b></td>
                                  <td class="greybluetext10" bgcolor="#E8EFF7" align="center"><b>:</b></td>
                                  <td bgcolor="#E8EFF7" class="greybluetext10"><?=$_SESSION['subject_name'] ?></td>
                                </tr>
                                <tr>
                                  <td class="greybluetext10" bgcolor="#E8EFF7"><b>Examination Centre</b></td>
                                  <td class="greybluetext10" bgcolor="#E8EFF7" align="center"><b>:</b></td>
                                  <td bgcolor="#E8EFF7" class="greybluetext10"><?=$iwayAddress ?></td>
                                </tr>
                                <tr>
                                  <td class="greybluetext10" bgcolor="#E8EFF7"><b>Examination Date</b></td>
                                  <td class="greybluetext10" bgcolor="#E8EFF7" align="center"><b>:</b></td>
                                  <td bgcolor="#E8EFF7" class="greybluetext10"><? echo ($examDate); ?></td>
                                </tr>
                                <tr>
                                  <td class="greybluetext10" bgcolor="#E8EFF7"><b>Examination Time</b></td>
                                  <td class="greybluetext10" bgcolor="#E8EFF7" align="center"><b>:</b></td>
                                  <td bgcolor="#E8EFF7" class="greybluetext10"><?php if ($examTime != "") echo ($examTime." Hrs."); ?></td>
                                </tr>
                                <?
				if ($display_scr == 'Y')
				{
				?>
                                <tr>
                                  <td class="greybluetext10" bgcolor="#E8EFF7"><b>Score</b></td>
                                  <td class="greybluetext10" bgcolor="#E8EFF7" align="center"><b>:</b></td>
                                  <td bgcolor="#E8EFF7" class="greybluetext10"><?=$sample_score ?></td>
                                </tr>
                                <?
				}
				if ($display_res == 'Y')
				{
				?>
                                <tr>
                                  <td class="greybluetext10" bgcolor="#E8EFF7"><b>Result</b></td>
                                  <td class="greybluetext10" bgcolor="#E8EFF7" align="center"><b>:</b></td>
                                  <td bgcolor="#E8EFF7" class="greybluetext10"><?=$sample_displayResult ?></td>
                                </tr>
                                <?
				}
				?>
                                <tr>
                                  <td class="greybluetext10" bgcolor="#E8EFF7" colspan="3"><?php
/*
if($result_type == 'PR')
{
?>
					<span class="greybluetext10"><b><?=$_SESSION['memname'] ?> has secured <?=$sample_score ?>/<?=$sample_totalMarks ?> Marks in <?=$_SESSION['subject_name'] ?></b> <br><br></span>
<?php
}
elseif($result_type == 'PA')
{
?>
					<span class="greybluetext10"><b><?=$_SESSION['memname'] ?> has secured <?=$sample_score ?>/<?=$sample_totalMarks ?> Marks in <?=$_SESSION['subject_name'] ?></b> <br><br>
					</span>
<?php
}*/
?>
                                      <!--Final results will be displayed once your result is reviewed by IBPS.<br>-->
                                      <b><br />
                                        Thank you for taking the Online Examination.</b></span></td>
                                </tr>
                                <tr>
                                  <td class="greybluetext10" bgcolor="#E8EFF7" colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td class="CellTextLight"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                              <td class="ColumnHeader"><table width="100%" border="0" cellspacing="1" cellpadding="3">
                                                  <tr>
                                                    <td height="32" colspan="10" bgcolor="#D1E0EF" class="arial11a"><strong>Response Report</strong></td>
                                                  </tr>
                                                  <tr>
                                                    <td width="5%" class="CellTextLight"><strong>Q.No</strong></td>
                                                    <td width="15%" class="CellTextLight"><strong>Answer</strong></td>
                                                    <td width="5%" class="CellTextLight"><strong>Q.No</strong></td>
                                                    <td width="15%" class="CellTextLight"><strong>Answer</strong></td>
                                                    <td width="5%" class="CellTextLight"><strong>Q.No</strong></td>
                                                    <td width="15%" class="CellTextLight"><strong>Answer</strong></td>
                                                    <td width="5%" class="CellTextLight"><strong>Q.No</strong></td>
                                                    <td width="15%" class="CellTextLight"><strong>Answer</strong></td>
                                                    <td width="5%" class="CellTextLight"><strong>Q.No</strong></td>
                                                    <td width="15%" class="CellTextLight"><strong>Answer</strong></td>
                                                  </tr>
                                                  <?PHP 	
					$sqlct=5;
					$v=$sqlct % 4;					
					$i=1;
					$j=1;
					$attempt_cnt=0;
					$unattempt_cnt=0;
					for ($k = 0; $k < 5; $k++)
  					{
	  					 $sample_answer = trim($_POST["answer".($k+1)]);  
						 $dis_order=$k+1;
						 if($sample_answer=='') {
						  $dis_answer='--';
						  $unattempt_cnt++;
						 }else{
						   $dis_answer=$sample_answer;
						   $attempt_cnt++;
						 }
								 if($i==1){
						  	echo '<tr><td width="5%" class="CellTextLight">'.$dis_order.'</td><td width="15%" class="CellTextLight">'. $dis_answer.'</td>';
							$i++;
							if($j==$sqlct && $v==1)
							{
								echo '<td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  </tr>';
							}
								
						  }else if($i==2){
						  	echo '<td width="5%" class="CellTextLight">'.$dis_order.'</td><td width="15%" class="CellTextLight">'. $dis_answer.'</td>';
							$i++;
							if($j==$sqlct && $v==2)
							{
								echo '<td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
								      <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  </tr>';
							}
								
						  }else if ($i==3){ 						  
						  	echo '<td width="5%" class="CellTextLight">'.$dis_order.'</td><td width="15%" class="CellTextLight">'. $dis_answer.'</td>';
						    $i++;
							if($j==$sqlct && $v==3)
							{
								echo '<td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
								      <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  </tr>';
							}
						  }else if ($i==4){ 						  
								echo '<td width="5%" class="CellTextLight">'.$dis_order.'</td><td width="15%" class="CellTextLight">'.$dis_answer.'</td>';
								$i++;
								if($j==$sqlct && $v==4)
								{
									echo '<td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									      <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
										  </tr>';
								}
						  }
						  else if ($i==5){ 						  	
							   echo '<td width="5%" class="CellTextLight">'.$dis_order.'</td><td width="15%" class="CellTextLight">'.$dis_answer.'</td>';
							   echo '</tr>';
							   $i=1;							   
	   	                  }	
						 $j++; 					  						 					  										
                 	}
					
					
										
					?>
                                              </table></td>
                                            </tr>
                                        </table></td>
                                      </tr>
                                      <tr>
                                        <td class="CellTextLight">&nbsp;</td>
                                      </tr>
                                  </table></td>
                                </tr>
                                <tr>
                                  <td class="greybluetext10" bgcolor="#E8EFF7" colspan="3"><b>Total number of questions attempted:
                                    <?=$attempt_cnt?>
                                  </b></td>
                                </tr>
                                <tr>
                                  <td class="greybluetext10" bgcolor="#E8EFF7" colspan="3"><b>Total number of questions unattempted:
                                    <?=$unattempt_cnt?>
                                  </b></td>
                                </tr>
                            </table></td>
                          </tr>
                      </table></td>
                    </tr>
                  </table>
                  <br>
                  <center>
                  <input class='button' id='sub_form' type='button' value='Get ready to Start the Exam' name="sub_form" onclick="valid()">
			      <br/><span id="submitstatus" class="errormsg"></span>							  
                    </center>
                  <br>
                </td>
              </tr>
            </table>
			<br>
			<center>
			</center><br>
			</td>
		</tr>
	</table>
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<?php include("includes/footer.php");?>
		</tr>
	</table>
	<br>
</form>
<!-- Timer Alert -->
<div id='styled_popup' style='cursor:hand;width: 350px; height: 78px; display:none; position: absolute; z-index: 100; top: -4px; left: -3px;' name='styled_popup'>
  <table width="100%" height="75" border="0" cellpadding="0" cellspacing="0" onclick='javascript:styledPopupClose()'>
    <tr>
      <td bgcolor="#000000"><table width='100%' height="78" border='0' cellpadding='5' cellspacing='1'>
        <tr>
          <td  align="center" bgcolor="#014A7F" class="ColumnHeader1 style3">TIME ALERT</td>
        </tr>
        <tr>
          <td colspan='2' align="center" class="CellTextDark"><span class="style4"><font face="Arial, Helvetica, sans-serif">You have spent much time for reading Instructions/Sample test and will be redirected to the Main exam in 5 Mins</font></span><br/>
            <br/>
              <input name="button" type="button" class="button" onclick='javascript:styledPopupClose()' value=' Close ' />          </td>
        </tr>
      </table></td>
    </tr>
  </table>
</div>
<!-- Timer Alert -->
</center>
</body>
</html>
