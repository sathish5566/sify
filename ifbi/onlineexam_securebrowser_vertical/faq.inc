<table width="95%" border="0" align="left" cellpadding="1" cellspacing="0">
  <tr>
    <td>
	<table width="98%" border="0" align="center" cellpadding="5" cellspacing="0" class="Report_tb">
  <tr>
    <th colspan="4" align="center" class="ColumnHeader1">FAQ'S FOR TEST ADMINISTRATORS</th>
  </tr>
  <tr>
    <td width="19%" align="center" class="ColumnHeader1">Category</td>
    <td width="21%" align="center" class="ColumnHeader1">Event</td>
    <td width="38%" align="center" class="ColumnHeader1">Franchisee</td>
    <td width="22%" align="center" class="ColumnHeader1">FM/BM</td>
  </tr>
  <tr>
    <td align="left" valign="middle">Test Centre  related</td>
    <td align="left" valign="middle">In case a power Shutdown has been declared in a particular locality/city for the day on which the exam is scheduled, what needs to be done?</td>
    <td align="left" valign="middle">In all locations other than Chennai: <br />
      1. Call Control room and report outage schedule <br />
      2. Schedule and run exams on backup power (min 2 hrs) till power supply is restored. <br />
      3. If backup power is drained before supply is restored, then call control room / FM for further instructions.</td>
    <td align="left" valign="middle">1. Report to Control room and ensure that Control room is aware of the outage.<br />
      2. Discuss the outage duration and create schedule of exams that will have to be conducted using backup power.<br />
      3. Have the batteries checked up to ensure that they will take the load for 2 hours<br />
      4. Call local electricity board to find out when the power is likely to be restored.<br />
      6. Keep Franchisees updated about the latest plan</td>
  </tr>
  <tr>
    <td align="left" valign="middle">Test Centre  related</td>
    <td align="left" valign="middle">If the power Fails for more than 2 hours, so that the UPS backup is not sufficient enough for the next shift, what steps need to be taken?</td>
    <td align="left" valign="middle">As above</td>
    <td align="left" valign="middle">As above</td>
  </tr>
  <tr>
    <td align="left" valign="middle">Test Centre  related</td>
    <td align="left" valign="middle">If a permanent PC problem occurs while the candidate is giving an exam, and there are no spare PCs available during that shift, what should the caf&eacute; host do?</td>
    <td align="left" valign="middle">1. Is there any spare PC in the cafe? If yes, assign it to the candidate. <br />
      2. If no, then request the candidate to appear in the next shift. <br />
      3. Talk to your PC vendor to correct the PC problem.<br />
      4. Call up control room and inform .<br />
      5. In the next shift, use &quot;restart exam&quot; option to ensure that a total of 2 hours only is given to the candidate.</td>
    <td align="left" valign="middle">1. Ensure that the affected candidate is not kept waiting beyond the next shift. <br />
      2. Ensure that franchisee talks to PC vendor immediately to correct the PC problem<br />
      3. Check every hour to ensure that it is rectified ASAP.</td>
  </tr>
  <tr>
    <td align="left" valign="middle">Test Centre  related</td>
    <td align="left" valign="middle">If there are browsers using the cafe before the start of the exam or in between shifts</td>
    <td align="left" valign="middle">1.Other browsers should not be permitted in the caf&eacute;  while exams are being conducted.<br />
      2. Ensure that there is a poster outside the caf&eacute; informing about the exam and regret inconvenience.<br />
      3. The poster can be downloaded from the IWFR messaging system</td>
    <td align="left" valign="middle">1. You should ensure that the posters are outside every caf&eacute;. <br />
      2. In case of any complaint of other browsers being present alongwith the IIB test candidates, caf&eacute; will be on Red list with a strong disciplinary action. <br />
      3. Even if there is only 1 IIB candidate , no browser is permitted.<br />
      4. Call up control room and report in case of any violation</td>
  </tr>
  <tr>
    <td align="left" valign="middle">Test Centre  related</td>
    <td align="left" valign="middle">If there is a problem with respect to the printer such as paper shortage, cartridge problem or any other situation in which the candidate is not able to take a print out, what needs to be done?</td>
    <td align="left" valign="middle">1. Please ensure that adequate stocks of cartridges and paper is kept before the commencement of the exam. The likely volume expected is about 30-35 sheets per day of exams.<br />
      2. Ensure that all the printers are in working order<br />
      3. In case of printer problems / or cartridge / paper stock outs still ocuring, take the saved results on a floppy and go to a nearest cafe / photocopier where a working printer is available and take the printout.<br />
      4. Inform control room</td>
    <td align="left" valign="middle">--</td>
  </tr>
  <tr>
    <td align="left" valign="middle">Candidate related</td>
    <td align="left" valign="middle">If all the candidates scheduled for the exam appear in the caf&eacute;, what needs to be done?</td>
    <td align="left" valign="middle">1. Assign PCs to the candidates on a first come basis. <br />
      2. Give extra candidates an option to appear in a later shift. If the candidate agrees to come later, call the control room and inform the membership id and the slot the candidate would now be appearing for. <strong>This is of utmost importance since changes in the backened needs to be made. </strong><br />
      3. In case there are extra candidates in the 3rd shift also, run the 4th shift after confirming with control room.</td>
    <td align="left" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="middle">Candidate related</td>
    <td align="left" valign="middle">What steps need to be taken if the candidate is caught using unfair means?</td>
    <td align="left" valign="middle">1. Talk to the candidate and ask him / her to stop doing the same.<br />
      2. Log in as TA and fill up document &quot;format F&quot; by clicking on the button with the same name.<br />
      3. If the candidate persists using unfaior means - do not allow the candidate to continue for the remainder of the exam and ask him / her to leave.<br />
      4. In case the candidate uses force in appearing for the exam, allow him, but inform control room once the exam is over and fill in the format F for reference.</td>
    <td align="left" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="middle">Candidate related</td>
    <td align="left" valign="middle">In case the candidate appears at the caf&eacute; without a valid admit and/or membership card, what step does the caf&eacute; host need to take?</td>
    <td align="left" valign="middle">1. Do not allow the candidate to appear for the exam. <br />
      2. Login as TA and fill up &quot;format A&quot; by clicking on the button with the same name and in the spaec provided, mention the reason for not allowing the candidate.<br />
      3. Inform Control room</td>
    <td align="left" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="middle">Candidate related</td>
    <td align="left" valign="middle">If the candidate appears late for the exam, what steps need to be taken by the caf&eacute; host?</td>
    <td align="left" valign="middle">1. Do not allow the candidate to appear for the exam, if the candidate is late by more than 15 minutes after the start of the exam. <br />
      2. Login as TA and fill up &quot;format A&quot; by clicking on the button with the same name and in the space provided, mention the reason for not allowing the candidate.<br />
      3. Inform Control room</td>
    <td align="left" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="middle">Network related</td>
    <td align="left" valign="middle">What happens if there is a network failure?</td>
    <td align="left" valign="middle">Cafe host : call Customer Care number and contact control room in Chennai. List of numbers attached. In case numbers are unreachable call up FM / BM</td>
    <td align="left" valign="middle">In case escalated - check with control room. Escalate if required, and follow up on status</td>
  </tr>
  <tr>
    <td align="left" valign="middle">Network related</td>
    <td align="left" valign="middle">If there is a problems with the Caf&eacute; Host being able to login to the system, what needs to be done?</td>
    <td align="left" valign="middle">1. Contact the control room immediately.<br />
      2. Follow instructions as given</td>
    <td align="left" valign="middle">&nbsp;</td>
  </tr>  
  
  
  <tr>
    <td align="left" valign="middle">Network related</td>
    <td align="left" valign="middle">In case there are problems with restarting the exam, what needs to be done?</td>
    <td align="left" valign="middle">1. Try restart by follwing exactly the same procedure as given to you in the booklet.<br />
      2. In case problem persists, contact control room in chennai.<br />
      3. Control room will inform you once the problem has ben resolved. Restart exam after that<br />
      4. In the meanwhile assure the candidate that he / she will get his total 2 hours, and whenever the exam restarts, the full balance time will be alloted to him</td>
    <td align="left" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="middle">Test related</td>
    <td align="left" valign="middle">In case the earlier shift extends into the later shift because of reasons like too frequent restarts, or frequent power cuts etc., what needs to be done?</td>
    <td align="left" valign="middle">1. Report about the issues to Control room.<br />
      2. Co-ordinate with control room to fix the delay period and inform the candidates <strong>AS ADVISED</strong> by control room.</td>
    <td align="left" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="middle">Test related</td>
    <td align="left" valign="middle">What happens if the candidate is unfamiliar with the PC?</td>
    <td align="left" valign="middle">1. Teach the candidate on how to use the mouse, how to open the site, log in and how to click on the radio buttons to record the answer.<br />
      2. In any case, do not tell any answers or help the candidate in actually answering the questions.<br />
      3. In case the candidates are still uncomfortable, ask him to get in touch with Indian Institute of Banking and Finance</td>
    <td align="left" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="middle">Test related</td>
    <td align="left" valign="middle">If the candidate is not able to log in using his membership id and password?</td>
    <td align="left" valign="middle">1. Call control room and report<br />
      2. Follow up every 10 minutes till problem persists and no resolution has been done yet.</td>
    <td align="left" valign="middle">&nbsp;</td>
  </tr> <tr>
    <td colspan="4" align="left" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" align="left" valign="middle" ><table width="100%" border="1" cellpadding="4" cellspacing="0" >
      <tr>
        <td align="center" class="ColumnHeader1">City</td>
        <td align="center" bordercolor="#014A7F" class="ColumnHeader1">PSTN - Customer Care Nos.</td>
        <td align="center" class="ColumnHeader1">UA Number</td>
      </tr>
      <tr>
        <td>Bangalore</td>
        <td>(080) 2289607/2284070</td>
        <td>1901 33 7439</td>
      </tr>
      <tr>
        <td>Calcutta</td>
        <td>(033) 22106065</td>
        <td>1901 33 7439</td>
      </tr>
      <tr>
        <td>Chandigarh</td>
        <td>(0172) 614730</td>
        <td>1901 33 7439</td>
      </tr>
      <tr>
        <td>Chennai</td>
        <td>(044) 22540865</td>
        <td>1901 33 7439</td>
      </tr>
      <tr>
        <td>Cochin</td>
        <td>(0484) 2384417</td>
        <td>1901 33 7439</td>
      </tr>
      <tr>
        <td>Coimbatore</td>
        <td>(0422) 2210830</td>
        <td>1901 33 7439</td>
      </tr>
      <tr>
        <td>Delhi</td>
        <td>(011) 23323789</td>
        <td>1901 33 7439</td>
      </tr>
      <tr>
        <td>Hyderabad</td>
        <td>(040) 27898126/27893198</td>
        <td>1901 33 7439</td>
      </tr>
      <tr>
        <td>Lucknow</td>
        <td>(0522) 2207098</td>
        <td>1901 33 7439</td>
      </tr>
      <tr>
        <td>Mumbai</td>
        <td>(022) 24363973</td>
        <td>1901 33 7439</td>
      </tr>
      <tr>
        <td>Pune</td>
        <td>(020) 6051090</td>
        <td>1901 33 7439</td>
      </tr>
      <tr>
        <td>Trichy</td>
        <td>(0431) 2401070</td>
        <td>1901 33 7439</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="4" align="left" valign="middle">UA Numbers cannot be accessed from Mobiles.All are requested to dial 2 after dialing UA number for contacting control room.</td>
  </tr>
  <tr>
    <td colspan="4" align="left" valign="middle">Only UA Number is to be used for all escalations.This is meant only for exam related help.</td>
  </tr>
  
  
  </table>
	</td>
  </tr>
</table>
