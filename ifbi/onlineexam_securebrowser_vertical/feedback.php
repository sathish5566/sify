<?php
/****************************************************
* Application Name            :  IIB 
* Module Name                 :  Online Exam Page
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  iib_feedback
* Tables used for only selects:  iib_feedback
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  
* Created ON                  :  
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  27-Jan-10
* Description                 :  Interface for feed back
*****************************************************/
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkOnlineExamOther();
$examCode = $_SESSION['ex'];
$subjectCode = $_SESSION['sc'];
$membershipNo = $_SESSION['memno'];
if(isset($_POST['sub_exit']))
{
	header("Location:destroy_login.php");
	exit;
}


$login_process	= getVal($_POST['login_process']);
$system_work	= getVal($_POST['system_work']);
$talogin_issue	= getVal($_POST['talogin_issue']);
$tech_prob		= getVal($_POST['tech_prob']);
$q_rating		= getVal($_POST['q_rating']);
$adeq_time		= getVal($_POST['adeq_time']);
$navigate_issue = getVal($_POST['navigate_issue']);
$rating			= getVal($_POST['rating']);
$feedback		= getVal($_POST['txtfeedback']);


$msg = "";
	if ($rating != "")
	{
		$sqlSelect = "SELECT count(1) from iib_feedback WHERE membership_no='$membershipNo' AND exam_code='$examCode' AND subject_code='$subjectCode' ";
		$resSelect = mysql_query($sqlSelect,$SLAVECONN) or errorlog('err05'," QUERY:$sqlSelect  ".mysql_error($MASTERCONN));  
		list($nRows) = mysql_fetch_row($resSelect);
		if ($nRows == 0)
		{
			$sqlInsert = "INSERT INTO iib_feedback (membership_no,exam_code,subject_code,login_process,system_work,talogin_issue,tech_prob,q_rating,adeq_time,navigate_issue,rating,feedback_text) values ('$membershipNo','$examCode','$subjectCode','$login_process','$system_work','$talogin_issue','$tech_prob','$q_rating','$adeq_time','$navigate_issue','$rating','$feedback') ";
			mysql_query($sqlInsert,$MASTERCONN) or errorlog('err08'," QUERY:$sqlInsert  ".mysql_error($MASTERCONN));
			$msg = "Thank you for your feedback.";
		}
		else
		{
			$sqlUpdate = "UPDATE iib_feedback SET login_process='$login_process',system_work='$system_work',talogin_issue='$talogin_issue',tech_prob='$tech_prob',q_rating='$q_rating',adeq_time='$adeq_time',navigate_issue='$navigate_issue',rating='$rating',feedback_text='$feedback' WHERE membership_no='$membershipNo' AND exam_code='$examCode' AND subject_code='$subjectCode' ";
			mysql_query($sqlUpdate,$MASTERCONN) or errorlog('err06'," QUERY:$sqlUpdate  ".mysql_error($MASTERCONN)); 
			$msg = "Thank you for your feedback.";
		}
	}		

$includeJS = '<script language="javascript" type="text/javascript" src="./includes/browser.js"></script>';
$includeJS .= '<script language="JavaScript" src="./includes/validations.js"></script>';

$includeJS .= <<<JSSC
<script language='JavaScript'>
	function submitForm()
	{
		
		var login_process_opt	= false;
		var system_work_opt		= false;
		var talogin_issue_opt	= false;
		var tech_prob_opt		= false;
		var q_rating_opt		= false;
		var adeq_time_opt		= false;
		var navigate_issue_opt	= false;
		var rating_opt			= false;
		
		// Loop from zero to the one minus the number of radio button selections
		for (counter = 0; counter < document.frmfeedback.login_process.length; counter++)
		{
			// If a radio button has been selected it will return true
			// (If not it will return false)
			if (document.frmfeedback.login_process[counter].checked)
				login_process_opt = true; 
		}		
		if (!login_process_opt)
		{
			// If there were no selections made display an alert box 
			alert("Please select an option for login process.")
			document.getElementById('login_process').focus();
			return (false);
		}

		for (counter1 = 0; counter1 < document.frmfeedback.system_work.length; counter1++)
		{
			if (document.frmfeedback.system_work[counter1].checked)
				system_work_opt = true; 
		}		
		if (!system_work_opt)
		{
			alert("Please select an option for system working.")
			document.getElementById('system_work').focus();
			return (false);
		}

		for (counter2 = 0; counter2 < document.frmfeedback.talogin_issue.length; counter2++)
		{
			if (document.frmfeedback.talogin_issue[counter2].checked)
				talogin_issue_opt = true; 
		}		
		if (!talogin_issue_opt)
		{
			alert("Please select an option for TA login issue.")
			document.getElementById('talogin_issue').focus();
			return (false);
		}

		for (counter3 = 0; counter3 < document.frmfeedback.tech_prob.length; counter3++)
		{
			if (document.frmfeedback.tech_prob[counter3].checked)
				tech_prob_opt = true; 
		}		
		if (!tech_prob_opt)
		{
			alert("Please select an option for Technical Problem.")
			document.getElementById('tech_prob').focus();
			return (false);
		}

		for (counter4 = 0; counter4 < document.frmfeedback.q_rating.length; counter4++)
		{
			if (document.frmfeedback.q_rating[counter4].checked)
				q_rating_opt = true; 
		}		
		if (!q_rating_opt)
		{
			alert("Please select an option for Question rating.")
			document.getElementById('q_rating').focus();
			return (false);
		}

		for (counter5 = 0; counter5 < document.frmfeedback.adeq_time.length; counter5++)
		{
			if (document.frmfeedback.adeq_time[counter5].checked)
				adeq_time_opt = true; 
		}		
		if (!adeq_time_opt)
		{
			alert("Please select an option for time to complete.")
			document.getElementById('adeq_time').focus();
			return (false);
		}

		for (counter6 = 0; counter6 < document.frmfeedback.navigate_issue.length; counter6++)
		{
			if (document.frmfeedback.navigate_issue[counter6].checked)
				navigate_issue_opt = true; 
		}		
		if (!navigate_issue_opt)
		{
			alert("Please select an option for navigation issue.")
			document.getElementById('navigate_issue').focus();
			return (false);
		}

		for (counter7 = 0; counter7 < document.frmfeedback.rating.length; counter7++)
		{
			if (document.frmfeedback.rating[counter7].checked)
				rating_opt = true; 
		}		
		if (!rating_opt)
		{
			alert("Please select an option for rating.")
			document.getElementById('rating').focus();
			return (false);
		}



		val = trim(document.frmfeedback.txtfeedback.value);
		if (val != "")
		{			
			strlen = val.length;		
			if (strlen > 2000)
			{
				alert('Feedback text can contain maximum of only 2000 characters');
				document.frmfeedback.txtfeedback.focus();
				return false;
			}
		}
		document.frmfeedback.submit();
	}
		
	function chkParent(h)
	{		
		/*var g = (navigator.appName == "Netscape") ? 1 : 0;
    	h = (g) ? h : window.event;
   	    if (!g) {
			if (h.clientY < 0) {
				window.opener.location.href="logout.php";
				self.close();			
			}
		}else
		{
			if(document.frmfeedback.action=='')
			{
				window.opener.location.href="logout.php";
				self.close();			
			}
		}*/	
	}	
	function screenclose()
	{
		window.location="destroy_login.php";		
	}
	</script>
JSSC;

$bodyOnload =' onKeyDown="return doKeyDown(event);" ondragstart ="return Stop(event)" onselectstart="return Stop(event)" onmouseup="return Stop(event)"';
include("themes/default/header.php");

?>
 <div id="body_container_inner"><div id="ca_fb_inner_wrapper"><div class="outerbox">
	<div class="innerbox">
    	<div class="innerbox_container_common">
		<?php if ($msg == "") { ?>
		<form name='frmfeedback' method='post'>
        	<div class="pager_header_common">Candidate Feedback</div>
            <div class="content_container_common">
            	<div class="content_container_box_common" >
            	  <div id="ca_fb_inner_box" >
                  <h1>Dear Candidate,</h1>
                  <h2>Please spend a few minutes of your valuable time in giving you feedback on the examination through this online form.</h2>
		<input type=hidden name='membership_no' value='<?=$membershipNo ?>'>
		<input type=hidden name='exam_code' value='<?=$examCode ?>'>
		<input type=hidden name='subject_code' value='<?=$subjectCode ?>'>
         <table width="98%"  border="0" align="center" cellpadding="0" cellspacing="0" id="ca_fb_inner_box_inner_tb" >
              <td width="43%" class="tb_rw1">Login Process was smooth  </td>
              <td width="2%" align="right" class="tb_col">:</td>
              <td width="11%" valign="top" class="tb_rw2"><input type="radio" name="login_process" id="login_process" value="Y" />Yes</td>
              <td width="11%" class="tb_rw2"><input type="radio" name="login_process" id="login_process" value="N" />No</td>
              <td width="10%" class="tb_rw2">&nbsp;</td>
              <td width="13%" class="tb_rw2">&nbsp;</td>
              <td width="10%" class="tb_rw2">&nbsp;</td>
            </tr>
            <tr>
              <td width="43%" class="tb_rw1">System was working fine during the exam</td>
              <td width="2%" align="right" class="tb_col">:</td>
              <td width="11%" class="tb_rw2"><input type="radio" name="system_work" id="system_work" id="radio3" value="Y" />
                <label for="radio3">Yes</label></td>
              <td width="11%" class="tb_rw2"><input type="radio" name="system_work" id="system_work"  value="N" />
                No</td>
              <td width="10%" class="tb_rw2">&nbsp;</td>
              <td width="13%" class="tb_rw2">&nbsp;</td>
              <td width="10%" class="tb_rw2">&nbsp;</td>
            </tr>
            <tr>
              <td width="43%" class="tb_rw1">Did you face any TA login issue</td>
              <td width="2%" align="right" class="tb_col">:</td>
              <td width="11%" class="tb_rw2"><input type="radio" name="talogin_issue" id="talogin_issue"  value="Y" />
                Yes</td>
              <td width="11%" class="tb_rw2"><input type="radio" name="talogin_issue" id="talogin_issue"  value="N" />
                No</td>
              <td width="10%" class="tb_rw2">&nbsp;</td>
              <td width="13%" class="tb_rw2">&nbsp;</td>
              <td width="10%" class="tb_rw2">&nbsp;</td>
            </tr>
            <tr>
              <td width="43%" class="tb_rw1">Did you have any technical problem during the exam</td>
              <td width="2%" align="right" class="tb_col">:</td>
              <td width="11%" class="tb_rw2"><input type="radio" name="tech_prob" id="tech_prob" value="Y" />
                <label for="radio6"></label>Yes<br /></td>
              <td width="11%" class="tb_rw2"><input type="radio" name="tech_prob" id="tech_prob" value="N" />
                No</td>
              <td width="10%" class="tb_rw2">&nbsp;</td>
              <td width="13%" class="tb_rw2">&nbsp;</td>
              <td width="10%" class="tb_rw2">&nbsp;</td>
            </tr>
            <tr>
              <td width="43%" class="tb_rw1">How will you rate the questions</td>
              <td width="2%" align="right" class="tb_col">:</td>
              <td width="11%" class="tb_rw2"><input type="radio" name="q_rating" id="q_rating"  value="easy" />                Easy	       	         	   </td>
              <td width="11%" class="tb_rw2"><input type="radio" name="q_rating" id="q_rating"  value="difficult" />
                Difficult</td>
              <td width="10%" class="tb_rw2"><input type="radio" name="q_rating" id="q_rating"  value="relevant" />
                Relevant</td>
              <td width="13%" class="tb_rw2"><input type="radio" name="q_rating" id="q_rating"  value="not relevant" />
                Not Relevant </td>
              <td width="10%" class="tb_rw2"><input type="radio" name="q_rating" id="q_rating"  value="cant say" />
                Can't say</td>
            </tr>
            <tr>
              <td width="43%" class="tb_rw1">Did you have adequate time to complete the questions </td>
              <td width="2%" align="right" class="tb_col">:</td>
              <td width="11%" class="tb_rw2"><input type="radio" name="adeq_time" id="adeq_time"  value="Y" />
                Yes </td>
              <td width="11%" class="tb_rw2"><input type="radio" name="adeq_time" id="adeq_time"  value="N" />
                No</td>
              <td width="10%" class="tb_rw2">&nbsp;</td>
              <td width="13%" class="tb_rw2">&nbsp;</td>
              <td width="10%" class="tb_rw2">&nbsp;</td>
            </tr>
            <tr>
              <td width="43%" class="tb_rw1">Did you face any issues while navigating the screen to the next question(s)</td>
              <td width="2%" align="right" class="tb_col">:</td>
              <td width="11%" class="tb_rw2"><input type="radio" name="navigate_issue" id="navigate_issue"  value="Y" />
                Yes</td>
              <td width="11%" class="tb_rw2"><input type="radio" name="navigate_issue" id="navigate_issue"  value="N" />
                No</td>
              <td width="10%" class="tb_rw2">&nbsp;</td>
              <td width="13%" class="tb_rw2">&nbsp;</td>
              <td width="10%" class="tb_rw2">&nbsp;</td>
            </tr>
            <tr>
              <td width="43%" class="tb_rw1">Your rating on online exam methodology</td>
              <td width="2%" align="right" class="tb_col">:</td>
              <td width="11%" class="tb_rw2"><input type="radio" name="rating" id="rating" value="excellent" />
                <label for="radio18">Excellent</label></td>
              <td width="11%" class="tb_rw2"><input type="radio" name="rating" id="rating" value="very good" />
                Very Good </td>
              <td width="10%" class="tb_rw2"><input type="radio" name="rating" id="rating" value="good" />
                Good</td>
              <td width="13%" class="tb_rw2"><input type="radio" name="rating" id="rating" value="average" />
                Average</td>
              <td width="10%" class="tb_rw2"><input type="radio" name="rating" id="rating" value="poor" />
                Poor</td>
            </tr>
            <tr>
              <td width="43%" class="tb_rw1">Suggestions</td>
              <td width="2%" align="right" class="tb_col">:</td>
              <td colspan="5" class="tb_rw2"><textarea name="txtfeedback" cols="" rows="" class="tx-box"></textarea></td>
              </tr>
          </table>
          <div class="clear"></div>
          <h3>We value your feedback and would constantly strive to improve our service.</h3>
          <div class="clear"></div>
   </div>
            	</div>
            </div>
            <div class="button_container_common"><input name="sub" class="green_button" type="button" value="Submit" onclick='javascript:submitForm()' />
            </div>
		  </form>
		  <?php }else{ ?>
			<div class="pager_header_common">Candidate Feedback</div>
            <div class="content_container_common">
            	<div class="content_container_box_common" >
            	  <div id="ca_fb_inner_box" >
				  <table width="98%"  border="0" align="center" cellpadding="0" cellspacing="0" id="ca_fb_inner_box_inner_tb" >
              <td width="100%" class="tb_rw1" align="center"><?php print($msg); ?></td>
            </tr>
			</table>
				 </div>
            	</div>
            </div>
		  <?php } ?>
        </div>
    
    </div>
</div></div> 
  </div>
<?php 
include("themes/default/footer.php"); 
//mysql_close($MASTERCONN);
//mysql_close($SLAVECONN);
?>



