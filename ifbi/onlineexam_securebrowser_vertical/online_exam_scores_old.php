<?php
/****************************************************
* Application Name            :  IIB 
* Module Name                 :  Online Exam Page
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  iib_candidate_test,iib_section_questions,iib_section_questions_hindi,iib_case_master,iib_case_master_hindi
* Tables used for only selects:  iib_exam_candidate,iib_exam_subjects
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  
* Created ON                  :  
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  14-Dec-09
* Description                 :  Interface for showing exam result
*****************************************************/
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");   
set_time_limit(0);		
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkCandidateLogin();
$testID = $_POST['testid'];	
checkTestID($testID,$_SESSION['memno']);
require_once("constants.inc");
	
	
	$questionPaperNo = $_SESSION['questionpaperno'];
	$examCode = $_SESSION['ex'];
	$subjectCode = $_SESSION['sc'];
	$membershipNo = $_SESSION['memno'];
	$memberName = $_SESSION['memname'];
	

	$subjectName = $_SESSION['subject_name'];
	$passMark = $_SESSION['pass_mark'];
	$graceMark = $_SESSION['grace_mark'];
	$totalMarks = $_SESSION['total_marks'];
  	$totalTime = $_SESSION['subject_duration'];
  	$display_scr = $_SESSION['display_score'];
  	$display_res = $_SESSION['display_result'];
  	$passmsg = $_SESSION['pass_msg'];
  	$failmsg = $_SESSION['fail_msg'];
  	$resultType = $_SESSION['result_type'];
	
	$memberAddress = $_SESSION['address'];
	$examName = $_SESSION['exam_name'];
	
	$cand_total_time = getVal($_POST['total_time_candi']);
	$timeLeft = getVal($_POST['time_left']);
	$timeTaken = $cand_total_time - $timeLeft;	
	$auto_submit = getVal($_POST['auto_submit']);
	$centreCode = $_SESSION['centrecode'];
	$examDate = date('Y-m-d');
	$examTime = $_SESSION['et'];
	$tmpDate = explode('-',$examDate);
	$tmpTime =  explode(':',$examTime);
	$examDate = $tmpDate[2]."/".$tmpDate[1]."/".$tmpDate[0];
	$examTime = $tmpTime[0].":".$tmpTime[1];
				

	$sqlIWay = "SELECT iway_address1, iway_address2, iway_city, iway_state, iway_pin_code FROM iib_iway_details WHERE centre_code='$centreCode'";
	$resIWay = @mysql_query($sqlIWay,$SLAVECONN)  or errorlog('err05'," iib_iway_details  ".mysql_error($SLAVECONN));
	$row = mysql_fetch_array($resIWay);
	$iwayAddress = "";
	$row['iway_address1'] != "" ? $iwayAddress .= " ".$row['iway_address1'] : $iwayAddress .= "";
	$row['iway_city'] != "" ? $iwayAddress .= " ".$row['iway_city'] : $iwayAddress .= "";
	$row['iway_pin_code'] != "" ? $iwayAddress .= " ".$row['iway_pin_code'] : $iwayAddress .= "";
	$row['iway_state'] != "" ? $iwayAddress .= " ".$row['iway_state'] : $iwayAddress .= "";
	$row['iway_address2'] != "" ? $iwayAddress .= ",Contact No. :  ".$row['iway_address2'] : $iwayAddress .= "";
	
	$datetime = getDateTime();
	$sqlUpdate = "UPDATE iib_candidate_test SET browser_status='closed',test_status='C', end_time='$datetime', last_updated_time='$datetime', time_taken=$timeTaken, time_left=$timeLeft WHERE test_id=$testID";
	$qrystatus=mysql_query($sqlUpdate,$MASTERCONN)  or errorlog('err06'," QUERY:$sqlUpdate  ".mysql_error($MASTERCONN));
 	if($qrystatus && mysql_affected_rows($MASTERCONN)==1)
	{		
			
	/*************************************** SCORE CALCULATION START *****************************************************/ 
  	$score = 0;	

	$sqlInsert = "INSERT INTO iib_candidate_scores (membership_no, exam_code, subject_code, exam_date,time_taken, auto_submit) VALUES ('$membershipNo', '$examCode', '$subjectCode', now(), $timeTaken,'$auto_submit')";
	mysql_query($sqlInsert,$MASTERCONN) or errorlog('err08'," QUERY:$sqlInsert  ".mysql_error($MASTERCONN)); 
			

  /*************************************** SCORE CALCULATION END *****************************************************/ 
 }		
	// Code added to replace the string given in the message
	if($msg != '')
	{	
	      if($resultType == 'PR')
			{
				$Constants_msg=array(
				   "/\bREP_MEMBERSHIPNAME\b/",
				   "/\bREP_SCORE\b/",
				   "/\bREP_TOTALMARK\b/",
				   "/\bREP_SUBJECTNAME\b/"
				 );
				 $replace_msg=array(
				   $memberName,
				   $score,
				   $totalMarks,
				   $subjectName
				);
			}
			elseif($resultType == 'PA')
			{
				 $Constants_msg=array(
				   "/\bREP_MEMBERSHIPNAME\b/",
				   "/\bREP_SCORE\b/",
				   "/\bREP_TOTALMARK\b/",
				   "/\bREP_SUBJECTNAME\b/"
				 );

				// Time array used to replace the constant string present in the message
				$replace_msg=array(
				   $memberName,
				   $score,
					$totalMarks,
				   $subjectName
				 );
			}
			
		    $display_msg=preg_replace($Constants_msg,$replace_msg,$msg); 	
	}
	else
	{
		$not_rep_msg=true;
	}
	if($not_rep_msg)
		$display_msg=$msg;


//To find number of question
$sql_select3 = "SELECT display_order,answer FROM iib_question_paper_details where question_paper_no='$questionPaperNo' order by display_order";
$resSql = @mysql_query($sql_select3,$SLAVECONN)  or errorlog('err05'," SQL:$sql_select3 ".mysql_error($SLAVECONN));
$sqlct=mysql_num_rows($resSql);	

//GET Responce from responce table
$respArr = array();
$sql_select_r = "select display_order,answer from iib_response where id in (select max(id) from iib_response where question_paper_no = '$questionPaperNo' group by question_id) order by display_order";
$resSql_r = @mysql_query($sql_select_r,$SLAVECONN)  or errorlog('err05'," SQL:$sql_select3 ".mysql_error($SLAVECONN));
$sqlct_r=mysql_num_rows($resSql_r);	
if($sqlct_r>0){
	while($row_r=mysql_fetch_array($resSql_r))
	{
		$respArr[$row_r['display_order']] = $row_r['answer'];
	}
}

$_SESSION['score']=$score;


if($_SESSION['exam_type']==2)
	$actionpage="feedback_ing.php";
else
	$actionpage="feedback.php";

//Added for BOI
//Code added to check if all the subjects under the exam code has been completed to enable feedback page
$sqlFeedbackIway="SELECT subject_code , TIME_TO_SEC(exam_time) from iib_candidate_iway where membership_no='$membershipNo' and exam_code='$examCode' order by subject_code";
$resFeedbackIway=mysql_query($sqlFeedbackIway,$SLAVECONN) or errorlog('err05',"SQL:$sqlFeedbackIway ".mysql_error($SLAVECONN));
$cnt_iway=mysql_num_rows($resFeedbackIway);
if($cnt_iway > 1)
{
	while(list($Iway_subCodes , $Iway_eTime)=mysql_fetch_row($resFeedbackIway))
	{
		$Iway_subArr[]=$Iway_subCodes;
		$ETimeArr[$Iway_subCodes]=$Iway_eTime;
	}
}
else
{
	list($Iway_subCodes,$Iway_eTime)=mysql_fetch_row($resFeedbackIway);
	$ETimeArr[$Iway_subCodes]=$Iway_eTime;
}

$sqlFeedbackScores="SELECT subject_code from iib_candidate_scores where membership_no='$membershipNo' and exam_code='$examCode' order by subject_code";
$resFeedbackScores=mysql_query($sqlFeedbackScores,$SLAVECONN) or errorlog('err05',"SQL:$sqlFeedbackIway ".mysql_error($SLAVECONN));
$cnt_scores=mysql_num_rows($resFeedbackScores);
if($cnt_scores > 1)
{
	while(list($Score_subCodes)=mysql_fetch_row($resFeedbackScores))
	{
		$Score_subArr[]=$Score_subCodes;
	}
}
else
{
	list($Score_subCodes)=mysql_fetch_row($resFeedbackScores);
}
$feedback_flag=false;
if(is_array($Iway_subArr) && is_array($Score_subArr))
{
	$SubCodes=array_diff($Iway_subArr,$Score_subArr);   /// Checking Entries present for all the subjects for exam_code in both scoreSUBArray and iwaySUBArr
	if(count($SubCodes) == 0)
		$feedback_flag=true;
	else
		$feedback_flag=false;
}
else
{
	if(($cnt_iway == $cnt_scores) && ($Score_subCodes == $Iway_subCodes))   /// both counts in iway and scores same then all the subjects for the exam is over.
			$feedback_flag=true;
		else
			$feedback_flag=false;
}
unset($_SESSION['testid']);

if($_SESSION['exam_type']==3 && $feedback_flag==false )
	$actionpage="intermediate_section_login.php";

//Added for BOI ends here


	$qry="select img_path,alt_text from iib_logo_master where active='Y'";
	$res = mysql_query($qry,$SLAVECONN) or errorlog('err05'," QUERY:$qry  ".mysql_error($SLAVECONN));
	list($imgpath,$alttext)=mysql_fetch_row($res); 
?>
<html>
<head>
<title><?PHP echo TITLE?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="./includes/browser.js"></script>
<script>	
function feedback()
{
	windowClose = 1;		
	document.feedback.submit();
}

/* Added for IBPS BOI */
function closeParent()
{ 
	windowClose = 1;
	try{ 
	var op = window.opener; 
	op.opener = self; 
	op.close(); 
	} 
	catch(er) 
	{} 
}
function displayScore(memno,examCode,subjectCode,qpno,pmark)
{
	var op="";
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
    }else{// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
	xmlhttp.open("GET",'ajax_scoredetails.php?memno='+memno+'&ecode='+examCode+'&scode='+subjectCode+'&qpno='+qpno+'&pmark='+pmark,false);
	xmlhttp.send();
	var resp = xmlhttp.responseText.split('|');

        var o = document.getElementById("scoredisp");
        if(o.hasChildNodes()) {
		l=o.childNodes.length
		if(l>0)
		for(i=0;i<l;i++) {
			o.removeChild(o.childNodes[0]);
		}
	}
	var td1 = document.createElement('td');
	td1.setAttribute("class", "greybluetext10");
	td1.setAttribute("bgColor", "#E8EFF7");
	td1.innerHTML = '<b>Score</b>';
	var td2 = document.createElement('td');
	td2.setAttribute("class", "greybluetext10");
	td2.setAttribute("bgColor", "#E8EFF7");
	td2.setAttribute("align", "center");
	td2.innerHTML = '<b>:</b>';
	var td3 = document.createElement('td');
	td3.setAttribute("class", "greybluetext10");
	td3.setAttribute("bgColor", "#E8EFF7");
	td3.innerHTML = resp[0];
	o.appendChild(td1);
	o.appendChild(td2);
	o.appendChild(td3);


	/*op += '<td class="greybluetext10" bgcolor="#E8EFF7"><b>Score</b></td>';
	op += '<td class="greybluetext10" bgcolor="#E8EFF7" align="center"><b>:</b></td>';
	op += '<td bgcolor="#E8EFF7" class="greybluetext10">'+resp[0]+'</td>';
	document.getElementById("scoredisp").innerHTML = op;*/
}
function displayResult(memno,examCode,subjectCode,qpno,pmark)
{
	var op1="";
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
    }else{// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
	xmlhttp.open("GET",'ajax_scoredetails.php?memno='+memno+'&ecode='+examCode+'&scode='+subjectCode+'&qpno='+qpno+'&pmark='+pmark,false);
	xmlhttp.send();
	var resp = xmlhttp.responseText.split('|');	
	op1 += '<td class="greybluetext10" bgcolor="#E8EFF7"><b>Result</b></td>';
	op1 += '<td class="greybluetext10" bgcolor="#E8EFF7" align="center"><b>:</b></td>';
	op1 += '<td bgcolor="#E8EFF7" class="greybluetext10">'+resp[1]+'</td>';
	
	document.getElementById("resultdisp").innerHTML = op1;
}

function gotoNextSection(){
//	  closeParent();	 
	//  self.focus();
	 //  self.close();
//		window.open("intermediate_section_login.php","IBPS",'toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=1,maximize=0');	
		window.location=intermediate_section_login.php
		//document.feedback.submit();		
	}
/* Added for IBPS BOI Ends here */
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onKeyDown="return doKeyDown(event);" ondragstart ="return Stop(event)" onselectstart="return Stop(event)" onmousedown="return Stop(event)" onmouseup="return Stop(event)" >
	<form name='feedback' method='post' action='<?PHP echo $actionpage?>'>
	<input type=hidden name='membership_no' value='<?=$membershipNo ?>'>
	<input type=hidden name='exam_code' value='<?=$examCode ?>'>
	<input type=hidden name='subject_code' value='<?=$subjectCode ?>'>
	<!-- Added for BOI -->
	<input type=hidden name='cafeid' value='<?=$centreCode ?>'>
	<!-- Added for BOI Ends here-->
	
	<table width="785" border="0" cellspacing="0" cellpadding="0" align=center >
		<tr> 
			<?php if($imgpath!=''){ ?>
				<td align="center" width="785" valign="top" class="menu"><img src="<?php echo $imgpath; ?>" alt="<?php echo $alttext;?>"></td>
			<?php }else{ ?>
			   	<td align="center" width="785" valign="top" class="menu">&nbsp;</td>
			<?php } ?>
			<!--  <td width="785" align="center" valign="top" bgcolor="#014A7F"><img src="./images/bank_logo/allahabad.jpg" alt="SIFY"></td> -->
		</tr>
		<tr>
			<td width="785" class="ColumnFooter">&nbsp;</td>
		</tr>
</table>
	<table border="0" cellspacing="0" align=center  cellpadding="0" width="785" background="images/tile.jpg">
		<tr>
		  <td align="left"><br>
			<table width="75%" border="0" align="center" cellpadding="1" cellspacing="0">
              <tr>
                <td bgcolor="#014A7F"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                      <td align="center"><table border="0" cellspacing="0" cellpadding="10" width="100%" align="center">
                          <tr>
                            <td colspan="3" class="ColumnHeader1" bgcolor="#D1E0EF"><b> Candidate Exam Result</b></td>
                          </tr>
                          <tr>
                            <td width="35%" class="greybluetext10" bgcolor="#E8EFF7"><b>Membership No</b></td>
                            <td width="2%" class="greybluetext10" bgcolor="#E8EFF7" align="center"><b>:</b></td>
                            <td width="53%" bgcolor="#E8EFF7" class="greybluetext10"><?=$membershipNo ?></td>
                          </tr>
                          <tr>
                            <td class="greybluetext10" bgcolor="#E8EFF7"><b>Name</b></td>
                            <td class="greybluetext10" bgcolor="#E8EFF7" align="center"><b>:</b></td>
                            <td bgcolor="#E8EFF7" class="greybluetext10"><?=$memberName ?></td>
                          </tr>
                          <tr>
                            <td class="greybluetext10" bgcolor="#E8EFF7"><b>Address</b></td>
                            <td class="greybluetext10" bgcolor="#E8EFF7" align="center"><b>:</b></td>
                            <td bgcolor="#E8EFF7" class="greybluetext10"><?=$memberAddress ?></td>
                          </tr>
                          <tr>
                            <td class="greybluetext10" bgcolor="#E8EFF7"><b>Examination</b></td>
                            <td class="greybluetext10" bgcolor="#E8EFF7" align="center"><b>:</b></td>
                            <td bgcolor="#E8EFF7" class="greybluetext10"><?=$examName ?></td>
                          </tr>
                          <tr>
                            <td class="greybluetext10" bgcolor="#E8EFF7"><b>Subject Name</b></td>
                            <td class="greybluetext10" bgcolor="#E8EFF7" align="center"><b>:</b></td>
                            <td bgcolor="#E8EFF7" class="greybluetext10"><?=$subjectName ?></td>
                          </tr>
                          <tr>
                            <td class="greybluetext10" bgcolor="#E8EFF7"><b>Examination Centre</b></td>
                            <td class="greybluetext10" bgcolor="#E8EFF7" align="center"><b>:</b></td>
                            <td bgcolor="#E8EFF7" class="greybluetext10"><?=$iwayAddress ?></td>
                          </tr>
                          <tr>
                            <td class="greybluetext10" bgcolor="#E8EFF7"><b>Examination Date</b></td>
                            <td class="greybluetext10" bgcolor="#E8EFF7" align="center"><b>:</b></td>
                            <td bgcolor="#E8EFF7" class="greybluetext10"><?=$examDate ?></td>
                          </tr>
                          <tr>
                            <td class="greybluetext10" bgcolor="#E8EFF7"><b>Examination Time</b></td>
                            <td class="greybluetext10" bgcolor="#E8EFF7" align="center"><b>:</b></td>
                            <td bgcolor="#E8EFF7" class="greybluetext10"><?php if ($examTime != "") print($examTime." Hrs."); ?></td>
                          </tr>
                          <?php if($display_scr == 'Y')
				{
				?>
                          <tr>
                            <td class="greybluetext10" bgcolor="#E8EFF7" colspan=3><a href="#" Onclick='displayScore(<?php echo '"'.$membershipNo.'"'; ?>,<?php echo $examCode; ?>,<?php echo $subjectCode; ?>,<?php echo $questionPaperNo; ?>,<?php echo $_SESSION['pass_mark']; ?>)'><b><u>Click here to know your Score</u></b></a></td>
                          </tr>
                          <tr id='scoredisp'>
                            
                          </tr>
                          <?
				}
				if($display_res == 'Y')
				{
				?>
                          <tr>
                            <td class="greybluetext10" bgcolor="#E8EFF7" colspan=3><a href="#" Onclick='displayScore(<?php echo '"'.$membershipNo.'"'; ?>,<?php echo $examCode; ?>,<?php echo $subjectCode; ?>,<?php echo $questionPaperNo; ?>,<?php echo $_SESSION['pass_mark']; ?>)'><b><u>Click here to know your Score</u></b></a></td>
                          </tr>
                          <tr id='resultdisp'>
                            
                          </tr>
                          <?}?>
                          <tr>
                            <td class="greybluetext10" bgcolor="#E8EFF7" colspan="3"><!--<b><?//=$display_msg?></b><br> <br>
                 Final results will be displayed once your result is reviewed by IBPS.<br>-->
                                <br />
                                <b>Thank you for taking the Online Examination.</b></td>
                          </tr>
                      </table></td>
                    </tr>
                </table></td>
              </tr>
            </table></td>
		</tr>
		<tr>
		  <td align="left">&nbsp;</td>
	  </tr>
		<tr>
		  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="CellTextLight"><table width="100%" border="1" cellspacing="0" cellpadding="2">
                <tr>
                  <td height="32" colspan="10" bgcolor="#D1E0EF" class="arial11a"><strong>Response Report</strong></td>
                </tr>
                <tr>
                  <td width="5%" class="CellTextLight"><strong>Q.No</strong></td>
                  <td width="15%" class="CellTextLight"><strong>Answer</strong></td>
                  <td width="5%" class="CellTextLight"><strong>Q.No</strong></td>
                  <td width="15%" class="CellTextLight"><strong>Answer</strong></td>
                  <td width="5%" class="CellTextLight"><strong>Q.No</strong></td>
                  <td width="15%" class="CellTextLight"><strong>Answer</strong></td>
                  <td width="5%" class="CellTextLight"><strong>Q.No</strong></td>
                  <td width="15%" class="CellTextLight"><strong>Answer</strong></td>
                  <td width="5%" class="CellTextLight"><strong>Q.No</strong></td>
                  <td width="15%" class="CellTextLight"><strong>Answer</strong></td>
                </tr>
                <?PHP 
					$attempt_cnt=0;
					$unattempt_cnt=0;
					$v=$sqlct % 5;					
					$i=1;
					$j=1;
					while($repData = mysql_fetch_array($resSql))
			  		{			   
						 $dis_order=$repData['display_order'];
						 if($respArr[$dis_order]==''){ 
						  		$dis_answer='--';
							 	$unattempt_cnt++;
						 }else{
						  	 	$dis_answer=$respArr[$dis_order];
						    	$attempt_cnt++;
						  } 
						 if($i==1){
						  	echo '<tr><td width="5%" class="CellTextLight">'.$dis_order.'</td><td width="15%" class="CellTextLight">'. $dis_answer.'</td>';
							$i++;
							if($j==$sqlct && $v==1)
							{
								echo '<td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  </tr>';
							}
								
						  }else if($i==2){
						  	echo '<td width="5%" class="CellTextLight">'.$dis_order.'</td><td width="15%" class="CellTextLight">'. $dis_answer.'</td>';
							$i++;
							if($j==$sqlct && $v==2)
							{
								echo '<td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
								      <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  </tr>';
							}
								
						  }else if ($i==3){ 						  
						  	echo '<td width="5%" class="CellTextLight">'.$dis_order.'</td><td width="15%" class="CellTextLight">'. $dis_answer.'</td>';
						    $i++;
							if($j==$sqlct && $v==3)
							{
								echo '<td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
								      <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									  </tr>';
							}
						  }else if ($i==4){ 						  
								echo '<td width="5%" class="CellTextLight">'.$dis_order.'</td><td width="15%" class="CellTextLight">'.$dis_answer.'</td>';
								$i++;
								if($j==$sqlct && $v==4)
								{
									echo '<td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
									      <td width="5%" class="CellTextLight">&nbsp;</td><td width="15%" class="CellTextLight">&nbsp;</td>
										  </tr>';
								}
						  }
						  else if ($i==5){ 						  	
							   echo '<td width="5%" class="CellTextLight">'.$dis_order.'</td><td width="15%" class="CellTextLight">'.$dis_answer.'</td>';
							   echo '</tr>';
							   $i=1;							   
	   	                  }	
						 $j++; 					  						 					  										
                 	}
					
										
					?>
              </table></td>
            </tr>
            <tr>
              <td class="CellTextLight">&nbsp;</td>
            </tr>
          </table></td>
	  </tr>
		<tr>
			<td class="CellTextLight">
			  <b>Total number of questions attempted:
              <?=$attempt_cnt?>
		    </b></td>
		</tr>
		<tr>
		  <td class="CellTextLight"><b>Total number of questions unattempted:
              <?=$unattempt_cnt?>
		  </b></td>
	  </tr>
		<Tr>
			<td>
			<table border=0 width=640 align=center >
				<tr>
				<td><img src="images/spacer.gif" width=273 height=17></td>  
              		<?php 
              		if($_SESSION['exam_type']==3 && $feedback_flag==false ) //For IBPS BOI
					 {
							$_SESSION['sub_opt']='Y';
							$_SESSION['ta_override']='N';						
					?>
					<td><input name="Submit" type='submit' class='button' id='ClearFrm' value='Start Optional Subject'></td>		
					<? }else if($_SESSION['exam_type']==4) { //For SBI
					?>
					<td background="images/exit.gif"><a href="javascript:feedback()"><img src="images/spacer.gif" width=27 height=17 border=0></a></td>										
				<?php }else {?>					
					<?PHP if($_SESSION['display_print'] == 'Y'){ ?>
					<td background="images/print.gif"><a href="javascript:window.print()"><img src="images/spacer.gif" width=38 height=17 border=0></a></td>
					<?PHP } ?>
					<td background="images/exit.gif"><a href="javascript:feedback()"><img src="images/spacer.gif" width=29 height=17 border=0></a></td>
					<?php
					}
					?>
				<td><img src="images/spacer.gif" width=273 height=17></td>																
				</tr>
			</table>			</td>
		</Tr>
		<tr>
			<td><br><br></td>
		</tr>
	</table>
	<table width="100%" height="23" border="0" align=center cellpadding="0" cellspacing="0">
		<tr>
			<td width="824" height="20" align="left" bgcolor="#014A7F" class="whitecolor"><?PHP echo date('Y')?>, SIFY &nbsp;</td>
		</tr>
</table>
	<br>
	</form>
</body>
</html>
