<?php /****************************************************
* Application Name            :  IIB 
* Module Name                 :  Exam Module
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  iib_candidate,iib_exam_subjects,iib_candidate_iway,iib_candidate_test
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              : 
* Created ON                  :  
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  22-01-2010
* Description                 :  Interface for the TA to candidate  view score card
*****************************************************/
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkTALogin();
require("constants.inc");

$commonDebug = false;
$errormsg = "";
$membershipNo = getVal($_POST['memno']);

$todaydate = date("Y-m-d");
$todayStart = $todaydate." 00:00:00";
$todayEnd = $todaydate." 23:59:59";
if ($membershipNo != "")
{
	$sqlSelect = " SELECT TIME_TO_SEC(DATE_FORMAT(last_updated_time,'%T')) ".
		" - TIME_TO_SEC(DATE_FORMAT(start_time,'%T')), test_id, total_time, test_status,exam_code ".
		" FROM iib_candidate_test WHERE membership_no='$membershipNo' AND ".
		" last_updated_time between '$todayStart' AND '$todayEnd' ".
		" order by last_updated_time desc LIMIT 1 ";
	if ($commonDebug)
	{
		print("\n".$sqlSelect);
	}		
	$result = @mysql_query($sqlSelect,$SLAVECONN) or errorlog('err05'," iib_candidate_test  ".mysql_error($SLAVECONN));		
	if (mysql_num_rows($result) > 0)
	{
		list($timeTaken, $testID, $totalTime, $testStatus, $ExamCode) = mysql_fetch_row($result);
		if ($testStatus == 'C')
		{
			
		}
		else
		{
			$errormsg = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please finish the exam before viewing the score card.";
		}
	}
	else
	{
		
		$errormsg = "This candidate does not have an exam scheduled today or has not taken the exam";
	}
}
else
{
	if ($_POST['submitted'] == 'Y')
	{
		$errormsg = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Membership No. found empty";
	}
}

$includeJS = '<script language="javascript" src="./includes/validations.js"></script>';
$includeJS .= '<script language="javascript" type="text/javascript" src="./includes/browser.js"></script>';
$includeJS .= <<<JSSC
<script language="javascript">
window.name = 'viewcard'
function validate()
{
	Memno=document.loginfrm.memno.value;
	Memno=trim(Memno);

	if (Memno == '')
	{
		alert('Please enter Member ID!');
		document.loginfrm.memno.focus();
		return;
	}	
	if (Memno.indexOf(' ') != -1)
	{
		alert('Member ID Cannot Contain Space');
		document.loginfrm.memno.focus();
		return;
	}
	document.forms[0].action="$_SERVER[PHP_SELF]";
	document.forms[0].target = '_self';
	document.loginfrm.memno.value=Memno;	
	document.forms[0].submit();	
}

function captureEnter()
{
	if (window.event.keyCode == 13)
	{
		document.forms[0].action="$_SERVER[PHP_SELF]";
		document.forms[0].target = '_self';
		document.forms[0].submit();		
	}
}

function setFocus()
{
	document.forms[0].memno.focus();		
}
</script>
JSSC;

	$cond = "'".($_POST['submitted'] != "Y") ? ("onLoad='javascript:setFocus()'") : ("")."'";
	$bodyCont = " onKeyPress='javascript:captureEnter()' $cond";
	
	$bodyOnload = $bodyCont;
	include("themes/default/header.php");
?>
<div id="body_container_inner">
      <div class="innerbox_container_calogin_wrapper"><div class="outerbox">
	<div class="innerbox">
    	<div class="innerbox_container_calogin">
			<div class="pager_header_common">View Score Card</div>
            <div class="content_container_calogin">
            	<!--<div class="content_container_box_calogin" >-->
				<form name='loginfrm' method="post">
					<input type="hidden" name="hid_lefttime">
					<input type=hidden name='submitted' value='Y'>
            	 <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" id="content_container_box_calogin_inner_tb" >
				  <tr>
					<td width="37%"><label>Membership Number</label></td>
					<td width="63%">
					<input class="textbox" name="memno" type="text" id="memno" value="<?php echo $membershipNo ?>" onkeydown='javascript:captureEnter()' onkeypress="return letternumber(event)" maxlength="20" />
					</td>
				  </tr>				  
				 </table> 
			 </form>
            	<!--</div>-->
            </div>
            <div class="button_container_common">                                
			
			<input name="sub_view" id='reset3' class="green_button" type="button" value="View"  style="margin-right:6px;" onclick="javascript:validate();" />
			<input name="sub_view2" id='sub_view' class="green_button" type="button" value="Back" onclick="javascript:window.location='ta_options.php'" />
			<input type='hidden' name='ta_override' value='Y'>
			<br/>
            </div>
        </div>
		<div class="errMsg"><?php if($errormsg != "") echo "$errormsg</br>";?></div>
    </div>
</div></div>
    </div>
<?php
if (($errormsg == "") && ($membershipNo != ""))
{
	$sqlType="SELECT exam_type FROM iib_exam WHERE exam_code=$ExamCode";
	$resType = @mysql_query($sqlType,$SLAVECONN)  or errorlog('err05'," view_score_card.php QUERY:$sqlType  ".mysql_error($SLAVECONN));	
	$rowType = mysql_fetch_array($resType);

	
	if($rowType['exam_type']==2)
		$actionpage="display_score_card_ing.php";
	else
	 $actionpage="display_score_card.php";
	 	
//header("Location:$actionpage");
//exit;

echo <<<lines
<script language='JavaScript'>
	document.loginfrm.action = '$actionpage';
	document.loginfrm.submit();
</script>
lines;
exit;
}
?>
<?php include("themes/default/footer.php");
//mysql_close($SLAVECONN);
?>
