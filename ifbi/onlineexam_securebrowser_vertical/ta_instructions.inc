<u><b>Instructions to Test Administrators for Conduct of On-line Examination:</b></u> 
<br/><br/>
<b>General:</b>
<br/><br/>
The guide is intended to highlight the key procedures and help the TA (Test Administrator) to ensure that exam sessions go as smoothly as possible
<br/><br/>
We urge the TA to read this completely. TA's participation and commitment is essential to the success of the Institute's examination process.
<br/><br/>
TA's must have a strong sense of customer service and at all times represent the IBPS in a professional manner.
<br/><br/>
One of the main tasks as a TA is to greet and direct students, sign them in, and respond to general enquiry before the commencement of the examination. When students arrive the TA must assist them by guiding them to the appropriate seat, answering their questions, or just being friendly face among a group of very stressed students about to appear an examination. TA's are present to act as problem solvers and to ensure that the students are treated in a courteous and professional manner. This may be the only interaction the students have with IBPS, so it is essential we use this opportunity to create a positive image of the IBPS in the minds of our students. IBPS requires our TA's to have the following attributes and / or skills
<ul>
  <ul>
    <ul>
      <li> Professionalism</li>
      <li> Good Judgement </li>
      <li> Organizational </li>
      <li> Calm Nature </li>
      <li> Friendliness </li>
      <li> Able to Maintain Confidentiality </li>
      <li> Customer Focus </li>
      <li> Problem Solvers </li>
      <li> Respectfulness </li>
      <li> Flexibility </li>
      <li> Ethical Behavior </li>
      <li> Adaptability </li>
    </ul>
  </ul>
</ul>
<p>Because our members are professionals and customer service experts themselves, they expect a high degree of professionalism and service from us. Our TA's must appear and act as professionals at all the times.  			
  <br/>
  <br/>
    <b>Detailed guidelines/instructions for the TA s about the conduct of examination are as under </b>
    <br/>
  <br/>
<ol>
<li>The TA will visit the Examination Centre on previous day of the examination and ensure that all the systems at the centre are ready in all respects for conduct of examination e.g. PCs, UPS, Printers, network, power source/backup are working and in good working condition. In particular, the TA will ensure that there is no possibility of downloading questions/answers, and there are no memory resident programs in the system, which may affect the conduct of the test. He will also ensure that candidates will not able to access any sites during exam, copy, cut, paste, Save, Save As functions are disabled in the system during examination.</li><li>
Before the commencement of the examination the TA should ensure that all the other programs, network connections except examination system are disabled.
</li><li>
3.	TA should ensure that there are no charts or table or any content displayed in the examination hall or gadgets, which may prove of assistance to candidates in answering questions.
</li><li>
<B>The examination centres should be sealed and no person other than the eligible candidates should be allowed to enter the examination centre.</B>
</li><li>
On the day of the Examination, the TA would verify the identity of the candidate from the Membership Identity Card, Admit Card and allow only eligible candidates to appear at the examination.  Cases with discrepancies should be referred to the Institutes Corporate Office in the <b>Format A</b>, for not allowing such candidates to enter the I Ways to appear at the examination. This format is to be prepared in each case in triplicates and to be signed by the candidate and TA. One copy of this format is to be given to the candidate and the original copy to the Institute by the TA. The third copy be retained Sify Ltd. / TA.
</li><li>
TA should ensure that no candidates keeps books, notes, Mobile phone etc. with him / her in the examination hall or use mathematical / log tables etc., during the examination.
</li><li>
	The TA will have to get signatures of the candidates on attendance sheets. 
</li>
<li>
TA will brief the candidates about the process of on-line examination in detail and TA will also announce the following to the candidates 
<ul>
  <ul>
    <li>Not to click on the Browser BACK button during the exam.</li>
    <li>Not to refresh the Page</li>
    <li>Inform TA in case if case of any issues related to connectivity or any error message like <span style="color:#FF0000">"Due to connectivity issue. Your previous question answer is not saved"</span>. </li>
    <li>The Candidate will not be able to re-appear for the Sample Test.</li>
    <li>Candidate cannot submit the exam unless the minumum time (3/4 of exam duration) Mins is completed.</li>
    <li>The Candidate should use only Internet Explorer browser for taking the exam.</li>
    <li>If the candidate selects the Medium of exam as �Hindi� in the beginning, he/she will not be able to change the medium.</li>
    <li>Inform TA in case the candidate gets message as "You are not scheduled to take the exam"</li>
    <li>Inform TA in case there are any interruptions in the system due to network issue / system hanging / Mouse not working / Monitor display problem etc  to address the problem and help in continuing the exam.</li>
  </ul>
</ul>
</li><li>
After the Instructions are given to candidates and their queries are answered, Candidates are to be given sample test provided in the system in the next 15 minutes to enable them to get acquainted with the system. Queries raised by candidates should be answered during this time as well.
</li><li>
TA will ensure that all queries from the candidates are answered. TA should note that, the Candidates have been informed that any queries during actual examination will not be entertained, however in exceptional cases necessary clarifications may be given to the candidates.  
</li><li>
Specific announcement is required to be made by the TA in relation to the process of negative marking. In this regard inform the candidates that they need not answer a particular question in case they are not sure about the answer so that the impact of negative marks could be avoided. Specific instructions are to be given in this regard to the candidates as to how they can correct / cancel the answer wrongly marked. 
</li><li>
It is possible that some candidates are not familiar with computers and mouse clicking , In such cases the TA may identify such candidates and explain to them the log-in procedure as well as the technique of 'Mouse-clicking'
</li><li>
The TA will log on to the system and hand over the same to the candidate for the purpose of the test. TA should ensure that no exposure is made to the candidate while entering his log in and password.
</li><li>
Once the candidate logs on, his / her membership number, name, and the subject detail will appear on the screen. TA should check the correctness of the membership number with the membership number written on the ID Card /Admit Card of the candidate.
</li><li>
Once the examination starts, the question paper will be displayed on the screen as generated by the host-end server/system.
</li><li>
The TA will arrange to generate a score card for each candidate after the test is completed and hand over the same to the candidate against acknowledgement.
</li><li>
In case the Internet Connection gets disconnected, the candidate has to appear for the balance number of questions (which will be sent by the host server afresh taking into consideration the criteria for selection of questions as well as the questions which the candidate has already attempted). This facility would be available to the candidate which he will have to avail on the same day failing which it will presumed that the candidate has completed answering the questions which he attempted earlier and accordingly a score card will be generated. In such cases the candidates have to wait at least for 30 minutes.  
</li></ol>
<b>Back up sheet:</b></br><br/>
The Test Administrator should provide the candidates with a back up sheets which can be plain papers for candidates to write down the answers that they have mouse clicked. Candidates should record the options mouse clicked against the related question number in the back up sheet.  The back up sheet may be used in case there is a power failure or the computer shuts down due to some technical reason to record the mouse clicks for options selected by the candidate for the respective questions. 
<br/><br/>
<b>Use of calculators:</b></br><br/>
Candidates appearing at the examinations of the Institute are permitted to use calculators in the subjects 'Basic Accountancy' and 'Introduction to Computers' and calculators should:
Be silent in operation<br/>
Be battery-operated<br/>
Not be capable of being programmed by the insertion of tapes or otherwise<br/>
Not have print out facilities<br/>
Be pocket size and fully portable<br/>
Be of the type up to 6 functions and 12 digits<br/>
Not be capable of retaining the data in a memory bank when they are switched off.<br/>
Calculators, which do not comply with the above conditions and found in possession of the candidates during the examination will be confiscated by the TA and returned to the candidate at the end of the examination. In case the candidate is found using the calculators not complying with the about conditions in the course of the examination, it would amount to adoption of unfair practices and the Institute shall take such action against the candidates as may be deemed appropriate.
<br/><br/>
<b>Unfair practices</b></br></br>
TA should maintain constant supervision over the candidates so as to prevent the candidates from communicating with one another or with persons outside the hall or from referring to books and papers or in any way obtaining or giving unfair assistance. 
</br>
During the course of examination, candidates will be allowed to attend to call of nature. But the cases have been observed where candidates have gone to the toilet during the course of the examination and utilized the opportunity to refer to material hidden in their pockets or elsewhere, or made available to them by someone else outside the examination hall.  It is therefore, necessary to keep a careful watch on such candidates.  When the candidate is permitted to go to the toilet during the examination and if the candidate takes a long time to return to his seat, he should be asked to give reasons for the delay to the satisfaction of the TA.
<br/><br/>
If, in the course of the supervision, TA finds any candidate having resorted to unfair means, he/she should have the fact recorded in a statement in the presence of the candidate. The candidate's signature should be obtained on the statement. Where the candidate refuses to sign the statement, a suitable record to that effect should be made in the statement.  The TA shall have the power to discontinue the examination of such candidates as also of those, who misbehave themselves.
<br/>
The candidate is not precluded from asking queries to the superintendent, but the latter will not give any information touching on the subject matter of any question.
<br/>
In case the TA recommends cancellation of examination at the Centre, due to adoption of malpractice on mass scale by the candidates, the TA should fill <b>format F</b>, in his letter of recommendations for such cancellation, information on the following points:
<ul>
  <ul>
    <li> The date and session in which the incidents of adoption of malpractice were noticed. </li>
    <li> The name of the subject. </li>
    <li> I way address. </li>
    <li> The names, Membership Numbers, addressees of candidate who were responsible for adoption of unfair means if available. </li>
    <li> Material found in possession to be confiscated and forwarded to the IBPS along with the report. </li>
  </ul>
</ul>

The nature and type of unfair means adopted by the candidates. For e.g. any outside source helped the candidates, if solved papers are supplied and circulated inside the examination hall, use of books/notes/other written material by the candidates, creating chaotic conditions at examination centre by the candidates or by the outsiders, disobedience by the candidates in the examination hall etc.
<br/>
Any documentary evidence collected in support of proving adoption of malpractice should be forward to the Institute with a specific recommendation of the TA's.
<br/>
TA will also prepare a Certificate as per <b>FORMAT E</b>, certifying that the examination has been conducted strictly as per the instructions given by the Institute. 
All the Format A, B, C, D should be sent to the Institute in a sealed packet along with the attendance sheet of the candidates immediately after the examination by courier/speed post.
<br/><br/><br/>

<h2>Error message due to intermittent loss of connectivity </h2><br/><img src="tab/local/tab_clip_image002.jpg" alt="Image" />