<?php 
require_once("dbconfig.php");
require_once("login_tracking_functions.php");
masterConnect();
slaveConnect();
$membershipNo = $_SESSION['memno'];
$subjectcode = $_SESSION['sc'];
$examcode = $_SESSION['ex'];		
$mediumcode = $_SESSION['mc'];	
$qpno =  $_SESSION['questionpaperno'];
$answer_shuffling = $_SESSION['answer_shuffling'];
$arrQuestionDetails=array();
$tableName = "iib_section_questions";
$tableName_case = "iib_case_master";
$arrQPUnicode=array();
$arrCaseUnicode=array();
$qryUnicode="select question_id,question_text,option_1,option_2,option_3,option_4,option_5,lang_code from iib_section_questions_unicode where exam_code='$examcode' and subject_code='$subjectcode'";
$unicoderesult=mysql_query($qryUnicode,$SLAVECONN) or errorlog('err05'," QUERY:$qryUnicode ".mysql_error($SLAVECONN));
while($objUnicodeResult=mysql_fetch_object($unicoderesult)){
	$arrQPUnicode[$objUnicodeResult->question_id][$objUnicodeResult->lang_code]['qtext']=$objUnicodeResult->question_text;
	$arrQPUnicode[$objUnicodeResult->question_id][$objUnicodeResult->lang_code]['opt1']=$objUnicodeResult->option_1;
	$arrQPUnicode[$objUnicodeResult->question_id][$objUnicodeResult->lang_code]['opt2']=$objUnicodeResult->option_2;
	$arrQPUnicode[$objUnicodeResult->question_id][$objUnicodeResult->lang_code]['opt3']=$objUnicodeResult->option_3;
	$arrQPUnicode[$objUnicodeResult->question_id][$objUnicodeResult->lang_code]['opt4']=$objUnicodeResult->option_4;
	$arrQPUnicode[$objUnicodeResult->question_id][$objUnicodeResult->lang_code]['opt5']=$objUnicodeResult->option_5;
}
$qryCaseUnicode="select case_id,case_text,lang_code from iib_case_master_unicode where exam_code='$examcode' and subject_code='$subjectcode'";
$unicodeCaseresult=mysql_query($qryCaseUnicode,$SLAVECONN) or errorlog('err05'," QUERY:$qryCaseUnicode ".mysql_error($SLAVECONN));
while($objUnicodeCaseResult=mysql_fetch_object($unicodeCaseresult)){
	$arrCaseUnicode[$objUnicodeCaseResult->case_id][$objUnicodeCaseResult->lang_code]['casetext']=$objUnicodeCaseResult->case_text;
}
	
$qrysubject="select question_display,display_noofquestions,display_casequestion from iib_exam_subjects where exam_code='$examcode' and subject_code='$subjectcode'";
$subjectresult=mysql_query($qrysubject,$SLAVECONN) or errorlog('err05'," QUERY:$qrysubject  ".mysql_error($SLAVECONN));
list($question_display,$display_noofquestions,$display_casequestion)=mysql_fetch_row($subjectresult);

$arrQuestionType=array();
$qrySecQuestion="select question_id,question_type from iib_section_questions where exam_code='$examcode' and subject_code='$subjectcode'";
$sectionresult=mysql_query($qrySecQuestion,$SLAVECONN) or errorlog('err05'," QUERY:$qrySecQuestion  ".mysql_error($SLAVECONN));
while($objSectionResult=mysql_fetch_object($sectionresult)){
	$arrQuestionType[$objSectionResult->question_id]=$objSectionResult->question_type;
}

$str='';
$secMarks = '';
$secName = '';
$btnValue='';
$tagBtn='';
$eraseBtn='';
$tmp=$str;
//$str.="<table bgcolor='#E8EFF7' border='0' cellspacing='1' cellpadding='3' valign=top width=100%>";
$alpha = array('1', '2', '3', '4', '5');	
$arraySection=array();
	/* Sections based on subjectcode */
	$qrySecques="select section_code,section_name,section_type from iib_subject_sections where subject_code='$subjectcode' and online='Y'";
	$resSec = mysql_query($qrySecques,$SLAVECONN)  or errorlog('err05'," QUERY:$sel_sec  ".mysql_error($SLAVECONN));
	$numresSec = mysql_num_rows($resSec);
	if($numresSec> 0)
	{
		 while(list($section_code,$section_name) = mysql_fetch_row($resSec))
		 {
				$arraySection[$section_code] = $section_name;
		 }
	}				

	/* Case Questions */
	$arrCaseID=array();
	$arrDisporderCase=array(); 
	if($question_display == 'O'){
		$caseStarttext="<div class='groupQues_v container_v'><div id='casetitletext' class='groupQuesTitle_v'>Group Question.</div>\n";
		$sel_caseques=" select A.case_id,A.case_text,A.case_marks,A.section_code from $tableName_case A JOIN iib_section_questions B ON "
							  ." A.exam_code = B.exam_code and A.subject_code=B.subject_code and A.section_code = B.section_code and "
							  ." A.case_id = B.case_id where A.exam_code='$examcode' and A.subject_code='$subjectcode' " 
							   ." and A.is_active='1' group by A.case_id ";
		$sql_case = mysql_query($sel_caseques,$SLAVECONN)  or errorlog('err05'," QUERY:$sel_caseques  ".mysql_error($SLAVECONN));
		$num_caseques = mysql_num_rows($sql_case);
		if($num_caseques > 0){
			while(list($case_id,$case_text,$case_marks,$case_section) = mysql_fetch_row($sql_case)){
								
				$_case_text='';
				for($i=0;$i < $LangCount;$i++){
					$lang=$arrLanguage[$i];
					if($lang == 'EN'){
						$_case_text.="<span name=qcaseEN$case_id id=qcaseEN$case_id class='groupQuesPara_v'>$case_text</span>";
					}else if ( ($lang== "HISU") || ($lang == "HISH") || ($lang == "HIDV") )	{
						$_case_text.="<span  name=qcase$lang$case_id id=qcase$lang$case_id class='groupQuesPara_v'><font class='ans'>".$arrCaseUnicode[$case_id][$lang]['casetext']."</font></span>";
					}else{
						$_case_text.="<span  name=qcase$lang$case_id id=qcase$lang$case_id class='groupQuesPara_v'>".$arrCaseUnicode[$case_id][$lang]['casetext']."</span>";
					}
				}	
				$case_array[$case_id]['case_id'] = $case_id;
				//$case_array[$case_id]['case_text'] = replaceStartTags($_case_text);
				$case_array[$case_id]['case_text'] = replaceStartTags(html_entity_decode($_case_text,ENT_NOQUOTES));
				$case_array[$case_id] ['case_marks'] = $case_marks;
				$case_array[$case_id] ['case_section'] = $case_section;							
				$caseStarttext.="<div style='clear: both;'  id='caseid$case_id' class='casetext_v'>\n";				
				$caseStarttext.=$case_array[$case_id]['case_text'];
			    $caseStarttext.="</div>\n";					
			}
		}
        $caseStarttext.='</div>';
		if($num_caseques > 0){
            $tmp=$caseStarttext;
        }else
            $caseStarttext='';    
	}else{
		$sel_caseques=" select A.case_id,A.case_text,A.case_marks,A.section_code from $tableName_case A JOIN iib_section_questions B ON "
							  ." A.exam_code = B.exam_code and A.subject_code=B.subject_code and A.section_code = B.section_code and "
							  ." A.case_id = B.case_id where A.exam_code='$examcode' and A.subject_code='$subjectcode' " 
							   ." and A.is_active='1' group by A.case_id ";
	
		$sql_case = mysql_query($sel_caseques,$SLAVECONN)  or errorlog('err05'," QUERY:$sel_caseques  ".mysql_error($SLAVECONN));
		$num_caseques = mysql_num_rows($sql_case);
		if($num_caseques > 0){
			while(list($case_id,$case_text,$case_marks,$case_section) = mysql_fetch_row($sql_case)){
				$_case_text='';
				for($i=0;$i < $LangCount;$i++){
					$lang=$arrLanguage[$i];
					if($lang == 'EN'){
						$_case_text.="<span name=qcaseEN$case_id  id=qcaseEN$case_id>$case_text</span>";
					}else if ( ($lang== "HISU") || ($lang == "HISH") || ($lang == "HIDV") )	{
						$_case_text.="<span name=qcase$lang$case_id id=qcase$lang$case_id><font class='ans'>".$arrCaseUnicode[$case_id][$lang]['casetext']."</font></span>";
					}else{
						$_case_text.="<span name=qcase$lang$case_id id=qcase$lang$case_id>".$arrCaseUnicode[$case_id][$lang]['casetext']."</span>";
					}
				}
				
				$case_array[$case_id]['case_id'] = $case_id;
				//$case_array[$case_id]['case_text'] = replaceStartTags($_case_text);
				$case_array[$case_id]['case_text'] = replaceStartTags(html_entity_decode($_case_text,ENT_NOQUOTES));
				$case_array[$case_id] ['case_marks'] = $case_marks;
				$case_array[$case_id] ['case_section'] = $case_section;							
			}
		}
	}	
    /* Section Questions */
	$sel_section_ques="select question_id,question_text,option_1,option_2,option_3,option_4,option_5,marks,case_id,negative_marks from $tableName where exam_code='$examcode' and subject_code='$subjectcode' order by question_id";
	$sql_section = mysql_query($sel_section_ques,$SLAVECONN)  or errorlog('err05'," QUERY:$sel_section_ques  ".mysql_error($SLAVECONN));
	$num_ques = mysql_num_rows($sql_section);
	if($num_ques > 0)
	{
		 while(list($q_id,$q_text,$q_opt1,$q_opt2,$q_opt3,$q_opt4,$q_opt5,$q_marks,$case_id,$negative_marks) = mysql_fetch_row($sql_section))
		 {
			 $_q_text='';
			 $_q_opt1='';
			 $_q_opt2='';
			 $_q_opt3='';
			 $_q_opt4='';
			 $_q_opt5='';

				 for($i=0;$i < $LangCount;$i++){
					 $lang=$arrLanguage[$i];
					if($lang == 'EN'){
						$_q_text.="<span  id=qtextEN$q_id >$q_text</span>";
						$_q_opt1.="<span  id=qopt1EN$q_id >$q_opt1</span>";
						$_q_opt2.="<span  id=qopt2EN$q_id >$q_opt2</span>";
						$_q_opt3.="<span  id=qopt3EN$q_id >$q_opt3</span>";
						$_q_opt4.="<span  id=qopt4EN$q_id >$q_opt4</span>";
						$_q_opt5.="<span  id=qopt5EN$q_id >$q_opt5</span>";						
					}else if( ($lang== "HISU") || ($lang == "HISH") || ($lang == "HIDV")){
						$_q_text.="<span  id=qtext$lang$q_id ><font class='qn'>".$arrQPUnicode[$q_id][$lang]['qtext']."</font></span>";
						$_q_opt1.="<span  id=qopt1$lang$q_id ><font class='ans'>".$arrQPUnicode[$q_id][$lang]['opt1']."</span>";
						$_q_opt2.="<span  id=qopt2$lang$q_id ><font class='ans'>".$arrQPUnicode[$q_id][$lang]['opt2']."</span>";
						$_q_opt3.="<span  id=qopt3$lang$q_id ><font class='ans'>".$arrQPUnicode[$q_id][$lang]['opt3']."</span>";
						$_q_opt4.="<span  id=qopt4$lang$q_id ><font class='ans'>".$arrQPUnicode[$q_id][$lang]['opt4']."</span>";
						$_q_opt5.="<span  id=qopt5$lang$q_id ><font class='ans'>".$arrQPUnicode[$q_id][$lang]['opt5']."</span>";
					}else{
						$_q_text.="<span  id=qtext$lang$q_id >".$arrQPUnicode[$q_id][$lang]['qtext']."</span>";
						$_q_opt1.="<span  id=qopt1$lang$q_id >".$arrQPUnicode[$q_id][$lang]['opt1']."</span>";
						$_q_opt2.="<span  id=qopt2$lang$q_id >".$arrQPUnicode[$q_id][$lang]['opt2']."</span>";
						$_q_opt3.="<span  id=qopt3$lang$q_id >".$arrQPUnicode[$q_id][$lang]['opt3']."</span>";
						$_q_opt4.="<span  id=qopt4$lang$q_id >".$arrQPUnicode[$q_id][$lang]['opt4']."</span>";
						$_q_opt5.="<span  id=qopt5$lang$q_id >".$arrQPUnicode[$q_id][$lang]['opt5']."</span>";
					}
				 }			 			 		 			 
			 	$section_text_array[$q_id] = replaceStartTags(html_entity_decode($_q_text,ENT_NOQUOTES));
				$option_1_array[$q_id] = replaceStartTags(html_entity_decode($_q_opt1,ENT_NOQUOTES));
				$option_2_array[$q_id] = replaceStartTags(html_entity_decode($_q_opt2,ENT_NOQUOTES));
				$option_3_array[$q_id] = replaceStartTags(html_entity_decode($_q_opt3,ENT_NOQUOTES));
				$option_4_array[$q_id] = replaceStartTags(html_entity_decode($_q_opt4,ENT_NOQUOTES));
				$option_5_array[$q_id] = replaceStartTags(html_entity_decode($_q_opt5,ENT_NOQUOTES));

				$marks_array[$q_id] =$q_marks;
				$negmarks_array[$q_id] =$negative_marks;
				if($case_id != 0)
					$casesection_array[$q_id] = $case_id;
				else
					$casesection_array[$q_id]=0;
		 }
	}	
	$sel_qn="select question_paper_no,question_id,display_order,answer_order,section_code,answer from iib_question_paper_details where question_paper_no = '$qpno' and subject_code='$subjectcode'";
	$res_qn = mysql_query($sel_qn,$SLAVECONN)  or errorlog('err05'," QUERY:$sel_qn  ".mysql_error($SLAVECONN));
	$count_n = mysql_num_rows($res_qn);
	$qp_count = $count_n;
	$i=0;
	while($details_arr=mysql_fetch_assoc($res_qn))
	{		
		$qp_id =$details_arr['question_id'];
		$qp_no = $details_arr['question_paper_no'];		
		$disp_order =$details_arr['display_order']; 
		$ans_order =$details_arr['answer_order'];
		$seccode = $details_arr['section_code'];
		$answer = stripslashes(html_entity_decode($details_arr['answer']));
				
		$qp_text = $section_text_array[$qp_id];
		$opt1 = $option_1_array[$qp_id];
		$opt2 = $option_2_array[$qp_id];
		$opt3 = $option_3_array[$qp_id];
		$opt4 = $option_4_array[$qp_id];
		$opt5 = $option_5_array[$qp_id];
		$marks = $marks_array[$qp_id];
		$negmarks = $negmarks_array[$qp_id];
		$case_id = $casesection_array[$qp_id];
		
		$qustn_arr[$qp_no][$qp_id][0]= $qp_no;
		$qustn_arr[$qp_no][$qp_id][1]= $qp_text;
		$qustn_arr[$qp_no][$qp_id][2]= $opt1;
		$qustn_arr[$qp_no][$qp_id][3]= $opt2;
		$qustn_arr[$qp_no][$qp_id][4]= $opt3;
		$qustn_arr[$qp_no][$qp_id][5]= $opt4;
		$qustn_arr[$qp_no][$qp_id][6]= $opt5;
		$qustn_arr[$qp_no][$qp_id][7]= $qp_id;
		$qustn_arr[$qp_no][$qp_id][8]= $marks;
		$qustn_arr[$qp_no][$qp_id][9]=$ans_order;
		$qustn_arr[$qp_no][$qp_id][10]=$case_id;
		$qustn_arr[$qp_no][$qp_id][11]= $negmarks;
		$qustn_arr[$qp_no][$qp_id][12]= $arraySection[$seccode];							
		$qustn_arr[$qp_no][$qp_id][13]= $answer;
		$qid_array[$qp_no][$disp_order]=$qp_id;		
		$i++;
		if( $case_id != 0 && !in_array($case_id,$arrCaseID) && isset($case_array[$case_id]['case_id'])){
			array_push($arrCaseID,$case_id);
		}	
	}

	//GET ANSWERS FROM RESPONCE TABLE
	$sqlMaxIds = "select  max(id) from iib_response where question_paper_no ='$qpno' group by question_id";
	$resMaxIds = mysql_query($sqlMaxIds);
	$arrMaxId = array();
	while(list($maxId) = @mysql_fetch_row($resMaxIds)) {
		$arrMaxId[] = $maxId;
	}
	$maxIds = implode(',', $arrMaxId);

	if($maxIds == "")
		$maxIds = "''";

	$sel_ans = "select question_paper_no,question_id,answer from iib_response where id in ($maxIds)";
	$res_ans = mysql_query($sel_ans,$SLAVECONN)  or errorlog('err05'," QUERY:$sel_ans  ".mysql_error($SLAVECONN));
	$count_ans = mysql_num_rows($res_ans);
	
	if($count_ans>0){
		while($details_ans_arr=mysql_fetch_assoc($res_ans))
		{		
			$answer = stripslashes(html_entity_decode($details_ans_arr['answer']));
			$qustn_arr[$details_ans_arr['question_paper_no']][$details_ans_arr['question_id']][13]= $answer;
		}
	}


		$question_paper_no = $qpno;
		$j=$question_paper_no;
		$qn_no=0;
		$arrQuestionid=array();
		$countMF=0;
		for($k=1;$k<=$qp_count;$k++) 
		{
			$sectionname='';
			$qid=$qid_array[$j][$k];
			
			$questionText=$qustn_arr[$j][$qid][1];
			
			$questionID=$qustn_arr[$j][$qid][7];
			$questionMarks=$qustn_arr[$j][$qid][8];
			$case_id = $qustn_arr[$qp_no][$qid][10];
			$answer = $qustn_arr[$qp_no][$qid][13];
									
			if($_SESSION['display_sectionname'] == 'Y'){
				$sectionname='Section:&nbsp;'.ucwords($qustn_arr[$qp_no][$qid][12]);
			}else{
				$sectionname='';
			}
			if($qustn_arr[$j][$qid][11]==0){
				$questionNegmarks="&nbsp;";
			}else{
				$questionNegmarks="&nbsp;(Negative Mark/s - ".$qustn_arr[$j][$qid][11].')';
			}						
			if($qn_no==0)
				$str=$tmp.'<div class="sam_quest_text_container" id="q'.$qn_no.'">'."\n";
			else
				$str.='<div class="sam_quest_text_container" id="q'.$qn_no.'">'."\n";

			$str.='<h1><b>'.$sectionname.'</b></h1>';
						
			
			$qno=$qn_no+1;
			$arrDisporderCase[$qn_no]=$case_id;	
			if($question_display != 'O'){					
				if(isset($case_id) && $case_id!=0){
					//$str=$str."<tr>\n<td class='greybluetextquestion' style='font-size: 12px;' bgcolor='#E8EFF7' align=left valign=top colspan=2 ><b>Group Question.</b><br/><br/>".$case_array[$case_id]['case_text']."</td>\n</tr>\n";			
					$str.='<div class="samp_quest_div_quest_text"><p class="greybluetextquestion"><b>Group Question.</b><br/><br/>'.$case_array[$case_id]['case_text'].'</p>'."\n";
				}	
			}		
			$str.="<input type=hidden name=question_id[".$qn_no."] value='$questionID' />\n";
			
			if($_SESSION['display_mark'] == 'Y'){
				//$str=$str."<tr>\n<td class='greybluetext10' bgcolor='#E8EFF7' align=left valign=top nowrap><b>$sectionname</b></td><td class='greybluetext10' bgcolor='#E8EFF7' align=right valign=top nowrap><b>(Mark/s - $questionMarks)$questionNegmarks</b><a name='link$qno' href='#'></a></td>\n</tr>\n";	
				$str .="<div  id=\"mark\">(Mark : <span>$questionMarks)$questionNegmarks</span></div>";
			}
			//$str=$str."<tr>\n<td class='greybluetext10' bgcolor='#E8EFF7' align=right valign=top nowrap colspan=2><a href='javascript:eraseAnswer($qn_no)' class='attempt' style='background-color:#E8EFF7'>Erase Answer</a></b></td>\n</tr>\n";																
		
			if($arrQuestionType[$questionID] == 'Z' ){						
				$title="Match the following (Column B draggable)";
				$str.="<tr>\n<td class='greybluetextquestion' style='font-size: 12px;' bgcolor='#E8EFF7' valign=top align='left'><b>Q".$qno.". ".$title."</b></td>\n</tr>\n";
				$arrColumnA=explode('[separator]',$questionText);
				$colA='';
				$colB='';
				$countMF++;
			}else{
				$str.='<div class="sam_quest_text_inner_container" id="sam_quest_text_inner_container"><span id="samp_quest_div_quest_no">Q'.$qno.'. </span>'."\n";
				$str.='<div id="samp_quest_div_quest_text" class="sam_quest_wid"><p class="greybluetextquestion">'.$questionText.'</p>'."\n</div>";
			}
					
						

			//Code added to display answer shuffled					
			$dis_answer_order=explode(",",$qustn_arr[$j][$qid][9]);						
			if(trim($answer_shuffling)=="N")
			{
					sort($dis_answer_order);
			}						
			$rand_keys = array();
		
			if($arrQuestionType[$questionID] == 'Z' ){						

				//to restore already selected answer for match the following.
				if($answer!='') $dis_answer_order = explode(",",$answer);

				foreach($dis_answer_order as $key=> $i){
						$var = "option".$i;							
						$$var = $qustn_arr[$j][$qid][$i+1];
						if (trim($$var) != ""){
								$ans=$$var;	
							
							$colA.='<li id="'.$key.'">'.$arrColumnA[$key].'</li>';
							$colB.='<li style="cursor:move;zoom:1;" title="click and drag" id="'.$qn_no.'_'.$i.'">'.$ans.'</li>';	
						}															
				}						
				$str.="<tr><td>\n";
				$str.="<table width=\"100%\" border=0 bgcolor=\"#014A7F\">
					   <th class=\"ColumnHeader1\" style=\"color:#FFFFFF\">Column A</th><th class=\"ColumnHeader1\" style=\"color:#FFFFFF\">Column B</th>
					   <tr><td class='greybluetextans' style='font-size: 11px;'  > <div id=\"mflistA\">     
					 	 <ul>".$colA."</ul> 
						</div>
						</td><td class='greybluetextans' style='font-size: 11px;' >
						<div id=\"mflistB\">     
						<ul>".$colB."</ul></div> 
						</td></tr></table>";
			   $str.="</td></tr>\n";	
				$colA='';
				$colB='';	

			}else{
				//Other Question Types
				$str.="<div class=\"sam_ans_text_container\"><ul>\n";
				foreach($dis_answer_order as $key=> $i){
						$var = "option".$i;							
						$$var = $qustn_arr[$j][$qid][$i+1];
						
						if (trim($$var) != "")
						{										
							//$str.="<td class='greybluetextans' style='font-size: 11px;' bgcolor='#E8EFF7' align='left' valign=middle colspan=2>";
							if($arrQuestionType[$questionID] == 'O' )
								$str.="<li class=\"greybluetextans\"><input type='radio' id='answer".$questionID."' name='answer".$questionID."' value='".$i."'onClick='javascript:changeChecked($qn_no)'/>". $alpha[$key].") <label onClick='javascript:changeChecked($qn_no)' dir=ltr>";
							else if($arrQuestionType[$questionID] == 'M' )
								$str.="<li class=\"greybluetextans\"><input type='checkbox' id='answer".$questionID."' name='answer".$questionID."' value='".$i."'onClick='javascript:changeChecked($qn_no)'/>". $alpha[$key].") <label onClick='javascript:changeChecked($qn_no)' dir=ltr>";
					
							$str.="";

							$str.=$$var;	
							$str.="</label></li>\n";
						}

				}
		$str.="</ul>\n";

                                //if($client_bookmark == 'Y'){

                                    if(@in_array($question_id,$marked_array))
                                        $tagByQuesId[$qn_no] = ' De-Tag ';
                                    else
                                        $tagByQuesId[$qn_no] = ' Tag ';
                                    
                                   // $str=$str."<tr>\n<td class='greybluetext10' bgcolor='#E8EFF7' valign=top align='left'>&nbsp; &nbsp; &nbsp;<input type='button' name='mark_btn_$questionID' id='mark_btn_$questionID' class='button' value='$btName' onClick='javascript: tagQ(this, $questionID);'></td>\n</tr>\n";
								 //  $str.='<div class="clear"></div>'."\n";
								//$str.="<div id=\"samp_quest_div_quest_text_btn_div\"><input title='Click to $btName the question' type='button' name='mark_btn_$questionID' id='mark_btn_$questionID' class='yellow_button' value='$btName' onClick='javascript: tagQ(this, $questionID);'><input title='Click to erase the answer' onclick=\"eraseAnswer($qn_no)\" class=\"red_button\" type=\"button\" value=\"Erase Answer\"></div>";
								
								
								/*$tagBtnvalue = "tagBtn".$qn_no;
								$eraseBtnvalue = "eraseBtn".$qn_no;
								$tagBtnvalue ="<div class=\"sam_quest_tag_btn_div\"><input title='Click to $btName the question' type='button' name='mark_btn_$questionID' id='mark_btn_$questionID' class='yellow_button' value='$btName' onClick='javascript: tagQ(this, $questionID);'></div>";
								$eraseBtnvalue ="<div class=\"sam_quest_er_ans_btn_div\"><input title='Click to erase the answer' onclick=\"eraseAnswer($qn_no)\" class=\"red_button\" type=\"button\" value=\"Erase Answer\"></div>";
*/
								//$str.="@T".$tagBtnvalue."<br>@E".$eraseBtnvalue;
                              //}																	
				/* I do not want to answer this question */
				if($_SESSION['donotwanttoanswer'] == 'Y')
					$str.="<li class=\"greybluetextans\"><input type='checkbox' id='donotanswer".$questionID."' name='donotanswer".$questionID."' onClick='javascript:donotanswer($qn_no,$questionID,this)'/><label onClick='javascript:donotanswer($qn_no,$questionID,this)' dir=ltr> I do not want to answer this question";
					//$str=$str."<tr>\n<td class='greybluetextans' style='font-size: 11px;' bgcolor='#E8EFF7' valign=top align='left'><input type='checkbox' id='donotanswer".$questionID."' name='donotanswer".$questionID."' onClick='javascript:donotanswer($qn_no,$questionID,this)'/>&nbsp;I do not want to answer this question</td>\n</tr>\n";
			}				
			
			$arrQuestionid[$qn_no]=$questionID;
			if($answer!=''){
			//	$attempt=1;
				$saveServer=1;
			}else{
			//	$attempt=0;
				$saveServer=0;	
			}
			$arrQuestionDetails[$qn_no]=array(
											 'displayorder' => $qn_no,
											 'questionid' => $questionID,
											 'questiontype' => $arrQuestionType[$questionID], 
											 'caseid' =>$case_id,
											 'answerinserver' => $answer,
											 'savedinserver' => $saveServer,
											 'answerinlocal' => $answer,
											 'lang' => $mediumcode,

										 );
			$qn_no++;
			//$str=$str."</table>\n";
			$str.="</div>\n";
			$str.="</div>\n";
			$str.="</div>\n";									
		}
		//$str=$str."</td></tr>\n";										
		$str.="<input type=hidden name=question_count id=question_count value='$qp_count'>";
		$str.='<script type="text/javascript">'."\n";
		$str.=arrayToJSObject($arrLanguage,'arrLanguage');
		$str.=arrayToJSObject($arrQuestionid,'arrQuestionid');
		$str.=arrayToJSObject($arrDisporderCase,'arrDisporderCase');
		$str.=arrayToJSObject($arrQuestionDetails,'arrQuestionDetails');
		$str.=arrayToJSObject($tagByQuesId,'tagByQuesId');

		if(count($arrCaseID)>0){
			$str.=arrayToJSObject($arrCaseID,'arrCaseid');
			$str.=arrayToJSArray($arrCaseID,'arrCaseid1');
		}
		$str.="</script>\n";				
		//echo $str;
//mysql_close($SLAVECONN);
?>
