<?php
/****************************************************
* Application Name            :  IIB 
* Module Name                 :  Online Exam Page
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  iib_candidate_test,iib_question_paper_details,iib_section_questions,iib_section_questions_hindi,iib_case_master,iib_case_master_hindi
* Tables used for only selects:  iib_exam_candidate,iib_exam_subjects
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  
* Created ON                  :  
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  14-Dec-09
* Description                 :  Interface for showing exam score for ING VYSYA Bank
*****************************************************/
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
require_once("constants.inc");
//checkOnlineExamOther();	
//checkScore();

	$questionPaperNo = $_SESSION['questionpaperno'];
	$examCode = $_SESSION['ex'];
	$subjectCode = $_SESSION['sc'];
	$membershipNo = $_SESSION['memno'];
	$memberName = $_SESSION['memname'];	
	$subjectName = $_SESSION['subject_name'];
	$passMark = $_SESSION['pass_mark'];
	$graceMark = $_SESSION['grace_mark'];
	$totalMarks = $_SESSION['total_marks'];
  	$totalTime = $_SESSION['subject_duration'];
  	$display_scr = $_SESSION['display_score'];
  	$display_res = $_SESSION['display_result'];
  	$passmsg = $_SESSION['pass_msg'];
  	$failmsg = $_SESSION['fail_msg'];
  	$resultType = $_SESSION['result_type'];
	$score = $_SESSION['score'];
	
	$memberAddress = $_SESSION['address'];
	$examName = $_SESSION['exam_name'];		
	$centreCode = $_SESSION['centrecode'];
	$examDate = date('Y-m-d');
	$examTime = $_SESSION['et'];
	$tmpDate = explode('-',$examDate);
	$tmpTime =  explode(':',$examTime);
	$examDate = $tmpDate[2]."/".$tmpDate[1]."/".$tmpDate[0];
	$examTime = $tmpTime[0].":".$tmpTime[1];
	
	$sqlIWay = "SELECT iway_address1, iway_address2, iway_city, iway_state, iway_pin_code FROM iib_iway_details WHERE centre_code='$centreCode'";
	$resIWay = @mysql_query($sqlIWay,$SLAVECONN)  or errorlog('err05'," iib_iway_details  ".mysql_error($SLAVECONN));
	$row = mysql_fetch_array($resIWay);
	$iwayAddress = "";
	$row['iway_address1'] != "" ? $iwayAddress .= " ".$row['iway_address1'] : $iwayAddress .= "";
	$row['iway_city'] != "" ? $iwayAddress .= " ".$row['iway_city'] : $iwayAddress .= "";
	$row['iway_pin_code'] != "" ? $iwayAddress .= " ".$row['iway_pin_code'] : $iwayAddress .= "";
	$row['iway_state'] != "" ? $iwayAddress .= " ".$row['iway_state'] : $iwayAddress .= "";
	$row['iway_address2'] != "" ? $iwayAddress .= ",Contact No. :  ".$row['iway_address2'] : $iwayAddress .= "";
?>
<html>
<head>
<title><?PHP echo TITLE?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="./images/iibf.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="./includes/browser.js"></script>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onKeyDown="return doKeyDown(event);" ondragstart ="return Stop(event)" onselectstart="return Stop(event)" onmousedown="return Stop(event)" onmouseup="return Stop(event)">	
	<table width="640" border="0" cellspacing="0" cellpadding="0" align=center >
		<tr> 
		 <td width="640" align="left" valign="top" bgcolor="#014A7F"><img src="./images/logo1.jpg" alt="IBPS"><img src="./images/logo2.jpg" alt="IBPS" ></td>
		</tr>
		<tr>
			<td width="640" bgcolor="7292C5">&nbsp;</td>
		</tr>
	</table>
	<table border="0" cellspacing="0" align=center  cellpadding="0" width="640" background="images/tile.jpg">
		<tr>
			<td align="left">
			<p>			</p>
			<table border="0" cellspacing="0" cellpadding="10" width="75%" align=center>
              <tr>
                <td colspan="3" class="greybluetext10" bgcolor="#D1E0EF"><b class="arial11a">Candidate Exam Result </b></td>
              </tr>
              <tr>
                <td width="35%" class="greybluetext10" bgcolor="#E8EFF7"><b>Membership No</b></td>
                <td width="2%" class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
                <td width="53%" bgcolor="#E8EFF7" class="greybluetext10"><?=$membershipNo ?></td>
              </tr>
              <tr>
                <td class="greybluetext10" bgcolor="#E8EFF7"><b>Name</b></td>
                <td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
                <td bgcolor="#E8EFF7" class="greybluetext10"><?=$memberName ?></td>
              </tr>
              <tr>
                <td class="greybluetext10" bgcolor="#E8EFF7"><b>Address</b></td>
                <td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
                <td bgcolor="#E8EFF7" class="greybluetext10"><?=$memberAddress ?></td>
              </tr>
              <tr>
                <td class="greybluetext10" bgcolor="#E8EFF7"><b>Examination</b></td>
                <td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
                <td bgcolor="#E8EFF7" class="greybluetext10"><?=$examName ?></td>
              </tr>
              <tr>
                <td class="greybluetext10" bgcolor="#E8EFF7"><b>Subject Name</b></td>
                <td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
                <td bgcolor="#E8EFF7" class="greybluetext10"><?=$subjectName ?></td>
              </tr>
              <tr>
                <td class="greybluetext10" bgcolor="#E8EFF7"><b>Examination Centre</b></td>
                <td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
                <td bgcolor="#E8EFF7" class="greybluetext10"><?=$iwayAddress ?></td>
              </tr>
              <tr>
                <td class="greybluetext10" bgcolor="#E8EFF7"><b>Examination Date</b></td>
                <td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
                <td bgcolor="#E8EFF7" class="greybluetext10"><?=$examDate ?></td>
              </tr>
              <tr>
                <td class="greybluetext10" bgcolor="#E8EFF7"><b>Examination Time</b></td>
                <td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
                <td bgcolor="#E8EFF7" class="greybluetext10"><?php if ($examTime != "") print($examTime." Hrs."); ?></td>
              </tr>             
              <tr>
                <td class="greybluetext10" bgcolor="#E8EFF7"><b>Score</b></td>
                <td class="greybluetext10" bgcolor="#E8EFF7" align=center><b>:</b></td>
                <td bgcolor="#E8EFF7" class="greybluetext10"><?=$score ?></td>
              </tr>             
             
            </table>			</td>
		</tr>
		<tr>
		  <td align="left">&nbsp;</td>
	  </tr>
		<Tr>
			<td>
			<table border=0 width=640 align=center >
				<tr>
					<td><img src="images/spacer.gif" width=273 height=17></td>					
					<?PHP if($_SESSION['display_print'] == 'Y'){ ?>
					<td background="images/print.gif">
					<a href="javascript:window.print()"><img src="images/spacer.gif" width=38 height=17 border=0></a>
					</td>
					<?PHP } ?>
					<td background="images/exit.gif"><a href="javascript:feedback()"><img src="images/spacer.gif" width=29 height=17 border=0></a></td>								
					<td><img src="images/spacer.gif" width=273 height=17></td>																
				</tr>
			</table>			</td>
		</Tr>
		<tr>
			<td><br><br></td>
		</tr>
	</table>
	<table border="0" align=center cellpadding="0" cellspacing="0" width="640">
		<tr>
			<?php include("includes/footer.php");?>
		</tr>
	</table>
	<br>
</body>
</html>
<?php
//mysql_close($SLAVECONN);
?>
