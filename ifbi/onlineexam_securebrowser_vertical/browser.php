<?php /****************************************************
* Application Name            :  IIB 
* Module Name                 :  TA Options 
* Revision Number             :  1
* Revision Date               :  -
* Table(s)                    :  -
* Tables used for only selects:  -
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :  -
* Document/Reference Material :  -
* Created By	              :  -
* Created ON                  :  -
* Last Modified By            :  Suresh.A
* Last Modified Date          :  30-10-2013
* Description                 :  Interface for Browser details
*****************************************************/
header("Expires: Mon, 26 Jul 1920 05:00:00 GMT");    // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");     // always modified
header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");  

require_once("dbconfig.php");
$useragent = $_SERVER['HTTP_USER_AGENT'];
//$redirect = (!empty($_GET['logout']) && isset($_GET['logout'])) ? $_GET['logout'] : '';
if(trim($useragent) == 'Sify Browser')
{				
	header("Location:login.php");
	exit;		
}

$includeJS .= <<<JSSC
<script language="javascript">
history.go(1);
</script>
JSSC;

	$bodyOnload = '';
	include("themes/default/header.php");
?>
<div id="body_container_inner">
      <div class="innerbox_container_calogin_wrapper"><div class="outerbox">
	<div class="innerbox">
    	<div class="innerbox_container_calogin">
			
            <div class="content_container_calogin">
            	<div align="center" class="content_secure_browser" style="">
                    <p align="left">This application will work only with Sify I-Test Browser. <a href="./browser/I-Testsetup.exe">Click here</a> to download/install. Before installing please check in your system it's already installed or not. </p>
					<p>&nbsp;</p>
                    <p align="left"> For Sify E-Port's it will be installed by iway admin . </p>
					<p>&nbsp;</p>
                    <p align="left">Start Menu </p>
					<p>&nbsp;</p>
                    <p align="left">1.Start-&gt; Programs -&gt; Sify-I-Test-&gt; Sify-I-Test .exe </p>
					<p>&nbsp;</p>
					<div align="left">
						<p align="left"><a href="./font/font.zip">Click here</a> 
							<span class="style2">to download Hindi Fonts.</span>
						</p>
						<p>&nbsp;</p>
					</div>				  
				    <div>
					<p align="left">1. Download the font.zip file and extract it.</p>                    
                    <p align="left">2. Click on Start Menu -> Settings -> Control Panel.</p>
                  <p align="left">3. Copy extracted folder  files(*.ttf) , then paste those files into Fonts folder.</p></div>
                </div>
				  
            </div>
            
        </div>		
    </div>
</div></div>
    </div>

<?php include("themes/default/footer.php");?>