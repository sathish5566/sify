<?php
/****************************************************
* Application Name            :  IIB 
* Module Name                 :  Online Exam Page
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  iib_candidate_test,iib_section_questions,iib_section_questions_hindi,iib_case_master,iib_case_master_hindi
* Tables used for only selects:  iib_exam_candidate,iib_exam_subjects
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  
* Created ON                  :  
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  14-Dec-09
* Description                 :  Interface for showing exam result
*****************************************************/
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");   
set_time_limit(0);		
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkCandidateLogin();
$testID = $_POST['testid'];	
checkTestID($testID,$_SESSION['memno']);
require_once("constants.inc");
	
	
	$questionPaperNo = $_SESSION['questionpaperno'];
	$examCode = $_SESSION['ex'];
	$subjectCode = $_SESSION['sc'];
	$membershipNo = $_SESSION['memno'];
	$memberName = $_SESSION['memname'];
	

	$subjectName = $_SESSION['subject_name'];
	$passMark = $_SESSION['pass_mark'];
	$graceMark = $_SESSION['grace_mark'];
	$totalMarks = $_SESSION['total_marks'];
  	$totalTime = $_SESSION['subject_duration'];
  	$display_scr = $_SESSION['display_score'];
  	$display_res = $_SESSION['display_result'];
  	$passmsg = $_SESSION['pass_msg'];
  	$failmsg = $_SESSION['fail_msg'];
  	$resultType = $_SESSION['result_type'];
	
	$memberAddress = $_SESSION['address'];
	$examName = $_SESSION['exam_name'];
	
	$cand_total_time = getVal($_POST['total_time_candi']);
	$timeLeft = getVal($_POST['time_left']);
	$timeTaken = $cand_total_time - $timeLeft;	
	$auto_submit = getVal($_POST['auto_submit']);
	$centreCode = $_SESSION['centrecode'];
	$examDate = date('Y-m-d');
	$examTime = $_SESSION['et'];
	$tmpDate = explode('-',$examDate);
	$tmpTime =  explode(':',$examTime);
	$examDate = $tmpDate[2]."/".$tmpDate[1]."/".$tmpDate[0];
	$examTime = $tmpTime[0].":".$tmpTime[1];
			
	$sqlIWay = "SELECT iway_address1, iway_address2, iway_city, iway_state, iway_pin_code FROM iib_iway_details WHERE centre_code='$centreCode'";
	$resIWay = @mysql_query($sqlIWay,$SLAVECONN)  or errorlog('err05'," iib_iway_details  ".mysql_error($SLAVECONN));
	$row = mysql_fetch_array($resIWay);
	$iwayAddress = "";
	$row['iway_address1'] != "" ? $iwayAddress .= " ".$row['iway_address1'] : $iwayAddress .= "";
	$row['iway_city'] != "" ? $iwayAddress .= " ".$row['iway_city'] : $iwayAddress .= "";
	$row['iway_pin_code'] != "" ? $iwayAddress .= " ".$row['iway_pin_code'] : $iwayAddress .= "";
	$row['iway_state'] != "" ? $iwayAddress .= " ".$row['iway_state'] : $iwayAddress .= "";
	$row['iway_address2'] != "" ? $iwayAddress .= ",Contact No. :  ".$row['iway_address2'] : $iwayAddress .= "";
	
	$datetime = getDateTime();
	$sqlUpdate = "UPDATE iib_candidate_test SET browser_status='closed',test_status='C', end_time='$datetime', last_updated_time='$datetime', time_taken=$timeTaken, time_left=$timeLeft WHERE test_id=$testID";
	$qrystatus=mysql_query($sqlUpdate,$MASTERCONN)  or errorlog('err06'," QUERY:$sqlUpdate  ".mysql_error($MASTERCONN));
 	if($qrystatus && mysql_affected_rows($MASTERCONN)==1)
	{		
			
	/*************************************** SCORE CALCULATION START *****************************************************/ 
  	$score = 0;	
	$sqlMaxIds = "select  max(id) from iib_response where question_paper_no ='$questionPaperNo' group by question_id";
	$resMaxIds = mysql_query($sqlMaxIds);
	$arrMaxId = array();
	while(list($maxId) = @mysql_fetch_row($resMaxIds)) {
		$arrMaxId[] = $maxId;
	}
	$maxIds = implode(',', $arrMaxId);
	if($maxIds == '') $maxIds = "''";
	
	$sql_select = "SELECT SUM(A.marks) FROM iib_section_questions A  LEFT JOIN iib_response B ON A.question_id=B.question_id where B.question_paper_no ='$questionPaperNo' AND B.id IN ($maxIds)  AND A.correct_answer=B.answer";	
			
	$sql_select1 = "SELECT SUM(A.negative_marks) FROM iib_section_questions A  LEFT JOIN iib_response B ON A.question_id=B.question_id where B.question_paper_no='$questionPaperNo' AND B.id IN ($maxIds) AND A.correct_answer!=B.answer AND B.answer!='' ";
			
				/* IIB  TABLE  ANSWERS UPDATED in iib_question_paper_Details End */	
		$res_select = mysql_query($sql_select,$MASTERCONN)  or errorlog('err05'," QUERY:$sql_select  ".mysql_error($MASTERCONN)); 
		list($posscore)=mysql_fetch_row($res_select);
		
		$res_select1 = mysql_query($sql_select1,$MASTERCONN) or errorlog('err05'," QUERY:$sql_select1  ".mysql_error($MASTERCONN)); 
		list($negscore)=mysql_fetch_row($res_select1);		
			
	
	if(isset($negscore) && $negscore > 0)	
	{
		$score = $posscore - $negscore;		
	}else
	  $score = $posscore ;	  					
  	$score = round($score);
  	if ($score == -0)
  		$score = 0;
	//code added to convert negative marks to 0
	if ($score < 0)
		$score = 0;
  	if ($score <= 0)
  	  	$percentage = 0;  	
  	else
	  	$percentage = round((($score+0.0) / $totalMarks) * 100);
		  	
	if (($score+$graceMark) >= $passMark)
	{
		$examResult = 'P';
		if($resultType == 'PA')
			$displayResult = 'Pass';
		elseif($resultType == 'PR')
			$displayResult = 'Provisional';
		$msg=$passmsg;
	}
	else
	{		
		$examResult = 'F';
		if($resultType == 'PA')
			$displayResult = 'Fail';
		elseif($resultType == 'PR')
			$displayResult = 'Provisional';
		$msg=$failmsg;
	}

	
    $sql_score_check = "SELECT count(1) from iib_candidate_scores where membership_no='$membershipNo' AND exam_code='$examCode' AND subject_code='$subjectCode'";
	$res_score_check = mysql_query($sql_score_check,$SLAVECONN) or errorlog('err05'," QUERY:$sql_score_check  ".mysql_error($MASTERCONN)); 
	list($cnt_score_chk) = mysql_fetch_row($res_score_check);
	if(!$cnt_score_chk)
	{
		$sqlInsert = "INSERT INTO iib_candidate_scores (membership_no, exam_code, subject_code, score, exam_date,time_taken, result,auto_submit) VALUES ('$membershipNo', '$examCode', '$subjectCode', $score, now(), $timeTaken,'$examResult','$auto_submit')";
		mysql_query($sqlInsert,$MASTERCONN) or errorlog('err08'," QUERY:$sqlInsert  ".mysql_error($MASTERCONN)); 
	}else{
	$sqlUpdate = "UPDATE iib_candidate_scores set score='$score',exam_date=now(),time_taken='$timeTaken',result='$examResult',auto_submit='$auto_submit' where membership_no='$membershipNo' and exam_code='$examCode' AND subject_code='$subjectCode'";
		mysql_query($sqlUpdate,$MASTERCONN) or errorlog('err06'," QUERY:$sqlUpdate   ".mysql_error($MASTERCONN)); 	
	}  	
			

  /*************************************** SCORE CALCULATION END *****************************************************/ 
 }		
	// Code added to replace the string given in the message
	if($msg != '')
	{	
	      if($resultType == 'PR')
			{
				$Constants_msg=array(
				   "/\bREP_MEMBERSHIPNAME\b/",
				   "/\bREP_SCORE\b/",
				   "/\bREP_TOTALMARK\b/",
				   "/\bREP_SUBJECTNAME\b/"
				 );
				 $replace_msg=array(
				   $memberName,
				   $score,
				   $totalMarks,
				   $subjectName
				);
			}
			elseif($resultType == 'PA')
			{
				 $Constants_msg=array(
				   "/\bREP_MEMBERSHIPNAME\b/",
				   "/\bREP_SCORE\b/",
				   "/\bREP_TOTALMARK\b/",
				   "/\bREP_SUBJECTNAME\b/"
				 );

				// Time array used to replace the constant string present in the message
				$replace_msg=array(
				   $memberName,
				   $score,
					$totalMarks,
				   $subjectName
				 );
			}
			
		    $display_msg=preg_replace($Constants_msg,$replace_msg,$msg); 	
	}
	else
	{
		$not_rep_msg=true;
	}
	if($not_rep_msg)
		$display_msg=$msg;


//To find number of question
$sql_select3 = "SELECT display_order,answer FROM iib_question_paper_details where question_paper_no='$questionPaperNo' order by display_order";
$resSql = @mysql_query($sql_select3,$SLAVECONN)  or errorlog('err05'," SQL:$sql_select3 ".mysql_error($SLAVECONN));
$sqlct=mysql_num_rows($resSql);	

//GET Responce from responce table
$respArr = array();
$sqlMaxIds = "select  max(id) from iib_response where question_paper_no ='$questionPaperNo' group by question_id";
$resMaxIds = mysql_query($sqlMaxIds);
$arrMaxId = array();
while(list($maxId) = @mysql_fetch_row($resMaxIds)) {
	$arrMaxId[] = $maxId;
}
$maxIds = implode(',', $arrMaxId);
if($maxIds == '') $maxIds = "''";

$sql_select_r = "select display_order,answer from iib_response where id in ($maxIds) order by display_order";
$resSql_r = @mysql_query($sql_select_r,$SLAVECONN)  or errorlog('err05'," SQL:$sql_select3 ".mysql_error($SLAVECONN));
$sqlct_r=mysql_num_rows($resSql_r);	
if($sqlct_r>0){
	while($row_r=mysql_fetch_array($resSql_r))
	{
		$respArr[$row_r['display_order']] = $row_r['answer'];
	}
}

$_SESSION['score']=$score;


if($_SESSION['exam_type']==2)
	$actionpage="feedback_ing.php";
else
	$actionpage="feedback.php";

//Added for BOI
//Code added to check if all the subjects under the exam code has been completed to enable feedback page
$sqlFeedbackIway="SELECT subject_code , TIME_TO_SEC(exam_time) from iib_candidate_iway where membership_no='$membershipNo' and exam_code='$examCode' order by subject_code";
$resFeedbackIway=mysql_query($sqlFeedbackIway,$SLAVECONN) or errorlog('err05',"SQL:$sqlFeedbackIway ".mysql_error($SLAVECONN));
$cnt_iway=mysql_num_rows($resFeedbackIway);
if($cnt_iway > 1)
{
	while(list($Iway_subCodes , $Iway_eTime)=mysql_fetch_row($resFeedbackIway))
	{
		$Iway_subArr[]=$Iway_subCodes;
		$ETimeArr[$Iway_subCodes]=$Iway_eTime;
	}
}
else
{
	list($Iway_subCodes,$Iway_eTime)=mysql_fetch_row($resFeedbackIway);
	$ETimeArr[$Iway_subCodes]=$Iway_eTime;
}

$sqlFeedbackScores="SELECT subject_code from iib_candidate_scores where membership_no='$membershipNo' and exam_code='$examCode' order by subject_code";
$resFeedbackScores=mysql_query($sqlFeedbackScores,$SLAVECONN) or errorlog('err05',"SQL:$sqlFeedbackIway ".mysql_error($SLAVECONN));
$cnt_scores=mysql_num_rows($resFeedbackScores);
if($cnt_scores > 1)
{
	while(list($Score_subCodes)=mysql_fetch_row($resFeedbackScores))
	{
		$Score_subArr[]=$Score_subCodes;
	}
}
else
{
	list($Score_subCodes)=mysql_fetch_row($resFeedbackScores);
}
$feedback_flag=false;
if(is_array($Iway_subArr) && is_array($Score_subArr))
{
	$SubCodes=array_diff($Iway_subArr,$Score_subArr);   /// Checking Entries present for all the subjects for exam_code in both scoreSUBArray and iwaySUBArr
	if(count($SubCodes) == 0)
		$feedback_flag=true;
	else
		$feedback_flag=false;
}
else
{
	if(($cnt_iway == $cnt_scores) && ($Score_subCodes == $Iway_subCodes))   /// both counts in iway and scores same then all the subjects for the exam is over.
			$feedback_flag=true;
		else
			$feedback_flag=false;
}
unset($_SESSION['testid']);

if($_SESSION['exam_type']==3 && $feedback_flag==false )
	$actionpage="intermediate_section_login.php";

//Added for BOI ends here
	$qry="select img_path,alt_text from iib_logo_master where active='Y'";
	$res = mysql_query($qry,$SLAVECONN) or errorlog('err05'," QUERY:$qry  ".mysql_error($SLAVECONN));
	list($imgpath,$alttext)=mysql_fetch_row($res); 
	
$includeJS = '<script language="javascript" type="text/javascript" src="./includes/browser.js"></script>';
$includeJS .= <<<JSSC
<script language='javascript' type="text/javascript">	
function feedbackFn() {
	windowClose = 1;		
	document.feedback.submit();
}
function closeParent() { 
	windowClose = 1;
	try{ 
	var op = window.opener; 
	op.opener = self; 
	op.close(); 
	} 
	catch(er) 
	{} 
}
function displayScore(memno,examCode,subjectCode,qpno,pmark) {
	var op="";
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
    }else{// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
	xmlhttp.open("GET",'ajax_scoredetails.php?memno='+memno+'&ecode='+examCode+'&scode='+subjectCode+'&qpno='+qpno+'&pmark='+pmark,false);
	xmlhttp.send();
	var resp = xmlhttp.responseText.split('|');

        var o = document.getElementById("scoredisp");
        if(o.hasChildNodes()) {
		l=o.childNodes.length
		if(l>0)
		for(i=0;i<l;i++) {
			o.removeChild(o.childNodes[0]);
		}
	}
	var td1 = document.createElement('td');
	td1.setAttribute("class", "tb_rw1");
	td1.setAttribute("width", "25%");
	td1.setAttribute("align", "right");
	td1.innerHTML = '<b>Score</b>';
	var td2 = document.createElement('td');
	td2.setAttribute("class", "tb_col");
	td2.setAttribute("width", "3%");
	td2.setAttribute("align", "left");
	td2.innerHTML = ':';
	var td3 = document.createElement('td');
	td3.setAttribute("class", "tb_rw2");
	td3.setAttribute("width", "72%");
	td3.innerHTML = resp[0];
	o.appendChild(td1);
	o.appendChild(td2);
	o.appendChild(td3);

		 

	/*op += '<td class="greybluetext10" bgcolor="#E8EFF7"><b>Score</b></td>';
	op += '<td class="greybluetext10" bgcolor="#E8EFF7" align="center"><b>:</b></td>';
	op += '<td bgcolor="#E8EFF7" class="greybluetext10">'+resp[0]+'</td>';
	document.getElementById("scoredisp").innerHTML = op;*/
}
function displayResult(memno,examCode,subjectCode,qpno,pmark) {
	var op1="";
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
    }else{// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
	xmlhttp.open("GET",'ajax_scoredetails.php?memno='+memno+'&ecode='+examCode+'&scode='+subjectCode+'&qpno='+qpno+'&pmark='+pmark,false);
	xmlhttp.send();
	var resp = xmlhttp.responseText.split('|');
     
	var o = document.getElementById("resultdisp");
	if(o.hasChildNodes()) {
		l=o.childNodes.length
		if(l>0)
		for(i=0;i<l;i++) {
			o.removeChild(o.childNodes[0]);
		}
	}
	var td1 = document.createElement('td');
	td1.setAttribute("class", "tb_rw1");
	td1.setAttribute("width", "25%");
	td1.setAttribute("align", "right");
	td1.innerHTML = 'Result';
	var td2 = document.createElement('td');
	td2.setAttribute("class", "tb_col");
	td2.setAttribute("width", "3%");
	td2.setAttribute("align", "left");
	td2.innerHTML = ':';
	var td3 = document.createElement('td');
	td3.setAttribute("class", "tb_rw2");
	td3.setAttribute("width", "72%");
	td3.innerHTML = resp[1];
	o.appendChild(td1);
	o.appendChild(td2);
	o.appendChild(td3);

}
function gotoNextSection(){
//	  closeParent();	 
	//  self.focus();
	 //  self.close();
//		window.open("intermediate_section_login.php","IBPS",'toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=1,maximize=0');	
		window.location=intermediate_section_login.php
		//document.feedback.submit();		
	}
/* Added for IBPS BOI Ends here */
</script>
JSSC;
$bodyOnLoad = 'onKeyDown="return doKeyDown(event);" ondragstart ="return Stop(event)" onselectstart="return Stop(event)" onmousedown="return Stop(event)" onmouseup="return Stop(event)"';
include("themes/default/header.php");	
?>

<form name='feedback' method='post' action='<?PHP echo $actionpage?>'>
<input type=hidden name='membership_no' value='<?=$membershipNo ?>'>
<input type=hidden name='exam_code' value='<?=$examCode ?>'>
<input type=hidden name='subject_code' value='<?=$subjectCode ?>'>
<!-- Added for BOI -->
<input type=hidden name='cafeid' value='<?=$centreCode ?>'>
<!-- Added for BOI Ends here-->

<!--table width="785" border="0" cellspacing="0" cellpadding="0" align=center >
	<tr> 
		<?php if($imgpath!=''){ ?>
			<td align="center" width="785" valign="top" class="menu"><img src="<?php echo $imgpath; ?>" alt="<?php echo $alttext;?>"></td>
		<?php }else{ ?>
			<td align="center" width="785" valign="top" class="menu">&nbsp;</td>
		<?php } ?>
	</tr>
	<tr>
		<td width="785" class="ColumnFooter">&nbsp;</td>
	</tr>
</table-->
<div id="body_container_inner"><div class="ca_ex_res_wrapper"><div class="outerbox">
	<div class="innerbox">
    	<div class="innerbox_container_common">
        	<div class="pager_header_common">Candidate Exam Result</div>
			<div class="content_container_common">
            	<div class="content_container_box_common" >
            	  <div id="ca_ex_res_inner_box" >
				  <table width="95%"  border="0" align="center" cellpadding="0" cellspacing="0" id="ca_ex_res_inner_box_inner_tb" >
				   <tr>
              <td width="25%" align="right" class="tb_rw1">Membership Number</td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td width="72%" class="tb_rw2"><?php echo $membershipNo ?></td>
            </tr>
			 <tr>
              <td width="25%" align="right" class="tb_rw1">Name</td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td width="72%" class="tb_rw2"><?php echo $memberName ?></td>
            </tr>
            <tr>
              <td width="25%" align="right" class="tb_rw1">Address</td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td width="72%" class="tb_rw2"><?php echo $memberAddress ?></td>
            </tr>
            <tr>
              <td width="25%" align="right" class="tb_rw1">Examination</td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td width="72%" class="tb_rw2"><?php echo $examName ?><br /></td>
            </tr>
            <tr>
              <td width="25%" align="right" class="tb_rw1">Subject Name</td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td width="72%" class="tb_rw2"><?php echo $subjectName ?><br /></td>
            </tr>
            <tr>
              <td width="25%" align="right" class="tb_rw1">Examination Centre </td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td width="72%" class="tb_rw2"><?php echo $iwayAddress ?><br /></td>
            </tr>
            <tr>
              <td align="right" class="tb_rw1">Examination Date<br /></td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td class="tb_rw2"><?php echo $examDate ?><br /></td>
            </tr>
            <tr>
              <td align="right" class="tb_rw1">Examination Time<br /></td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td class="tb_rw2"><?php if ($examTime != "") print($examTime." Hrs."); ?><br /></td>
            </tr>
			<?php if($display_scr == 'Y') { ?>
			<tr>
              <td align="right" class="tb_rw1">Score<br /></td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td class="tb_rw2"><?php echo $score; ?><br /></td>
            </tr>
			<?php }  if($display_res == 'Y') { ?>
			<tr>
              <td align="right" class="tb_rw1">Result<br /></td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td class="tb_rw2"><?php echo $displayResult; ?><br /></td>
            </tr>
			<? }?>				
			</table> 
			<div class="clear"></div><h1>Thank you for taking the online examination</h1>	  
			 <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="Report_tb">
			  <tr><th colspan="10">Response Report</th>	</tr>
			  <tr class="tb_rw3">
				<td>Q.No.</td>
				<td>Answer</td>
				<td>Q.No.</td>
				<td>Answer</td>
				<td>Q.No.</td>
				<td>Answer</td>
				<td>Q.No.</td>
				<td>Answer</td>
				<td>Q.No.</td>
				<td>Answer</td>
			  </tr>
<?PHP 
					$attempt_cnt = $unattempt_cnt = 0;
					$v = $sqlct % 5;					
					$i = $j =1;
					while($repData = mysql_fetch_array($resSql))
			  		{			   
						 $dis_order=$repData['display_order'];
						 if($respArr[$dis_order]==''){ 
						  		$dis_answer='--';
							 	$unattempt_cnt++;
						 }else{
						  	 	$dis_answer=$respArr[$dis_order];
						    	$attempt_cnt++;
						 } 
						 if($i==1){
						  	echo '<tr  class="tb_rw4"><td class="tb_rw5" >'.$dis_order.'</td><td class="tb_rw5" >'. $dis_answer.'</td>';
							$i++;
							if($j==$sqlct && $v==1)
							{
								echo '<td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  </tr>';
							}								
						 }else if($i==2){
						  	echo '<td class="tb_rw5" >'.$dis_order.'</td><td class="tb_rw5" >'. $dis_answer.'</td>';
							$i++;
							if($j==$sqlct && $v==2)	{
								echo '<td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
								      <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  </tr>';
							}								
						  }else if ($i==3){ 						  
						  	echo '<td class="tb_rw5" >'.$dis_order.'</td><td class="tb_rw5" >'. $dis_answer.'</td>';
						    $i++;
							if($j==$sqlct && $v==3) {
								echo '<td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
								      <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  </tr>';
							}
						  }else if ($i==4){ 						  
								echo '<td class="tb_rw5" >'.$dis_order.'</td><td class="tb_rw5" >'.$dis_answer.'</td>';
								$i++;
								if($j==$sqlct && $v==4) {
									echo '<td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									      <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
										  </tr>';
								}
						  }
						  else if ($i==5){ 						  	
							   echo '<td class="tb_rw5" >'.$dis_order.'</td><td class="tb_rw5" >'.$dis_answer.'</td>';
							   echo '</tr>';
							   $i=1;							   
	   	                  }	
						 $j++;			  						 					  										
                 	}											
					?>
              </table>
<div id="tot_no_quest">Total number of question attempted</b><span class="N-attempt"><?php echo $attempt_cnt?></span></div>
          <div id="tot_no_quest_right">Unattempted<span class="N-unattempt"><?php echo $unattempt_cnt?></span></div>     
        </div></div></div>
	<div class="button_container_common">
			<?php if($_SESSION['exam_type']==3 && $feedback_flag==false ) {
						$_SESSION['sub_opt']='Y';
						$_SESSION['ta_override']='N';						
			?>
			<input name="ClearFrm" id="ClearFrm" class="green_button" type="submit"  value="Start Optional Subject" /> 
			<?php }else if($_SESSION['exam_type']==4) { //For SBI ?>
			<input name="feedbtn" class="green_button" type="button" onClick="feedbackFn()" value="Exit" />
			<?php }else {
				if($_SESSION['display_print'] == 'Y') {
			?>		
					<input name="prtbtn" class="green_button" type="button" onClick="javascript:window.print()" value="Print" /> 
			<?php } ?>
			<input name="exitbtn" class="green_button" type="button" onClick="feedbackFn();" value="Exit" />
			<?php } ?>
            </div>
        </div> </div>
</div></div> 
  </div>
	</form>
<?php 
include("themes/default/footer.php");
//mysql_close($MASTERCONN);
//mysql_close($SLAVECONN);

?>	
			  

