<?php
/****************************************************
* Application Name			: IIB
* Module Name				: Sample scores page
* Revision Number			: 1
* Revision Date			:
* Table(s) used for Selects	:  iib_candidate , iib_exam , iib_exam_subjects , iib_iway_details , iib_candidate_iway 
* Tables used for Updates,Inserts : 
* View(s)				:  -
* Stored Procedure(s)		:  -
* Dependant Module(s)		:  -
* Output File(s)			: -
* Document/Reference Material	:
* Created By				: 
* Created On				: 
* Last Modified By			: Bala Murugan.S
* Last Modified Date		: 31-08-2010
* Change reason				: 
* Description               : Page where the score will be calculated for the the sample test. 
*****************************************************/
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkTALogin();
checkCandidateLogin();
require_once("constants.inc");
	
	$time_to_submit = getVal($_REQUEST['time_to_submit']);
	$centreCode = $_SESSION['cafeID'];
	$todaydate=date('Y-m-d');
	
	$exam_code = $_SESSION['ex'];
	$subject_code = $_SESSION['sc'];
	$selExamTime = $_SESSION['et'];
	$display_scr = $_SESSION['display_score'];
  	$display_res = $_SESSION['display_result'];
	$result_type=$_SESSION['result_type'];		
	$exam_view=$_SESSION['exam_view'];		
  	for ($i =0 ; $i < 5; $i++)
  	{		
		$sample_answer = trim($_POST["answer".($i+1)]);					
  		if ($sample_answer == $QUESTION_ARRAY[$i]['EN']['correct_answer'])
  		{
	  		$sample_score += $QUESTION_ARRAY[$i]['EN']['mark'];
  		}
  		if (($sample_answer != $QUESTION_ARRAY[$i]['EN']['correct_answer']) && ($sample_answer != ""))
  		{
	  		$sample_score -=  $QUESTION_ARRAY[$i]['EN']['negative_mark'];
  		}
  	} // end of for

  	$sample_score = round($sample_score);

  	//code added to convert negative marks to 0
  	if ($sample_score == -0)
  		$sample_score = 0;
	if ($sample_score < 0)
		$sample_score = 0;

  	if ($commonDebug)
  	{
  		print("<br>score = ".$sample_score);
  		print("<br>total marks = ".$sample_totalMarks);
  		print("<br>percentage = ".$sample_percentage);
  		print("<br>pass mark = ".$sample_passMark);
	}
	if ($sample_score >= $sample_passMark)
	{
		if($result_type == 'PA')
			$sample_displayResult = 'Pass';
		elseif($result_type == 'PR')
			$sample_displayResult = 'Provisional';
	
	}
	else
	{
		if($result_type == 'PA')
			$sample_displayResult = 'Fail';
		elseif($result_type == 'PR')
			$sample_displayResult = 'Provisional';
	}

	
	

	$tmpDate = $todaydate;
	$tmpTime = $selExamTime;
	$tmpDate = explode('-',$tmpDate);
	$tmpTime =  explode(':',$tmpTime);
	$examDate = $tmpDate[2]."/".$tmpDate[1]."/".$tmpDate[0];
	$examTime = $tmpTime[0].":".$tmpTime[1];
	$sqlIWay = "SELECT iway_address1, iway_address2, iway_city, iway_state, iway_pin_code FROM iib_iway_details ".
		" WHERE centre_code='$centreCode'";
	if ($commonDebug)
	{
		print("\n".$sqlIWay);
	}
	$resIWay = @mysql_query($sqlIWay,$SLAVECONN) or errorlog('err05'," iib_iway_details  ".mysql_error($SLAVECONN));
	$row = mysql_fetch_array($resIWay);
	$iwayAddress = "";
	$row['iway_address1'] != "" ? $iwayAddress .= " ".$row['iway_address1'] : $iwayAddress .= "";
	$row['iway_city'] != "" ? $iwayAddress .= " ".$row['iway_city'] : $iwayAddress .= "";
	$row['iway_pin_code'] != "" ? $iwayAddress .= " ".$row['iway_pin_code'] : $iwayAddress .= "";
	$row['iway_state'] != "" ? $iwayAddress .= " ".$row['iway_state'] : $iwayAddress .= "";
	$row['iway_address2'] != "" ? $iwayAddress .= ",Contact No. :  ".$row['iway_address2'] : $iwayAddress .= "";
	$includeJS = "<script language='JavaScript' src='./includes/timer_submit.js'></script>";
	$includeJS .= "<script language='javascript' type='text/javascript' src='./includes/browser.js'></script>";
	$includeJS .= <<<JSSC
<script language='JavaScript'>
history.go(1);
function valid(){
	document.getElementById("sub_form").disabled=true;
    document.getElementById("submitstatus").innerHTML='please wait submitting form...';
    document.frmscores.submit();
}
function callOnLoad()
{
	if(document.getElementById("exam_view").value == "V")
		InitializeTimer($time_to_submit,"frmscores","online_exam_ver.php");
	else
		InitializeTimer($time_to_submit,"frmscores","online_exam.php");
}
window.onload =  callOnLoad;
</script>
JSSC;
$bodyOnLoad = ' onLoad="javascript:callOnLoad();" onKeyDown="return doKeyDown(event);"  ondragstart ="return Stop(event)" onselectstart="return Stop(event)" onmousedown="return Stop(event)" onmouseup="return Stop(event)"';
include("themes/default/header.php");
?>
<?php if($exam_view=='V') { ?>
<form name='frmscores' method='post' action='online_exam_ver.php'>
<?php }else{ ?>
<form name='frmscores' method='post' action='online_exam.php'>
<?php } ?>
	<input type=hidden name='time_to_submit' value='<?=$time_to_submit?>'>
	<input type='hidden' name='exam_view' id='exam_view' value='<?php echo $exam_view;?>'>
        <div id="body_container_inner"><div class="ca_ex_res_wrapper"><div class="outerbox">
        <div class="innerbox">
        <div class="innerbox_container_common">
        <div class="pager_header_common">Candidate Sample Exam Result</div>
        <div class="content_container_common">
        <div class="content_container_box_common" >
        <div id="ca_ex_res_inner_box" >
        <table width="95%"  border="0" align="center" cellpadding="0" cellspacing="0" id="ca_ex_res_inner_box_inner_tb" >
            <tr>
              <td width="25%" align="right" class="tb_rw1">Membership Number</td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td width="72%" class="tb_rw2"><?php echo $_SESSION['memno']; ?></td>
            </tr>
            <tr>
              <td width="25%" align="right" class="tb_rw1">Name</td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td width="72%" class="tb_rw2"><?php echo $_SESSION['memname']; ?></td>
            </tr>
            <tr>
              <td width="25%" align="right" class="tb_rw1">Address</td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td width="72%" class="tb_rw2"><?php echo $_SESSION['address']; ?></td>
            </tr>
            <tr>
              <td width="25%" align="right" class="tb_rw1">Examination</td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td width="72%" class="tb_rw2"><?php echo $_SESSION['exam_name']; ?><br /></td>
            </tr>
            <tr>
              <td width="25%" align="right" class="tb_rw1">Subject Name</td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td width="72%" class="tb_rw2"><?php echo $_SESSION['subject_name']; ?><br /></td>
            </tr>
            <tr>
              <td width="25%" align="right" class="tb_rw1">Examination Centre </td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td width="72%" class="tb_rw2"><?php echo $iwayAddress ?><br /></td>
            </tr>
            <tr>
              <td align="right" class="tb_rw1">Examination Date<br /></td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td class="tb_rw2"><?php echo $examDate ?><br /></td>
            </tr>
	    <tr>
              <td align="right" class="tb_rw1">Examination Time<br /></td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td class="tb_rw2"><?php if ($examTime != "") print($examTime." Hrs."); ?><br /></td>
            </tr>
            <?php if ($display_scr == 'Y') { ?>
		 <tr>
              <td align="right" class="tb_rw1">Score</td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td class="tb_rw2"><?php echo $sample_score ?></td>
            </tr>
            <?php } if ($display_res == 'Y') { ?>
		<tr>
              	<td align="right" class="tb_rw1">Result</td>
              	<td width="3%" align="left" class="tb_col">:</td>
              	<td class="tb_rw2"><?php echo $sample_displayResult; ?></td>
            	</tr>
             <?php } ?>
		</table>
               <!--Final results will be displayed once your result is reviewed by IBPS.<br>-->
               <div class="clear"></div><h1>Thank you for taking the online examination</h1>
				 <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="Report_tb">
			  <tr><th colspan="10">Response Report</th>	</tr>
			  <tr class="tb_rw3">
				<td>Q.No.</td>
				<td>Answer</td>
				<td>Q.No.</td>
				<td>Answer</td>
				<td>Q.No.</td>
				<td>Answer</td>
				<td>Q.No.</td>
				<td>Answer</td>
				<td>Q.No.</td>
				<td>Answer</td>
			  </tr>
              <?php
					$sqlct=5;
					$v=$sqlct % 4;					
					$i=1;
					$j=1;
					$attempt_cnt=0;
					$unattempt_cnt=0;
					for ($k = 0; $k < 5; $k++)
  					{
	  					 $sample_answer = trim($_POST["answer".($k+1)]);  
						 $dis_order=$k+1;
						 if($sample_answer=='') {
						  $dis_answer='--';
						  $unattempt_cnt++;
						 }else{
						   $dis_answer=$sample_answer;
						   $attempt_cnt++;
						 }
								 if($i==1){
						  	echo '<tr  class="tb_rw4"><td class="tb_rw5" >'.$dis_order.'</td><td class="tb_rw5">'. $dis_answer.'</td>';
							$i++;
							if($j==$sqlct && $v==1)
							{
								echo '<td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  </tr>';
							}
								
						  }else if($i==2){
						  	echo '<td class="tb_rw5" >'.$dis_order.'</td><td class="tb_rw5" >'. $dis_answer.'</td>';
							$i++;
							if($j==$sqlct && $v==2)
							{
								echo '<td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
								      <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  </tr>';
							}
								
						  }else if ($i==3){ 						  
						  	echo '<td class="tb_rw5" >'.$dis_order.'</td><td class="tb_rw5" >'. $dis_answer.'</td>';
						    $i++;
							if($j==$sqlct && $v==3)
							{
								echo '<td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
								      <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  </tr>';
							}
						  }else if ($i==4){ 						  
								echo '<td class="tb_rw5" >'.$dis_order.'</td><td class="tb_rw5" >'.$dis_answer.'</td>';
								$i++;
								if($j==$sqlct && $v==4)
								{
									echo '<td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									      <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
										  </tr>';
								}
						  }
						  else if ($i==5){ 						  	
							   echo '<td class="tb_rw5" >'.$dis_order.'</td><td class="tb_rw5" >'.$dis_answer.'</td>';
							   echo '</tr>';
							   $i=1;							   
	   	                  }	
						 $j++;			  						 					  										
                 	}
			?>
</table>
<div id="tot_no_quest">Total number of question attempted</b><span class="N-attempt"><?php echo $attempt_cnt?></span></div>
          <div id="tot_no_quest_right">Unattempted<span class="N-unattempt"><?php echo $unattempt_cnt?></span></div>     
        </div></div></div>
<div class="button_container_common">
                  <input class='green_button' id='sub_form' type='button' value='Get ready to Start the Exam' name="sub_form" onclick="valid()">
			      <br/><span id="submitstatus" class="errormsg"></span>							  
 </div>                   
                 </div> </div>
</div></div> 
  </div>
	</form>
	
<!-- Timer Alert -->
<div id='styled_popup' style='cursor:hand;width: 350px; height: 78px; display:none; position: absolute; z-index: 100; top: -4px; left: -3px;' name='styled_popup'>
  <table width="100%" height="75" border="0" cellpadding="0" cellspacing="0" onclick='javascript:styledPopupClose()' >
    <tr>
      <td><table width='100%' height="78" border='0' cellpadding='5' cellspacing='1' class='Report_tb'>
	  <tr><th colspan="10">Time Alert</th>	</tr>
      <tr class='tb_rw3'>
          <td colspan='2' align="center">You have spent much time for reading Instructions/Sample test and will be redirected to the Main exam in 5 Mins<br/>
            <br/>
        <input name="button" type="button" class="green_button" onclick='javascript:styledPopupClose()' value=' Close ' />          </td>
        </tr>
      </table></td>
    </tr>
  </table>
</div>
<!-- Timer Alert -->
<?php include("themes/default/footer.php");
//mysql_close($SLAVECONN);
?>
