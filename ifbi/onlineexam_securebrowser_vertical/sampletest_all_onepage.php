<?php
/****************************************************
* Application Name            :  IIB 
* Module Name                 :  Exam Module
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  -
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              : 
* Created ON                  :  
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  22-01-2010
* Description                 :  Interface for the Sample Exam page all questions one page
*****************************************************/
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkTALogin();
checkCandidateLogin();
require_once("constants.inc");

$hid_lefttime=$_REQUEST['hid_lefttime'];

$memno=$_REQUEST['memno'];
$subcode=$_REQUEST['subjectcode'];
$examcode=$_REQUEST['examcode'];
$taOverride = $_REQUEST['ta_override'];
$loginOverride = $_REQUEST['login_override'];
$time_to_submit = $_REQUEST['time_to_submit'];

$membershipNo = $memno;
if ($subcode == "")
{
	$subcode = $_REQUEST['subject_code'];
}
if ($examcode == "")
{
	$examcode = $_REQUEST['exam_code'];
}

$lang = $_SESSION['mc'];

	$qry="select img_path,alt_text from iib_logo_master where active='Y'";
	$res = mysql_query($qry,$SLAVECONN) or errorlog('err05'," QUERY:$qry  ".mysql_error($SLAVECONN));
	list($imgpath,$alttext)=mysql_fetch_row($res); 
?>
<html>
<head>
<title><?PHP echo TITLE?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<script language='JavaScript' src="./includes/timer_submit.js"></script>
<STYLE TYPE="text/css">
  @font-face {
    font-family: DVBW-TTYogeshEN;
    font-style:  normal;
    src: url(http://sifyitest.com/eot/DVBWTTY0.eot);
  }
 .qn{font-family:'DVBW-TTYogeshEN';font-weight:bold;font-size:18px}
 .ans{font-family:'DVBW-TTYogeshEN';font-weight:normal;font-size:18px}
 .details {border:0 solid black; display:none; padding:6 2 2 6; margin: 0 0 0 0;}
</style>
<script language='JavaScript'>
	history.go(1);
	function eraseAnswer(q_id)
	{
		var pos;
		var answervar;
		var len = eval('document.sample_form.answer'+q_id+'.length');
		for (pos = 0; pos < len; pos++)
		{
			answervar = eval('document.sample_form.answer'+q_id+'['+pos+']');				
			answervar.checked = false;
		}			
		return;
	}
	function submitForm()
	{
		document.sample_form.submit();
	}

function callOnLoad()
{
	InitializeTimer(<?=$time_to_submit?>,"sample_form","sample_preview_all_onepage.php");
}
window.onload =  callOnLoad;
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<center>
<form name='sample_form' action="sample_preview_all_onepage.php" method="post">
<input type=hidden name='time_to_submit' value='<?=$time_to_submit?>'>
<input type=hidden name="hid_lefttime" value='<?=$hid_lefttime?>'>
<input type=hidden name=memno value='<?=$memno ?>'>
<input type=hidden name=subject_code value='<?=$subcode ?>'>
<input type=hidden name=exam_code value='<?=$examcode ?>'>
<input type=hidden name=ta_override value='<?=$taOverride ?>'>
<input type=hidden name=login_override value='<?=$loginOverride ?>'>
<table width="780" border="0" cellspacing="0" cellpadding="0">
<tr>
	<?php if($imgpath!=''){ ?>
		<td align="center" width='780'><img src="<?php echo $imgpath; ?>" alt="<?php echo $alttext;?>"></td>
	<?php }else{ ?>
	   	<td align="center" width='780'>&nbsp;</td>
	<?php } ?>
	<!-- <td><img src="images/bank_logo/allahabad.jpg" alt='SIFY'> -->
</td>
</tr>
<tr>
	<td width="780" bgcolor="7292C5">&nbsp;</td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="780" background="images/tile.jpg">
<tr>
	<td align=center>
	<br>
	<table border="0" cellspacing="0" cellpadding="5" align=center width="80%">
	<tr> 
		<td class="greybluetext10" height="35" bgcolor="#D1E0EF" align=center><b class="arial11a"><u><?if($lang=='E') echo "Sample Question Paper";else echo "<font class='qn'>�ִ���� ��Ͽ�֯֡�</font>";?></u></b></td>
	</tr>
	<tr>
	<td colspan=2 class="greybluetext10" bgcolor="#E8EFF7" valign=top>
		 <table border="0" cellspacing="0" cellpadding="0" width=100%>
		 <tr>
		 
		 <?if($lang=='E') {?>
		 <td class="greybluetext10" bgcolor="#E8EFF7" width=85% style={text-align:justify}>
		 <a name='#1'></a><b>Q1. What is www.iibf.org.in ?</b></td>
		 <?}?>
		 
		 <?if($lang=='H') {?>
		 <td class="greybluetext10" bgcolor="#E8EFF7" width=85% style={text-align:justify}>
		 <a name='#1'></a><b>Q1.<font class='qn'>�ևԆևԲ�ߋ��-���ֻ�և��.���ָ���� (www.iibf.org.in) ���� ��� ?</font></b></td>
		 <?}?>
		 
		 
	 	 <td class="greybluetext10" bgcolor="#E8EFF7" align=right valign=top>
	 	 <b><?php if($lang=='E') print(" (Mark/s - ".$aSMarks[0].") ");if($lang=='H') echo "<font class='qn'>(����-".$aSMarks[0].")</font>"; ?></b>
	 	 		<?if($lang=='E') echo "<a href='javascript:eraseAnswer(1)'>Erase Answer</a>";?>
	 	 		<?if($lang=='H') echo "<a href='javascript:eraseAnswer(1)'><font class='qn'>��ָ ״֙�ֆ��� </font></a>";?>
		 </td>
		 </tr>		 
		 </table>
		 <br>
		<table border="0" cellspacing="0" cellpadding="2">
		<tr> 
			<td class="greybluetext10"><input name='answer1' type=radio value=1 <?php ($_POST['answer1'] == 1) ? print("checked") : print(""); ?>></td>
			<td class="greybluetext10">a) <?php if($lang=='E')  echo "online education site";else echo "<font class='ans'>���ֻ�և�� ���������� ��և��</font>";?></td>
		</tr>
		<tr>
			<td class="greybluetext10"><input name='answer1' type=radio value=2 <?php ($_POST['answer1'] == 2) ? print("checked") : print(""); ?>></td>
			<td class="greybluetext10">b) <?php if($lang=='E')  echo "Website"; else echo "<font class='ans'>������և��</font>";?></td>
		</tr>
		<tr>
			<td class="greybluetext10"><input name='answer1' type=radio value=3 <?php ($_POST['answer1'] == 3) ? print("checked") : print(""); ?>></td>
			<td class="greybluetext10">c) <?if($lang=='E') echo "Portal"; else echo "<font class='ans'>��������</font>";?></td>
		</tr>
		<tr>
			<td class="greybluetext10"><input name='answer1' type=radio value=4 <?php ($_POST['answer1'] == 4) ? print("checked") : print(""); ?>></td>
			<td class="greybluetext10">d) <?if($lang=='E') echo "Online tool"; else echo "<font class='ans'>���ֻ�և�� ��ӡ� (�����)</font>";?></td>
		</tr>
		</table>	
	<img alt="" src="tran.gif" width=1 height=4><br clear=all></td>
	</tr>
	<tr> 
	<td colspan=2 class="greybluetext10" bgcolor="#E8EFF7" valign=top>
		 <table border="0" cellspacing="0" cellpadding="0" width=100%>
		 <tr>
		 <td class="greybluetext10" bgcolor="#E8EFF7" width=85% style={text-align:justify}>
		 <a name='#2'></a><b>Q2. <?if($lang=='E') echo "How is www.iibf.org.in different from other online educational web sites ?"; else echo "<font class='qn'>�ևԆևԲ�ߋ��-���ֻ�և��.���ָ���� (www.iibf.org.in) ���� ����ָ�� ���������� ��և���� ��� ����� �ָ��� ����� ��� ?"; ?>
	 		</b></td>
	 	 <td class="greybluetext10" bgcolor="#E8EFF7" align=right valign=top>
	 	 <b><?php if($lang=='E') print(" (Mark/s - ".$aSMarks[1].") ");else echo "<font class='qn'>(����-".$aSMarks[1].")</font>"; ?></b>
	 	 
	 	 <?if($lang=='E') echo "<a href='javascript:eraseAnswer(2)'>Erase Answer</a>";?>
	 	 <?if($lang=='H') echo "<a href='javascript:eraseAnswer(2)'><font class='qn'>��ָ ״֙�ֆ��� </font></a>";?>
	 	 
		 </td>
		 </tr>		 
		 </table>
		 <br>		
		<table border="0" cellspacing="0" cellpadding="2">
		<tr> 
			<td class="greybluetext10"><input name=answer2 type=radio value=1 <?php ($_POST['answer2'] == 1) ? print("checked") : print(""); ?>></td>
			<td class="greybluetext10"> a) <?if($lang=='E') echo "it offers special coaching";else echo "<font class='ans'>���� ׾ֿ���� ����ؓ��� ��Ϥ�֮� ������� �� �</font>";?></td>
		</tr>
		<tr>
			<td class="greybluetext10"><input name=answer2 type=radio value=2 <?php ($_POST['answer2'] == 2) ? print("checked") : print(""); ?>></td>
			<td class="greybluetext10">b) <?if($lang=='E') echo "offers discounts";else echo "<font class='ans'>���� ����� ��Ϥ�֮� ������� ��� �</font>";?></td>
		</tr>
		<tr> 
			<td class="greybluetext10"><input name=answer2 type=radio value=3 <?php ($_POST['answer2'] == 3) ? print("checked") : print(""); ?>></td>
			<td class="greybluetext10">c) <?if($lang=='E') echo "follows a learning methodology";else echo "<font class='ans'>���� ��֮���ָ�� �����ֻ�� ��� �����ָ��� ������� ��� �</font>";?></td>
		</tr>
		<tr>
			<td class="greybluetext10"><input name=answer2 type=radio value=4 <?php ($_POST['answer2'] == 4) ? print("checked") : print(""); ?>></td>
			<td class="greybluetext10">d) <?if($lang=='E') echo "free registration";else echo "<font class='ans'>���� ׮�:����� ��ו�Ù����֮� ��Ϥ�֮� ������� ��� �</font>";?></td>
		</tr>
		</table>	
		<img alt="" src="tran.gif" width=1 height=4><br clear=all></td>
		</tr>
		<tr> 
		<td colspan=2 class="greybluetext10" bgcolor="#E8EFF7" valign=top>
		 <table border="0" cellspacing="0" cellpadding="0" width=100%>
		 <tr>
		 <td class="greybluetext10" bgcolor="#E8EFF7" width=85% style={text-align:justify}>
		 <a name='#3'></a><b>Q3. <?if($lang=='E') echo "How is the course content structured in www.iibf.org.in?"; else echo "<font class='qn'>�ևԆևԲ�ߋ��-���ֻ�և��.���ָ���� (www.iibf.org.in) ��� ��֚������� ׾��ֵ� ������ ��Ӹ�ד֟� ���� ��֟�� ��� ?</font>";?>
	 		</b></td>
	 	 <td class="greybluetext10" bgcolor="#E8EFF7" align=right valign=top>
	 	 <b><?php if ($lang=='E') print(" (Mark/s - ".$aSMarks[2].") "); else echo "<font class='qn'>(����-".$aSMarks[2].")</font>";?></b>
	 	 
	 	  <?if($lang=='E') echo "<a href='javascript:eraseAnswer(3)'>Erase Answer</a>";?>
	 	 <?if($lang=='H') echo "<a href='javascript:eraseAnswer(3)'><font class='qn'>��ָ ״֙�ֆ��� </font></a>";?>
	 	 
		 </td>
		 </tr>		 
		 </table>
		 <br>					
			<table border="0" cellspacing="0" cellpadding="2">
			<tr> 
				<td class="greybluetext10"><input name=answer3 type=radio value=1 <?php ($_POST['answer3'] == 1) ? print("checked") : print(""); ?>></td>
				<td class="greybluetext10"> a) <?if($lang=='E') echo "it offers special coaching";else echo "<font class='ans'>���� ׾ֿ���� ����ؓ��� ��Ϥ�֮� ������� �� �";?></td>
			</tr>
			<tr>
				<td class="greybluetext10"><input name=answer3 type=radio value=2 <?php ($_POST['answer3'] == 2) ? print("checked") : print(""); ?>></td>
				<td class="greybluetext10">b) <?if($lang=='E') echo "organized in a hierarchical manner";else echo "<font class='ans'>���� �׬����״��� ���� ��� �����ך��� ��� �</font>";?></td>
			</tr>
			<tr> 
				<td class="greybluetext10"><input name=answer3 type=radio value=3 <?php ($_POST['answer3'] == 3) ? print("checked") : print(""); ?>></td>
				<td class="greybluetext10">c) <?if($lang=='E') echo "follows a learning methodology";else echo "<font class='ans'>���� ������ ���ֵ֮� �����ֻ�� ��� �����ָ��� ������� ��� �</font>";?></td>
			</tr>
			<tr>
				<td class="greybluetext10"><input name=answer3 type=radio value=4 <?php ($_POST['answer3'] == 4) ? print("checked") : print(""); ?>></td>
				<td class="greybluetext10">d) <?if($lang=='E') echo "free registration";else echo "<font class='ans'>���� ׮�:����� ��ו�Ù����֮� ��Ϥ�֮� ������� ��� �</font>";?></td>
			</tr>
			</table>	
		<img alt="" src="tran.gif" width=1 height=4><br clear=all></td>
		</tr>
		<tr> 
			<td colspan=2 class="greybluetext10" bgcolor="#E8EFF7" valign=top>
		 <table border="0" cellspacing="0" cellpadding="0" width=100%>
		 <tr>
		 <td class="greybluetext10" bgcolor="#E8EFF7" width=85% style={text-align:justify}>
		 <a name='#4'></a><b>Q4. <?if($lang=='E') echo "What are the educational facilities offered at www.iibf.org.in?";else echo "<font class='qn'>�ևԆևԲ�ߋ��-���ֻ�և��.���ָ���� (www.iibf.org.in) �������� ���������� ���׾֬�֋� ��Ϥ�֮� ������� ��� ?</font>";?>
	 		</b></td>
	 	 <td class="greybluetext10" bgcolor="#E8EFF7" align=right valign=top>
	 	 <b><?php if ($lang=='E') print(" (Mark/s - ".$aSMarks[3].") ");else echo "<font class='qn'>(����-".$aSMarks[3].")</font>"; ?></b>
	 	 
	 	 
	 	 <?if($lang=='E') echo "<a href='javascript:eraseAnswer(4)'>Erase Answer</a>";?>
	 	 <?if($lang=='H') echo "<a href='javascript:eraseAnswer(4)'><font class='qn'>��ָ ״֙�ֆ��� </font></a>";?>
	 	 
		 </td>
		 </tr>		 
		 </table>
		 <br>			
			<table border="0" cellspacing="0" cellpadding="2">
			<tr> 
				<td class="greybluetext10"><input name=answer4 type=radio value=1 <?php ($_POST['answer4'] == 1) ? print("checked") : print(""); ?>></td>
				<td class="greybluetext10"> a) <?if($lang=='E') echo "abundance of choices";else echo "<font class='ans'>�ֵ֮� ��� ��ϓ������</font>";?> </td>
			</tr>
			<tr>
				<td class="greybluetext10"><input name=answer4 type=radio value=2 <?php ($_POST['answer4'] == 2) ? print("checked") : print(""); ?>></td>
				<td class="greybluetext10">b) <?if($lang=='E') echo "organized in a hierarchical manner";else echo "<font class='ans'>���� �׬����״��� ����� ��� �����ך��� ���</font>";?></td>
			</tr>
			<tr> 
				<td class="greybluetext10"><input name=answer4 type=radio value=3 <?php ($_POST['answer4'] == 3) ? print("checked") : print(""); ?>></td>
				<td class="greybluetext10">c) <?if($lang=='E') echo "follows a learning methodology";else echo "<font class='ans'>���� ������ ���ֵ֮� �����ֻ�� ��� �����ָ��� ������� ���</font>";?></td>
			</tr>
			<tr>
				<td class="greybluetext10"><input name=answer4 type=radio value=4 <?php ($_POST['answer4'] == 4) ? print("checked") : print(""); ?>></td>
				<td class="greybluetext10">d) <?if($lang=='E') echo "free registration"; else echo "<font class='ans'>���� ׮�:����� ��ו�Ù����֮� ��Ϥ�֮� ������� ���</font>";?></td>
			</tr>
			</table>
		<img alt="" src="tran.gif" width=1 height=4><br clear=all></td>
		</tr>
		<tr> 
		<td colspan=2 class="greybluetext10" bgcolor="#E8EFF7" valign=top>
		 <table border="0" cellspacing="0" cellpadding="0" width=100%>
		 <tr>
		 <td class="greybluetext10" bgcolor="#E8EFF7" width=85% style={text-align:justify}>
		 <a name='#5'></a><b>Q5. <?if($lang=='E') echo "What does a learner gain from www.iibf.org.in?"; else echo "<font class='qn'>��� ���������ֵ�� ���� �ևԆևԲ�ߋ��-���ֻ�և��.���ָ���� (www.iibf.org.in) ��� ���� �����ֻ� ������ ��� ?</font>";?>
	 		</b></td>
	 	 <td class="greybluetext10" bgcolor="#E8EFF7" align=right valign=top>
	 	 <b><?php if($lang=='E') print(" (Mark/s - ".$aSMarks[4].") ");else echo "<font class='qn'>(����-".$aSMarks[4].")</font>"; ?></b>
	 	 
	 	 
	 	 <?if($lang=='E') echo "<a href='javascript:eraseAnswer(5)'>Erase Answer</a>";?>
	 	 <?if($lang=='H') echo "<a href='javascript:eraseAnswer(5)'><font class='qn'>��ָ� ״֙�ֆ�� </font></a>";?>		 
	 	 
	 	 </td>
		 </tr>		 
		 </table>
		 <br>						
			<table border="0" cellspacing="0" cellpadding="2">
			<tr> 
				<td class="greybluetext10"><input name=answer5 type=radio value=1 <?php ($_POST['answer5'] == 1) ? print("checked") : print(""); ?>></td>
				<td class="greybluetext10"> a) <?if($lang=='E') echo "abundance of choices";else echo "<font class='ans'> �ֵ֮� ��� ��ϓ������</font>";?></td>
			</tr>
			<tr>
				<td class="greybluetext10"><input name=answer5 type=radio value=2 <?php ($_POST['answer5'] == 2) ? print("checked") : print(""); ?>></td>
				<td class="greybluetext10">b) <?if($lang=='E') echo "organized in a hierarchical manner";else echo "<font class='ans'>�׬����״��� ����� ��� �����ך��� ���ֵ֮� �����ֻ��</font>";?></td>
			</tr>
			<tr> 
				<td class="greybluetext10"><input name=answer5 type=radio value=3 <?php ($_POST['answer5'] == 3) ? print("checked") : print(""); ?>></td>
				<td class="greybluetext10">c) <?if($lang=='E') echo "follows a learning methodology"; else echo "<font class='ans'>������ ���ֵ֮� �����ֻ�� ��� �����ָ��� </font>";?></td>
			</tr>
			<tr>
				<td class="greybluetext10"><input name=answer5 type=radio value=4 <?php ($_POST['answer5'] == 4) ? print("checked") : print(""); ?>></td>
				<td class="greybluetext10">d) <?if($lang=='E') echo "can access the study material at his / her own time";else echo "<font class='ans'>������/������ ׮֕�� �ִֵ� �ָ� ���ֵ֮� ��ִ����� ���� ��������</font>";?></td>
			</tr>
			</table>	
		<img alt="" src="tran.gif" width=1 height=4><br clear=all></td>
		</tr>
		<tr> 
			<td align="center" bgcolor="#D1E0EF" height="35"><input class='button' id='ClearFrm' type='button' value='Preview & Submit' name="sub" onClick='javascript:submitForm()'></td>
		</tr>
		</table>
		<br>
		</td>
	</tr>
	</table>
	<table border="0" cellpadding="0" cellspacing="0" width="780">
	<tr>
		<?php include("includes/footer.php");?>
	</tr>
	</table>
	<br>
</form>
</center>
</body>
</html>
<?php
//mysql_close($SLAVECONN);
?>
