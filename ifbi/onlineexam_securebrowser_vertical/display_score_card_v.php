<?php
/****************************************************
* Application Name            :  IIB 
* Module Name                 :  Exam Module
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  iib_candidate,iib_exam_subjects,iib_candidate_iway,iib_candidate_test
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              : 
* Created ON                  :  
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  22-01-2010
* Description                 :  Interface for the TA to candidate  view score card
*****************************************************/
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");  // always modified
header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");                          // HTTP/1.0
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkTALogin();
require("constants.inc");


if(isset($_POST['sub_exit']))
{
	header("Location:ta_options.php");
	exit;
}

	$todaydate = date("Y-m-d");
	$todate_start = $todaydate." 00:00:00 ";
	$todate_end = $todaydate. " 23:59:59 ";
	$membershipNo = $_POST['memno'];	
	
    $sqlSelect = " SELECT exam_code, subject_code, score, time_taken, result FROM iib_candidate_scores WHERE membership_no='$membershipNo' ".
					   " AND exam_date between '$todate_start' and '$todate_end' ORDER BY exam_date desc limit 1";	
	
	if ($commonDebug)
	{
		print("\n".$sqlSelect);
	}
	
  	$resSelect = @mysql_query($sqlSelect,$SLAVECONN) or errorlog('err05'," QUERY:$sqlSelect ".mysql_error($SLAVECONN));
  	list($examCode, $subjectCode, $score, $timeTaken, $result) = mysql_fetch_row($resSelect);
  	
  	$sqlSubject = "SELECT subject_name, pass_mark, grace_mark, total_marks , display_score, display_result, pass_msg, fail_msg,result_type, display_print,display_response FROM iib_exam_subjects WHERE ".
		" subject_code='$subjectCode' AND exam_code='$examCode'";
	if ($commonDebug)
	{
		print("\n".$sqlSubject);
	}
	
	$resSubject = @mysql_query($sqlSubject,$SLAVECONN) or errorlog('err05'," QUERY:$sqlSubject  ".mysql_error($SLAVECONN));
	$row = mysql_fetch_array($resSubject);
	
	$subjectName = $row['subject_name'];
	$passMark = $row['pass_mark'];
	$graceMark = $row['grace_mark'];
  	$totalMarks = $row['total_marks'];
  	$display_scr = $row['display_score'];
  	$display_res = $row['display_result'];
  	$display_response = $row['display_response'];
  	$passmsg = $row['pass_msg'];
  	$failmsg = $row['fail_msg'];
	$resultType = $row['result_type'];
	$display_print = $row['display_print'];
	
  	  	  	
  	$score = round($score);
  	
  	if ($score <= 0)
  	{
	  	$percentage = 0;
  	}
  	else
  	{
	  	$percentage = round((($score+0.0) / $totalMarks) * 100);
  	}
  	if ($commonDebug)
  	{
  		print("<br>score = ".$score);
  		print("<br>total marks = ".$totalMarks);
  		print("<br>percentage = ".$percentage);
  		print("<br>pass mark = ".$passMark);
  		print("<br>grace mark = ".$graceMark);
	}
	if ($result == 'P')
	{	
		if($resultType == 'PA')
			$displayResult = 'Pass';
		elseif($resultType == 'PR')
			$displayResult = 'Provisional';
		$msg=$passmsg;
	}
	else
	{		
		if($resultType == 'PA')
			$displayResult = 'Fail';
		elseif($resultType == 'PR')
			$displayResult = 'Provisional';
		$msg=$failmsg;
	}

  	$sqlMember = "SELECT name, address1, address2, address3, address4, address5, address6, pin_code FROM iib_candidate WHERE ".
  		"membership_no='$membershipNo'";
	if ($commonDebug)
	{
		print("\n".$sqlMember);
	}
	
	$resMember = @mysql_query($sqlMember,$SLAVECONN) or errorlog('err05'," iib_candidate  ".mysql_error($SLAVECONN));
	$row = mysql_fetch_array($resMember);
	
	$memberName = $row['name'];
	$c_addr1 = $row['address1'];
	$c_addr2 = $row['address2'];
	$c_addr3 = $row['address3'];
	$c_addr4 = $row['address4'];

	$c_addr5 = $row['address5'];
	$c_addr6 = $row['address6'];
	$c_pin = $row['pin_code'];
	
	$memberAddress = "";
	if ($c_addr1 != "")
		$memberAddress .= $c_addr1;
	if ($memberAddress != "") 
		$memberAddress .= " ";
	if ($c_addr2 != "")
		$memberAddress .= $c_addr2;
	if (($memberAddress != "") && ($c_addr2 != ""))
		$memberAddress .= " ";
	if ($c_addr3 != "")
		$memberAddress .= $c_addr3;
	if (($memberAddress != "") && ($c_addr3 != ""))
		$memberAddress .= " ";
	if ($c_addr4 != "")
		$memberAddress .= $c_addr4;
	if (($memberAddress != "") && ($c_addr4 != ""))
		$memberAddress .= " ";
	if ($c_addr5 != "")
		$memberAddress .= $c_addr5;
	if (($memberAddress != "") && ($c_addr5 != ""))
		$memberAddress .= " ";
	if ($c_addr6 != "")
		$memberAddress .= $c_addr6;
	if (($memberAddress != "") && ($c_addr6 != ""))
		$memberAddress .= " ";	
	if ($c_pin != "")
		$memberAddress .= $c_pin;		
	
	$sqlExam = "SELECT exam_name,exam_type FROM iib_exam WHERE exam_code='$examCode'";
	if ($commonDebug)
	{
		print("\n".$sqlExam);
	}
	
	$resExam = @mysql_query($sqlExam,$SLAVECONN) or errorlog('err05'," iib_exam  ".mysql_error($SLAVECONN));
	$row = mysql_fetch_array($resExam);
	
	$examName = $row['exam_name'];
	$exam_type=$row['exam_type'];
	
	$sqlCentre = "SELECT centre_code, DATE_FORMAT(exam_date, '%d/%m/%Y') examdate, ".
		" TIME_FORMAT(exam_time, '%H:%i') examtime FROM iib_candidate_iway WHERE ".
		" subject_code='$subjectCode' AND exam_code='$examCode' AND membership_no='$membershipNo' ";
	
	if ($commonDebug)
	{
		print("\n".$sqlCentre);
	}
	
	$resCentre = @mysql_query($sqlCentre,$SLAVECONN) or errorlog('err05'," iib_candidate_iway  ".mysql_error($SLAVECONN));
	$row = mysql_fetch_array($resCentre);
	
	$centreCode = $row['centre_code'];
	$examDate = $row['examdate'];
	$examTime = $row['examtime'];
	
	$sqlIWay = "SELECT iway_address1, iway_address2, iway_city, iway_state, iway_pin_code FROM iib_iway_details ".
		" WHERE centre_code='$centreCode'";
	if ($commonDebug)
	{
		print("\n".$sqlIWay);
	}
	$resIWay = @mysql_query($sqlIWay,$SLAVECONN) or errorlog('err05'," iib_iway_details  ".mysql_error($SLAVECONN));
	$row = mysql_fetch_array($resIWay);
	$iwayAddress = "";
	$row['iway_address1'] != "" ? $iwayAddress .= " ".$row['iway_address1'] : $iwayAddress .= "";
	//$row['iway_address2'] != "" ? $iwayAddress .= " ".$row['iway_address2'] : $iwayAddress .= "";
	$row['iway_city'] != "" ? $iwayAddress .= " ".$row['iway_city'] : $iwayAddress .= "";
	$row['iway_pin_code'] != "" ? $iwayAddress .= " ".$row['iway_pin_code'] : $iwayAddress .= "";
	$row['iway_state'] != "" ? $iwayAddress .= " ".$row['iway_state'] : $iwayAddress .= "";
	$row['iway_address2'] != "" ? $iwayAddress .= ",Contact No. :  ".$row['iway_address2'] : $iwayAddress .= "";
	
	if($msg != '')
	{					
		// Constant for displaying the time in the score card message
	     if($resultType == 'PR')
		{
		   $Constants_msg=array(
		       "/\bREP_MEMBERSHIPNAME\b/",
		       "/\bREP_SCORE\b/",
		       "/\bREP_TOTALMARK\b/",
		       "/\bREP_SUBJECTNAME\b/"
		     );
			 $replace_msg=array(
		       $memberName,
		       $score,
		       $totalMarks,
		       $subjectName
		     );
		}
		elseif($resultType == 'PA')
		{
			 $Constants_msg=array(
		       "/\bREP_MEMBERSHIPNAME\b/",
		       "/\bREP_SCORE\b/",
			   "/\bREP_TOTALMARK\b/",
		       "/\bREP_SUBJECTNAME\b/"
		     );
			 $replace_msg=array(
		       $memberName,
		       $score,
				$totalMarks,
		       $subjectName
		     );
		}

		   
		     $display_msg=preg_replace($Constants_msg,$replace_msg,$msg); 
		
	}
	else
	{
		$not_rep_msg=true;
	}

	if($not_rep_msg)
	$display_msg=$msg;
	
	
	
	$sql_select2 = "SELECT question_paper_no FROM iib_question_paper where membership_no='$membershipNo' AND exam_Code='$examCode' AND subject_code='$subjectCode'";
	$resSql2 = @mysql_query($sql_select2,$SLAVECONN)  or errorlog('err05'," SQL:$sql_select2 ".mysql_error($SLAVECONN));
	list($questionPaperNo) = mysql_fetch_row($resSql2);
	  		
	//To find number of question
	$sql_select3 = "SELECT display_order,answer FROM iib_question_paper_details where question_paper_no='$questionPaperNo' order by display_order";
	$resSql = @mysql_query($sql_select3,$SLAVECONN)  or errorlog('err05'," SQL:$sql_select3 ".mysql_error($SLAVECONN));
	$sqlct=mysql_num_rows($resSql);	

	//GET Responce from responce table
	$respArr = array();

	$sqlMaxIds = "select  max(id) from iib_response where question_paper_no ='$questionPaperNo' group by question_id";
	$resMaxIds = mysql_query($sqlMaxIds);
	$arrMaxId = array();
	while(list($maxId) = @mysql_fetch_row($resMaxIds)) {
		$arrMaxId[] = $maxId;
	}
	$maxIds = implode(',', $arrMaxId);

	if($maxIds == "" )
		$maxIds = "''";


	$sql_select_r = "select display_order,answer from iib_response where id in ($maxIds) order by display_order";
	$resSql_r = @mysql_query($sql_select_r,$SLAVECONN)  or errorlog('err05'," SQL:$sql_select3 ".mysql_error($SLAVECONN));
	$sqlct_r=mysql_num_rows($resSql_r);	
	if($sqlct_r>0){
		while($row_r=mysql_fetch_array($resSql_r))
		{
			$respArr[$row_r['display_order']] = $row_r['answer'];
		}
	}	 
	$qry="select img_path,alt_text from iib_logo_master where active='Y'";
	$res = mysql_query($qry,$SLAVECONN) or errorlog('err05'," QUERY:$qry  ".mysql_error($SLAVECONN));
	list($imgpath,$alttext)=mysql_fetch_row($res); 
	
$includeJS = '<script language="javascript" type="text/javascript" src="./includes/browser.js"></script>';
$includeJS .= "<script language='JavaScript'>history.go(1);</script>";
$bodyOnLoad = " onLoad='this.focus()'";
include("themes/default/header.php");
?>
<form method="post" name="exit_frm">
	<input type=hidden class=button name='sub_exit'>
	<div id="body_container_inner"><div class="ca_ex_res_wrapper"><div class="outerbox">
	<div class="innerbox">
    	<div class="innerbox_container_common">
        <div class="pager_header_common">Candidate Exam Result</div>
	<div class="content_container_common">
        <div class="content_container_box_common" >
        <div id="ca_ex_res_inner_box" >
	<table width="95%"  border="0" align="center" cellpadding="0" cellspacing="0" id="ca_ex_res_inner_box_inner_tb" >
            <tr>
              <td width="25%" align="right" class="tb_rw1">Membership Number</td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td width="72%" class="tb_rw2"><?php echo $membershipNo ?></td>
            </tr>
			 <tr>
              <td width="25%" align="right" class="tb_rw1">Name</td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td width="72%" class="tb_rw2"><?php echo $memberName ?></td>
            </tr>
            <tr>
              <td width="25%" align="right" class="tb_rw1">Address</td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td width="72%" class="tb_rw2"><?php echo $memberAddress ?></td>
            </tr>
            <tr>
              <td width="25%" align="right" class="tb_rw1">Examination</td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td width="72%" class="tb_rw2"><?php echo $examName ?><br /></td>
            </tr>
            <tr>
              <td width="25%" align="right" class="tb_rw1">Subject Name</td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td width="72%" class="tb_rw2"><?php echo $subjectName ?><br /></td>
            </tr>
            <tr>
              <td width="25%" align="right" class="tb_rw1">Examination Centre </td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td width="72%" class="tb_rw2"><?php echo $iwayAddress ?><br /></td>
            </tr>
            <tr>
              <td align="right" class="tb_rw1">Examination Date<br /></td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td class="tb_rw2"><?php echo $examDate ?><br /></td>
            </tr>
            <tr>
              <td align="right" class="tb_rw1">Examination Time<br /></td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td class="tb_rw2"><?php if ($examTime != "") print($examTime." Hrs."); ?><br /></td>
            </tr>
			<?php if($display_scr == 'Y') { ?>
            <tr>
              <td align="right" class="tb_rw1">Score</td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td class="tb_rw2"><?php echo $score ?></td>
            </tr>
			<?php } if($display_res == 'Y') { ?>	
			<tr>
              <td align="right" class="tb_rw1">Result</td>
              <td width="3%" align="left" class="tb_col">:</td>
              <td class="tb_rw2"><?php echo $displayResult ?></td>
            </tr>
			<? }?>				
			</table> 
			<div class="clear"></div><h1>Thank you for taking the online examination</h1>
<?php
			$attempt_cnt = $unattempt_cnt = 0;
			$v = $sqlct % 5;
			$i = $j =1;
			if($display_response == 'Y') {
?>
		    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="Report_tb">
			  <tr><th colspan="10">Response Report</th>	</tr>
			  <tr class="tb_rw3">
				<td>Q.No.</td>
				<td>Answer</td>
				<td>Q.No.</td>
				<td>Answer</td>
				<td>Q.No.</td>
				<td>Answer</td>
				<td>Q.No.</td>
				<td>Answer</td>
				<td>Q.No.</td>
				<td>Answer</td>
			  </tr>
		  <?PHP 
					$attempt_cnt = $unattempt_cnt = 0;
					$v = $sqlct % 5;					
					$i = $j =1;
					while($repData = mysql_fetch_array($resSql))
			  		{			   
						 $dis_order=$repData['display_order'];
						 if($respArr[$dis_order]==''){ 
						  		$dis_answer='--';
							 	$unattempt_cnt++;
						 }else{
						  	 	$dis_answer=$respArr[$dis_order];
						    	$attempt_cnt++;
						 } 
						 if($i==1){
						  	echo '<tr  class="tb_rw4"><td class="tb_rw5" >'.$dis_order.'</td><td class="tb_rw5" >'. $dis_answer.'</td>';
							$i++;
							if($j==$sqlct && $v==1)
							{
								echo '<td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  </tr>';
							}								
						 }else if($i==2){
						  	echo '<td class="tb_rw5" >'.$dis_order.'</td><td class="tb_rw5" >'. $dis_answer.'</td>';
							$i++;
							if($j==$sqlct && $v==2)	{
								echo '<td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
								      <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  </tr>';
							}								
						  }else if ($i==3){ 						  
						  	echo '<td class="tb_rw5" >'.$dis_order.'</td><td class="tb_rw5" >'. $dis_answer.'</td>';
						    $i++;
							if($j==$sqlct && $v==3) {
								echo '<td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
								      <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									  </tr>';
							}
						  }else if ($i==4){ 						  
								echo '<td class="tb_rw5" >'.$dis_order.'</td><td class="tb_rw5" >'.$dis_answer.'</td>';
								$i++;
								if($j==$sqlct && $v==4) {
									echo '<td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
									      <td class="tb_rw5" >&nbsp;</td><td class="tb_rw5" >&nbsp;</td>
										  </tr>';
								}
						  }
						  else if ($i==5){ 						  	
							   echo '<td class="tb_rw5" >'.$dis_order.'</td><td class="tb_rw5" >'.$dis_answer.'</td>';
							   echo '</tr>';
							   $i=1;							   
	   	                  }	
						 $j++; 					  						 					  										
                 	}											
					?>
              </table>
<?php

			} else {
				while($repData = mysql_fetch_array($resSql))
				{
					$dis_order=$repData['display_order'];
					if($respArr[$dis_order]==''){
						$dis_answer='--';
						$unattempt_cnt++;
					}else{
						$dis_answer=$respArr[$dis_order];
						$attempt_cnt++;
					}
				}
			}
?>
		  <div id="tot_no_quest">Total number of question attempted</b><span class="N-attempt"><?php echo $attempt_cnt?></span></div>
          <div id="tot_no_quest_right">Unattempted<span class="N-unattempt"><?php echo $unattempt_cnt?></span></div>     
        </div></div></div>			
            <div class="button_container_common">
			<?php if($display_print == 'Y'){
					if($exam_type != 4){ ?>
						<input name="prtbtn" class="green_button" type="button" onClick="javascript:window.print()" value="Print" /> 
			<?php } }?>
			<input name="exitbtn" class="green_button" type="button" onClick="document.exit_frm.submit()" value="Exit" />
            </div>
        </div> </div>
</div></div> 
  </div>
	</form>
<?php 
include("themes/default/footer.php");
//mysql_close($SLAVECONN);

?>
