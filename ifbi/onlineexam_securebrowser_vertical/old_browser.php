<?php /****************************************************
* Application Name            :  IIB 
* Module Name                 :  TA Options 
* Revision Number             :  1
* Revision Date               :  -
* Table(s)                    :  -
* Tables used for only selects:  -
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :  -
* Document/Reference Material :  -
* Created By	              :  -
* Created ON                  :  -
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  22-01-2010
* Description                 :  Interface for Browser details
*****************************************************/
header("Expires: Mon, 26 Jul 1920 05:00:00 GMT");    // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");     // always modified
header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");  

require_once("dbconfig.php");
$useragent = $_SERVER['HTTP_USER_AGENT'];
//$redirect = (!empty($_GET['logout']) && isset($_GET['logout'])) ? $_GET['logout'] : '';
if(trim($useragent) == 'Sify Browser')
{				
	header("Location:login.php");
	exit;		
}
//print_r($_SERVER);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?PHP echo TITLE?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<script language='JavaScript'>
history.go(1);
</script>
<style type="text/css">
<!--
.alertmsg{font-family:Verdana, Arial, Helvetica, sans-serif;font-style:normal;font-weight:bold;color:blue;text-decoration:none;}
.style1 {
	font-size: 16px;
	color: #FF0000;
}
.style2 {font-size: 14px}
.style5 {color: #FF0000}
.style6 {font-size: 12px}
-->
</style>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<center>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <? include("includes/header.php");?>
    </tr>
    <tr> </tr>
    <tr>
      <td width="100%" height="315" align="left" valign="top" background="images/tile.jpg" ><table width="79%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td height="160" align="left" valign="middle" bgcolor="#E8EFF7" class="alertmsg style1"><table width="100%" border="0" align="center" cellpadding="5" cellspacing="5">
                <tr>
                  <td><div align="center" class="style2">
                    <p align="left">This application will work only with Sify I-Test Browser. <a href="./browser/I-Testsetup.exe">Click here</a> to download/install. Before installing please check in your system it's already installed or not. </p>
                    <p align="left"> For Sify E-Port's it will be installed by iway admin . </p>
                    <p align="left">Start Menu </p>
                    <p align="left">1.Start-&gt; Programs -&gt; Sify-I-Test-&gt; Sify-I-Test .exe </p>
                  </div></td>
                </tr>
                <tr>
                  <td><div align="left"><a href="./font/font.zip">Click here</a> <span class="style2">to download Hindi Fonts.</span></div></td>
                </tr>
                <tr>
                  <td><span class="style6"><br>
				    <span class="style5">1. Download the font.zip file and extract it. 
                    <br>
                    2. Click on Start Menu -> Settings -> Control Panel. <br>
                  3. Copy extracted folder  files(*.ttf) , then paste those files into Fonts folder.</span></span></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          </table>
      </td>
    </tr>
    <tr>
      <?php include("includes/footer.php");?>
    </tr>
  </table>
</center>
</body>
</html>
