<?php
/****************************************************
* Application Name            :  IIB
* Module Name                 :  Connecting to DB
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :
* Tables used for only selects:  -
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  -
* Output File(s)              :  -
* Document/Reference Material :
* Created By                  : BalaMurugan.S
* Created ON                  : 12-01-2010
* Last Modified By            :  
* Last Modified Date          :  
* Description                 : MYSQL Connection config file
*****************************************************/
//	Master DB for all writes i.e., DMLs 
$masterhost="221.135.110.217";  

//	Slave DB IP 
$slavehost = "221.135.110.196"; 


//DB user name, password used to connect to DB
$dbuser = "onlineexamuser";
$dbpwd = "onlineexampwd";

//Database name to which the connection is to be opened
$db="onlineexam_generic";



$MASTERCONN='';
$SLAVECONN='';
define('TITLE','Sify Online Exam');
if(function_exists('date_default_timezone_set'))
		date_default_timezone_set('Asia/Calcutta');
		
/**	
	Function to establish connection with the Mysql Slave server
	Output: returns Slave connection String if successfull else error message
**/
function slaveConnect($erroption=true)
{
	global $slavehost;
    global $dbuser;
	global $dbpwd;
	global $db;
	global $SLAVECONN;
	if(!is_resource($SLAVECONN)){
		$SLAVECONN=@mysql_connect($slavehost,$dbuser,$dbpwd);
		$i=0;
		while(!is_resource($SLAVECONN))
		{				
			if($i < 3 && !is_resource($SLAVECONN))
			{						
				sleep(2);			
				$SLAVECONN=@mysql_connect($slavehost,$dbuser,$dbpwd);	
				$i=$i+1;				
			}else{			   
			   break;				
			 }  
		}		
		if(!is_resource($SLAVECONN))		 		    			
				 if($erroption)
				 	 errorlog('err01',mysql_error($SLAVECONN));				 
		}									
		if(is_resource($SLAVECONN)){					
			$dbupd = mysql_select_db($db,$SLAVECONN)  or errorlog('err02',mysql_error($SLAVECONN));				
		}						
}
/**
	Function to establish connection with the Mysql Master server
	Output: returns Master connection String if successfull else error message
**/
function masterConnect($erroption=true)
{		
	global $masterhost;
    global $dbuser;
	global $dbpwd;
	global $db;
	global $MASTERCONN;
	if(!is_resource($MASTERCONN)){
		$MASTERCONN=@mysql_connect($masterhost,$dbuser,$dbpwd);		
		$i=0;
		while(!is_resource($MASTERCONN))
		{				
			if($i < 3 && !is_resource($MASTERCONN))
			{						
				sleep(2);			
				$MASTERCONN=@mysql_connect($masterhost,$dbuser,$dbpwd);	
				$i=$i+1;				
			}else{			   
			   break;				
			 }  
		}		
		if(!is_resource($MASTERCONN))		 		    			
				 if($erroption)
				 	errorlog('err03',@mysql_error($MASTERCONN));				 
		}									
		if(is_resource($MASTERCONN)){					
			$dbupd = mysql_select_db($db,$MASTERCONN)  or errorlog('err04',mysql_error($MASTERCONN));				
		}	
}

/**
	Function to close Mysql slave connection
	Output: NIL
**/
function slaveConnectClose()
{
	global $SLAVECONN;
	if(is_resource($SLAVECONN))
	  @mysql_close($SLAVECONN);
}

/**
	Function to close Mysql master connection
	Output: NIL
**/
function masterConnectClose()
{
	global $MASTERCONN;
	if(is_resource($MASTERCONN)){
		@mysql_close($MASTERCONN);
	}	
}

function errorlog($errcode,$additionalmsg='') 
{
	$arrErrorDetails=array(
		'err01'=>"Connection to MySql slave database failed.",
		'err02'=>"Slave Database - select may not exists.",	
		'err03'=>"Connection to MySql master database failed.",	
		'err04'=>"Master Database - update may not exists.",
		'err05'=>"select failed for table ",
		'err06'=>"update failed for table ",
		'err07'=>"delete failed for table ",
		'err08'=>"insert failed for table ",					
		'err10'=>" Question Paper Not Available. ",
		'err11'=>" Question Paper Assignment Not Done. ",																					 
	);
	$filename="./applicationerror_log/errorlog_".date('ymd').".log";
	$msg = $arrErrorDetails[$errcode];
	$msg.= " ".$additionalmsg. " | ".date('d-m-Y h:i:s A')." | File:".$_SERVER['REQUEST_URI']."\r\n";
	error_log($msg, 3, $filename);
	ob_end_clean();
	echo "<h2>Error Code: ".$errcode. "  <br/>Please contact administrator...";
	exit;
}


?>
