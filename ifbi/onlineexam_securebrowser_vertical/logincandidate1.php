<?php
/****************************************************
* Application Name            :  IIB 
* Module Name                 :  Exam Module
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  iib_candidate,iib_exam_subjects,iib_candidate_iway,iib_candidate_test
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              : 
* Created ON                  :  
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  22-01-2010
* Description                 :  Interface for the candidate to confirm exam details
*****************************************************/
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkCandidateLogin();
require("constants.inc");

$todaydate=date('Y-m-d');
$exam_code = $_SESSION['ex'];
$subject_code = $_SESSION['sc'];
$selExamTime = $_SESSION['et'];
$memno=$_SESSION['memno'];

//Added for BOI starts here
$option_subject = $_REQUEST['post_subcode']; //optional subject code
$instruction_subcode=$subject_code;  //optional id
if(isset($option_subject))
{
  $subject_code =$option_subject; 
  $_SESSION['sc'] = $option_subject;

	$sql_candidateiway = "update iib_candidate_iway set subject_code='$subject_code' where membership_no='$memno' and exam_code='$exam_code' and subject_code='$instruction_subcode'";
	mysql_query($sql_candidateiway,$MASTERCONN) or errorlog('err06'," QUERY:$sql_candidateiway  ".mysql_error($MASTERCONN));

	$sql_candidatetest = "update iib_exam_candidate set subject_code='$subject_code' where membership_no='$memno' and exam_code='$exam_code' and subject_code='$instruction_subcode'";
	mysql_query($sql_candidatetest,$MASTERCONN) or  errorlog('err06'," QUERY:$sql_candidatetest  ".mysql_error($MASTERCONN));

}
//Added for BOI ends here

$query1 = "SELECT exam_name,exam_type FROM iib_exam WHERE exam_code='$exam_code' ";
$result1=@mysql_query($query1,$SLAVECONN)  or errorlog('err05'," QUERY:$query1  ".mysql_error($SLAVECONN));
list($exam_name,$exam_type)=@mysql_fetch_row($result1);
$_SESSION['exam_name'] = $exam_name;
$_SESSION['exam_type'] = $exam_type;

$snoquery1 = "SELECT serverno FROM autofeed";
$snoresult1=@mysql_query($snoquery1,$SLAVECONN)  or errorlog('err05'," QUERY:$snoquery1  ".mysql_error($SLAVECONN));
list($serverno)=@mysql_fetch_row($snoresult1);
$_SESSION['serverno'] = $serverno;


$query2 = "SELECT total_marks,question_display,subject_duration,subject_name,result_type,display_score,display_result,pass_mark,grace_mark,pass_msg,fail_msg,answer_shuffling,option_flag,option_id,max_no_of_questions,break_duration,duration_prevent,display_print,display_mark,languages,display_sectionname,display_casequestion,display_noofquestions,answer_saving_type,answer_saving_interval,display_donotwanttoanswer,timelog_enable,display_response,roundoff_score FROM iib_exam_subjects WHERE subject_code='$subject_code' ";
$result2=@mysql_query($query2,$SLAVECONN) or errorlog('err05'," QUERY:$query2  ".mysql_error($SLAVECONN));
list($totalMarks,$question_display,$total_duration,$subject_name,$result_type,$display_score,$display_result,$pass_mark,$grace_mark,$pass_msg,$fail_msg,$answer_shuffling,$option_flag,$option_id,$max_no_of_questions,$break_duration,$duration_prevent,$display_print,$display_mark,$languages,$display_sectionname,$display_casequestion,$display_noofquestions,$answer_saving_type,$answer_saving_interval,$donotwanttoanswer,$timelogenable,$display_response,$roundoff_score) = mysql_fetch_row($result2);
$_SESSION['subject_name']=$subject_name;
$_SESSION['total_marks']=$totalMarks;
$_SESSION['question_display']=$question_display;
$_SESSION['subject_duration']=$total_duration;
$_SESSION['result_type']=$result_type;
$_SESSION['display_score']=$display_score;
$_SESSION['display_result']=$display_result;
$_SESSION['display_response']=$display_response;
$_SESSION['roundoff_score']=$roundoff_score;
$_SESSION['pass_mark']=$pass_mark;
$_SESSION['grace_mark']=$grace_mark;
$_SESSION['pass_msg']=$pass_msg;
$_SESSION['fail_msg']=$fail_msg;
$_SESSION['answer_shuffling']=$answer_shuffling;
$_SESSION['option_flag']=$option_flag;
$_SESSION['option_id']=$option_id;
$_SESSION['max_no_of_questions']=$max_no_of_questions;
$_SESSION['break_duration']=$break_duration;
$_SESSION['duration_prevent']=$duration_prevent;
$_SESSION['display_print']=$display_print;
$_SESSION['display_mark']=$display_mark;
$_SESSION['languages']=$languages;
$_SESSION['display_sectionname']=$display_sectionname;
$_SESSION['display_casequestion']=$display_casequestion;
$_SESSION['display_noofquestions']=$display_noofquestions;
//$_SESSION['answer_saving_type']=$answer_saving_type;
//$_SESSION['answer_saving_interval']=$answer_saving_interval;
$_SESSION['answer_saving_type']="Q";
$_SESSION['answer_saving_interval']=0;
$_SESSION['donotwanttoanswer'] = $donotwanttoanswer;
$_SESSION['timelog_enable'] = $timelogenable;
$_SESSION['exam_view'] = "V";


$sql="select name,address1,address2,address3,address4,address5,address6,pin_code from iib_candidate where membership_no='".$memno."'";
$res=@mysql_query($sql,$SLAVECONN) or errorlog('err05'," QUERY:$sql ".mysql_error($SLAVECONN));
$num=@mysql_num_rows($res);
if($res && $num>0) 
{
		list($name,$c_addr1,$c_addr2,$c_addr3,$c_addr4,$c_addr5,$c_addr6,$c_pin)=mysql_fetch_row($res);
		$caddress = "";
		$caddress = trim($c_addr1.''.$c_addr2.''.$c_addr3.' '.$c_addr4.''.$c_addr5.' '.$c_addr6.' '.$c_pin);					
}
$_SESSION['memname']=$name;
$_SESSION['address']=$caddress;

 $sqlSelect = "SELECT medium_code, institution_code FROM iib_exam_candidate where membership_no='$memno' AND exam_code='$exam_code' AND subject_code='$subject_code' ";
$resSelect = @mysql_query($sqlSelect,$SLAVECONN) or errorlog('err05'," QUERY:$sqlSelect  ".mysql_error($SLAVECONN));
list($mediumCode, $institutionCode) = @mysql_fetch_row($resSelect);
$_SESSION['mc']=$mediumCode;
$_SESSION['institutioncode']=$institutionCode;


$_SESSION['exam_load']=0;
unset($_SESSION['testid']);
unset($_SESSION['questionpaperno']);

$sqlLang = "select lang_code,lang_name from iib_languages where is_active='Y' ";
$resLang = mysql_query($sqlLang,$SLAVECONN);
while ($rowLang = mysql_fetch_array($resLang))
{
	if( ($rowLang['lang_code']=='HI') || ($rowLang['lang_code']=='HIDV') || ($rowLang['lang_code']=='HISH') || ($rowLang['lang_code']=='HISU') )
		$arrLang[$rowLang['lang_code']] = "HINDI";
	else
	  $arrLang[$rowLang['lang_code']] = $rowLang['lang_name'];
}

$includeJS = '<script language="javascript" type="text/javascript" src="./includes/browser.js"></script>';
$includeJS .= <<<JSSC
<script language='JavaScript'>
function confirmlang()
{
	if(document.frmlogincheck.lstmedium != undefined)
	{		
		var text=document.frmlogincheck.lstmedium.options[document.frmlogincheck.lstmedium.selectedIndex].text
		if(confirm("You have selected ' "+text+" ' Language. Do you want to continue?"))
			return true;
		else
			return false;						
	}else
		return true;	
}
history.go(1);
function validate()
{	
	if(confirmlang()){		
		document.getElementById("Confirm").disabled=true;
		document.getElementById("submitstatus").innerHTML='please wait submitting form...';
		document.frmlogincheck.action='instructions.php';
		document.frmlogincheck.submit();
	}
}
</script>
JSSC;

$bodyOnload = ' onKeyDown="return doKeyDown(event);"  ondragstart ="return Stop(event)" onselectstart="return Stop(event)"onmousedown="return Stop(event)" onmouseup="return Stop(event)"';

include("themes/default/header.php");

?>
<form name=frmlogincheck method=post>
    <div id="body_container_inner">
      <div class="innerbox_container_cadetails_wrapper"><div class="outerbox">
	<div class="innerbox">
    	<div class="innerbox_container_cadetails">
        	<div class="pager_header_common">Candidate Details</div>
            <div class="content_container_cadetails">
            	<div class="content_container_box_cadetails" >
                
                <div class="content_container_inner_box_cadetails">
        
<table width="95%"  border="0" align="center" cellpadding="0" cellspacing="0" class="content_container_inner_box_cadetails_inner_tb"  >
            <tr>
              <td width="25%" height="30" align="right" class="tb_rw1">Membership Number</td>
              <td width="3%" height="30" class="tb_col">:</td>
              <td width="72%" height="30" class="tb_rw2"><?php echo $memno;?></td>
            </tr>
            <tr>
              <td width="25%" height="30" align="right" class="tb_rw1">Name</td>
              <td width="3%" height="30" class="tb_col">:</td>
              <td width="72%" height="30" class="tb_rw2"><?php echo $name;?></td>
            </tr>
            <tr>
              <td width="25%" height="30" align="right" class="tb_rw1">Address</td>
              <td width="3%" height="30" class="tb_col">:</td>
              <td width="72%" height="30" class="tb_rw2"><?php echo nl2br($caddress);?></td>
            </tr>
            <tr>
              <td width="25%" height="30" align="right" class="tb_rw1">Exam Code</td>
              <td width="3%" height="30" class="tb_col">:</td>
              <td width="72%" height="30" class="tb_rw2"><?php echo $exam_code."($exam_name)";?></td>
            </tr>
            <tr>
              <td width="25%" height="30" align="right" class="tb_rw1">Subject Code</td>
              <td width="3%" height="30" class="tb_col">:</td>
              <td width="72%" height="30" class="tb_rw2"><?php echo $subject_code."($subject_name)";?></td>
            </tr>
            <tr>
              <td height="30" align="right" class="tb_rw1">Medium Code</td>
              <td width="3%" height="30" class="tb_col">:</td>
              <td height="30" class="tb_rw2">
<?PHP
if($_SESSION['ta_override']!='Y'){  ?>
<select name='lstmedium' class="dropdownbox">
<?php 
	$aDispMedium['EN']='ENGLISH';							
	$sql_hindi_chk = "select lang_code from iib_section_questions_unicode where exam_code ='$exam_code'and subject_code = '$subject_code' group by lang_code";
	$res_hindi_chk = @mysql_query($sql_hindi_chk,$SLAVECONN);
	$nhindi= @mysql_num_rows($res_hindi_chk);

	if($nhindi > 0){
		while($rlang=mysql_fetch_array($res_hindi_chk)){
			if(array_key_exists($rlang['lang_code'],$arrLang)) {
				$aDispMedium[$rlang['lang_code']]=$arrLang[$rlang['lang_code']]; 
			}														
		}
	}
	foreach ($aDispMedium as $key=>$val){	                        
		print("<option value='$key' ");
		if ($key == $mediumCode) {
			print(" selected ");
		}
		print(">$val</option>");
	}
?>
</select>
<?PHP }else echo $arrLang[$_SESSION['mc']];?>
               </td>
            </tr>
          </table>
        </div>
            	</div>
            </div>
            <div class="button_container_common">
              <input class='green_button' id='Confirm' type='button' value='Confirm' name="confirm" <?PHP if($_SESSION['subject_name']!='') echo 'onclick="javascript:return validate();"'?> />
              <br/><span id="submitstatus" class="errormsg"></span>							  
              <input type="hidden" name="old_mediumcode" id="old_mediumcode" value="<?PHP echo $mediumCode?>" />
              <!--  Added for BOI -->
              <input type="hidden" name="opt_subcode" value='<?=$option_subject?>' />
              <input type="hidden" name="instruction_subcode" value="<?=$instruction_subcode?>" />
              <!--  Added for BOI ends here-->
            </div>
        </div>
    
    </div>
</div></div>
    </div>
</form>
<?php include("themes/default/footer.php");
//mysql_close($MASTERCONN);
//mysql_close($SLAVECONN);
?>
