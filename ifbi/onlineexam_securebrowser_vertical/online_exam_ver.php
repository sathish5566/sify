<?php ob_start();
/****************************************************
* Application Name            :  IIB 
* Module Name                 :  Online Exam Page
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  iib_candidate_test,iib_section_questions,iib_section_questions_hindi,iib_case_master,iib_case_master_hindi
* Tables used for only selects:  iib_exam_candidate,iib_exam_subjects
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  
* Created ON                  :  
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  14-Dec-09
* Description                 :  Interface for taking the exam
*****************************************************/
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");   
set_time_limit(0);
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkCandidateLogin();
//checkQuestionPaper();
require_once("constants.inc");

$membershipNo = $_SESSION['memno'];
$subjectCode = $_SESSION['sc'];
$examCode = $_SESSION['ex'];		
$mediumcode = $_SESSION['mc'];		
$questionPaperNo = $_SESSION['questionpaperno'];
$taOverride = $_SESSION['ta_override'];
$loginOverride = $_SESSION['login_override'];
$subjectName=$_SESSION['subject_name'];
$subjectDuration=$_SESSION['subject_duration'];
$serverno=$_SESSION['serverno'];

$arrQpDisOrder=array();
$sqlSelect = "SELECT question_id, answer, display_order FROM iib_question_paper_details WHERE question_paper_no='$questionPaperNo' ORDER BY display_order ";
$result = @mysql_query($sqlSelect,$SLAVECONN) or errorlog('err05'," QUERY: $sqlSelect  ".mysql_error($SLAVECONN));
$prevAnswers = '';
$arrprevAnswers = array();

while($r=mysql_fetch_array($result))
{
        if( $r['answer'] != ''){
                //$prevAnswers.=$r['question_id'].':'.$r['answer'].',';
                $arrprevAnswers[$r['question_id']]=$r['answer'];
        }else{
           //$prevAnswers.=$r['question_id'].':0'.',';
           $arrprevAnswers[$r['question_id']]=0;
         }
        $arrQpDisOrder[$r['question_id']]= $r['display_order'] ;
        $arrDisOrder[$r['display_order']]= $r['question_id'] ;
}
$prevAnswers=substr($prevAnswers,0,strlen($prevAnswers)-1);

$marked_string = $_REQUEST['marked_string'];

/***** photo Display ***************/

$p1=$canPhotoPath.'P'.$membershipNo.'.jpg';
$p2=$canPhotoPath.'p'.$membershipNo.'.jpg';
$sp1=$canSignPath.'S'.$membershipNo.'.jpg';
$sp2=$canSignPath.'s'.$membershipNo.'.jpg';
//$memPhoto='./images/user.jpg';
$memPhoto='./themes/default/images/photo_no.jpg';
//$memSignPhoto='./images/cm-sign.jpg';
$memSignPhoto='./themes/default/images/sign.jpg';
if(file_exists($p1))
	$memPhoto=$p1;
else if(file_exists($p2))
	$memPhoto=$p2;
else{
	//$memPhoto='./images/user.jpg';
	$memPhoto='./themes/default/images/photo_no.jpg';
}
if(file_exists($sp1))
	$memSignPhoto=$sp1;
else if(file_exists($sp2))
	$memSignPhoto=$sp2;
else {
	//$memSignPhoto='./images/cm-sign.jpg';
	$memSignPhoto='./themes/default/images/sign.jpg';
}	
/***** photo Display ***************/
if( isset($_SESSION['ltime']) && ( $_SESSION['ta_override'] == 'Y') ){
	$timeLeft = $_SESSION['ltime'];
	$total_time_candi = $timeLeft;
}else{
	$total_time_candi = $_SESSION['subject_duration'];
	$timeLeft=$total_time_candi;
}

//$starttime = getDateTime();
$starttime = date("Y-m-d H:i:s");
$starttimehrs =  substr($starttime,10,strlen($starttime));

if($taOverride == 'Y')
{
	$upqry=" UPDATE iib_candidate_test set current_session='N', browser_status='closed' where membership_no='$membershipNo' and exam_code='$examCode' and subject_code='$subjectCode'";
	mysql_query($upqry,$MASTERCONN)  or errorlog('err06'," QUERY:$upqry  ".mysql_error($MASTERCONN));
}

$browser=$_SERVER['HTTP_USER_AGENT'];
$hostIP = $_SERVER['REMOTE_ADDR'];
$sqlInsert = "INSERT INTO iib_candidate_test (test_id,membership_no, exam_code, subject_code, question_paper_no, test_status, ta_override, login_override, start_time, last_updated_time, total_time, host_ip, browser_details, serverno) VALUES ('','$membershipNo', '$examCode', '$subjectCode','$questionPaperNo', 'IC', '$taOverride', '$loginOverride', '$starttime', '$starttime', '$total_time_candi', '$hostIP', '$browser', '$serverno')";
mysql_query($sqlInsert,$MASTERCONN)  or errorlog('err08'," QUERY:$sqlInsert  ".mysql_error($MASTERCONN));
if(mysql_affected_rows($MASTERCONN) == 1)
{
	$testID=0;
	$testID = mysql_insert_id($MASTERCONN);
	$_SESSION['testid'] = $testID;
	if($testID == 0 || $testID == '')
		errorlog('err08'," TEST ID RETRIVAL FAILED(mysql_insert_id) IN iib_candidate_test  ".mysql_error($MASTERCONN));

}else
 errorlog('err08'," QUERY:$sqlInsert  ".mysql_error($MASTERCONN));


$nRow = 0; 
$QPfilename=$QuestionPaperPath.$mediumcode."/".$examCode."_".$subjectCode."_".$questionPaperNo.".php";
/*Languages */
$sqlLang = "select lang_code,lang_name from iib_languages where is_active='Y' ";
$resLang = mysql_query($sqlLang,$SLAVECONN);
while ($rowLang = mysql_fetch_array($resLang))
{
	if( ($rowLang['lang_code']=='HI') || ($rowLang['lang_code']=='HIDV') || ($rowLang['lang_code']=='HISH') || ($rowLang['lang_code']=='HISU') )
		$arrLang[$rowLang['lang_code']] = "HINDI";
	else
	  $arrLang[$rowLang['lang_code']] = strtoupper($rowLang['lang_name']);
}
if(isset($_SESSION['languages'])){
	$arrLanguage=explode(',',$_SESSION['languages']);
}
$LangCount=count($arrLanguage);

$LANGLIST='<select  id=langlist name=langlist class="listbox" onchange="changeLangList()" >';
$FONTLANGUAGE=1;
for($i=0;$i < $LangCount;$i++){
	if( ($arrLanguage[$i]== 'HISU') || ($arrLanguage[$i] == 'HISH') || ($arrLanguage[$i] == 'HIDV') ){
		$FONTLANGUAGE=$arrLanguage[$i];
	}
	if($mediumcode == $arrLanguage[$i])
		$LANGLIST.="<option selected='selected' value=$arrLanguage[$i]>".$arrLang[$arrLanguage[$i]]."</option>";
	else	
		$LANGLIST.="<option value=$arrLanguage[$i]>".$arrLang[$arrLanguage[$i]]."</option>";
}
$LANGLIST.='</select>';

/*Languages */


$metaTagHD = '<meta http-equiv="pragma" content="no-cache"></meta>';
$metaTagHD .= '<meta http-equiv="imagetoolbar" content="no">';

//$includeStyleHD = '<style>.qnodisp_tag { Background-color:#FF8000;font-family="Verdana,Arial,Helvetica,sans-serif";font-weight:bold;font-size:10px;}</style>';

if( ($FONTLANGUAGE == "HISU") || ($FONTLANGUAGE == "HISH") || ($FONTLANGUAGE == "HIDV") ) {
$metaTagHD .= '<meta http-equiv="content-type" content="text/html; charset=x-user-defined">';
}else { 
$metaTagHD .= '<meta http-equiv="content-type" content="text/html; charset=utf-8">';
}

if ($FONTLANGUAGE == "HISU"){
	$includeStyleHD .= <<<CSSHD
	<STYLE TYPE="text/css">
	  @font-face {
		font-family: Suchi-Dev-0714W;
		font-style:  normal;
		font-weight: normal;
		src: url(./eot/SUCHIDE0.eot);
	  }
	.qn{font-family:'Suchi-Dev-0714W';font-weight:bold;font-size:18px}
	.ans{font-family:'Suchi-Dev-0714W';font-weight:normal;font-size:18px}
	  
	</STYLE>
CSSHD;
}else if ($FONTLANGUAGE == "HISH"){
	$includeStyleHD .= <<<CSSHD
	<STYLE TYPE="text/css">
	  @font-face {
		font-family: Shree-Dev-0714EW;
		font-style:  normal;
		font-weight: normal;
		src: url(./eot/SHREEDE0.eot);
	  }
	.qn{font-family:'Shree-Dev-0714EW';font-weight:bold;font-size:18px}
	.ans{font-family:'Shree-Dev-0714EW';font-weight:normal;font-size:18px}  
	</STYLE>
CSSHD;
}else if ($FONTLANGUAGE == "HIDV"){
	$includeStyleHD .= <<<CSSHD
	<STYLE TYPE="text/css">
	 @font-face {
		font-family: DVBW-TTYogeshEN;
		font-style:  normal;
		font-weight: normal;
		src: url(./eot/DVBWTTY0.eot);
	 }
	 .qn{font-family:'DVBW-TTYogeshEN';font-weight:bold;font-size:18px}
	 .ans{font-family:'DVBW-TTYogeshEN';font-weight:normal;font-size:18px}
	</STYLE>
CSSHD;
}

$includeStyleHD .= <<<CSSHD
<style type="text/css">
.greybluetextquestion{font-size: 16px;line-height:100%;}
.greybluetextans{font-size: 14px;line-height:100%;}
</style>
CSSHD;


$includeJS = <<<JSSC
<script language="javascript" type="text/javascript">
function tagQ(button_ctl, c)
{
        marked_string = document.getElementById("marked_string").value;
		//alert(marked_string+"~~"+button_ctl.value);
        if(button_ctl.value == " Tag ")
        {
		      if(marked_string != '')
                        var marked_string = marked_string+','+c;
                else
                        var marked_string = c;


                        document.getElementById("marked_string").value = marked_string;
                        document.getElementById("mark_btn_"+c).value = " De-Tag " ;
                        document.getElementById("mark_btn_"+c).title = " Click to De-Tag the question " ;
                        document.getElementById("q_disp"+arrQpDisOrder[c]).className= "qnodisp_tag";
        }
        else if(button_ctl.value == " De-Tag ")
        {
                var mstring = marked_string.split(",");

                var splittedValue = new Array();

                var j = 0 ;
                for(var i=0;i<mstring.length;i++)
                {
                        if(mstring[i] != c)
                        {
                                splittedValue[j] = mstring[i];
                                j++;
                        }
                }

                var reqValue = splittedValue.toString();
                document.getElementById("marked_string").value = reqValue;
                document.getElementById("mark_btn_"+c).value = " Tag " ;
                document.getElementById("mark_btn_"+c).title = " Click to Tag the question " ;
                if(arrprevanswers[c] == '')
                        document.getElementById("q_disp"+arrQpDisOrder[c]).className= "unattempt";
                else
                        document.getElementById("q_disp"+arrQpDisOrder[c]).className= "attempt";
        }
}
</script>
<script src='./includes/AC_RunActiveContent.js' language='javascript'></script>
JSSC;


$bodyOnload = ' onKeyDown="return doKeyDown(event);"  ondragstart ="return Stop(event)" onselectstart="return Stop(event)"  onload="callOnLoad()"';
include("themes/default/header1.php");
//sleep(5);
?>

<form name="frmonlineexam" method="post" action=""/>
	<input type="hidden" id="medium_code" name="medium_code" value='<?=$mediumcode ?>'/>
	<input type="hidden" id="exam_code" name="exam_code" value='<?=$examCode ?>'/>
	<input type="hidden" id="subject_code" name="subject_code" value='<?=$subjectCode ?>'/>
	<input type="hidden" id="memno" name="memno" value='<?=$membershipNo ?>'/>
	<input type="hidden" id="test_id" name="test_id" value='<?=$testID ?>'/>
	<input type="hidden" id="question_paper_no" name='question_paper_no' value='<?=$questionPaperNo ?>'/>
	<input type="hidden" id="ta_override"  name="ta_override" value='<?=$taOverride ?>'/>
	<input type="hidden" id="time_left" name='time_left' value='<?=$timeLeft ?>'/>
	<input type="hidden" id="total_time_candi" name='total_time_candi' value='<?=$total_time_candi?>'/>
	<input type="hidden" id="starttime" name='starttime' value='<?=$starttime ?>'/>	
	<input type="hidden" id="h_totalTime" name="h_totalTime" value='<?=$subjectDuration?>'/>
	<input type="hidden" id="h_startTime" name="h_startTime" value='<?=$starttimehrs?>'/>		
	<input type="hidden" id="ques_ID" name='ques_ID' />
	<input type="hidden" id="answer_ques"  name='answer_ques'/>
	<input type="hidden" id="last_clicked_question" name="last_clicked_question" value="0"/>
	<input type="hidden" id="auto_submit" name="auto_submit" />		
	<input type="hidden" id="exam_type" name="exam_type" value="<?php echo $_SESSION['exam_type'];?>" />
	<input type="hidden" id="max_no_of_questions" name="max_no_of_questions" value="<?PHP echo $_SESSION['max_no_of_questions'];?>" />
	<input type="hidden" id="duration_prevent" name="duration_prevent" value="<?PHP echo $_SESSION['duration_prevent'];?>" />
	<input type="hidden" id="connfailureduration" name="connfailureduration" value="0"/>		 
	<input type="hidden" id="timeLoss_start" name="timeLoss_start" value="0"/>		 
	<input type="hidden" id="display_noofquestions" name="display_noofquestions" value="<?PHP echo $_SESSION['display_noofquestions'];?>"/>
	<input type="hidden" id="question_display" name="question_display" value="<?PHP echo $_SESSION['question_display'];?>"/>
	<input type="hidden" id="case_display" name="case_display" value="<?PHP echo $_SESSION['display_casequestion'];?>"/>
	<input type="hidden" id="answer_saving_type" name="answer_saving_type" value="<?PHP echo $_SESSION['answer_saving_type'];?>"/>
	<input type="hidden" id="answer_saving_interval" name="answer_saving_interval" value="<?PHP echo $_SESSION['answer_saving_interval'];?>"/>
	<input type="hidden" id="timelog" name="timelog" value="<?PHP echo $_SESSION['timelog_enable'];?>"/>	
	<input type="hidden" id="qd_no" name='qd_no' />
    <input type='hidden' name='marked_string' id='marked_string' value='<?php echo htmlentities($marked_string);?>'>





<?PHP 
		if(file_exists($QPfilename) && filesize($QPfilename)!=0)		  
			require_once($QPfilename);
		else{
			require_once("questionpaper_ver.php");
		}

		echo '<script type="text/javascript">'."\n";
		echo arrayToJSObject($arrprevAnswers,'arrprevanswers');
		echo arrayToJSObject($arrQpDisOrder,'arrQpDisOrder');
		echo arrayToJSObject($arrDisOrder,'arrDisOrder');
		echo "</script>\n";
   ?>


<!-- Top Container -->
    <div id="top-container-wrapper">
      <div id="top-container" class="container">
        <div class="text_bold"><?php strtoupper(print($subjectName)); ?><span class='lang_disp'>Language : <?PHP echo $LANGLIST;?></span></div>		
        <div class="sam_quest_ti_lt_container">
        <div class="sam_quest_ti_lt_hrs" id="timeleft"></div>
          <div class="sam_quest_ti_lt_txt"><b>Time Left</b></div>
          
        </div>
        <div class="sam_quest_pap_container" id="questionpaper_div" style="position:relative;visibility:hidden;display:none;">
          <div class="sam_quest_left_container">
            <div class="sam_quest_heading_bg">
              <h1>Text Size</h1>
              <div class="sam_quest_heading_ft_div">
				<img style="cursor:hand" onclick="changeQPFontSize('0');" src="themes/default/images/A-.png" title="Decrease Font" width="26" height="26" />
				<img style="cursor:hand" onclick="changeQPFontSize('1');" src="themes/default/images/A+.png" title="Increase Font" width="26" height="26" />
				</div>
			</div>
			<div class='sam_group'>
			<?php echo $str;?>
			</div>
            <!-- <div class="sam_quest_text_container">
              <h1><b>Section : Sample Question Title</b></h1>
				  <div class="sam_quest_text_inner_container">
					<div id="samp_quest_div_quest_no">Q500.</div>
						<div id="samp_quest_div_quest_text" class="sam_quest_wid" >
						  <p>A bill discounted by the bank is returned unpaid as dishonored.  The Bank will a bill discounted by the bank is returned unpaid as 
							A bill discounted by the bank is returned unpaid as dishonored dishonored .............................</p>
						</div>
						<div class="sam_ans_text_container">
						  <ul>
							<li><input name="" type="radio" value=""  />
							  
							  debit the parties account immediately without prior notice</li>
							<li><input name="" type="radio" value=""  />
							  
							  debit the parties account with prior arrangement</li>
							<li>
							  <input name="" type="radio" value="" />
							  take a fresh bill and adjust the amount of the dishonored bill with the proceeds thereof</li>
							<li>
							  <input name="" type="radio" value="" />
							  keep the dishonoured bill pending for sometime</li>
							<li>
							  <input name="" type="radio" value="" />
							  none of these</li>
							<li>
							  <input name="" type="radio" value=""  />
							  debit the parties account immediately without prior notice</li>
							<li>
							  <input name="" type="radio" value="" checked="checked" />
							  debit the parties account with prior arrangement</li>
							<li>
							  <input name="" type="radio" value="" />
							  take a fresh bill and adjust the amount of the dishonored bill with the proceeds thereof </li>
							<li> <input name="" type="radio" value="" />
							  keep the dishonoured bill pending for sometime</li>
							<li>
							  <input name="" type="radio" value="" />
		none of these</li>
							<li>
							  <input name="" type="radio" value=""  />
							  debit the parties account immediately without prior notice</li>
							<li>
							  <input name="" type="radio" value="" checked="checked" />
							  debit the parties account with prior arrangement</li>
							<li>
							  <input name="" type="radio" value="" />
							  take a fresh bill and adjust the amount of the dishonored bill with the proceeds thereof</li>
							<li>
							  <input name="" type="radio" value="" />
							  keep the dishonoured bill pending for sometime</li>
							<li>
							  <input name="" type="radio" value="" />
							  none of these</li>
							<li>
							  <input name="" type="radio" value=""  />
							  debit the parties account immediately without prior notice</li>
							<li>
							  <input name="" type="radio" value="" checked="checked" />
							  debit the parties account with prior arrangement</li>
							<li>
							  <input name="" type="radio" value="" />
							  take a fresh bill and adjust the amount of the dishonored bill with the proceeds thereof</li>
							<li>
							  <input name="" type="radio" value="" />
							  keep the dishonoured bill pending for sometime</li>
							<li>
							  <input name="" type="radio" value="" />
							  none of these</li>
						  </ul>
						</div>
				  </div>
            </div>-->
          </div>
          <div class="sam_quest_right_top_container">
            <div class="sam_quest_photo"><img src="<?php echo $memPhoto?>" title="Photo" width="<?php echo $memPhotoWidth?>" height="<?PHP echo $memPhotoHeight?>" border="0"/></div>
            <div class="sam_quest_sig"><img src="<?php echo $memSignPhoto?>" title="Signature" width="<?PHP echo $signPhotoWidth?>" height="<?PHP echo $signPhotoHeight?>" border="0"/></div>
            <div class="sam_quest_ca_detail">
              <div id="can-dt2">
					<!--<span class="can-lable2">Subject Name</span> 
					<span class="details2"><input title="<?php strtoupper(print($subjectName)); ?>" type="text" name="subname" id="subname" class="bigtextbox" readonly="readonly" value="<?php strtoupper(print($subjectName)); ?>" /></span> -->
					
					<span class="can-lable2">Membership No. </span> 
					<span class="details2"><?PHP echo $membershipNo?></span> <span class="can-lable2">Candidate Name</span> <span class="details2"><?PHP echo ucwords($_SESSION['memname']);?></span> 
					
					<!--<span class="can-lable2">Language</span> 
					<span class="details2"><?PHP echo $LANGLIST;?></span>--></div>
            </div>
          </div>
          <div class="sam_quest_right_bottom_container"  id="preview_div">
            <div id="sam-que-att">
              <h1 style="padding-top:12px;"><b>Number of Questions</b></h1>
              <div id="que-scroll">  <div id="qnodetails"></div> </div>
              <div class="clear"></div>
              <div class="attempt_container">

              <div class="sam_attempt" id="no_attempt"></div>
              <div class="sam_attempt_txt">Attempted</div>
              
			  <div class="sam_tag" id="no_tagged"></div>
              <div class="sam_tag_text">Tagged</div>
              
			  <div class="sam_unattempt" id="no_unattempt"></div> 
              <div class="sam_unattempt_text">Unattempted</div>
			  
            </div>
          </div>
        </div>
       
      </div>
    </div>
    
    <!-- Link Container-->
    <div class="clear"></div>
    <div id="link-con">
      <div class="container">
	  <?PHP if($_SESSION['question_display'] =='A'){?>
				<div id="final_submit_btn_div"><div id="final_submit_arrow_div"><a href="#" onclick="preview_submit(0)"><img src="images/final_submit_arow.png" width="16" height="13" /></a></div><div id="final_submit_arrow_text"><a title="Click to submit Question Paper" href="#" onclick="preview_submit(0)">Final Submit</a></div></div>
	   <?PHP }else if($_SESSION['question_display'] == 'M' ){ ?>
				<div id="pre_btn_div"><div id="prev_arrow_div"><a href="#" onclick='javascript:prev()'><img src="images/prev_arrow.png" width="12" height="16" /></a></div><div id="prev_arrow_text"><a href="#" onclick='javascript:prev()' title="Click to view previous question" id="Prev">Previous Question</a></div></div>
    
				<div id="next_btn_div"><div id="next_arrow_text"><a title="Click to view next question" href="#" onclick="javascript:next()" id="Next">Next Question</a></div><div id="next_arrow_div"><a href="#" onclick='javascript:next()'><img src="images/next_arrow.png" width="12" height="16" /></a></div></div>

				
			
				<div id="final_submit_btn_div"><div id="final_submit_arrow_div"><a href="#" onclick="preview_submit(0)"><img src="images/final_submit_arow.png" width="16" height="13" /></a></div><div id="final_submit_arrow_text"><a title="Click to submit Question Paper" href="#" onclick="preview_submit(0)">Preview Submit</a></div></div>
	 <?PHP }else{ ?>
				<div id="pre_btn_div"><div id="prev_arrow_div"><a href="#" onclick='javascript:prev()'><img src="images/prev_arrow.png" width="12" height="16" /></a></div><div id="prev_arrow_text"><a href="#" onclick='javascript:prev()' title="Click to view previous question" id="Prev">Previous Question</a></div></div>
    
				<div id="next_btn_div"><div id="next_arrow_text"><a title="Click to view next question" href="#" onclick="javascript:next()" id="Next">Next Question</a></div><div id="next_arrow_div"><a href="#" onclick='javascript:next()'><img src="images/next_arrow.png" width="12" height="16" /></a></div></div>

				
			
				<div id="final_submit_btn_div"><div id="final_submit_arrow_div"><a href="#" onclick="preview_submit(0)"><img src="images/final_submit_arow.png" width="16" height="13" /></a></div><div id="final_submit_arrow_text"><a title="Click to submit Question Paper" href="#" onclick="preview_submit(0)">Preview Submit</a></div></div>
	 <?PHP }?>	
    <input type=hidden name="qindex" id="qindex" value='0' />

<div class="sam_quest_tag_btn_div" id="tagDiv"></div>
        <div class="sam_quest_er_ans_btn_div" id="eraseDiv"></div>
	
<!--	<div class="sam_quest_tag_btn_div">
		<input title='Click to $btName the question' type='button' name='mark_btn_$questionID' id='mark_btn_$questionID' class='yellow_button' value='TAG' onClick='javascript: tagQ(this, $questionID);'>
	</div>

	<div class="sam_quest_er_ans_btn_div">
		<input title='Click to erase the answer' onclick="eraseAnswer($qn_no)" class="red_button" type="button" value="Erase Answer">
	</div>-->
                        
      </div>
    </div>
<div id="statusContainer" style="visibility: hidden;"></div>
<div id="statusMessageContainer" style="visibility: hidden;"></div>
<div id="screenoverlay" style="visibility:hidden;"></div>






























<!-- Timer Alert -->
<div id='styled_popup' style='cursor:hand;width: 350px; height: 78px; display:none; position: absolute; z-index: 100; top: -4px; left: -3px;' name='styled_popup'>
  <table width="100%" height="75" border="0" cellpadding="0" cellspacing="0" onclick='javascript:styledPopupClose()' >
    <tr>
      <td><table width='100%' height="78" border='0' cellpadding='5' cellspacing='1' class='Report_tb'>
	  <tr><th colspan="10">Time Alert</th>	</tr>
      <tr class='tb_rw3'>
          <td colspan='2' align="center">5 Minutes left to auto submit your exam.<br/>
            <br/>
        <input name="button" type="button" class="green_button" onclick='javascript:styledPopupClose()' value=' Close ' />          </td>
        </tr>
      </table></td>
    </tr>
  </table>
</div>
<!-- Timer Alert -->

<script language='JavaScript' type="text/javascript" src="./includes/tinybox.js"></script>
<script language="javascript" type="text/javascript" src="./includes/json.js"></script>				
<script language="javascript" type="text/javascript" src="./includes/browser.js"></script>
<script language='JavaScript' type="text/javascript" src="./includes/PopUnchal.js"></script>		
<script language="javascript" type="text/javascript" src="./includes/online_exam_util.js"></script>
<script language='JavaScript' type="text/javascript" src="./includes/timer.js"></script>	
<script language="javascript" type="text/javascript">
//window.onload=function() { callOnLoad() }
</script>	
<script language="javascript" type="text/javascript" src="./includes/online_exam.js" ></script>

<?php if($countMF > 0){ ?>
<script language="javascript" type="text/javascript" src="./includes/jquery-min.js"></script> 
<script language="javascript"  type="text/javascript" src="./includes/jquery-ui.js"></script> 
<script type="text/javascript"> 
$(document).ready(function(){ 	
	$(function() {
		$("#mflistB ul").sortable({ opacity: 0.8,
	   							cursor: 'move',
							   	update: function() {		
									var order = $(this).sortable('toArray');
									var ans='';		
									var s='';
									var q_rowno='';
									for(i=0;i < order.length;i++){			    
										var s=order[i].split("_");
										var q_rowno=s[0];
										if(ans==0)
										    ans+=s[1];
										else 
										    ans+=","+s[1];
									}
									if(trim(arrQuestionDetails[q_rowno]['answerinserver']) != trim(ans)){
										if(trim(arrQuestionDetails[q_rowno]['answerinlocal']) != trim(ans)){
											arrQuestionDetails[q_rowno]['answerinlocal'] = ans;
											arrQuestionDetails[q_rowno]['savedinserver'] = 0;
										}else{
											arrQuestionDetails[q_rowno]['answerinlocal'] = arrQuestionDetails[q_rowno]['answerinserver'];
											arrQuestionDetails[q_rowno]['savedinserver'] = 1;
										}
								}								
							}
						});
	}); 
});	
</script> 
<?php
}
include "themes/default/footer.php";
//mysql_close($MASTERCONN);
//mysql_close($SLAVECONN);

?>
