<?php require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkTALogin();
require("member_functions.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?PHP echo TITLE?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/iibf.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<center>
<?php
	$taLogin = $_SESSION['login'];	
	$centreCode =  getCentre($taLogin);	
	$examDate = date("Y-m-d");		
	$dispDate = date("d-m-Y");
	$formatType = 'A';
	$iwayAddress = getIwayDetails($centreCode);
	$sqlFormat = "SELECT membership_no, exam_time, reasons, others FROM iib_format WHERE format_type='$formatType' AND exam_date='$examDate' order by membership_no,exam_time";	
	$resFormat = mysql_query($sqlFormat,$SLAVECONN);
	$nFormat = mysql_num_rows($resFormat);
	if ($nFormat == 0)
	{
		$msg = "Format A details not filled for any candidate";
	}
require("topnav.php");
require("formmenu.php");
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" > 
  <tr> 
    <td width="100%" background="images/tile.jpg"><table width="100%" border="0" align="center" cellpadding="1" cellspacing="0">
      <tr>
        <td bgcolor="#014A7F"><table width="100%" border="0" cellspacing="1" cellpadding="5">
            <tr>
              <td  width="40%" align="left" valign="middle" bgcolor="#E8EFF7" class="greybluetext10"><b>DATE OF THE EXAMINATION:</b></td>
              <td align="left" valign="middle" bgcolor="#E8EFF7" class="greybluetext10" ><?=$dispDate ?></td>
            </tr>
            <tr>
              <td  width="40%" align="left" valign="middle" bgcolor="#E8EFF7" class="greybluetext10"><b>NAME OF THE CENTRE:</b></td>
              <td align="left" valign="middle" bgcolor="#E8EFF7" class="greybluetext10" ><?=$centreCode ?></td>
            </tr>
            <tr>
              <td  width="40%" align="left" valign="middle" bgcolor="#E8EFF7" class="greybluetext10"><b> IWAY ADDRESS:</b></td>
              <td align="left" valign="middle" bgcolor="#E8EFF7" class="greybluetext10" ><?=$iwayAddress ?></td>
            </tr>
        </table></td>
      </tr>
    </table>
      <?php
			if ($nFormat > 0)
			{
				print("<br/><table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"1\" cellspacing=\"0\"><tr><td bgcolor=\"#014A7F\">");
				print("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"5\">");
				print("<tr><td  class=\"ColumnHeader1\" valign=top  width=15%><b>Membership No.</b></td><td valign=top class=\"ColumnHeader1\"  width=20%><b>Name</b></td>"."<td valign=top class=\"ColumnHeader1\" width=15%><b>Exam Time</b></td><td valign=top  class=\"ColumnHeader1\" width=25%><b>Reasons</b></td>".
				"<td valign=top class=\"ColumnHeader1\" width=25%><b>Other Reasons</b></td></tr>");
				while (list($membershipNo,$examTime,$reasons,$others) = mysql_fetch_row($resFormat))
				{
	  				$dispTime = substr($examTime,0,5);
	  				$memberDetails = getMemberDetails($membershipNo);
	  				$memberName = $memberDetails[0];
	  				if ($membershipNo == "")
	  					$membershipNo = "<br>";
	  				if ($memberName == "")
	  					$memberName = "<br>";
					if ($reasons == "")
						$reasons = "<br>";
					if ($others == "")
						$others = "<br>";		  					
	  				$others = stripslashes($others);
	  				print("<tr><td bgcolor=\"#E8EFF7\" valign=top class=greybluetext10>$membershipNo</td><td bgcolor=\"#E8EFF7\" valign=top class=greybluetext10>$memberName</td>".
	  				"<td bgcolor=\"#E8EFF7\" valign=top class=greybluetext10>$dispTime</td><td bgcolor=\"#E8EFF7\" valign=top class=greybluetext10>$reasons</td><td bgcolor=\"#E8EFF7\" valign=top class=greybluetext10>$others</td></tr>");
				}
				print("</table>");
				print("</td></tr></table>");
			}
			else
			{
				print(" <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\"><tr><td class=errormsg align=center>$msg</td></tr></table>");
			}
		?>
  	</td>
  </tr>
</table> 	
<?php
require("bottomnav.php");
//mysql_close($SLAVECONN);

?>
</center>
</body>
</html>
