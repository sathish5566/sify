<?php /****************************************************
* Application Name            :  IIB
* Module Name                 :  Sample Preview
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  
* Created ON                  :  
* Last Modified By            :  B.Devi
* Last Modified Date          :  27-09-2006
* Description                 :  Interface to preview the sample question ids
*****************************************************/
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkTALogin();
checkCandidateLogin();
require_once("constants.inc");

$time_to_submit = getVal($_REQUEST['time_to_submit']);
$qindex = isset($_POST["qindex"])?$_POST["qindex"]:0;
$nAnswered = 0;
$nUnAnswered = 0;	
$aAnswered = array();
$aUnAnswered = array();
for ($i = 1; $i <= 5; $i++)
{
	$answer = trim($_POST['answer'.$i]);
	if ($answer == "")
	{
		$aUnAnswered[$i] = $i;
	}
	else
	{	
		$aAnswered[$i] = $i;
		$nAnswered++;
	}
}
$nUnAnswered = 5 - $nAnswered;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?PHP echo TITLE?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/iibf.css" rel="stylesheet" type="text/css">
<script language='JavaScript' src="./includes/timer_submit.js"></script>
<script language='JavaScript'>
history.go(1);
		function submitQuestion(q_no)
		{
			document.frmpreview.action = "sampletest.php?qid="+q_no;
			document.frmpreview.submit();	
		}
		
		function getSamplePaper()
		{
			document.frmpreview.action = 'sampletest.php?qid=<?=$qindex+1?>';
			document.frmpreview.submit();		
		}
		
		function submitPaper()
		{
			var question_id;
			var answer;
			var chk = 0;
			
			for (var i = 1; i <= 5; i++)
			{							
				answer = eval('document.frmpreview.answer'+i+'.value');							
				if (answer == '')
				{
					chk = 1;
					break;
				}				
			}
			if (chk == 1)
			{
				if (popup() == -1)
				{
					return;
				}
				else
				{
					document.frmpreview.action = 'sample_scores.php';
					document.frmpreview.submit();		
				}
			}
			else
			{			
				if (!confirm ("Are you sure you want to Submit?"))
				{
					return;
				}
				else
				{
					document.frmpreview.action = 'sample_scores.php';
					document.frmpreview.submit();		
				}
			}
		}
		
		function popup()
		{
			if (!confirm ("You have unattempted questions. Do you really want to Submit?"))
			{
				return(-1);
			}	
			else
			{		
				return(0);
			}	
		}
	function callOnLoad()
	{
		InitializeTimer(<?=$time_to_submit?>,"frmpreview","sample_scores.php");
	}
	window.onload =  callOnLoad;
	</script>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<center>
<form name=frmpreview method=post>
<?php
	for ($i = 1; $i <= 5; $i++)
	{
		$val = trim($_POST['answer'.$i]);
		print("<input type=hidden name='answer$i' value='$val'>\n");
	}
?>
<input type=hidden name='time_to_submit' value='<?=$time_to_submit?>'>
<table width="780" border="0" cellspacing="0" cellpadding="0">
	<tr> 
	<?php include("includes/header.php")?>
	</tr>
	<tr>
		
	</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="780" background="images/tile.jpg">
	<tr>
		<td>
		<br>		
		<table width="80%" border="0" cellspacing="1" cellpadding="0" align=center>
		<tr> 
			<td align=center width="50%" class="greybluetext10" height="35" bgcolor="#D1E0EF"><b class="arial11a">Attempted Questions</b></td>
			<td align=center width="50%" class="greybluetext10" height="35" bgcolor="#D1E0EF"><b class="arial11a">Unattempted Questions</b></td>
		</tr>
		<tr>
			<td  width="50%" valign=top class="greybluetext10" bgcolor="#E8EFF7">
				<table border="0" cellspacing="0" cellpadding="5" align=center width=100%>
					<tr>
						<td class="greybluetext10" align=center>
						<!--code for attempted comes here -->
						<?php
						if ($nAnswered > 0)
						{
							$first = 0;
							foreach ($aAnswered as $sno=>$id)
							{			
								($first == 0) ? print("") : print("&nbsp;&nbsp; ");
								print(" <a href='javascript:submitQuestion(".$sno.")'><b> Q".$sno.".</b></a> ");			
								$first++;
							} // end of for 
						} //end of if
						else
						{
							print("<br>");
						}
						?>
						</td>
					</tr>
				</table>
				</td>
				<td valign=top width="50%" valign="top" bgcolor="#E8EFF7"> 
				<table border="0" cellspacing="0" cellpadding="5" align=center width=100%>
					<tr>
						<td class="greybluetext10" align=center>
						<!--code for unattempted comes here -->
						<?php
						if ($nUnAnswered > 0)
						{
							$first = 0;
							foreach ($aUnAnswered as $sno=>$id)
							{			
								($first == 0) ? print("") : print("&nbsp;&nbsp; ");
								print(" <a href='javascript:submitQuestion(".$sno.")'><b> Q".$sno.".</b></a> ");			
								$first++;
							} // end of for 
						}
						else
						{
							print("<br>");
						}
						?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
				<td valign=top width="50%" valign=top class="greybluetext10" bgcolor="#E8EFF7">
				<table border="0" cellspacing="0" cellpadding="5" align=center width=100%>
					<tr>
						<td class="greybluetext10" align=center><b>No. of Attempted Questions : <?=$nAnswered ?></b></td>
					</tr>
				</table>
				</td>
				<td valign=top width="50%" valign=top class="greybluetext10" bgcolor="#E8EFF7">
				<table border="0" cellspacing="0" cellpadding="5" align=center width=100%>
					<tr>
						<td class="greybluetext10" align=center><b>No. of Unattempted Questions : <?=$nUnAnswered ?></b></td>
					</tr>
				</table>
				</td>
		</tr>
		<tr bgcolor="#D1E0EF" align="center" valign="middle"> 
			<td colspan="2" height="40"><input class='button' id='ClearFrm' onClick='javascript:getSamplePaper()' type=button value='Back' name="sub_questionpaper">&nbsp;<input class='button' id='ClearFrm' onClick="javascript:submitPaper()" type='button' value='Submit' name="sub_submit">
			</td>
		</tr>
		</table>
		<br>
		</td>
	</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="780">
<tr>
	<?php include("includes/footer.php");?>
</tr>
</table>
<br>
</form>
</center>
</body>
</html>	