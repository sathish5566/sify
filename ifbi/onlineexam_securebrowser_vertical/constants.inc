<?php
class Duration
   {
         function toString ($duration, $periods = null)
       {
           if (!is_array($duration)) {
              $duration = Duration::int2array($duration, $periods);
           }
            
          return Duration::array2string($duration);
       }
    
         function int2array ($seconds, $periods = null)
       {        
           // Define time periods
           if (!is_array($periods)) {
               $periods = array ('hours'     => 3600,'minutes'   => 60,'seconds'   => 1);
              
           }
    
           // Loop
           $seconds = (float) $seconds;
           foreach ($periods as $period => $value) {
               $count = floor($seconds / $value);
    
               if ($count == 0) {
                   continue;
               }
    
               $values[$period] = $count;
               $seconds = $seconds % $value;
           }
   
           // Return
           if (empty($values)) {
               $values = null;
          }
    
           return $values;
      }
     
      function array2string ($duration)
      {
          if (!is_array($duration)) {
              return false;
          }
           
          foreach ($duration as $key => $value) {
              $segment_name = substr($key, 0, -1);
              $segment = $value . ' ' . $segment_name; 
			  
   
              // Plural
              if ($value != 1) {
                 $segment .= 's';
              }
   
              $array[] = $segment;
          }
   
          $str = implode('  ', $array);
          return $str;
      }
   
  } 

    //$photoURL='http://photos.sifyitest.com/ibps/';
	$photoURL='./photos/';
	$canPhotoPath=$photoURL.'candidate_photos/';
	$canSignPath=$photoURL.'candidate_signature/';
	$qpImgPath=$photoURL.'questionpaper/images/';
	$qpAudPath=$photoURL.'questionpaper/audios/';
	$qpVidPath=$photoURL.'questionpaper/videos/';

	$memPhotoWidth ='125';
	$memPhotoHeight ='140';
	$signPhotoWidth ='125';
	$signPhotoHeight ='40';
	$qpImgMinWidth = '50';
	$qpImgMinHeight = '50';
	$qpImgMaxWidth = '800';
	$qpImgMaxHeight = '800';

	/*Flash Player Settings */
	$audWidth = 400;
	$audHeight = 50;
	$vidWidth = 400;
	$vidHeight = 350;
	
	$flvBtnColor = 0x333333;
	$flvAccentColor = 0x31b8e9;
	$flvtxtcolor = 0xdddddd;
	$flvVolume = 50;
	$flvAutoload = 'on';
	$flvAutoplay = 'off';
	$flvTitle='sifyitest';
	$flvshowTitle = 'yes';
	$flvAllowFullScreen='yes';

  /* Flash Player Settings End */
    $update_fail_path = "./update_logs/";
    $QuestionPaperPath='./questionpaper/';	
	$timeToSubmit=600;



/* SAMPLE  QUESTION PAPER */
$QUESTION_ARRAY=array();
$QUESTION_ARRAY[0]['EN']['question']='The Debt Equity Ratio _______';
$QUESTION_ARRAY[0]['EN']['option1']='compares the investment of owners to long term borrowings";';
$QUESTION_ARRAY[0]['EN']['option2']='is used to determine whether the mix of term borrowings and stake of owners in making up the capital structure of the organization is satisfactory';
$QUESTION_ARRAY[0]['EN']['option3']='indicates the soundness of the long term financial policy of the unit';
$QUESTION_ARRAY[0]['EN']['option4']='All of the above';
$QUESTION_ARRAY[0]['EN']['option5']='None of these';
$QUESTION_ARRAY[0]['EN']['correct_answer']='1';
$QUESTION_ARRAY[0]['EN']['question_id']='1';
$QUESTION_ARRAY[0]['EN']['mark']='1';
$QUESTION_ARRAY[0]['EN']['negative_mark']='0';



$QUESTION_ARRAY[1]['EN']['question']='Statutory Liquidity Ratio (SLR) for Scheduled Banks is regulated by the Reserve Bank of India under powers conferred upon it by_____?';
$QUESTION_ARRAY[1]['EN']['option1']='RBI Act, 1934';
$QUESTION_ARRAY[1]['EN']['option2']='Banking Regulation Act, 1949';
$QUESTION_ARRAY[1]['EN']['option3']='Union Ministry of Finance';
$QUESTION_ARRAY[1]['EN']['option4']='Companies Act, 1956';
$QUESTION_ARRAY[1]['EN']['option5']='None of these';
$QUESTION_ARRAY[1]['EN']['correct_answer']='2';
$QUESTION_ARRAY[1]['EN']['question_id']='2';
$QUESTION_ARRAY[1]['EN']['mark']='1';
$QUESTION_ARRAY[1]['EN']['negative_mark']='0';

$QUESTION_ARRAY[2]['EN']['question']='A bill discounted by the bank is returned unpaid as dishonored. The Bank will____ ';
$QUESTION_ARRAY[2]['EN']['option1']='debit the parties account immediately without prior notice';
$QUESTION_ARRAY[2]['EN']['option2']='debit the parties account with prior arrangement';
$QUESTION_ARRAY[2]['EN']['option3']='take a fresh bill and adjust the amount of the dishonored bill with the proceeds thereof';
$QUESTION_ARRAY[2]['EN']['option4']='keep the dishonoured bill pending for sometime';
$QUESTION_ARRAY[2]['EN']['option5']='None of these';
$QUESTION_ARRAY[2]['EN']['correct_answer']='3';
$QUESTION_ARRAY[2]['EN']['question_id']='3';
$QUESTION_ARRAY[2]['EN']['mark']='1';
$QUESTION_ARRAY[2]['EN']['negative_mark']='0';

$QUESTION_ARRAY[3]['EN']['question']='What is the rate of interest payable on Term Deposits in case the deposit is closed before maturity but after 46 days from the date of issue?';
$QUESTION_ARRAY[3]['EN']['option1']='One percent less than the originally contracted rate';
$QUESTION_ARRAY[3]['EN']['option2']='One percent less than the rate payable for the period for which the deposit has run';
$QUESTION_ARRAY[3]['EN']['option3']='The originally contracted rate';
$QUESTION_ARRAY[3]['EN']['option4']='Two percent less than the originally contracted rate';
$QUESTION_ARRAY[3]['EN']['option5']='None of these';
$QUESTION_ARRAY[3]['EN']['correct_answer']='4';
$QUESTION_ARRAY[3]['EN']['question_id']='4';
$QUESTION_ARRAY[3]['EN']['mark']='1';
$QUESTION_ARRAY[3]['EN']['negative_mark']='0';

$QUESTION_ARRAY[4]['EN']['question']='An increase in the growth rate of money supply is most likely to be followed by______';
$QUESTION_ARRAY[4]['EN']['option1']='deflation';
$QUESTION_ARRAY[4]['EN']['option2']='inflation';
$QUESTION_ARRAY[4]['EN']['option3']='an increase in economic activity';
$QUESTION_ARRAY[4]['EN']['option4']='a decrease in economic activity';
$QUESTION_ARRAY[4]['EN']['option5']='None of these';
$QUESTION_ARRAY[4]['EN']['correct_answer']='5';
$QUESTION_ARRAY[4]['EN']['question_id']='5';
$QUESTION_ARRAY[4]['EN']['mark']='1';
$QUESTION_ARRAY[4]['EN']['negative_mark']='0';
	
	
?>
