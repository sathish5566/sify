<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php echo $metaTagHD ?>

<title><?PHP echo TITLE?></title>
<?php echo $includeJS ?>

<link href="themes/default/css/style.css" rel="stylesheet" type="text/css" />
<link href="themes/default/css/reset.css" rel="stylesheet" type="text/css" />
<?php echo $includeStyleHD ?>

</head>
<body<?php echo $bodyOnload; ?>>
<div id="wrapper">
  <div id="header_container">
    <div id="header_container_inner">
      <div id="header_sify_logo"><img src="themes/default/images/sify_logo.png" width="70" height="37" /></div>
    </div>
  </div>
  <div class="clear"></div>
  <div id="samp_quest_body_container">

