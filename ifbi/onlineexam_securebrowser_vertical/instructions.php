<?php /****************************************************
* Application Name            :  IIB 
* Module Name                 :  Exam Module
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  iib_instructions_template,iib_candidate,iib_exam_subjects,iib_candidate_iway,iib_candidate_test
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              : 
* Created ON                  :  
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  22-01-2010
* Description                 :  Interface for the Instrunctions details
*****************************************************/
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkCandidateLogin();
require_once("constants.inc");
$strSourceFile = $_SERVER['HTTP_REFERER'];
if ($strSourceFile != "")
{
	$aSubStr = explode("/", $strSourceFile);
	$strFileName = $aSubStr[count($aSubStr) - 1];
}


$memno = getVal($_SESSION['memno']); 
$subcode=getVal($_SESSION['sc']);
$examcode=getVal($_SESSION['ex']);
$taOverride = getVal($_SESSION['ta_override']);
$loginOverride = getVal($_SESSION['login_override']);
$exam_view = getVal($_SESSION['exam_view']);

$sqlQuestionPaper = "SELECT question_paper_no FROM iib_question_paper WHERE membership_no='$memno' and  exam_code='$examcode' AND subject_code='$subcode' AND sample='N' AND enabled='Y' AND online='Y' AND assigned='Y' AND complete='Y' LIMIT 1";
$result = mysql_query($sqlQuestionPaper,$SLAVECONN) or errorlog('err05'," QUERY: $sqlQuestionPaper  ".mysql_error($SLAVECONN));

$nRows = mysql_num_rows($result);
if ($nRows > 0)
{
       list($questionPaperNo) = mysql_fetch_array($result);
	  $_SESSION['questionpaperno']=$questionPaperNo;
}


$old_mediumcode = getVal($_POST['old_mediumcode']);
$lstmedium = getVal($_POST['lstmedium']);	



if( ($strFileName != 'sample_scores.php') && !isset($_POST['hLanguage']) ) 	
{
	if($taOverride=='Y')
		$mediumcode=$old_mediumcode;
	else if(isset($_POST['lstmedium']))
	{
		if( $old_mediumcode != $lstmedium )
		{
			$sqlUpdate = "UPDATE iib_exam_candidate SET medium_code='".$lstmedium."' WHERE membership_no='$memno' AND exam_code='$examcode' AND  subject_code='$subcode' ";
			$mediumcode=$lstmedium;
			mysql_query($sqlUpdate,$MASTERCONN) or errorlog('err06'," QUERY:$sqlUpdate  ".mysql_error($MASTERCONN));
		}else
		 $mediumcode=$old_mediumcode;
	}
		 		
$_SESSION['mc']=$mediumcode;
}else
	$mediumcode=$_SESSION['mc'];


//fetch instruction language change
$lang=isset($_REQUEST["hLanguage"])?$_REQUEST["hLanguage"]:"EN";

$sql_instruction = "SELECT instruction_text FROM iib_instructions_template WHERE exam_code='".$examcode."' AND subject_code='".$subcode."' AND medium_code='".$lang."' AND is_active='Y'";
$res_instruction = mysql_query($sql_instruction,$SLAVECONN) or errorlog('err05'," QUERY: $sql_instruction  ".mysql_error($SLAVECONN));
if ($res_instruction)
{
	list($instructions_new) = mysql_fetch_row($res_instruction);
}

$samplepage="sampletest.php";
if ($taOverride == 'Y'){
	if($exam_view == 'V')
	  header("Location:online_exam_ver.php");
	else
	  header("Location:online_exam.php");

	exit;  
}else{
  $target="sampletest.php";  
}
$includeJS = '<script language="javascript" type="text/javascript" src="./includes/browser.js"></script>';
$includeJS .= '<script language="JavaScript" src="./includes/timer_submit.js"></script>';
$redirectPage = ($taOverride == 'Y') ? instructions.php : $samplepage;
$includeJS .= <<<JSSC
<script language='JavaScript'>
history.go(1);
function changeLanguage(){
frm = document.frmname;
if (frm.hLanguage.value == "E")
	frm.hLanguage.value="H";
else
	frm.hLanguage.value="E";
	frm.action="instructions.php";
	frm.submit();
}
function submitForm()
{			
	document.getElementById("sub_form").disabled=true;
	document.getElementById("submitstatus").innerHTML='please wait submitting form...';
	document.frmname.action='$redirectPage';
	document.frmname.submit();
}
</script>
JSSC;
$bodyOnload =' onKeyDown="return doKeyDown(event);"  ondragstart ="return Stop(event)" onselectstart="return Stop(event)"onmousedown="return Stop(event)" onmouseup="return Stop(event)" onKeyPress="javascript:submitForm(event)"';
include("themes/default/header.php");
?>
<form name=frmname method=post>	
<input type=hidden name="time_to_submit" id="time_to_submit" value='<?=$timeToSubmit?>'>
<input type=hidden name="hLanguage" value='<?=$lang?>'>
</form>

    <div id="body_container_inner">
      <div id="ca_inst_wrapper">
        <div class="outerbox">
          <div class="innerbox">
            <div class="innerbox_container_common">
              <div class="pager_header_common">Candidate Instructions</div>
              <div class="content_container_common">
                <div class="content_container_box_common" >
                  <div id="ca_inst_content">
<?php
if ($lang=="E" || $lang == 'EN'){
	echo stripslashes($instructions_new);
}
?>
                  </div>
                </div>
              </div>
<?PHP if( ($strFileName!='sample_scores.php') && ($taOverride != 'Y')){ ?>
              <div class="button_container_common">
<input class="green_button" id='sub_form' type='button' value='I have read the Instructions' name="sub_form" onclick='javascript:submitForm();'/>
<br/><span id="submitstatus" class="errormsg"></span>
              </div>
<?PHP } ?>
            </div>
          </div>
        </div>
      </div>
    </div>

<!-- Timer Alert -->
<div id='styled_popup' style='cursor:hand;width: 350px; height: 78px; display:none; position: absolute; z-index: 100; top: -4px; left: -3px;' name='styled_popup'>
  <table width="100%" height="75" border="0" cellpadding="0" cellspacing="0" onclick='javascript:styledPopupClose()'>
    <tr>
      <td bgcolor="#000000"><table width='100%' height="78" border='0' cellpadding='5' cellspacing='1'>
        <tr>
          <td  align="center" bgcolor="#014A7F" class="ColumnHeader1 style3">TIME ALERT</td>
        </tr>
        <tr>
          <td colspan='2' align="center" class="CellTextDark"><span class="style4"><font face="Arial, Helvetica, sans-serif">You have spent much time for reading Instructions/Sample test and will be redirected to the Main exam in 5 Mins</font></span><br/>
            <br/>
              <input name="button" type="button" class="button" onclick='javascript:styledPopupClose()' value=' Close ' />          </td>
        </tr>
      </table></td>
    </tr>
  </table>
</div>
<!-- Timer Alert -->
<?php 
include("themes/default/footer.php");
//mysql_close($MASTERCONN);
//mysql_close($SLAVECONN);
?>
