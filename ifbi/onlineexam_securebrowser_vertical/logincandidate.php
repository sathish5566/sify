<?php
/****************************************************
* Application Name            :  IIB 
* Module Name                 :  Exam Module
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  
* Tables used for only selects:  iib_candidate,iib_exam_subjects,iib_candidate_iway,iib_candidate_test
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              : 
* Created ON                  :  
* Last Modified By            :  Suresh.A
* Last Modified Date          :  26-10-2013
* Description                 :  Interface for the candidate to login
*****************************************************/
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkTALogin();
unset($_SESSION['memno']);

setcookie("tloss", 0, time());

//Delete Duplicate sessions;
if(isset($_POST['deleteall'])){
        if(get_magic_quotes_gpc())
         $sid=stripslashes($_POST['SID']);
        else
         $sid=stripslashes(getVal($_POST['SID']));
        deleteDuplicateSession($sid);
        unset($_SESSION['memno']);
        unset($_SESSION['ta_override']);
        redirect_taoption(1);
        exit;
}

//Added for BOI
$sqlSelectTest = "SELECT membership_no , exam_code , subject_code , test_status from iib_candidate_test where current_session='Y'";
$resSelectTest = @mysql_query($sqlSelectTest);
$cntSelectTest = mysql_num_rows($resSelectTest);
if($cntSelectTest > 0)
{
	while(list($M_No,$E_Code,$SC,$TS)=mysql_fetch_row($resSelectTest))
	{
		$StatusArr[$M_No][$E_Code][$SC]=$TS;
	}
}

/*
 ** Function to fetch the status(IC/C/0) of the candidate whose exam code , subject code is being passed as an input along with membership no. The result will be returned to the called page.
 ** IC - Incomplete Status
 ** C - Complete Status
 ** 0 - Not taken the test
 */
function checkExamStatus($MemNo,$ExamCode,$SubCode)
{
	$return_value=0;
	global $StatusArr;
	
	if(is_array($StatusArr))
	{
				
		if(array_key_exists($SubCode,$StatusArr[$MemNo][$ExamCode]))
		{
			
			$return_value = $StatusArr[$MemNo][$ExamCode][$SubCode];			
			return $return_value;
		}
		else
		{
			//echo "hi1";
			return $return_value;
		}
	}
	else
	{
		//echo "bye";
		return $return_value;
	}
}

//Added for BOI ends here

/* TA override*/
$strSourceFile = $_SERVER['HTTP_REFERER'];
if ($strSourceFile != "")
{
	$aSubStr = explode("/", $strSourceFile);
	$strFileName = $aSubStr[count($aSubStr) - 1];
}
$taLogin = $_SESSION['login'];
$cafeID = $_SESSION['cafeID'];

if( isset($_POST['ta_override']) )
{	
	 $MEMNO = getVal($_POST["memno"]);
	 $taOverride = getVal($_POST['ta_override']);
	 $hid_lefttime = getVal($_POST["hid_lefttime"]);
	
}	
/* TA override*/

$memno = strtoupper($_POST['memno']);
$candpass = $_POST['candpassword'];
if($strFileName == 'restartingexam.php') $from_page = 'R';
else $from_page = isset($_POST['from_page'])?$_POST['from_page']:'';
$emsg = "";
 $option_subject=0;
if ( isset($_POST['candpassword']) &&  ($memno != '') && ($candpass !=='') )
{
	//$MEMNO = $memno;
 
       $query1 = "select password,password('$candpass') from iib_candidate where membership_no='$memno' ";
        $result1 = @mysql_query($query1,$SLAVECONN) or errorlog('err05'," QUERY:$query1  ".mysql_error($SLAVECONN));
		list($cmp_pass,$cand_pass) = mysql_fetch_row($result1);
		if(mysql_num_rows($result1) == 0)		
			  $emsg .= "Invalid Login. Please check whether the Caps Lock on your keyboard is turned on.";
		else if ($cand_pass != $cmp_pass)
			  $emsg .= "Invalid Password. Please check whether the Caps Lock on your keyboard is turned on.";
		else{
		
				$hostIP = $_SERVER['REMOTE_ADDR'];				
				

				$aExamCode = array();
				$aSubCode = array();
				$aCentreCode = array();
				$aTime = array();
				$aTimeStamp = array();
							
				$todaydate = date("Y-m-d");
				$currTimeStamp = mktime(date("H"),date("i"), date("s"),date("m"),date("d"),date("Y"));        	
				
			
				 $sqlTime = "SELECT exam_time,subject_code,exam_code,centre_code from iib_candidate_iway where membership_no='$memno' and exam_date='$todaydate' order by exam_time";        	
				$resTime = @mysql_query($sqlTime,$SLAVECONN) or errorlog('err05'," QUERY:$sqlTime  ".mysql_error($SLAVECONN));
				$nTime = @mysql_num_rows($resTime);
				if ($nTime > 0)
				{
							$nCntTime = 0;
							while (list($eTime,$suCode,$exCode,$ceCode) = mysql_fetch_row($resTime))
							{
								$aTime[$nCntTime] = $eTime;
								$aExamCode[$nCntTime] = $exCode;
								$aSubCode[$nCntTime] = $suCode;
								$aCentreCode[$nCntTime] = $ceCode;
								$aTmp = array();
								$aTmp = explode(":", $eTime);
								$aTimeStamp[$nCntTime] = mktime($aTmp[0],$aTmp[1],$aTmp[2],date("m"),date("d"),date("Y"));
								$nCntTime++;
							} //end of while
 							$check_array=array_unique($aTime);
							if(count($check_array)==count($aTime))
							{
								
									/* Start :If more than 1 exam scheduled means, need to get details of which exam time match to current time */
										if ($nCntTime > 1)
										{											
											for($l=0;$l<$nCntTime;$l++)
											{																								
												$SubjectStatus = checkExamStatus($memno,$aExamCode[$l],$aSubCode[$l]);
												$next = $l+1;
												if(trim($SubjectStatus) == 'IC')
												{	
													//echo "hi";
													$selExamTime = $aTime[$l];
													$subject_code = $aSubCode[$l];
													$exam_code = $aExamCode[$l];
													$centre_code = $aCentreCode[$l];
													$break=true;
												}else if( (trim($SubjectStatus) == '0') && ($currTimeStamp >= $aTimeStamp[$l]) && ($currTimeStamp < $aTimeStamp[$next]))																								
												{																																																		
													//echo "222";
													$selExamTime = $aTime[$l];
													$subject_code = $aSubCode[$l];
													$exam_code = $aExamCode[$l];
													$centre_code = $aCentreCode[$l];
													$break=true;																										
												}
												else
												{
													//echo "bye";
													if ($next==$nCntTime) 
													{
														$m = $l;
													} 
													else
													{
														$m=$l-1;
													}
													$selExamTime = $aTime[$m];
													$subject_code = $aSubCode[$m];
													$exam_code = $aExamCode[$m];
													$centre_code = $aCentreCode[$m];																										
												}
												if($break) break;												
											}		
										} 
										else 
										{
											/* Only one exam scheduled */
											$selExamTime = $aTime[0];
											$subject_code = $aSubCode[0];
											$exam_code = $aExamCode[0];
											$centre_code = $aCentreCode[0];
										}						
										/* End :If more than 1 exam scheduled means, need to get details of which exam time match to current time */										
										/* Centre check */
										if (strtoupper($centre_code) != strtoupper($cafeID))
											 $emsg .= "You are not scheduled to take the exam from this centre. Please call the Toll free number printed on your admit card.";
										else{
											   $todate_start = date("Y-m-d")." 00:00:00";
											   $todate_end = date("Y-m-d")." 23:59:59";
											   
											  $sqlTest = " SELECT test_status FROM iib_candidate_test WHERE membership_no='$memno' AND exam_code='$exam_code' AND subject_code='$subject_code' AND start_time between '$todate_start' and '$todate_end' ";
											   $resTest = mysql_query($sqlTest,$SLAVECONN) or errorlog('err05'," QUERY:$sqlTest  ".mysql_error($SLAVECONN));
											   $nTestTaken=mysql_num_rows($resTest);											   											   
											   
											   $nTestCompleted=0;
											   while($rTest = @mysql_fetch_array($resTest)){
											   			if($rTest[0]=='C')
															$nTestCompleted=1;														
												}																								
												//Exam is not started 
												if( ($nTestTaken == 0))
			        							{																																								
														if ($taOverride == 'Y')
			        									{																	
															redirect_taoption(6);
														}else{
															 $selGrace="select grace_pre , grace_post from iib_exam_subjects  where exam_code='$exam_code' AND subject_code='$subject_code'";
															 $resGrace=mysql_query($selGrace,$SLAVECONN) or errorlog('err05'," QUERY:$selGrace  ".mysql_error($SLAVECONN));															 
															if(mysql_num_rows($resGrace) == 0)
															{
															 $selGrace="select grace_pre , grace_post  from iib_exam_subjects  where exam_code='$exam_code' AND option_id='$subject_code'";
															 $resGrace=mysql_query($selGrace,$SLAVECONN) or errorlog('err05'," QUERY:$selGrace  ".mysql_error($SLAVECONN));															 															 $_SESSION['sub_opt']='Y';
															 $_SESSION['memno']=$memno;
															 $_SESSION['centrecode']=$centre_code;
															 $option_subject=1;
															}
															 list($pre_grace,$post_grace )=mysql_fetch_row($resGrace);
															 $pre_grace1 = $pre_grace / 60;
															 $post_grace1 = $post_grace / 60;

															 $chkTime = "select (TIME_TO_SEC(current_time)-TIME_TO_SEC(exam_time))/60  time_diff from iib_candidate_iway where membership_no='$memno' and exam_date='$todaydate' AND exam_code='$exam_code' AND subject_code='$subject_code' ";																

															  $resTime = mysql_query($chkTime,$SLAVECONN) or errorlog('err05'," QUERY:$chkTime  ".mysql_error($SLAVECONN));
															  list ($timeDiff) = @mysql_fetch_row($resTime);
															  if ($timeDiff < - $pre_grace1)
															  {																																										
															     $emsg = "You are allowed to login only ".$pre_grace1." minutes prior to your exam time. Please contact the Test Administrator.";																	
															   } //end of 15 mins prior
															   else
															   {
																	if ($timeDiff > $post_grace1)
																	{
																		$emsg = "Exam timed out. Please contact the Test Administrator.";
																	}
																	else
																	{			    	        			
																		if( $option_subject==1){
																			@header("Location: intermediate_section_login.php");
																			exit;
																		}else{	
																	
																		$resultsetTracking=checkCandidateTracking($memno,$exam_code,$subject_code);												
																		if($resultsetTracking==0){
																		
																	
																		$sessionID = session_id();
																		trackCandidateLogin($memno, $taLogin, $cafeID, $hostIP, $sessionID, $exam_code,$subject_code);
																		$_SESSION['sc'] = $subject_code;
																		$_SESSION['ex'] = $exam_code;
																		$_SESSION['et'] = $selExamTime;
																		$_SESSION['memno']=$memno;																		
																		$_SESSION['centrecode']=$centre_code;
																		$_SESSION['ltime']='';
																		$_SESSION['ta_override']='N';
																																																																							
																		@header("Location: logincandidate1.php");
																		exit;
																		
																		}
																	  }
																	}
																}																																																																	
														}//Taoverride else end																																								
												}else{
													//Exam Already Started													
													// If the candidate has 2 exams same day. Display appropriate message
													  for($l=0;$l<$nCntTime;$l++)
													  {															
															
															/*if (($nTime > 1) && ($selExamTime == $aTime[$l]) && $nTestCompleted!=0)
															{
																	$emsg = "You have already logged in for your previous exam. Login after ". $aTime[$l+1] . " to take the next exam ";
																	break;
															}*/
																																									
													  }
													  if (($l==$nCntTime) && ($nTestCompleted==0) && ($taOverride=='Y'))
													  {
													  		
															 $resultsetTracking=checkCandidateTracking($memno,$exam_code,$subject_code);												 
															 if($resultsetTracking==0){															
																$sessionID = session_id();
																trackCandidateLogin($memno, $taLogin, $cafeID, $hostIP, $sessionID, $exam_code,$subject_code);
																$_SESSION['sc'] = $subject_code;
																$_SESSION['ex'] = $exam_code;
																$_SESSION['et'] = $selExamTime;
																$_SESSION['memno']=$memno;
																$_SESSION['centrecode']=$centre_code;
																$_SESSION['ltime']=$hid_lefttime;
																$_SESSION['ta_override']=$taOverride;															
																@header("Location: logincandidate1.php");
																exit;
															}																	
													  }else if (($l==$nCntTime) && ($nTestCompleted==0)){
													   	redirect_taoption(3);														
													  }elseif($l==$nCntTime){
														redirect_taoption(4);
													  } 						  
													  
												
												}// Exam Already Started end
										
										}	//Centre code else end							
																												
							}else //(count($check_array)==count($aTime))
								$emsg.="Problem in Exam Timing.Please Re-schedule the Timings(More than one exam scheduled in same time)";																																																
				
				 }else{ // ($nTime > 0)
				 	redirect_taoption(5);
				  }
		}
}   
$ccode=substr($_SESSION['login'],5);
$qry_iway="SELECT logomaster_ref from  iib_iway_details where centre_code='$ccode'";
$reslogo = mysql_query($qry_iway,$SLAVECONN) or errorlog('err05'," QUERY:$qry  ".mysql_error($SLAVECONN));
list($logoid)=mysql_fetch_row($reslogo);
if($logoid!=''){
	$qry="select img_path,alt_text from iib_logo_master where id IN('$logoid')";
	$res = mysql_query($qry,$SLAVECONN) or errorlog('err05'," QUERY:$qry  ".mysql_error($SLAVECONN));
	list($imgpath,$alttext)=mysql_fetch_row($res); 
}else{
	$qry="select img_path,alt_text from iib_logo_master where active='Y'";
	$res = mysql_query($qry,$SLAVECONN) or errorlog('err05'," QUERY:$qry  ".mysql_error($SLAVECONN));
	list($imgpath,$alttext)=mysql_fetch_row($res); 
}

	$includeJS = '<script language="javascript" type="text/javascript" src="./includes/browser.js"></script>';
	$includeJS .= '<script language="javascript" src="./includes/validations.js"></script>';
	
$includeJS .= <<<JSSC
<script language="javascript">
function validate()
{
	loginname=trim(document.loginfrm.memno.value);
	pass=trim(document.loginfrm.candpassword.value);
	document.loginfrm.memno.value=loginname;
	if(loginname=='')
	{
		alert('Please enter your membership no');
		document.loginfrm.memno.focus();
		return;
	}else if(pass==''){
		alert('Please enter your password!');
		document.loginfrm.candpassword.focus();
		return;
	}
	if (!checktext(document.loginfrm.memno))
		return;
	if (!checktext(document.loginfrm.candpassword))
		return;
	document.getElementById("go").disabled=true;
    document.getElementById("submitstatus").innerHTML='please wait submitting form...';     
	document.loginfrm.action="logincandidate.php";   
	document.loginfrm.submit();
}
function submitForm(e)
{
	var keyCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
	if (keyCode == 13)
	{
		validate();
	}		
}
</script>
JSSC;

	//$cond = "'".($strFileName != "restartingexam.php") ? ("document.loginfrm.memno.focus()") : ("document.loginfrm.candpassword.focus()")."'";
	                if($from_page == 'R') $cond = "document.loginfrm.candpassword.focus();";
                else $cond = "document.loginfrm.memno.focus();";

	$bodyCont = " onKeyDown='return doKeyDown(event);'  ondragstart = 'return Stop(event)' onselectstart = 'return Stop(event)' onmousedown = 'return Stop(event)' onmouseup = 'return Stop(event)' onKeyPress = 'javascript:submitForm(event)' onload = $cond";

	$bodyOnload = $bodyCont;
	include("themes/default/header.php");

?>

	<div id="body_container_inner">
		<?php				   
			if(isset($resultsetTracking) && is_resource($resultsetTracking)){
				$arrSession=array();
				echo "<div class=\"innerbox_container_calogin_wrapper_above\">
						<div class=\"innerbox_container_calogin_above\">
						  <div class=\"content_container_calogin\">";
				echo "<form id=\"frmDupSession\" name=\"frmDupSession\" method=\"post\">
					  <table width=\"80%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" id=\"content_container_box_calogin_inner_tb\" >
				  <tr>
					<td width=\"29%\" align=\"left\" class=\"label-header\"><b>IP Address</b></td>
					<td width=\"50%\" class=\"label-header\"><b>Updated Date Time</b></td>
					<td width=\"21%\" rowspan=\"2\"><input name=\"deleteall\" class=\"green_button\" type=\"submit\" value=\"DELETE ALL\" />					
					</td>
				  </tr>";				
				while($rTrack=mysql_fetch_array($resultsetTracking)){						
					 array_push($arrSession,"'".$rTrack['session_id']."'");
					 echo "<tr>
					   <td width=\"29%\" class=\"label-header\">".$rTrack['host_ip']."</td>
					  <td width=\"41%\" class=\"label-header\">".$rTrack['updated_time']."</td>
					 </tr>";
				}
				$session=implode(',',$arrSession);					
				echo "<input type=\"hidden\" name=\"SID\" value=\"$session\"/></table>
			   </form>";
			   echo "</div></div></div>";
			   $emsg="You have unlogged sessions.please click \"Delete All\" to delete other sessions and proceed";
			}
												   
		  ?>
      <div class="innerbox_container_calogin_wrapper"><div class="outerbox">
	<div class="innerbox">
    	<div class="innerbox_container_calogin">			
        	<div class="pager_header_common">Candidate Login</div>
            <div class="content_container_calogin">
            	<!--<div class="content_container_box_calogin" >-->
				<form name='loginfrm' method="post">
					<input type="hidden" name="from_page" value="<?php echo $from_page; ?>" />
					<input type="hidden" name="hid_lefttime" value='<?php echo $hid_lefttime; ?>'>
					<input type='hidden' name='ta_override' value='<?php echo $taOverride; ?>'>
            	 <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" id="content_container_box_calogin_inner_tb" >
				  <tr>
					<td width="37%"><label>Membership Number</label></td>
					<?php if($from_page == 'R') { ?>
				<td width="63%"><input type="text" name="memno" class="textbox" value="<?php if(isset($MEMNO)) echo $MEMNO;?>" maxlength="20" onkeypress="return letternumber(event)" readonly /></td>
					<?php } else { ?>
					<td width="63%"><input type="text" name="memno" class="textbox" value="<?php if(isset($MEMNO)) echo $MEMNO;?>" <?php if(isset($MEMNO) && !isset($memno)) echo "readonly";?> maxlength="20" onkeypress="return letternumber(event)"/></td>
					<?php } ?>
				  </tr>
				  <tr>
					<td width="37%"><label>Password</label></td>
					<td width="63%"><input type="password" name="candpassword" class="textbox" maxlength="50" value="" onkeypress="return letternumber(event)" /></td>
				  </tr>
				 </table> 
			 </form>
            	<!--</div>-->
            </div>
            <div class="button_container_common">			
			<input name="go" id="go" class="green_button" type="button" value="Submit"  style="margin-right:6px;" onclick="javascript:validate();" />
			<input name="sub_view2" id='sub_view' class="green_button" type="button" value="Back" onclick="javascript:window.location='ta_options.php'" />
			<br/><span id="submitstatus" class="errormsg"></span>
            </div>
        </div>
		<div class="errMsg"><?php if($emsg != "") echo "$emsg</br>";?></div>
    </div>
</div></div>
    </div>
	<?php include("themes/default/footer.php");
//mysql_close($SLAVECONN);
?>
