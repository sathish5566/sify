<?php /****************************************************
* Application Name            :  IIB 
* Module Name                 :  TA Login 
* Revision Number             :  1
* Revision Date               :  -
* Table(s)                    :  -
* Tables used for only selects:  iib_ta_tracking
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              : 
* Created ON                  :  
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  22-01-2010
* Description                 :  Interface for the TA to login
*****************************************************/
	header("Expires: Mon, 26 Jul 1920 05:00:00 GMT");    // Date in the past
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");     // always modified
	header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP/1.1
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");                          // HTTP/1.0

require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");

/***************** Sify Browser Access Check ***********************/
$Referer=$_SERVER['HTTP_REFERER'];	
$url = $Referer;
$arrURL=explode("/",$url);
$arrHost=explode("www.",$arrURL[2]);
if(count($arrHost)==1)
  $host=$arrHost[0];
else
  $host=$arrHost[1];
  $arrHost1=explode(":",$host);				
  $hostname=$arrHost1[0];
$redirect = (!empty($_GET['logout']) && isset($_GET['logout']))	? $_GET['logout'] : '';
if( ($_SERVER['HTTP_REFERER'] == '') || ($hostname!=$_SERVER['HTTP_HOST']) ){
        if($redirect != 'yes')
            $useragent = $_SERVER['HTTP_USER_AGENT'];
        else
            $useragent = 'Sify Browser';

	if(trim($useragent) != 'Sify Browser')
	{
		//header("Location:browser.php");
		//exit;		
	}	
}
/***************** Sify Browser Access Check ***********************/

$emsg="";
unset($_SESSION['exam_load']);

$strRefUser=getVal($_GET['userid']);
$login = getVal($_POST['login']);
$password=getVal($_POST['password']);


if( ($login!='') && ($password!=''))
{		
	$sql="select ta_name, ta_password, password('$password'),centre_code from iib_ta_details where ta_login='$login' ";
	
	 $result=@mysql_query($sql,$SLAVECONN);
	  if (mysql_num_rows($result) > 0)
	  {
				list($taname,$pass, $userpass,$cafeID) = mysql_fetch_row($result);					
				if($pass == $userpass)
				{							
							$_SESSION['login'] = $login;
							$_SESSION['cafeID'] = $cafeID;
							$_SESSION['URL']=$_SERVER['HTTP_HOST'];
							$hostIP = $_SERVER['REMOTE_ADDR'];
							$sessionID = session_id();
							trackTALogin($login, $cafeID, $hostIP, $sessionID);							
							header("Location:ta_options.php");
							exit;
				 }
				 else
					 $emsg="Invalid Login. Please check whether the Caps Lock on your keyboard is turned on.";
	  }else
		$emsg="Invalid Login. Please check whether the Caps Lock on your keyboard is turned on.";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?PHP echo TITLE?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<NOSCRIPT>
<meta http-equiv="Refresh" content="1; URL=javascript_disabled.php">
</NOSCRIPT>
<link href="./images/iibf.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="./includes/browser.js"></script>
<script language='JavaScript' src="./includes/validations.js"></script>
<script language='JavaScript'>
function downloadHindi()
{
	var w=window.open('installfont.html',"winfont","top=2,left=2,width=600,height=500");
}
function authenticate()
{
	var w=window.open('f_login.php','win2','top=10,left=10,width=500,height=400,toolbar=no');
}
function validate()
{
	loginname=document.loginfrm.login.value;
	loginname=trim(loginname);
	document.loginfrm.login.value=loginname;
	pass=document.loginfrm.password.value;

	if (loginname=='')
	{
		alert('TA Login Empty!');
		document.loginfrm.login.focus(); 
		return;
	}
	
	 if(pass=='')
	 {
    	  alert('Password Empty!');
	  document.loginfrm.password.focus();
	  return;
     }
     document.getElementById("go").disabled=true;
     document.getElementById("submitstatus").innerHTML='please wait submitting form...';     
     document.loginfrm.submit();
}

function submitForm(e)
{	
	var keyCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
	if (keyCode == 13)
	{
		validate();
	}
}
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="javascript:document.loginfrm.login.focus();" 
 onKeyDown="return doKeyDown(event);"  ondragstart ="return Stop(event)" onselectstart="return Stop(event)"onmousedown="return Stop(event)" onmouseup="return Stop(event)" onKeyPress='javascript:submitForm(event)'>
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <?php require_once("includes/header.php");?>
  </tr>
  <tr>     
  </tr>
  <tr> 
    <td width="100%" background="images/tile.jpg" height="315">
	  <form name="loginfrm" method="post" >
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr> 
              <td align="left" valign="top">&nbsp;</td>
              <td align="left" valign="top" class="errormsg"><b>
                <?php if($emsg!="") echo $emsg;?>
                <?php if($_GET['reason']==1) echo "You Need To Login/Re-Login";?>
                <?php if($_GET['reason']==2) echo "You have already taken the exam";?>
				<?php if($_GET['reason']==3) echo "Invalid access to URL";?>
				</b></td>
              <td align="left" valign="top">&nbsp;</td>
            </tr>
            <tr> 
              <td align="left" valign="top"> <table width="90%" border="0" align="left" cellpadding="5" cellspacing="5">
                  <tr align="center"> 
                    <td class="greybluetext12"><div align="left"><b>For any help/query, 
                        please call Universal Access Number </b></div></td>
                  </tr>
                  <tr align="center"> 
                    <td class="greybluetext12"><div align="left"><b><font color="#CC0000">1901-345-0112</font></b></div></td>
                  </tr>
                  <tr align="center">
                    <td height="23" align="left" class="greybluetext12"><a href='loginform.php' style="text-decoration:none"><b><font color="#CC0000"><u>TA Forms Login</u></font></b></a></td>
                  </tr>                
              </table></td>
              <td width="279" height="160" align="left" valign="top" background="images/logintile.jpg"> 
                <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
                  <tr> 
                    <td width="125" height="30">&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr> 
                    <td>&nbsp;</td>
                    <td height="30" valign="top"> <input type="text" name="login"  class="textbox" value='<?=$strRefUser ?>' maxlength="20" size="15"  onKeyPress="return letternumber(event)"  >                    </td>
                  </tr>
                  <tr> 
                    <td>&nbsp;</td>
                    <td> <input type="password" name="password" class="textbox" maxlength="50" size="15" onKeyPress="return letternumber(event)">                    </td>
                  </tr>
                  <tr align="center" valign="bottom"> 
                    <td colspan="2" height="35">                    
                    <input class="button" type='button' value="Submit" name="go" id="go" onClick="javascript:validate();">
                     <br/><span id="submitstatus" class="errormsg"></span>
                    </td>
                  </tr>
              </table></td>
              <td width="20" height="160" align="left" valign="top">&nbsp;</td>
            </tr>
            <tr> 
              <td height="99" colspan="3" align="left" valign="top"><p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p></td>
            </tr>
          </table>
	  </form>
    </td>
  </tr>
  <tr>
  <?php include("includes/footer.php");?>
  </tr>
</table>
<br/>
</center>
</body>
</html>
