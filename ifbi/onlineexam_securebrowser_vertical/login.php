<?php /****************************************************
* Application Name            :  IIB 
* Module Name                 :  TA Login 
* Revision Number             :  1
* Revision Date               :  -
* Table(s)                    :  -
* Tables used for only selects:  iib_ta_tracking
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              : 
* Created ON                  :  
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  22-01-2010
* Description                 :  Interface for the TA to login
*****************************************************/
	header("Expires: Mon, 26 Jul 1920 05:00:00 GMT");    // Date in the past
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");     // always modified
	header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP/1.1
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");                          // HTTP/1.0

require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");

/***************** Sify Browser Access Check ***********************/
$Referer=$_SERVER['HTTP_REFERER'];	
$url = $Referer;
$arrURL=explode("/",$url);
$arrHost=explode("www.",$arrURL[2]);
if(count($arrHost)==1)
  $host=$arrHost[0];
else
  $host=$arrHost[1];
  $arrHost1=explode(":",$host);				
  $hostname=$arrHost1[0];
$redirect = (!empty($_GET['logout']) && isset($_GET['logout']))	? $_GET['logout'] : '';
if( ($_SERVER['HTTP_REFERER'] == '') || ($hostname!=$_SERVER['HTTP_HOST']) ){
        if($redirect != 'yes')
            $useragent = $_SERVER['HTTP_USER_AGENT'];
        else
            $useragent = 'Sify Browser';

	if(trim($useragent) != 'Sify Browser')
	{ 
		#header("Location:browser.php");
		#exit;		
	}	
}
/***************** Sify Browser Access Check ***********************/

$emsg="";
unset($_SESSION['exam_load']);

$strRefUser=getVal($_GET['userid']);
$login = getVal($_POST['login']);
$password=getVal($_POST['password']);


if( ($login!='') && ($password!=''))
{		
	$sql="select ta_name, ta_password, password('$password'),centre_code from iib_ta_details where ta_login='$login' ";
	
	 $result=@mysql_query($sql,$SLAVECONN);
	  if (mysql_num_rows($result) > 0)
	  {
				list($taname,$pass, $userpass,$cafeID) = mysql_fetch_row($result);					
				if($pass == $userpass)
				{							
							$_SESSION['login'] = $login;
							$_SESSION['cafeID'] = $cafeID;
							$_SESSION['URL']=$_SERVER['HTTP_HOST'];
							$hostIP = $_SERVER['REMOTE_ADDR'];
							$sessionID = session_id();
							trackTALogin($login, $cafeID, $hostIP, $sessionID);							
							header("Location:ta_options.php");
							exit;
				 }
				 else
					 $emsg="Invalid Login. Please check whether the Caps Lock on your keyboard is turned on.";
	  }else
		$emsg="Invalid Login. Please check whether the Caps Lock on your keyboard is turned on.";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sify itest</title>
<link href="./themes/default/css/style.css" rel="stylesheet" type="text/css" />
<link href="./themes/default/css/reset.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="./themes/default/css/accord_style.css" />
<script type="text/javascript" src="./includes/jquery.min.js"></script>
<script type="text/javascript" src="./includes/ddaccordion.js"></script>
<script type="text/javascript">


ddaccordion.init({
	headerclass: "submenuheader", //Shared CSS class name of headers group
	contentclass: "submenu", //Shared CSS class name of contents group
	revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
	mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
	collapseprev: true, //Collapse previous content (so only one open at any time)? true/false 
	defaultexpanded: [], //index of content(s) open by default [index1, index2, etc] [] denotes no content
	onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
	animatedefault: false, //Should contents open by default be animated into view?
	persiststate: false, //persist state of opened contents within browser session?
	toggleclass: ["prefix", "menuitemactive"], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
	togglehtml: ["suffix", "<img src='./themes/default/images/expand.png' class='statusicon' />", "<img src='./themes/default/images/minimize.png' class='statusicon' />"], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
	animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
	oninit:function(headers, expandedindices){ //custom code to run when headers have initalized
		//do nothing
	},
	onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
		//do nothing
	}
})
</script>
<script language="javascript" type="text/javascript" src="./includes/browser.js"></script>
<script language='JavaScript' src="./includes/validations.js"></script>
<script language='JavaScript'>
function downloadHindi()
{
	var w=window.open('installfont.html',"winfont","top=2,left=2,width=600,height=500");
}
function authenticate()
{
	var w=window.open('f_login.php','win2','top=10,left=10,width=500,height=400,toolbar=no');
}
function validate()
{
	loginname=document.loginfrm.login.value;
	loginname=trim(loginname);
	document.loginfrm.login.value=loginname;
	pass=document.loginfrm.password.value;

	if (loginname=='')
	{
		alert('TA Login Empty!');
		document.loginfrm.login.focus(); 
		return;
	}
	
	 if(pass=='')
	 {
    	  alert('Password Empty!');
	  document.loginfrm.password.focus();
	  return;
     }
     document.getElementById("go").disabled=true;
     document.getElementById("submitstatus").innerHTML='please wait submitting form...';     
     document.loginfrm.submit();
}

function submitForm(e)
{	
	var keyCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
	if (keyCode == 13)
	{
		validate();
	}
}
</script>
</head>

<body onLoad="javascript:document.loginfrm.login.focus();" 
 onKeyDown="return doKeyDown(event);"  ondragstart ="return Stop(event)" onselectstart="return Stop(event)"onmousedown="return Stop(event)" onmouseup="return Stop(event)" onKeyPress='javascript:submitForm(event)'>
 <form name="loginfrm" method="post" >
<div id="ta_body_wrapper">
  <div class="ta_header_container">
    <div id="ta_logo"><img src="images/sify_logo.png" width="70" height="37" /></div>
   
  </div>
  <div class="clear"></div>
  <div class="ta_container"><div id="ta_log_box">
      <h1>Test Administration Login</h1>
      <div class="ta_log_box_inner_sec"><label>TA Login </label>
            <input type="text" name="login"  class="textbox" value='<?=$strRefUser ?>' maxlength="20" size="15"  onKeyPress="return letternumber(event)"  >
            <label>TA Password</label>
                <input type="password" name="password" class="textbox" maxlength="50" size="15" onKeyPress="return letternumber(event)"> 
                        <div class="clear"></div>
       <div class="ta_log_box_inner_sec_for_pas">
	   <?php if($emsg!="") echo $emsg;?>
		<?php if($_GET['reason']==1) echo "You Need To Login/Re-Login";?>
		<?php if($_GET['reason']==2) echo "You have already taken the exam";?>
		<?php if($_GET['reason']==3) echo "Invalid access to URL";?>
	   </div>   
       <div class="ta_log_box_inner_sec_btn_div"><input name="go" id="go" class="green_button" type="button" value="Submit" style="margin-right:8px;" onClick="javascript:validate();" />
	   <br/><span id="submitstatus" class="errormsg"></span></div>
 </div>
  </div><div class="ta_body_container_bg"></div></div>
</div>
<div class="ta_fot_top_wrapper">
  <div class="ta_container">
  <div class="ta_fot_top_con_no"><h2>For any help / query, please call Universal Access Number<br /><span>1901 - 345 - 0112</span></h2></div>
  <div class="clear"></div>
  <div class="ta_fot_acc_box"><div class="glossymenu"> <a class="menuitem submenuheader" href="#" >TA Instructions</a>
        <div class="submenu">
       <div id="ta_content_box">
	   <?php include("ta_instructions.inc"); ?>	   
        </div>	
          
        </div>
<a class="menuitem submenuheader" href="#" >Freequently Asked Questions</a>
        <div class="submenu">
        <div id="ta_content_box">
		<?php include("faq.inc"); ?>		
        </div></div>
      
       
    </div></div>
    <div class="ta_fot_top_log_inval_box">
      
    </div>
  </div>
<?php include("themes/default/footer.php");
//mysql_close($SLAVECONN);
?>
