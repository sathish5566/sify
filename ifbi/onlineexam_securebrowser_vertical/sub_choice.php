<?php
/****************************************************
* Application Name            :  IIB 
* Module Name                 :  Online Exam Page
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  iib_feedback
* Tables used for only selects:  iib_feedback
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  
* Created ON                  :  
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  27-Jan-10
* Description                 :  Interface for optional subject BOI
*****************************************************/
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkCandidateLogin();
checkOptionlSubject();
$memno=$_SESSION['memno'];
$exam_code = getVal($_GET['ex']);
$subject_code = getVal($_GET['sc']);
$selExamTime = getVal($_GET['et']);


/*$exam_code = $_SESSION['ex'];
$subject_code = $_SESSION['sc'];
$selExamTime = $_SESSION['et'];
*/

$opt_query = "select subject_code,subject_name,option_id from iib_exam_subjects where option_id='$subject_code' and option_flag='Y'";
$sql_options = mysql_query($opt_query,$SLAVECONN) or errorlog('err05',"SQL:$opt_query  ".mysql_error($SLAVECONN));
$num_options = mysql_num_rows($sql_options);
	if($num_options > 0)
	{
		  $kl=0;	
		  while(list($opt_sub,$opt_subname,$optt_id)=mysql_fetch_row($sql_options))
			{
			  $opt_sub_arr[$kl] = $opt_sub;
			  $opt_subname_arr[$kl]=$opt_subname;
			  $optt_id_arr[$kl] =$optt_id;
			  $kl++;
			}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?PHP echo TITLE?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<link href="images/iibf.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="./includes/browser.js"></script>
<script language="javascript">
	function validate()
	{
		var frm = document.examFrm;
		
		myOption = -1;				
		if(typeof frm.rad_sub === null || typeof frm.rad_sub === undefined )
			return false;
		else{								
			if(frm.rad_sub.length != undefined){
				for (i=frm.rad_sub.length-1; i > -1; i--) 
				{
					if (frm.rad_sub[i].checked) {
					myOption = i; i = -1;
					}
				}
				
				if (myOption == -1) 
				{
					alert("You must select a radio button");
					return false;
				}
			}					
			frm.action="subject_confirm.php?ex=<?=$exam_code?>&sc=<?=$subject_code?>&et=<?=$selExamTime?>";
			frm.submit();
		}
	}
	
	var windowClose = 0;
	function chkParent()
	{
		if(windowClose == 0) {
			window.opener.location.href="login.php";
			self.close();			
		}
	}
	
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onKeyDown="return doKeyDown(event);" ondragstart ="return Stop(event)" onselectstart="return Stop(event)" onmousedown="return Stop(event)" onmouseup="return Stop(event)">
<Form name="examFrm" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" background="images/tile.jpg">
 <tr> 
    <? include("includes/header.php");?>
  </tr>

<tr valign="top" HEIGHT="10" ><td>&nbsp;</td></tr>
<tr valign="top" HEIGHT="10" ><td><?php include("includes/inner_header.php")?></td></tr>
<tr valign="top" HEIGHT="10" ><td>&nbsp;</td></tr>
<tr><td>
<TABLE Border="1" Bordercolor="#004a80"  align="center" cellspacing="0">
<TR><TD>
	<table border="0" cellspacing="0"  cellpadding="5">
	<tr><td class="greybluetext10" colspan="2"  bgcolor="#D1E0EF" height="35"><b>Kindly choose the version of the test you want to refer to</b></td></tr>
	<tr><td colspan="2" bgcolor="#E8EFF7">&nbsp;</td></tr>
	<tr>
	<td colspan="2" bgcolor="#E8EFF7" style="padding-left:10px" class="greybluetext12">
	<?
	 for($ii=0;$ii<count($opt_sub_arr);$ii++)
	 {

		 if( ($back_subject==$opt_sub_arr[$ii]) || (count($opt_sub_arr) == 1) )
			 $checked="checked";
		 else
			 $checked="";
	?>
	   
		<input type="radio" name="rad_sub" value="<?=$opt_sub_arr[$ii]?>" <?php echo $checked?>/>
		&nbsp;<?=$opt_subname_arr[$ii]?>
		<br/>
		<br/>
	<?
	 }
	?>	</td>
	</tr>
	
	<tr><td colspan="2" bgcolor="#E8EFF7">&nbsp;</td></tr>	
	<tr>
	<td bgcolor="#D1E0EF" align="center" colspan="2"><input type="submit" name="btnSubmit" id="btnSubmit" class="button" value="Submit" onclick="javascript:return validate();"></td>
	</table>
</TD></TR>
</TABLE>
</td><tr> 
<tr><td>&nbsp;</td></tr>
<tr>
    <?php include("includes/footer.php");?>
  </tr>
</table>
</Form>
</body>
</html>
<?php
//mysql_close($SLAVECONN);
?>
