<?php
/****************************************************
* Application Name            :  IIB 
* Module Name                 :  Online Exam Page
* Revision Number             :  1
* Revision Date               :
* Table(s)                    :  iib_feedback
* Tables used for only selects:  iib_feedback
* View(s)                     :  -
* Stored Procedure(s)         :  -
* Dependant Module(s)         :  all include files
* Output File(s)              :
* Document/Reference Material :
* Created By	              :  
* Created ON                  :  
* Last Modified By            :  Bala Murugan.S
* Last Modified Date          :  27-Jan-10
* Description                 :  Interface for optional subject BOI
*****************************************************/
require_once("dbconfig.php");
masterConnect();
slaveConnect();
require_once("session_handle.php");
require_once("login_tracking_functions.php");
checkCandidateLogin();
checkOptionlSubject();

$memno=$_SESSION['memno'];
$exam_code = getVal($_GET['ex']);
$subject_code = getVal($_GET['sc']);
$selExamTime = getVal($_GET['et']);

$post_subcode=$_REQUEST['rad_sub'];
$sql_sub_name = "select subject_name from iib_exam_subjects where subject_code = '$post_subcode' and online = 'Y'";
$res_sub_name = mysql_query($sql_sub_name,$SLAVECONN);
list($sub_name) = mysql_fetch_row($res_sub_name);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?PHP echo TITLE?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<link href="./images/iibf.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="./includes/browser.js"></script>
<script language="javascript" type="text/javascript" src="./includes/validations.js"></script>

<script language="javascript">
	function validate()
	{
		var frm = document.examFrm;		
		if(trim(frm.post_subcode.value)!=''){			
			frm.action="logincandidate1.php";
			frm.submit();
		}else
			return false;	
	}
	function bak()
	{
		  var frm = document.examFrm;
		  if(trim(frm.post_subcode.value)!=''){
			  frm.action="sub_choice.php?ex=<?=$exam_code?>&sc=<?=$subject_code?>&et=<?=$selExamTime?>";
			  frm.submit();
		  }else
		   return false;		  
	}
	
	var windowClose = 0;
	function chkParent()
	{
		if(windowClose == 0) {
			window.opener.location.href="login.php";
			self.close();			
		}
	}	
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onKeyDown="return doKeyDown(event);" ondragstart ="return Stop(event)" onselectstart="return Stop(event)" onmousedown="return Stop(event)" onmouseup="return Stop(event)" >
<Form name="examFrm" method="post">
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" background="images/tile.jpg">
    <tr>
      <? include("includes/header.php");?>
    </tr>
    <tr valign="top" height="10" >
      <td>&nbsp;</td>
    </tr>
    <tr valign="top" height="10" >
      <td><?php include("includes/inner_header.php")?></td>
    </tr>
    <tr valign="top" height="10" >
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table border="1" bordercolor="#004a80"  align="center" cellspacing="0">
          <tr>
            <td><table border="0" cellspacing="0"  cellpadding="5">
                <tr>
                  <td class="greybluetext10" colspan="2"  bgcolor="#D1E0EF" height="35"><b>Kindly confirm the group you want to attempt</b></td>
                </tr>
                <tr>
                  <td colspan="2" bgcolor="#D1E0EF">&nbsp;</td>
                </tr>
                <tr>
                  <td class="greybluetext10" colspan="2"  bgcolor="#D1E0EF" height="35"><b>You have selected <?echo $sub_name?> </b></td>
                </tr>
                <tr>
                  <td bgcolor="#D1E0EF" align="center" colspan="2"><input type="hidden" name="hidd_lan" value=<?=$lang_val?> />
                   
                      <input type="hidden" name="post_subcode" value='<?=$post_subcode?>' />                    
                      <input type="button" name="conf" id="confi" class="button" value="Confirm" onclick="javascript:return validate()" />
                    &nbsp;
                    <input type="button" name="btnBack" id="btnBack" class="button" value="Back" onclick="javascript:return bak()" />
                  </td>
                </tr>
            </table></td>
          </tr>
      </table></td>
    </tr>
    <tr> </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <?php include("includes/footer.php");?>
    </tr>
  </table>
</Form>
</body>
</html>
<?php
//mysql_close($SLAVECONN);
?>
